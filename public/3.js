(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./resources/js/frontend/33fc1.js?ver=5.3.6":
/*!**************************************************!*\
  !*** ./resources/js/frontend/33fc1.js?ver=5.3.6 ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_0__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
 * jQuery JavaScript Library v3.5.1
 * https://jquery.com/
 *
 * Includes Sizzle.js
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://jquery.org/license
 *
 * Date: 2020-05-04T22:49Z
 */
(function (global, factory) {
  "use strict";

  if (( false ? undefined : _typeof(module)) === "object" && _typeof(module.exports) === "object") {
    module.exports = global.document ? factory(global, true) : function (w) {
      if (!w.document) {
        throw new Error("jQuery requires a window with a document");
      }

      return factory(w);
    };
  } else {
    factory(global);
  }
})(typeof window !== "undefined" ? window : this, function (window, noGlobal) {
  "use strict";

  var arr = [];
  var getProto = Object.getPrototypeOf;
  var _slice = arr.slice;
  var flat = arr.flat ? function (array) {
    return arr.flat.call(array);
  } : function (array) {
    return arr.concat.apply([], array);
  };
  var push = arr.push;
  var indexOf = arr.indexOf;
  var class2type = {};
  var toString = class2type.toString;
  var hasOwn = class2type.hasOwnProperty;
  var fnToString = hasOwn.toString;
  var ObjectFunctionString = fnToString.call(Object);
  var support = {};

  var isFunction = function isFunction(obj) {
    return typeof obj === "function" && typeof obj.nodeType !== "number";
  };

  var isWindow = function isWindow(obj) {
    return obj != null && obj === obj.window;
  };

  var document = window.document;
  var preservedScriptAttributes = {
    type: true,
    src: true,
    nonce: true,
    noModule: true
  };

  function DOMEval(code, node, doc) {
    doc = doc || document;
    var i,
        val,
        script = doc.createElement("script");
    script.text = code;

    if (node) {
      for (i in preservedScriptAttributes) {
        val = node[i] || node.getAttribute && node.getAttribute(i);

        if (val) {
          script.setAttribute(i, val);
        }
      }
    }

    doc.head.appendChild(script).parentNode.removeChild(script);
  }

  function toType(obj) {
    if (obj == null) {
      return obj + "";
    }

    return _typeof(obj) === "object" || typeof obj === "function" ? class2type[toString.call(obj)] || "object" : _typeof(obj);
  }

  var version = "3.5.1",
      jQuery = function jQuery(selector, context) {
    return new jQuery.fn.init(selector, context);
  };

  jQuery.fn = jQuery.prototype = {
    jquery: version,
    constructor: jQuery,
    length: 0,
    toArray: function toArray() {
      return _slice.call(this);
    },
    get: function get(num) {
      if (num == null) {
        return _slice.call(this);
      }

      return num < 0 ? this[num + this.length] : this[num];
    },
    pushStack: function pushStack(elems) {
      var ret = jQuery.merge(this.constructor(), elems);
      ret.prevObject = this;
      return ret;
    },
    each: function each(callback) {
      return jQuery.each(this, callback);
    },
    map: function map(callback) {
      return this.pushStack(jQuery.map(this, function (elem, i) {
        return callback.call(elem, i, elem);
      }));
    },
    slice: function slice() {
      return this.pushStack(_slice.apply(this, arguments));
    },
    first: function first() {
      return this.eq(0);
    },
    last: function last() {
      return this.eq(-1);
    },
    even: function even() {
      return this.pushStack(jQuery.grep(this, function (_elem, i) {
        return (i + 1) % 2;
      }));
    },
    odd: function odd() {
      return this.pushStack(jQuery.grep(this, function (_elem, i) {
        return i % 2;
      }));
    },
    eq: function eq(i) {
      var len = this.length,
          j = +i + (i < 0 ? len : 0);
      return this.pushStack(j >= 0 && j < len ? [this[j]] : []);
    },
    end: function end() {
      return this.prevObject || this.constructor();
    },
    push: push,
    sort: arr.sort,
    splice: arr.splice
  };

  jQuery.extend = jQuery.fn.extend = function () {
    var options,
        name,
        src,
        copy,
        copyIsArray,
        clone,
        target = arguments[0] || {},
        i = 1,
        length = arguments.length,
        deep = false;

    if (typeof target === "boolean") {
      deep = target;
      target = arguments[i] || {};
      i++;
    }

    if (_typeof(target) !== "object" && !isFunction(target)) {
      target = {};
    }

    if (i === length) {
      target = this;
      i--;
    }

    for (; i < length; i++) {
      if ((options = arguments[i]) != null) {
        for (name in options) {
          copy = options[name];

          if (name === "__proto__" || target === copy) {
            continue;
          }

          if (deep && copy && (jQuery.isPlainObject(copy) || (copyIsArray = Array.isArray(copy)))) {
            src = target[name];

            if (copyIsArray && !Array.isArray(src)) {
              clone = [];
            } else if (!copyIsArray && !jQuery.isPlainObject(src)) {
              clone = {};
            } else {
              clone = src;
            }

            copyIsArray = false;
            target[name] = jQuery.extend(deep, clone, copy);
          } else if (copy !== undefined) {
            target[name] = copy;
          }
        }
      }
    }

    return target;
  };

  jQuery.extend({
    expando: "jQuery" + (version + Math.random()).replace(/\D/g, ""),
    isReady: true,
    error: function error(msg) {
      throw new Error(msg);
    },
    noop: function noop() {},
    isPlainObject: function isPlainObject(obj) {
      var proto, Ctor;

      if (!obj || toString.call(obj) !== "[object Object]") {
        return false;
      }

      proto = getProto(obj);

      if (!proto) {
        return true;
      }

      Ctor = hasOwn.call(proto, "constructor") && proto.constructor;
      return typeof Ctor === "function" && fnToString.call(Ctor) === ObjectFunctionString;
    },
    isEmptyObject: function isEmptyObject(obj) {
      var name;

      for (name in obj) {
        return false;
      }

      return true;
    },
    globalEval: function globalEval(code, options, doc) {
      DOMEval(code, {
        nonce: options && options.nonce
      }, doc);
    },
    each: function each(obj, callback) {
      var length,
          i = 0;

      if (isArrayLike(obj)) {
        length = obj.length;

        for (; i < length; i++) {
          if (callback.call(obj[i], i, obj[i]) === false) {
            break;
          }
        }
      } else {
        for (i in obj) {
          if (callback.call(obj[i], i, obj[i]) === false) {
            break;
          }
        }
      }

      return obj;
    },
    makeArray: function makeArray(arr, results) {
      var ret = results || [];

      if (arr != null) {
        if (isArrayLike(Object(arr))) {
          jQuery.merge(ret, typeof arr === "string" ? [arr] : arr);
        } else {
          push.call(ret, arr);
        }
      }

      return ret;
    },
    inArray: function inArray(elem, arr, i) {
      return arr == null ? -1 : indexOf.call(arr, elem, i);
    },
    merge: function merge(first, second) {
      var len = +second.length,
          j = 0,
          i = first.length;

      for (; j < len; j++) {
        first[i++] = second[j];
      }

      first.length = i;
      return first;
    },
    grep: function grep(elems, callback, invert) {
      var callbackInverse,
          matches = [],
          i = 0,
          length = elems.length,
          callbackExpect = !invert;

      for (; i < length; i++) {
        callbackInverse = !callback(elems[i], i);

        if (callbackInverse !== callbackExpect) {
          matches.push(elems[i]);
        }
      }

      return matches;
    },
    map: function map(elems, callback, arg) {
      var length,
          value,
          i = 0,
          ret = [];

      if (isArrayLike(elems)) {
        length = elems.length;

        for (; i < length; i++) {
          value = callback(elems[i], i, arg);

          if (value != null) {
            ret.push(value);
          }
        }
      } else {
        for (i in elems) {
          value = callback(elems[i], i, arg);

          if (value != null) {
            ret.push(value);
          }
        }
      }

      return flat(ret);
    },
    guid: 1,
    support: support
  });

  if (typeof Symbol === "function") {
    jQuery.fn[Symbol.iterator] = arr[Symbol.iterator];
  }

  jQuery.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (_i, name) {
    class2type["[object " + name + "]"] = name.toLowerCase();
  });

  function isArrayLike(obj) {
    var length = !!obj && "length" in obj && obj.length,
        type = toType(obj);

    if (isFunction(obj) || isWindow(obj)) {
      return false;
    }

    return type === "array" || length === 0 || typeof length === "number" && length > 0 && length - 1 in obj;
  }

  var Sizzle =
  /*!
   * Sizzle CSS Selector Engine v2.3.5
   * https://sizzlejs.com/
   *
   * Copyright JS Foundation and other contributors
   * Released under the MIT license
   * https://js.foundation/
   *
   * Date: 2020-03-14
   */
  function (window) {
    var i,
        support,
        Expr,
        getText,
        isXML,
        tokenize,
        compile,
        select,
        outermostContext,
        sortInput,
        hasDuplicate,
        setDocument,
        document,
        docElem,
        documentIsHTML,
        rbuggyQSA,
        rbuggyMatches,
        matches,
        contains,
        expando = "sizzle" + 1 * new Date(),
        preferredDoc = window.document,
        dirruns = 0,
        done = 0,
        classCache = createCache(),
        tokenCache = createCache(),
        compilerCache = createCache(),
        nonnativeSelectorCache = createCache(),
        sortOrder = function sortOrder(a, b) {
      if (a === b) {
        hasDuplicate = true;
      }

      return 0;
    },
        hasOwn = {}.hasOwnProperty,
        arr = [],
        pop = arr.pop,
        pushNative = arr.push,
        push = arr.push,
        slice = arr.slice,
        indexOf = function indexOf(list, elem) {
      var i = 0,
          len = list.length;

      for (; i < len; i++) {
        if (list[i] === elem) {
          return i;
        }
      }

      return -1;
    },
        booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|" + "ismap|loop|multiple|open|readonly|required|scoped",
        whitespace = "[\\x20\\t\\r\\n\\f]",
        identifier = "(?:\\\\[\\da-fA-F]{1,6}" + whitespace + "?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",
        attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace + "*([*^$|!~]?=)" + whitespace + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace + "*\\]",
        pseudos = ":(" + identifier + ")(?:\\((" + "('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" + "((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" + ".*" + ")\\)|)",
        rwhitespace = new RegExp(whitespace + "+", "g"),
        rtrim = new RegExp("^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g"),
        rcomma = new RegExp("^" + whitespace + "*," + whitespace + "*"),
        rcombinators = new RegExp("^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*"),
        rdescend = new RegExp(whitespace + "|>"),
        rpseudo = new RegExp(pseudos),
        ridentifier = new RegExp("^" + identifier + "$"),
        matchExpr = {
      "ID": new RegExp("^#(" + identifier + ")"),
      "CLASS": new RegExp("^\\.(" + identifier + ")"),
      "TAG": new RegExp("^(" + identifier + "|[*])"),
      "ATTR": new RegExp("^" + attributes),
      "PSEUDO": new RegExp("^" + pseudos),
      "CHILD": new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace + "*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace + "*(\\d+)|))" + whitespace + "*\\)|)", "i"),
      "bool": new RegExp("^(?:" + booleans + ")$", "i"),
      "needsContext": new RegExp("^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i")
    },
        rhtml = /HTML$/i,
        rinputs = /^(?:input|select|textarea|button)$/i,
        rheader = /^h\d$/i,
        rnative = /^[^{]+\{\s*\[native \w/,
        rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
        rsibling = /[+~]/,
        runescape = new RegExp("\\\\[\\da-fA-F]{1,6}" + whitespace + "?|\\\\([^\\r\\n\\f])", "g"),
        funescape = function funescape(escape, nonHex) {
      var high = "0x" + escape.slice(1) - 0x10000;
      return nonHex ? nonHex : high < 0 ? String.fromCharCode(high + 0x10000) : String.fromCharCode(high >> 10 | 0xD800, high & 0x3FF | 0xDC00);
    },
        rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
        fcssescape = function fcssescape(ch, asCodePoint) {
      if (asCodePoint) {
        if (ch === "\0") {
          return "\uFFFD";
        }

        return ch.slice(0, -1) + "\\" + ch.charCodeAt(ch.length - 1).toString(16) + " ";
      }

      return "\\" + ch;
    },
        unloadHandler = function unloadHandler() {
      setDocument();
    },
        inDisabledFieldset = addCombinator(function (elem) {
      return elem.disabled === true && elem.nodeName.toLowerCase() === "fieldset";
    }, {
      dir: "parentNode",
      next: "legend"
    });

    try {
      push.apply(arr = slice.call(preferredDoc.childNodes), preferredDoc.childNodes);
      arr[preferredDoc.childNodes.length].nodeType;
    } catch (e) {
      push = {
        apply: arr.length ? function (target, els) {
          pushNative.apply(target, slice.call(els));
        } : function (target, els) {
          var j = target.length,
              i = 0;

          while (target[j++] = els[i++]) {}

          target.length = j - 1;
        }
      };
    }

    function Sizzle(selector, context, results, seed) {
      var m,
          i,
          elem,
          nid,
          match,
          groups,
          newSelector,
          newContext = context && context.ownerDocument,
          nodeType = context ? context.nodeType : 9;
      results = results || [];

      if (typeof selector !== "string" || !selector || nodeType !== 1 && nodeType !== 9 && nodeType !== 11) {
        return results;
      }

      if (!seed) {
        setDocument(context);
        context = context || document;

        if (documentIsHTML) {
          if (nodeType !== 11 && (match = rquickExpr.exec(selector))) {
            if (m = match[1]) {
              if (nodeType === 9) {
                if (elem = context.getElementById(m)) {
                  if (elem.id === m) {
                    results.push(elem);
                    return results;
                  }
                } else {
                  return results;
                }
              } else {
                if (newContext && (elem = newContext.getElementById(m)) && contains(context, elem) && elem.id === m) {
                  results.push(elem);
                  return results;
                }
              }
            } else if (match[2]) {
              push.apply(results, context.getElementsByTagName(selector));
              return results;
            } else if ((m = match[3]) && support.getElementsByClassName && context.getElementsByClassName) {
              push.apply(results, context.getElementsByClassName(m));
              return results;
            }
          }

          if (support.qsa && !nonnativeSelectorCache[selector + " "] && (!rbuggyQSA || !rbuggyQSA.test(selector)) && (nodeType !== 1 || context.nodeName.toLowerCase() !== "object")) {
            newSelector = selector;
            newContext = context;

            if (nodeType === 1 && (rdescend.test(selector) || rcombinators.test(selector))) {
              newContext = rsibling.test(selector) && testContext(context.parentNode) || context;

              if (newContext !== context || !support.scope) {
                if (nid = context.getAttribute("id")) {
                  nid = nid.replace(rcssescape, fcssescape);
                } else {
                  context.setAttribute("id", nid = expando);
                }
              }

              groups = tokenize(selector);
              i = groups.length;

              while (i--) {
                groups[i] = (nid ? "#" + nid : ":scope") + " " + toSelector(groups[i]);
              }

              newSelector = groups.join(",");
            }

            try {
              push.apply(results, newContext.querySelectorAll(newSelector));
              return results;
            } catch (qsaError) {
              nonnativeSelectorCache(selector, true);
            } finally {
              if (nid === expando) {
                context.removeAttribute("id");
              }
            }
          }
        }
      }

      return select(selector.replace(rtrim, "$1"), context, results, seed);
    }

    function createCache() {
      var keys = [];

      function cache(key, value) {
        if (keys.push(key + " ") > Expr.cacheLength) {
          delete cache[keys.shift()];
        }

        return cache[key + " "] = value;
      }

      return cache;
    }

    function markFunction(fn) {
      fn[expando] = true;
      return fn;
    }

    function assert(fn) {
      var el = document.createElement("fieldset");

      try {
        return !!fn(el);
      } catch (e) {
        return false;
      } finally {
        if (el.parentNode) {
          el.parentNode.removeChild(el);
        }

        el = null;
      }
    }

    function addHandle(attrs, handler) {
      var arr = attrs.split("|"),
          i = arr.length;

      while (i--) {
        Expr.attrHandle[arr[i]] = handler;
      }
    }

    function siblingCheck(a, b) {
      var cur = b && a,
          diff = cur && a.nodeType === 1 && b.nodeType === 1 && a.sourceIndex - b.sourceIndex;

      if (diff) {
        return diff;
      }

      if (cur) {
        while (cur = cur.nextSibling) {
          if (cur === b) {
            return -1;
          }
        }
      }

      return a ? 1 : -1;
    }

    function createInputPseudo(type) {
      return function (elem) {
        var name = elem.nodeName.toLowerCase();
        return name === "input" && elem.type === type;
      };
    }

    function createButtonPseudo(type) {
      return function (elem) {
        var name = elem.nodeName.toLowerCase();
        return (name === "input" || name === "button") && elem.type === type;
      };
    }

    function createDisabledPseudo(disabled) {
      return function (elem) {
        if ("form" in elem) {
          if (elem.parentNode && elem.disabled === false) {
            if ("label" in elem) {
              if ("label" in elem.parentNode) {
                return elem.parentNode.disabled === disabled;
              } else {
                return elem.disabled === disabled;
              }
            }

            return elem.isDisabled === disabled || elem.isDisabled !== !disabled && inDisabledFieldset(elem) === disabled;
          }

          return elem.disabled === disabled;
        } else if ("label" in elem) {
          return elem.disabled === disabled;
        }

        return false;
      };
    }

    function createPositionalPseudo(fn) {
      return markFunction(function (argument) {
        argument = +argument;
        return markFunction(function (seed, matches) {
          var j,
              matchIndexes = fn([], seed.length, argument),
              i = matchIndexes.length;

          while (i--) {
            if (seed[j = matchIndexes[i]]) {
              seed[j] = !(matches[j] = seed[j]);
            }
          }
        });
      });
    }

    function testContext(context) {
      return context && typeof context.getElementsByTagName !== "undefined" && context;
    }

    support = Sizzle.support = {};

    isXML = Sizzle.isXML = function (elem) {
      var namespace = elem.namespaceURI,
          docElem = (elem.ownerDocument || elem).documentElement;
      return !rhtml.test(namespace || docElem && docElem.nodeName || "HTML");
    };

    setDocument = Sizzle.setDocument = function (node) {
      var hasCompare,
          subWindow,
          doc = node ? node.ownerDocument || node : preferredDoc;

      if (doc == document || doc.nodeType !== 9 || !doc.documentElement) {
        return document;
      }

      document = doc;
      docElem = document.documentElement;
      documentIsHTML = !isXML(document);

      if (preferredDoc != document && (subWindow = document.defaultView) && subWindow.top !== subWindow) {
        if (subWindow.addEventListener) {
          subWindow.addEventListener("unload", unloadHandler, false);
        } else if (subWindow.attachEvent) {
          subWindow.attachEvent("onunload", unloadHandler);
        }
      }

      support.scope = assert(function (el) {
        docElem.appendChild(el).appendChild(document.createElement("div"));
        return typeof el.querySelectorAll !== "undefined" && !el.querySelectorAll(":scope fieldset div").length;
      });
      support.attributes = assert(function (el) {
        el.className = "i";
        return !el.getAttribute("className");
      });
      support.getElementsByTagName = assert(function (el) {
        el.appendChild(document.createComment(""));
        return !el.getElementsByTagName("*").length;
      });
      support.getElementsByClassName = rnative.test(document.getElementsByClassName);
      support.getById = assert(function (el) {
        docElem.appendChild(el).id = expando;
        return !document.getElementsByName || !document.getElementsByName(expando).length;
      });

      if (support.getById) {
        Expr.filter["ID"] = function (id) {
          var attrId = id.replace(runescape, funescape);
          return function (elem) {
            return elem.getAttribute("id") === attrId;
          };
        };

        Expr.find["ID"] = function (id, context) {
          if (typeof context.getElementById !== "undefined" && documentIsHTML) {
            var elem = context.getElementById(id);
            return elem ? [elem] : [];
          }
        };
      } else {
        Expr.filter["ID"] = function (id) {
          var attrId = id.replace(runescape, funescape);
          return function (elem) {
            var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");
            return node && node.value === attrId;
          };
        };

        Expr.find["ID"] = function (id, context) {
          if (typeof context.getElementById !== "undefined" && documentIsHTML) {
            var node,
                i,
                elems,
                elem = context.getElementById(id);

            if (elem) {
              node = elem.getAttributeNode("id");

              if (node && node.value === id) {
                return [elem];
              }

              elems = context.getElementsByName(id);
              i = 0;

              while (elem = elems[i++]) {
                node = elem.getAttributeNode("id");

                if (node && node.value === id) {
                  return [elem];
                }
              }
            }

            return [];
          }
        };
      }

      Expr.find["TAG"] = support.getElementsByTagName ? function (tag, context) {
        if (typeof context.getElementsByTagName !== "undefined") {
          return context.getElementsByTagName(tag);
        } else if (support.qsa) {
          return context.querySelectorAll(tag);
        }
      } : function (tag, context) {
        var elem,
            tmp = [],
            i = 0,
            results = context.getElementsByTagName(tag);

        if (tag === "*") {
          while (elem = results[i++]) {
            if (elem.nodeType === 1) {
              tmp.push(elem);
            }
          }

          return tmp;
        }

        return results;
      };

      Expr.find["CLASS"] = support.getElementsByClassName && function (className, context) {
        if (typeof context.getElementsByClassName !== "undefined" && documentIsHTML) {
          return context.getElementsByClassName(className);
        }
      };

      rbuggyMatches = [];
      rbuggyQSA = [];

      if (support.qsa = rnative.test(document.querySelectorAll)) {
        assert(function (el) {
          var input;
          docElem.appendChild(el).innerHTML = "<a id='" + expando + "'></a>" + "<select id='" + expando + "-\r\\' msallowcapture=''>" + "<option selected=''></option></select>";

          if (el.querySelectorAll("[msallowcapture^='']").length) {
            rbuggyQSA.push("[*^$]=" + whitespace + "*(?:''|\"\")");
          }

          if (!el.querySelectorAll("[selected]").length) {
            rbuggyQSA.push("\\[" + whitespace + "*(?:value|" + booleans + ")");
          }

          if (!el.querySelectorAll("[id~=" + expando + "-]").length) {
            rbuggyQSA.push("~=");
          }

          input = document.createElement("input");
          input.setAttribute("name", "");
          el.appendChild(input);

          if (!el.querySelectorAll("[name='']").length) {
            rbuggyQSA.push("\\[" + whitespace + "*name" + whitespace + "*=" + whitespace + "*(?:''|\"\")");
          }

          if (!el.querySelectorAll(":checked").length) {
            rbuggyQSA.push(":checked");
          }

          if (!el.querySelectorAll("a#" + expando + "+*").length) {
            rbuggyQSA.push(".#.+[+~]");
          }

          el.querySelectorAll("\\\f");
          rbuggyQSA.push("[\\r\\n\\f]");
        });
        assert(function (el) {
          el.innerHTML = "<a href='' disabled='disabled'></a>" + "<select disabled='disabled'><option/></select>";
          var input = document.createElement("input");
          input.setAttribute("type", "hidden");
          el.appendChild(input).setAttribute("name", "D");

          if (el.querySelectorAll("[name=d]").length) {
            rbuggyQSA.push("name" + whitespace + "*[*^$|!~]?=");
          }

          if (el.querySelectorAll(":enabled").length !== 2) {
            rbuggyQSA.push(":enabled", ":disabled");
          }

          docElem.appendChild(el).disabled = true;

          if (el.querySelectorAll(":disabled").length !== 2) {
            rbuggyQSA.push(":enabled", ":disabled");
          }

          el.querySelectorAll("*,:x");
          rbuggyQSA.push(",.*:");
        });
      }

      if (support.matchesSelector = rnative.test(matches = docElem.matches || docElem.webkitMatchesSelector || docElem.mozMatchesSelector || docElem.oMatchesSelector || docElem.msMatchesSelector)) {
        assert(function (el) {
          support.disconnectedMatch = matches.call(el, "*");
          matches.call(el, "[s!='']:x");
          rbuggyMatches.push("!=", pseudos);
        });
      }

      rbuggyQSA = rbuggyQSA.length && new RegExp(rbuggyQSA.join("|"));
      rbuggyMatches = rbuggyMatches.length && new RegExp(rbuggyMatches.join("|"));
      hasCompare = rnative.test(docElem.compareDocumentPosition);
      contains = hasCompare || rnative.test(docElem.contains) ? function (a, b) {
        var adown = a.nodeType === 9 ? a.documentElement : a,
            bup = b && b.parentNode;
        return a === bup || !!(bup && bup.nodeType === 1 && (adown.contains ? adown.contains(bup) : a.compareDocumentPosition && a.compareDocumentPosition(bup) & 16));
      } : function (a, b) {
        if (b) {
          while (b = b.parentNode) {
            if (b === a) {
              return true;
            }
          }
        }

        return false;
      };
      sortOrder = hasCompare ? function (a, b) {
        if (a === b) {
          hasDuplicate = true;
          return 0;
        }

        var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;

        if (compare) {
          return compare;
        }

        compare = (a.ownerDocument || a) == (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1;

        if (compare & 1 || !support.sortDetached && b.compareDocumentPosition(a) === compare) {
          if (a == document || a.ownerDocument == preferredDoc && contains(preferredDoc, a)) {
            return -1;
          }

          if (b == document || b.ownerDocument == preferredDoc && contains(preferredDoc, b)) {
            return 1;
          }

          return sortInput ? indexOf(sortInput, a) - indexOf(sortInput, b) : 0;
        }

        return compare & 4 ? -1 : 1;
      } : function (a, b) {
        if (a === b) {
          hasDuplicate = true;
          return 0;
        }

        var cur,
            i = 0,
            aup = a.parentNode,
            bup = b.parentNode,
            ap = [a],
            bp = [b];

        if (!aup || !bup) {
          return a == document ? -1 : b == document ? 1 : aup ? -1 : bup ? 1 : sortInput ? indexOf(sortInput, a) - indexOf(sortInput, b) : 0;
        } else if (aup === bup) {
          return siblingCheck(a, b);
        }

        cur = a;

        while (cur = cur.parentNode) {
          ap.unshift(cur);
        }

        cur = b;

        while (cur = cur.parentNode) {
          bp.unshift(cur);
        }

        while (ap[i] === bp[i]) {
          i++;
        }

        return i ? siblingCheck(ap[i], bp[i]) : ap[i] == preferredDoc ? -1 : bp[i] == preferredDoc ? 1 : 0;
      };
      return document;
    };

    Sizzle.matches = function (expr, elements) {
      return Sizzle(expr, null, null, elements);
    };

    Sizzle.matchesSelector = function (elem, expr) {
      setDocument(elem);

      if (support.matchesSelector && documentIsHTML && !nonnativeSelectorCache[expr + " "] && (!rbuggyMatches || !rbuggyMatches.test(expr)) && (!rbuggyQSA || !rbuggyQSA.test(expr))) {
        try {
          var ret = matches.call(elem, expr);

          if (ret || support.disconnectedMatch || elem.document && elem.document.nodeType !== 11) {
            return ret;
          }
        } catch (e) {
          nonnativeSelectorCache(expr, true);
        }
      }

      return Sizzle(expr, document, null, [elem]).length > 0;
    };

    Sizzle.contains = function (context, elem) {
      if ((context.ownerDocument || context) != document) {
        setDocument(context);
      }

      return contains(context, elem);
    };

    Sizzle.attr = function (elem, name) {
      if ((elem.ownerDocument || elem) != document) {
        setDocument(elem);
      }

      var fn = Expr.attrHandle[name.toLowerCase()],
          val = fn && hasOwn.call(Expr.attrHandle, name.toLowerCase()) ? fn(elem, name, !documentIsHTML) : undefined;
      return val !== undefined ? val : support.attributes || !documentIsHTML ? elem.getAttribute(name) : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
    };

    Sizzle.escape = function (sel) {
      return (sel + "").replace(rcssescape, fcssescape);
    };

    Sizzle.error = function (msg) {
      throw new Error("Syntax error, unrecognized expression: " + msg);
    };

    Sizzle.uniqueSort = function (results) {
      var elem,
          duplicates = [],
          j = 0,
          i = 0;
      hasDuplicate = !support.detectDuplicates;
      sortInput = !support.sortStable && results.slice(0);
      results.sort(sortOrder);

      if (hasDuplicate) {
        while (elem = results[i++]) {
          if (elem === results[i]) {
            j = duplicates.push(i);
          }
        }

        while (j--) {
          results.splice(duplicates[j], 1);
        }
      }

      sortInput = null;
      return results;
    };

    getText = Sizzle.getText = function (elem) {
      var node,
          ret = "",
          i = 0,
          nodeType = elem.nodeType;

      if (!nodeType) {
        while (node = elem[i++]) {
          ret += getText(node);
        }
      } else if (nodeType === 1 || nodeType === 9 || nodeType === 11) {
        if (typeof elem.textContent === "string") {
          return elem.textContent;
        } else {
          for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
            ret += getText(elem);
          }
        }
      } else if (nodeType === 3 || nodeType === 4) {
        return elem.nodeValue;
      }

      return ret;
    };

    Expr = Sizzle.selectors = {
      cacheLength: 50,
      createPseudo: markFunction,
      match: matchExpr,
      attrHandle: {},
      find: {},
      relative: {
        ">": {
          dir: "parentNode",
          first: true
        },
        " ": {
          dir: "parentNode"
        },
        "+": {
          dir: "previousSibling",
          first: true
        },
        "~": {
          dir: "previousSibling"
        }
      },
      preFilter: {
        "ATTR": function ATTR(match) {
          match[1] = match[1].replace(runescape, funescape);
          match[3] = (match[3] || match[4] || match[5] || "").replace(runescape, funescape);

          if (match[2] === "~=") {
            match[3] = " " + match[3] + " ";
          }

          return match.slice(0, 4);
        },
        "CHILD": function CHILD(match) {
          match[1] = match[1].toLowerCase();

          if (match[1].slice(0, 3) === "nth") {
            if (!match[3]) {
              Sizzle.error(match[0]);
            }

            match[4] = +(match[4] ? match[5] + (match[6] || 1) : 2 * (match[3] === "even" || match[3] === "odd"));
            match[5] = +(match[7] + match[8] || match[3] === "odd");
          } else if (match[3]) {
            Sizzle.error(match[0]);
          }

          return match;
        },
        "PSEUDO": function PSEUDO(match) {
          var excess,
              unquoted = !match[6] && match[2];

          if (matchExpr["CHILD"].test(match[0])) {
            return null;
          }

          if (match[3]) {
            match[2] = match[4] || match[5] || "";
          } else if (unquoted && rpseudo.test(unquoted) && (excess = tokenize(unquoted, true)) && (excess = unquoted.indexOf(")", unquoted.length - excess) - unquoted.length)) {
            match[0] = match[0].slice(0, excess);
            match[2] = unquoted.slice(0, excess);
          }

          return match.slice(0, 3);
        }
      },
      filter: {
        "TAG": function TAG(nodeNameSelector) {
          var nodeName = nodeNameSelector.replace(runescape, funescape).toLowerCase();
          return nodeNameSelector === "*" ? function () {
            return true;
          } : function (elem) {
            return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
          };
        },
        "CLASS": function CLASS(className) {
          var pattern = classCache[className + " "];
          return pattern || (pattern = new RegExp("(^|" + whitespace + ")" + className + "(" + whitespace + "|$)")) && classCache(className, function (elem) {
            return pattern.test(typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "");
          });
        },
        "ATTR": function ATTR(name, operator, check) {
          return function (elem) {
            var result = Sizzle.attr(elem, name);

            if (result == null) {
              return operator === "!=";
            }

            if (!operator) {
              return true;
            }

            result += "";
            return operator === "=" ? result === check : operator === "!=" ? result !== check : operator === "^=" ? check && result.indexOf(check) === 0 : operator === "*=" ? check && result.indexOf(check) > -1 : operator === "$=" ? check && result.slice(-check.length) === check : operator === "~=" ? (" " + result.replace(rwhitespace, " ") + " ").indexOf(check) > -1 : operator === "|=" ? result === check || result.slice(0, check.length + 1) === check + "-" : false;
          };
        },
        "CHILD": function CHILD(type, what, _argument, first, last) {
          var simple = type.slice(0, 3) !== "nth",
              forward = type.slice(-4) !== "last",
              ofType = what === "of-type";
          return first === 1 && last === 0 ? function (elem) {
            return !!elem.parentNode;
          } : function (elem, _context, xml) {
            var cache,
                uniqueCache,
                outerCache,
                node,
                nodeIndex,
                start,
                dir = simple !== forward ? "nextSibling" : "previousSibling",
                parent = elem.parentNode,
                name = ofType && elem.nodeName.toLowerCase(),
                useCache = !xml && !ofType,
                diff = false;

            if (parent) {
              if (simple) {
                while (dir) {
                  node = elem;

                  while (node = node[dir]) {
                    if (ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) {
                      return false;
                    }
                  }

                  start = dir = type === "only" && !start && "nextSibling";
                }

                return true;
              }

              start = [forward ? parent.firstChild : parent.lastChild];

              if (forward && useCache) {
                node = parent;
                outerCache = node[expando] || (node[expando] = {});
                uniqueCache = outerCache[node.uniqueID] || (outerCache[node.uniqueID] = {});
                cache = uniqueCache[type] || [];
                nodeIndex = cache[0] === dirruns && cache[1];
                diff = nodeIndex && cache[2];
                node = nodeIndex && parent.childNodes[nodeIndex];

                while (node = ++nodeIndex && node && node[dir] || (diff = nodeIndex = 0) || start.pop()) {
                  if (node.nodeType === 1 && ++diff && node === elem) {
                    uniqueCache[type] = [dirruns, nodeIndex, diff];
                    break;
                  }
                }
              } else {
                if (useCache) {
                  node = elem;
                  outerCache = node[expando] || (node[expando] = {});
                  uniqueCache = outerCache[node.uniqueID] || (outerCache[node.uniqueID] = {});
                  cache = uniqueCache[type] || [];
                  nodeIndex = cache[0] === dirruns && cache[1];
                  diff = nodeIndex;
                }

                if (diff === false) {
                  while (node = ++nodeIndex && node && node[dir] || (diff = nodeIndex = 0) || start.pop()) {
                    if ((ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) && ++diff) {
                      if (useCache) {
                        outerCache = node[expando] || (node[expando] = {});
                        uniqueCache = outerCache[node.uniqueID] || (outerCache[node.uniqueID] = {});
                        uniqueCache[type] = [dirruns, diff];
                      }

                      if (node === elem) {
                        break;
                      }
                    }
                  }
                }
              }

              diff -= last;
              return diff === first || diff % first === 0 && diff / first >= 0;
            }
          };
        },
        "PSEUDO": function PSEUDO(pseudo, argument) {
          var args,
              fn = Expr.pseudos[pseudo] || Expr.setFilters[pseudo.toLowerCase()] || Sizzle.error("unsupported pseudo: " + pseudo);

          if (fn[expando]) {
            return fn(argument);
          }

          if (fn.length > 1) {
            args = [pseudo, pseudo, "", argument];
            return Expr.setFilters.hasOwnProperty(pseudo.toLowerCase()) ? markFunction(function (seed, matches) {
              var idx,
                  matched = fn(seed, argument),
                  i = matched.length;

              while (i--) {
                idx = indexOf(seed, matched[i]);
                seed[idx] = !(matches[idx] = matched[i]);
              }
            }) : function (elem) {
              return fn(elem, 0, args);
            };
          }

          return fn;
        }
      },
      pseudos: {
        "not": markFunction(function (selector) {
          var input = [],
              results = [],
              matcher = compile(selector.replace(rtrim, "$1"));
          return matcher[expando] ? markFunction(function (seed, matches, _context, xml) {
            var elem,
                unmatched = matcher(seed, null, xml, []),
                i = seed.length;

            while (i--) {
              if (elem = unmatched[i]) {
                seed[i] = !(matches[i] = elem);
              }
            }
          }) : function (elem, _context, xml) {
            input[0] = elem;
            matcher(input, null, xml, results);
            input[0] = null;
            return !results.pop();
          };
        }),
        "has": markFunction(function (selector) {
          return function (elem) {
            return Sizzle(selector, elem).length > 0;
          };
        }),
        "contains": markFunction(function (text) {
          text = text.replace(runescape, funescape);
          return function (elem) {
            return (elem.textContent || getText(elem)).indexOf(text) > -1;
          };
        }),
        "lang": markFunction(function (lang) {
          if (!ridentifier.test(lang || "")) {
            Sizzle.error("unsupported lang: " + lang);
          }

          lang = lang.replace(runescape, funescape).toLowerCase();
          return function (elem) {
            var elemLang;

            do {
              if (elemLang = documentIsHTML ? elem.lang : elem.getAttribute("xml:lang") || elem.getAttribute("lang")) {
                elemLang = elemLang.toLowerCase();
                return elemLang === lang || elemLang.indexOf(lang + "-") === 0;
              }
            } while ((elem = elem.parentNode) && elem.nodeType === 1);

            return false;
          };
        }),
        "target": function target(elem) {
          var hash = window.location && window.location.hash;
          return hash && hash.slice(1) === elem.id;
        },
        "root": function root(elem) {
          return elem === docElem;
        },
        "focus": function focus(elem) {
          return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
        },
        "enabled": createDisabledPseudo(false),
        "disabled": createDisabledPseudo(true),
        "checked": function checked(elem) {
          var nodeName = elem.nodeName.toLowerCase();
          return nodeName === "input" && !!elem.checked || nodeName === "option" && !!elem.selected;
        },
        "selected": function selected(elem) {
          if (elem.parentNode) {
            elem.parentNode.selectedIndex;
          }

          return elem.selected === true;
        },
        "empty": function empty(elem) {
          for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
            if (elem.nodeType < 6) {
              return false;
            }
          }

          return true;
        },
        "parent": function parent(elem) {
          return !Expr.pseudos["empty"](elem);
        },
        "header": function header(elem) {
          return rheader.test(elem.nodeName);
        },
        "input": function input(elem) {
          return rinputs.test(elem.nodeName);
        },
        "button": function button(elem) {
          var name = elem.nodeName.toLowerCase();
          return name === "input" && elem.type === "button" || name === "button";
        },
        "text": function text(elem) {
          var attr;
          return elem.nodeName.toLowerCase() === "input" && elem.type === "text" && ((attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text");
        },
        "first": createPositionalPseudo(function () {
          return [0];
        }),
        "last": createPositionalPseudo(function (_matchIndexes, length) {
          return [length - 1];
        }),
        "eq": createPositionalPseudo(function (_matchIndexes, length, argument) {
          return [argument < 0 ? argument + length : argument];
        }),
        "even": createPositionalPseudo(function (matchIndexes, length) {
          var i = 0;

          for (; i < length; i += 2) {
            matchIndexes.push(i);
          }

          return matchIndexes;
        }),
        "odd": createPositionalPseudo(function (matchIndexes, length) {
          var i = 1;

          for (; i < length; i += 2) {
            matchIndexes.push(i);
          }

          return matchIndexes;
        }),
        "lt": createPositionalPseudo(function (matchIndexes, length, argument) {
          var i = argument < 0 ? argument + length : argument > length ? length : argument;

          for (; --i >= 0;) {
            matchIndexes.push(i);
          }

          return matchIndexes;
        }),
        "gt": createPositionalPseudo(function (matchIndexes, length, argument) {
          var i = argument < 0 ? argument + length : argument;

          for (; ++i < length;) {
            matchIndexes.push(i);
          }

          return matchIndexes;
        })
      }
    };
    Expr.pseudos["nth"] = Expr.pseudos["eq"];

    for (i in {
      radio: true,
      checkbox: true,
      file: true,
      password: true,
      image: true
    }) {
      Expr.pseudos[i] = createInputPseudo(i);
    }

    for (i in {
      submit: true,
      reset: true
    }) {
      Expr.pseudos[i] = createButtonPseudo(i);
    }

    function setFilters() {}

    setFilters.prototype = Expr.filters = Expr.pseudos;
    Expr.setFilters = new setFilters();

    tokenize = Sizzle.tokenize = function (selector, parseOnly) {
      var matched,
          match,
          tokens,
          type,
          soFar,
          groups,
          preFilters,
          cached = tokenCache[selector + " "];

      if (cached) {
        return parseOnly ? 0 : cached.slice(0);
      }

      soFar = selector;
      groups = [];
      preFilters = Expr.preFilter;

      while (soFar) {
        if (!matched || (match = rcomma.exec(soFar))) {
          if (match) {
            soFar = soFar.slice(match[0].length) || soFar;
          }

          groups.push(tokens = []);
        }

        matched = false;

        if (match = rcombinators.exec(soFar)) {
          matched = match.shift();
          tokens.push({
            value: matched,
            type: match[0].replace(rtrim, " ")
          });
          soFar = soFar.slice(matched.length);
        }

        for (type in Expr.filter) {
          if ((match = matchExpr[type].exec(soFar)) && (!preFilters[type] || (match = preFilters[type](match)))) {
            matched = match.shift();
            tokens.push({
              value: matched,
              type: type,
              matches: match
            });
            soFar = soFar.slice(matched.length);
          }
        }

        if (!matched) {
          break;
        }
      }

      return parseOnly ? soFar.length : soFar ? Sizzle.error(selector) : tokenCache(selector, groups).slice(0);
    };

    function toSelector(tokens) {
      var i = 0,
          len = tokens.length,
          selector = "";

      for (; i < len; i++) {
        selector += tokens[i].value;
      }

      return selector;
    }

    function addCombinator(matcher, combinator, base) {
      var dir = combinator.dir,
          skip = combinator.next,
          key = skip || dir,
          checkNonElements = base && key === "parentNode",
          doneName = done++;
      return combinator.first ? function (elem, context, xml) {
        while (elem = elem[dir]) {
          if (elem.nodeType === 1 || checkNonElements) {
            return matcher(elem, context, xml);
          }
        }

        return false;
      } : function (elem, context, xml) {
        var oldCache,
            uniqueCache,
            outerCache,
            newCache = [dirruns, doneName];

        if (xml) {
          while (elem = elem[dir]) {
            if (elem.nodeType === 1 || checkNonElements) {
              if (matcher(elem, context, xml)) {
                return true;
              }
            }
          }
        } else {
          while (elem = elem[dir]) {
            if (elem.nodeType === 1 || checkNonElements) {
              outerCache = elem[expando] || (elem[expando] = {});
              uniqueCache = outerCache[elem.uniqueID] || (outerCache[elem.uniqueID] = {});

              if (skip && skip === elem.nodeName.toLowerCase()) {
                elem = elem[dir] || elem;
              } else if ((oldCache = uniqueCache[key]) && oldCache[0] === dirruns && oldCache[1] === doneName) {
                return newCache[2] = oldCache[2];
              } else {
                uniqueCache[key] = newCache;

                if (newCache[2] = matcher(elem, context, xml)) {
                  return true;
                }
              }
            }
          }
        }

        return false;
      };
    }

    function elementMatcher(matchers) {
      return matchers.length > 1 ? function (elem, context, xml) {
        var i = matchers.length;

        while (i--) {
          if (!matchers[i](elem, context, xml)) {
            return false;
          }
        }

        return true;
      } : matchers[0];
    }

    function multipleContexts(selector, contexts, results) {
      var i = 0,
          len = contexts.length;

      for (; i < len; i++) {
        Sizzle(selector, contexts[i], results);
      }

      return results;
    }

    function condense(unmatched, map, filter, context, xml) {
      var elem,
          newUnmatched = [],
          i = 0,
          len = unmatched.length,
          mapped = map != null;

      for (; i < len; i++) {
        if (elem = unmatched[i]) {
          if (!filter || filter(elem, context, xml)) {
            newUnmatched.push(elem);

            if (mapped) {
              map.push(i);
            }
          }
        }
      }

      return newUnmatched;
    }

    function setMatcher(preFilter, selector, matcher, postFilter, postFinder, postSelector) {
      if (postFilter && !postFilter[expando]) {
        postFilter = setMatcher(postFilter);
      }

      if (postFinder && !postFinder[expando]) {
        postFinder = setMatcher(postFinder, postSelector);
      }

      return markFunction(function (seed, results, context, xml) {
        var temp,
            i,
            elem,
            preMap = [],
            postMap = [],
            preexisting = results.length,
            elems = seed || multipleContexts(selector || "*", context.nodeType ? [context] : context, []),
            matcherIn = preFilter && (seed || !selector) ? condense(elems, preMap, preFilter, context, xml) : elems,
            matcherOut = matcher ? postFinder || (seed ? preFilter : preexisting || postFilter) ? [] : results : matcherIn;

        if (matcher) {
          matcher(matcherIn, matcherOut, context, xml);
        }

        if (postFilter) {
          temp = condense(matcherOut, postMap);
          postFilter(temp, [], context, xml);
          i = temp.length;

          while (i--) {
            if (elem = temp[i]) {
              matcherOut[postMap[i]] = !(matcherIn[postMap[i]] = elem);
            }
          }
        }

        if (seed) {
          if (postFinder || preFilter) {
            if (postFinder) {
              temp = [];
              i = matcherOut.length;

              while (i--) {
                if (elem = matcherOut[i]) {
                  temp.push(matcherIn[i] = elem);
                }
              }

              postFinder(null, matcherOut = [], temp, xml);
            }

            i = matcherOut.length;

            while (i--) {
              if ((elem = matcherOut[i]) && (temp = postFinder ? indexOf(seed, elem) : preMap[i]) > -1) {
                seed[temp] = !(results[temp] = elem);
              }
            }
          }
        } else {
          matcherOut = condense(matcherOut === results ? matcherOut.splice(preexisting, matcherOut.length) : matcherOut);

          if (postFinder) {
            postFinder(null, results, matcherOut, xml);
          } else {
            push.apply(results, matcherOut);
          }
        }
      });
    }

    function matcherFromTokens(tokens) {
      var checkContext,
          matcher,
          j,
          len = tokens.length,
          leadingRelative = Expr.relative[tokens[0].type],
          implicitRelative = leadingRelative || Expr.relative[" "],
          i = leadingRelative ? 1 : 0,
          matchContext = addCombinator(function (elem) {
        return elem === checkContext;
      }, implicitRelative, true),
          matchAnyContext = addCombinator(function (elem) {
        return indexOf(checkContext, elem) > -1;
      }, implicitRelative, true),
          matchers = [function (elem, context, xml) {
        var ret = !leadingRelative && (xml || context !== outermostContext) || ((checkContext = context).nodeType ? matchContext(elem, context, xml) : matchAnyContext(elem, context, xml));
        checkContext = null;
        return ret;
      }];

      for (; i < len; i++) {
        if (matcher = Expr.relative[tokens[i].type]) {
          matchers = [addCombinator(elementMatcher(matchers), matcher)];
        } else {
          matcher = Expr.filter[tokens[i].type].apply(null, tokens[i].matches);

          if (matcher[expando]) {
            j = ++i;

            for (; j < len; j++) {
              if (Expr.relative[tokens[j].type]) {
                break;
              }
            }

            return setMatcher(i > 1 && elementMatcher(matchers), i > 1 && toSelector(tokens.slice(0, i - 1).concat({
              value: tokens[i - 2].type === " " ? "*" : ""
            })).replace(rtrim, "$1"), matcher, i < j && matcherFromTokens(tokens.slice(i, j)), j < len && matcherFromTokens(tokens = tokens.slice(j)), j < len && toSelector(tokens));
          }

          matchers.push(matcher);
        }
      }

      return elementMatcher(matchers);
    }

    function matcherFromGroupMatchers(elementMatchers, setMatchers) {
      var bySet = setMatchers.length > 0,
          byElement = elementMatchers.length > 0,
          superMatcher = function superMatcher(seed, context, xml, results, outermost) {
        var elem,
            j,
            matcher,
            matchedCount = 0,
            i = "0",
            unmatched = seed && [],
            setMatched = [],
            contextBackup = outermostContext,
            elems = seed || byElement && Expr.find["TAG"]("*", outermost),
            dirrunsUnique = dirruns += contextBackup == null ? 1 : Math.random() || 0.1,
            len = elems.length;

        if (outermost) {
          outermostContext = context == document || context || outermost;
        }

        for (; i !== len && (elem = elems[i]) != null; i++) {
          if (byElement && elem) {
            j = 0;

            if (!context && elem.ownerDocument != document) {
              setDocument(elem);
              xml = !documentIsHTML;
            }

            while (matcher = elementMatchers[j++]) {
              if (matcher(elem, context || document, xml)) {
                results.push(elem);
                break;
              }
            }

            if (outermost) {
              dirruns = dirrunsUnique;
            }
          }

          if (bySet) {
            if (elem = !matcher && elem) {
              matchedCount--;
            }

            if (seed) {
              unmatched.push(elem);
            }
          }
        }

        matchedCount += i;

        if (bySet && i !== matchedCount) {
          j = 0;

          while (matcher = setMatchers[j++]) {
            matcher(unmatched, setMatched, context, xml);
          }

          if (seed) {
            if (matchedCount > 0) {
              while (i--) {
                if (!(unmatched[i] || setMatched[i])) {
                  setMatched[i] = pop.call(results);
                }
              }
            }

            setMatched = condense(setMatched);
          }

          push.apply(results, setMatched);

          if (outermost && !seed && setMatched.length > 0 && matchedCount + setMatchers.length > 1) {
            Sizzle.uniqueSort(results);
          }
        }

        if (outermost) {
          dirruns = dirrunsUnique;
          outermostContext = contextBackup;
        }

        return unmatched;
      };

      return bySet ? markFunction(superMatcher) : superMatcher;
    }

    compile = Sizzle.compile = function (selector, match) {
      var i,
          setMatchers = [],
          elementMatchers = [],
          cached = compilerCache[selector + " "];

      if (!cached) {
        if (!match) {
          match = tokenize(selector);
        }

        i = match.length;

        while (i--) {
          cached = matcherFromTokens(match[i]);

          if (cached[expando]) {
            setMatchers.push(cached);
          } else {
            elementMatchers.push(cached);
          }
        }

        cached = compilerCache(selector, matcherFromGroupMatchers(elementMatchers, setMatchers));
        cached.selector = selector;
      }

      return cached;
    };

    select = Sizzle.select = function (selector, context, results, seed) {
      var i,
          tokens,
          token,
          type,
          find,
          compiled = typeof selector === "function" && selector,
          match = !seed && tokenize(selector = compiled.selector || selector);
      results = results || [];

      if (match.length === 1) {
        tokens = match[0] = match[0].slice(0);

        if (tokens.length > 2 && (token = tokens[0]).type === "ID" && context.nodeType === 9 && documentIsHTML && Expr.relative[tokens[1].type]) {
          context = (Expr.find["ID"](token.matches[0].replace(runescape, funescape), context) || [])[0];

          if (!context) {
            return results;
          } else if (compiled) {
            context = context.parentNode;
          }

          selector = selector.slice(tokens.shift().value.length);
        }

        i = matchExpr["needsContext"].test(selector) ? 0 : tokens.length;

        while (i--) {
          token = tokens[i];

          if (Expr.relative[type = token.type]) {
            break;
          }

          if (find = Expr.find[type]) {
            if (seed = find(token.matches[0].replace(runescape, funescape), rsibling.test(tokens[0].type) && testContext(context.parentNode) || context)) {
              tokens.splice(i, 1);
              selector = seed.length && toSelector(tokens);

              if (!selector) {
                push.apply(results, seed);
                return results;
              }

              break;
            }
          }
        }
      }

      (compiled || compile(selector, match))(seed, context, !documentIsHTML, results, !context || rsibling.test(selector) && testContext(context.parentNode) || context);
      return results;
    };

    support.sortStable = expando.split("").sort(sortOrder).join("") === expando;
    support.detectDuplicates = !!hasDuplicate;
    setDocument();
    support.sortDetached = assert(function (el) {
      return el.compareDocumentPosition(document.createElement("fieldset")) & 1;
    });

    if (!assert(function (el) {
      el.innerHTML = "<a href='#'></a>";
      return el.firstChild.getAttribute("href") === "#";
    })) {
      addHandle("type|href|height|width", function (elem, name, isXML) {
        if (!isXML) {
          return elem.getAttribute(name, name.toLowerCase() === "type" ? 1 : 2);
        }
      });
    }

    if (!support.attributes || !assert(function (el) {
      el.innerHTML = "<input/>";
      el.firstChild.setAttribute("value", "");
      return el.firstChild.getAttribute("value") === "";
    })) {
      addHandle("value", function (elem, _name, isXML) {
        if (!isXML && elem.nodeName.toLowerCase() === "input") {
          return elem.defaultValue;
        }
      });
    }

    if (!assert(function (el) {
      return el.getAttribute("disabled") == null;
    })) {
      addHandle(booleans, function (elem, name, isXML) {
        var val;

        if (!isXML) {
          return elem[name] === true ? name.toLowerCase() : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
        }
      });
    }

    return Sizzle;
  }(window);

  jQuery.find = Sizzle;
  jQuery.expr = Sizzle.selectors;
  jQuery.expr[":"] = jQuery.expr.pseudos;
  jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
  jQuery.text = Sizzle.getText;
  jQuery.isXMLDoc = Sizzle.isXML;
  jQuery.contains = Sizzle.contains;
  jQuery.escapeSelector = Sizzle.escape;

  var dir = function dir(elem, _dir, until) {
    var matched = [],
        truncate = until !== undefined;

    while ((elem = elem[_dir]) && elem.nodeType !== 9) {
      if (elem.nodeType === 1) {
        if (truncate && jQuery(elem).is(until)) {
          break;
        }

        matched.push(elem);
      }
    }

    return matched;
  };

  var _siblings = function siblings(n, elem) {
    var matched = [];

    for (; n; n = n.nextSibling) {
      if (n.nodeType === 1 && n !== elem) {
        matched.push(n);
      }
    }

    return matched;
  };

  var rneedsContext = jQuery.expr.match.needsContext;

  function nodeName(elem, name) {
    return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
  }

  ;
  var rsingleTag = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

  function winnow(elements, qualifier, not) {
    if (isFunction(qualifier)) {
      return jQuery.grep(elements, function (elem, i) {
        return !!qualifier.call(elem, i, elem) !== not;
      });
    }

    if (qualifier.nodeType) {
      return jQuery.grep(elements, function (elem) {
        return elem === qualifier !== not;
      });
    }

    if (typeof qualifier !== "string") {
      return jQuery.grep(elements, function (elem) {
        return indexOf.call(qualifier, elem) > -1 !== not;
      });
    }

    return jQuery.filter(qualifier, elements, not);
  }

  jQuery.filter = function (expr, elems, not) {
    var elem = elems[0];

    if (not) {
      expr = ":not(" + expr + ")";
    }

    if (elems.length === 1 && elem.nodeType === 1) {
      return jQuery.find.matchesSelector(elem, expr) ? [elem] : [];
    }

    return jQuery.find.matches(expr, jQuery.grep(elems, function (elem) {
      return elem.nodeType === 1;
    }));
  };

  jQuery.fn.extend({
    find: function find(selector) {
      var i,
          ret,
          len = this.length,
          self = this;

      if (typeof selector !== "string") {
        return this.pushStack(jQuery(selector).filter(function () {
          for (i = 0; i < len; i++) {
            if (jQuery.contains(self[i], this)) {
              return true;
            }
          }
        }));
      }

      ret = this.pushStack([]);

      for (i = 0; i < len; i++) {
        jQuery.find(selector, self[i], ret);
      }

      return len > 1 ? jQuery.uniqueSort(ret) : ret;
    },
    filter: function filter(selector) {
      return this.pushStack(winnow(this, selector || [], false));
    },
    not: function not(selector) {
      return this.pushStack(winnow(this, selector || [], true));
    },
    is: function is(selector) {
      return !!winnow(this, typeof selector === "string" && rneedsContext.test(selector) ? jQuery(selector) : selector || [], false).length;
    }
  });

  var rootjQuery,
      rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
      init = jQuery.fn.init = function (selector, context, root) {
    var match, elem;

    if (!selector) {
      return this;
    }

    root = root || rootjQuery;

    if (typeof selector === "string") {
      if (selector[0] === "<" && selector[selector.length - 1] === ">" && selector.length >= 3) {
        match = [null, selector, null];
      } else {
        match = rquickExpr.exec(selector);
      }

      if (match && (match[1] || !context)) {
        if (match[1]) {
          context = context instanceof jQuery ? context[0] : context;
          jQuery.merge(this, jQuery.parseHTML(match[1], context && context.nodeType ? context.ownerDocument || context : document, true));

          if (rsingleTag.test(match[1]) && jQuery.isPlainObject(context)) {
            for (match in context) {
              if (isFunction(this[match])) {
                this[match](context[match]);
              } else {
                this.attr(match, context[match]);
              }
            }
          }

          return this;
        } else {
          elem = document.getElementById(match[2]);

          if (elem) {
            this[0] = elem;
            this.length = 1;
          }

          return this;
        }
      } else if (!context || context.jquery) {
        return (context || root).find(selector);
      } else {
        return this.constructor(context).find(selector);
      }
    } else if (selector.nodeType) {
      this[0] = selector;
      this.length = 1;
      return this;
    } else if (isFunction(selector)) {
      return root.ready !== undefined ? root.ready(selector) : selector(jQuery);
    }

    return jQuery.makeArray(selector, this);
  };

  init.prototype = jQuery.fn;
  rootjQuery = jQuery(document);
  var rparentsprev = /^(?:parents|prev(?:Until|All))/,
      guaranteedUnique = {
    children: true,
    contents: true,
    next: true,
    prev: true
  };
  jQuery.fn.extend({
    has: function has(target) {
      var targets = jQuery(target, this),
          l = targets.length;
      return this.filter(function () {
        var i = 0;

        for (; i < l; i++) {
          if (jQuery.contains(this, targets[i])) {
            return true;
          }
        }
      });
    },
    closest: function closest(selectors, context) {
      var cur,
          i = 0,
          l = this.length,
          matched = [],
          targets = typeof selectors !== "string" && jQuery(selectors);

      if (!rneedsContext.test(selectors)) {
        for (; i < l; i++) {
          for (cur = this[i]; cur && cur !== context; cur = cur.parentNode) {
            if (cur.nodeType < 11 && (targets ? targets.index(cur) > -1 : cur.nodeType === 1 && jQuery.find.matchesSelector(cur, selectors))) {
              matched.push(cur);
              break;
            }
          }
        }
      }

      return this.pushStack(matched.length > 1 ? jQuery.uniqueSort(matched) : matched);
    },
    index: function index(elem) {
      if (!elem) {
        return this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
      }

      if (typeof elem === "string") {
        return indexOf.call(jQuery(elem), this[0]);
      }

      return indexOf.call(this, elem.jquery ? elem[0] : elem);
    },
    add: function add(selector, context) {
      return this.pushStack(jQuery.uniqueSort(jQuery.merge(this.get(), jQuery(selector, context))));
    },
    addBack: function addBack(selector) {
      return this.add(selector == null ? this.prevObject : this.prevObject.filter(selector));
    }
  });

  function sibling(cur, dir) {
    while ((cur = cur[dir]) && cur.nodeType !== 1) {}

    return cur;
  }

  jQuery.each({
    parent: function parent(elem) {
      var parent = elem.parentNode;
      return parent && parent.nodeType !== 11 ? parent : null;
    },
    parents: function parents(elem) {
      return dir(elem, "parentNode");
    },
    parentsUntil: function parentsUntil(elem, _i, until) {
      return dir(elem, "parentNode", until);
    },
    next: function next(elem) {
      return sibling(elem, "nextSibling");
    },
    prev: function prev(elem) {
      return sibling(elem, "previousSibling");
    },
    nextAll: function nextAll(elem) {
      return dir(elem, "nextSibling");
    },
    prevAll: function prevAll(elem) {
      return dir(elem, "previousSibling");
    },
    nextUntil: function nextUntil(elem, _i, until) {
      return dir(elem, "nextSibling", until);
    },
    prevUntil: function prevUntil(elem, _i, until) {
      return dir(elem, "previousSibling", until);
    },
    siblings: function siblings(elem) {
      return _siblings((elem.parentNode || {}).firstChild, elem);
    },
    children: function children(elem) {
      return _siblings(elem.firstChild);
    },
    contents: function contents(elem) {
      if (elem.contentDocument != null && getProto(elem.contentDocument)) {
        return elem.contentDocument;
      }

      if (nodeName(elem, "template")) {
        elem = elem.content || elem;
      }

      return jQuery.merge([], elem.childNodes);
    }
  }, function (name, fn) {
    jQuery.fn[name] = function (until, selector) {
      var matched = jQuery.map(this, fn, until);

      if (name.slice(-5) !== "Until") {
        selector = until;
      }

      if (selector && typeof selector === "string") {
        matched = jQuery.filter(selector, matched);
      }

      if (this.length > 1) {
        if (!guaranteedUnique[name]) {
          jQuery.uniqueSort(matched);
        }

        if (rparentsprev.test(name)) {
          matched.reverse();
        }
      }

      return this.pushStack(matched);
    };
  });
  var rnothtmlwhite = /[^\x20\t\r\n\f]+/g;

  function createOptions(options) {
    var object = {};
    jQuery.each(options.match(rnothtmlwhite) || [], function (_, flag) {
      object[flag] = true;
    });
    return object;
  }

  jQuery.Callbacks = function (options) {
    options = typeof options === "string" ? createOptions(options) : jQuery.extend({}, options);

    var firing,
        memory,
        _fired,
        _locked,
        list = [],
        queue = [],
        firingIndex = -1,
        fire = function fire() {
      _locked = _locked || options.once;
      _fired = firing = true;

      for (; queue.length; firingIndex = -1) {
        memory = queue.shift();

        while (++firingIndex < list.length) {
          if (list[firingIndex].apply(memory[0], memory[1]) === false && options.stopOnFalse) {
            firingIndex = list.length;
            memory = false;
          }
        }
      }

      if (!options.memory) {
        memory = false;
      }

      firing = false;

      if (_locked) {
        if (memory) {
          list = [];
        } else {
          list = "";
        }
      }
    },
        self = {
      add: function add() {
        if (list) {
          if (memory && !firing) {
            firingIndex = list.length - 1;
            queue.push(memory);
          }

          (function add(args) {
            jQuery.each(args, function (_, arg) {
              if (isFunction(arg)) {
                if (!options.unique || !self.has(arg)) {
                  list.push(arg);
                }
              } else if (arg && arg.length && toType(arg) !== "string") {
                add(arg);
              }
            });
          })(arguments);

          if (memory && !firing) {
            fire();
          }
        }

        return this;
      },
      remove: function remove() {
        jQuery.each(arguments, function (_, arg) {
          var index;

          while ((index = jQuery.inArray(arg, list, index)) > -1) {
            list.splice(index, 1);

            if (index <= firingIndex) {
              firingIndex--;
            }
          }
        });
        return this;
      },
      has: function has(fn) {
        return fn ? jQuery.inArray(fn, list) > -1 : list.length > 0;
      },
      empty: function empty() {
        if (list) {
          list = [];
        }

        return this;
      },
      disable: function disable() {
        _locked = queue = [];
        list = memory = "";
        return this;
      },
      disabled: function disabled() {
        return !list;
      },
      lock: function lock() {
        _locked = queue = [];

        if (!memory && !firing) {
          list = memory = "";
        }

        return this;
      },
      locked: function locked() {
        return !!_locked;
      },
      fireWith: function fireWith(context, args) {
        if (!_locked) {
          args = args || [];
          args = [context, args.slice ? args.slice() : args];
          queue.push(args);

          if (!firing) {
            fire();
          }
        }

        return this;
      },
      fire: function fire() {
        self.fireWith(this, arguments);
        return this;
      },
      fired: function fired() {
        return !!_fired;
      }
    };

    return self;
  };

  function Identity(v) {
    return v;
  }

  function Thrower(ex) {
    throw ex;
  }

  function adoptValue(value, resolve, reject, noValue) {
    var method;

    try {
      if (value && isFunction(method = value.promise)) {
        method.call(value).done(resolve).fail(reject);
      } else if (value && isFunction(method = value.then)) {
        method.call(value, resolve, reject);
      } else {
        resolve.apply(undefined, [value].slice(noValue));
      }
    } catch (value) {
      reject.apply(undefined, [value]);
    }
  }

  jQuery.extend({
    Deferred: function Deferred(func) {
      var tuples = [["notify", "progress", jQuery.Callbacks("memory"), jQuery.Callbacks("memory"), 2], ["resolve", "done", jQuery.Callbacks("once memory"), jQuery.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", jQuery.Callbacks("once memory"), jQuery.Callbacks("once memory"), 1, "rejected"]],
          _state = "pending",
          _promise = {
        state: function state() {
          return _state;
        },
        always: function always() {
          deferred.done(arguments).fail(arguments);
          return this;
        },
        "catch": function _catch(fn) {
          return _promise.then(null, fn);
        },
        pipe: function pipe() {
          var fns = arguments;
          return jQuery.Deferred(function (newDefer) {
            jQuery.each(tuples, function (_i, tuple) {
              var fn = isFunction(fns[tuple[4]]) && fns[tuple[4]];
              deferred[tuple[1]](function () {
                var returned = fn && fn.apply(this, arguments);

                if (returned && isFunction(returned.promise)) {
                  returned.promise().progress(newDefer.notify).done(newDefer.resolve).fail(newDefer.reject);
                } else {
                  newDefer[tuple[0] + "With"](this, fn ? [returned] : arguments);
                }
              });
            });
            fns = null;
          }).promise();
        },
        then: function then(onFulfilled, onRejected, onProgress) {
          var maxDepth = 0;

          function resolve(depth, deferred, handler, special) {
            return function () {
              var that = this,
                  args = arguments,
                  mightThrow = function mightThrow() {
                var returned, then;

                if (depth < maxDepth) {
                  return;
                }

                returned = handler.apply(that, args);

                if (returned === deferred.promise()) {
                  throw new TypeError("Thenable self-resolution");
                }

                then = returned && (_typeof(returned) === "object" || typeof returned === "function") && returned.then;

                if (isFunction(then)) {
                  if (special) {
                    then.call(returned, resolve(maxDepth, deferred, Identity, special), resolve(maxDepth, deferred, Thrower, special));
                  } else {
                    maxDepth++;
                    then.call(returned, resolve(maxDepth, deferred, Identity, special), resolve(maxDepth, deferred, Thrower, special), resolve(maxDepth, deferred, Identity, deferred.notifyWith));
                  }
                } else {
                  if (handler !== Identity) {
                    that = undefined;
                    args = [returned];
                  }

                  (special || deferred.resolveWith)(that, args);
                }
              },
                  process = special ? mightThrow : function () {
                try {
                  mightThrow();
                } catch (e) {
                  if (jQuery.Deferred.exceptionHook) {
                    jQuery.Deferred.exceptionHook(e, process.stackTrace);
                  }

                  if (depth + 1 >= maxDepth) {
                    if (handler !== Thrower) {
                      that = undefined;
                      args = [e];
                    }

                    deferred.rejectWith(that, args);
                  }
                }
              };

              if (depth) {
                process();
              } else {
                if (jQuery.Deferred.getStackHook) {
                  process.stackTrace = jQuery.Deferred.getStackHook();
                }

                window.setTimeout(process);
              }
            };
          }

          return jQuery.Deferred(function (newDefer) {
            tuples[0][3].add(resolve(0, newDefer, isFunction(onProgress) ? onProgress : Identity, newDefer.notifyWith));
            tuples[1][3].add(resolve(0, newDefer, isFunction(onFulfilled) ? onFulfilled : Identity));
            tuples[2][3].add(resolve(0, newDefer, isFunction(onRejected) ? onRejected : Thrower));
          }).promise();
        },
        promise: function promise(obj) {
          return obj != null ? jQuery.extend(obj, _promise) : _promise;
        }
      },
          deferred = {};
      jQuery.each(tuples, function (i, tuple) {
        var list = tuple[2],
            stateString = tuple[5];
        _promise[tuple[1]] = list.add;

        if (stateString) {
          list.add(function () {
            _state = stateString;
          }, tuples[3 - i][2].disable, tuples[3 - i][3].disable, tuples[0][2].lock, tuples[0][3].lock);
        }

        list.add(tuple[3].fire);

        deferred[tuple[0]] = function () {
          deferred[tuple[0] + "With"](this === deferred ? undefined : this, arguments);
          return this;
        };

        deferred[tuple[0] + "With"] = list.fireWith;
      });

      _promise.promise(deferred);

      if (func) {
        func.call(deferred, deferred);
      }

      return deferred;
    },
    when: function when(singleValue) {
      var remaining = arguments.length,
          i = remaining,
          resolveContexts = Array(i),
          resolveValues = _slice.call(arguments),
          master = jQuery.Deferred(),
          updateFunc = function updateFunc(i) {
        return function (value) {
          resolveContexts[i] = this;
          resolveValues[i] = arguments.length > 1 ? _slice.call(arguments) : value;

          if (! --remaining) {
            master.resolveWith(resolveContexts, resolveValues);
          }
        };
      };

      if (remaining <= 1) {
        adoptValue(singleValue, master.done(updateFunc(i)).resolve, master.reject, !remaining);

        if (master.state() === "pending" || isFunction(resolveValues[i] && resolveValues[i].then)) {
          return master.then();
        }
      }

      while (i--) {
        adoptValue(resolveValues[i], updateFunc(i), master.reject);
      }

      return master.promise();
    }
  });
  var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;

  jQuery.Deferred.exceptionHook = function (error, stack) {
    if (window.console && window.console.warn && error && rerrorNames.test(error.name)) {
      window.console.warn("jQuery.Deferred exception: " + error.message, error.stack, stack);
    }
  };

  jQuery.readyException = function (error) {
    window.setTimeout(function () {
      throw error;
    });
  };

  var readyList = jQuery.Deferred();

  jQuery.fn.ready = function (fn) {
    readyList.then(fn)["catch"](function (error) {
      jQuery.readyException(error);
    });
    return this;
  };

  jQuery.extend({
    isReady: false,
    readyWait: 1,
    ready: function ready(wait) {
      if (wait === true ? --jQuery.readyWait : jQuery.isReady) {
        return;
      }

      jQuery.isReady = true;

      if (wait !== true && --jQuery.readyWait > 0) {
        return;
      }

      readyList.resolveWith(document, [jQuery]);
    }
  });
  jQuery.ready.then = readyList.then;

  function completed() {
    document.removeEventListener("DOMContentLoaded", completed);
    window.removeEventListener("load", completed);
    jQuery.ready();
  }

  if (document.readyState === "complete" || document.readyState !== "loading" && !document.documentElement.doScroll) {
    window.setTimeout(jQuery.ready);
  } else {
    document.addEventListener("DOMContentLoaded", completed);
    window.addEventListener("load", completed);
  }

  var access = function access(elems, fn, key, value, chainable, emptyGet, raw) {
    var i = 0,
        len = elems.length,
        bulk = key == null;

    if (toType(key) === "object") {
      chainable = true;

      for (i in key) {
        access(elems, fn, i, key[i], true, emptyGet, raw);
      }
    } else if (value !== undefined) {
      chainable = true;

      if (!isFunction(value)) {
        raw = true;
      }

      if (bulk) {
        if (raw) {
          fn.call(elems, value);
          fn = null;
        } else {
          bulk = fn;

          fn = function fn(elem, _key, value) {
            return bulk.call(jQuery(elem), value);
          };
        }
      }

      if (fn) {
        for (; i < len; i++) {
          fn(elems[i], key, raw ? value : value.call(elems[i], i, fn(elems[i], key)));
        }
      }
    }

    if (chainable) {
      return elems;
    }

    if (bulk) {
      return fn.call(elems);
    }

    return len ? fn(elems[0], key) : emptyGet;
  };

  var rmsPrefix = /^-ms-/,
      rdashAlpha = /-([a-z])/g;

  function fcamelCase(_all, letter) {
    return letter.toUpperCase();
  }

  function camelCase(string) {
    return string.replace(rmsPrefix, "ms-").replace(rdashAlpha, fcamelCase);
  }

  var acceptData = function acceptData(owner) {
    return owner.nodeType === 1 || owner.nodeType === 9 || !+owner.nodeType;
  };

  function Data() {
    this.expando = jQuery.expando + Data.uid++;
  }

  Data.uid = 1;
  Data.prototype = {
    cache: function cache(owner) {
      var value = owner[this.expando];

      if (!value) {
        value = {};

        if (acceptData(owner)) {
          if (owner.nodeType) {
            owner[this.expando] = value;
          } else {
            Object.defineProperty(owner, this.expando, {
              value: value,
              configurable: true
            });
          }
        }
      }

      return value;
    },
    set: function set(owner, data, value) {
      var prop,
          cache = this.cache(owner);

      if (typeof data === "string") {
        cache[camelCase(data)] = value;
      } else {
        for (prop in data) {
          cache[camelCase(prop)] = data[prop];
        }
      }

      return cache;
    },
    get: function get(owner, key) {
      return key === undefined ? this.cache(owner) : owner[this.expando] && owner[this.expando][camelCase(key)];
    },
    access: function access(owner, key, value) {
      if (key === undefined || key && typeof key === "string" && value === undefined) {
        return this.get(owner, key);
      }

      this.set(owner, key, value);
      return value !== undefined ? value : key;
    },
    remove: function remove(owner, key) {
      var i,
          cache = owner[this.expando];

      if (cache === undefined) {
        return;
      }

      if (key !== undefined) {
        if (Array.isArray(key)) {
          key = key.map(camelCase);
        } else {
          key = camelCase(key);
          key = key in cache ? [key] : key.match(rnothtmlwhite) || [];
        }

        i = key.length;

        while (i--) {
          delete cache[key[i]];
        }
      }

      if (key === undefined || jQuery.isEmptyObject(cache)) {
        if (owner.nodeType) {
          owner[this.expando] = undefined;
        } else {
          delete owner[this.expando];
        }
      }
    },
    hasData: function hasData(owner) {
      var cache = owner[this.expando];
      return cache !== undefined && !jQuery.isEmptyObject(cache);
    }
  };
  var dataPriv = new Data();
  var dataUser = new Data();
  var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
      rmultiDash = /[A-Z]/g;

  function getData(data) {
    if (data === "true") {
      return true;
    }

    if (data === "false") {
      return false;
    }

    if (data === "null") {
      return null;
    }

    if (data === +data + "") {
      return +data;
    }

    if (rbrace.test(data)) {
      return JSON.parse(data);
    }

    return data;
  }

  function dataAttr(elem, key, data) {
    var name;

    if (data === undefined && elem.nodeType === 1) {
      name = "data-" + key.replace(rmultiDash, "-$&").toLowerCase();
      data = elem.getAttribute(name);

      if (typeof data === "string") {
        try {
          data = getData(data);
        } catch (e) {}

        dataUser.set(elem, key, data);
      } else {
        data = undefined;
      }
    }

    return data;
  }

  jQuery.extend({
    hasData: function hasData(elem) {
      return dataUser.hasData(elem) || dataPriv.hasData(elem);
    },
    data: function data(elem, name, _data) {
      return dataUser.access(elem, name, _data);
    },
    removeData: function removeData(elem, name) {
      dataUser.remove(elem, name);
    },
    _data: function _data(elem, name, data) {
      return dataPriv.access(elem, name, data);
    },
    _removeData: function _removeData(elem, name) {
      dataPriv.remove(elem, name);
    }
  });
  jQuery.fn.extend({
    data: function data(key, value) {
      var i,
          name,
          data,
          elem = this[0],
          attrs = elem && elem.attributes;

      if (key === undefined) {
        if (this.length) {
          data = dataUser.get(elem);

          if (elem.nodeType === 1 && !dataPriv.get(elem, "hasDataAttrs")) {
            i = attrs.length;

            while (i--) {
              if (attrs[i]) {
                name = attrs[i].name;

                if (name.indexOf("data-") === 0) {
                  name = camelCase(name.slice(5));
                  dataAttr(elem, name, data[name]);
                }
              }
            }

            dataPriv.set(elem, "hasDataAttrs", true);
          }
        }

        return data;
      }

      if (_typeof(key) === "object") {
        return this.each(function () {
          dataUser.set(this, key);
        });
      }

      return access(this, function (value) {
        var data;

        if (elem && value === undefined) {
          data = dataUser.get(elem, key);

          if (data !== undefined) {
            return data;
          }

          data = dataAttr(elem, key);

          if (data !== undefined) {
            return data;
          }

          return;
        }

        this.each(function () {
          dataUser.set(this, key, value);
        });
      }, null, value, arguments.length > 1, null, true);
    },
    removeData: function removeData(key) {
      return this.each(function () {
        dataUser.remove(this, key);
      });
    }
  });
  jQuery.extend({
    queue: function queue(elem, type, data) {
      var queue;

      if (elem) {
        type = (type || "fx") + "queue";
        queue = dataPriv.get(elem, type);

        if (data) {
          if (!queue || Array.isArray(data)) {
            queue = dataPriv.access(elem, type, jQuery.makeArray(data));
          } else {
            queue.push(data);
          }
        }

        return queue || [];
      }
    },
    dequeue: function dequeue(elem, type) {
      type = type || "fx";

      var queue = jQuery.queue(elem, type),
          startLength = queue.length,
          fn = queue.shift(),
          hooks = jQuery._queueHooks(elem, type),
          next = function next() {
        jQuery.dequeue(elem, type);
      };

      if (fn === "inprogress") {
        fn = queue.shift();
        startLength--;
      }

      if (fn) {
        if (type === "fx") {
          queue.unshift("inprogress");
        }

        delete hooks.stop;
        fn.call(elem, next, hooks);
      }

      if (!startLength && hooks) {
        hooks.empty.fire();
      }
    },
    _queueHooks: function _queueHooks(elem, type) {
      var key = type + "queueHooks";
      return dataPriv.get(elem, key) || dataPriv.access(elem, key, {
        empty: jQuery.Callbacks("once memory").add(function () {
          dataPriv.remove(elem, [type + "queue", key]);
        })
      });
    }
  });
  jQuery.fn.extend({
    queue: function queue(type, data) {
      var setter = 2;

      if (typeof type !== "string") {
        data = type;
        type = "fx";
        setter--;
      }

      if (arguments.length < setter) {
        return jQuery.queue(this[0], type);
      }

      return data === undefined ? this : this.each(function () {
        var queue = jQuery.queue(this, type, data);

        jQuery._queueHooks(this, type);

        if (type === "fx" && queue[0] !== "inprogress") {
          jQuery.dequeue(this, type);
        }
      });
    },
    dequeue: function dequeue(type) {
      return this.each(function () {
        jQuery.dequeue(this, type);
      });
    },
    clearQueue: function clearQueue(type) {
      return this.queue(type || "fx", []);
    },
    promise: function promise(type, obj) {
      var tmp,
          count = 1,
          defer = jQuery.Deferred(),
          elements = this,
          i = this.length,
          resolve = function resolve() {
        if (! --count) {
          defer.resolveWith(elements, [elements]);
        }
      };

      if (typeof type !== "string") {
        obj = type;
        type = undefined;
      }

      type = type || "fx";

      while (i--) {
        tmp = dataPriv.get(elements[i], type + "queueHooks");

        if (tmp && tmp.empty) {
          count++;
          tmp.empty.add(resolve);
        }
      }

      resolve();
      return defer.promise(obj);
    }
  });
  var pnum = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source;
  var rcssNum = new RegExp("^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i");
  var cssExpand = ["Top", "Right", "Bottom", "Left"];
  var documentElement = document.documentElement;

  var isAttached = function isAttached(elem) {
    return jQuery.contains(elem.ownerDocument, elem);
  },
      composed = {
    composed: true
  };

  if (documentElement.getRootNode) {
    isAttached = function isAttached(elem) {
      return jQuery.contains(elem.ownerDocument, elem) || elem.getRootNode(composed) === elem.ownerDocument;
    };
  }

  var isHiddenWithinTree = function isHiddenWithinTree(elem, el) {
    elem = el || elem;
    return elem.style.display === "none" || elem.style.display === "" && isAttached(elem) && jQuery.css(elem, "display") === "none";
  };

  function adjustCSS(elem, prop, valueParts, tween) {
    var adjusted,
        scale,
        maxIterations = 20,
        currentValue = tween ? function () {
      return tween.cur();
    } : function () {
      return jQuery.css(elem, prop, "");
    },
        initial = currentValue(),
        unit = valueParts && valueParts[3] || (jQuery.cssNumber[prop] ? "" : "px"),
        initialInUnit = elem.nodeType && (jQuery.cssNumber[prop] || unit !== "px" && +initial) && rcssNum.exec(jQuery.css(elem, prop));

    if (initialInUnit && initialInUnit[3] !== unit) {
      initial = initial / 2;
      unit = unit || initialInUnit[3];
      initialInUnit = +initial || 1;

      while (maxIterations--) {
        jQuery.style(elem, prop, initialInUnit + unit);

        if ((1 - scale) * (1 - (scale = currentValue() / initial || 0.5)) <= 0) {
          maxIterations = 0;
        }

        initialInUnit = initialInUnit / scale;
      }

      initialInUnit = initialInUnit * 2;
      jQuery.style(elem, prop, initialInUnit + unit);
      valueParts = valueParts || [];
    }

    if (valueParts) {
      initialInUnit = +initialInUnit || +initial || 0;
      adjusted = valueParts[1] ? initialInUnit + (valueParts[1] + 1) * valueParts[2] : +valueParts[2];

      if (tween) {
        tween.unit = unit;
        tween.start = initialInUnit;
        tween.end = adjusted;
      }
    }

    return adjusted;
  }

  var defaultDisplayMap = {};

  function getDefaultDisplay(elem) {
    var temp,
        doc = elem.ownerDocument,
        nodeName = elem.nodeName,
        display = defaultDisplayMap[nodeName];

    if (display) {
      return display;
    }

    temp = doc.body.appendChild(doc.createElement(nodeName));
    display = jQuery.css(temp, "display");
    temp.parentNode.removeChild(temp);

    if (display === "none") {
      display = "block";
    }

    defaultDisplayMap[nodeName] = display;
    return display;
  }

  function showHide(elements, show) {
    var display,
        elem,
        values = [],
        index = 0,
        length = elements.length;

    for (; index < length; index++) {
      elem = elements[index];

      if (!elem.style) {
        continue;
      }

      display = elem.style.display;

      if (show) {
        if (display === "none") {
          values[index] = dataPriv.get(elem, "display") || null;

          if (!values[index]) {
            elem.style.display = "";
          }
        }

        if (elem.style.display === "" && isHiddenWithinTree(elem)) {
          values[index] = getDefaultDisplay(elem);
        }
      } else {
        if (display !== "none") {
          values[index] = "none";
          dataPriv.set(elem, "display", display);
        }
      }
    }

    for (index = 0; index < length; index++) {
      if (values[index] != null) {
        elements[index].style.display = values[index];
      }
    }

    return elements;
  }

  jQuery.fn.extend({
    show: function show() {
      return showHide(this, true);
    },
    hide: function hide() {
      return showHide(this);
    },
    toggle: function toggle(state) {
      if (typeof state === "boolean") {
        return state ? this.show() : this.hide();
      }

      return this.each(function () {
        if (isHiddenWithinTree(this)) {
          jQuery(this).show();
        } else {
          jQuery(this).hide();
        }
      });
    }
  });
  var rcheckableType = /^(?:checkbox|radio)$/i;
  var rtagName = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i;
  var rscriptType = /^$|^module$|\/(?:java|ecma)script/i;

  (function () {
    var fragment = document.createDocumentFragment(),
        div = fragment.appendChild(document.createElement("div")),
        input = document.createElement("input");
    input.setAttribute("type", "radio");
    input.setAttribute("checked", "checked");
    input.setAttribute("name", "t");
    div.appendChild(input);
    support.checkClone = div.cloneNode(true).cloneNode(true).lastChild.checked;
    div.innerHTML = "<textarea>x</textarea>";
    support.noCloneChecked = !!div.cloneNode(true).lastChild.defaultValue;
    div.innerHTML = "<option></option>";
    support.option = !!div.lastChild;
  })();

  var wrapMap = {
    thead: [1, "<table>", "</table>"],
    col: [2, "<table><colgroup>", "</colgroup></table>"],
    tr: [2, "<table><tbody>", "</tbody></table>"],
    td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
    _default: [0, "", ""]
  };
  wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
  wrapMap.th = wrapMap.td;

  if (!support.option) {
    wrapMap.optgroup = wrapMap.option = [1, "<select multiple='multiple'>", "</select>"];
  }

  function getAll(context, tag) {
    var ret;

    if (typeof context.getElementsByTagName !== "undefined") {
      ret = context.getElementsByTagName(tag || "*");
    } else if (typeof context.querySelectorAll !== "undefined") {
      ret = context.querySelectorAll(tag || "*");
    } else {
      ret = [];
    }

    if (tag === undefined || tag && nodeName(context, tag)) {
      return jQuery.merge([context], ret);
    }

    return ret;
  }

  function setGlobalEval(elems, refElements) {
    var i = 0,
        l = elems.length;

    for (; i < l; i++) {
      dataPriv.set(elems[i], "globalEval", !refElements || dataPriv.get(refElements[i], "globalEval"));
    }
  }

  var rhtml = /<|&#?\w+;/;

  function buildFragment(elems, context, scripts, selection, ignored) {
    var elem,
        tmp,
        tag,
        wrap,
        attached,
        j,
        fragment = context.createDocumentFragment(),
        nodes = [],
        i = 0,
        l = elems.length;

    for (; i < l; i++) {
      elem = elems[i];

      if (elem || elem === 0) {
        if (toType(elem) === "object") {
          jQuery.merge(nodes, elem.nodeType ? [elem] : elem);
        } else if (!rhtml.test(elem)) {
          nodes.push(context.createTextNode(elem));
        } else {
          tmp = tmp || fragment.appendChild(context.createElement("div"));
          tag = (rtagName.exec(elem) || ["", ""])[1].toLowerCase();
          wrap = wrapMap[tag] || wrapMap._default;
          tmp.innerHTML = wrap[1] + jQuery.htmlPrefilter(elem) + wrap[2];
          j = wrap[0];

          while (j--) {
            tmp = tmp.lastChild;
          }

          jQuery.merge(nodes, tmp.childNodes);
          tmp = fragment.firstChild;
          tmp.textContent = "";
        }
      }
    }

    fragment.textContent = "";
    i = 0;

    while (elem = nodes[i++]) {
      if (selection && jQuery.inArray(elem, selection) > -1) {
        if (ignored) {
          ignored.push(elem);
        }

        continue;
      }

      attached = isAttached(elem);
      tmp = getAll(fragment.appendChild(elem), "script");

      if (attached) {
        setGlobalEval(tmp);
      }

      if (scripts) {
        j = 0;

        while (elem = tmp[j++]) {
          if (rscriptType.test(elem.type || "")) {
            scripts.push(elem);
          }
        }
      }
    }

    return fragment;
  }

  var rkeyEvent = /^key/,
      rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
      rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

  function returnTrue() {
    return true;
  }

  function returnFalse() {
    return false;
  }

  function expectSync(elem, type) {
    return elem === safeActiveElement() === (type === "focus");
  }

  function safeActiveElement() {
    try {
      return document.activeElement;
    } catch (err) {}
  }

  function _on(elem, types, selector, data, fn, one) {
    var origFn, type;

    if (_typeof(types) === "object") {
      if (typeof selector !== "string") {
        data = data || selector;
        selector = undefined;
      }

      for (type in types) {
        _on(elem, type, selector, data, types[type], one);
      }

      return elem;
    }

    if (data == null && fn == null) {
      fn = selector;
      data = selector = undefined;
    } else if (fn == null) {
      if (typeof selector === "string") {
        fn = data;
        data = undefined;
      } else {
        fn = data;
        data = selector;
        selector = undefined;
      }
    }

    if (fn === false) {
      fn = returnFalse;
    } else if (!fn) {
      return elem;
    }

    if (one === 1) {
      origFn = fn;

      fn = function fn(event) {
        jQuery().off(event);
        return origFn.apply(this, arguments);
      };

      fn.guid = origFn.guid || (origFn.guid = jQuery.guid++);
    }

    return elem.each(function () {
      jQuery.event.add(this, types, fn, data, selector);
    });
  }

  jQuery.event = {
    global: {},
    add: function add(elem, types, handler, data, selector) {
      var handleObjIn,
          eventHandle,
          tmp,
          events,
          t,
          handleObj,
          special,
          handlers,
          type,
          namespaces,
          origType,
          elemData = dataPriv.get(elem);

      if (!acceptData(elem)) {
        return;
      }

      if (handler.handler) {
        handleObjIn = handler;
        handler = handleObjIn.handler;
        selector = handleObjIn.selector;
      }

      if (selector) {
        jQuery.find.matchesSelector(documentElement, selector);
      }

      if (!handler.guid) {
        handler.guid = jQuery.guid++;
      }

      if (!(events = elemData.events)) {
        events = elemData.events = Object.create(null);
      }

      if (!(eventHandle = elemData.handle)) {
        eventHandle = elemData.handle = function (e) {
          return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ? jQuery.event.dispatch.apply(elem, arguments) : undefined;
        };
      }

      types = (types || "").match(rnothtmlwhite) || [""];
      t = types.length;

      while (t--) {
        tmp = rtypenamespace.exec(types[t]) || [];
        type = origType = tmp[1];
        namespaces = (tmp[2] || "").split(".").sort();

        if (!type) {
          continue;
        }

        special = jQuery.event.special[type] || {};
        type = (selector ? special.delegateType : special.bindType) || type;
        special = jQuery.event.special[type] || {};
        handleObj = jQuery.extend({
          type: type,
          origType: origType,
          data: data,
          handler: handler,
          guid: handler.guid,
          selector: selector,
          needsContext: selector && jQuery.expr.match.needsContext.test(selector),
          namespace: namespaces.join(".")
        }, handleObjIn);

        if (!(handlers = events[type])) {
          handlers = events[type] = [];
          handlers.delegateCount = 0;

          if (!special.setup || special.setup.call(elem, data, namespaces, eventHandle) === false) {
            if (elem.addEventListener) {
              elem.addEventListener(type, eventHandle);
            }
          }
        }

        if (special.add) {
          special.add.call(elem, handleObj);

          if (!handleObj.handler.guid) {
            handleObj.handler.guid = handler.guid;
          }
        }

        if (selector) {
          handlers.splice(handlers.delegateCount++, 0, handleObj);
        } else {
          handlers.push(handleObj);
        }

        jQuery.event.global[type] = true;
      }
    },
    remove: function remove(elem, types, handler, selector, mappedTypes) {
      var j,
          origCount,
          tmp,
          events,
          t,
          handleObj,
          special,
          handlers,
          type,
          namespaces,
          origType,
          elemData = dataPriv.hasData(elem) && dataPriv.get(elem);

      if (!elemData || !(events = elemData.events)) {
        return;
      }

      types = (types || "").match(rnothtmlwhite) || [""];
      t = types.length;

      while (t--) {
        tmp = rtypenamespace.exec(types[t]) || [];
        type = origType = tmp[1];
        namespaces = (tmp[2] || "").split(".").sort();

        if (!type) {
          for (type in events) {
            jQuery.event.remove(elem, type + types[t], handler, selector, true);
          }

          continue;
        }

        special = jQuery.event.special[type] || {};
        type = (selector ? special.delegateType : special.bindType) || type;
        handlers = events[type] || [];
        tmp = tmp[2] && new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)");
        origCount = j = handlers.length;

        while (j--) {
          handleObj = handlers[j];

          if ((mappedTypes || origType === handleObj.origType) && (!handler || handler.guid === handleObj.guid) && (!tmp || tmp.test(handleObj.namespace)) && (!selector || selector === handleObj.selector || selector === "**" && handleObj.selector)) {
            handlers.splice(j, 1);

            if (handleObj.selector) {
              handlers.delegateCount--;
            }

            if (special.remove) {
              special.remove.call(elem, handleObj);
            }
          }
        }

        if (origCount && !handlers.length) {
          if (!special.teardown || special.teardown.call(elem, namespaces, elemData.handle) === false) {
            jQuery.removeEvent(elem, type, elemData.handle);
          }

          delete events[type];
        }
      }

      if (jQuery.isEmptyObject(events)) {
        dataPriv.remove(elem, "handle events");
      }
    },
    dispatch: function dispatch(nativeEvent) {
      var i,
          j,
          ret,
          matched,
          handleObj,
          handlerQueue,
          args = new Array(arguments.length),
          event = jQuery.event.fix(nativeEvent),
          handlers = (dataPriv.get(this, "events") || Object.create(null))[event.type] || [],
          special = jQuery.event.special[event.type] || {};
      args[0] = event;

      for (i = 1; i < arguments.length; i++) {
        args[i] = arguments[i];
      }

      event.delegateTarget = this;

      if (special.preDispatch && special.preDispatch.call(this, event) === false) {
        return;
      }

      handlerQueue = jQuery.event.handlers.call(this, event, handlers);
      i = 0;

      while ((matched = handlerQueue[i++]) && !event.isPropagationStopped()) {
        event.currentTarget = matched.elem;
        j = 0;

        while ((handleObj = matched.handlers[j++]) && !event.isImmediatePropagationStopped()) {
          if (!event.rnamespace || handleObj.namespace === false || event.rnamespace.test(handleObj.namespace)) {
            event.handleObj = handleObj;
            event.data = handleObj.data;
            ret = ((jQuery.event.special[handleObj.origType] || {}).handle || handleObj.handler).apply(matched.elem, args);

            if (ret !== undefined) {
              if ((event.result = ret) === false) {
                event.preventDefault();
                event.stopPropagation();
              }
            }
          }
        }
      }

      if (special.postDispatch) {
        special.postDispatch.call(this, event);
      }

      return event.result;
    },
    handlers: function handlers(event, _handlers) {
      var i,
          handleObj,
          sel,
          matchedHandlers,
          matchedSelectors,
          handlerQueue = [],
          delegateCount = _handlers.delegateCount,
          cur = event.target;

      if (delegateCount && cur.nodeType && !(event.type === "click" && event.button >= 1)) {
        for (; cur !== this; cur = cur.parentNode || this) {
          if (cur.nodeType === 1 && !(event.type === "click" && cur.disabled === true)) {
            matchedHandlers = [];
            matchedSelectors = {};

            for (i = 0; i < delegateCount; i++) {
              handleObj = _handlers[i];
              sel = handleObj.selector + " ";

              if (matchedSelectors[sel] === undefined) {
                matchedSelectors[sel] = handleObj.needsContext ? jQuery(sel, this).index(cur) > -1 : jQuery.find(sel, this, null, [cur]).length;
              }

              if (matchedSelectors[sel]) {
                matchedHandlers.push(handleObj);
              }
            }

            if (matchedHandlers.length) {
              handlerQueue.push({
                elem: cur,
                handlers: matchedHandlers
              });
            }
          }
        }
      }

      cur = this;

      if (delegateCount < _handlers.length) {
        handlerQueue.push({
          elem: cur,
          handlers: _handlers.slice(delegateCount)
        });
      }

      return handlerQueue;
    },
    addProp: function addProp(name, hook) {
      Object.defineProperty(jQuery.Event.prototype, name, {
        enumerable: true,
        configurable: true,
        get: isFunction(hook) ? function () {
          if (this.originalEvent) {
            return hook(this.originalEvent);
          }
        } : function () {
          if (this.originalEvent) {
            return this.originalEvent[name];
          }
        },
        set: function set(value) {
          Object.defineProperty(this, name, {
            enumerable: true,
            configurable: true,
            writable: true,
            value: value
          });
        }
      });
    },
    fix: function fix(originalEvent) {
      return originalEvent[jQuery.expando] ? originalEvent : new jQuery.Event(originalEvent);
    },
    special: {
      load: {
        noBubble: true
      },
      click: {
        setup: function setup(data) {
          var el = this || data;

          if (rcheckableType.test(el.type) && el.click && nodeName(el, "input")) {
            leverageNative(el, "click", returnTrue);
          }

          return false;
        },
        trigger: function trigger(data) {
          var el = this || data;

          if (rcheckableType.test(el.type) && el.click && nodeName(el, "input")) {
            leverageNative(el, "click");
          }

          return true;
        },
        _default: function _default(event) {
          var target = event.target;
          return rcheckableType.test(target.type) && target.click && nodeName(target, "input") && dataPriv.get(target, "click") || nodeName(target, "a");
        }
      },
      beforeunload: {
        postDispatch: function postDispatch(event) {
          if (event.result !== undefined && event.originalEvent) {
            event.originalEvent.returnValue = event.result;
          }
        }
      }
    }
  };

  function leverageNative(el, type, expectSync) {
    if (!expectSync) {
      if (dataPriv.get(el, type) === undefined) {
        jQuery.event.add(el, type, returnTrue);
      }

      return;
    }

    dataPriv.set(el, type, false);
    jQuery.event.add(el, type, {
      namespace: false,
      handler: function handler(event) {
        var notAsync,
            result,
            saved = dataPriv.get(this, type);

        if (event.isTrigger & 1 && this[type]) {
          if (!saved.length) {
            saved = _slice.call(arguments);
            dataPriv.set(this, type, saved);
            notAsync = expectSync(this, type);
            this[type]();
            result = dataPriv.get(this, type);

            if (saved !== result || notAsync) {
              dataPriv.set(this, type, false);
            } else {
              result = {};
            }

            if (saved !== result) {
              event.stopImmediatePropagation();
              event.preventDefault();
              return result.value;
            }
          } else if ((jQuery.event.special[type] || {}).delegateType) {
            event.stopPropagation();
          }
        } else if (saved.length) {
          dataPriv.set(this, type, {
            value: jQuery.event.trigger(jQuery.extend(saved[0], jQuery.Event.prototype), saved.slice(1), this)
          });
          event.stopImmediatePropagation();
        }
      }
    });
  }

  jQuery.removeEvent = function (elem, type, handle) {
    if (elem.removeEventListener) {
      elem.removeEventListener(type, handle);
    }
  };

  jQuery.Event = function (src, props) {
    if (!(this instanceof jQuery.Event)) {
      return new jQuery.Event(src, props);
    }

    if (src && src.type) {
      this.originalEvent = src;
      this.type = src.type;
      this.isDefaultPrevented = src.defaultPrevented || src.defaultPrevented === undefined && src.returnValue === false ? returnTrue : returnFalse;
      this.target = src.target && src.target.nodeType === 3 ? src.target.parentNode : src.target;
      this.currentTarget = src.currentTarget;
      this.relatedTarget = src.relatedTarget;
    } else {
      this.type = src;
    }

    if (props) {
      jQuery.extend(this, props);
    }

    this.timeStamp = src && src.timeStamp || Date.now();
    this[jQuery.expando] = true;
  };

  jQuery.Event.prototype = {
    constructor: jQuery.Event,
    isDefaultPrevented: returnFalse,
    isPropagationStopped: returnFalse,
    isImmediatePropagationStopped: returnFalse,
    isSimulated: false,
    preventDefault: function preventDefault() {
      var e = this.originalEvent;
      this.isDefaultPrevented = returnTrue;

      if (e && !this.isSimulated) {
        e.preventDefault();
      }
    },
    stopPropagation: function stopPropagation() {
      var e = this.originalEvent;
      this.isPropagationStopped = returnTrue;

      if (e && !this.isSimulated) {
        e.stopPropagation();
      }
    },
    stopImmediatePropagation: function stopImmediatePropagation() {
      var e = this.originalEvent;
      this.isImmediatePropagationStopped = returnTrue;

      if (e && !this.isSimulated) {
        e.stopImmediatePropagation();
      }

      this.stopPropagation();
    }
  };
  jQuery.each({
    altKey: true,
    bubbles: true,
    cancelable: true,
    changedTouches: true,
    ctrlKey: true,
    detail: true,
    eventPhase: true,
    metaKey: true,
    pageX: true,
    pageY: true,
    shiftKey: true,
    view: true,
    "char": true,
    code: true,
    charCode: true,
    key: true,
    keyCode: true,
    button: true,
    buttons: true,
    clientX: true,
    clientY: true,
    offsetX: true,
    offsetY: true,
    pointerId: true,
    pointerType: true,
    screenX: true,
    screenY: true,
    targetTouches: true,
    toElement: true,
    touches: true,
    which: function which(event) {
      var button = event.button;

      if (event.which == null && rkeyEvent.test(event.type)) {
        return event.charCode != null ? event.charCode : event.keyCode;
      }

      if (!event.which && button !== undefined && rmouseEvent.test(event.type)) {
        if (button & 1) {
          return 1;
        }

        if (button & 2) {
          return 3;
        }

        if (button & 4) {
          return 2;
        }

        return 0;
      }

      return event.which;
    }
  }, jQuery.event.addProp);
  jQuery.each({
    focus: "focusin",
    blur: "focusout"
  }, function (type, delegateType) {
    jQuery.event.special[type] = {
      setup: function setup() {
        leverageNative(this, type, expectSync);
        return false;
      },
      trigger: function trigger() {
        leverageNative(this, type);
        return true;
      },
      delegateType: delegateType
    };
  });
  jQuery.each({
    mouseenter: "mouseover",
    mouseleave: "mouseout",
    pointerenter: "pointerover",
    pointerleave: "pointerout"
  }, function (orig, fix) {
    jQuery.event.special[orig] = {
      delegateType: fix,
      bindType: fix,
      handle: function handle(event) {
        var ret,
            target = this,
            related = event.relatedTarget,
            handleObj = event.handleObj;

        if (!related || related !== target && !jQuery.contains(target, related)) {
          event.type = handleObj.origType;
          ret = handleObj.handler.apply(this, arguments);
          event.type = fix;
        }

        return ret;
      }
    };
  });
  jQuery.fn.extend({
    on: function on(types, selector, data, fn) {
      return _on(this, types, selector, data, fn);
    },
    one: function one(types, selector, data, fn) {
      return _on(this, types, selector, data, fn, 1);
    },
    off: function off(types, selector, fn) {
      var handleObj, type;

      if (types && types.preventDefault && types.handleObj) {
        handleObj = types.handleObj;
        jQuery(types.delegateTarget).off(handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType, handleObj.selector, handleObj.handler);
        return this;
      }

      if (_typeof(types) === "object") {
        for (type in types) {
          this.off(type, selector, types[type]);
        }

        return this;
      }

      if (selector === false || typeof selector === "function") {
        fn = selector;
        selector = undefined;
      }

      if (fn === false) {
        fn = returnFalse;
      }

      return this.each(function () {
        jQuery.event.remove(this, types, fn, selector);
      });
    }
  });
  var rnoInnerhtml = /<script|<style|<link/i,
      rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
      rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

  function manipulationTarget(elem, content) {
    if (nodeName(elem, "table") && nodeName(content.nodeType !== 11 ? content : content.firstChild, "tr")) {
      return jQuery(elem).children("tbody")[0] || elem;
    }

    return elem;
  }

  function disableScript(elem) {
    elem.type = (elem.getAttribute("type") !== null) + "/" + elem.type;
    return elem;
  }

  function restoreScript(elem) {
    if ((elem.type || "").slice(0, 5) === "true/") {
      elem.type = elem.type.slice(5);
    } else {
      elem.removeAttribute("type");
    }

    return elem;
  }

  function cloneCopyEvent(src, dest) {
    var i, l, type, pdataOld, udataOld, udataCur, events;

    if (dest.nodeType !== 1) {
      return;
    }

    if (dataPriv.hasData(src)) {
      pdataOld = dataPriv.get(src);
      events = pdataOld.events;

      if (events) {
        dataPriv.remove(dest, "handle events");

        for (type in events) {
          for (i = 0, l = events[type].length; i < l; i++) {
            jQuery.event.add(dest, type, events[type][i]);
          }
        }
      }
    }

    if (dataUser.hasData(src)) {
      udataOld = dataUser.access(src);
      udataCur = jQuery.extend({}, udataOld);
      dataUser.set(dest, udataCur);
    }
  }

  function fixInput(src, dest) {
    var nodeName = dest.nodeName.toLowerCase();

    if (nodeName === "input" && rcheckableType.test(src.type)) {
      dest.checked = src.checked;
    } else if (nodeName === "input" || nodeName === "textarea") {
      dest.defaultValue = src.defaultValue;
    }
  }

  function domManip(collection, args, callback, ignored) {
    args = flat(args);
    var fragment,
        first,
        scripts,
        hasScripts,
        node,
        doc,
        i = 0,
        l = collection.length,
        iNoClone = l - 1,
        value = args[0],
        valueIsFunction = isFunction(value);

    if (valueIsFunction || l > 1 && typeof value === "string" && !support.checkClone && rchecked.test(value)) {
      return collection.each(function (index) {
        var self = collection.eq(index);

        if (valueIsFunction) {
          args[0] = value.call(this, index, self.html());
        }

        domManip(self, args, callback, ignored);
      });
    }

    if (l) {
      fragment = buildFragment(args, collection[0].ownerDocument, false, collection, ignored);
      first = fragment.firstChild;

      if (fragment.childNodes.length === 1) {
        fragment = first;
      }

      if (first || ignored) {
        scripts = jQuery.map(getAll(fragment, "script"), disableScript);
        hasScripts = scripts.length;

        for (; i < l; i++) {
          node = fragment;

          if (i !== iNoClone) {
            node = jQuery.clone(node, true, true);

            if (hasScripts) {
              jQuery.merge(scripts, getAll(node, "script"));
            }
          }

          callback.call(collection[i], node, i);
        }

        if (hasScripts) {
          doc = scripts[scripts.length - 1].ownerDocument;
          jQuery.map(scripts, restoreScript);

          for (i = 0; i < hasScripts; i++) {
            node = scripts[i];

            if (rscriptType.test(node.type || "") && !dataPriv.access(node, "globalEval") && jQuery.contains(doc, node)) {
              if (node.src && (node.type || "").toLowerCase() !== "module") {
                if (jQuery._evalUrl && !node.noModule) {
                  jQuery._evalUrl(node.src, {
                    nonce: node.nonce || node.getAttribute("nonce")
                  }, doc);
                }
              } else {
                DOMEval(node.textContent.replace(rcleanScript, ""), node, doc);
              }
            }
          }
        }
      }
    }

    return collection;
  }

  function _remove(elem, selector, keepData) {
    var node,
        nodes = selector ? jQuery.filter(selector, elem) : elem,
        i = 0;

    for (; (node = nodes[i]) != null; i++) {
      if (!keepData && node.nodeType === 1) {
        jQuery.cleanData(getAll(node));
      }

      if (node.parentNode) {
        if (keepData && isAttached(node)) {
          setGlobalEval(getAll(node, "script"));
        }

        node.parentNode.removeChild(node);
      }
    }

    return elem;
  }

  jQuery.extend({
    htmlPrefilter: function htmlPrefilter(html) {
      return html;
    },
    clone: function clone(elem, dataAndEvents, deepDataAndEvents) {
      var i,
          l,
          srcElements,
          destElements,
          clone = elem.cloneNode(true),
          inPage = isAttached(elem);

      if (!support.noCloneChecked && (elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem)) {
        destElements = getAll(clone);
        srcElements = getAll(elem);

        for (i = 0, l = srcElements.length; i < l; i++) {
          fixInput(srcElements[i], destElements[i]);
        }
      }

      if (dataAndEvents) {
        if (deepDataAndEvents) {
          srcElements = srcElements || getAll(elem);
          destElements = destElements || getAll(clone);

          for (i = 0, l = srcElements.length; i < l; i++) {
            cloneCopyEvent(srcElements[i], destElements[i]);
          }
        } else {
          cloneCopyEvent(elem, clone);
        }
      }

      destElements = getAll(clone, "script");

      if (destElements.length > 0) {
        setGlobalEval(destElements, !inPage && getAll(elem, "script"));
      }

      return clone;
    },
    cleanData: function cleanData(elems) {
      var data,
          elem,
          type,
          special = jQuery.event.special,
          i = 0;

      for (; (elem = elems[i]) !== undefined; i++) {
        if (acceptData(elem)) {
          if (data = elem[dataPriv.expando]) {
            if (data.events) {
              for (type in data.events) {
                if (special[type]) {
                  jQuery.event.remove(elem, type);
                } else {
                  jQuery.removeEvent(elem, type, data.handle);
                }
              }
            }

            elem[dataPriv.expando] = undefined;
          }

          if (elem[dataUser.expando]) {
            elem[dataUser.expando] = undefined;
          }
        }
      }
    }
  });
  jQuery.fn.extend({
    detach: function detach(selector) {
      return _remove(this, selector, true);
    },
    remove: function remove(selector) {
      return _remove(this, selector);
    },
    text: function text(value) {
      return access(this, function (value) {
        return value === undefined ? jQuery.text(this) : this.empty().each(function () {
          if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
            this.textContent = value;
          }
        });
      }, null, value, arguments.length);
    },
    append: function append() {
      return domManip(this, arguments, function (elem) {
        if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
          var target = manipulationTarget(this, elem);
          target.appendChild(elem);
        }
      });
    },
    prepend: function prepend() {
      return domManip(this, arguments, function (elem) {
        if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
          var target = manipulationTarget(this, elem);
          target.insertBefore(elem, target.firstChild);
        }
      });
    },
    before: function before() {
      return domManip(this, arguments, function (elem) {
        if (this.parentNode) {
          this.parentNode.insertBefore(elem, this);
        }
      });
    },
    after: function after() {
      return domManip(this, arguments, function (elem) {
        if (this.parentNode) {
          this.parentNode.insertBefore(elem, this.nextSibling);
        }
      });
    },
    empty: function empty() {
      var elem,
          i = 0;

      for (; (elem = this[i]) != null; i++) {
        if (elem.nodeType === 1) {
          jQuery.cleanData(getAll(elem, false));
          elem.textContent = "";
        }
      }

      return this;
    },
    clone: function clone(dataAndEvents, deepDataAndEvents) {
      dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
      deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;
      return this.map(function () {
        return jQuery.clone(this, dataAndEvents, deepDataAndEvents);
      });
    },
    html: function html(value) {
      return access(this, function (value) {
        var elem = this[0] || {},
            i = 0,
            l = this.length;

        if (value === undefined && elem.nodeType === 1) {
          return elem.innerHTML;
        }

        if (typeof value === "string" && !rnoInnerhtml.test(value) && !wrapMap[(rtagName.exec(value) || ["", ""])[1].toLowerCase()]) {
          value = jQuery.htmlPrefilter(value);

          try {
            for (; i < l; i++) {
              elem = this[i] || {};

              if (elem.nodeType === 1) {
                jQuery.cleanData(getAll(elem, false));
                elem.innerHTML = value;
              }
            }

            elem = 0;
          } catch (e) {}
        }

        if (elem) {
          this.empty().append(value);
        }
      }, null, value, arguments.length);
    },
    replaceWith: function replaceWith() {
      var ignored = [];
      return domManip(this, arguments, function (elem) {
        var parent = this.parentNode;

        if (jQuery.inArray(this, ignored) < 0) {
          jQuery.cleanData(getAll(this));

          if (parent) {
            parent.replaceChild(elem, this);
          }
        }
      }, ignored);
    }
  });
  jQuery.each({
    appendTo: "append",
    prependTo: "prepend",
    insertBefore: "before",
    insertAfter: "after",
    replaceAll: "replaceWith"
  }, function (name, original) {
    jQuery.fn[name] = function (selector) {
      var elems,
          ret = [],
          insert = jQuery(selector),
          last = insert.length - 1,
          i = 0;

      for (; i <= last; i++) {
        elems = i === last ? this : this.clone(true);
        jQuery(insert[i])[original](elems);
        push.apply(ret, elems.get());
      }

      return this.pushStack(ret);
    };
  });
  var rnumnonpx = new RegExp("^(" + pnum + ")(?!px)[a-z%]+$", "i");

  var getStyles = function getStyles(elem) {
    var view = elem.ownerDocument.defaultView;

    if (!view || !view.opener) {
      view = window;
    }

    return view.getComputedStyle(elem);
  };

  var swap = function swap(elem, options, callback) {
    var ret,
        name,
        old = {};

    for (name in options) {
      old[name] = elem.style[name];
      elem.style[name] = options[name];
    }

    ret = callback.call(elem);

    for (name in options) {
      elem.style[name] = old[name];
    }

    return ret;
  };

  var rboxStyle = new RegExp(cssExpand.join("|"), "i");

  (function () {
    function computeStyleTests() {
      if (!div) {
        return;
      }

      container.style.cssText = "position:absolute;left:-11111px;width:60px;" + "margin-top:1px;padding:0;border:0";
      div.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;" + "margin:auto;border:1px;padding:1px;" + "width:60%;top:1%";
      documentElement.appendChild(container).appendChild(div);
      var divStyle = window.getComputedStyle(div);
      pixelPositionVal = divStyle.top !== "1%";
      reliableMarginLeftVal = roundPixelMeasures(divStyle.marginLeft) === 12;
      div.style.right = "60%";
      pixelBoxStylesVal = roundPixelMeasures(divStyle.right) === 36;
      boxSizingReliableVal = roundPixelMeasures(divStyle.width) === 36;
      div.style.position = "absolute";
      scrollboxSizeVal = roundPixelMeasures(div.offsetWidth / 3) === 12;
      documentElement.removeChild(container);
      div = null;
    }

    function roundPixelMeasures(measure) {
      return Math.round(parseFloat(measure));
    }

    var pixelPositionVal,
        boxSizingReliableVal,
        scrollboxSizeVal,
        pixelBoxStylesVal,
        reliableTrDimensionsVal,
        reliableMarginLeftVal,
        container = document.createElement("div"),
        div = document.createElement("div");

    if (!div.style) {
      return;
    }

    div.style.backgroundClip = "content-box";
    div.cloneNode(true).style.backgroundClip = "";
    support.clearCloneStyle = div.style.backgroundClip === "content-box";
    jQuery.extend(support, {
      boxSizingReliable: function boxSizingReliable() {
        computeStyleTests();
        return boxSizingReliableVal;
      },
      pixelBoxStyles: function pixelBoxStyles() {
        computeStyleTests();
        return pixelBoxStylesVal;
      },
      pixelPosition: function pixelPosition() {
        computeStyleTests();
        return pixelPositionVal;
      },
      reliableMarginLeft: function reliableMarginLeft() {
        computeStyleTests();
        return reliableMarginLeftVal;
      },
      scrollboxSize: function scrollboxSize() {
        computeStyleTests();
        return scrollboxSizeVal;
      },
      reliableTrDimensions: function reliableTrDimensions() {
        var table, tr, trChild, trStyle;

        if (reliableTrDimensionsVal == null) {
          table = document.createElement("table");
          tr = document.createElement("tr");
          trChild = document.createElement("div");
          table.style.cssText = "position:absolute;left:-11111px";
          tr.style.height = "1px";
          trChild.style.height = "9px";
          documentElement.appendChild(table).appendChild(tr).appendChild(trChild);
          trStyle = window.getComputedStyle(tr);
          reliableTrDimensionsVal = parseInt(trStyle.height) > 3;
          documentElement.removeChild(table);
        }

        return reliableTrDimensionsVal;
      }
    });
  })();

  function curCSS(elem, name, computed) {
    var width,
        minWidth,
        maxWidth,
        ret,
        style = elem.style;
    computed = computed || getStyles(elem);

    if (computed) {
      ret = computed.getPropertyValue(name) || computed[name];

      if (ret === "" && !isAttached(elem)) {
        ret = jQuery.style(elem, name);
      }

      if (!support.pixelBoxStyles() && rnumnonpx.test(ret) && rboxStyle.test(name)) {
        width = style.width;
        minWidth = style.minWidth;
        maxWidth = style.maxWidth;
        style.minWidth = style.maxWidth = style.width = ret;
        ret = computed.width;
        style.width = width;
        style.minWidth = minWidth;
        style.maxWidth = maxWidth;
      }
    }

    return ret !== undefined ? ret + "" : ret;
  }

  function addGetHookIf(conditionFn, hookFn) {
    return {
      get: function get() {
        if (conditionFn()) {
          delete this.get;
          return;
        }

        return (this.get = hookFn).apply(this, arguments);
      }
    };
  }

  var cssPrefixes = ["Webkit", "Moz", "ms"],
      emptyStyle = document.createElement("div").style,
      vendorProps = {};

  function vendorPropName(name) {
    var capName = name[0].toUpperCase() + name.slice(1),
        i = cssPrefixes.length;

    while (i--) {
      name = cssPrefixes[i] + capName;

      if (name in emptyStyle) {
        return name;
      }
    }
  }

  function finalPropName(name) {
    var _final = jQuery.cssProps[name] || vendorProps[name];

    if (_final) {
      return _final;
    }

    if (name in emptyStyle) {
      return name;
    }

    return vendorProps[name] = vendorPropName(name) || name;
  }

  var rdisplayswap = /^(none|table(?!-c[ea]).+)/,
      rcustomProp = /^--/,
      cssShow = {
    position: "absolute",
    visibility: "hidden",
    display: "block"
  },
      cssNormalTransform = {
    letterSpacing: "0",
    fontWeight: "400"
  };

  function setPositiveNumber(_elem, value, subtract) {
    var matches = rcssNum.exec(value);
    return matches ? Math.max(0, matches[2] - (subtract || 0)) + (matches[3] || "px") : value;
  }

  function boxModelAdjustment(elem, dimension, box, isBorderBox, styles, computedVal) {
    var i = dimension === "width" ? 1 : 0,
        extra = 0,
        delta = 0;

    if (box === (isBorderBox ? "border" : "content")) {
      return 0;
    }

    for (; i < 4; i += 2) {
      if (box === "margin") {
        delta += jQuery.css(elem, box + cssExpand[i], true, styles);
      }

      if (!isBorderBox) {
        delta += jQuery.css(elem, "padding" + cssExpand[i], true, styles);

        if (box !== "padding") {
          delta += jQuery.css(elem, "border" + cssExpand[i] + "Width", true, styles);
        } else {
          extra += jQuery.css(elem, "border" + cssExpand[i] + "Width", true, styles);
        }
      } else {
        if (box === "content") {
          delta -= jQuery.css(elem, "padding" + cssExpand[i], true, styles);
        }

        if (box !== "margin") {
          delta -= jQuery.css(elem, "border" + cssExpand[i] + "Width", true, styles);
        }
      }
    }

    if (!isBorderBox && computedVal >= 0) {
      delta += Math.max(0, Math.ceil(elem["offset" + dimension[0].toUpperCase() + dimension.slice(1)] - computedVal - delta - extra - 0.5)) || 0;
    }

    return delta;
  }

  function getWidthOrHeight(elem, dimension, extra) {
    var styles = getStyles(elem),
        boxSizingNeeded = !support.boxSizingReliable() || extra,
        isBorderBox = boxSizingNeeded && jQuery.css(elem, "boxSizing", false, styles) === "border-box",
        valueIsBorderBox = isBorderBox,
        val = curCSS(elem, dimension, styles),
        offsetProp = "offset" + dimension[0].toUpperCase() + dimension.slice(1);

    if (rnumnonpx.test(val)) {
      if (!extra) {
        return val;
      }

      val = "auto";
    }

    if ((!support.boxSizingReliable() && isBorderBox || !support.reliableTrDimensions() && nodeName(elem, "tr") || val === "auto" || !parseFloat(val) && jQuery.css(elem, "display", false, styles) === "inline") && elem.getClientRects().length) {
      isBorderBox = jQuery.css(elem, "boxSizing", false, styles) === "border-box";
      valueIsBorderBox = offsetProp in elem;

      if (valueIsBorderBox) {
        val = elem[offsetProp];
      }
    }

    val = parseFloat(val) || 0;
    return val + boxModelAdjustment(elem, dimension, extra || (isBorderBox ? "border" : "content"), valueIsBorderBox, styles, val) + "px";
  }

  jQuery.extend({
    cssHooks: {
      opacity: {
        get: function get(elem, computed) {
          if (computed) {
            var ret = curCSS(elem, "opacity");
            return ret === "" ? "1" : ret;
          }
        }
      }
    },
    cssNumber: {
      "animationIterationCount": true,
      "columnCount": true,
      "fillOpacity": true,
      "flexGrow": true,
      "flexShrink": true,
      "fontWeight": true,
      "gridArea": true,
      "gridColumn": true,
      "gridColumnEnd": true,
      "gridColumnStart": true,
      "gridRow": true,
      "gridRowEnd": true,
      "gridRowStart": true,
      "lineHeight": true,
      "opacity": true,
      "order": true,
      "orphans": true,
      "widows": true,
      "zIndex": true,
      "zoom": true
    },
    cssProps: {},
    style: function style(elem, name, value, extra) {
      if (!elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style) {
        return;
      }

      var ret,
          type,
          hooks,
          origName = camelCase(name),
          isCustomProp = rcustomProp.test(name),
          style = elem.style;

      if (!isCustomProp) {
        name = finalPropName(origName);
      }

      hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName];

      if (value !== undefined) {
        type = _typeof(value);

        if (type === "string" && (ret = rcssNum.exec(value)) && ret[1]) {
          value = adjustCSS(elem, name, ret);
          type = "number";
        }

        if (value == null || value !== value) {
          return;
        }

        if (type === "number" && !isCustomProp) {
          value += ret && ret[3] || (jQuery.cssNumber[origName] ? "" : "px");
        }

        if (!support.clearCloneStyle && value === "" && name.indexOf("background") === 0) {
          style[name] = "inherit";
        }

        if (!hooks || !("set" in hooks) || (value = hooks.set(elem, value, extra)) !== undefined) {
          if (isCustomProp) {
            style.setProperty(name, value);
          } else {
            style[name] = value;
          }
        }
      } else {
        if (hooks && "get" in hooks && (ret = hooks.get(elem, false, extra)) !== undefined) {
          return ret;
        }

        return style[name];
      }
    },
    css: function css(elem, name, extra, styles) {
      var val,
          num,
          hooks,
          origName = camelCase(name),
          isCustomProp = rcustomProp.test(name);

      if (!isCustomProp) {
        name = finalPropName(origName);
      }

      hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName];

      if (hooks && "get" in hooks) {
        val = hooks.get(elem, true, extra);
      }

      if (val === undefined) {
        val = curCSS(elem, name, styles);
      }

      if (val === "normal" && name in cssNormalTransform) {
        val = cssNormalTransform[name];
      }

      if (extra === "" || extra) {
        num = parseFloat(val);
        return extra === true || isFinite(num) ? num || 0 : val;
      }

      return val;
    }
  });
  jQuery.each(["height", "width"], function (_i, dimension) {
    jQuery.cssHooks[dimension] = {
      get: function get(elem, computed, extra) {
        if (computed) {
          return rdisplayswap.test(jQuery.css(elem, "display")) && (!elem.getClientRects().length || !elem.getBoundingClientRect().width) ? swap(elem, cssShow, function () {
            return getWidthOrHeight(elem, dimension, extra);
          }) : getWidthOrHeight(elem, dimension, extra);
        }
      },
      set: function set(elem, value, extra) {
        var matches,
            styles = getStyles(elem),
            scrollboxSizeBuggy = !support.scrollboxSize() && styles.position === "absolute",
            boxSizingNeeded = scrollboxSizeBuggy || extra,
            isBorderBox = boxSizingNeeded && jQuery.css(elem, "boxSizing", false, styles) === "border-box",
            subtract = extra ? boxModelAdjustment(elem, dimension, extra, isBorderBox, styles) : 0;

        if (isBorderBox && scrollboxSizeBuggy) {
          subtract -= Math.ceil(elem["offset" + dimension[0].toUpperCase() + dimension.slice(1)] - parseFloat(styles[dimension]) - boxModelAdjustment(elem, dimension, "border", false, styles) - 0.5);
        }

        if (subtract && (matches = rcssNum.exec(value)) && (matches[3] || "px") !== "px") {
          elem.style[dimension] = value;
          value = jQuery.css(elem, dimension);
        }

        return setPositiveNumber(elem, value, subtract);
      }
    };
  });
  jQuery.cssHooks.marginLeft = addGetHookIf(support.reliableMarginLeft, function (elem, computed) {
    if (computed) {
      return (parseFloat(curCSS(elem, "marginLeft")) || elem.getBoundingClientRect().left - swap(elem, {
        marginLeft: 0
      }, function () {
        return elem.getBoundingClientRect().left;
      })) + "px";
    }
  });
  jQuery.each({
    margin: "",
    padding: "",
    border: "Width"
  }, function (prefix, suffix) {
    jQuery.cssHooks[prefix + suffix] = {
      expand: function expand(value) {
        var i = 0,
            expanded = {},
            parts = typeof value === "string" ? value.split(" ") : [value];

        for (; i < 4; i++) {
          expanded[prefix + cssExpand[i] + suffix] = parts[i] || parts[i - 2] || parts[0];
        }

        return expanded;
      }
    };

    if (prefix !== "margin") {
      jQuery.cssHooks[prefix + suffix].set = setPositiveNumber;
    }
  });
  jQuery.fn.extend({
    css: function css(name, value) {
      return access(this, function (elem, name, value) {
        var styles,
            len,
            map = {},
            i = 0;

        if (Array.isArray(name)) {
          styles = getStyles(elem);
          len = name.length;

          for (; i < len; i++) {
            map[name[i]] = jQuery.css(elem, name[i], false, styles);
          }

          return map;
        }

        return value !== undefined ? jQuery.style(elem, name, value) : jQuery.css(elem, name);
      }, name, value, arguments.length > 1);
    }
  });

  function Tween(elem, options, prop, end, easing) {
    return new Tween.prototype.init(elem, options, prop, end, easing);
  }

  jQuery.Tween = Tween;
  Tween.prototype = {
    constructor: Tween,
    init: function init(elem, options, prop, end, easing, unit) {
      this.elem = elem;
      this.prop = prop;
      this.easing = easing || jQuery.easing._default;
      this.options = options;
      this.start = this.now = this.cur();
      this.end = end;
      this.unit = unit || (jQuery.cssNumber[prop] ? "" : "px");
    },
    cur: function cur() {
      var hooks = Tween.propHooks[this.prop];
      return hooks && hooks.get ? hooks.get(this) : Tween.propHooks._default.get(this);
    },
    run: function run(percent) {
      var eased,
          hooks = Tween.propHooks[this.prop];

      if (this.options.duration) {
        this.pos = eased = jQuery.easing[this.easing](percent, this.options.duration * percent, 0, 1, this.options.duration);
      } else {
        this.pos = eased = percent;
      }

      this.now = (this.end - this.start) * eased + this.start;

      if (this.options.step) {
        this.options.step.call(this.elem, this.now, this);
      }

      if (hooks && hooks.set) {
        hooks.set(this);
      } else {
        Tween.propHooks._default.set(this);
      }

      return this;
    }
  };
  Tween.prototype.init.prototype = Tween.prototype;
  Tween.propHooks = {
    _default: {
      get: function get(tween) {
        var result;

        if (tween.elem.nodeType !== 1 || tween.elem[tween.prop] != null && tween.elem.style[tween.prop] == null) {
          return tween.elem[tween.prop];
        }

        result = jQuery.css(tween.elem, tween.prop, "");
        return !result || result === "auto" ? 0 : result;
      },
      set: function set(tween) {
        if (jQuery.fx.step[tween.prop]) {
          jQuery.fx.step[tween.prop](tween);
        } else if (tween.elem.nodeType === 1 && (jQuery.cssHooks[tween.prop] || tween.elem.style[finalPropName(tween.prop)] != null)) {
          jQuery.style(tween.elem, tween.prop, tween.now + tween.unit);
        } else {
          tween.elem[tween.prop] = tween.now;
        }
      }
    }
  };
  Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
    set: function set(tween) {
      if (tween.elem.nodeType && tween.elem.parentNode) {
        tween.elem[tween.prop] = tween.now;
      }
    }
  };
  jQuery.easing = {
    linear: function linear(p) {
      return p;
    },
    swing: function swing(p) {
      return 0.5 - Math.cos(p * Math.PI) / 2;
    },
    _default: "swing"
  };
  jQuery.fx = Tween.prototype.init;
  jQuery.fx.step = {};
  var fxNow,
      inProgress,
      rfxtypes = /^(?:toggle|show|hide)$/,
      rrun = /queueHooks$/;

  function schedule() {
    if (inProgress) {
      if (document.hidden === false && window.requestAnimationFrame) {
        window.requestAnimationFrame(schedule);
      } else {
        window.setTimeout(schedule, jQuery.fx.interval);
      }

      jQuery.fx.tick();
    }
  }

  function createFxNow() {
    window.setTimeout(function () {
      fxNow = undefined;
    });
    return fxNow = Date.now();
  }

  function genFx(type, includeWidth) {
    var which,
        i = 0,
        attrs = {
      height: type
    };
    includeWidth = includeWidth ? 1 : 0;

    for (; i < 4; i += 2 - includeWidth) {
      which = cssExpand[i];
      attrs["margin" + which] = attrs["padding" + which] = type;
    }

    if (includeWidth) {
      attrs.opacity = attrs.width = type;
    }

    return attrs;
  }

  function createTween(value, prop, animation) {
    var tween,
        collection = (Animation.tweeners[prop] || []).concat(Animation.tweeners["*"]),
        index = 0,
        length = collection.length;

    for (; index < length; index++) {
      if (tween = collection[index].call(animation, prop, value)) {
        return tween;
      }
    }
  }

  function defaultPrefilter(elem, props, opts) {
    var prop,
        value,
        toggle,
        hooks,
        oldfire,
        propTween,
        restoreDisplay,
        display,
        isBox = "width" in props || "height" in props,
        anim = this,
        orig = {},
        style = elem.style,
        hidden = elem.nodeType && isHiddenWithinTree(elem),
        dataShow = dataPriv.get(elem, "fxshow");

    if (!opts.queue) {
      hooks = jQuery._queueHooks(elem, "fx");

      if (hooks.unqueued == null) {
        hooks.unqueued = 0;
        oldfire = hooks.empty.fire;

        hooks.empty.fire = function () {
          if (!hooks.unqueued) {
            oldfire();
          }
        };
      }

      hooks.unqueued++;
      anim.always(function () {
        anim.always(function () {
          hooks.unqueued--;

          if (!jQuery.queue(elem, "fx").length) {
            hooks.empty.fire();
          }
        });
      });
    }

    for (prop in props) {
      value = props[prop];

      if (rfxtypes.test(value)) {
        delete props[prop];
        toggle = toggle || value === "toggle";

        if (value === (hidden ? "hide" : "show")) {
          if (value === "show" && dataShow && dataShow[prop] !== undefined) {
            hidden = true;
          } else {
            continue;
          }
        }

        orig[prop] = dataShow && dataShow[prop] || jQuery.style(elem, prop);
      }
    }

    propTween = !jQuery.isEmptyObject(props);

    if (!propTween && jQuery.isEmptyObject(orig)) {
      return;
    }

    if (isBox && elem.nodeType === 1) {
      opts.overflow = [style.overflow, style.overflowX, style.overflowY];
      restoreDisplay = dataShow && dataShow.display;

      if (restoreDisplay == null) {
        restoreDisplay = dataPriv.get(elem, "display");
      }

      display = jQuery.css(elem, "display");

      if (display === "none") {
        if (restoreDisplay) {
          display = restoreDisplay;
        } else {
          showHide([elem], true);
          restoreDisplay = elem.style.display || restoreDisplay;
          display = jQuery.css(elem, "display");
          showHide([elem]);
        }
      }

      if (display === "inline" || display === "inline-block" && restoreDisplay != null) {
        if (jQuery.css(elem, "float") === "none") {
          if (!propTween) {
            anim.done(function () {
              style.display = restoreDisplay;
            });

            if (restoreDisplay == null) {
              display = style.display;
              restoreDisplay = display === "none" ? "" : display;
            }
          }

          style.display = "inline-block";
        }
      }
    }

    if (opts.overflow) {
      style.overflow = "hidden";
      anim.always(function () {
        style.overflow = opts.overflow[0];
        style.overflowX = opts.overflow[1];
        style.overflowY = opts.overflow[2];
      });
    }

    propTween = false;

    for (prop in orig) {
      if (!propTween) {
        if (dataShow) {
          if ("hidden" in dataShow) {
            hidden = dataShow.hidden;
          }
        } else {
          dataShow = dataPriv.access(elem, "fxshow", {
            display: restoreDisplay
          });
        }

        if (toggle) {
          dataShow.hidden = !hidden;
        }

        if (hidden) {
          showHide([elem], true);
        }

        anim.done(function () {
          if (!hidden) {
            showHide([elem]);
          }

          dataPriv.remove(elem, "fxshow");

          for (prop in orig) {
            jQuery.style(elem, prop, orig[prop]);
          }
        });
      }

      propTween = createTween(hidden ? dataShow[prop] : 0, prop, anim);

      if (!(prop in dataShow)) {
        dataShow[prop] = propTween.start;

        if (hidden) {
          propTween.end = propTween.start;
          propTween.start = 0;
        }
      }
    }
  }

  function propFilter(props, specialEasing) {
    var index, name, easing, value, hooks;

    for (index in props) {
      name = camelCase(index);
      easing = specialEasing[name];
      value = props[index];

      if (Array.isArray(value)) {
        easing = value[1];
        value = props[index] = value[0];
      }

      if (index !== name) {
        props[name] = value;
        delete props[index];
      }

      hooks = jQuery.cssHooks[name];

      if (hooks && "expand" in hooks) {
        value = hooks.expand(value);
        delete props[name];

        for (index in value) {
          if (!(index in props)) {
            props[index] = value[index];
            specialEasing[index] = easing;
          }
        }
      } else {
        specialEasing[name] = easing;
      }
    }
  }

  function Animation(elem, properties, options) {
    var result,
        stopped,
        index = 0,
        length = Animation.prefilters.length,
        deferred = jQuery.Deferred().always(function () {
      delete tick.elem;
    }),
        tick = function tick() {
      if (stopped) {
        return false;
      }

      var currentTime = fxNow || createFxNow(),
          remaining = Math.max(0, animation.startTime + animation.duration - currentTime),
          temp = remaining / animation.duration || 0,
          percent = 1 - temp,
          index = 0,
          length = animation.tweens.length;

      for (; index < length; index++) {
        animation.tweens[index].run(percent);
      }

      deferred.notifyWith(elem, [animation, percent, remaining]);

      if (percent < 1 && length) {
        return remaining;
      }

      if (!length) {
        deferred.notifyWith(elem, [animation, 1, 0]);
      }

      deferred.resolveWith(elem, [animation]);
      return false;
    },
        animation = deferred.promise({
      elem: elem,
      props: jQuery.extend({}, properties),
      opts: jQuery.extend(true, {
        specialEasing: {},
        easing: jQuery.easing._default
      }, options),
      originalProperties: properties,
      originalOptions: options,
      startTime: fxNow || createFxNow(),
      duration: options.duration,
      tweens: [],
      createTween: function createTween(prop, end) {
        var tween = jQuery.Tween(elem, animation.opts, prop, end, animation.opts.specialEasing[prop] || animation.opts.easing);
        animation.tweens.push(tween);
        return tween;
      },
      stop: function stop(gotoEnd) {
        var index = 0,
            length = gotoEnd ? animation.tweens.length : 0;

        if (stopped) {
          return this;
        }

        stopped = true;

        for (; index < length; index++) {
          animation.tweens[index].run(1);
        }

        if (gotoEnd) {
          deferred.notifyWith(elem, [animation, 1, 0]);
          deferred.resolveWith(elem, [animation, gotoEnd]);
        } else {
          deferred.rejectWith(elem, [animation, gotoEnd]);
        }

        return this;
      }
    }),
        props = animation.props;

    propFilter(props, animation.opts.specialEasing);

    for (; index < length; index++) {
      result = Animation.prefilters[index].call(animation, elem, props, animation.opts);

      if (result) {
        if (isFunction(result.stop)) {
          jQuery._queueHooks(animation.elem, animation.opts.queue).stop = result.stop.bind(result);
        }

        return result;
      }
    }

    jQuery.map(props, createTween, animation);

    if (isFunction(animation.opts.start)) {
      animation.opts.start.call(elem, animation);
    }

    animation.progress(animation.opts.progress).done(animation.opts.done, animation.opts.complete).fail(animation.opts.fail).always(animation.opts.always);
    jQuery.fx.timer(jQuery.extend(tick, {
      elem: elem,
      anim: animation,
      queue: animation.opts.queue
    }));
    return animation;
  }

  jQuery.Animation = jQuery.extend(Animation, {
    tweeners: {
      "*": [function (prop, value) {
        var tween = this.createTween(prop, value);
        adjustCSS(tween.elem, prop, rcssNum.exec(value), tween);
        return tween;
      }]
    },
    tweener: function tweener(props, callback) {
      if (isFunction(props)) {
        callback = props;
        props = ["*"];
      } else {
        props = props.match(rnothtmlwhite);
      }

      var prop,
          index = 0,
          length = props.length;

      for (; index < length; index++) {
        prop = props[index];
        Animation.tweeners[prop] = Animation.tweeners[prop] || [];
        Animation.tweeners[prop].unshift(callback);
      }
    },
    prefilters: [defaultPrefilter],
    prefilter: function prefilter(callback, prepend) {
      if (prepend) {
        Animation.prefilters.unshift(callback);
      } else {
        Animation.prefilters.push(callback);
      }
    }
  });

  jQuery.speed = function (speed, easing, fn) {
    var opt = speed && _typeof(speed) === "object" ? jQuery.extend({}, speed) : {
      complete: fn || !fn && easing || isFunction(speed) && speed,
      duration: speed,
      easing: fn && easing || easing && !isFunction(easing) && easing
    };

    if (jQuery.fx.off) {
      opt.duration = 0;
    } else {
      if (typeof opt.duration !== "number") {
        if (opt.duration in jQuery.fx.speeds) {
          opt.duration = jQuery.fx.speeds[opt.duration];
        } else {
          opt.duration = jQuery.fx.speeds._default;
        }
      }
    }

    if (opt.queue == null || opt.queue === true) {
      opt.queue = "fx";
    }

    opt.old = opt.complete;

    opt.complete = function () {
      if (isFunction(opt.old)) {
        opt.old.call(this);
      }

      if (opt.queue) {
        jQuery.dequeue(this, opt.queue);
      }
    };

    return opt;
  };

  jQuery.fn.extend({
    fadeTo: function fadeTo(speed, to, easing, callback) {
      return this.filter(isHiddenWithinTree).css("opacity", 0).show().end().animate({
        opacity: to
      }, speed, easing, callback);
    },
    animate: function animate(prop, speed, easing, callback) {
      var empty = jQuery.isEmptyObject(prop),
          optall = jQuery.speed(speed, easing, callback),
          doAnimation = function doAnimation() {
        var anim = Animation(this, jQuery.extend({}, prop), optall);

        if (empty || dataPriv.get(this, "finish")) {
          anim.stop(true);
        }
      };

      doAnimation.finish = doAnimation;
      return empty || optall.queue === false ? this.each(doAnimation) : this.queue(optall.queue, doAnimation);
    },
    stop: function stop(type, clearQueue, gotoEnd) {
      var stopQueue = function stopQueue(hooks) {
        var stop = hooks.stop;
        delete hooks.stop;
        stop(gotoEnd);
      };

      if (typeof type !== "string") {
        gotoEnd = clearQueue;
        clearQueue = type;
        type = undefined;
      }

      if (clearQueue) {
        this.queue(type || "fx", []);
      }

      return this.each(function () {
        var dequeue = true,
            index = type != null && type + "queueHooks",
            timers = jQuery.timers,
            data = dataPriv.get(this);

        if (index) {
          if (data[index] && data[index].stop) {
            stopQueue(data[index]);
          }
        } else {
          for (index in data) {
            if (data[index] && data[index].stop && rrun.test(index)) {
              stopQueue(data[index]);
            }
          }
        }

        for (index = timers.length; index--;) {
          if (timers[index].elem === this && (type == null || timers[index].queue === type)) {
            timers[index].anim.stop(gotoEnd);
            dequeue = false;
            timers.splice(index, 1);
          }
        }

        if (dequeue || !gotoEnd) {
          jQuery.dequeue(this, type);
        }
      });
    },
    finish: function finish(type) {
      if (type !== false) {
        type = type || "fx";
      }

      return this.each(function () {
        var index,
            data = dataPriv.get(this),
            queue = data[type + "queue"],
            hooks = data[type + "queueHooks"],
            timers = jQuery.timers,
            length = queue ? queue.length : 0;
        data.finish = true;
        jQuery.queue(this, type, []);

        if (hooks && hooks.stop) {
          hooks.stop.call(this, true);
        }

        for (index = timers.length; index--;) {
          if (timers[index].elem === this && timers[index].queue === type) {
            timers[index].anim.stop(true);
            timers.splice(index, 1);
          }
        }

        for (index = 0; index < length; index++) {
          if (queue[index] && queue[index].finish) {
            queue[index].finish.call(this);
          }
        }

        delete data.finish;
      });
    }
  });
  jQuery.each(["toggle", "show", "hide"], function (_i, name) {
    var cssFn = jQuery.fn[name];

    jQuery.fn[name] = function (speed, easing, callback) {
      return speed == null || typeof speed === "boolean" ? cssFn.apply(this, arguments) : this.animate(genFx(name, true), speed, easing, callback);
    };
  });
  jQuery.each({
    slideDown: genFx("show"),
    slideUp: genFx("hide"),
    slideToggle: genFx("toggle"),
    fadeIn: {
      opacity: "show"
    },
    fadeOut: {
      opacity: "hide"
    },
    fadeToggle: {
      opacity: "toggle"
    }
  }, function (name, props) {
    jQuery.fn[name] = function (speed, easing, callback) {
      return this.animate(props, speed, easing, callback);
    };
  });
  jQuery.timers = [];

  jQuery.fx.tick = function () {
    var timer,
        i = 0,
        timers = jQuery.timers;
    fxNow = Date.now();

    for (; i < timers.length; i++) {
      timer = timers[i];

      if (!timer() && timers[i] === timer) {
        timers.splice(i--, 1);
      }
    }

    if (!timers.length) {
      jQuery.fx.stop();
    }

    fxNow = undefined;
  };

  jQuery.fx.timer = function (timer) {
    jQuery.timers.push(timer);
    jQuery.fx.start();
  };

  jQuery.fx.interval = 13;

  jQuery.fx.start = function () {
    if (inProgress) {
      return;
    }

    inProgress = true;
    schedule();
  };

  jQuery.fx.stop = function () {
    inProgress = null;
  };

  jQuery.fx.speeds = {
    slow: 600,
    fast: 200,
    _default: 400
  };

  jQuery.fn.delay = function (time, type) {
    time = jQuery.fx ? jQuery.fx.speeds[time] || time : time;
    type = type || "fx";
    return this.queue(type, function (next, hooks) {
      var timeout = window.setTimeout(next, time);

      hooks.stop = function () {
        window.clearTimeout(timeout);
      };
    });
  };

  (function () {
    var input = document.createElement("input"),
        select = document.createElement("select"),
        opt = select.appendChild(document.createElement("option"));
    input.type = "checkbox";
    support.checkOn = input.value !== "";
    support.optSelected = opt.selected;
    input = document.createElement("input");
    input.value = "t";
    input.type = "radio";
    support.radioValue = input.value === "t";
  })();

  var boolHook,
      attrHandle = jQuery.expr.attrHandle;
  jQuery.fn.extend({
    attr: function attr(name, value) {
      return access(this, jQuery.attr, name, value, arguments.length > 1);
    },
    removeAttr: function removeAttr(name) {
      return this.each(function () {
        jQuery.removeAttr(this, name);
      });
    }
  });
  jQuery.extend({
    attr: function attr(elem, name, value) {
      var ret,
          hooks,
          nType = elem.nodeType;

      if (nType === 3 || nType === 8 || nType === 2) {
        return;
      }

      if (typeof elem.getAttribute === "undefined") {
        return jQuery.prop(elem, name, value);
      }

      if (nType !== 1 || !jQuery.isXMLDoc(elem)) {
        hooks = jQuery.attrHooks[name.toLowerCase()] || (jQuery.expr.match.bool.test(name) ? boolHook : undefined);
      }

      if (value !== undefined) {
        if (value === null) {
          jQuery.removeAttr(elem, name);
          return;
        }

        if (hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined) {
          return ret;
        }

        elem.setAttribute(name, value + "");
        return value;
      }

      if (hooks && "get" in hooks && (ret = hooks.get(elem, name)) !== null) {
        return ret;
      }

      ret = jQuery.find.attr(elem, name);
      return ret == null ? undefined : ret;
    },
    attrHooks: {
      type: {
        set: function set(elem, value) {
          if (!support.radioValue && value === "radio" && nodeName(elem, "input")) {
            var val = elem.value;
            elem.setAttribute("type", value);

            if (val) {
              elem.value = val;
            }

            return value;
          }
        }
      }
    },
    removeAttr: function removeAttr(elem, value) {
      var name,
          i = 0,
          attrNames = value && value.match(rnothtmlwhite);

      if (attrNames && elem.nodeType === 1) {
        while (name = attrNames[i++]) {
          elem.removeAttribute(name);
        }
      }
    }
  });
  boolHook = {
    set: function set(elem, value, name) {
      if (value === false) {
        jQuery.removeAttr(elem, name);
      } else {
        elem.setAttribute(name, name);
      }

      return name;
    }
  };
  jQuery.each(jQuery.expr.match.bool.source.match(/\w+/g), function (_i, name) {
    var getter = attrHandle[name] || jQuery.find.attr;

    attrHandle[name] = function (elem, name, isXML) {
      var ret,
          handle,
          lowercaseName = name.toLowerCase();

      if (!isXML) {
        handle = attrHandle[lowercaseName];
        attrHandle[lowercaseName] = ret;
        ret = getter(elem, name, isXML) != null ? lowercaseName : null;
        attrHandle[lowercaseName] = handle;
      }

      return ret;
    };
  });
  var rfocusable = /^(?:input|select|textarea|button)$/i,
      rclickable = /^(?:a|area)$/i;
  jQuery.fn.extend({
    prop: function prop(name, value) {
      return access(this, jQuery.prop, name, value, arguments.length > 1);
    },
    removeProp: function removeProp(name) {
      return this.each(function () {
        delete this[jQuery.propFix[name] || name];
      });
    }
  });
  jQuery.extend({
    prop: function prop(elem, name, value) {
      var ret,
          hooks,
          nType = elem.nodeType;

      if (nType === 3 || nType === 8 || nType === 2) {
        return;
      }

      if (nType !== 1 || !jQuery.isXMLDoc(elem)) {
        name = jQuery.propFix[name] || name;
        hooks = jQuery.propHooks[name];
      }

      if (value !== undefined) {
        if (hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined) {
          return ret;
        }

        return elem[name] = value;
      }

      if (hooks && "get" in hooks && (ret = hooks.get(elem, name)) !== null) {
        return ret;
      }

      return elem[name];
    },
    propHooks: {
      tabIndex: {
        get: function get(elem) {
          var tabindex = jQuery.find.attr(elem, "tabindex");

          if (tabindex) {
            return parseInt(tabindex, 10);
          }

          if (rfocusable.test(elem.nodeName) || rclickable.test(elem.nodeName) && elem.href) {
            return 0;
          }

          return -1;
        }
      }
    },
    propFix: {
      "for": "htmlFor",
      "class": "className"
    }
  });

  if (!support.optSelected) {
    jQuery.propHooks.selected = {
      get: function get(elem) {
        var parent = elem.parentNode;

        if (parent && parent.parentNode) {
          parent.parentNode.selectedIndex;
        }

        return null;
      },
      set: function set(elem) {
        var parent = elem.parentNode;

        if (parent) {
          parent.selectedIndex;

          if (parent.parentNode) {
            parent.parentNode.selectedIndex;
          }
        }
      }
    };
  }

  jQuery.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
    jQuery.propFix[this.toLowerCase()] = this;
  });

  function stripAndCollapse(value) {
    var tokens = value.match(rnothtmlwhite) || [];
    return tokens.join(" ");
  }

  function getClass(elem) {
    return elem.getAttribute && elem.getAttribute("class") || "";
  }

  function classesToArray(value) {
    if (Array.isArray(value)) {
      return value;
    }

    if (typeof value === "string") {
      return value.match(rnothtmlwhite) || [];
    }

    return [];
  }

  jQuery.fn.extend({
    addClass: function addClass(value) {
      var classes,
          elem,
          cur,
          curValue,
          clazz,
          j,
          finalValue,
          i = 0;

      if (isFunction(value)) {
        return this.each(function (j) {
          jQuery(this).addClass(value.call(this, j, getClass(this)));
        });
      }

      classes = classesToArray(value);

      if (classes.length) {
        while (elem = this[i++]) {
          curValue = getClass(elem);
          cur = elem.nodeType === 1 && " " + stripAndCollapse(curValue) + " ";

          if (cur) {
            j = 0;

            while (clazz = classes[j++]) {
              if (cur.indexOf(" " + clazz + " ") < 0) {
                cur += clazz + " ";
              }
            }

            finalValue = stripAndCollapse(cur);

            if (curValue !== finalValue) {
              elem.setAttribute("class", finalValue);
            }
          }
        }
      }

      return this;
    },
    removeClass: function removeClass(value) {
      var classes,
          elem,
          cur,
          curValue,
          clazz,
          j,
          finalValue,
          i = 0;

      if (isFunction(value)) {
        return this.each(function (j) {
          jQuery(this).removeClass(value.call(this, j, getClass(this)));
        });
      }

      if (!arguments.length) {
        return this.attr("class", "");
      }

      classes = classesToArray(value);

      if (classes.length) {
        while (elem = this[i++]) {
          curValue = getClass(elem);
          cur = elem.nodeType === 1 && " " + stripAndCollapse(curValue) + " ";

          if (cur) {
            j = 0;

            while (clazz = classes[j++]) {
              while (cur.indexOf(" " + clazz + " ") > -1) {
                cur = cur.replace(" " + clazz + " ", " ");
              }
            }

            finalValue = stripAndCollapse(cur);

            if (curValue !== finalValue) {
              elem.setAttribute("class", finalValue);
            }
          }
        }
      }

      return this;
    },
    toggleClass: function toggleClass(value, stateVal) {
      var type = _typeof(value),
          isValidValue = type === "string" || Array.isArray(value);

      if (typeof stateVal === "boolean" && isValidValue) {
        return stateVal ? this.addClass(value) : this.removeClass(value);
      }

      if (isFunction(value)) {
        return this.each(function (i) {
          jQuery(this).toggleClass(value.call(this, i, getClass(this), stateVal), stateVal);
        });
      }

      return this.each(function () {
        var className, i, self, classNames;

        if (isValidValue) {
          i = 0;
          self = jQuery(this);
          classNames = classesToArray(value);

          while (className = classNames[i++]) {
            if (self.hasClass(className)) {
              self.removeClass(className);
            } else {
              self.addClass(className);
            }
          }
        } else if (value === undefined || type === "boolean") {
          className = getClass(this);

          if (className) {
            dataPriv.set(this, "__className__", className);
          }

          if (this.setAttribute) {
            this.setAttribute("class", className || value === false ? "" : dataPriv.get(this, "__className__") || "");
          }
        }
      });
    },
    hasClass: function hasClass(selector) {
      var className,
          elem,
          i = 0;
      className = " " + selector + " ";

      while (elem = this[i++]) {
        if (elem.nodeType === 1 && (" " + stripAndCollapse(getClass(elem)) + " ").indexOf(className) > -1) {
          return true;
        }
      }

      return false;
    }
  });
  var rreturn = /\r/g;
  jQuery.fn.extend({
    val: function val(value) {
      var hooks,
          ret,
          valueIsFunction,
          elem = this[0];

      if (!arguments.length) {
        if (elem) {
          hooks = jQuery.valHooks[elem.type] || jQuery.valHooks[elem.nodeName.toLowerCase()];

          if (hooks && "get" in hooks && (ret = hooks.get(elem, "value")) !== undefined) {
            return ret;
          }

          ret = elem.value;

          if (typeof ret === "string") {
            return ret.replace(rreturn, "");
          }

          return ret == null ? "" : ret;
        }

        return;
      }

      valueIsFunction = isFunction(value);
      return this.each(function (i) {
        var val;

        if (this.nodeType !== 1) {
          return;
        }

        if (valueIsFunction) {
          val = value.call(this, i, jQuery(this).val());
        } else {
          val = value;
        }

        if (val == null) {
          val = "";
        } else if (typeof val === "number") {
          val += "";
        } else if (Array.isArray(val)) {
          val = jQuery.map(val, function (value) {
            return value == null ? "" : value + "";
          });
        }

        hooks = jQuery.valHooks[this.type] || jQuery.valHooks[this.nodeName.toLowerCase()];

        if (!hooks || !("set" in hooks) || hooks.set(this, val, "value") === undefined) {
          this.value = val;
        }
      });
    }
  });
  jQuery.extend({
    valHooks: {
      option: {
        get: function get(elem) {
          var val = jQuery.find.attr(elem, "value");
          return val != null ? val : stripAndCollapse(jQuery.text(elem));
        }
      },
      select: {
        get: function get(elem) {
          var value,
              option,
              i,
              options = elem.options,
              index = elem.selectedIndex,
              one = elem.type === "select-one",
              values = one ? null : [],
              max = one ? index + 1 : options.length;

          if (index < 0) {
            i = max;
          } else {
            i = one ? index : 0;
          }

          for (; i < max; i++) {
            option = options[i];

            if ((option.selected || i === index) && !option.disabled && (!option.parentNode.disabled || !nodeName(option.parentNode, "optgroup"))) {
              value = jQuery(option).val();

              if (one) {
                return value;
              }

              values.push(value);
            }
          }

          return values;
        },
        set: function set(elem, value) {
          var optionSet,
              option,
              options = elem.options,
              values = jQuery.makeArray(value),
              i = options.length;

          while (i--) {
            option = options[i];

            if (option.selected = jQuery.inArray(jQuery.valHooks.option.get(option), values) > -1) {
              optionSet = true;
            }
          }

          if (!optionSet) {
            elem.selectedIndex = -1;
          }

          return values;
        }
      }
    }
  });
  jQuery.each(["radio", "checkbox"], function () {
    jQuery.valHooks[this] = {
      set: function set(elem, value) {
        if (Array.isArray(value)) {
          return elem.checked = jQuery.inArray(jQuery(elem).val(), value) > -1;
        }
      }
    };

    if (!support.checkOn) {
      jQuery.valHooks[this].get = function (elem) {
        return elem.getAttribute("value") === null ? "on" : elem.value;
      };
    }
  });
  support.focusin = "onfocusin" in window;

  var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
      stopPropagationCallback = function stopPropagationCallback(e) {
    e.stopPropagation();
  };

  jQuery.extend(jQuery.event, {
    trigger: function trigger(event, data, elem, onlyHandlers) {
      var i,
          cur,
          tmp,
          bubbleType,
          ontype,
          handle,
          special,
          lastElement,
          eventPath = [elem || document],
          type = hasOwn.call(event, "type") ? event.type : event,
          namespaces = hasOwn.call(event, "namespace") ? event.namespace.split(".") : [];
      cur = lastElement = tmp = elem = elem || document;

      if (elem.nodeType === 3 || elem.nodeType === 8) {
        return;
      }

      if (rfocusMorph.test(type + jQuery.event.triggered)) {
        return;
      }

      if (type.indexOf(".") > -1) {
        namespaces = type.split(".");
        type = namespaces.shift();
        namespaces.sort();
      }

      ontype = type.indexOf(":") < 0 && "on" + type;
      event = event[jQuery.expando] ? event : new jQuery.Event(type, _typeof(event) === "object" && event);
      event.isTrigger = onlyHandlers ? 2 : 3;
      event.namespace = namespaces.join(".");
      event.rnamespace = event.namespace ? new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)") : null;
      event.result = undefined;

      if (!event.target) {
        event.target = elem;
      }

      data = data == null ? [event] : jQuery.makeArray(data, [event]);
      special = jQuery.event.special[type] || {};

      if (!onlyHandlers && special.trigger && special.trigger.apply(elem, data) === false) {
        return;
      }

      if (!onlyHandlers && !special.noBubble && !isWindow(elem)) {
        bubbleType = special.delegateType || type;

        if (!rfocusMorph.test(bubbleType + type)) {
          cur = cur.parentNode;
        }

        for (; cur; cur = cur.parentNode) {
          eventPath.push(cur);
          tmp = cur;
        }

        if (tmp === (elem.ownerDocument || document)) {
          eventPath.push(tmp.defaultView || tmp.parentWindow || window);
        }
      }

      i = 0;

      while ((cur = eventPath[i++]) && !event.isPropagationStopped()) {
        lastElement = cur;
        event.type = i > 1 ? bubbleType : special.bindType || type;
        handle = (dataPriv.get(cur, "events") || Object.create(null))[event.type] && dataPriv.get(cur, "handle");

        if (handle) {
          handle.apply(cur, data);
        }

        handle = ontype && cur[ontype];

        if (handle && handle.apply && acceptData(cur)) {
          event.result = handle.apply(cur, data);

          if (event.result === false) {
            event.preventDefault();
          }
        }
      }

      event.type = type;

      if (!onlyHandlers && !event.isDefaultPrevented()) {
        if ((!special._default || special._default.apply(eventPath.pop(), data) === false) && acceptData(elem)) {
          if (ontype && isFunction(elem[type]) && !isWindow(elem)) {
            tmp = elem[ontype];

            if (tmp) {
              elem[ontype] = null;
            }

            jQuery.event.triggered = type;

            if (event.isPropagationStopped()) {
              lastElement.addEventListener(type, stopPropagationCallback);
            }

            elem[type]();

            if (event.isPropagationStopped()) {
              lastElement.removeEventListener(type, stopPropagationCallback);
            }

            jQuery.event.triggered = undefined;

            if (tmp) {
              elem[ontype] = tmp;
            }
          }
        }
      }

      return event.result;
    },
    simulate: function simulate(type, elem, event) {
      var e = jQuery.extend(new jQuery.Event(), event, {
        type: type,
        isSimulated: true
      });
      jQuery.event.trigger(e, null, elem);
    }
  });
  jQuery.fn.extend({
    trigger: function trigger(type, data) {
      return this.each(function () {
        jQuery.event.trigger(type, data, this);
      });
    },
    triggerHandler: function triggerHandler(type, data) {
      var elem = this[0];

      if (elem) {
        return jQuery.event.trigger(type, data, elem, true);
      }
    }
  });

  if (!support.focusin) {
    jQuery.each({
      focus: "focusin",
      blur: "focusout"
    }, function (orig, fix) {
      var handler = function handler(event) {
        jQuery.event.simulate(fix, event.target, jQuery.event.fix(event));
      };

      jQuery.event.special[fix] = {
        setup: function setup() {
          var doc = this.ownerDocument || this.document || this,
              attaches = dataPriv.access(doc, fix);

          if (!attaches) {
            doc.addEventListener(orig, handler, true);
          }

          dataPriv.access(doc, fix, (attaches || 0) + 1);
        },
        teardown: function teardown() {
          var doc = this.ownerDocument || this.document || this,
              attaches = dataPriv.access(doc, fix) - 1;

          if (!attaches) {
            doc.removeEventListener(orig, handler, true);
            dataPriv.remove(doc, fix);
          } else {
            dataPriv.access(doc, fix, attaches);
          }
        }
      };
    });
  }

  var location = window.location;
  var nonce = {
    guid: Date.now()
  };
  var rquery = /\?/;

  jQuery.parseXML = function (data) {
    var xml;

    if (!data || typeof data !== "string") {
      return null;
    }

    try {
      xml = new window.DOMParser().parseFromString(data, "text/xml");
    } catch (e) {
      xml = undefined;
    }

    if (!xml || xml.getElementsByTagName("parsererror").length) {
      jQuery.error("Invalid XML: " + data);
    }

    return xml;
  };

  var rbracket = /\[\]$/,
      rCRLF = /\r?\n/g,
      rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
      rsubmittable = /^(?:input|select|textarea|keygen)/i;

  function buildParams(prefix, obj, traditional, add) {
    var name;

    if (Array.isArray(obj)) {
      jQuery.each(obj, function (i, v) {
        if (traditional || rbracket.test(prefix)) {
          add(prefix, v);
        } else {
          buildParams(prefix + "[" + (_typeof(v) === "object" && v != null ? i : "") + "]", v, traditional, add);
        }
      });
    } else if (!traditional && toType(obj) === "object") {
      for (name in obj) {
        buildParams(prefix + "[" + name + "]", obj[name], traditional, add);
      }
    } else {
      add(prefix, obj);
    }
  }

  jQuery.param = function (a, traditional) {
    var prefix,
        s = [],
        add = function add(key, valueOrFunction) {
      var value = isFunction(valueOrFunction) ? valueOrFunction() : valueOrFunction;
      s[s.length] = encodeURIComponent(key) + "=" + encodeURIComponent(value == null ? "" : value);
    };

    if (a == null) {
      return "";
    }

    if (Array.isArray(a) || a.jquery && !jQuery.isPlainObject(a)) {
      jQuery.each(a, function () {
        add(this.name, this.value);
      });
    } else {
      for (prefix in a) {
        buildParams(prefix, a[prefix], traditional, add);
      }
    }

    return s.join("&");
  };

  jQuery.fn.extend({
    serialize: function serialize() {
      return jQuery.param(this.serializeArray());
    },
    serializeArray: function serializeArray() {
      return this.map(function () {
        var elements = jQuery.prop(this, "elements");
        return elements ? jQuery.makeArray(elements) : this;
      }).filter(function () {
        var type = this.type;
        return this.name && !jQuery(this).is(":disabled") && rsubmittable.test(this.nodeName) && !rsubmitterTypes.test(type) && (this.checked || !rcheckableType.test(type));
      }).map(function (_i, elem) {
        var val = jQuery(this).val();

        if (val == null) {
          return null;
        }

        if (Array.isArray(val)) {
          return jQuery.map(val, function (val) {
            return {
              name: elem.name,
              value: val.replace(rCRLF, "\r\n")
            };
          });
        }

        return {
          name: elem.name,
          value: val.replace(rCRLF, "\r\n")
        };
      }).get();
    }
  });
  var r20 = /%20/g,
      rhash = /#.*$/,
      rantiCache = /([?&])_=[^&]*/,
      rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,
      rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
      rnoContent = /^(?:GET|HEAD)$/,
      rprotocol = /^\/\//,
      prefilters = {},
      transports = {},
      allTypes = "*/".concat("*"),
      originAnchor = document.createElement("a");
  originAnchor.href = location.href;

  function addToPrefiltersOrTransports(structure) {
    return function (dataTypeExpression, func) {
      if (typeof dataTypeExpression !== "string") {
        func = dataTypeExpression;
        dataTypeExpression = "*";
      }

      var dataType,
          i = 0,
          dataTypes = dataTypeExpression.toLowerCase().match(rnothtmlwhite) || [];

      if (isFunction(func)) {
        while (dataType = dataTypes[i++]) {
          if (dataType[0] === "+") {
            dataType = dataType.slice(1) || "*";
            (structure[dataType] = structure[dataType] || []).unshift(func);
          } else {
            (structure[dataType] = structure[dataType] || []).push(func);
          }
        }
      }
    };
  }

  function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR) {
    var inspected = {},
        seekingTransport = structure === transports;

    function inspect(dataType) {
      var selected;
      inspected[dataType] = true;
      jQuery.each(structure[dataType] || [], function (_, prefilterOrFactory) {
        var dataTypeOrTransport = prefilterOrFactory(options, originalOptions, jqXHR);

        if (typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[dataTypeOrTransport]) {
          options.dataTypes.unshift(dataTypeOrTransport);
          inspect(dataTypeOrTransport);
          return false;
        } else if (seekingTransport) {
          return !(selected = dataTypeOrTransport);
        }
      });
      return selected;
    }

    return inspect(options.dataTypes[0]) || !inspected["*"] && inspect("*");
  }

  function ajaxExtend(target, src) {
    var key,
        deep,
        flatOptions = jQuery.ajaxSettings.flatOptions || {};

    for (key in src) {
      if (src[key] !== undefined) {
        (flatOptions[key] ? target : deep || (deep = {}))[key] = src[key];
      }
    }

    if (deep) {
      jQuery.extend(true, target, deep);
    }

    return target;
  }

  function ajaxHandleResponses(s, jqXHR, responses) {
    var ct,
        type,
        finalDataType,
        firstDataType,
        contents = s.contents,
        dataTypes = s.dataTypes;

    while (dataTypes[0] === "*") {
      dataTypes.shift();

      if (ct === undefined) {
        ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
      }
    }

    if (ct) {
      for (type in contents) {
        if (contents[type] && contents[type].test(ct)) {
          dataTypes.unshift(type);
          break;
        }
      }
    }

    if (dataTypes[0] in responses) {
      finalDataType = dataTypes[0];
    } else {
      for (type in responses) {
        if (!dataTypes[0] || s.converters[type + " " + dataTypes[0]]) {
          finalDataType = type;
          break;
        }

        if (!firstDataType) {
          firstDataType = type;
        }
      }

      finalDataType = finalDataType || firstDataType;
    }

    if (finalDataType) {
      if (finalDataType !== dataTypes[0]) {
        dataTypes.unshift(finalDataType);
      }

      return responses[finalDataType];
    }
  }

  function ajaxConvert(s, response, jqXHR, isSuccess) {
    var conv2,
        current,
        conv,
        tmp,
        prev,
        converters = {},
        dataTypes = s.dataTypes.slice();

    if (dataTypes[1]) {
      for (conv in s.converters) {
        converters[conv.toLowerCase()] = s.converters[conv];
      }
    }

    current = dataTypes.shift();

    while (current) {
      if (s.responseFields[current]) {
        jqXHR[s.responseFields[current]] = response;
      }

      if (!prev && isSuccess && s.dataFilter) {
        response = s.dataFilter(response, s.dataType);
      }

      prev = current;
      current = dataTypes.shift();

      if (current) {
        if (current === "*") {
          current = prev;
        } else if (prev !== "*" && prev !== current) {
          conv = converters[prev + " " + current] || converters["* " + current];

          if (!conv) {
            for (conv2 in converters) {
              tmp = conv2.split(" ");

              if (tmp[1] === current) {
                conv = converters[prev + " " + tmp[0]] || converters["* " + tmp[0]];

                if (conv) {
                  if (conv === true) {
                    conv = converters[conv2];
                  } else if (converters[conv2] !== true) {
                    current = tmp[0];
                    dataTypes.unshift(tmp[1]);
                  }

                  break;
                }
              }
            }
          }

          if (conv !== true) {
            if (conv && s["throws"]) {
              response = conv(response);
            } else {
              try {
                response = conv(response);
              } catch (e) {
                return {
                  state: "parsererror",
                  error: conv ? e : "No conversion from " + prev + " to " + current
                };
              }
            }
          }
        }
      }
    }

    return {
      state: "success",
      data: response
    };
  }

  jQuery.extend({
    active: 0,
    lastModified: {},
    etag: {},
    ajaxSettings: {
      url: location.href,
      type: "GET",
      isLocal: rlocalProtocol.test(location.protocol),
      global: true,
      processData: true,
      async: true,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      accepts: {
        "*": allTypes,
        text: "text/plain",
        html: "text/html",
        xml: "application/xml, text/xml",
        json: "application/json, text/javascript"
      },
      contents: {
        xml: /\bxml\b/,
        html: /\bhtml/,
        json: /\bjson\b/
      },
      responseFields: {
        xml: "responseXML",
        text: "responseText",
        json: "responseJSON"
      },
      converters: {
        "* text": String,
        "text html": true,
        "text json": JSON.parse,
        "text xml": jQuery.parseXML
      },
      flatOptions: {
        url: true,
        context: true
      }
    },
    ajaxSetup: function ajaxSetup(target, settings) {
      return settings ? ajaxExtend(ajaxExtend(target, jQuery.ajaxSettings), settings) : ajaxExtend(jQuery.ajaxSettings, target);
    },
    ajaxPrefilter: addToPrefiltersOrTransports(prefilters),
    ajaxTransport: addToPrefiltersOrTransports(transports),
    ajax: function ajax(url, options) {
      if (_typeof(url) === "object") {
        options = url;
        url = undefined;
      }

      options = options || {};

      var transport,
          cacheURL,
          responseHeadersString,
          responseHeaders,
          timeoutTimer,
          urlAnchor,
          completed,
          fireGlobals,
          i,
          uncached,
          s = jQuery.ajaxSetup({}, options),
          callbackContext = s.context || s,
          globalEventContext = s.context && (callbackContext.nodeType || callbackContext.jquery) ? jQuery(callbackContext) : jQuery.event,
          deferred = jQuery.Deferred(),
          completeDeferred = jQuery.Callbacks("once memory"),
          _statusCode = s.statusCode || {},
          requestHeaders = {},
          requestHeadersNames = {},
          strAbort = "canceled",
          jqXHR = {
        readyState: 0,
        getResponseHeader: function getResponseHeader(key) {
          var match;

          if (completed) {
            if (!responseHeaders) {
              responseHeaders = {};

              while (match = rheaders.exec(responseHeadersString)) {
                responseHeaders[match[1].toLowerCase() + " "] = (responseHeaders[match[1].toLowerCase() + " "] || []).concat(match[2]);
              }
            }

            match = responseHeaders[key.toLowerCase() + " "];
          }

          return match == null ? null : match.join(", ");
        },
        getAllResponseHeaders: function getAllResponseHeaders() {
          return completed ? responseHeadersString : null;
        },
        setRequestHeader: function setRequestHeader(name, value) {
          if (completed == null) {
            name = requestHeadersNames[name.toLowerCase()] = requestHeadersNames[name.toLowerCase()] || name;
            requestHeaders[name] = value;
          }

          return this;
        },
        overrideMimeType: function overrideMimeType(type) {
          if (completed == null) {
            s.mimeType = type;
          }

          return this;
        },
        statusCode: function statusCode(map) {
          var code;

          if (map) {
            if (completed) {
              jqXHR.always(map[jqXHR.status]);
            } else {
              for (code in map) {
                _statusCode[code] = [_statusCode[code], map[code]];
              }
            }
          }

          return this;
        },
        abort: function abort(statusText) {
          var finalText = statusText || strAbort;

          if (transport) {
            transport.abort(finalText);
          }

          done(0, finalText);
          return this;
        }
      };

      deferred.promise(jqXHR);
      s.url = ((url || s.url || location.href) + "").replace(rprotocol, location.protocol + "//");
      s.type = options.method || options.type || s.method || s.type;
      s.dataTypes = (s.dataType || "*").toLowerCase().match(rnothtmlwhite) || [""];

      if (s.crossDomain == null) {
        urlAnchor = document.createElement("a");

        try {
          urlAnchor.href = s.url;
          urlAnchor.href = urlAnchor.href;
          s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !== urlAnchor.protocol + "//" + urlAnchor.host;
        } catch (e) {
          s.crossDomain = true;
        }
      }

      if (s.data && s.processData && typeof s.data !== "string") {
        s.data = jQuery.param(s.data, s.traditional);
      }

      inspectPrefiltersOrTransports(prefilters, s, options, jqXHR);

      if (completed) {
        return jqXHR;
      }

      fireGlobals = jQuery.event && s.global;

      if (fireGlobals && jQuery.active++ === 0) {
        jQuery.event.trigger("ajaxStart");
      }

      s.type = s.type.toUpperCase();
      s.hasContent = !rnoContent.test(s.type);
      cacheURL = s.url.replace(rhash, "");

      if (!s.hasContent) {
        uncached = s.url.slice(cacheURL.length);

        if (s.data && (s.processData || typeof s.data === "string")) {
          cacheURL += (rquery.test(cacheURL) ? "&" : "?") + s.data;
          delete s.data;
        }

        if (s.cache === false) {
          cacheURL = cacheURL.replace(rantiCache, "$1");
          uncached = (rquery.test(cacheURL) ? "&" : "?") + "_=" + nonce.guid++ + uncached;
        }

        s.url = cacheURL + uncached;
      } else if (s.data && s.processData && (s.contentType || "").indexOf("application/x-www-form-urlencoded") === 0) {
        s.data = s.data.replace(r20, "+");
      }

      if (s.ifModified) {
        if (jQuery.lastModified[cacheURL]) {
          jqXHR.setRequestHeader("If-Modified-Since", jQuery.lastModified[cacheURL]);
        }

        if (jQuery.etag[cacheURL]) {
          jqXHR.setRequestHeader("If-None-Match", jQuery.etag[cacheURL]);
        }
      }

      if (s.data && s.hasContent && s.contentType !== false || options.contentType) {
        jqXHR.setRequestHeader("Content-Type", s.contentType);
      }

      jqXHR.setRequestHeader("Accept", s.dataTypes[0] && s.accepts[s.dataTypes[0]] ? s.accepts[s.dataTypes[0]] + (s.dataTypes[0] !== "*" ? ", " + allTypes + "; q=0.01" : "") : s.accepts["*"]);

      for (i in s.headers) {
        jqXHR.setRequestHeader(i, s.headers[i]);
      }

      if (s.beforeSend && (s.beforeSend.call(callbackContext, jqXHR, s) === false || completed)) {
        return jqXHR.abort();
      }

      strAbort = "abort";
      completeDeferred.add(s.complete);
      jqXHR.done(s.success);
      jqXHR.fail(s.error);
      transport = inspectPrefiltersOrTransports(transports, s, options, jqXHR);

      if (!transport) {
        done(-1, "No Transport");
      } else {
        jqXHR.readyState = 1;

        if (fireGlobals) {
          globalEventContext.trigger("ajaxSend", [jqXHR, s]);
        }

        if (completed) {
          return jqXHR;
        }

        if (s.async && s.timeout > 0) {
          timeoutTimer = window.setTimeout(function () {
            jqXHR.abort("timeout");
          }, s.timeout);
        }

        try {
          completed = false;
          transport.send(requestHeaders, done);
        } catch (e) {
          if (completed) {
            throw e;
          }

          done(-1, e);
        }
      }

      function done(status, nativeStatusText, responses, headers) {
        var isSuccess,
            success,
            error,
            response,
            modified,
            statusText = nativeStatusText;

        if (completed) {
          return;
        }

        completed = true;

        if (timeoutTimer) {
          window.clearTimeout(timeoutTimer);
        }

        transport = undefined;
        responseHeadersString = headers || "";
        jqXHR.readyState = status > 0 ? 4 : 0;
        isSuccess = status >= 200 && status < 300 || status === 304;

        if (responses) {
          response = ajaxHandleResponses(s, jqXHR, responses);
        }

        if (!isSuccess && jQuery.inArray("script", s.dataTypes) > -1) {
          s.converters["text script"] = function () {};
        }

        response = ajaxConvert(s, response, jqXHR, isSuccess);

        if (isSuccess) {
          if (s.ifModified) {
            modified = jqXHR.getResponseHeader("Last-Modified");

            if (modified) {
              jQuery.lastModified[cacheURL] = modified;
            }

            modified = jqXHR.getResponseHeader("etag");

            if (modified) {
              jQuery.etag[cacheURL] = modified;
            }
          }

          if (status === 204 || s.type === "HEAD") {
            statusText = "nocontent";
          } else if (status === 304) {
            statusText = "notmodified";
          } else {
            statusText = response.state;
            success = response.data;
            error = response.error;
            isSuccess = !error;
          }
        } else {
          error = statusText;

          if (status || !statusText) {
            statusText = "error";

            if (status < 0) {
              status = 0;
            }
          }
        }

        jqXHR.status = status;
        jqXHR.statusText = (nativeStatusText || statusText) + "";

        if (isSuccess) {
          deferred.resolveWith(callbackContext, [success, statusText, jqXHR]);
        } else {
          deferred.rejectWith(callbackContext, [jqXHR, statusText, error]);
        }

        jqXHR.statusCode(_statusCode);
        _statusCode = undefined;

        if (fireGlobals) {
          globalEventContext.trigger(isSuccess ? "ajaxSuccess" : "ajaxError", [jqXHR, s, isSuccess ? success : error]);
        }

        completeDeferred.fireWith(callbackContext, [jqXHR, statusText]);

        if (fireGlobals) {
          globalEventContext.trigger("ajaxComplete", [jqXHR, s]);

          if (! --jQuery.active) {
            jQuery.event.trigger("ajaxStop");
          }
        }
      }

      return jqXHR;
    },
    getJSON: function getJSON(url, data, callback) {
      return jQuery.get(url, data, callback, "json");
    },
    getScript: function getScript(url, callback) {
      return jQuery.get(url, undefined, callback, "script");
    }
  });
  jQuery.each(["get", "post"], function (_i, method) {
    jQuery[method] = function (url, data, callback, type) {
      if (isFunction(data)) {
        type = type || callback;
        callback = data;
        data = undefined;
      }

      return jQuery.ajax(jQuery.extend({
        url: url,
        type: method,
        dataType: type,
        data: data,
        success: callback
      }, jQuery.isPlainObject(url) && url));
    };
  });
  jQuery.ajaxPrefilter(function (s) {
    var i;

    for (i in s.headers) {
      if (i.toLowerCase() === "content-type") {
        s.contentType = s.headers[i] || "";
      }
    }
  });

  jQuery._evalUrl = function (url, options, doc) {
    return jQuery.ajax({
      url: url,
      type: "GET",
      dataType: "script",
      cache: true,
      async: false,
      global: false,
      converters: {
        "text script": function textScript() {}
      },
      dataFilter: function dataFilter(response) {
        jQuery.globalEval(response, options, doc);
      }
    });
  };

  jQuery.fn.extend({
    wrapAll: function wrapAll(html) {
      var wrap;

      if (this[0]) {
        if (isFunction(html)) {
          html = html.call(this[0]);
        }

        wrap = jQuery(html, this[0].ownerDocument).eq(0).clone(true);

        if (this[0].parentNode) {
          wrap.insertBefore(this[0]);
        }

        wrap.map(function () {
          var elem = this;

          while (elem.firstElementChild) {
            elem = elem.firstElementChild;
          }

          return elem;
        }).append(this);
      }

      return this;
    },
    wrapInner: function wrapInner(html) {
      if (isFunction(html)) {
        return this.each(function (i) {
          jQuery(this).wrapInner(html.call(this, i));
        });
      }

      return this.each(function () {
        var self = jQuery(this),
            contents = self.contents();

        if (contents.length) {
          contents.wrapAll(html);
        } else {
          self.append(html);
        }
      });
    },
    wrap: function wrap(html) {
      var htmlIsFunction = isFunction(html);
      return this.each(function (i) {
        jQuery(this).wrapAll(htmlIsFunction ? html.call(this, i) : html);
      });
    },
    unwrap: function unwrap(selector) {
      this.parent(selector).not("body").each(function () {
        jQuery(this).replaceWith(this.childNodes);
      });
      return this;
    }
  });

  jQuery.expr.pseudos.hidden = function (elem) {
    return !jQuery.expr.pseudos.visible(elem);
  };

  jQuery.expr.pseudos.visible = function (elem) {
    return !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
  };

  jQuery.ajaxSettings.xhr = function () {
    try {
      return new window.XMLHttpRequest();
    } catch (e) {}
  };

  var xhrSuccessStatus = {
    0: 200,
    1223: 204
  },
      xhrSupported = jQuery.ajaxSettings.xhr();
  support.cors = !!xhrSupported && "withCredentials" in xhrSupported;
  support.ajax = xhrSupported = !!xhrSupported;
  jQuery.ajaxTransport(function (options) {
    var _callback, errorCallback;

    if (support.cors || xhrSupported && !options.crossDomain) {
      return {
        send: function send(headers, complete) {
          var i,
              xhr = options.xhr();
          xhr.open(options.type, options.url, options.async, options.username, options.password);

          if (options.xhrFields) {
            for (i in options.xhrFields) {
              xhr[i] = options.xhrFields[i];
            }
          }

          if (options.mimeType && xhr.overrideMimeType) {
            xhr.overrideMimeType(options.mimeType);
          }

          if (!options.crossDomain && !headers["X-Requested-With"]) {
            headers["X-Requested-With"] = "XMLHttpRequest";
          }

          for (i in headers) {
            xhr.setRequestHeader(i, headers[i]);
          }

          _callback = function callback(type) {
            return function () {
              if (_callback) {
                _callback = errorCallback = xhr.onload = xhr.onerror = xhr.onabort = xhr.ontimeout = xhr.onreadystatechange = null;

                if (type === "abort") {
                  xhr.abort();
                } else if (type === "error") {
                  if (typeof xhr.status !== "number") {
                    complete(0, "error");
                  } else {
                    complete(xhr.status, xhr.statusText);
                  }
                } else {
                  complete(xhrSuccessStatus[xhr.status] || xhr.status, xhr.statusText, (xhr.responseType || "text") !== "text" || typeof xhr.responseText !== "string" ? {
                    binary: xhr.response
                  } : {
                    text: xhr.responseText
                  }, xhr.getAllResponseHeaders());
                }
              }
            };
          };

          xhr.onload = _callback();
          errorCallback = xhr.onerror = xhr.ontimeout = _callback("error");

          if (xhr.onabort !== undefined) {
            xhr.onabort = errorCallback;
          } else {
            xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                window.setTimeout(function () {
                  if (_callback) {
                    errorCallback();
                  }
                });
              }
            };
          }

          _callback = _callback("abort");

          try {
            xhr.send(options.hasContent && options.data || null);
          } catch (e) {
            if (_callback) {
              throw e;
            }
          }
        },
        abort: function abort() {
          if (_callback) {
            _callback();
          }
        }
      };
    }
  });
  jQuery.ajaxPrefilter(function (s) {
    if (s.crossDomain) {
      s.contents.script = false;
    }
  });
  jQuery.ajaxSetup({
    accepts: {
      script: "text/javascript, application/javascript, " + "application/ecmascript, application/x-ecmascript"
    },
    contents: {
      script: /\b(?:java|ecma)script\b/
    },
    converters: {
      "text script": function textScript(text) {
        jQuery.globalEval(text);
        return text;
      }
    }
  });
  jQuery.ajaxPrefilter("script", function (s) {
    if (s.cache === undefined) {
      s.cache = false;
    }

    if (s.crossDomain) {
      s.type = "GET";
    }
  });
  jQuery.ajaxTransport("script", function (s) {
    if (s.crossDomain || s.scriptAttrs) {
      var script, _callback2;

      return {
        send: function send(_, complete) {
          script = jQuery("<script>").attr(s.scriptAttrs || {}).prop({
            charset: s.scriptCharset,
            src: s.url
          }).on("load error", _callback2 = function callback(evt) {
            script.remove();
            _callback2 = null;

            if (evt) {
              complete(evt.type === "error" ? 404 : 200, evt.type);
            }
          });
          document.head.appendChild(script[0]);
        },
        abort: function abort() {
          if (_callback2) {
            _callback2();
          }
        }
      };
    }
  });
  var oldCallbacks = [],
      rjsonp = /(=)\?(?=&|$)|\?\?/;
  jQuery.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function jsonpCallback() {
      var callback = oldCallbacks.pop() || jQuery.expando + "_" + nonce.guid++;
      this[callback] = true;
      return callback;
    }
  });
  jQuery.ajaxPrefilter("json jsonp", function (s, originalSettings, jqXHR) {
    var callbackName,
        overwritten,
        responseContainer,
        jsonProp = s.jsonp !== false && (rjsonp.test(s.url) ? "url" : typeof s.data === "string" && (s.contentType || "").indexOf("application/x-www-form-urlencoded") === 0 && rjsonp.test(s.data) && "data");

    if (jsonProp || s.dataTypes[0] === "jsonp") {
      callbackName = s.jsonpCallback = isFunction(s.jsonpCallback) ? s.jsonpCallback() : s.jsonpCallback;

      if (jsonProp) {
        s[jsonProp] = s[jsonProp].replace(rjsonp, "$1" + callbackName);
      } else if (s.jsonp !== false) {
        s.url += (rquery.test(s.url) ? "&" : "?") + s.jsonp + "=" + callbackName;
      }

      s.converters["script json"] = function () {
        if (!responseContainer) {
          jQuery.error(callbackName + " was not called");
        }

        return responseContainer[0];
      };

      s.dataTypes[0] = "json";
      overwritten = window[callbackName];

      window[callbackName] = function () {
        responseContainer = arguments;
      };

      jqXHR.always(function () {
        if (overwritten === undefined) {
          jQuery(window).removeProp(callbackName);
        } else {
          window[callbackName] = overwritten;
        }

        if (s[callbackName]) {
          s.jsonpCallback = originalSettings.jsonpCallback;
          oldCallbacks.push(callbackName);
        }

        if (responseContainer && isFunction(overwritten)) {
          overwritten(responseContainer[0]);
        }

        responseContainer = overwritten = undefined;
      });
      return "script";
    }
  });

  support.createHTMLDocument = function () {
    var body = document.implementation.createHTMLDocument("").body;
    body.innerHTML = "<form></form><form></form>";
    return body.childNodes.length === 2;
  }();

  jQuery.parseHTML = function (data, context, keepScripts) {
    if (typeof data !== "string") {
      return [];
    }

    if (typeof context === "boolean") {
      keepScripts = context;
      context = false;
    }

    var base, parsed, scripts;

    if (!context) {
      if (support.createHTMLDocument) {
        context = document.implementation.createHTMLDocument("");
        base = context.createElement("base");
        base.href = document.location.href;
        context.head.appendChild(base);
      } else {
        context = document;
      }
    }

    parsed = rsingleTag.exec(data);
    scripts = !keepScripts && [];

    if (parsed) {
      return [context.createElement(parsed[1])];
    }

    parsed = buildFragment([data], context, scripts);

    if (scripts && scripts.length) {
      jQuery(scripts).remove();
    }

    return jQuery.merge([], parsed.childNodes);
  };

  jQuery.fn.load = function (url, params, callback) {
    var selector,
        type,
        response,
        self = this,
        off = url.indexOf(" ");

    if (off > -1) {
      selector = stripAndCollapse(url.slice(off));
      url = url.slice(0, off);
    }

    if (isFunction(params)) {
      callback = params;
      params = undefined;
    } else if (params && _typeof(params) === "object") {
      type = "POST";
    }

    if (self.length > 0) {
      jQuery.ajax({
        url: url,
        type: type || "GET",
        dataType: "html",
        data: params
      }).done(function (responseText) {
        response = arguments;
        self.html(selector ? jQuery("<div>").append(jQuery.parseHTML(responseText)).find(selector) : responseText);
      }).always(callback && function (jqXHR, status) {
        self.each(function () {
          callback.apply(this, response || [jqXHR.responseText, status, jqXHR]);
        });
      });
    }

    return this;
  };

  jQuery.expr.pseudos.animated = function (elem) {
    return jQuery.grep(jQuery.timers, function (fn) {
      return elem === fn.elem;
    }).length;
  };

  jQuery.offset = {
    setOffset: function setOffset(elem, options, i) {
      var curPosition,
          curLeft,
          curCSSTop,
          curTop,
          curOffset,
          curCSSLeft,
          calculatePosition,
          position = jQuery.css(elem, "position"),
          curElem = jQuery(elem),
          props = {};

      if (position === "static") {
        elem.style.position = "relative";
      }

      curOffset = curElem.offset();
      curCSSTop = jQuery.css(elem, "top");
      curCSSLeft = jQuery.css(elem, "left");
      calculatePosition = (position === "absolute" || position === "fixed") && (curCSSTop + curCSSLeft).indexOf("auto") > -1;

      if (calculatePosition) {
        curPosition = curElem.position();
        curTop = curPosition.top;
        curLeft = curPosition.left;
      } else {
        curTop = parseFloat(curCSSTop) || 0;
        curLeft = parseFloat(curCSSLeft) || 0;
      }

      if (isFunction(options)) {
        options = options.call(elem, i, jQuery.extend({}, curOffset));
      }

      if (options.top != null) {
        props.top = options.top - curOffset.top + curTop;
      }

      if (options.left != null) {
        props.left = options.left - curOffset.left + curLeft;
      }

      if ("using" in options) {
        options.using.call(elem, props);
      } else {
        if (typeof props.top === "number") {
          props.top += "px";
        }

        if (typeof props.left === "number") {
          props.left += "px";
        }

        curElem.css(props);
      }
    }
  };
  jQuery.fn.extend({
    offset: function offset(options) {
      if (arguments.length) {
        return options === undefined ? this : this.each(function (i) {
          jQuery.offset.setOffset(this, options, i);
        });
      }

      var rect,
          win,
          elem = this[0];

      if (!elem) {
        return;
      }

      if (!elem.getClientRects().length) {
        return {
          top: 0,
          left: 0
        };
      }

      rect = elem.getBoundingClientRect();
      win = elem.ownerDocument.defaultView;
      return {
        top: rect.top + win.pageYOffset,
        left: rect.left + win.pageXOffset
      };
    },
    position: function position() {
      if (!this[0]) {
        return;
      }

      var offsetParent,
          offset,
          doc,
          elem = this[0],
          parentOffset = {
        top: 0,
        left: 0
      };

      if (jQuery.css(elem, "position") === "fixed") {
        offset = elem.getBoundingClientRect();
      } else {
        offset = this.offset();
        doc = elem.ownerDocument;
        offsetParent = elem.offsetParent || doc.documentElement;

        while (offsetParent && (offsetParent === doc.body || offsetParent === doc.documentElement) && jQuery.css(offsetParent, "position") === "static") {
          offsetParent = offsetParent.parentNode;
        }

        if (offsetParent && offsetParent !== elem && offsetParent.nodeType === 1) {
          parentOffset = jQuery(offsetParent).offset();
          parentOffset.top += jQuery.css(offsetParent, "borderTopWidth", true);
          parentOffset.left += jQuery.css(offsetParent, "borderLeftWidth", true);
        }
      }

      return {
        top: offset.top - parentOffset.top - jQuery.css(elem, "marginTop", true),
        left: offset.left - parentOffset.left - jQuery.css(elem, "marginLeft", true)
      };
    },
    offsetParent: function offsetParent() {
      return this.map(function () {
        var offsetParent = this.offsetParent;

        while (offsetParent && jQuery.css(offsetParent, "position") === "static") {
          offsetParent = offsetParent.offsetParent;
        }

        return offsetParent || documentElement;
      });
    }
  });
  jQuery.each({
    scrollLeft: "pageXOffset",
    scrollTop: "pageYOffset"
  }, function (method, prop) {
    var top = "pageYOffset" === prop;

    jQuery.fn[method] = function (val) {
      return access(this, function (elem, method, val) {
        var win;

        if (isWindow(elem)) {
          win = elem;
        } else if (elem.nodeType === 9) {
          win = elem.defaultView;
        }

        if (val === undefined) {
          return win ? win[prop] : elem[method];
        }

        if (win) {
          win.scrollTo(!top ? val : win.pageXOffset, top ? val : win.pageYOffset);
        } else {
          elem[method] = val;
        }
      }, method, val, arguments.length);
    };
  });
  jQuery.each(["top", "left"], function (_i, prop) {
    jQuery.cssHooks[prop] = addGetHookIf(support.pixelPosition, function (elem, computed) {
      if (computed) {
        computed = curCSS(elem, prop);
        return rnumnonpx.test(computed) ? jQuery(elem).position()[prop] + "px" : computed;
      }
    });
  });
  jQuery.each({
    Height: "height",
    Width: "width"
  }, function (name, type) {
    jQuery.each({
      padding: "inner" + name,
      content: type,
      "": "outer" + name
    }, function (defaultExtra, funcName) {
      jQuery.fn[funcName] = function (margin, value) {
        var chainable = arguments.length && (defaultExtra || typeof margin !== "boolean"),
            extra = defaultExtra || (margin === true || value === true ? "margin" : "border");
        return access(this, function (elem, type, value) {
          var doc;

          if (isWindow(elem)) {
            return funcName.indexOf("outer") === 0 ? elem["inner" + name] : elem.document.documentElement["client" + name];
          }

          if (elem.nodeType === 9) {
            doc = elem.documentElement;
            return Math.max(elem.body["scroll" + name], doc["scroll" + name], elem.body["offset" + name], doc["offset" + name], doc["client" + name]);
          }

          return value === undefined ? jQuery.css(elem, type, extra) : jQuery.style(elem, type, value, extra);
        }, type, chainable ? margin : undefined, chainable);
      };
    });
  });
  jQuery.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (_i, type) {
    jQuery.fn[type] = function (fn) {
      return this.on(type, fn);
    };
  });
  jQuery.fn.extend({
    bind: function bind(types, data, fn) {
      return this.on(types, null, data, fn);
    },
    unbind: function unbind(types, fn) {
      return this.off(types, null, fn);
    },
    delegate: function delegate(selector, types, data, fn) {
      return this.on(types, selector, data, fn);
    },
    undelegate: function undelegate(selector, types, fn) {
      return arguments.length === 1 ? this.off(selector, "**") : this.off(types, selector || "**", fn);
    },
    hover: function hover(fnOver, fnOut) {
      return this.mouseenter(fnOver).mouseleave(fnOut || fnOver);
    }
  });
  jQuery.each(("blur focus focusin focusout resize scroll click dblclick " + "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " + "change select submit keydown keypress keyup contextmenu").split(" "), function (_i, name) {
    jQuery.fn[name] = function (data, fn) {
      return arguments.length > 0 ? this.on(name, null, data, fn) : this.trigger(name);
    };
  });
  var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

  jQuery.proxy = function (fn, context) {
    var tmp, args, proxy;

    if (typeof context === "string") {
      tmp = fn[context];
      context = fn;
      fn = tmp;
    }

    if (!isFunction(fn)) {
      return undefined;
    }

    args = _slice.call(arguments, 2);

    proxy = function proxy() {
      return fn.apply(context || this, args.concat(_slice.call(arguments)));
    };

    proxy.guid = fn.guid = fn.guid || jQuery.guid++;
    return proxy;
  };

  jQuery.holdReady = function (hold) {
    if (hold) {
      jQuery.readyWait++;
    } else {
      jQuery.ready(true);
    }
  };

  jQuery.isArray = Array.isArray;
  jQuery.parseJSON = JSON.parse;
  jQuery.nodeName = nodeName;
  jQuery.isFunction = isFunction;
  jQuery.isWindow = isWindow;
  jQuery.camelCase = camelCase;
  jQuery.type = toType;
  jQuery.now = Date.now;

  jQuery.isNumeric = function (obj) {
    var type = jQuery.type(obj);
    return (type === "number" || type === "string") && !isNaN(obj - parseFloat(obj));
  };

  jQuery.trim = function (text) {
    return text == null ? "" : (text + "").replace(rtrim, "");
  };

  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_LOCAL_MODULE_0__ = ((function () {
      return jQuery;
    }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)));
  }

  var _jQuery = window.jQuery,
      _$ = window.$;

  jQuery.noConflict = function (deep) {
    if (window.$ === jQuery) {
      window.$ = _$;
    }

    if (deep && window.jQuery === jQuery) {
      window.jQuery = _jQuery;
    }

    return jQuery;
  };

  if (typeof noGlobal === "undefined") {
    window.jQuery = window.$ = jQuery;
  }

  return jQuery;
});

jQuery.noConflict();
;
/*!
 * jQuery Migrate - v3.3.2 - 2020-11-18T08:29Z
 * Copyright OpenJS Foundation and other contributors
 */

(function (factory) {
  "use strict";

  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_0__], __WEBPACK_AMD_DEFINE_RESULT__ = (function (jQuery) {
      return factory(jQuery, window);
    }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
})(function (jQuery, window) {
  "use strict";

  jQuery.migrateVersion = "3.3.2";

  function compareVersions(v1, v2) {
    var i,
        rVersionParts = /^(\d+)\.(\d+)\.(\d+)/,
        v1p = rVersionParts.exec(v1) || [],
        v2p = rVersionParts.exec(v2) || [];

    for (i = 1; i <= 3; i++) {
      if (+v1p[i] > +v2p[i]) {
        return 1;
      }

      if (+v1p[i] < +v2p[i]) {
        return -1;
      }
    }

    return 0;
  }

  function jQueryVersionSince(version) {
    return compareVersions(jQuery.fn.jquery, version) >= 0;
  }

  (function () {
    if (!window.console || !window.console.log) {
      return;
    }

    if (!jQuery || !jQueryVersionSince("3.0.0")) {
      window.console.log("JQMIGRATE: jQuery 3.0.0+ REQUIRED");
    }

    if (jQuery.migrateWarnings) {
      window.console.log("JQMIGRATE: Migrate plugin loaded multiple times");
    }

    window.console.log("JQMIGRATE: Migrate is installed" + (jQuery.migrateMute ? "" : " with logging active") + ", version " + jQuery.migrateVersion);
  })();

  var warnedAbout = {};
  jQuery.migrateDeduplicateWarnings = true;
  jQuery.migrateWarnings = [];

  if (jQuery.migrateTrace === undefined) {
    jQuery.migrateTrace = true;
  }

  jQuery.migrateReset = function () {
    warnedAbout = {};
    jQuery.migrateWarnings.length = 0;
  };

  function migrateWarn(msg) {
    var console = window.console;

    if (!jQuery.migrateDeduplicateWarnings || !warnedAbout[msg]) {
      warnedAbout[msg] = true;
      jQuery.migrateWarnings.push(msg);

      if (console && console.warn && !jQuery.migrateMute) {
        console.warn("JQMIGRATE: " + msg);

        if (jQuery.migrateTrace && console.trace) {
          console.trace();
        }
      }
    }
  }

  function migrateWarnProp(obj, prop, value, msg) {
    Object.defineProperty(obj, prop, {
      configurable: true,
      enumerable: true,
      get: function get() {
        migrateWarn(msg);
        return value;
      },
      set: function set(newValue) {
        migrateWarn(msg);
        value = newValue;
      }
    });
  }

  function migrateWarnFunc(obj, prop, newFunc, msg) {
    obj[prop] = function () {
      migrateWarn(msg);
      return newFunc.apply(this, arguments);
    };
  }

  if (window.document.compatMode === "BackCompat") {
    migrateWarn("jQuery is not compatible with Quirks Mode");
  }

  var findProp,
      class2type = {},
      oldInit = jQuery.fn.init,
      oldFind = jQuery.find,
      rattrHashTest = /\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/,
      rattrHashGlob = /\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/g,
      rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

  jQuery.fn.init = function (arg1) {
    var args = Array.prototype.slice.call(arguments);

    if (typeof arg1 === "string" && arg1 === "#") {
      migrateWarn("jQuery( '#' ) is not a valid selector");
      args[0] = [];
    }

    return oldInit.apply(this, args);
  };

  jQuery.fn.init.prototype = jQuery.fn;

  jQuery.find = function (selector) {
    var args = Array.prototype.slice.call(arguments);

    if (typeof selector === "string" && rattrHashTest.test(selector)) {
      try {
        window.document.querySelector(selector);
      } catch (err1) {
        selector = selector.replace(rattrHashGlob, function (_, attr, op, value) {
          return "[" + attr + op + "\"" + value + "\"]";
        });

        try {
          window.document.querySelector(selector);
          migrateWarn("Attribute selector with '#' must be quoted: " + args[0]);
          args[0] = selector;
        } catch (err2) {
          migrateWarn("Attribute selector with '#' was not fixed: " + args[0]);
        }
      }
    }

    return oldFind.apply(this, args);
  };

  for (findProp in oldFind) {
    if (Object.prototype.hasOwnProperty.call(oldFind, findProp)) {
      jQuery.find[findProp] = oldFind[findProp];
    }
  }

  migrateWarnFunc(jQuery.fn, "size", function () {
    return this.length;
  }, "jQuery.fn.size() is deprecated and removed; use the .length property");
  migrateWarnFunc(jQuery, "parseJSON", function () {
    return JSON.parse.apply(null, arguments);
  }, "jQuery.parseJSON is deprecated; use JSON.parse");
  migrateWarnFunc(jQuery, "holdReady", jQuery.holdReady, "jQuery.holdReady is deprecated");
  migrateWarnFunc(jQuery, "unique", jQuery.uniqueSort, "jQuery.unique is deprecated; use jQuery.uniqueSort");
  migrateWarnProp(jQuery.expr, "filters", jQuery.expr.pseudos, "jQuery.expr.filters is deprecated; use jQuery.expr.pseudos");
  migrateWarnProp(jQuery.expr, ":", jQuery.expr.pseudos, "jQuery.expr[':'] is deprecated; use jQuery.expr.pseudos");

  if (jQueryVersionSince("3.1.1")) {
    migrateWarnFunc(jQuery, "trim", function (text) {
      return text == null ? "" : (text + "").replace(rtrim, "");
    }, "jQuery.trim is deprecated; use String.prototype.trim");
  }

  if (jQueryVersionSince("3.2.0")) {
    migrateWarnFunc(jQuery, "nodeName", function (elem, name) {
      return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
    }, "jQuery.nodeName is deprecated");
    migrateWarnFunc(jQuery, "isArray", Array.isArray, "jQuery.isArray is deprecated; use Array.isArray");
  }

  if (jQueryVersionSince("3.3.0")) {
    migrateWarnFunc(jQuery, "isNumeric", function (obj) {
      var type = _typeof(obj);

      return (type === "number" || type === "string") && !isNaN(obj - parseFloat(obj));
    }, "jQuery.isNumeric() is deprecated");
    jQuery.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (_, name) {
      class2type["[object " + name + "]"] = name.toLowerCase();
    });
    migrateWarnFunc(jQuery, "type", function (obj) {
      if (obj == null) {
        return obj + "";
      }

      return _typeof(obj) === "object" || typeof obj === "function" ? class2type[Object.prototype.toString.call(obj)] || "object" : _typeof(obj);
    }, "jQuery.type is deprecated");
    migrateWarnFunc(jQuery, "isFunction", function (obj) {
      return typeof obj === "function";
    }, "jQuery.isFunction() is deprecated");
    migrateWarnFunc(jQuery, "isWindow", function (obj) {
      return obj != null && obj === obj.window;
    }, "jQuery.isWindow() is deprecated");
  }

  if (jQuery.ajax) {
    var oldAjax = jQuery.ajax,
        rjsonp = /(=)\?(?=&|$)|\?\?/;

    jQuery.ajax = function () {
      var jQXHR = oldAjax.apply(this, arguments);

      if (jQXHR.promise) {
        migrateWarnFunc(jQXHR, "success", jQXHR.done, "jQXHR.success is deprecated and removed");
        migrateWarnFunc(jQXHR, "error", jQXHR.fail, "jQXHR.error is deprecated and removed");
        migrateWarnFunc(jQXHR, "complete", jQXHR.always, "jQXHR.complete is deprecated and removed");
      }

      return jQXHR;
    };

    if (!jQueryVersionSince("4.0.0")) {
      jQuery.ajaxPrefilter("+json", function (s) {
        if (s.jsonp !== false && (rjsonp.test(s.url) || typeof s.data === "string" && (s.contentType || "").indexOf("application/x-www-form-urlencoded") === 0 && rjsonp.test(s.data))) {
          migrateWarn("JSON-to-JSONP auto-promotion is deprecated");
        }
      });
    }
  }

  var oldRemoveAttr = jQuery.fn.removeAttr,
      oldToggleClass = jQuery.fn.toggleClass,
      rmatchNonSpace = /\S+/g;

  jQuery.fn.removeAttr = function (name) {
    var self = this;
    jQuery.each(name.match(rmatchNonSpace), function (_i, attr) {
      if (jQuery.expr.match.bool.test(attr)) {
        migrateWarn("jQuery.fn.removeAttr no longer sets boolean properties: " + attr);
        self.prop(attr, false);
      }
    });
    return oldRemoveAttr.apply(this, arguments);
  };

  jQuery.fn.toggleClass = function (state) {
    if (state !== undefined && typeof state !== "boolean") {
      return oldToggleClass.apply(this, arguments);
    }

    migrateWarn("jQuery.fn.toggleClass( boolean ) is deprecated");
    return this.each(function () {
      var className = this.getAttribute && this.getAttribute("class") || "";

      if (className) {
        jQuery.data(this, "__className__", className);
      }

      if (this.setAttribute) {
        this.setAttribute("class", className || state === false ? "" : jQuery.data(this, "__className__") || "");
      }
    });
  };

  function camelCase(string) {
    return string.replace(/-([a-z])/g, function (_, letter) {
      return letter.toUpperCase();
    });
  }

  var oldFnCss,
      internalSwapCall = false,
      ralphaStart = /^[a-z]/,
      rautoPx = /^(?:Border(?:Top|Right|Bottom|Left)?(?:Width|)|(?:Margin|Padding)?(?:Top|Right|Bottom|Left)?|(?:Min|Max)?(?:Width|Height))$/;

  if (jQuery.swap) {
    jQuery.each(["height", "width", "reliableMarginRight"], function (_, name) {
      var oldHook = jQuery.cssHooks[name] && jQuery.cssHooks[name].get;

      if (oldHook) {
        jQuery.cssHooks[name].get = function () {
          var ret;
          internalSwapCall = true;
          ret = oldHook.apply(this, arguments);
          internalSwapCall = false;
          return ret;
        };
      }
    });
  }

  jQuery.swap = function (elem, options, callback, args) {
    var ret,
        name,
        old = {};

    if (!internalSwapCall) {
      migrateWarn("jQuery.swap() is undocumented and deprecated");
    }

    for (name in options) {
      old[name] = elem.style[name];
      elem.style[name] = options[name];
    }

    ret = callback.apply(elem, args || []);

    for (name in options) {
      elem.style[name] = old[name];
    }

    return ret;
  };

  if (jQueryVersionSince("3.4.0") && typeof Proxy !== "undefined") {
    jQuery.cssProps = new Proxy(jQuery.cssProps || {}, {
      set: function set() {
        migrateWarn("JQMIGRATE: jQuery.cssProps is deprecated");
        return Reflect.set.apply(this, arguments);
      }
    });
  }

  if (!jQuery.cssNumber) {
    jQuery.cssNumber = {};
  }

  function isAutoPx(prop) {
    return ralphaStart.test(prop) && rautoPx.test(prop[0].toUpperCase() + prop.slice(1));
  }

  oldFnCss = jQuery.fn.css;

  jQuery.fn.css = function (name, value) {
    var camelName,
        origThis = this;

    if (name && _typeof(name) === "object" && !Array.isArray(name)) {
      jQuery.each(name, function (n, v) {
        jQuery.fn.css.call(origThis, n, v);
      });
      return this;
    }

    if (typeof value === "number") {
      camelName = camelCase(name);

      if (!isAutoPx(camelName) && !jQuery.cssNumber[camelName]) {
        migrateWarn("Number-typed values are deprecated for jQuery.fn.css( \"" + name + "\", value )");
      }
    }

    return oldFnCss.apply(this, arguments);
  };

  var oldData = jQuery.data;

  jQuery.data = function (elem, name, value) {
    var curData, sameKeys, key;

    if (name && _typeof(name) === "object" && arguments.length === 2) {
      curData = jQuery.hasData(elem) && oldData.call(this, elem);
      sameKeys = {};

      for (key in name) {
        if (key !== camelCase(key)) {
          migrateWarn("jQuery.data() always sets/gets camelCased names: " + key);
          curData[key] = name[key];
        } else {
          sameKeys[key] = name[key];
        }
      }

      oldData.call(this, elem, sameKeys);
      return name;
    }

    if (name && typeof name === "string" && name !== camelCase(name)) {
      curData = jQuery.hasData(elem) && oldData.call(this, elem);

      if (curData && name in curData) {
        migrateWarn("jQuery.data() always sets/gets camelCased names: " + name);

        if (arguments.length > 2) {
          curData[name] = value;
        }

        return curData[name];
      }
    }

    return oldData.apply(this, arguments);
  };

  if (jQuery.fx) {
    var intervalValue,
        intervalMsg,
        oldTweenRun = jQuery.Tween.prototype.run,
        linearEasing = function linearEasing(pct) {
      return pct;
    };

    jQuery.Tween.prototype.run = function () {
      if (jQuery.easing[this.easing].length > 1) {
        migrateWarn("'jQuery.easing." + this.easing.toString() + "' should use only one argument");
        jQuery.easing[this.easing] = linearEasing;
      }

      oldTweenRun.apply(this, arguments);
    };

    intervalValue = jQuery.fx.interval || 13;
    intervalMsg = "jQuery.fx.interval is deprecated";

    if (window.requestAnimationFrame) {
      Object.defineProperty(jQuery.fx, "interval", {
        configurable: true,
        enumerable: true,
        get: function get() {
          if (!window.document.hidden) {
            migrateWarn(intervalMsg);
          }

          return intervalValue;
        },
        set: function set(newValue) {
          migrateWarn(intervalMsg);
          intervalValue = newValue;
        }
      });
    }
  }

  var oldLoad = jQuery.fn.load,
      oldEventAdd = jQuery.event.add,
      originalFix = jQuery.event.fix;
  jQuery.event.props = [];
  jQuery.event.fixHooks = {};
  migrateWarnProp(jQuery.event.props, "concat", jQuery.event.props.concat, "jQuery.event.props.concat() is deprecated and removed");

  jQuery.event.fix = function (originalEvent) {
    var event,
        type = originalEvent.type,
        fixHook = this.fixHooks[type],
        props = jQuery.event.props;

    if (props.length) {
      migrateWarn("jQuery.event.props are deprecated and removed: " + props.join());

      while (props.length) {
        jQuery.event.addProp(props.pop());
      }
    }

    if (fixHook && !fixHook._migrated_) {
      fixHook._migrated_ = true;
      migrateWarn("jQuery.event.fixHooks are deprecated and removed: " + type);

      if ((props = fixHook.props) && props.length) {
        while (props.length) {
          jQuery.event.addProp(props.pop());
        }
      }
    }

    event = originalFix.call(this, originalEvent);
    return fixHook && fixHook.filter ? fixHook.filter(event, originalEvent) : event;
  };

  jQuery.event.add = function (elem, types) {
    if (elem === window && types === "load" && window.document.readyState === "complete") {
      migrateWarn("jQuery(window).on('load'...) called after load event occurred");
    }

    return oldEventAdd.apply(this, arguments);
  };

  jQuery.each(["load", "unload", "error"], function (_, name) {
    jQuery.fn[name] = function () {
      var args = Array.prototype.slice.call(arguments, 0);

      if (name === "load" && typeof args[0] === "string") {
        return oldLoad.apply(this, args);
      }

      migrateWarn("jQuery.fn." + name + "() is deprecated");
      args.splice(0, 0, name);

      if (arguments.length) {
        return this.on.apply(this, args);
      }

      this.triggerHandler.apply(this, args);
      return this;
    };
  });
  jQuery.each(("blur focus focusin focusout resize scroll click dblclick " + "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " + "change select submit keydown keypress keyup contextmenu").split(" "), function (_i, name) {
    jQuery.fn[name] = function (data, fn) {
      migrateWarn("jQuery.fn." + name + "() event shorthand is deprecated");
      return arguments.length > 0 ? this.on(name, null, data, fn) : this.trigger(name);
    };
  });
  jQuery(function () {
    jQuery(window.document).triggerHandler("ready");
  });
  jQuery.event.special.ready = {
    setup: function setup() {
      if (this === window.document) {
        migrateWarn("'ready' event is deprecated");
      }
    }
  };
  jQuery.fn.extend({
    bind: function bind(types, data, fn) {
      migrateWarn("jQuery.fn.bind() is deprecated");
      return this.on(types, null, data, fn);
    },
    unbind: function unbind(types, fn) {
      migrateWarn("jQuery.fn.unbind() is deprecated");
      return this.off(types, null, fn);
    },
    delegate: function delegate(selector, types, data, fn) {
      migrateWarn("jQuery.fn.delegate() is deprecated");
      return this.on(types, selector, data, fn);
    },
    undelegate: function undelegate(selector, types, fn) {
      migrateWarn("jQuery.fn.undelegate() is deprecated");
      return arguments.length === 1 ? this.off(selector, "**") : this.off(types, selector || "**", fn);
    },
    hover: function hover(fnOver, fnOut) {
      migrateWarn("jQuery.fn.hover() is deprecated");
      return this.on("mouseenter", fnOver).on("mouseleave", fnOut || fnOver);
    }
  });

  var rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
      origHtmlPrefilter = jQuery.htmlPrefilter,
      makeMarkup = function makeMarkup(html) {
    var doc = window.document.implementation.createHTMLDocument("");
    doc.body.innerHTML = html;
    return doc.body && doc.body.innerHTML;
  },
      warnIfChanged = function warnIfChanged(html) {
    var changed = html.replace(rxhtmlTag, "<$1></$2>");

    if (changed !== html && makeMarkup(html) !== makeMarkup(changed)) {
      migrateWarn("HTML tags must be properly nested and closed: " + html);
    }
  };

  jQuery.UNSAFE_restoreLegacyHtmlPrefilter = function () {
    jQuery.htmlPrefilter = function (html) {
      warnIfChanged(html);
      return html.replace(rxhtmlTag, "<$1></$2>");
    };
  };

  jQuery.htmlPrefilter = function (html) {
    warnIfChanged(html);
    return origHtmlPrefilter(html);
  };

  var oldOffset = jQuery.fn.offset;

  jQuery.fn.offset = function () {
    var elem = this[0];

    if (elem && (!elem.nodeType || !elem.getBoundingClientRect)) {
      migrateWarn("jQuery.fn.offset() requires a valid DOM element");
      return arguments.length ? this : undefined;
    }

    return oldOffset.apply(this, arguments);
  };

  if (jQuery.ajax) {
    var oldParam = jQuery.param;

    jQuery.param = function (data, traditional) {
      var ajaxTraditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;

      if (traditional === undefined && ajaxTraditional) {
        migrateWarn("jQuery.param() no longer uses jQuery.ajaxSettings.traditional");
        traditional = ajaxTraditional;
      }

      return oldParam.call(this, data, traditional);
    };
  }

  var oldSelf = jQuery.fn.andSelf || jQuery.fn.addBack;

  jQuery.fn.andSelf = function () {
    migrateWarn("jQuery.fn.andSelf() is deprecated and removed, use jQuery.fn.addBack()");
    return oldSelf.apply(this, arguments);
  };

  if (jQuery.Deferred) {
    var oldDeferred = jQuery.Deferred,
        tuples = [["resolve", "done", jQuery.Callbacks("once memory"), jQuery.Callbacks("once memory"), "resolved"], ["reject", "fail", jQuery.Callbacks("once memory"), jQuery.Callbacks("once memory"), "rejected"], ["notify", "progress", jQuery.Callbacks("memory"), jQuery.Callbacks("memory")]];

    jQuery.Deferred = function (func) {
      var deferred = oldDeferred(),
          promise = deferred.promise();

      deferred.pipe = promise.pipe = function () {
        var fns = arguments;
        migrateWarn("deferred.pipe() is deprecated");
        return jQuery.Deferred(function (newDefer) {
          jQuery.each(tuples, function (i, tuple) {
            var fn = typeof fns[i] === "function" && fns[i];
            deferred[tuple[1]](function () {
              var returned = fn && fn.apply(this, arguments);

              if (returned && typeof returned.promise === "function") {
                returned.promise().done(newDefer.resolve).fail(newDefer.reject).progress(newDefer.notify);
              } else {
                newDefer[tuple[0] + "With"](this === promise ? newDefer.promise() : this, fn ? [returned] : arguments);
              }
            });
          });
          fns = null;
        }).promise();
      };

      if (func) {
        func.call(deferred, deferred);
      }

      return deferred;
    };

    jQuery.Deferred.exceptionHook = oldDeferred.exceptionHook;
  }

  return jQuery;
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/module.js */ "./node_modules/webpack/buildin/module.js")(module)))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvZnJvbnRlbmQvMzNmYzEuanMiXSwibmFtZXMiOlsiZ2xvYmFsIiwiZmFjdG9yeSIsIm1vZHVsZSIsImV4cG9ydHMiLCJkb2N1bWVudCIsInciLCJFcnJvciIsIndpbmRvdyIsIm5vR2xvYmFsIiwiYXJyIiwiZ2V0UHJvdG8iLCJPYmplY3QiLCJnZXRQcm90b3R5cGVPZiIsInNsaWNlIiwiZmxhdCIsImFycmF5IiwiY2FsbCIsImNvbmNhdCIsImFwcGx5IiwicHVzaCIsImluZGV4T2YiLCJjbGFzczJ0eXBlIiwidG9TdHJpbmciLCJoYXNPd24iLCJoYXNPd25Qcm9wZXJ0eSIsImZuVG9TdHJpbmciLCJPYmplY3RGdW5jdGlvblN0cmluZyIsInN1cHBvcnQiLCJpc0Z1bmN0aW9uIiwib2JqIiwibm9kZVR5cGUiLCJpc1dpbmRvdyIsInByZXNlcnZlZFNjcmlwdEF0dHJpYnV0ZXMiLCJ0eXBlIiwic3JjIiwibm9uY2UiLCJub01vZHVsZSIsIkRPTUV2YWwiLCJjb2RlIiwibm9kZSIsImRvYyIsImkiLCJ2YWwiLCJzY3JpcHQiLCJjcmVhdGVFbGVtZW50IiwidGV4dCIsImdldEF0dHJpYnV0ZSIsInNldEF0dHJpYnV0ZSIsImhlYWQiLCJhcHBlbmRDaGlsZCIsInBhcmVudE5vZGUiLCJyZW1vdmVDaGlsZCIsInRvVHlwZSIsInZlcnNpb24iLCJqUXVlcnkiLCJzZWxlY3RvciIsImNvbnRleHQiLCJmbiIsImluaXQiLCJwcm90b3R5cGUiLCJqcXVlcnkiLCJjb25zdHJ1Y3RvciIsImxlbmd0aCIsInRvQXJyYXkiLCJnZXQiLCJudW0iLCJwdXNoU3RhY2siLCJlbGVtcyIsInJldCIsIm1lcmdlIiwicHJldk9iamVjdCIsImVhY2giLCJjYWxsYmFjayIsIm1hcCIsImVsZW0iLCJhcmd1bWVudHMiLCJmaXJzdCIsImVxIiwibGFzdCIsImV2ZW4iLCJncmVwIiwiX2VsZW0iLCJvZGQiLCJsZW4iLCJqIiwiZW5kIiwic29ydCIsInNwbGljZSIsImV4dGVuZCIsIm9wdGlvbnMiLCJuYW1lIiwiY29weSIsImNvcHlJc0FycmF5IiwiY2xvbmUiLCJ0YXJnZXQiLCJkZWVwIiwiaXNQbGFpbk9iamVjdCIsIkFycmF5IiwiaXNBcnJheSIsInVuZGVmaW5lZCIsImV4cGFuZG8iLCJNYXRoIiwicmFuZG9tIiwicmVwbGFjZSIsImlzUmVhZHkiLCJlcnJvciIsIm1zZyIsIm5vb3AiLCJwcm90byIsIkN0b3IiLCJpc0VtcHR5T2JqZWN0IiwiZ2xvYmFsRXZhbCIsImlzQXJyYXlMaWtlIiwibWFrZUFycmF5IiwicmVzdWx0cyIsImluQXJyYXkiLCJzZWNvbmQiLCJpbnZlcnQiLCJjYWxsYmFja0ludmVyc2UiLCJtYXRjaGVzIiwiY2FsbGJhY2tFeHBlY3QiLCJhcmciLCJ2YWx1ZSIsImd1aWQiLCJTeW1ib2wiLCJpdGVyYXRvciIsInNwbGl0IiwiX2kiLCJ0b0xvd2VyQ2FzZSIsIlNpenpsZSIsIkV4cHIiLCJnZXRUZXh0IiwiaXNYTUwiLCJ0b2tlbml6ZSIsImNvbXBpbGUiLCJzZWxlY3QiLCJvdXRlcm1vc3RDb250ZXh0Iiwic29ydElucHV0IiwiaGFzRHVwbGljYXRlIiwic2V0RG9jdW1lbnQiLCJkb2NFbGVtIiwiZG9jdW1lbnRJc0hUTUwiLCJyYnVnZ3lRU0EiLCJyYnVnZ3lNYXRjaGVzIiwiY29udGFpbnMiLCJEYXRlIiwicHJlZmVycmVkRG9jIiwiZGlycnVucyIsImRvbmUiLCJjbGFzc0NhY2hlIiwiY3JlYXRlQ2FjaGUiLCJ0b2tlbkNhY2hlIiwiY29tcGlsZXJDYWNoZSIsIm5vbm5hdGl2ZVNlbGVjdG9yQ2FjaGUiLCJzb3J0T3JkZXIiLCJhIiwiYiIsInBvcCIsInB1c2hOYXRpdmUiLCJsaXN0IiwiYm9vbGVhbnMiLCJ3aGl0ZXNwYWNlIiwiaWRlbnRpZmllciIsImF0dHJpYnV0ZXMiLCJwc2V1ZG9zIiwicndoaXRlc3BhY2UiLCJSZWdFeHAiLCJydHJpbSIsInJjb21tYSIsInJjb21iaW5hdG9ycyIsInJkZXNjZW5kIiwicnBzZXVkbyIsInJpZGVudGlmaWVyIiwibWF0Y2hFeHByIiwicmh0bWwiLCJyaW5wdXRzIiwicmhlYWRlciIsInJuYXRpdmUiLCJycXVpY2tFeHByIiwicnNpYmxpbmciLCJydW5lc2NhcGUiLCJmdW5lc2NhcGUiLCJlc2NhcGUiLCJub25IZXgiLCJoaWdoIiwiU3RyaW5nIiwiZnJvbUNoYXJDb2RlIiwicmNzc2VzY2FwZSIsImZjc3Nlc2NhcGUiLCJjaCIsImFzQ29kZVBvaW50IiwiY2hhckNvZGVBdCIsInVubG9hZEhhbmRsZXIiLCJpbkRpc2FibGVkRmllbGRzZXQiLCJhZGRDb21iaW5hdG9yIiwiZGlzYWJsZWQiLCJub2RlTmFtZSIsImRpciIsIm5leHQiLCJjaGlsZE5vZGVzIiwiZSIsImVscyIsInNlZWQiLCJtIiwibmlkIiwibWF0Y2giLCJncm91cHMiLCJuZXdTZWxlY3RvciIsIm5ld0NvbnRleHQiLCJvd25lckRvY3VtZW50IiwiZXhlYyIsImdldEVsZW1lbnRCeUlkIiwiaWQiLCJnZXRFbGVtZW50c0J5VGFnTmFtZSIsImdldEVsZW1lbnRzQnlDbGFzc05hbWUiLCJxc2EiLCJ0ZXN0IiwidGVzdENvbnRleHQiLCJzY29wZSIsInRvU2VsZWN0b3IiLCJqb2luIiwicXVlcnlTZWxlY3RvckFsbCIsInFzYUVycm9yIiwicmVtb3ZlQXR0cmlidXRlIiwia2V5cyIsImNhY2hlIiwia2V5IiwiY2FjaGVMZW5ndGgiLCJzaGlmdCIsIm1hcmtGdW5jdGlvbiIsImFzc2VydCIsImVsIiwiYWRkSGFuZGxlIiwiYXR0cnMiLCJoYW5kbGVyIiwiYXR0ckhhbmRsZSIsInNpYmxpbmdDaGVjayIsImN1ciIsImRpZmYiLCJzb3VyY2VJbmRleCIsIm5leHRTaWJsaW5nIiwiY3JlYXRlSW5wdXRQc2V1ZG8iLCJjcmVhdGVCdXR0b25Qc2V1ZG8iLCJjcmVhdGVEaXNhYmxlZFBzZXVkbyIsImlzRGlzYWJsZWQiLCJjcmVhdGVQb3NpdGlvbmFsUHNldWRvIiwiYXJndW1lbnQiLCJtYXRjaEluZGV4ZXMiLCJuYW1lc3BhY2UiLCJuYW1lc3BhY2VVUkkiLCJkb2N1bWVudEVsZW1lbnQiLCJoYXNDb21wYXJlIiwic3ViV2luZG93IiwiZGVmYXVsdFZpZXciLCJ0b3AiLCJhZGRFdmVudExpc3RlbmVyIiwiYXR0YWNoRXZlbnQiLCJjbGFzc05hbWUiLCJjcmVhdGVDb21tZW50IiwiZ2V0QnlJZCIsImdldEVsZW1lbnRzQnlOYW1lIiwiZmlsdGVyIiwiYXR0cklkIiwiZmluZCIsImdldEF0dHJpYnV0ZU5vZGUiLCJ0YWciLCJ0bXAiLCJpbnB1dCIsImlubmVySFRNTCIsIm1hdGNoZXNTZWxlY3RvciIsIndlYmtpdE1hdGNoZXNTZWxlY3RvciIsIm1vek1hdGNoZXNTZWxlY3RvciIsIm9NYXRjaGVzU2VsZWN0b3IiLCJtc01hdGNoZXNTZWxlY3RvciIsImRpc2Nvbm5lY3RlZE1hdGNoIiwiY29tcGFyZURvY3VtZW50UG9zaXRpb24iLCJhZG93biIsImJ1cCIsImNvbXBhcmUiLCJzb3J0RGV0YWNoZWQiLCJhdXAiLCJhcCIsImJwIiwidW5zaGlmdCIsImV4cHIiLCJlbGVtZW50cyIsImF0dHIiLCJzcGVjaWZpZWQiLCJzZWwiLCJ1bmlxdWVTb3J0IiwiZHVwbGljYXRlcyIsImRldGVjdER1cGxpY2F0ZXMiLCJzb3J0U3RhYmxlIiwidGV4dENvbnRlbnQiLCJmaXJzdENoaWxkIiwibm9kZVZhbHVlIiwic2VsZWN0b3JzIiwiY3JlYXRlUHNldWRvIiwicmVsYXRpdmUiLCJwcmVGaWx0ZXIiLCJleGNlc3MiLCJ1bnF1b3RlZCIsIm5vZGVOYW1lU2VsZWN0b3IiLCJwYXR0ZXJuIiwib3BlcmF0b3IiLCJjaGVjayIsInJlc3VsdCIsIndoYXQiLCJfYXJndW1lbnQiLCJzaW1wbGUiLCJmb3J3YXJkIiwib2ZUeXBlIiwiX2NvbnRleHQiLCJ4bWwiLCJ1bmlxdWVDYWNoZSIsIm91dGVyQ2FjaGUiLCJub2RlSW5kZXgiLCJzdGFydCIsInBhcmVudCIsInVzZUNhY2hlIiwibGFzdENoaWxkIiwidW5pcXVlSUQiLCJwc2V1ZG8iLCJhcmdzIiwic2V0RmlsdGVycyIsImlkeCIsIm1hdGNoZWQiLCJtYXRjaGVyIiwidW5tYXRjaGVkIiwibGFuZyIsImVsZW1MYW5nIiwiaGFzaCIsImxvY2F0aW9uIiwiYWN0aXZlRWxlbWVudCIsImhhc0ZvY3VzIiwiaHJlZiIsInRhYkluZGV4IiwiY2hlY2tlZCIsInNlbGVjdGVkIiwic2VsZWN0ZWRJbmRleCIsIl9tYXRjaEluZGV4ZXMiLCJyYWRpbyIsImNoZWNrYm94IiwiZmlsZSIsInBhc3N3b3JkIiwiaW1hZ2UiLCJzdWJtaXQiLCJyZXNldCIsImZpbHRlcnMiLCJwYXJzZU9ubHkiLCJ0b2tlbnMiLCJzb0ZhciIsInByZUZpbHRlcnMiLCJjYWNoZWQiLCJjb21iaW5hdG9yIiwiYmFzZSIsInNraXAiLCJjaGVja05vbkVsZW1lbnRzIiwiZG9uZU5hbWUiLCJvbGRDYWNoZSIsIm5ld0NhY2hlIiwiZWxlbWVudE1hdGNoZXIiLCJtYXRjaGVycyIsIm11bHRpcGxlQ29udGV4dHMiLCJjb250ZXh0cyIsImNvbmRlbnNlIiwibmV3VW5tYXRjaGVkIiwibWFwcGVkIiwic2V0TWF0Y2hlciIsInBvc3RGaWx0ZXIiLCJwb3N0RmluZGVyIiwicG9zdFNlbGVjdG9yIiwidGVtcCIsInByZU1hcCIsInBvc3RNYXAiLCJwcmVleGlzdGluZyIsIm1hdGNoZXJJbiIsIm1hdGNoZXJPdXQiLCJtYXRjaGVyRnJvbVRva2VucyIsImNoZWNrQ29udGV4dCIsImxlYWRpbmdSZWxhdGl2ZSIsImltcGxpY2l0UmVsYXRpdmUiLCJtYXRjaENvbnRleHQiLCJtYXRjaEFueUNvbnRleHQiLCJtYXRjaGVyRnJvbUdyb3VwTWF0Y2hlcnMiLCJlbGVtZW50TWF0Y2hlcnMiLCJzZXRNYXRjaGVycyIsImJ5U2V0IiwiYnlFbGVtZW50Iiwic3VwZXJNYXRjaGVyIiwib3V0ZXJtb3N0IiwibWF0Y2hlZENvdW50Iiwic2V0TWF0Y2hlZCIsImNvbnRleHRCYWNrdXAiLCJkaXJydW5zVW5pcXVlIiwidG9rZW4iLCJjb21waWxlZCIsIl9uYW1lIiwiZGVmYXVsdFZhbHVlIiwidW5pcXVlIiwiaXNYTUxEb2MiLCJlc2NhcGVTZWxlY3RvciIsInVudGlsIiwidHJ1bmNhdGUiLCJpcyIsInNpYmxpbmdzIiwibiIsInJuZWVkc0NvbnRleHQiLCJuZWVkc0NvbnRleHQiLCJyc2luZ2xlVGFnIiwid2lubm93IiwicXVhbGlmaWVyIiwibm90Iiwic2VsZiIsInJvb3RqUXVlcnkiLCJyb290IiwicGFyc2VIVE1MIiwicmVhZHkiLCJycGFyZW50c3ByZXYiLCJndWFyYW50ZWVkVW5pcXVlIiwiY2hpbGRyZW4iLCJjb250ZW50cyIsInByZXYiLCJoYXMiLCJ0YXJnZXRzIiwibCIsImNsb3Nlc3QiLCJpbmRleCIsInByZXZBbGwiLCJhZGQiLCJhZGRCYWNrIiwic2libGluZyIsInBhcmVudHMiLCJwYXJlbnRzVW50aWwiLCJuZXh0QWxsIiwibmV4dFVudGlsIiwicHJldlVudGlsIiwiY29udGVudERvY3VtZW50IiwiY29udGVudCIsInJldmVyc2UiLCJybm90aHRtbHdoaXRlIiwiY3JlYXRlT3B0aW9ucyIsIm9iamVjdCIsIl8iLCJmbGFnIiwiQ2FsbGJhY2tzIiwiZmlyaW5nIiwibWVtb3J5IiwiZmlyZWQiLCJsb2NrZWQiLCJxdWV1ZSIsImZpcmluZ0luZGV4IiwiZmlyZSIsIm9uY2UiLCJzdG9wT25GYWxzZSIsInJlbW92ZSIsImVtcHR5IiwiZGlzYWJsZSIsImxvY2siLCJmaXJlV2l0aCIsIklkZW50aXR5IiwidiIsIlRocm93ZXIiLCJleCIsImFkb3B0VmFsdWUiLCJyZXNvbHZlIiwicmVqZWN0Iiwibm9WYWx1ZSIsIm1ldGhvZCIsInByb21pc2UiLCJmYWlsIiwidGhlbiIsIkRlZmVycmVkIiwiZnVuYyIsInR1cGxlcyIsInN0YXRlIiwiYWx3YXlzIiwiZGVmZXJyZWQiLCJwaXBlIiwiZm5zIiwibmV3RGVmZXIiLCJ0dXBsZSIsInJldHVybmVkIiwicHJvZ3Jlc3MiLCJub3RpZnkiLCJvbkZ1bGZpbGxlZCIsIm9uUmVqZWN0ZWQiLCJvblByb2dyZXNzIiwibWF4RGVwdGgiLCJkZXB0aCIsInNwZWNpYWwiLCJ0aGF0IiwibWlnaHRUaHJvdyIsIlR5cGVFcnJvciIsIm5vdGlmeVdpdGgiLCJyZXNvbHZlV2l0aCIsInByb2Nlc3MiLCJleGNlcHRpb25Ib29rIiwic3RhY2tUcmFjZSIsInJlamVjdFdpdGgiLCJnZXRTdGFja0hvb2siLCJzZXRUaW1lb3V0Iiwic3RhdGVTdHJpbmciLCJ3aGVuIiwic2luZ2xlVmFsdWUiLCJyZW1haW5pbmciLCJyZXNvbHZlQ29udGV4dHMiLCJyZXNvbHZlVmFsdWVzIiwibWFzdGVyIiwidXBkYXRlRnVuYyIsInJlcnJvck5hbWVzIiwic3RhY2siLCJjb25zb2xlIiwid2FybiIsIm1lc3NhZ2UiLCJyZWFkeUV4Y2VwdGlvbiIsInJlYWR5TGlzdCIsInJlYWR5V2FpdCIsIndhaXQiLCJjb21wbGV0ZWQiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwicmVhZHlTdGF0ZSIsImRvU2Nyb2xsIiwiYWNjZXNzIiwiY2hhaW5hYmxlIiwiZW1wdHlHZXQiLCJyYXciLCJidWxrIiwiX2tleSIsInJtc1ByZWZpeCIsInJkYXNoQWxwaGEiLCJmY2FtZWxDYXNlIiwiX2FsbCIsImxldHRlciIsInRvVXBwZXJDYXNlIiwiY2FtZWxDYXNlIiwic3RyaW5nIiwiYWNjZXB0RGF0YSIsIm93bmVyIiwiRGF0YSIsInVpZCIsImRlZmluZVByb3BlcnR5IiwiY29uZmlndXJhYmxlIiwic2V0IiwiZGF0YSIsInByb3AiLCJoYXNEYXRhIiwiZGF0YVByaXYiLCJkYXRhVXNlciIsInJicmFjZSIsInJtdWx0aURhc2giLCJnZXREYXRhIiwiSlNPTiIsInBhcnNlIiwiZGF0YUF0dHIiLCJyZW1vdmVEYXRhIiwiX2RhdGEiLCJfcmVtb3ZlRGF0YSIsImRlcXVldWUiLCJzdGFydExlbmd0aCIsImhvb2tzIiwiX3F1ZXVlSG9va3MiLCJzdG9wIiwic2V0dGVyIiwiY2xlYXJRdWV1ZSIsImNvdW50IiwiZGVmZXIiLCJwbnVtIiwic291cmNlIiwicmNzc051bSIsImNzc0V4cGFuZCIsImlzQXR0YWNoZWQiLCJjb21wb3NlZCIsImdldFJvb3ROb2RlIiwiaXNIaWRkZW5XaXRoaW5UcmVlIiwic3R5bGUiLCJkaXNwbGF5IiwiY3NzIiwiYWRqdXN0Q1NTIiwidmFsdWVQYXJ0cyIsInR3ZWVuIiwiYWRqdXN0ZWQiLCJzY2FsZSIsIm1heEl0ZXJhdGlvbnMiLCJjdXJyZW50VmFsdWUiLCJpbml0aWFsIiwidW5pdCIsImNzc051bWJlciIsImluaXRpYWxJblVuaXQiLCJkZWZhdWx0RGlzcGxheU1hcCIsImdldERlZmF1bHREaXNwbGF5IiwiYm9keSIsInNob3dIaWRlIiwic2hvdyIsInZhbHVlcyIsImhpZGUiLCJ0b2dnbGUiLCJyY2hlY2thYmxlVHlwZSIsInJ0YWdOYW1lIiwicnNjcmlwdFR5cGUiLCJmcmFnbWVudCIsImNyZWF0ZURvY3VtZW50RnJhZ21lbnQiLCJkaXYiLCJjaGVja0Nsb25lIiwiY2xvbmVOb2RlIiwibm9DbG9uZUNoZWNrZWQiLCJvcHRpb24iLCJ3cmFwTWFwIiwidGhlYWQiLCJjb2wiLCJ0ciIsInRkIiwiX2RlZmF1bHQiLCJ0Ym9keSIsInRmb290IiwiY29sZ3JvdXAiLCJjYXB0aW9uIiwidGgiLCJvcHRncm91cCIsImdldEFsbCIsInNldEdsb2JhbEV2YWwiLCJyZWZFbGVtZW50cyIsImJ1aWxkRnJhZ21lbnQiLCJzY3JpcHRzIiwic2VsZWN0aW9uIiwiaWdub3JlZCIsIndyYXAiLCJhdHRhY2hlZCIsIm5vZGVzIiwiY3JlYXRlVGV4dE5vZGUiLCJodG1sUHJlZmlsdGVyIiwicmtleUV2ZW50Iiwicm1vdXNlRXZlbnQiLCJydHlwZW5hbWVzcGFjZSIsInJldHVyblRydWUiLCJyZXR1cm5GYWxzZSIsImV4cGVjdFN5bmMiLCJzYWZlQWN0aXZlRWxlbWVudCIsImVyciIsIm9uIiwidHlwZXMiLCJvbmUiLCJvcmlnRm4iLCJldmVudCIsIm9mZiIsImhhbmRsZU9iakluIiwiZXZlbnRIYW5kbGUiLCJldmVudHMiLCJ0IiwiaGFuZGxlT2JqIiwiaGFuZGxlcnMiLCJuYW1lc3BhY2VzIiwib3JpZ1R5cGUiLCJlbGVtRGF0YSIsImNyZWF0ZSIsImhhbmRsZSIsInRyaWdnZXJlZCIsImRpc3BhdGNoIiwiZGVsZWdhdGVUeXBlIiwiYmluZFR5cGUiLCJkZWxlZ2F0ZUNvdW50Iiwic2V0dXAiLCJtYXBwZWRUeXBlcyIsIm9yaWdDb3VudCIsInRlYXJkb3duIiwicmVtb3ZlRXZlbnQiLCJuYXRpdmVFdmVudCIsImhhbmRsZXJRdWV1ZSIsImZpeCIsImRlbGVnYXRlVGFyZ2V0IiwicHJlRGlzcGF0Y2giLCJpc1Byb3BhZ2F0aW9uU3RvcHBlZCIsImN1cnJlbnRUYXJnZXQiLCJpc0ltbWVkaWF0ZVByb3BhZ2F0aW9uU3RvcHBlZCIsInJuYW1lc3BhY2UiLCJwcmV2ZW50RGVmYXVsdCIsInN0b3BQcm9wYWdhdGlvbiIsInBvc3REaXNwYXRjaCIsIm1hdGNoZWRIYW5kbGVycyIsIm1hdGNoZWRTZWxlY3RvcnMiLCJidXR0b24iLCJhZGRQcm9wIiwiaG9vayIsIkV2ZW50IiwiZW51bWVyYWJsZSIsIm9yaWdpbmFsRXZlbnQiLCJ3cml0YWJsZSIsImxvYWQiLCJub0J1YmJsZSIsImNsaWNrIiwibGV2ZXJhZ2VOYXRpdmUiLCJ0cmlnZ2VyIiwiYmVmb3JldW5sb2FkIiwicmV0dXJuVmFsdWUiLCJub3RBc3luYyIsInNhdmVkIiwiaXNUcmlnZ2VyIiwic3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uIiwicHJvcHMiLCJpc0RlZmF1bHRQcmV2ZW50ZWQiLCJkZWZhdWx0UHJldmVudGVkIiwicmVsYXRlZFRhcmdldCIsInRpbWVTdGFtcCIsIm5vdyIsImlzU2ltdWxhdGVkIiwiYWx0S2V5IiwiYnViYmxlcyIsImNhbmNlbGFibGUiLCJjaGFuZ2VkVG91Y2hlcyIsImN0cmxLZXkiLCJkZXRhaWwiLCJldmVudFBoYXNlIiwibWV0YUtleSIsInBhZ2VYIiwicGFnZVkiLCJzaGlmdEtleSIsInZpZXciLCJjaGFyQ29kZSIsImtleUNvZGUiLCJidXR0b25zIiwiY2xpZW50WCIsImNsaWVudFkiLCJvZmZzZXRYIiwib2Zmc2V0WSIsInBvaW50ZXJJZCIsInBvaW50ZXJUeXBlIiwic2NyZWVuWCIsInNjcmVlblkiLCJ0YXJnZXRUb3VjaGVzIiwidG9FbGVtZW50IiwidG91Y2hlcyIsIndoaWNoIiwiZm9jdXMiLCJibHVyIiwibW91c2VlbnRlciIsIm1vdXNlbGVhdmUiLCJwb2ludGVyZW50ZXIiLCJwb2ludGVybGVhdmUiLCJvcmlnIiwicmVsYXRlZCIsInJub0lubmVyaHRtbCIsInJjaGVja2VkIiwicmNsZWFuU2NyaXB0IiwibWFuaXB1bGF0aW9uVGFyZ2V0IiwiZGlzYWJsZVNjcmlwdCIsInJlc3RvcmVTY3JpcHQiLCJjbG9uZUNvcHlFdmVudCIsImRlc3QiLCJwZGF0YU9sZCIsInVkYXRhT2xkIiwidWRhdGFDdXIiLCJmaXhJbnB1dCIsImRvbU1hbmlwIiwiY29sbGVjdGlvbiIsImhhc1NjcmlwdHMiLCJpTm9DbG9uZSIsInZhbHVlSXNGdW5jdGlvbiIsImh0bWwiLCJfZXZhbFVybCIsImtlZXBEYXRhIiwiY2xlYW5EYXRhIiwiZGF0YUFuZEV2ZW50cyIsImRlZXBEYXRhQW5kRXZlbnRzIiwic3JjRWxlbWVudHMiLCJkZXN0RWxlbWVudHMiLCJpblBhZ2UiLCJkZXRhY2giLCJhcHBlbmQiLCJwcmVwZW5kIiwiaW5zZXJ0QmVmb3JlIiwiYmVmb3JlIiwiYWZ0ZXIiLCJyZXBsYWNlV2l0aCIsInJlcGxhY2VDaGlsZCIsImFwcGVuZFRvIiwicHJlcGVuZFRvIiwiaW5zZXJ0QWZ0ZXIiLCJyZXBsYWNlQWxsIiwib3JpZ2luYWwiLCJpbnNlcnQiLCJybnVtbm9ucHgiLCJnZXRTdHlsZXMiLCJvcGVuZXIiLCJnZXRDb21wdXRlZFN0eWxlIiwic3dhcCIsIm9sZCIsInJib3hTdHlsZSIsImNvbXB1dGVTdHlsZVRlc3RzIiwiY29udGFpbmVyIiwiY3NzVGV4dCIsImRpdlN0eWxlIiwicGl4ZWxQb3NpdGlvblZhbCIsInJlbGlhYmxlTWFyZ2luTGVmdFZhbCIsInJvdW5kUGl4ZWxNZWFzdXJlcyIsIm1hcmdpbkxlZnQiLCJyaWdodCIsInBpeGVsQm94U3R5bGVzVmFsIiwiYm94U2l6aW5nUmVsaWFibGVWYWwiLCJ3aWR0aCIsInBvc2l0aW9uIiwic2Nyb2xsYm94U2l6ZVZhbCIsIm9mZnNldFdpZHRoIiwibWVhc3VyZSIsInJvdW5kIiwicGFyc2VGbG9hdCIsInJlbGlhYmxlVHJEaW1lbnNpb25zVmFsIiwiYmFja2dyb3VuZENsaXAiLCJjbGVhckNsb25lU3R5bGUiLCJib3hTaXppbmdSZWxpYWJsZSIsInBpeGVsQm94U3R5bGVzIiwicGl4ZWxQb3NpdGlvbiIsInJlbGlhYmxlTWFyZ2luTGVmdCIsInNjcm9sbGJveFNpemUiLCJyZWxpYWJsZVRyRGltZW5zaW9ucyIsInRhYmxlIiwidHJDaGlsZCIsInRyU3R5bGUiLCJoZWlnaHQiLCJwYXJzZUludCIsImN1ckNTUyIsImNvbXB1dGVkIiwibWluV2lkdGgiLCJtYXhXaWR0aCIsImdldFByb3BlcnR5VmFsdWUiLCJhZGRHZXRIb29rSWYiLCJjb25kaXRpb25GbiIsImhvb2tGbiIsImNzc1ByZWZpeGVzIiwiZW1wdHlTdHlsZSIsInZlbmRvclByb3BzIiwidmVuZG9yUHJvcE5hbWUiLCJjYXBOYW1lIiwiZmluYWxQcm9wTmFtZSIsImZpbmFsIiwiY3NzUHJvcHMiLCJyZGlzcGxheXN3YXAiLCJyY3VzdG9tUHJvcCIsImNzc1Nob3ciLCJ2aXNpYmlsaXR5IiwiY3NzTm9ybWFsVHJhbnNmb3JtIiwibGV0dGVyU3BhY2luZyIsImZvbnRXZWlnaHQiLCJzZXRQb3NpdGl2ZU51bWJlciIsInN1YnRyYWN0IiwibWF4IiwiYm94TW9kZWxBZGp1c3RtZW50IiwiZGltZW5zaW9uIiwiYm94IiwiaXNCb3JkZXJCb3giLCJzdHlsZXMiLCJjb21wdXRlZFZhbCIsImV4dHJhIiwiZGVsdGEiLCJjZWlsIiwiZ2V0V2lkdGhPckhlaWdodCIsImJveFNpemluZ05lZWRlZCIsInZhbHVlSXNCb3JkZXJCb3giLCJvZmZzZXRQcm9wIiwiZ2V0Q2xpZW50UmVjdHMiLCJjc3NIb29rcyIsIm9wYWNpdHkiLCJvcmlnTmFtZSIsImlzQ3VzdG9tUHJvcCIsInNldFByb3BlcnR5IiwiaXNGaW5pdGUiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCJzY3JvbGxib3hTaXplQnVnZ3kiLCJsZWZ0IiwibWFyZ2luIiwicGFkZGluZyIsImJvcmRlciIsInByZWZpeCIsInN1ZmZpeCIsImV4cGFuZCIsImV4cGFuZGVkIiwicGFydHMiLCJUd2VlbiIsImVhc2luZyIsInByb3BIb29rcyIsInJ1biIsInBlcmNlbnQiLCJlYXNlZCIsImR1cmF0aW9uIiwicG9zIiwic3RlcCIsImZ4Iiwic2Nyb2xsVG9wIiwic2Nyb2xsTGVmdCIsImxpbmVhciIsInAiLCJzd2luZyIsImNvcyIsIlBJIiwiZnhOb3ciLCJpblByb2dyZXNzIiwicmZ4dHlwZXMiLCJycnVuIiwic2NoZWR1bGUiLCJoaWRkZW4iLCJyZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJpbnRlcnZhbCIsInRpY2siLCJjcmVhdGVGeE5vdyIsImdlbkZ4IiwiaW5jbHVkZVdpZHRoIiwiY3JlYXRlVHdlZW4iLCJhbmltYXRpb24iLCJBbmltYXRpb24iLCJ0d2VlbmVycyIsImRlZmF1bHRQcmVmaWx0ZXIiLCJvcHRzIiwib2xkZmlyZSIsInByb3BUd2VlbiIsInJlc3RvcmVEaXNwbGF5IiwiaXNCb3giLCJhbmltIiwiZGF0YVNob3ciLCJ1bnF1ZXVlZCIsIm92ZXJmbG93Iiwib3ZlcmZsb3dYIiwib3ZlcmZsb3dZIiwicHJvcEZpbHRlciIsInNwZWNpYWxFYXNpbmciLCJwcm9wZXJ0aWVzIiwic3RvcHBlZCIsInByZWZpbHRlcnMiLCJjdXJyZW50VGltZSIsInN0YXJ0VGltZSIsInR3ZWVucyIsIm9yaWdpbmFsUHJvcGVydGllcyIsIm9yaWdpbmFsT3B0aW9ucyIsImdvdG9FbmQiLCJiaW5kIiwiY29tcGxldGUiLCJ0aW1lciIsInR3ZWVuZXIiLCJwcmVmaWx0ZXIiLCJzcGVlZCIsIm9wdCIsInNwZWVkcyIsImZhZGVUbyIsInRvIiwiYW5pbWF0ZSIsIm9wdGFsbCIsImRvQW5pbWF0aW9uIiwiZmluaXNoIiwic3RvcFF1ZXVlIiwidGltZXJzIiwiY3NzRm4iLCJzbGlkZURvd24iLCJzbGlkZVVwIiwic2xpZGVUb2dnbGUiLCJmYWRlSW4iLCJmYWRlT3V0IiwiZmFkZVRvZ2dsZSIsInNsb3ciLCJmYXN0IiwiZGVsYXkiLCJ0aW1lIiwidGltZW91dCIsImNsZWFyVGltZW91dCIsImNoZWNrT24iLCJvcHRTZWxlY3RlZCIsInJhZGlvVmFsdWUiLCJib29sSG9vayIsInJlbW92ZUF0dHIiLCJuVHlwZSIsImF0dHJIb29rcyIsImJvb2wiLCJhdHRyTmFtZXMiLCJnZXR0ZXIiLCJsb3dlcmNhc2VOYW1lIiwicmZvY3VzYWJsZSIsInJjbGlja2FibGUiLCJyZW1vdmVQcm9wIiwicHJvcEZpeCIsInRhYmluZGV4Iiwic3RyaXBBbmRDb2xsYXBzZSIsImdldENsYXNzIiwiY2xhc3Nlc1RvQXJyYXkiLCJhZGRDbGFzcyIsImNsYXNzZXMiLCJjdXJWYWx1ZSIsImNsYXp6IiwiZmluYWxWYWx1ZSIsInJlbW92ZUNsYXNzIiwidG9nZ2xlQ2xhc3MiLCJzdGF0ZVZhbCIsImlzVmFsaWRWYWx1ZSIsImNsYXNzTmFtZXMiLCJoYXNDbGFzcyIsInJyZXR1cm4iLCJ2YWxIb29rcyIsIm9wdGlvblNldCIsImZvY3VzaW4iLCJyZm9jdXNNb3JwaCIsInN0b3BQcm9wYWdhdGlvbkNhbGxiYWNrIiwib25seUhhbmRsZXJzIiwiYnViYmxlVHlwZSIsIm9udHlwZSIsImxhc3RFbGVtZW50IiwiZXZlbnRQYXRoIiwicGFyZW50V2luZG93Iiwic2ltdWxhdGUiLCJ0cmlnZ2VySGFuZGxlciIsImF0dGFjaGVzIiwicnF1ZXJ5IiwicGFyc2VYTUwiLCJET01QYXJzZXIiLCJwYXJzZUZyb21TdHJpbmciLCJyYnJhY2tldCIsInJDUkxGIiwicnN1Ym1pdHRlclR5cGVzIiwicnN1Ym1pdHRhYmxlIiwiYnVpbGRQYXJhbXMiLCJ0cmFkaXRpb25hbCIsInBhcmFtIiwicyIsInZhbHVlT3JGdW5jdGlvbiIsImVuY29kZVVSSUNvbXBvbmVudCIsInNlcmlhbGl6ZSIsInNlcmlhbGl6ZUFycmF5IiwicjIwIiwicmhhc2giLCJyYW50aUNhY2hlIiwicmhlYWRlcnMiLCJybG9jYWxQcm90b2NvbCIsInJub0NvbnRlbnQiLCJycHJvdG9jb2wiLCJ0cmFuc3BvcnRzIiwiYWxsVHlwZXMiLCJvcmlnaW5BbmNob3IiLCJhZGRUb1ByZWZpbHRlcnNPclRyYW5zcG9ydHMiLCJzdHJ1Y3R1cmUiLCJkYXRhVHlwZUV4cHJlc3Npb24iLCJkYXRhVHlwZSIsImRhdGFUeXBlcyIsImluc3BlY3RQcmVmaWx0ZXJzT3JUcmFuc3BvcnRzIiwianFYSFIiLCJpbnNwZWN0ZWQiLCJzZWVraW5nVHJhbnNwb3J0IiwiaW5zcGVjdCIsInByZWZpbHRlck9yRmFjdG9yeSIsImRhdGFUeXBlT3JUcmFuc3BvcnQiLCJhamF4RXh0ZW5kIiwiZmxhdE9wdGlvbnMiLCJhamF4U2V0dGluZ3MiLCJhamF4SGFuZGxlUmVzcG9uc2VzIiwicmVzcG9uc2VzIiwiY3QiLCJmaW5hbERhdGFUeXBlIiwiZmlyc3REYXRhVHlwZSIsIm1pbWVUeXBlIiwiZ2V0UmVzcG9uc2VIZWFkZXIiLCJjb252ZXJ0ZXJzIiwiYWpheENvbnZlcnQiLCJyZXNwb25zZSIsImlzU3VjY2VzcyIsImNvbnYyIiwiY3VycmVudCIsImNvbnYiLCJyZXNwb25zZUZpZWxkcyIsImRhdGFGaWx0ZXIiLCJhY3RpdmUiLCJsYXN0TW9kaWZpZWQiLCJldGFnIiwidXJsIiwiaXNMb2NhbCIsInByb3RvY29sIiwicHJvY2Vzc0RhdGEiLCJhc3luYyIsImNvbnRlbnRUeXBlIiwiYWNjZXB0cyIsImpzb24iLCJhamF4U2V0dXAiLCJzZXR0aW5ncyIsImFqYXhQcmVmaWx0ZXIiLCJhamF4VHJhbnNwb3J0IiwiYWpheCIsInRyYW5zcG9ydCIsImNhY2hlVVJMIiwicmVzcG9uc2VIZWFkZXJzU3RyaW5nIiwicmVzcG9uc2VIZWFkZXJzIiwidGltZW91dFRpbWVyIiwidXJsQW5jaG9yIiwiZmlyZUdsb2JhbHMiLCJ1bmNhY2hlZCIsImNhbGxiYWNrQ29udGV4dCIsImdsb2JhbEV2ZW50Q29udGV4dCIsImNvbXBsZXRlRGVmZXJyZWQiLCJzdGF0dXNDb2RlIiwicmVxdWVzdEhlYWRlcnMiLCJyZXF1ZXN0SGVhZGVyc05hbWVzIiwic3RyQWJvcnQiLCJnZXRBbGxSZXNwb25zZUhlYWRlcnMiLCJzZXRSZXF1ZXN0SGVhZGVyIiwib3ZlcnJpZGVNaW1lVHlwZSIsInN0YXR1cyIsImFib3J0Iiwic3RhdHVzVGV4dCIsImZpbmFsVGV4dCIsImNyb3NzRG9tYWluIiwiaG9zdCIsImhhc0NvbnRlbnQiLCJpZk1vZGlmaWVkIiwiaGVhZGVycyIsImJlZm9yZVNlbmQiLCJzdWNjZXNzIiwic2VuZCIsIm5hdGl2ZVN0YXR1c1RleHQiLCJtb2RpZmllZCIsImdldEpTT04iLCJnZXRTY3JpcHQiLCJ3cmFwQWxsIiwiZmlyc3RFbGVtZW50Q2hpbGQiLCJ3cmFwSW5uZXIiLCJodG1sSXNGdW5jdGlvbiIsInVud3JhcCIsInZpc2libGUiLCJvZmZzZXRIZWlnaHQiLCJ4aHIiLCJYTUxIdHRwUmVxdWVzdCIsInhoclN1Y2Nlc3NTdGF0dXMiLCJ4aHJTdXBwb3J0ZWQiLCJjb3JzIiwiZXJyb3JDYWxsYmFjayIsIm9wZW4iLCJ1c2VybmFtZSIsInhockZpZWxkcyIsIm9ubG9hZCIsIm9uZXJyb3IiLCJvbmFib3J0Iiwib250aW1lb3V0Iiwib25yZWFkeXN0YXRlY2hhbmdlIiwicmVzcG9uc2VUeXBlIiwicmVzcG9uc2VUZXh0IiwiYmluYXJ5Iiwic2NyaXB0QXR0cnMiLCJjaGFyc2V0Iiwic2NyaXB0Q2hhcnNldCIsImV2dCIsIm9sZENhbGxiYWNrcyIsInJqc29ucCIsImpzb25wIiwianNvbnBDYWxsYmFjayIsIm9yaWdpbmFsU2V0dGluZ3MiLCJjYWxsYmFja05hbWUiLCJvdmVyd3JpdHRlbiIsInJlc3BvbnNlQ29udGFpbmVyIiwianNvblByb3AiLCJjcmVhdGVIVE1MRG9jdW1lbnQiLCJpbXBsZW1lbnRhdGlvbiIsImtlZXBTY3JpcHRzIiwicGFyc2VkIiwicGFyYW1zIiwiYW5pbWF0ZWQiLCJvZmZzZXQiLCJzZXRPZmZzZXQiLCJjdXJQb3NpdGlvbiIsImN1ckxlZnQiLCJjdXJDU1NUb3AiLCJjdXJUb3AiLCJjdXJPZmZzZXQiLCJjdXJDU1NMZWZ0IiwiY2FsY3VsYXRlUG9zaXRpb24iLCJjdXJFbGVtIiwidXNpbmciLCJyZWN0Iiwid2luIiwicGFnZVlPZmZzZXQiLCJwYWdlWE9mZnNldCIsIm9mZnNldFBhcmVudCIsInBhcmVudE9mZnNldCIsInNjcm9sbFRvIiwiSGVpZ2h0IiwiV2lkdGgiLCJkZWZhdWx0RXh0cmEiLCJmdW5jTmFtZSIsInVuYmluZCIsImRlbGVnYXRlIiwidW5kZWxlZ2F0ZSIsImhvdmVyIiwiZm5PdmVyIiwiZm5PdXQiLCJwcm94eSIsImhvbGRSZWFkeSIsImhvbGQiLCJwYXJzZUpTT04iLCJpc051bWVyaWMiLCJpc05hTiIsInRyaW0iLCJkZWZpbmUiLCJfalF1ZXJ5IiwiXyQiLCIkIiwibm9Db25mbGljdCIsIm1pZ3JhdGVWZXJzaW9uIiwiY29tcGFyZVZlcnNpb25zIiwidjEiLCJ2MiIsInJWZXJzaW9uUGFydHMiLCJ2MXAiLCJ2MnAiLCJqUXVlcnlWZXJzaW9uU2luY2UiLCJsb2ciLCJtaWdyYXRlV2FybmluZ3MiLCJtaWdyYXRlTXV0ZSIsIndhcm5lZEFib3V0IiwibWlncmF0ZURlZHVwbGljYXRlV2FybmluZ3MiLCJtaWdyYXRlVHJhY2UiLCJtaWdyYXRlUmVzZXQiLCJtaWdyYXRlV2FybiIsInRyYWNlIiwibWlncmF0ZVdhcm5Qcm9wIiwibmV3VmFsdWUiLCJtaWdyYXRlV2FybkZ1bmMiLCJuZXdGdW5jIiwiY29tcGF0TW9kZSIsImZpbmRQcm9wIiwib2xkSW5pdCIsIm9sZEZpbmQiLCJyYXR0ckhhc2hUZXN0IiwicmF0dHJIYXNoR2xvYiIsImFyZzEiLCJxdWVyeVNlbGVjdG9yIiwiZXJyMSIsIm9wIiwiZXJyMiIsIm9sZEFqYXgiLCJqUVhIUiIsIm9sZFJlbW92ZUF0dHIiLCJvbGRUb2dnbGVDbGFzcyIsInJtYXRjaE5vblNwYWNlIiwib2xkRm5Dc3MiLCJpbnRlcm5hbFN3YXBDYWxsIiwicmFscGhhU3RhcnQiLCJyYXV0b1B4Iiwib2xkSG9vayIsIlByb3h5IiwiUmVmbGVjdCIsImlzQXV0b1B4IiwiY2FtZWxOYW1lIiwib3JpZ1RoaXMiLCJvbGREYXRhIiwiY3VyRGF0YSIsInNhbWVLZXlzIiwiaW50ZXJ2YWxWYWx1ZSIsImludGVydmFsTXNnIiwib2xkVHdlZW5SdW4iLCJsaW5lYXJFYXNpbmciLCJwY3QiLCJvbGRMb2FkIiwib2xkRXZlbnRBZGQiLCJvcmlnaW5hbEZpeCIsImZpeEhvb2tzIiwiZml4SG9vayIsIl9taWdyYXRlZF8iLCJyeGh0bWxUYWciLCJvcmlnSHRtbFByZWZpbHRlciIsIm1ha2VNYXJrdXAiLCJ3YXJuSWZDaGFuZ2VkIiwiY2hhbmdlZCIsIlVOU0FGRV9yZXN0b3JlTGVnYWN5SHRtbFByZWZpbHRlciIsIm9sZE9mZnNldCIsIm9sZFBhcmFtIiwiYWpheFRyYWRpdGlvbmFsIiwib2xkU2VsZiIsImFuZFNlbGYiLCJvbGREZWZlcnJlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsVUFBU0EsTUFBVCxFQUFnQkMsT0FBaEIsRUFBd0I7QUFBQzs7QUFBYSxNQUFHLDhCQUFPQyxNQUFQLE9BQWdCLFFBQWhCLElBQTBCLFFBQU9BLE1BQU0sQ0FBQ0MsT0FBZCxNQUF3QixRQUFyRCxFQUE4RDtBQUFDRCxVQUFNLENBQUNDLE9BQVAsR0FBZUgsTUFBTSxDQUFDSSxRQUFQLEdBQWdCSCxPQUFPLENBQUNELE1BQUQsRUFBUSxJQUFSLENBQXZCLEdBQXFDLFVBQVNLLENBQVQsRUFBVztBQUFDLFVBQUcsQ0FBQ0EsQ0FBQyxDQUFDRCxRQUFOLEVBQWU7QUFBQyxjQUFNLElBQUlFLEtBQUosQ0FBVSwwQ0FBVixDQUFOO0FBQTZEOztBQUNuUCxhQUFPTCxPQUFPLENBQUNJLENBQUQsQ0FBZDtBQUFtQixLQURtRjtBQUNqRixHQURrQixNQUNkO0FBQUNKLFdBQU8sQ0FBQ0QsTUFBRCxDQUFQO0FBQWlCO0FBQUMsQ0FENUMsRUFDOEMsT0FBT08sTUFBUCxLQUFnQixXQUFoQixHQUE0QkEsTUFBNUIsR0FBbUMsSUFEakYsRUFDc0YsVUFBU0EsTUFBVCxFQUFnQkMsUUFBaEIsRUFBeUI7QUFBQzs7QUFBYSxNQUFJQyxHQUFHLEdBQUMsRUFBUjtBQUFXLE1BQUlDLFFBQVEsR0FBQ0MsTUFBTSxDQUFDQyxjQUFwQjtBQUFtQyxNQUFJQyxNQUFLLEdBQUNKLEdBQUcsQ0FBQ0ksS0FBZDtBQUFvQixNQUFJQyxJQUFJLEdBQUNMLEdBQUcsQ0FBQ0ssSUFBSixHQUFTLFVBQVNDLEtBQVQsRUFBZTtBQUFDLFdBQU9OLEdBQUcsQ0FBQ0ssSUFBSixDQUFTRSxJQUFULENBQWNELEtBQWQsQ0FBUDtBQUE2QixHQUF0RCxHQUF1RCxVQUFTQSxLQUFULEVBQWU7QUFBQyxXQUFPTixHQUFHLENBQUNRLE1BQUosQ0FBV0MsS0FBWCxDQUFpQixFQUFqQixFQUFvQkgsS0FBcEIsQ0FBUDtBQUFtQyxHQUFuSDtBQUFvSCxNQUFJSSxJQUFJLEdBQUNWLEdBQUcsQ0FBQ1UsSUFBYjtBQUFrQixNQUFJQyxPQUFPLEdBQUNYLEdBQUcsQ0FBQ1csT0FBaEI7QUFBd0IsTUFBSUMsVUFBVSxHQUFDLEVBQWY7QUFBa0IsTUFBSUMsUUFBUSxHQUFDRCxVQUFVLENBQUNDLFFBQXhCO0FBQWlDLE1BQUlDLE1BQU0sR0FBQ0YsVUFBVSxDQUFDRyxjQUF0QjtBQUFxQyxNQUFJQyxVQUFVLEdBQUNGLE1BQU0sQ0FBQ0QsUUFBdEI7QUFBK0IsTUFBSUksb0JBQW9CLEdBQUNELFVBQVUsQ0FBQ1QsSUFBWCxDQUFnQkwsTUFBaEIsQ0FBekI7QUFBaUQsTUFBSWdCLE9BQU8sR0FBQyxFQUFaOztBQUFlLE1BQUlDLFVBQVUsR0FBQyxTQUFTQSxVQUFULENBQW9CQyxHQUFwQixFQUF3QjtBQUFDLFdBQU8sT0FBT0EsR0FBUCxLQUFhLFVBQWIsSUFBeUIsT0FBT0EsR0FBRyxDQUFDQyxRQUFYLEtBQXNCLFFBQXREO0FBQWdFLEdBQXhHOztBQUF5RyxNQUFJQyxRQUFRLEdBQUMsU0FBU0EsUUFBVCxDQUFrQkYsR0FBbEIsRUFBc0I7QUFBQyxXQUFPQSxHQUFHLElBQUUsSUFBTCxJQUFXQSxHQUFHLEtBQUdBLEdBQUcsQ0FBQ3RCLE1BQTVCO0FBQW9DLEdBQXhFOztBQUF5RSxNQUFJSCxRQUFRLEdBQUNHLE1BQU0sQ0FBQ0gsUUFBcEI7QUFBNkIsTUFBSTRCLHlCQUF5QixHQUFDO0FBQUNDLFFBQUksRUFBQyxJQUFOO0FBQVdDLE9BQUcsRUFBQyxJQUFmO0FBQW9CQyxTQUFLLEVBQUMsSUFBMUI7QUFBK0JDLFlBQVEsRUFBQztBQUF4QyxHQUE5Qjs7QUFBNEUsV0FBU0MsT0FBVCxDQUFpQkMsSUFBakIsRUFBc0JDLElBQXRCLEVBQTJCQyxHQUEzQixFQUErQjtBQUFDQSxPQUFHLEdBQUNBLEdBQUcsSUFBRXBDLFFBQVQ7QUFBa0IsUUFBSXFDLENBQUo7QUFBQSxRQUFNQyxHQUFOO0FBQUEsUUFBVUMsTUFBTSxHQUFDSCxHQUFHLENBQUNJLGFBQUosQ0FBa0IsUUFBbEIsQ0FBakI7QUFBNkNELFVBQU0sQ0FBQ0UsSUFBUCxHQUFZUCxJQUFaOztBQUFpQixRQUFHQyxJQUFILEVBQVE7QUFBQyxXQUFJRSxDQUFKLElBQVNULHlCQUFULEVBQW1DO0FBQUNVLFdBQUcsR0FBQ0gsSUFBSSxDQUFDRSxDQUFELENBQUosSUFBU0YsSUFBSSxDQUFDTyxZQUFMLElBQW1CUCxJQUFJLENBQUNPLFlBQUwsQ0FBa0JMLENBQWxCLENBQWhDOztBQUFxRCxZQUFHQyxHQUFILEVBQU87QUFBQ0MsZ0JBQU0sQ0FBQ0ksWUFBUCxDQUFvQk4sQ0FBcEIsRUFBc0JDLEdBQXRCO0FBQTRCO0FBQUM7QUFBQzs7QUFDdmlDRixPQUFHLENBQUNRLElBQUosQ0FBU0MsV0FBVCxDQUFxQk4sTUFBckIsRUFBNkJPLFVBQTdCLENBQXdDQyxXQUF4QyxDQUFvRFIsTUFBcEQ7QUFBNkQ7O0FBQzdELFdBQVNTLE1BQVQsQ0FBZ0J2QixHQUFoQixFQUFvQjtBQUFDLFFBQUdBLEdBQUcsSUFBRSxJQUFSLEVBQWE7QUFBQyxhQUFPQSxHQUFHLEdBQUMsRUFBWDtBQUFlOztBQUNsRCxXQUFPLFFBQU9BLEdBQVAsTUFBYSxRQUFiLElBQXVCLE9BQU9BLEdBQVAsS0FBYSxVQUFwQyxHQUErQ1IsVUFBVSxDQUFDQyxRQUFRLENBQUNOLElBQVQsQ0FBY2EsR0FBZCxDQUFELENBQVYsSUFBZ0MsUUFBL0UsV0FBK0ZBLEdBQS9GLENBQVA7QUFBMkc7O0FBQzNHLE1BQ0F3QixPQUFPLEdBQUMsT0FEUjtBQUFBLE1BQ2dCQyxNQUFNLEdBQUMsU0FBUEEsTUFBTyxDQUFTQyxRQUFULEVBQWtCQyxPQUFsQixFQUEwQjtBQUFDLFdBQU8sSUFBSUYsTUFBTSxDQUFDRyxFQUFQLENBQVVDLElBQWQsQ0FBbUJILFFBQW5CLEVBQTRCQyxPQUE1QixDQUFQO0FBQTZDLEdBRC9GOztBQUNnR0YsUUFBTSxDQUFDRyxFQUFQLEdBQVVILE1BQU0sQ0FBQ0ssU0FBUCxHQUFpQjtBQUFDQyxVQUFNLEVBQUNQLE9BQVI7QUFBZ0JRLGVBQVcsRUFBQ1AsTUFBNUI7QUFBbUNRLFVBQU0sRUFBQyxDQUExQztBQUE0Q0MsV0FBTyxFQUFDLG1CQUFVO0FBQUMsYUFBT2xELE1BQUssQ0FBQ0csSUFBTixDQUFXLElBQVgsQ0FBUDtBQUF5QixLQUF4RjtBQUF5RmdELE9BQUcsRUFBQyxhQUFTQyxHQUFULEVBQWE7QUFBQyxVQUFHQSxHQUFHLElBQUUsSUFBUixFQUFhO0FBQUMsZUFBT3BELE1BQUssQ0FBQ0csSUFBTixDQUFXLElBQVgsQ0FBUDtBQUF5Qjs7QUFDN1EsYUFBT2lELEdBQUcsR0FBQyxDQUFKLEdBQU0sS0FBS0EsR0FBRyxHQUFDLEtBQUtILE1BQWQsQ0FBTixHQUE0QixLQUFLRyxHQUFMLENBQW5DO0FBQThDLEtBRDZFO0FBQzVFQyxhQUFTLEVBQUMsbUJBQVNDLEtBQVQsRUFBZTtBQUFDLFVBQUlDLEdBQUcsR0FBQ2QsTUFBTSxDQUFDZSxLQUFQLENBQWEsS0FBS1IsV0FBTCxFQUFiLEVBQWdDTSxLQUFoQyxDQUFSO0FBQStDQyxTQUFHLENBQUNFLFVBQUosR0FBZSxJQUFmO0FBQW9CLGFBQU9GLEdBQVA7QUFBWSxLQUQ3QjtBQUM4QkcsUUFBSSxFQUFDLGNBQVNDLFFBQVQsRUFBa0I7QUFBQyxhQUFPbEIsTUFBTSxDQUFDaUIsSUFBUCxDQUFZLElBQVosRUFBaUJDLFFBQWpCLENBQVA7QUFBbUMsS0FEekY7QUFDMEZDLE9BQUcsRUFBQyxhQUFTRCxRQUFULEVBQWtCO0FBQUMsYUFBTyxLQUFLTixTQUFMLENBQWVaLE1BQU0sQ0FBQ21CLEdBQVAsQ0FBVyxJQUFYLEVBQWdCLFVBQVNDLElBQVQsRUFBY2pDLENBQWQsRUFBZ0I7QUFBQyxlQUFPK0IsUUFBUSxDQUFDeEQsSUFBVCxDQUFjMEQsSUFBZCxFQUFtQmpDLENBQW5CLEVBQXFCaUMsSUFBckIsQ0FBUDtBQUFtQyxPQUFwRSxDQUFmLENBQVA7QUFBOEYsS0FEL007QUFDZ043RCxTQUFLLEVBQUMsaUJBQVU7QUFBQyxhQUFPLEtBQUtxRCxTQUFMLENBQWVyRCxNQUFLLENBQUNLLEtBQU4sQ0FBWSxJQUFaLEVBQWlCeUQsU0FBakIsQ0FBZixDQUFQO0FBQW9ELEtBRHJSO0FBQ3NSQyxTQUFLLEVBQUMsaUJBQVU7QUFBQyxhQUFPLEtBQUtDLEVBQUwsQ0FBUSxDQUFSLENBQVA7QUFBbUIsS0FEMVQ7QUFDMlRDLFFBQUksRUFBQyxnQkFBVTtBQUFDLGFBQU8sS0FBS0QsRUFBTCxDQUFRLENBQUMsQ0FBVCxDQUFQO0FBQW9CLEtBRC9WO0FBQ2dXRSxRQUFJLEVBQUMsZ0JBQVU7QUFBQyxhQUFPLEtBQUtiLFNBQUwsQ0FBZVosTUFBTSxDQUFDMEIsSUFBUCxDQUFZLElBQVosRUFBaUIsVUFBU0MsS0FBVCxFQUFleEMsQ0FBZixFQUFpQjtBQUFDLGVBQU0sQ0FBQ0EsQ0FBQyxHQUFDLENBQUgsSUFBTSxDQUFaO0FBQWUsT0FBbEQsQ0FBZixDQUFQO0FBQTRFLEtBRDViO0FBQzZieUMsT0FBRyxFQUFDLGVBQVU7QUFBQyxhQUFPLEtBQUtoQixTQUFMLENBQWVaLE1BQU0sQ0FBQzBCLElBQVAsQ0FBWSxJQUFaLEVBQWlCLFVBQVNDLEtBQVQsRUFBZXhDLENBQWYsRUFBaUI7QUFBQyxlQUFPQSxDQUFDLEdBQUMsQ0FBVDtBQUFZLE9BQS9DLENBQWYsQ0FBUDtBQUF5RSxLQURyaEI7QUFDc2hCb0MsTUFBRSxFQUFDLFlBQVNwQyxDQUFULEVBQVc7QUFBQyxVQUFJMEMsR0FBRyxHQUFDLEtBQUtyQixNQUFiO0FBQUEsVUFBb0JzQixDQUFDLEdBQUMsQ0FBQzNDLENBQUQsSUFBSUEsQ0FBQyxHQUFDLENBQUYsR0FBSTBDLEdBQUosR0FBUSxDQUFaLENBQXRCO0FBQXFDLGFBQU8sS0FBS2pCLFNBQUwsQ0FBZWtCLENBQUMsSUFBRSxDQUFILElBQU1BLENBQUMsR0FBQ0QsR0FBUixHQUFZLENBQUMsS0FBS0MsQ0FBTCxDQUFELENBQVosR0FBc0IsRUFBckMsQ0FBUDtBQUFpRCxLQUQzbkI7QUFDNG5CQyxPQUFHLEVBQUMsZUFBVTtBQUFDLGFBQU8sS0FBS2YsVUFBTCxJQUFpQixLQUFLVCxXQUFMLEVBQXhCO0FBQTRDLEtBRHZyQjtBQUN3ckIxQyxRQUFJLEVBQUNBLElBRDdyQjtBQUNrc0JtRSxRQUFJLEVBQUM3RSxHQUFHLENBQUM2RSxJQUQzc0I7QUFDZ3RCQyxVQUFNLEVBQUM5RSxHQUFHLENBQUM4RTtBQUQzdEIsR0FBM0I7O0FBQzh2QmpDLFFBQU0sQ0FBQ2tDLE1BQVAsR0FBY2xDLE1BQU0sQ0FBQ0csRUFBUCxDQUFVK0IsTUFBVixHQUFpQixZQUFVO0FBQUMsUUFBSUMsT0FBSjtBQUFBLFFBQVlDLElBQVo7QUFBQSxRQUFpQnhELEdBQWpCO0FBQUEsUUFBcUJ5RCxJQUFyQjtBQUFBLFFBQTBCQyxXQUExQjtBQUFBLFFBQXNDQyxLQUF0QztBQUFBLFFBQTRDQyxNQUFNLEdBQUNuQixTQUFTLENBQUMsQ0FBRCxDQUFULElBQWMsRUFBakU7QUFBQSxRQUFvRWxDLENBQUMsR0FBQyxDQUF0RTtBQUFBLFFBQXdFcUIsTUFBTSxHQUFDYSxTQUFTLENBQUNiLE1BQXpGO0FBQUEsUUFBZ0dpQyxJQUFJLEdBQUMsS0FBckc7O0FBQTJHLFFBQUcsT0FBT0QsTUFBUCxLQUFnQixTQUFuQixFQUE2QjtBQUFDQyxVQUFJLEdBQUNELE1BQUw7QUFBWUEsWUFBTSxHQUFDbkIsU0FBUyxDQUFDbEMsQ0FBRCxDQUFULElBQWMsRUFBckI7QUFBd0JBLE9BQUM7QUFBSTs7QUFDMWpDLFFBQUcsUUFBT3FELE1BQVAsTUFBZ0IsUUFBaEIsSUFBMEIsQ0FBQ2xFLFVBQVUsQ0FBQ2tFLE1BQUQsQ0FBeEMsRUFBaUQ7QUFBQ0EsWUFBTSxHQUFDLEVBQVA7QUFBVzs7QUFDN0QsUUFBR3JELENBQUMsS0FBR3FCLE1BQVAsRUFBYztBQUFDZ0MsWUFBTSxHQUFDLElBQVA7QUFBWXJELE9BQUM7QUFBSTs7QUFDaEMsV0FBS0EsQ0FBQyxHQUFDcUIsTUFBUCxFQUFjckIsQ0FBQyxFQUFmLEVBQWtCO0FBQUMsVUFBRyxDQUFDZ0QsT0FBTyxHQUFDZCxTQUFTLENBQUNsQyxDQUFELENBQWxCLEtBQXdCLElBQTNCLEVBQWdDO0FBQUMsYUFBSWlELElBQUosSUFBWUQsT0FBWixFQUFvQjtBQUFDRSxjQUFJLEdBQUNGLE9BQU8sQ0FBQ0MsSUFBRCxDQUFaOztBQUFtQixjQUFHQSxJQUFJLEtBQUcsV0FBUCxJQUFvQkksTUFBTSxLQUFHSCxJQUFoQyxFQUFxQztBQUFDO0FBQVU7O0FBQzVJLGNBQUdJLElBQUksSUFBRUosSUFBTixLQUFhckMsTUFBTSxDQUFDMEMsYUFBUCxDQUFxQkwsSUFBckIsTUFBNkJDLFdBQVcsR0FBQ0ssS0FBSyxDQUFDQyxPQUFOLENBQWNQLElBQWQsQ0FBekMsQ0FBYixDQUFILEVBQStFO0FBQUN6RCxlQUFHLEdBQUM0RCxNQUFNLENBQUNKLElBQUQsQ0FBVjs7QUFBaUIsZ0JBQUdFLFdBQVcsSUFBRSxDQUFDSyxLQUFLLENBQUNDLE9BQU4sQ0FBY2hFLEdBQWQsQ0FBakIsRUFBb0M7QUFBQzJELG1CQUFLLEdBQUMsRUFBTjtBQUFVLGFBQS9DLE1BQW9ELElBQUcsQ0FBQ0QsV0FBRCxJQUFjLENBQUN0QyxNQUFNLENBQUMwQyxhQUFQLENBQXFCOUQsR0FBckIsQ0FBbEIsRUFBNEM7QUFBQzJELG1CQUFLLEdBQUMsRUFBTjtBQUFVLGFBQXZELE1BQTJEO0FBQUNBLG1CQUFLLEdBQUMzRCxHQUFOO0FBQVc7O0FBQzVOMEQsdUJBQVcsR0FBQyxLQUFaO0FBQWtCRSxrQkFBTSxDQUFDSixJQUFELENBQU4sR0FBYXBDLE1BQU0sQ0FBQ2tDLE1BQVAsQ0FBY08sSUFBZCxFQUFtQkYsS0FBbkIsRUFBeUJGLElBQXpCLENBQWI7QUFBNkMsV0FEL0QsTUFDb0UsSUFBR0EsSUFBSSxLQUFHUSxTQUFWLEVBQW9CO0FBQUNMLGtCQUFNLENBQUNKLElBQUQsQ0FBTixHQUFhQyxJQUFiO0FBQW1CO0FBQUM7QUFBQztBQUFDOztBQUMvRyxXQUFPRyxNQUFQO0FBQWUsR0FOKzBCOztBQU05MEJ4QyxRQUFNLENBQUNrQyxNQUFQLENBQWM7QUFBQ1ksV0FBTyxFQUFDLFdBQVMsQ0FBQy9DLE9BQU8sR0FBQ2dELElBQUksQ0FBQ0MsTUFBTCxFQUFULEVBQXdCQyxPQUF4QixDQUFnQyxLQUFoQyxFQUFzQyxFQUF0QyxDQUFsQjtBQUE0REMsV0FBTyxFQUFDLElBQXBFO0FBQXlFQyxTQUFLLEVBQUMsZUFBU0MsR0FBVCxFQUFhO0FBQUMsWUFBTSxJQUFJcEcsS0FBSixDQUFVb0csR0FBVixDQUFOO0FBQXNCLEtBQW5IO0FBQW9IQyxRQUFJLEVBQUMsZ0JBQVUsQ0FBRSxDQUFySTtBQUFzSVgsaUJBQWEsRUFBQyx1QkFBU25FLEdBQVQsRUFBYTtBQUFDLFVBQUkrRSxLQUFKLEVBQVVDLElBQVY7O0FBQWUsVUFBRyxDQUFDaEYsR0FBRCxJQUFNUCxRQUFRLENBQUNOLElBQVQsQ0FBY2EsR0FBZCxNQUFxQixpQkFBOUIsRUFBZ0Q7QUFBQyxlQUFPLEtBQVA7QUFBYzs7QUFDOVErRSxXQUFLLEdBQUNsRyxRQUFRLENBQUNtQixHQUFELENBQWQ7O0FBQW9CLFVBQUcsQ0FBQytFLEtBQUosRUFBVTtBQUFDLGVBQU8sSUFBUDtBQUFhOztBQUM1Q0MsVUFBSSxHQUFDdEYsTUFBTSxDQUFDUCxJQUFQLENBQVk0RixLQUFaLEVBQWtCLGFBQWxCLEtBQWtDQSxLQUFLLENBQUMvQyxXQUE3QztBQUF5RCxhQUFPLE9BQU9nRCxJQUFQLEtBQWMsVUFBZCxJQUEwQnBGLFVBQVUsQ0FBQ1QsSUFBWCxDQUFnQjZGLElBQWhCLE1BQXdCbkYsb0JBQXpEO0FBQStFLEtBRjFHO0FBRTJHb0YsaUJBQWEsRUFBQyx1QkFBU2pGLEdBQVQsRUFBYTtBQUFDLFVBQUk2RCxJQUFKOztBQUFTLFdBQUlBLElBQUosSUFBWTdELEdBQVosRUFBZ0I7QUFBQyxlQUFPLEtBQVA7QUFBYzs7QUFDN00sYUFBTyxJQUFQO0FBQWEsS0FIaUI7QUFHaEJrRixjQUFVLEVBQUMsb0JBQVN6RSxJQUFULEVBQWNtRCxPQUFkLEVBQXNCakQsR0FBdEIsRUFBMEI7QUFBQ0gsYUFBTyxDQUFDQyxJQUFELEVBQU07QUFBQ0gsYUFBSyxFQUFDc0QsT0FBTyxJQUFFQSxPQUFPLENBQUN0RDtBQUF4QixPQUFOLEVBQXFDSyxHQUFyQyxDQUFQO0FBQWtELEtBSHhFO0FBR3lFK0IsUUFBSSxFQUFDLGNBQVMxQyxHQUFULEVBQWEyQyxRQUFiLEVBQXNCO0FBQUMsVUFBSVYsTUFBSjtBQUFBLFVBQVdyQixDQUFDLEdBQUMsQ0FBYjs7QUFBZSxVQUFHdUUsV0FBVyxDQUFDbkYsR0FBRCxDQUFkLEVBQW9CO0FBQUNpQyxjQUFNLEdBQUNqQyxHQUFHLENBQUNpQyxNQUFYOztBQUFrQixlQUFLckIsQ0FBQyxHQUFDcUIsTUFBUCxFQUFjckIsQ0FBQyxFQUFmLEVBQWtCO0FBQUMsY0FBRytCLFFBQVEsQ0FBQ3hELElBQVQsQ0FBY2EsR0FBRyxDQUFDWSxDQUFELENBQWpCLEVBQXFCQSxDQUFyQixFQUF1QlosR0FBRyxDQUFDWSxDQUFELENBQTFCLE1BQWlDLEtBQXBDLEVBQTBDO0FBQUM7QUFBTztBQUFDO0FBQUMsT0FBOUcsTUFBa0g7QUFBQyxhQUFJQSxDQUFKLElBQVNaLEdBQVQsRUFBYTtBQUFDLGNBQUcyQyxRQUFRLENBQUN4RCxJQUFULENBQWNhLEdBQUcsQ0FBQ1ksQ0FBRCxDQUFqQixFQUFxQkEsQ0FBckIsRUFBdUJaLEdBQUcsQ0FBQ1ksQ0FBRCxDQUExQixNQUFpQyxLQUFwQyxFQUEwQztBQUFDO0FBQU87QUFBQztBQUFDOztBQUN2VSxhQUFPWixHQUFQO0FBQVksS0FKa0I7QUFJakJvRixhQUFTLEVBQUMsbUJBQVN4RyxHQUFULEVBQWF5RyxPQUFiLEVBQXFCO0FBQUMsVUFBSTlDLEdBQUcsR0FBQzhDLE9BQU8sSUFBRSxFQUFqQjs7QUFBb0IsVUFBR3pHLEdBQUcsSUFBRSxJQUFSLEVBQWE7QUFBQyxZQUFHdUcsV0FBVyxDQUFDckcsTUFBTSxDQUFDRixHQUFELENBQVAsQ0FBZCxFQUE0QjtBQUFDNkMsZ0JBQU0sQ0FBQ2UsS0FBUCxDQUFhRCxHQUFiLEVBQWlCLE9BQU8zRCxHQUFQLEtBQWEsUUFBYixHQUFzQixDQUFDQSxHQUFELENBQXRCLEdBQTRCQSxHQUE3QztBQUFtRCxTQUFoRixNQUFvRjtBQUFDVSxjQUFJLENBQUNILElBQUwsQ0FBVW9ELEdBQVYsRUFBYzNELEdBQWQ7QUFBb0I7QUFBQzs7QUFDekwsYUFBTzJELEdBQVA7QUFBWSxLQUxrQjtBQUtqQitDLFdBQU8sRUFBQyxpQkFBU3pDLElBQVQsRUFBY2pFLEdBQWQsRUFBa0JnQyxDQUFsQixFQUFvQjtBQUFDLGFBQU9oQyxHQUFHLElBQUUsSUFBTCxHQUFVLENBQUMsQ0FBWCxHQUFhVyxPQUFPLENBQUNKLElBQVIsQ0FBYVAsR0FBYixFQUFpQmlFLElBQWpCLEVBQXNCakMsQ0FBdEIsQ0FBcEI7QUFBOEMsS0FMMUQ7QUFLMkQ0QixTQUFLLEVBQUMsZUFBU08sS0FBVCxFQUFld0MsTUFBZixFQUFzQjtBQUFDLFVBQUlqQyxHQUFHLEdBQUMsQ0FBQ2lDLE1BQU0sQ0FBQ3RELE1BQWhCO0FBQUEsVUFBdUJzQixDQUFDLEdBQUMsQ0FBekI7QUFBQSxVQUEyQjNDLENBQUMsR0FBQ21DLEtBQUssQ0FBQ2QsTUFBbkM7O0FBQTBDLGFBQUtzQixDQUFDLEdBQUNELEdBQVAsRUFBV0MsQ0FBQyxFQUFaLEVBQWU7QUFBQ1IsYUFBSyxDQUFDbkMsQ0FBQyxFQUFGLENBQUwsR0FBVzJFLE1BQU0sQ0FBQ2hDLENBQUQsQ0FBakI7QUFBc0I7O0FBQ3RNUixXQUFLLENBQUNkLE1BQU4sR0FBYXJCLENBQWI7QUFBZSxhQUFPbUMsS0FBUDtBQUFjLEtBTkM7QUFNQUksUUFBSSxFQUFDLGNBQVNiLEtBQVQsRUFBZUssUUFBZixFQUF3QjZDLE1BQXhCLEVBQStCO0FBQUMsVUFBSUMsZUFBSjtBQUFBLFVBQW9CQyxPQUFPLEdBQUMsRUFBNUI7QUFBQSxVQUErQjlFLENBQUMsR0FBQyxDQUFqQztBQUFBLFVBQW1DcUIsTUFBTSxHQUFDSyxLQUFLLENBQUNMLE1BQWhEO0FBQUEsVUFBdUQwRCxjQUFjLEdBQUMsQ0FBQ0gsTUFBdkU7O0FBQThFLGFBQUs1RSxDQUFDLEdBQUNxQixNQUFQLEVBQWNyQixDQUFDLEVBQWYsRUFBa0I7QUFBQzZFLHVCQUFlLEdBQUMsQ0FBQzlDLFFBQVEsQ0FBQ0wsS0FBSyxDQUFDMUIsQ0FBRCxDQUFOLEVBQVVBLENBQVYsQ0FBekI7O0FBQXNDLFlBQUc2RSxlQUFlLEtBQUdFLGNBQXJCLEVBQW9DO0FBQUNELGlCQUFPLENBQUNwRyxJQUFSLENBQWFnRCxLQUFLLENBQUMxQixDQUFELENBQWxCO0FBQXdCO0FBQUM7O0FBQ3hRLGFBQU84RSxPQUFQO0FBQWdCLEtBUGM7QUFPYjlDLE9BQUcsRUFBQyxhQUFTTixLQUFULEVBQWVLLFFBQWYsRUFBd0JpRCxHQUF4QixFQUE0QjtBQUFDLFVBQUkzRCxNQUFKO0FBQUEsVUFBVzRELEtBQVg7QUFBQSxVQUFpQmpGLENBQUMsR0FBQyxDQUFuQjtBQUFBLFVBQXFCMkIsR0FBRyxHQUFDLEVBQXpCOztBQUE0QixVQUFHNEMsV0FBVyxDQUFDN0MsS0FBRCxDQUFkLEVBQXNCO0FBQUNMLGNBQU0sR0FBQ0ssS0FBSyxDQUFDTCxNQUFiOztBQUFvQixlQUFLckIsQ0FBQyxHQUFDcUIsTUFBUCxFQUFjckIsQ0FBQyxFQUFmLEVBQWtCO0FBQUNpRixlQUFLLEdBQUNsRCxRQUFRLENBQUNMLEtBQUssQ0FBQzFCLENBQUQsQ0FBTixFQUFVQSxDQUFWLEVBQVlnRixHQUFaLENBQWQ7O0FBQStCLGNBQUdDLEtBQUssSUFBRSxJQUFWLEVBQWU7QUFBQ3RELGVBQUcsQ0FBQ2pELElBQUosQ0FBU3VHLEtBQVQ7QUFBaUI7QUFBQztBQUFDLE9BQWhJLE1BQW9JO0FBQUMsYUFBSWpGLENBQUosSUFBUzBCLEtBQVQsRUFBZTtBQUFDdUQsZUFBSyxHQUFDbEQsUUFBUSxDQUFDTCxLQUFLLENBQUMxQixDQUFELENBQU4sRUFBVUEsQ0FBVixFQUFZZ0YsR0FBWixDQUFkOztBQUErQixjQUFHQyxLQUFLLElBQUUsSUFBVixFQUFlO0FBQUN0RCxlQUFHLENBQUNqRCxJQUFKLENBQVN1RyxLQUFUO0FBQWlCO0FBQUM7QUFBQzs7QUFDclMsYUFBTzVHLElBQUksQ0FBQ3NELEdBQUQsQ0FBWDtBQUFrQixLQVJZO0FBUVh1RCxRQUFJLEVBQUMsQ0FSTTtBQVFKaEcsV0FBTyxFQUFDQTtBQVJKLEdBQWQ7O0FBUTRCLE1BQUcsT0FBT2lHLE1BQVAsS0FBZ0IsVUFBbkIsRUFBOEI7QUFBQ3RFLFVBQU0sQ0FBQ0csRUFBUCxDQUFVbUUsTUFBTSxDQUFDQyxRQUFqQixJQUEyQnBILEdBQUcsQ0FBQ21ILE1BQU0sQ0FBQ0MsUUFBUixDQUE5QjtBQUFpRDs7QUFDNUh2RSxRQUFNLENBQUNpQixJQUFQLENBQVksdUVBQXVFdUQsS0FBdkUsQ0FBNkUsR0FBN0UsQ0FBWixFQUE4RixVQUFTQyxFQUFULEVBQVlyQyxJQUFaLEVBQWlCO0FBQUNyRSxjQUFVLENBQUMsYUFBV3FFLElBQVgsR0FBZ0IsR0FBakIsQ0FBVixHQUFnQ0EsSUFBSSxDQUFDc0MsV0FBTCxFQUFoQztBQUFvRCxHQUFwSzs7QUFBc0ssV0FBU2hCLFdBQVQsQ0FBcUJuRixHQUFyQixFQUF5QjtBQUFDLFFBQUlpQyxNQUFNLEdBQUMsQ0FBQyxDQUFDakMsR0FBRixJQUFPLFlBQVdBLEdBQWxCLElBQXVCQSxHQUFHLENBQUNpQyxNQUF0QztBQUFBLFFBQTZDN0IsSUFBSSxHQUFDbUIsTUFBTSxDQUFDdkIsR0FBRCxDQUF4RDs7QUFBOEQsUUFBR0QsVUFBVSxDQUFDQyxHQUFELENBQVYsSUFBaUJFLFFBQVEsQ0FBQ0YsR0FBRCxDQUE1QixFQUFrQztBQUFDLGFBQU8sS0FBUDtBQUFjOztBQUMvUyxXQUFPSSxJQUFJLEtBQUcsT0FBUCxJQUFnQjZCLE1BQU0sS0FBRyxDQUF6QixJQUE0QixPQUFPQSxNQUFQLEtBQWdCLFFBQWhCLElBQTBCQSxNQUFNLEdBQUMsQ0FBakMsSUFBcUNBLE1BQU0sR0FBQyxDQUFSLElBQWFqQyxHQUFwRjtBQUF5Rjs7QUFDekYsTUFBSW9HLE1BQU07QUFDVjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNDLFlBQVMxSCxNQUFULEVBQWdCO0FBQUMsUUFBSWtDLENBQUo7QUFBQSxRQUFNZCxPQUFOO0FBQUEsUUFBY3VHLElBQWQ7QUFBQSxRQUFtQkMsT0FBbkI7QUFBQSxRQUEyQkMsS0FBM0I7QUFBQSxRQUFpQ0MsUUFBakM7QUFBQSxRQUEwQ0MsT0FBMUM7QUFBQSxRQUFrREMsTUFBbEQ7QUFBQSxRQUF5REMsZ0JBQXpEO0FBQUEsUUFBMEVDLFNBQTFFO0FBQUEsUUFBb0ZDLFlBQXBGO0FBQUEsUUFBaUdDLFdBQWpHO0FBQUEsUUFBNkd2SSxRQUE3RztBQUFBLFFBQXNId0ksT0FBdEg7QUFBQSxRQUE4SEMsY0FBOUg7QUFBQSxRQUE2SUMsU0FBN0k7QUFBQSxRQUF1SkMsYUFBdko7QUFBQSxRQUFxS3hCLE9BQXJLO0FBQUEsUUFBNkt5QixRQUE3SztBQUFBLFFBQXNMNUMsT0FBTyxHQUFDLFdBQVMsSUFBRSxJQUFJNkMsSUFBSixFQUF6TTtBQUFBLFFBQW9OQyxZQUFZLEdBQUMzSSxNQUFNLENBQUNILFFBQXhPO0FBQUEsUUFBaVArSSxPQUFPLEdBQUMsQ0FBelA7QUFBQSxRQUEyUEMsSUFBSSxHQUFDLENBQWhRO0FBQUEsUUFBa1FDLFVBQVUsR0FBQ0MsV0FBVyxFQUF4UjtBQUFBLFFBQTJSQyxVQUFVLEdBQUNELFdBQVcsRUFBalQ7QUFBQSxRQUFvVEUsYUFBYSxHQUFDRixXQUFXLEVBQTdVO0FBQUEsUUFBZ1ZHLHNCQUFzQixHQUFDSCxXQUFXLEVBQWxYO0FBQUEsUUFBcVhJLFNBQVMsR0FBQyxtQkFBU0MsQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxVQUFHRCxDQUFDLEtBQUdDLENBQVAsRUFBUztBQUFDbEIsb0JBQVksR0FBQyxJQUFiO0FBQW1COztBQUM1YixhQUFPLENBQVA7QUFBVSxLQURRO0FBQUEsUUFDUG5ILE1BQU0sR0FBRSxFQUFELENBQUtDLGNBREw7QUFBQSxRQUNvQmYsR0FBRyxHQUFDLEVBRHhCO0FBQUEsUUFDMkJvSixHQUFHLEdBQUNwSixHQUFHLENBQUNvSixHQURuQztBQUFBLFFBQ3VDQyxVQUFVLEdBQUNySixHQUFHLENBQUNVLElBRHREO0FBQUEsUUFDMkRBLElBQUksR0FBQ1YsR0FBRyxDQUFDVSxJQURwRTtBQUFBLFFBQ3lFTixLQUFLLEdBQUNKLEdBQUcsQ0FBQ0ksS0FEbkY7QUFBQSxRQUN5Rk8sT0FBTyxHQUFDLFNBQVJBLE9BQVEsQ0FBUzJJLElBQVQsRUFBY3JGLElBQWQsRUFBbUI7QUFBQyxVQUFJakMsQ0FBQyxHQUFDLENBQU47QUFBQSxVQUFRMEMsR0FBRyxHQUFDNEUsSUFBSSxDQUFDakcsTUFBakI7O0FBQXdCLGFBQUtyQixDQUFDLEdBQUMwQyxHQUFQLEVBQVcxQyxDQUFDLEVBQVosRUFBZTtBQUFDLFlBQUdzSCxJQUFJLENBQUN0SCxDQUFELENBQUosS0FBVWlDLElBQWIsRUFBa0I7QUFBQyxpQkFBT2pDLENBQVA7QUFBVTtBQUFDOztBQUM3TSxhQUFNLENBQUMsQ0FBUDtBQUFVLEtBRlE7QUFBQSxRQUVQdUgsUUFBUSxHQUFDLDhFQUE0RSxtREFGOUU7QUFBQSxRQUVrSUMsVUFBVSxHQUFDLHFCQUY3STtBQUFBLFFBRW1LQyxVQUFVLEdBQUMsNEJBQTBCRCxVQUExQixHQUFxQyx5Q0FGbk47QUFBQSxRQUU2UEUsVUFBVSxHQUFDLFFBQU1GLFVBQU4sR0FBaUIsSUFBakIsR0FBc0JDLFVBQXRCLEdBQWlDLE1BQWpDLEdBQXdDRCxVQUF4QyxHQUFtRCxlQUFuRCxHQUFtRUEsVUFBbkUsR0FBOEUsMERBQTlFLEdBQXlJQyxVQUF6SSxHQUFvSixNQUFwSixHQUMxUkQsVUFEMFIsR0FDL1EsTUFITztBQUFBLFFBR0FHLE9BQU8sR0FBQyxPQUFLRixVQUFMLEdBQWdCLFVBQWhCLEdBQTJCLHVEQUEzQixHQUFtRiwwQkFBbkYsR0FBOEdDLFVBQTlHLEdBQXlILE1BQXpILEdBQWdJLElBQWhJLEdBQXFJLFFBSDdJO0FBQUEsUUFHc0pFLFdBQVcsR0FBQyxJQUFJQyxNQUFKLENBQVdMLFVBQVUsR0FBQyxHQUF0QixFQUEwQixHQUExQixDQUhsSztBQUFBLFFBR2lNTSxLQUFLLEdBQUMsSUFBSUQsTUFBSixDQUFXLE1BQUlMLFVBQUosR0FBZSw2QkFBZixHQUNwT0EsVUFEb08sR0FDek4sSUFEOE0sRUFDek0sR0FEeU0sQ0FIdk07QUFBQSxRQUlHTyxNQUFNLEdBQUMsSUFBSUYsTUFBSixDQUFXLE1BQUlMLFVBQUosR0FBZSxJQUFmLEdBQW9CQSxVQUFwQixHQUErQixHQUExQyxDQUpWO0FBQUEsUUFJeURRLFlBQVksR0FBQyxJQUFJSCxNQUFKLENBQVcsTUFBSUwsVUFBSixHQUFlLFVBQWYsR0FBMEJBLFVBQTFCLEdBQXFDLEdBQXJDLEdBQXlDQSxVQUF6QyxHQUFvRCxHQUEvRCxDQUp0RTtBQUFBLFFBSTBJUyxRQUFRLEdBQUMsSUFBSUosTUFBSixDQUFXTCxVQUFVLEdBQUMsSUFBdEIsQ0FKbko7QUFBQSxRQUkrS1UsT0FBTyxHQUFDLElBQUlMLE1BQUosQ0FBV0YsT0FBWCxDQUp2TDtBQUFBLFFBSTJNUSxXQUFXLEdBQUMsSUFBSU4sTUFBSixDQUFXLE1BQUlKLFVBQUosR0FBZSxHQUExQixDQUp2TjtBQUFBLFFBSXNQVyxTQUFTLEdBQUM7QUFBQyxZQUFLLElBQUlQLE1BQUosQ0FBVyxRQUFNSixVQUFOLEdBQWlCLEdBQTVCLENBQU47QUFBdUMsZUFBUSxJQUFJSSxNQUFKLENBQVcsVUFBUUosVUFBUixHQUFtQixHQUE5QixDQUEvQztBQUFrRixhQUFNLElBQUlJLE1BQUosQ0FBVyxPQUFLSixVQUFMLEdBQWdCLE9BQTNCLENBQXhGO0FBQTRILGNBQU8sSUFBSUksTUFBSixDQUFXLE1BQUlILFVBQWYsQ0FBbkk7QUFBOEosZ0JBQVMsSUFBSUcsTUFBSixDQUFXLE1BQUlGLE9BQWYsQ0FBdks7QUFBK0wsZUFBUSxJQUFJRSxNQUFKLENBQVcsMkRBQ3BlTCxVQURvZSxHQUN6ZCw4QkFEeWQsR0FDMWJBLFVBRDBiLEdBQy9hLGFBRCthLEdBRXBlQSxVQUZvZSxHQUV6ZCxZQUZ5ZCxHQUU1Y0EsVUFGNGMsR0FFamMsUUFGc2IsRUFFN2EsR0FGNmEsQ0FBdk07QUFFak8sY0FBTyxJQUFJSyxNQUFKLENBQVcsU0FBT04sUUFBUCxHQUFnQixJQUEzQixFQUFnQyxHQUFoQyxDQUYwTjtBQUVyTCxzQkFBZSxJQUFJTSxNQUFKLENBQVcsTUFBSUwsVUFBSixHQUFlLGtEQUFmLEdBQWtFQSxVQUFsRSxHQUE2RSxrQkFBN0UsR0FBZ0dBLFVBQWhHLEdBQTJHLGtCQUF0SCxFQUF5SSxHQUF6STtBQUZzSyxLQUpoUTtBQUFBLFFBTXlPYSxLQUFLLEdBQUMsUUFOL087QUFBQSxRQU13UEMsT0FBTyxHQUFDLHFDQU5oUTtBQUFBLFFBTXNTQyxPQUFPLEdBQUMsUUFOOVM7QUFBQSxRQU11VEMsT0FBTyxHQUFDLHdCQU4vVDtBQUFBLFFBTXdWQyxVQUFVLEdBQUMsa0NBTm5XO0FBQUEsUUFNc1lDLFFBQVEsR0FBQyxNQU4vWTtBQUFBLFFBTXNaQyxTQUFTLEdBQUMsSUFBSWQsTUFBSixDQUFXLHlCQUF1QkwsVUFBdkIsR0FBa0Msc0JBQTdDLEVBQW9FLEdBQXBFLENBTmhhO0FBQUEsUUFNeWVvQixTQUFTLEdBQUMsU0FBVkEsU0FBVSxDQUFTQyxNQUFULEVBQWdCQyxNQUFoQixFQUF1QjtBQUFDLFVBQUlDLElBQUksR0FBQyxPQUFLRixNQUFNLENBQUN6SyxLQUFQLENBQWEsQ0FBYixDQUFMLEdBQXFCLE9BQTlCO0FBQXNDLGFBQU8wSyxNQUFNLEdBQUNBLE1BQUQsR0FBUUMsSUFBSSxHQUFDLENBQUwsR0FBT0MsTUFBTSxDQUFDQyxZQUFQLENBQW9CRixJQUFJLEdBQUMsT0FBekIsQ0FBUCxHQUF5Q0MsTUFBTSxDQUFDQyxZQUFQLENBQW9CRixJQUFJLElBQUUsRUFBTixHQUFTLE1BQTdCLEVBQW9DQSxJQUFJLEdBQUMsS0FBTCxHQUFXLE1BQS9DLENBQTlEO0FBQXNILEtBTnZxQjtBQUFBLFFBTXdxQkcsVUFBVSxHQUFDLHFEQU5uckI7QUFBQSxRQU15dUJDLFVBQVUsR0FBQyxTQUFYQSxVQUFXLENBQVNDLEVBQVQsRUFBWUMsV0FBWixFQUF3QjtBQUFDLFVBQUdBLFdBQUgsRUFBZTtBQUFDLFlBQUdELEVBQUUsS0FBRyxJQUFSLEVBQWE7QUFBQyxpQkFBTSxRQUFOO0FBQWdCOztBQUM3MEIsZUFBT0EsRUFBRSxDQUFDaEwsS0FBSCxDQUFTLENBQVQsRUFBVyxDQUFDLENBQVosSUFBZSxJQUFmLEdBQ1BnTCxFQUFFLENBQUNFLFVBQUgsQ0FBY0YsRUFBRSxDQUFDL0gsTUFBSCxHQUFVLENBQXhCLEVBQTJCeEMsUUFBM0IsQ0FBb0MsRUFBcEMsQ0FETyxHQUNpQyxHQUR4QztBQUM2Qzs7QUFDN0MsYUFBTSxPQUFLdUssRUFBWDtBQUFlLEtBVEc7QUFBQSxRQVNGRyxhQUFhLEdBQUMsU0FBZEEsYUFBYyxHQUFVO0FBQUNyRCxpQkFBVztBQUFJLEtBVHRDO0FBQUEsUUFTdUNzRCxrQkFBa0IsR0FBQ0MsYUFBYSxDQUFDLFVBQVN4SCxJQUFULEVBQWM7QUFBQyxhQUFPQSxJQUFJLENBQUN5SCxRQUFMLEtBQWdCLElBQWhCLElBQXNCekgsSUFBSSxDQUFDMEgsUUFBTCxDQUFjcEUsV0FBZCxPQUE4QixVQUEzRDtBQUF1RSxLQUF2RixFQUF3RjtBQUFDcUUsU0FBRyxFQUFDLFlBQUw7QUFBa0JDLFVBQUksRUFBQztBQUF2QixLQUF4RixDQVR2RTs7QUFTaU0sUUFBRztBQUFDbkwsVUFBSSxDQUFDRCxLQUFMLENBQVlULEdBQUcsR0FBQ0ksS0FBSyxDQUFDRyxJQUFOLENBQVdrSSxZQUFZLENBQUNxRCxVQUF4QixDQUFoQixFQUFxRHJELFlBQVksQ0FBQ3FELFVBQWxFO0FBQThFOUwsU0FBRyxDQUFDeUksWUFBWSxDQUFDcUQsVUFBYixDQUF3QnpJLE1BQXpCLENBQUgsQ0FBb0NoQyxRQUFwQztBQUE4QyxLQUFoSSxDQUFnSSxPQUFNMEssQ0FBTixFQUFRO0FBQUNyTCxVQUFJLEdBQUM7QUFBQ0QsYUFBSyxFQUFDVCxHQUFHLENBQUNxRCxNQUFKLEdBQVcsVUFBU2dDLE1BQVQsRUFBZ0IyRyxHQUFoQixFQUFvQjtBQUFDM0Msb0JBQVUsQ0FBQzVJLEtBQVgsQ0FBaUI0RSxNQUFqQixFQUF3QmpGLEtBQUssQ0FBQ0csSUFBTixDQUFXeUwsR0FBWCxDQUF4QjtBQUEwQyxTQUExRSxHQUEyRSxVQUFTM0csTUFBVCxFQUFnQjJHLEdBQWhCLEVBQW9CO0FBQUMsY0FBSXJILENBQUMsR0FBQ1UsTUFBTSxDQUFDaEMsTUFBYjtBQUFBLGNBQW9CckIsQ0FBQyxHQUFDLENBQXRCOztBQUF3QixpQkFBT3FELE1BQU0sQ0FBQ1YsQ0FBQyxFQUFGLENBQU4sR0FBWXFILEdBQUcsQ0FBQ2hLLENBQUMsRUFBRixDQUF0QixFQUE2QixDQUFFOztBQUMvZnFELGdCQUFNLENBQUNoQyxNQUFQLEdBQWNzQixDQUFDLEdBQUMsQ0FBaEI7QUFBbUI7QUFEOFUsT0FBTDtBQUN0VTs7QUFDdEIsYUFBUzZDLE1BQVQsQ0FBZ0IxRSxRQUFoQixFQUF5QkMsT0FBekIsRUFBaUMwRCxPQUFqQyxFQUF5Q3dGLElBQXpDLEVBQThDO0FBQUMsVUFBSUMsQ0FBSjtBQUFBLFVBQU1sSyxDQUFOO0FBQUEsVUFBUWlDLElBQVI7QUFBQSxVQUFha0ksR0FBYjtBQUFBLFVBQWlCQyxLQUFqQjtBQUFBLFVBQXVCQyxNQUF2QjtBQUFBLFVBQThCQyxXQUE5QjtBQUFBLFVBQTBDQyxVQUFVLEdBQUN4SixPQUFPLElBQUVBLE9BQU8sQ0FBQ3lKLGFBQXRFO0FBQUEsVUFBb0ZuTCxRQUFRLEdBQUMwQixPQUFPLEdBQUNBLE9BQU8sQ0FBQzFCLFFBQVQsR0FBa0IsQ0FBdEg7QUFBd0hvRixhQUFPLEdBQUNBLE9BQU8sSUFBRSxFQUFqQjs7QUFBb0IsVUFBRyxPQUFPM0QsUUFBUCxLQUFrQixRQUFsQixJQUE0QixDQUFDQSxRQUE3QixJQUF1Q3pCLFFBQVEsS0FBRyxDQUFYLElBQWNBLFFBQVEsS0FBRyxDQUF6QixJQUE0QkEsUUFBUSxLQUFHLEVBQWpGLEVBQW9GO0FBQUMsZUFBT29GLE9BQVA7QUFBZ0I7O0FBQ2hTLFVBQUcsQ0FBQ3dGLElBQUosRUFBUztBQUFDL0QsbUJBQVcsQ0FBQ25GLE9BQUQsQ0FBWDtBQUFxQkEsZUFBTyxHQUFDQSxPQUFPLElBQUVwRCxRQUFqQjs7QUFBMEIsWUFBR3lJLGNBQUgsRUFBa0I7QUFBQyxjQUFHL0csUUFBUSxLQUFHLEVBQVgsS0FBZ0IrSyxLQUFLLEdBQUMzQixVQUFVLENBQUNnQyxJQUFYLENBQWdCM0osUUFBaEIsQ0FBdEIsQ0FBSCxFQUFvRDtBQUFDLGdCQUFJb0osQ0FBQyxHQUFDRSxLQUFLLENBQUMsQ0FBRCxDQUFYLEVBQWdCO0FBQUMsa0JBQUcvSyxRQUFRLEtBQUcsQ0FBZCxFQUFnQjtBQUFDLG9CQUFJNEMsSUFBSSxHQUFDbEIsT0FBTyxDQUFDMkosY0FBUixDQUF1QlIsQ0FBdkIsQ0FBVCxFQUFvQztBQUFDLHNCQUFHakksSUFBSSxDQUFDMEksRUFBTCxLQUFVVCxDQUFiLEVBQWU7QUFBQ3pGLDJCQUFPLENBQUMvRixJQUFSLENBQWF1RCxJQUFiO0FBQW1CLDJCQUFPd0MsT0FBUDtBQUFnQjtBQUFDLGlCQUF6RixNQUE2RjtBQUFDLHlCQUFPQSxPQUFQO0FBQWdCO0FBQUMsZUFBaEksTUFBb0k7QUFBQyxvQkFBRzhGLFVBQVUsS0FBR3RJLElBQUksR0FBQ3NJLFVBQVUsQ0FBQ0csY0FBWCxDQUEwQlIsQ0FBMUIsQ0FBUixDQUFWLElBQWlEM0QsUUFBUSxDQUFDeEYsT0FBRCxFQUFTa0IsSUFBVCxDQUF6RCxJQUF5RUEsSUFBSSxDQUFDMEksRUFBTCxLQUFVVCxDQUF0RixFQUF3RjtBQUFDekYseUJBQU8sQ0FBQy9GLElBQVIsQ0FBYXVELElBQWI7QUFBbUIseUJBQU93QyxPQUFQO0FBQWdCO0FBQUM7QUFBQyxhQUFwUixNQUF5UixJQUFHMkYsS0FBSyxDQUFDLENBQUQsQ0FBUixFQUFZO0FBQUMxTCxrQkFBSSxDQUFDRCxLQUFMLENBQVdnRyxPQUFYLEVBQW1CMUQsT0FBTyxDQUFDNkosb0JBQVIsQ0FBNkI5SixRQUE3QixDQUFuQjtBQUEyRCxxQkFBTzJELE9BQVA7QUFBZ0IsYUFBeEYsTUFBNkYsSUFBRyxDQUFDeUYsQ0FBQyxHQUFDRSxLQUFLLENBQUMsQ0FBRCxDQUFSLEtBQWNsTCxPQUFPLENBQUMyTCxzQkFBdEIsSUFBOEM5SixPQUFPLENBQUM4SixzQkFBekQsRUFBZ0Y7QUFBQ25NLGtCQUFJLENBQUNELEtBQUwsQ0FBV2dHLE9BQVgsRUFBbUIxRCxPQUFPLENBQUM4SixzQkFBUixDQUErQlgsQ0FBL0IsQ0FBbkI7QUFBc0QscUJBQU96RixPQUFQO0FBQWdCO0FBQUM7O0FBQy9vQixjQUFHdkYsT0FBTyxDQUFDNEwsR0FBUixJQUFhLENBQUM5RCxzQkFBc0IsQ0FBQ2xHLFFBQVEsR0FBQyxHQUFWLENBQXBDLEtBQXFELENBQUN1RixTQUFELElBQVksQ0FBQ0EsU0FBUyxDQUFDMEUsSUFBVixDQUFlakssUUFBZixDQUFsRSxNQUE4RnpCLFFBQVEsS0FBRyxDQUFYLElBQWMwQixPQUFPLENBQUM0SSxRQUFSLENBQWlCcEUsV0FBakIsT0FBaUMsUUFBN0ksQ0FBSCxFQUEwSjtBQUFDK0UsdUJBQVcsR0FBQ3hKLFFBQVo7QUFBcUJ5SixzQkFBVSxHQUFDeEosT0FBWDs7QUFBbUIsZ0JBQUcxQixRQUFRLEtBQUcsQ0FBWCxLQUFlNEksUUFBUSxDQUFDOEMsSUFBVCxDQUFjakssUUFBZCxLQUF5QmtILFlBQVksQ0FBQytDLElBQWIsQ0FBa0JqSyxRQUFsQixDQUF4QyxDQUFILEVBQXdFO0FBQUN5Six3QkFBVSxHQUFDN0IsUUFBUSxDQUFDcUMsSUFBVCxDQUFjakssUUFBZCxLQUF5QmtLLFdBQVcsQ0FBQ2pLLE9BQU8sQ0FBQ04sVUFBVCxDQUFwQyxJQUEwRE0sT0FBckU7O0FBQTZFLGtCQUFHd0osVUFBVSxLQUFHeEosT0FBYixJQUFzQixDQUFDN0IsT0FBTyxDQUFDK0wsS0FBbEMsRUFBd0M7QUFBQyxvQkFBSWQsR0FBRyxHQUFDcEosT0FBTyxDQUFDVixZQUFSLENBQXFCLElBQXJCLENBQVIsRUFBb0M7QUFBQzhKLHFCQUFHLEdBQUNBLEdBQUcsQ0FBQ3JHLE9BQUosQ0FBWW9GLFVBQVosRUFBdUJDLFVBQXZCLENBQUo7QUFBd0MsaUJBQTdFLE1BQWlGO0FBQUNwSSx5QkFBTyxDQUFDVCxZQUFSLENBQXFCLElBQXJCLEVBQTJCNkosR0FBRyxHQUFDeEcsT0FBL0I7QUFBMEM7QUFBQzs7QUFDL2YwRyxvQkFBTSxHQUFDekUsUUFBUSxDQUFDOUUsUUFBRCxDQUFmO0FBQTBCZCxlQUFDLEdBQUNxSyxNQUFNLENBQUNoSixNQUFUOztBQUFnQixxQkFBTXJCLENBQUMsRUFBUCxFQUFVO0FBQUNxSyxzQkFBTSxDQUFDckssQ0FBRCxDQUFOLEdBQVUsQ0FBQ21LLEdBQUcsR0FBQyxNQUFJQSxHQUFMLEdBQVMsUUFBYixJQUF1QixHQUF2QixHQUMvRGUsVUFBVSxDQUFDYixNQUFNLENBQUNySyxDQUFELENBQVAsQ0FEMkM7QUFDOUI7O0FBQ3ZCc0sseUJBQVcsR0FBQ0QsTUFBTSxDQUFDYyxJQUFQLENBQVksR0FBWixDQUFaO0FBQThCOztBQUM5QixnQkFBRztBQUFDek0sa0JBQUksQ0FBQ0QsS0FBTCxDQUFXZ0csT0FBWCxFQUFtQjhGLFVBQVUsQ0FBQ2EsZ0JBQVgsQ0FBNEJkLFdBQTVCLENBQW5CO0FBQTZELHFCQUFPN0YsT0FBUDtBQUFnQixhQUFqRixDQUFpRixPQUFNNEcsUUFBTixFQUFlO0FBQUNyRSxvQ0FBc0IsQ0FBQ2xHLFFBQUQsRUFBVSxJQUFWLENBQXRCO0FBQXVDLGFBQXhJLFNBQStJO0FBQUMsa0JBQUdxSixHQUFHLEtBQUd4RyxPQUFULEVBQWlCO0FBQUM1Qyx1QkFBTyxDQUFDdUssZUFBUixDQUF3QixJQUF4QjtBQUErQjtBQUFDO0FBQUM7QUFBQztBQUFDOztBQUNyTSxhQUFPeEYsTUFBTSxDQUFDaEYsUUFBUSxDQUFDZ0QsT0FBVCxDQUFpQmdFLEtBQWpCLEVBQXVCLElBQXZCLENBQUQsRUFBOEIvRyxPQUE5QixFQUFzQzBELE9BQXRDLEVBQThDd0YsSUFBOUMsQ0FBYjtBQUFrRTs7QUFDbEUsYUFBU3BELFdBQVQsR0FBc0I7QUFBQyxVQUFJMEUsSUFBSSxHQUFDLEVBQVQ7O0FBQVksZUFBU0MsS0FBVCxDQUFlQyxHQUFmLEVBQW1CeEcsS0FBbkIsRUFBeUI7QUFBQyxZQUFHc0csSUFBSSxDQUFDN00sSUFBTCxDQUFVK00sR0FBRyxHQUFDLEdBQWQsSUFBbUJoRyxJQUFJLENBQUNpRyxXQUEzQixFQUF1QztBQUFDLGlCQUFPRixLQUFLLENBQUNELElBQUksQ0FBQ0ksS0FBTCxFQUFELENBQVo7QUFBNEI7O0FBQ2pJLGVBQU9ILEtBQUssQ0FBQ0MsR0FBRyxHQUFDLEdBQUwsQ0FBTCxHQUFleEcsS0FBdEI7QUFBOEI7O0FBQzlCLGFBQU91RyxLQUFQO0FBQWM7O0FBQ2QsYUFBU0ksWUFBVCxDQUFzQjVLLEVBQXRCLEVBQXlCO0FBQUNBLFFBQUUsQ0FBQzJDLE9BQUQsQ0FBRixHQUFZLElBQVo7QUFBaUIsYUFBTzNDLEVBQVA7QUFBVzs7QUFDdEQsYUFBUzZLLE1BQVQsQ0FBZ0I3SyxFQUFoQixFQUFtQjtBQUFDLFVBQUk4SyxFQUFFLEdBQUNuTyxRQUFRLENBQUN3QyxhQUFULENBQXVCLFVBQXZCLENBQVA7O0FBQTBDLFVBQUc7QUFBQyxlQUFNLENBQUMsQ0FBQ2EsRUFBRSxDQUFDOEssRUFBRCxDQUFWO0FBQWdCLE9BQXBCLENBQW9CLE9BQU0vQixDQUFOLEVBQVE7QUFBQyxlQUFPLEtBQVA7QUFBYyxPQUEzQyxTQUFrRDtBQUFDLFlBQUcrQixFQUFFLENBQUNyTCxVQUFOLEVBQWlCO0FBQUNxTCxZQUFFLENBQUNyTCxVQUFILENBQWNDLFdBQWQsQ0FBMEJvTCxFQUExQjtBQUErQjs7QUFDbEtBLFVBQUUsR0FBQyxJQUFIO0FBQVM7QUFBQzs7QUFDVixhQUFTQyxTQUFULENBQW1CQyxLQUFuQixFQUF5QkMsT0FBekIsRUFBaUM7QUFBQyxVQUFJak8sR0FBRyxHQUFDZ08sS0FBSyxDQUFDM0csS0FBTixDQUFZLEdBQVosQ0FBUjtBQUFBLFVBQXlCckYsQ0FBQyxHQUFDaEMsR0FBRyxDQUFDcUQsTUFBL0I7O0FBQXNDLGFBQU1yQixDQUFDLEVBQVAsRUFBVTtBQUFDeUYsWUFBSSxDQUFDeUcsVUFBTCxDQUFnQmxPLEdBQUcsQ0FBQ2dDLENBQUQsQ0FBbkIsSUFBd0JpTSxPQUF4QjtBQUFpQztBQUFDOztBQUNySCxhQUFTRSxZQUFULENBQXNCakYsQ0FBdEIsRUFBd0JDLENBQXhCLEVBQTBCO0FBQUMsVUFBSWlGLEdBQUcsR0FBQ2pGLENBQUMsSUFBRUQsQ0FBWDtBQUFBLFVBQWFtRixJQUFJLEdBQUNELEdBQUcsSUFBRWxGLENBQUMsQ0FBQzdILFFBQUYsS0FBYSxDQUFsQixJQUFxQjhILENBQUMsQ0FBQzlILFFBQUYsS0FBYSxDQUFsQyxJQUFxQzZILENBQUMsQ0FBQ29GLFdBQUYsR0FBY25GLENBQUMsQ0FBQ21GLFdBQXZFOztBQUFtRixVQUFHRCxJQUFILEVBQVE7QUFBQyxlQUFPQSxJQUFQO0FBQWE7O0FBQ3BJLFVBQUdELEdBQUgsRUFBTztBQUFDLGVBQU9BLEdBQUcsR0FBQ0EsR0FBRyxDQUFDRyxXQUFmLEVBQTRCO0FBQUMsY0FBR0gsR0FBRyxLQUFHakYsQ0FBVCxFQUFXO0FBQUMsbUJBQU0sQ0FBQyxDQUFQO0FBQVU7QUFBQztBQUFDOztBQUM3RCxhQUFPRCxDQUFDLEdBQUMsQ0FBRCxHQUFHLENBQUMsQ0FBWjtBQUFlOztBQUNmLGFBQVNzRixpQkFBVCxDQUEyQmhOLElBQTNCLEVBQWdDO0FBQUMsYUFBTyxVQUFTeUMsSUFBVCxFQUFjO0FBQUMsWUFBSWdCLElBQUksR0FBQ2hCLElBQUksQ0FBQzBILFFBQUwsQ0FBY3BFLFdBQWQsRUFBVDtBQUFxQyxlQUFPdEMsSUFBSSxLQUFHLE9BQVAsSUFBZ0JoQixJQUFJLENBQUN6QyxJQUFMLEtBQVlBLElBQW5DO0FBQXlDLE9BQXBHO0FBQXNHOztBQUN2SSxhQUFTaU4sa0JBQVQsQ0FBNEJqTixJQUE1QixFQUFpQztBQUFDLGFBQU8sVUFBU3lDLElBQVQsRUFBYztBQUFDLFlBQUlnQixJQUFJLEdBQUNoQixJQUFJLENBQUMwSCxRQUFMLENBQWNwRSxXQUFkLEVBQVQ7QUFBcUMsZUFBTSxDQUFDdEMsSUFBSSxLQUFHLE9BQVAsSUFBZ0JBLElBQUksS0FBRyxRQUF4QixLQUFtQ2hCLElBQUksQ0FBQ3pDLElBQUwsS0FBWUEsSUFBckQ7QUFBMkQsT0FBdEg7QUFBd0g7O0FBQzFKLGFBQVNrTixvQkFBVCxDQUE4QmhELFFBQTlCLEVBQXVDO0FBQUMsYUFBTyxVQUFTekgsSUFBVCxFQUFjO0FBQUMsWUFBRyxVQUFTQSxJQUFaLEVBQWlCO0FBQUMsY0FBR0EsSUFBSSxDQUFDeEIsVUFBTCxJQUFpQndCLElBQUksQ0FBQ3lILFFBQUwsS0FBZ0IsS0FBcEMsRUFBMEM7QUFBQyxnQkFBRyxXQUFVekgsSUFBYixFQUFrQjtBQUFDLGtCQUFHLFdBQVVBLElBQUksQ0FBQ3hCLFVBQWxCLEVBQTZCO0FBQUMsdUJBQU93QixJQUFJLENBQUN4QixVQUFMLENBQWdCaUosUUFBaEIsS0FBMkJBLFFBQWxDO0FBQTRDLGVBQTFFLE1BQThFO0FBQUMsdUJBQU96SCxJQUFJLENBQUN5SCxRQUFMLEtBQWdCQSxRQUF2QjtBQUFpQztBQUFDOztBQUMvUCxtQkFBT3pILElBQUksQ0FBQzBLLFVBQUwsS0FBa0JqRCxRQUFsQixJQUE0QnpILElBQUksQ0FBQzBLLFVBQUwsS0FBa0IsQ0FBQ2pELFFBQW5CLElBQTZCRixrQkFBa0IsQ0FBQ3ZILElBQUQsQ0FBbEIsS0FBMkJ5SCxRQUEzRjtBQUFxRzs7QUFDckcsaUJBQU96SCxJQUFJLENBQUN5SCxRQUFMLEtBQWdCQSxRQUF2QjtBQUFpQyxTQUY2QixNQUV4QixJQUFHLFdBQVV6SCxJQUFiLEVBQWtCO0FBQUMsaUJBQU9BLElBQUksQ0FBQ3lILFFBQUwsS0FBZ0JBLFFBQXZCO0FBQWlDOztBQUMxRixlQUFPLEtBQVA7QUFBYyxPQUgwQjtBQUd4Qjs7QUFDaEIsYUFBU2tELHNCQUFULENBQWdDNUwsRUFBaEMsRUFBbUM7QUFBQyxhQUFPNEssWUFBWSxDQUFDLFVBQVNpQixRQUFULEVBQWtCO0FBQUNBLGdCQUFRLEdBQUMsQ0FBQ0EsUUFBVjtBQUFtQixlQUFPakIsWUFBWSxDQUFDLFVBQVMzQixJQUFULEVBQWNuRixPQUFkLEVBQXNCO0FBQUMsY0FBSW5DLENBQUo7QUFBQSxjQUFNbUssWUFBWSxHQUFDOUwsRUFBRSxDQUFDLEVBQUQsRUFBSWlKLElBQUksQ0FBQzVJLE1BQVQsRUFBZ0J3TCxRQUFoQixDQUFyQjtBQUFBLGNBQStDN00sQ0FBQyxHQUFDOE0sWUFBWSxDQUFDekwsTUFBOUQ7O0FBQXFFLGlCQUFNckIsQ0FBQyxFQUFQLEVBQVU7QUFBQyxnQkFBR2lLLElBQUksQ0FBRXRILENBQUMsR0FBQ21LLFlBQVksQ0FBQzlNLENBQUQsQ0FBaEIsQ0FBUCxFQUE2QjtBQUFDaUssa0JBQUksQ0FBQ3RILENBQUQsQ0FBSixHQUFRLEVBQUVtQyxPQUFPLENBQUNuQyxDQUFELENBQVAsR0FBV3NILElBQUksQ0FBQ3RILENBQUQsQ0FBakIsQ0FBUjtBQUErQjtBQUFDO0FBQUMsU0FBdkssQ0FBbkI7QUFBNkwsT0FBcE8sQ0FBbkI7QUFBMFA7O0FBQzlSLGFBQVNxSSxXQUFULENBQXFCakssT0FBckIsRUFBNkI7QUFBQyxhQUFPQSxPQUFPLElBQUUsT0FBT0EsT0FBTyxDQUFDNkosb0JBQWYsS0FBc0MsV0FBL0MsSUFBNEQ3SixPQUFuRTtBQUE0RTs7QUFDMUc3QixXQUFPLEdBQUNzRyxNQUFNLENBQUN0RyxPQUFQLEdBQWUsRUFBdkI7O0FBQTBCeUcsU0FBSyxHQUFDSCxNQUFNLENBQUNHLEtBQVAsR0FBYSxVQUFTMUQsSUFBVCxFQUFjO0FBQUMsVUFBSThLLFNBQVMsR0FBQzlLLElBQUksQ0FBQytLLFlBQW5CO0FBQUEsVUFBZ0M3RyxPQUFPLEdBQUMsQ0FBQ2xFLElBQUksQ0FBQ3VJLGFBQUwsSUFBb0J2SSxJQUFyQixFQUEyQmdMLGVBQW5FO0FBQW1GLGFBQU0sQ0FBQzVFLEtBQUssQ0FBQzBDLElBQU4sQ0FBV2dDLFNBQVMsSUFBRTVHLE9BQU8sSUFBRUEsT0FBTyxDQUFDd0QsUUFBNUIsSUFBc0MsTUFBakQsQ0FBUDtBQUFpRSxLQUF0TDs7QUFBdUx6RCxlQUFXLEdBQUNWLE1BQU0sQ0FBQ1UsV0FBUCxHQUFtQixVQUFTcEcsSUFBVCxFQUFjO0FBQUMsVUFBSW9OLFVBQUo7QUFBQSxVQUFlQyxTQUFmO0FBQUEsVUFBeUJwTixHQUFHLEdBQUNELElBQUksR0FBQ0EsSUFBSSxDQUFDMEssYUFBTCxJQUFvQjFLLElBQXJCLEdBQTBCMkcsWUFBM0Q7O0FBQXdFLFVBQUcxRyxHQUFHLElBQUVwQyxRQUFMLElBQWVvQyxHQUFHLENBQUNWLFFBQUosS0FBZSxDQUE5QixJQUFpQyxDQUFDVSxHQUFHLENBQUNrTixlQUF6QyxFQUF5RDtBQUFDLGVBQU90UCxRQUFQO0FBQWlCOztBQUNsWkEsY0FBUSxHQUFDb0MsR0FBVDtBQUFhb0csYUFBTyxHQUFDeEksUUFBUSxDQUFDc1AsZUFBakI7QUFBaUM3RyxvQkFBYyxHQUFDLENBQUNULEtBQUssQ0FBQ2hJLFFBQUQsQ0FBckI7O0FBQWdDLFVBQUc4SSxZQUFZLElBQUU5SSxRQUFkLEtBQXlCd1AsU0FBUyxHQUFDeFAsUUFBUSxDQUFDeVAsV0FBNUMsS0FBMERELFNBQVMsQ0FBQ0UsR0FBVixLQUFnQkYsU0FBN0UsRUFBdUY7QUFBQyxZQUFHQSxTQUFTLENBQUNHLGdCQUFiLEVBQThCO0FBQUNILG1CQUFTLENBQUNHLGdCQUFWLENBQTJCLFFBQTNCLEVBQW9DL0QsYUFBcEMsRUFBa0QsS0FBbEQ7QUFBMEQsU0FBekYsTUFBOEYsSUFBRzRELFNBQVMsQ0FBQ0ksV0FBYixFQUF5QjtBQUFDSixtQkFBUyxDQUFDSSxXQUFWLENBQXNCLFVBQXRCLEVBQWlDaEUsYUFBakM7QUFBaUQ7QUFBQzs7QUFDaFZySyxhQUFPLENBQUMrTCxLQUFSLEdBQWNZLE1BQU0sQ0FBQyxVQUFTQyxFQUFULEVBQVk7QUFBQzNGLGVBQU8sQ0FBQzNGLFdBQVIsQ0FBb0JzTCxFQUFwQixFQUF3QnRMLFdBQXhCLENBQW9DN0MsUUFBUSxDQUFDd0MsYUFBVCxDQUF1QixLQUF2QixDQUFwQztBQUFtRSxlQUFPLE9BQU8yTCxFQUFFLENBQUNWLGdCQUFWLEtBQTZCLFdBQTdCLElBQTBDLENBQUNVLEVBQUUsQ0FBQ1YsZ0JBQUgsQ0FBb0IscUJBQXBCLEVBQTJDL0osTUFBN0Y7QUFBcUcsT0FBdEwsQ0FBcEI7QUFBNE1uQyxhQUFPLENBQUN3SSxVQUFSLEdBQW1CbUUsTUFBTSxDQUFDLFVBQVNDLEVBQVQsRUFBWTtBQUFDQSxVQUFFLENBQUMwQixTQUFILEdBQWEsR0FBYjtBQUFpQixlQUFNLENBQUMxQixFQUFFLENBQUN6TCxZQUFILENBQWdCLFdBQWhCLENBQVA7QUFBcUMsT0FBcEUsQ0FBekI7QUFBK0ZuQixhQUFPLENBQUMwTCxvQkFBUixHQUE2QmlCLE1BQU0sQ0FBQyxVQUFTQyxFQUFULEVBQVk7QUFBQ0EsVUFBRSxDQUFDdEwsV0FBSCxDQUFlN0MsUUFBUSxDQUFDOFAsYUFBVCxDQUF1QixFQUF2QixDQUFmO0FBQTJDLGVBQU0sQ0FBQzNCLEVBQUUsQ0FBQ2xCLG9CQUFILENBQXdCLEdBQXhCLEVBQTZCdkosTUFBcEM7QUFBNEMsT0FBckcsQ0FBbkM7QUFBMEluQyxhQUFPLENBQUMyTCxzQkFBUixHQUErQnJDLE9BQU8sQ0FBQ3VDLElBQVIsQ0FBYXBOLFFBQVEsQ0FBQ2tOLHNCQUF0QixDQUEvQjtBQUE2RTNMLGFBQU8sQ0FBQ3dPLE9BQVIsR0FBZ0I3QixNQUFNLENBQUMsVUFBU0MsRUFBVCxFQUFZO0FBQUMzRixlQUFPLENBQUMzRixXQUFSLENBQW9Cc0wsRUFBcEIsRUFBd0JuQixFQUF4QixHQUEyQmhILE9BQTNCO0FBQW1DLGVBQU0sQ0FBQ2hHLFFBQVEsQ0FBQ2dRLGlCQUFWLElBQTZCLENBQUNoUSxRQUFRLENBQUNnUSxpQkFBVCxDQUEyQmhLLE9BQTNCLEVBQW9DdEMsTUFBeEU7QUFBZ0YsT0FBakksQ0FBdEI7O0FBQXlKLFVBQUduQyxPQUFPLENBQUN3TyxPQUFYLEVBQW1CO0FBQUNqSSxZQUFJLENBQUNtSSxNQUFMLENBQVksSUFBWixJQUFrQixVQUFTakQsRUFBVCxFQUFZO0FBQUMsY0FBSWtELE1BQU0sR0FBQ2xELEVBQUUsQ0FBQzdHLE9BQUgsQ0FBVzZFLFNBQVgsRUFBcUJDLFNBQXJCLENBQVg7QUFBMkMsaUJBQU8sVUFBUzNHLElBQVQsRUFBYztBQUFDLG1CQUFPQSxJQUFJLENBQUM1QixZQUFMLENBQWtCLElBQWxCLE1BQTBCd04sTUFBakM7QUFBeUMsV0FBL0Q7QUFBaUUsU0FBM0k7O0FBQTRJcEksWUFBSSxDQUFDcUksSUFBTCxDQUFVLElBQVYsSUFBZ0IsVUFBU25ELEVBQVQsRUFBWTVKLE9BQVosRUFBb0I7QUFBQyxjQUFHLE9BQU9BLE9BQU8sQ0FBQzJKLGNBQWYsS0FBZ0MsV0FBaEMsSUFBNkN0RSxjQUFoRCxFQUErRDtBQUFDLGdCQUFJbkUsSUFBSSxHQUFDbEIsT0FBTyxDQUFDMkosY0FBUixDQUF1QkMsRUFBdkIsQ0FBVDtBQUFvQyxtQkFBTzFJLElBQUksR0FBQyxDQUFDQSxJQUFELENBQUQsR0FBUSxFQUFuQjtBQUF1QjtBQUFDLFNBQWpLO0FBQW1LLE9BQW5VLE1BQXVVO0FBQUN3RCxZQUFJLENBQUNtSSxNQUFMLENBQVksSUFBWixJQUFrQixVQUFTakQsRUFBVCxFQUFZO0FBQUMsY0FBSWtELE1BQU0sR0FBQ2xELEVBQUUsQ0FBQzdHLE9BQUgsQ0FBVzZFLFNBQVgsRUFBcUJDLFNBQXJCLENBQVg7QUFBMkMsaUJBQU8sVUFBUzNHLElBQVQsRUFBYztBQUFDLGdCQUFJbkMsSUFBSSxHQUFDLE9BQU9tQyxJQUFJLENBQUM4TCxnQkFBWixLQUErQixXQUEvQixJQUE0QzlMLElBQUksQ0FBQzhMLGdCQUFMLENBQXNCLElBQXRCLENBQXJEO0FBQWlGLG1CQUFPak8sSUFBSSxJQUFFQSxJQUFJLENBQUNtRixLQUFMLEtBQWE0SSxNQUExQjtBQUFrQyxXQUF6STtBQUEySSxTQUFyTjs7QUFBc05wSSxZQUFJLENBQUNxSSxJQUFMLENBQVUsSUFBVixJQUFnQixVQUFTbkQsRUFBVCxFQUFZNUosT0FBWixFQUFvQjtBQUFDLGNBQUcsT0FBT0EsT0FBTyxDQUFDMkosY0FBZixLQUFnQyxXQUFoQyxJQUE2Q3RFLGNBQWhELEVBQStEO0FBQUMsZ0JBQUl0RyxJQUFKO0FBQUEsZ0JBQVNFLENBQVQ7QUFBQSxnQkFBVzBCLEtBQVg7QUFBQSxnQkFBaUJPLElBQUksR0FBQ2xCLE9BQU8sQ0FBQzJKLGNBQVIsQ0FBdUJDLEVBQXZCLENBQXRCOztBQUFpRCxnQkFBRzFJLElBQUgsRUFBUTtBQUFDbkMsa0JBQUksR0FBQ21DLElBQUksQ0FBQzhMLGdCQUFMLENBQXNCLElBQXRCLENBQUw7O0FBQWlDLGtCQUFHak8sSUFBSSxJQUFFQSxJQUFJLENBQUNtRixLQUFMLEtBQWEwRixFQUF0QixFQUF5QjtBQUFDLHVCQUFNLENBQUMxSSxJQUFELENBQU47QUFBYzs7QUFDajZDUCxtQkFBSyxHQUFDWCxPQUFPLENBQUM0TSxpQkFBUixDQUEwQmhELEVBQTFCLENBQU47QUFBb0MzSyxlQUFDLEdBQUMsQ0FBRjs7QUFBSSxxQkFBT2lDLElBQUksR0FBQ1AsS0FBSyxDQUFDMUIsQ0FBQyxFQUFGLENBQWpCLEVBQXdCO0FBQUNGLG9CQUFJLEdBQUNtQyxJQUFJLENBQUM4TCxnQkFBTCxDQUFzQixJQUF0QixDQUFMOztBQUFpQyxvQkFBR2pPLElBQUksSUFBRUEsSUFBSSxDQUFDbUYsS0FBTCxLQUFhMEYsRUFBdEIsRUFBeUI7QUFBQyx5QkFBTSxDQUFDMUksSUFBRCxDQUFOO0FBQWM7QUFBQztBQUFDOztBQUM1SSxtQkFBTSxFQUFOO0FBQVU7QUFBQyxTQUY4cUM7QUFFNXFDOztBQUNid0QsVUFBSSxDQUFDcUksSUFBTCxDQUFVLEtBQVYsSUFBaUI1TyxPQUFPLENBQUMwTCxvQkFBUixHQUE2QixVQUFTb0QsR0FBVCxFQUFhak4sT0FBYixFQUFxQjtBQUFDLFlBQUcsT0FBT0EsT0FBTyxDQUFDNkosb0JBQWYsS0FBc0MsV0FBekMsRUFBcUQ7QUFBQyxpQkFBTzdKLE9BQU8sQ0FBQzZKLG9CQUFSLENBQTZCb0QsR0FBN0IsQ0FBUDtBQUEwQyxTQUFoRyxNQUFxRyxJQUFHOU8sT0FBTyxDQUFDNEwsR0FBWCxFQUFlO0FBQUMsaUJBQU8vSixPQUFPLENBQUNxSyxnQkFBUixDQUF5QjRDLEdBQXpCLENBQVA7QUFBc0M7QUFBQyxPQUEvTSxHQUFnTixVQUFTQSxHQUFULEVBQWFqTixPQUFiLEVBQXFCO0FBQUMsWUFBSWtCLElBQUo7QUFBQSxZQUFTZ00sR0FBRyxHQUFDLEVBQWI7QUFBQSxZQUFnQmpPLENBQUMsR0FBQyxDQUFsQjtBQUFBLFlBQW9CeUUsT0FBTyxHQUFDMUQsT0FBTyxDQUFDNkosb0JBQVIsQ0FBNkJvRCxHQUE3QixDQUE1Qjs7QUFBOEQsWUFBR0EsR0FBRyxLQUFHLEdBQVQsRUFBYTtBQUFDLGlCQUFPL0wsSUFBSSxHQUFDd0MsT0FBTyxDQUFDekUsQ0FBQyxFQUFGLENBQW5CLEVBQTBCO0FBQUMsZ0JBQUdpQyxJQUFJLENBQUM1QyxRQUFMLEtBQWdCLENBQW5CLEVBQXFCO0FBQUM0TyxpQkFBRyxDQUFDdlAsSUFBSixDQUFTdUQsSUFBVDtBQUFnQjtBQUFDOztBQUNyWSxpQkFBT2dNLEdBQVA7QUFBWTs7QUFDWixlQUFPeEosT0FBUDtBQUFnQixPQUZoQjs7QUFFaUJnQixVQUFJLENBQUNxSSxJQUFMLENBQVUsT0FBVixJQUFtQjVPLE9BQU8sQ0FBQzJMLHNCQUFSLElBQWdDLFVBQVMyQyxTQUFULEVBQW1Cek0sT0FBbkIsRUFBMkI7QUFBQyxZQUFHLE9BQU9BLE9BQU8sQ0FBQzhKLHNCQUFmLEtBQXdDLFdBQXhDLElBQXFEekUsY0FBeEQsRUFBdUU7QUFBQyxpQkFBT3JGLE9BQU8sQ0FBQzhKLHNCQUFSLENBQStCMkMsU0FBL0IsQ0FBUDtBQUFrRDtBQUFDLE9BQTFNOztBQUEyTWxILG1CQUFhLEdBQUMsRUFBZDtBQUFpQkQsZUFBUyxHQUFDLEVBQVY7O0FBQWEsVUFBSW5ILE9BQU8sQ0FBQzRMLEdBQVIsR0FBWXRDLE9BQU8sQ0FBQ3VDLElBQVIsQ0FBYXBOLFFBQVEsQ0FBQ3lOLGdCQUF0QixDQUFoQixFQUF5RDtBQUFDUyxjQUFNLENBQUMsVUFBU0MsRUFBVCxFQUFZO0FBQUMsY0FBSW9DLEtBQUo7QUFBVS9ILGlCQUFPLENBQUMzRixXQUFSLENBQW9Cc0wsRUFBcEIsRUFBd0JxQyxTQUF4QixHQUFrQyxZQUFVeEssT0FBVixHQUFrQixRQUFsQixHQUEyQixjQUEzQixHQUEwQ0EsT0FBMUMsR0FBa0QsMkJBQWxELEdBQThFLHdDQUFoSDs7QUFBeUosY0FBR21JLEVBQUUsQ0FBQ1YsZ0JBQUgsQ0FBb0Isc0JBQXBCLEVBQTRDL0osTUFBL0MsRUFBc0Q7QUFBQ2dGLHFCQUFTLENBQUMzSCxJQUFWLENBQWUsV0FBUzhJLFVBQVQsR0FBb0IsY0FBbkM7QUFBb0Q7O0FBQ3RsQixjQUFHLENBQUNzRSxFQUFFLENBQUNWLGdCQUFILENBQW9CLFlBQXBCLEVBQWtDL0osTUFBdEMsRUFBNkM7QUFBQ2dGLHFCQUFTLENBQUMzSCxJQUFWLENBQWUsUUFBTThJLFVBQU4sR0FBaUIsWUFBakIsR0FBOEJELFFBQTlCLEdBQXVDLEdBQXREO0FBQTREOztBQUMxRyxjQUFHLENBQUN1RSxFQUFFLENBQUNWLGdCQUFILENBQW9CLFVBQVF6SCxPQUFSLEdBQWdCLElBQXBDLEVBQTBDdEMsTUFBOUMsRUFBcUQ7QUFBQ2dGLHFCQUFTLENBQUMzSCxJQUFWLENBQWUsSUFBZjtBQUFzQjs7QUFDNUV3UCxlQUFLLEdBQUN2USxRQUFRLENBQUN3QyxhQUFULENBQXVCLE9BQXZCLENBQU47QUFBc0MrTixlQUFLLENBQUM1TixZQUFOLENBQW1CLE1BQW5CLEVBQTBCLEVBQTFCO0FBQThCd0wsWUFBRSxDQUFDdEwsV0FBSCxDQUFlME4sS0FBZjs7QUFBc0IsY0FBRyxDQUFDcEMsRUFBRSxDQUFDVixnQkFBSCxDQUFvQixXQUFwQixFQUFpQy9KLE1BQXJDLEVBQTRDO0FBQUNnRixxQkFBUyxDQUFDM0gsSUFBVixDQUFlLFFBQU04SSxVQUFOLEdBQWlCLE9BQWpCLEdBQXlCQSxVQUF6QixHQUFvQyxJQUFwQyxHQUN0SkEsVUFEc0osR0FDM0ksY0FENEg7QUFDM0c7O0FBQzVCLGNBQUcsQ0FBQ3NFLEVBQUUsQ0FBQ1YsZ0JBQUgsQ0FBb0IsVUFBcEIsRUFBZ0MvSixNQUFwQyxFQUEyQztBQUFDZ0YscUJBQVMsQ0FBQzNILElBQVYsQ0FBZSxVQUFmO0FBQTRCOztBQUN4RSxjQUFHLENBQUNvTixFQUFFLENBQUNWLGdCQUFILENBQW9CLE9BQUt6SCxPQUFMLEdBQWEsSUFBakMsRUFBdUN0QyxNQUEzQyxFQUFrRDtBQUFDZ0YscUJBQVMsQ0FBQzNILElBQVYsQ0FBZSxVQUFmO0FBQTRCOztBQUMvRW9OLFlBQUUsQ0FBQ1YsZ0JBQUgsQ0FBb0IsTUFBcEI7QUFBNEIvRSxtQkFBUyxDQUFDM0gsSUFBVixDQUFlLGFBQWY7QUFBK0IsU0FQK1AsQ0FBTjtBQU92UG1OLGNBQU0sQ0FBQyxVQUFTQyxFQUFULEVBQVk7QUFBQ0EsWUFBRSxDQUFDcUMsU0FBSCxHQUFhLHdDQUFzQyxnREFBbkQ7QUFBb0csY0FBSUQsS0FBSyxHQUFDdlEsUUFBUSxDQUFDd0MsYUFBVCxDQUF1QixPQUF2QixDQUFWO0FBQTBDK04sZUFBSyxDQUFDNU4sWUFBTixDQUFtQixNQUFuQixFQUEwQixRQUExQjtBQUFvQ3dMLFlBQUUsQ0FBQ3RMLFdBQUgsQ0FBZTBOLEtBQWYsRUFBc0I1TixZQUF0QixDQUFtQyxNQUFuQyxFQUEwQyxHQUExQzs7QUFBK0MsY0FBR3dMLEVBQUUsQ0FBQ1YsZ0JBQUgsQ0FBb0IsVUFBcEIsRUFBZ0MvSixNQUFuQyxFQUEwQztBQUFDZ0YscUJBQVMsQ0FBQzNILElBQVYsQ0FBZSxTQUFPOEksVUFBUCxHQUFrQixhQUFqQztBQUFpRDs7QUFDOVksY0FBR3NFLEVBQUUsQ0FBQ1YsZ0JBQUgsQ0FBb0IsVUFBcEIsRUFBZ0MvSixNQUFoQyxLQUF5QyxDQUE1QyxFQUE4QztBQUFDZ0YscUJBQVMsQ0FBQzNILElBQVYsQ0FBZSxVQUFmLEVBQTBCLFdBQTFCO0FBQXdDOztBQUN2RnlILGlCQUFPLENBQUMzRixXQUFSLENBQW9Cc0wsRUFBcEIsRUFBd0JwQyxRQUF4QixHQUFpQyxJQUFqQzs7QUFBc0MsY0FBR29DLEVBQUUsQ0FBQ1YsZ0JBQUgsQ0FBb0IsV0FBcEIsRUFBaUMvSixNQUFqQyxLQUEwQyxDQUE3QyxFQUErQztBQUFDZ0YscUJBQVMsQ0FBQzNILElBQVYsQ0FBZSxVQUFmLEVBQTBCLFdBQTFCO0FBQXdDOztBQUM5SG9OLFlBQUUsQ0FBQ1YsZ0JBQUgsQ0FBb0IsTUFBcEI7QUFBNEIvRSxtQkFBUyxDQUFDM0gsSUFBVixDQUFlLE1BQWY7QUFBd0IsU0FIZSxDQUFOO0FBR047O0FBQ3ZELFVBQUlRLE9BQU8sQ0FBQ2tQLGVBQVIsR0FBd0I1RixPQUFPLENBQUN1QyxJQUFSLENBQWNqRyxPQUFPLEdBQUNxQixPQUFPLENBQUNyQixPQUFSLElBQWlCcUIsT0FBTyxDQUFDa0kscUJBQXpCLElBQWdEbEksT0FBTyxDQUFDbUksa0JBQXhELElBQTRFbkksT0FBTyxDQUFDb0ksZ0JBQXBGLElBQXNHcEksT0FBTyxDQUFDcUksaUJBQXBJLENBQTVCLEVBQXFMO0FBQUMzQyxjQUFNLENBQUMsVUFBU0MsRUFBVCxFQUFZO0FBQUM1TSxpQkFBTyxDQUFDdVAsaUJBQVIsR0FBMEIzSixPQUFPLENBQUN2RyxJQUFSLENBQWF1TixFQUFiLEVBQWdCLEdBQWhCLENBQTFCO0FBQStDaEgsaUJBQU8sQ0FBQ3ZHLElBQVIsQ0FBYXVOLEVBQWIsRUFBZ0IsV0FBaEI7QUFBNkJ4Rix1QkFBYSxDQUFDNUgsSUFBZCxDQUFtQixJQUFuQixFQUF3QmlKLE9BQXhCO0FBQWtDLFNBQTVILENBQU47QUFBcUk7O0FBQzNUdEIsZUFBUyxHQUFDQSxTQUFTLENBQUNoRixNQUFWLElBQWtCLElBQUl3RyxNQUFKLENBQVd4QixTQUFTLENBQUM4RSxJQUFWLENBQWUsR0FBZixDQUFYLENBQTVCO0FBQTREN0UsbUJBQWEsR0FBQ0EsYUFBYSxDQUFDakYsTUFBZCxJQUFzQixJQUFJd0csTUFBSixDQUFXdkIsYUFBYSxDQUFDNkUsSUFBZCxDQUFtQixHQUFuQixDQUFYLENBQXBDO0FBQXdFK0IsZ0JBQVUsR0FBQzFFLE9BQU8sQ0FBQ3VDLElBQVIsQ0FBYTVFLE9BQU8sQ0FBQ3VJLHVCQUFyQixDQUFYO0FBQXlEbkksY0FBUSxHQUFDMkcsVUFBVSxJQUFFMUUsT0FBTyxDQUFDdUMsSUFBUixDQUFhNUUsT0FBTyxDQUFDSSxRQUFyQixDQUFaLEdBQTJDLFVBQVNXLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsWUFBSXdILEtBQUssR0FBQ3pILENBQUMsQ0FBQzdILFFBQUYsS0FBYSxDQUFiLEdBQWU2SCxDQUFDLENBQUMrRixlQUFqQixHQUFpQy9GLENBQTNDO0FBQUEsWUFBNkMwSCxHQUFHLEdBQUN6SCxDQUFDLElBQUVBLENBQUMsQ0FBQzFHLFVBQXREO0FBQWlFLGVBQU95RyxDQUFDLEtBQUcwSCxHQUFKLElBQVMsQ0FBQyxFQUFFQSxHQUFHLElBQUVBLEdBQUcsQ0FBQ3ZQLFFBQUosS0FBZSxDQUFwQixLQUF3QnNQLEtBQUssQ0FBQ3BJLFFBQU4sR0FBZW9JLEtBQUssQ0FBQ3BJLFFBQU4sQ0FBZXFJLEdBQWYsQ0FBZixHQUFtQzFILENBQUMsQ0FBQ3dILHVCQUFGLElBQTJCeEgsQ0FBQyxDQUFDd0gsdUJBQUYsQ0FBMEJFLEdBQTFCLElBQStCLEVBQXJILENBQUYsQ0FBakI7QUFBOEksT0FBeFEsR0FBeVEsVUFBUzFILENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsWUFBR0EsQ0FBSCxFQUFLO0FBQUMsaUJBQU9BLENBQUMsR0FBQ0EsQ0FBQyxDQUFDMUcsVUFBWCxFQUF1QjtBQUFDLGdCQUFHMEcsQ0FBQyxLQUFHRCxDQUFQLEVBQVM7QUFBQyxxQkFBTyxJQUFQO0FBQWE7QUFBQztBQUFDOztBQUNwaEIsZUFBTyxLQUFQO0FBQWMsT0FEK0s7QUFDOUtELGVBQVMsR0FBQ2lHLFVBQVUsR0FBQyxVQUFTaEcsQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxZQUFHRCxDQUFDLEtBQUdDLENBQVAsRUFBUztBQUFDbEIsc0JBQVksR0FBQyxJQUFiO0FBQWtCLGlCQUFPLENBQVA7QUFBVTs7QUFDeEYsWUFBSTRJLE9BQU8sR0FBQyxDQUFDM0gsQ0FBQyxDQUFDd0gsdUJBQUgsR0FBMkIsQ0FBQ3ZILENBQUMsQ0FBQ3VILHVCQUExQzs7QUFBa0UsWUFBR0csT0FBSCxFQUFXO0FBQUMsaUJBQU9BLE9BQVA7QUFBZ0I7O0FBQzlGQSxlQUFPLEdBQUMsQ0FBQzNILENBQUMsQ0FBQ3NELGFBQUYsSUFBaUJ0RCxDQUFsQixNQUF1QkMsQ0FBQyxDQUFDcUQsYUFBRixJQUFpQnJELENBQXhDLElBQTJDRCxDQUFDLENBQUN3SCx1QkFBRixDQUEwQnZILENBQTFCLENBQTNDLEdBQXdFLENBQWhGOztBQUFrRixZQUFHMEgsT0FBTyxHQUFDLENBQVIsSUFBWSxDQUFDM1AsT0FBTyxDQUFDNFAsWUFBVCxJQUF1QjNILENBQUMsQ0FBQ3VILHVCQUFGLENBQTBCeEgsQ0FBMUIsTUFBK0IySCxPQUFyRSxFQUE4RTtBQUFDLGNBQUczSCxDQUFDLElBQUV2SixRQUFILElBQWF1SixDQUFDLENBQUNzRCxhQUFGLElBQWlCL0QsWUFBakIsSUFBK0JGLFFBQVEsQ0FBQ0UsWUFBRCxFQUFjUyxDQUFkLENBQXZELEVBQXdFO0FBQUMsbUJBQU0sQ0FBQyxDQUFQO0FBQVU7O0FBQ3BQLGNBQUdDLENBQUMsSUFBRXhKLFFBQUgsSUFBYXdKLENBQUMsQ0FBQ3FELGFBQUYsSUFBaUIvRCxZQUFqQixJQUErQkYsUUFBUSxDQUFDRSxZQUFELEVBQWNVLENBQWQsQ0FBdkQsRUFBd0U7QUFBQyxtQkFBTyxDQUFQO0FBQVU7O0FBQ25GLGlCQUFPbkIsU0FBUyxHQUFFckgsT0FBTyxDQUFDcUgsU0FBRCxFQUFXa0IsQ0FBWCxDQUFQLEdBQXFCdkksT0FBTyxDQUFDcUgsU0FBRCxFQUFXbUIsQ0FBWCxDQUE5QixHQUE2QyxDQUE3RDtBQUFnRTs7QUFDaEUsZUFBTzBILE9BQU8sR0FBQyxDQUFSLEdBQVUsQ0FBQyxDQUFYLEdBQWEsQ0FBcEI7QUFBdUIsT0FMWSxHQUtYLFVBQVMzSCxDQUFULEVBQVdDLENBQVgsRUFBYTtBQUFDLFlBQUdELENBQUMsS0FBR0MsQ0FBUCxFQUFTO0FBQUNsQixzQkFBWSxHQUFDLElBQWI7QUFBa0IsaUJBQU8sQ0FBUDtBQUFVOztBQUM1RSxZQUFJbUcsR0FBSjtBQUFBLFlBQVFwTSxDQUFDLEdBQUMsQ0FBVjtBQUFBLFlBQVkrTyxHQUFHLEdBQUM3SCxDQUFDLENBQUN6RyxVQUFsQjtBQUFBLFlBQTZCbU8sR0FBRyxHQUFDekgsQ0FBQyxDQUFDMUcsVUFBbkM7QUFBQSxZQUE4Q3VPLEVBQUUsR0FBQyxDQUFDOUgsQ0FBRCxDQUFqRDtBQUFBLFlBQXFEK0gsRUFBRSxHQUFDLENBQUM5SCxDQUFELENBQXhEOztBQUE0RCxZQUFHLENBQUM0SCxHQUFELElBQU0sQ0FBQ0gsR0FBVixFQUFjO0FBQUMsaUJBQU8xSCxDQUFDLElBQUV2SixRQUFILEdBQVksQ0FBQyxDQUFiLEdBQWV3SixDQUFDLElBQUV4SixRQUFILEdBQVksQ0FBWixHQUFjb1IsR0FBRyxHQUFDLENBQUMsQ0FBRixHQUFJSCxHQUFHLEdBQUMsQ0FBRCxHQUFHNUksU0FBUyxHQUFFckgsT0FBTyxDQUFDcUgsU0FBRCxFQUFXa0IsQ0FBWCxDQUFQLEdBQXFCdkksT0FBTyxDQUFDcUgsU0FBRCxFQUFXbUIsQ0FBWCxDQUE5QixHQUE2QyxDQUF2RztBQUEwRyxTQUF6SCxNQUE4SCxJQUFHNEgsR0FBRyxLQUFHSCxHQUFULEVBQWE7QUFBQyxpQkFBT3pDLFlBQVksQ0FBQ2pGLENBQUQsRUFBR0MsQ0FBSCxDQUFuQjtBQUEwQjs7QUFDbE9pRixXQUFHLEdBQUNsRixDQUFKOztBQUFNLGVBQU9rRixHQUFHLEdBQUNBLEdBQUcsQ0FBQzNMLFVBQWYsRUFBMkI7QUFBQ3VPLFlBQUUsQ0FBQ0UsT0FBSCxDQUFXOUMsR0FBWDtBQUFpQjs7QUFDbkRBLFdBQUcsR0FBQ2pGLENBQUo7O0FBQU0sZUFBT2lGLEdBQUcsR0FBQ0EsR0FBRyxDQUFDM0wsVUFBZixFQUEyQjtBQUFDd08sWUFBRSxDQUFDQyxPQUFILENBQVc5QyxHQUFYO0FBQWlCOztBQUNuRCxlQUFNNEMsRUFBRSxDQUFDaFAsQ0FBRCxDQUFGLEtBQVFpUCxFQUFFLENBQUNqUCxDQUFELENBQWhCLEVBQW9CO0FBQUNBLFdBQUM7QUFBSTs7QUFDMUIsZUFBT0EsQ0FBQyxHQUFDbU0sWUFBWSxDQUFDNkMsRUFBRSxDQUFDaFAsQ0FBRCxDQUFILEVBQU9pUCxFQUFFLENBQUNqUCxDQUFELENBQVQsQ0FBYixHQUEyQmdQLEVBQUUsQ0FBQ2hQLENBQUQsQ0FBRixJQUFPeUcsWUFBUCxHQUFvQixDQUFDLENBQXJCLEdBQXVCd0ksRUFBRSxDQUFDalAsQ0FBRCxDQUFGLElBQU95RyxZQUFQLEdBQW9CLENBQXBCLEdBQXNCLENBQWhGO0FBQW1GLE9BVnBFO0FBVXFFLGFBQU85SSxRQUFQO0FBQWlCLEtBOUI0Rzs7QUE4QjNHNkgsVUFBTSxDQUFDVixPQUFQLEdBQWUsVUFBU3FLLElBQVQsRUFBY0MsUUFBZCxFQUF1QjtBQUFDLGFBQU81SixNQUFNLENBQUMySixJQUFELEVBQU0sSUFBTixFQUFXLElBQVgsRUFBZ0JDLFFBQWhCLENBQWI7QUFBd0MsS0FBL0U7O0FBQWdGNUosVUFBTSxDQUFDNEksZUFBUCxHQUF1QixVQUFTbk0sSUFBVCxFQUFja04sSUFBZCxFQUFtQjtBQUFDakosaUJBQVcsQ0FBQ2pFLElBQUQsQ0FBWDs7QUFBa0IsVUFBRy9DLE9BQU8sQ0FBQ2tQLGVBQVIsSUFBeUJoSSxjQUF6QixJQUF5QyxDQUFDWSxzQkFBc0IsQ0FBQ21JLElBQUksR0FBQyxHQUFOLENBQWhFLEtBQTZFLENBQUM3SSxhQUFELElBQWdCLENBQUNBLGFBQWEsQ0FBQ3lFLElBQWQsQ0FBbUJvRSxJQUFuQixDQUE5RixNQUEwSCxDQUFDOUksU0FBRCxJQUFZLENBQUNBLFNBQVMsQ0FBQzBFLElBQVYsQ0FBZW9FLElBQWYsQ0FBdkksQ0FBSCxFQUFnSztBQUFDLFlBQUc7QUFBQyxjQUFJeE4sR0FBRyxHQUFDbUQsT0FBTyxDQUFDdkcsSUFBUixDQUFhMEQsSUFBYixFQUFrQmtOLElBQWxCLENBQVI7O0FBQWdDLGNBQUd4TixHQUFHLElBQUV6QyxPQUFPLENBQUN1UCxpQkFBYixJQUFnQ3hNLElBQUksQ0FBQ3RFLFFBQUwsSUFBZXNFLElBQUksQ0FBQ3RFLFFBQUwsQ0FBYzBCLFFBQWQsS0FBeUIsRUFBM0UsRUFBOEU7QUFBQyxtQkFBT3NDLEdBQVA7QUFBWTtBQUFDLFNBQWhJLENBQWdJLE9BQU1vSSxDQUFOLEVBQVE7QUFBQy9DLGdDQUFzQixDQUFDbUksSUFBRCxFQUFNLElBQU4sQ0FBdEI7QUFBbUM7QUFBQzs7QUFDamtCLGFBQU8zSixNQUFNLENBQUMySixJQUFELEVBQU14UixRQUFOLEVBQWUsSUFBZixFQUFvQixDQUFDc0UsSUFBRCxDQUFwQixDQUFOLENBQWtDWixNQUFsQyxHQUF5QyxDQUFoRDtBQUFtRCxLQURtSTs7QUFDbEltRSxVQUFNLENBQUNlLFFBQVAsR0FBZ0IsVUFBU3hGLE9BQVQsRUFBaUJrQixJQUFqQixFQUFzQjtBQUFDLFVBQUcsQ0FBQ2xCLE9BQU8sQ0FBQ3lKLGFBQVIsSUFBdUJ6SixPQUF4QixLQUFrQ3BELFFBQXJDLEVBQThDO0FBQUN1SSxtQkFBVyxDQUFDbkYsT0FBRCxDQUFYO0FBQXNCOztBQUNoSyxhQUFPd0YsUUFBUSxDQUFDeEYsT0FBRCxFQUFTa0IsSUFBVCxDQUFmO0FBQStCLEtBRHFCOztBQUNwQnVELFVBQU0sQ0FBQzZKLElBQVAsR0FBWSxVQUFTcE4sSUFBVCxFQUFjZ0IsSUFBZCxFQUFtQjtBQUFDLFVBQUcsQ0FBQ2hCLElBQUksQ0FBQ3VJLGFBQUwsSUFBb0J2SSxJQUFyQixLQUE0QnRFLFFBQS9CLEVBQXdDO0FBQUN1SSxtQkFBVyxDQUFDakUsSUFBRCxDQUFYO0FBQW1COztBQUM1SCxVQUFJakIsRUFBRSxHQUFDeUUsSUFBSSxDQUFDeUcsVUFBTCxDQUFnQmpKLElBQUksQ0FBQ3NDLFdBQUwsRUFBaEIsQ0FBUDtBQUFBLFVBQTJDdEYsR0FBRyxHQUFDZSxFQUFFLElBQUVsQyxNQUFNLENBQUNQLElBQVAsQ0FBWWtILElBQUksQ0FBQ3lHLFVBQWpCLEVBQTRCakosSUFBSSxDQUFDc0MsV0FBTCxFQUE1QixDQUFKLEdBQW9EdkUsRUFBRSxDQUFDaUIsSUFBRCxFQUFNZ0IsSUFBTixFQUFXLENBQUNtRCxjQUFaLENBQXRELEdBQWtGMUMsU0FBakk7QUFBMkksYUFBT3pELEdBQUcsS0FBR3lELFNBQU4sR0FBZ0J6RCxHQUFoQixHQUFvQmYsT0FBTyxDQUFDd0ksVUFBUixJQUFvQixDQUFDdEIsY0FBckIsR0FBb0NuRSxJQUFJLENBQUM1QixZQUFMLENBQWtCNEMsSUFBbEIsQ0FBcEMsR0FBNEQsQ0FBQ2hELEdBQUcsR0FBQ2dDLElBQUksQ0FBQzhMLGdCQUFMLENBQXNCOUssSUFBdEIsQ0FBTCxLQUFtQ2hELEdBQUcsQ0FBQ3FQLFNBQXZDLEdBQWlEclAsR0FBRyxDQUFDZ0YsS0FBckQsR0FBMkQsSUFBbEo7QUFBd0osS0FEblE7O0FBQ29RTyxVQUFNLENBQUNxRCxNQUFQLEdBQWMsVUFBUzBHLEdBQVQsRUFBYTtBQUFDLGFBQU0sQ0FBQ0EsR0FBRyxHQUFDLEVBQUwsRUFBU3pMLE9BQVQsQ0FBaUJvRixVQUFqQixFQUE0QkMsVUFBNUIsQ0FBTjtBQUErQyxLQUEzRTs7QUFBNEUzRCxVQUFNLENBQUN4QixLQUFQLEdBQWEsVUFBU0MsR0FBVCxFQUFhO0FBQUMsWUFBTSxJQUFJcEcsS0FBSixDQUFVLDRDQUEwQ29HLEdBQXBELENBQU47QUFBZ0UsS0FBM0Y7O0FBQTRGdUIsVUFBTSxDQUFDZ0ssVUFBUCxHQUFrQixVQUFTL0ssT0FBVCxFQUFpQjtBQUFDLFVBQUl4QyxJQUFKO0FBQUEsVUFBU3dOLFVBQVUsR0FBQyxFQUFwQjtBQUFBLFVBQXVCOU0sQ0FBQyxHQUFDLENBQXpCO0FBQUEsVUFBMkIzQyxDQUFDLEdBQUMsQ0FBN0I7QUFBK0JpRyxrQkFBWSxHQUFDLENBQUMvRyxPQUFPLENBQUN3USxnQkFBdEI7QUFBdUMxSixlQUFTLEdBQUMsQ0FBQzlHLE9BQU8sQ0FBQ3lRLFVBQVQsSUFBcUJsTCxPQUFPLENBQUNyRyxLQUFSLENBQWMsQ0FBZCxDQUEvQjtBQUFnRHFHLGFBQU8sQ0FBQzVCLElBQVIsQ0FBYW9FLFNBQWI7O0FBQXdCLFVBQUdoQixZQUFILEVBQWdCO0FBQUMsZUFBT2hFLElBQUksR0FBQ3dDLE9BQU8sQ0FBQ3pFLENBQUMsRUFBRixDQUFuQixFQUEwQjtBQUFDLGNBQUdpQyxJQUFJLEtBQUd3QyxPQUFPLENBQUN6RSxDQUFELENBQWpCLEVBQXFCO0FBQUMyQyxhQUFDLEdBQUM4TSxVQUFVLENBQUMvUSxJQUFYLENBQWdCc0IsQ0FBaEIsQ0FBRjtBQUFzQjtBQUFDOztBQUN2dEIsZUFBTTJDLENBQUMsRUFBUCxFQUFVO0FBQUM4QixpQkFBTyxDQUFDM0IsTUFBUixDQUFlMk0sVUFBVSxDQUFDOU0sQ0FBRCxDQUF6QixFQUE2QixDQUE3QjtBQUFpQztBQUFDOztBQUM3Q3FELGVBQVMsR0FBQyxJQUFWO0FBQWUsYUFBT3ZCLE9BQVA7QUFBZ0IsS0FGNmE7O0FBRTVhaUIsV0FBTyxHQUFDRixNQUFNLENBQUNFLE9BQVAsR0FBZSxVQUFTekQsSUFBVCxFQUFjO0FBQUMsVUFBSW5DLElBQUo7QUFBQSxVQUFTNkIsR0FBRyxHQUFDLEVBQWI7QUFBQSxVQUFnQjNCLENBQUMsR0FBQyxDQUFsQjtBQUFBLFVBQW9CWCxRQUFRLEdBQUM0QyxJQUFJLENBQUM1QyxRQUFsQzs7QUFBMkMsVUFBRyxDQUFDQSxRQUFKLEVBQWE7QUFBQyxlQUFPUyxJQUFJLEdBQUNtQyxJQUFJLENBQUNqQyxDQUFDLEVBQUYsQ0FBaEIsRUFBdUI7QUFBQzJCLGFBQUcsSUFBRStELE9BQU8sQ0FBQzVGLElBQUQsQ0FBWjtBQUFvQjtBQUFDLE9BQTNELE1BQWdFLElBQUdULFFBQVEsS0FBRyxDQUFYLElBQWNBLFFBQVEsS0FBRyxDQUF6QixJQUE0QkEsUUFBUSxLQUFHLEVBQTFDLEVBQTZDO0FBQUMsWUFBRyxPQUFPNEMsSUFBSSxDQUFDMk4sV0FBWixLQUEwQixRQUE3QixFQUFzQztBQUFDLGlCQUFPM04sSUFBSSxDQUFDMk4sV0FBWjtBQUF5QixTQUFoRSxNQUFvRTtBQUFDLGVBQUkzTixJQUFJLEdBQUNBLElBQUksQ0FBQzROLFVBQWQsRUFBeUI1TixJQUF6QixFQUE4QkEsSUFBSSxHQUFDQSxJQUFJLENBQUNzSyxXQUF4QyxFQUFvRDtBQUFDNUssZUFBRyxJQUFFK0QsT0FBTyxDQUFDekQsSUFBRCxDQUFaO0FBQW9CO0FBQUM7QUFBQyxPQUE5TCxNQUFtTSxJQUFHNUMsUUFBUSxLQUFHLENBQVgsSUFBY0EsUUFBUSxLQUFHLENBQTVCLEVBQThCO0FBQUMsZUFBTzRDLElBQUksQ0FBQzZOLFNBQVo7QUFBdUI7O0FBQzFhLGFBQU9uTyxHQUFQO0FBQVksS0FEb0I7O0FBQ25COEQsUUFBSSxHQUFDRCxNQUFNLENBQUN1SyxTQUFQLEdBQWlCO0FBQUNyRSxpQkFBVyxFQUFDLEVBQWI7QUFBZ0JzRSxrQkFBWSxFQUFDcEUsWUFBN0I7QUFBMEN4QixXQUFLLEVBQUNoQyxTQUFoRDtBQUEwRDhELGdCQUFVLEVBQUMsRUFBckU7QUFBd0U0QixVQUFJLEVBQUMsRUFBN0U7QUFBZ0ZtQyxjQUFRLEVBQUM7QUFBQyxhQUFJO0FBQUNyRyxhQUFHLEVBQUMsWUFBTDtBQUFrQnpILGVBQUssRUFBQztBQUF4QixTQUFMO0FBQW1DLGFBQUk7QUFBQ3lILGFBQUcsRUFBQztBQUFMLFNBQXZDO0FBQTBELGFBQUk7QUFBQ0EsYUFBRyxFQUFDLGlCQUFMO0FBQXVCekgsZUFBSyxFQUFDO0FBQTdCLFNBQTlEO0FBQWlHLGFBQUk7QUFBQ3lILGFBQUcsRUFBQztBQUFMO0FBQXJHLE9BQXpGO0FBQXVOc0csZUFBUyxFQUFDO0FBQUMsZ0JBQU8sY0FBUzlGLEtBQVQsRUFBZTtBQUFDQSxlQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVNBLEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBU3RHLE9BQVQsQ0FBaUI2RSxTQUFqQixFQUEyQkMsU0FBM0IsQ0FBVDtBQUErQ3dCLGVBQUssQ0FBQyxDQUFELENBQUwsR0FBUyxDQUFDQSxLQUFLLENBQUMsQ0FBRCxDQUFMLElBQVVBLEtBQUssQ0FBQyxDQUFELENBQWYsSUFBb0JBLEtBQUssQ0FBQyxDQUFELENBQXpCLElBQThCLEVBQS9CLEVBQW1DdEcsT0FBbkMsQ0FBMkM2RSxTQUEzQyxFQUFxREMsU0FBckQsQ0FBVDs7QUFBeUUsY0FBR3dCLEtBQUssQ0FBQyxDQUFELENBQUwsS0FBVyxJQUFkLEVBQW1CO0FBQUNBLGlCQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVMsTUFBSUEsS0FBSyxDQUFDLENBQUQsQ0FBVCxHQUFhLEdBQXRCO0FBQTJCOztBQUNuYyxpQkFBT0EsS0FBSyxDQUFDaE0sS0FBTixDQUFZLENBQVosRUFBYyxDQUFkLENBQVA7QUFBeUIsU0FEMk87QUFDMU8saUJBQVEsZUFBU2dNLEtBQVQsRUFBZTtBQUFDQSxlQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVNBLEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBUzdFLFdBQVQsRUFBVDs7QUFBZ0MsY0FBRzZFLEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBU2hNLEtBQVQsQ0FBZSxDQUFmLEVBQWlCLENBQWpCLE1BQXNCLEtBQXpCLEVBQStCO0FBQUMsZ0JBQUcsQ0FBQ2dNLEtBQUssQ0FBQyxDQUFELENBQVQsRUFBYTtBQUFDNUUsb0JBQU0sQ0FBQ3hCLEtBQVAsQ0FBYW9HLEtBQUssQ0FBQyxDQUFELENBQWxCO0FBQXdCOztBQUN4SkEsaUJBQUssQ0FBQyxDQUFELENBQUwsR0FBUyxFQUFFQSxLQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVNBLEtBQUssQ0FBQyxDQUFELENBQUwsSUFBVUEsS0FBSyxDQUFDLENBQUQsQ0FBTCxJQUFVLENBQXBCLENBQVQsR0FBZ0MsS0FBR0EsS0FBSyxDQUFDLENBQUQsQ0FBTCxLQUFXLE1BQVgsSUFBbUJBLEtBQUssQ0FBQyxDQUFELENBQUwsS0FBVyxLQUFqQyxDQUFsQyxDQUFUO0FBQW9GQSxpQkFBSyxDQUFDLENBQUQsQ0FBTCxHQUFTLEVBQUdBLEtBQUssQ0FBQyxDQUFELENBQUwsR0FBU0EsS0FBSyxDQUFDLENBQUQsQ0FBZixJQUFxQkEsS0FBSyxDQUFDLENBQUQsQ0FBTCxLQUFXLEtBQWxDLENBQVQ7QUFBbUQsV0FEckQsTUFDMEQsSUFBR0EsS0FBSyxDQUFDLENBQUQsQ0FBUixFQUFZO0FBQUM1RSxrQkFBTSxDQUFDeEIsS0FBUCxDQUFhb0csS0FBSyxDQUFDLENBQUQsQ0FBbEI7QUFBd0I7O0FBQ2pMLGlCQUFPQSxLQUFQO0FBQWMsU0FIc1A7QUFHclAsa0JBQVMsZ0JBQVNBLEtBQVQsRUFBZTtBQUFDLGNBQUkrRixNQUFKO0FBQUEsY0FBV0MsUUFBUSxHQUFDLENBQUNoRyxLQUFLLENBQUMsQ0FBRCxDQUFOLElBQVdBLEtBQUssQ0FBQyxDQUFELENBQXBDOztBQUF3QyxjQUFHaEMsU0FBUyxDQUFDLE9BQUQsQ0FBVCxDQUFtQjJDLElBQW5CLENBQXdCWCxLQUFLLENBQUMsQ0FBRCxDQUE3QixDQUFILEVBQXFDO0FBQUMsbUJBQU8sSUFBUDtBQUFhOztBQUNuSSxjQUFHQSxLQUFLLENBQUMsQ0FBRCxDQUFSLEVBQVk7QUFBQ0EsaUJBQUssQ0FBQyxDQUFELENBQUwsR0FBU0EsS0FBSyxDQUFDLENBQUQsQ0FBTCxJQUFVQSxLQUFLLENBQUMsQ0FBRCxDQUFmLElBQW9CLEVBQTdCO0FBQWlDLFdBQTlDLE1BQW1ELElBQUdnRyxRQUFRLElBQUVsSSxPQUFPLENBQUM2QyxJQUFSLENBQWFxRixRQUFiLENBQVYsS0FBbUNELE1BQU0sR0FBQ3ZLLFFBQVEsQ0FBQ3dLLFFBQUQsRUFBVSxJQUFWLENBQWxELE1BQXFFRCxNQUFNLEdBQUNDLFFBQVEsQ0FBQ3pSLE9BQVQsQ0FBaUIsR0FBakIsRUFBcUJ5UixRQUFRLENBQUMvTyxNQUFULEdBQWdCOE8sTUFBckMsSUFBNkNDLFFBQVEsQ0FBQy9PLE1BQWxJLENBQUgsRUFBNkk7QUFBQytJLGlCQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVNBLEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBU2hNLEtBQVQsQ0FBZSxDQUFmLEVBQWlCK1IsTUFBakIsQ0FBVDtBQUFrQy9GLGlCQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVNnRyxRQUFRLENBQUNoUyxLQUFULENBQWUsQ0FBZixFQUFpQitSLE1BQWpCLENBQVQ7QUFBbUM7O0FBQ3RRLGlCQUFPL0YsS0FBSyxDQUFDaE0sS0FBTixDQUFZLENBQVosRUFBYyxDQUFkLENBQVA7QUFBeUI7QUFMMk8sT0FBak87QUFLUndQLFlBQU0sRUFBQztBQUFDLGVBQU0sYUFBU3lDLGdCQUFULEVBQTBCO0FBQUMsY0FBSTFHLFFBQVEsR0FBQzBHLGdCQUFnQixDQUFDdk0sT0FBakIsQ0FBeUI2RSxTQUF6QixFQUFtQ0MsU0FBbkMsRUFBOENyRCxXQUE5QyxFQUFiO0FBQXlFLGlCQUFPOEssZ0JBQWdCLEtBQUcsR0FBbkIsR0FBdUIsWUFBVTtBQUFDLG1CQUFPLElBQVA7QUFBYSxXQUEvQyxHQUFnRCxVQUFTcE8sSUFBVCxFQUFjO0FBQUMsbUJBQU9BLElBQUksQ0FBQzBILFFBQUwsSUFBZTFILElBQUksQ0FBQzBILFFBQUwsQ0FBY3BFLFdBQWQsT0FBOEJvRSxRQUFwRDtBQUE4RCxXQUFwSTtBQUFzSSxTQUFqUDtBQUFrUCxpQkFBUSxlQUFTNkQsU0FBVCxFQUFtQjtBQUFDLGNBQUk4QyxPQUFPLEdBQUMxSixVQUFVLENBQUM0RyxTQUFTLEdBQUMsR0FBWCxDQUF0QjtBQUFzQyxpQkFBTzhDLE9BQU8sSUFBRSxDQUFDQSxPQUFPLEdBQUMsSUFBSXpJLE1BQUosQ0FBVyxRQUFNTCxVQUFOLEdBQWlCLEdBQWpCLEdBQXFCZ0csU0FBckIsR0FBK0IsR0FBL0IsR0FBbUNoRyxVQUFuQyxHQUE4QyxLQUF6RCxDQUFULEtBQTJFWixVQUFVLENBQUM0RyxTQUFELEVBQVcsVUFBU3ZMLElBQVQsRUFBYztBQUFDLG1CQUFPcU8sT0FBTyxDQUFDdkYsSUFBUixDQUFhLE9BQU85SSxJQUFJLENBQUN1TCxTQUFaLEtBQXdCLFFBQXhCLElBQWtDdkwsSUFBSSxDQUFDdUwsU0FBdkMsSUFBa0QsT0FBT3ZMLElBQUksQ0FBQzVCLFlBQVosS0FBMkIsV0FBM0IsSUFBd0M0QixJQUFJLENBQUM1QixZQUFMLENBQWtCLE9BQWxCLENBQTFGLElBQXNILEVBQW5JLENBQVA7QUFBK0ksV0FBekssQ0FBckc7QUFBaVIsU0FBcmtCO0FBQXNrQixnQkFBTyxjQUFTNEMsSUFBVCxFQUFjc04sUUFBZCxFQUF1QkMsS0FBdkIsRUFBNkI7QUFBQyxpQkFBTyxVQUFTdk8sSUFBVCxFQUFjO0FBQUMsZ0JBQUl3TyxNQUFNLEdBQUNqTCxNQUFNLENBQUM2SixJQUFQLENBQVlwTixJQUFaLEVBQWlCZ0IsSUFBakIsQ0FBWDs7QUFBa0MsZ0JBQUd3TixNQUFNLElBQUUsSUFBWCxFQUFnQjtBQUFDLHFCQUFPRixRQUFRLEtBQUcsSUFBbEI7QUFBd0I7O0FBQzl1QixnQkFBRyxDQUFDQSxRQUFKLEVBQWE7QUFBQyxxQkFBTyxJQUFQO0FBQWE7O0FBQzNCRSxrQkFBTSxJQUFFLEVBQVI7QUFBVyxtQkFBT0YsUUFBUSxLQUFHLEdBQVgsR0FBZUUsTUFBTSxLQUFHRCxLQUF4QixHQUE4QkQsUUFBUSxLQUFHLElBQVgsR0FBZ0JFLE1BQU0sS0FBR0QsS0FBekIsR0FBK0JELFFBQVEsS0FBRyxJQUFYLEdBQWdCQyxLQUFLLElBQUVDLE1BQU0sQ0FBQzlSLE9BQVAsQ0FBZTZSLEtBQWYsTUFBd0IsQ0FBL0MsR0FBaURELFFBQVEsS0FBRyxJQUFYLEdBQWdCQyxLQUFLLElBQUVDLE1BQU0sQ0FBQzlSLE9BQVAsQ0FBZTZSLEtBQWYsSUFBc0IsQ0FBQyxDQUE5QyxHQUFnREQsUUFBUSxLQUFHLElBQVgsR0FBZ0JDLEtBQUssSUFBRUMsTUFBTSxDQUFDclMsS0FBUCxDQUFhLENBQUNvUyxLQUFLLENBQUNuUCxNQUFwQixNQUE4Qm1QLEtBQXJELEdBQTJERCxRQUFRLEtBQUcsSUFBWCxHQUFnQixDQUFDLE1BQUlFLE1BQU0sQ0FBQzNNLE9BQVAsQ0FBZThELFdBQWYsRUFBMkIsR0FBM0IsQ0FBSixHQUFvQyxHQUFyQyxFQUEwQ2pKLE9BQTFDLENBQWtENlIsS0FBbEQsSUFBeUQsQ0FBQyxDQUExRSxHQUE0RUQsUUFBUSxLQUFHLElBQVgsR0FBZ0JFLE1BQU0sS0FBR0QsS0FBVCxJQUFnQkMsTUFBTSxDQUFDclMsS0FBUCxDQUFhLENBQWIsRUFBZW9TLEtBQUssQ0FBQ25QLE1BQU4sR0FBYSxDQUE1QixNQUFpQ21QLEtBQUssR0FBQyxHQUF2RSxHQUEyRSxLQUF2WDtBQUE4WCxXQUZvUTtBQUVsUSxTQUZ6VztBQUUwVyxpQkFBUSxlQUFTaFIsSUFBVCxFQUFja1IsSUFBZCxFQUFtQkMsU0FBbkIsRUFBNkJ4TyxLQUE3QixFQUFtQ0UsSUFBbkMsRUFBd0M7QUFBQyxjQUFJdU8sTUFBTSxHQUFDcFIsSUFBSSxDQUFDcEIsS0FBTCxDQUFXLENBQVgsRUFBYSxDQUFiLE1BQWtCLEtBQTdCO0FBQUEsY0FBbUN5UyxPQUFPLEdBQUNyUixJQUFJLENBQUNwQixLQUFMLENBQVcsQ0FBQyxDQUFaLE1BQWlCLE1BQTVEO0FBQUEsY0FBbUUwUyxNQUFNLEdBQUNKLElBQUksS0FBRyxTQUFqRjtBQUEyRixpQkFBT3ZPLEtBQUssS0FBRyxDQUFSLElBQVdFLElBQUksS0FBRyxDQUFsQixHQUFvQixVQUFTSixJQUFULEVBQWM7QUFBQyxtQkFBTSxDQUFDLENBQUNBLElBQUksQ0FBQ3hCLFVBQWI7QUFBeUIsV0FBNUQsR0FBNkQsVUFBU3dCLElBQVQsRUFBYzhPLFFBQWQsRUFBdUJDLEdBQXZCLEVBQTJCO0FBQUMsZ0JBQUl4RixLQUFKO0FBQUEsZ0JBQVV5RixXQUFWO0FBQUEsZ0JBQXNCQyxVQUF0QjtBQUFBLGdCQUFpQ3BSLElBQWpDO0FBQUEsZ0JBQXNDcVIsU0FBdEM7QUFBQSxnQkFBZ0RDLEtBQWhEO0FBQUEsZ0JBQXNEeEgsR0FBRyxHQUFDZ0gsTUFBTSxLQUFHQyxPQUFULEdBQWlCLGFBQWpCLEdBQStCLGlCQUF6RjtBQUFBLGdCQUEyR1EsTUFBTSxHQUFDcFAsSUFBSSxDQUFDeEIsVUFBdkg7QUFBQSxnQkFBa0l3QyxJQUFJLEdBQUM2TixNQUFNLElBQUU3TyxJQUFJLENBQUMwSCxRQUFMLENBQWNwRSxXQUFkLEVBQS9JO0FBQUEsZ0JBQTJLK0wsUUFBUSxHQUFDLENBQUNOLEdBQUQsSUFBTSxDQUFDRixNQUEzTDtBQUFBLGdCQUFrTXpFLElBQUksR0FBQyxLQUF2TTs7QUFBNk0sZ0JBQUdnRixNQUFILEVBQVU7QUFBQyxrQkFBR1QsTUFBSCxFQUFVO0FBQUMsdUJBQU1oSCxHQUFOLEVBQVU7QUFBQzlKLHNCQUFJLEdBQUNtQyxJQUFMOztBQUFVLHlCQUFPbkMsSUFBSSxHQUFDQSxJQUFJLENBQUM4SixHQUFELENBQWhCLEVBQXVCO0FBQUMsd0JBQUdrSCxNQUFNLEdBQUNoUixJQUFJLENBQUM2SixRQUFMLENBQWNwRSxXQUFkLE9BQThCdEMsSUFBL0IsR0FBb0NuRCxJQUFJLENBQUNULFFBQUwsS0FBZ0IsQ0FBN0QsRUFBK0Q7QUFBQyw2QkFBTyxLQUFQO0FBQWM7QUFBQzs7QUFDdjlCK1IsdUJBQUssR0FBQ3hILEdBQUcsR0FBQ3BLLElBQUksS0FBRyxNQUFQLElBQWUsQ0FBQzRSLEtBQWhCLElBQXVCLGFBQWpDO0FBQWdEOztBQUNoRCx1QkFBTyxJQUFQO0FBQWE7O0FBQ2JBLG1CQUFLLEdBQUMsQ0FBQ1AsT0FBTyxHQUFDUSxNQUFNLENBQUN4QixVQUFSLEdBQW1Cd0IsTUFBTSxDQUFDRSxTQUFsQyxDQUFOOztBQUFtRCxrQkFBR1YsT0FBTyxJQUFFUyxRQUFaLEVBQXFCO0FBQUN4UixvQkFBSSxHQUFDdVIsTUFBTDtBQUFZSCwwQkFBVSxHQUFDcFIsSUFBSSxDQUFDNkQsT0FBRCxDQUFKLEtBQWdCN0QsSUFBSSxDQUFDNkQsT0FBRCxDQUFKLEdBQWMsRUFBOUIsQ0FBWDtBQUE2Q3NOLDJCQUFXLEdBQUNDLFVBQVUsQ0FBQ3BSLElBQUksQ0FBQzBSLFFBQU4sQ0FBVixLQUE0Qk4sVUFBVSxDQUFDcFIsSUFBSSxDQUFDMFIsUUFBTixDQUFWLEdBQTBCLEVBQXRELENBQVo7QUFBc0VoRyxxQkFBSyxHQUFDeUYsV0FBVyxDQUFDelIsSUFBRCxDQUFYLElBQW1CLEVBQXpCO0FBQTRCMlIseUJBQVMsR0FBQzNGLEtBQUssQ0FBQyxDQUFELENBQUwsS0FBVzlFLE9BQVgsSUFBb0I4RSxLQUFLLENBQUMsQ0FBRCxDQUFuQztBQUF1Q2Esb0JBQUksR0FBQzhFLFNBQVMsSUFBRTNGLEtBQUssQ0FBQyxDQUFELENBQXJCO0FBQXlCMUwsb0JBQUksR0FBQ3FSLFNBQVMsSUFBRUUsTUFBTSxDQUFDdkgsVUFBUCxDQUFrQnFILFNBQWxCLENBQWhCOztBQUE2Qyx1QkFBT3JSLElBQUksR0FBQyxFQUFFcVIsU0FBRixJQUFhclIsSUFBYixJQUFtQkEsSUFBSSxDQUFDOEosR0FBRCxDQUF2QixLQUErQnlDLElBQUksR0FBQzhFLFNBQVMsR0FBQyxDQUE5QyxLQUFrREMsS0FBSyxDQUFDaEssR0FBTixFQUE5RCxFQUEyRTtBQUFDLHNCQUFHdEgsSUFBSSxDQUFDVCxRQUFMLEtBQWdCLENBQWhCLElBQW1CLEVBQUVnTixJQUFyQixJQUEyQnZNLElBQUksS0FBR21DLElBQXJDLEVBQTBDO0FBQUNnUCwrQkFBVyxDQUFDelIsSUFBRCxDQUFYLEdBQWtCLENBQUNrSCxPQUFELEVBQVN5SyxTQUFULEVBQW1COUUsSUFBbkIsQ0FBbEI7QUFBMkM7QUFBTztBQUFDO0FBQUMsZUFBemMsTUFBNmM7QUFBQyxvQkFBR2lGLFFBQUgsRUFBWTtBQUFDeFIsc0JBQUksR0FBQ21DLElBQUw7QUFBVWlQLDRCQUFVLEdBQUNwUixJQUFJLENBQUM2RCxPQUFELENBQUosS0FBZ0I3RCxJQUFJLENBQUM2RCxPQUFELENBQUosR0FBYyxFQUE5QixDQUFYO0FBQTZDc04sNkJBQVcsR0FBQ0MsVUFBVSxDQUFDcFIsSUFBSSxDQUFDMFIsUUFBTixDQUFWLEtBQTRCTixVQUFVLENBQUNwUixJQUFJLENBQUMwUixRQUFOLENBQVYsR0FBMEIsRUFBdEQsQ0FBWjtBQUFzRWhHLHVCQUFLLEdBQUN5RixXQUFXLENBQUN6UixJQUFELENBQVgsSUFBbUIsRUFBekI7QUFBNEIyUiwyQkFBUyxHQUFDM0YsS0FBSyxDQUFDLENBQUQsQ0FBTCxLQUFXOUUsT0FBWCxJQUFvQjhFLEtBQUssQ0FBQyxDQUFELENBQW5DO0FBQXVDYSxzQkFBSSxHQUFDOEUsU0FBTDtBQUFnQjs7QUFDOXRCLG9CQUFHOUUsSUFBSSxLQUFHLEtBQVYsRUFBZ0I7QUFBQyx5QkFBT3ZNLElBQUksR0FBQyxFQUFFcVIsU0FBRixJQUFhclIsSUFBYixJQUFtQkEsSUFBSSxDQUFDOEosR0FBRCxDQUF2QixLQUErQnlDLElBQUksR0FBQzhFLFNBQVMsR0FBQyxDQUE5QyxLQUFrREMsS0FBSyxDQUFDaEssR0FBTixFQUE5RCxFQUEyRTtBQUFDLHdCQUFHLENBQUMwSixNQUFNLEdBQUNoUixJQUFJLENBQUM2SixRQUFMLENBQWNwRSxXQUFkLE9BQThCdEMsSUFBL0IsR0FBb0NuRCxJQUFJLENBQUNULFFBQUwsS0FBZ0IsQ0FBM0QsS0FBK0QsRUFBRWdOLElBQXBFLEVBQXlFO0FBQUMsMEJBQUdpRixRQUFILEVBQVk7QUFBQ0osa0NBQVUsR0FBQ3BSLElBQUksQ0FBQzZELE9BQUQsQ0FBSixLQUFnQjdELElBQUksQ0FBQzZELE9BQUQsQ0FBSixHQUFjLEVBQTlCLENBQVg7QUFBNkNzTixtQ0FBVyxHQUFDQyxVQUFVLENBQUNwUixJQUFJLENBQUMwUixRQUFOLENBQVYsS0FBNEJOLFVBQVUsQ0FBQ3BSLElBQUksQ0FBQzBSLFFBQU4sQ0FBVixHQUEwQixFQUF0RCxDQUFaO0FBQXNFUCxtQ0FBVyxDQUFDelIsSUFBRCxDQUFYLEdBQWtCLENBQUNrSCxPQUFELEVBQVMyRixJQUFULENBQWxCO0FBQWtDOztBQUN6VSwwQkFBR3ZNLElBQUksS0FBR21DLElBQVYsRUFBZTtBQUFDO0FBQU87QUFBQztBQUFDO0FBQUM7QUFBQzs7QUFDM0JvSyxrQkFBSSxJQUFFaEssSUFBTjtBQUFXLHFCQUFPZ0ssSUFBSSxLQUFHbEssS0FBUCxJQUFla0ssSUFBSSxHQUFDbEssS0FBTCxLQUFhLENBQWIsSUFBZ0JrSyxJQUFJLEdBQUNsSyxLQUFMLElBQVksQ0FBbEQ7QUFBc0Q7QUFBQyxXQU5zZDtBQU1wZCxTQVJsQztBQVFtQyxrQkFBUyxnQkFBU3NQLE1BQVQsRUFBZ0I1RSxRQUFoQixFQUF5QjtBQUFDLGNBQUk2RSxJQUFKO0FBQUEsY0FBUzFRLEVBQUUsR0FBQ3lFLElBQUksQ0FBQ2tDLE9BQUwsQ0FBYThKLE1BQWIsS0FBc0JoTSxJQUFJLENBQUNrTSxVQUFMLENBQWdCRixNQUFNLENBQUNsTSxXQUFQLEVBQWhCLENBQXRCLElBQTZEQyxNQUFNLENBQUN4QixLQUFQLENBQWEseUJBQXVCeU4sTUFBcEMsQ0FBekU7O0FBQXFILGNBQUd6USxFQUFFLENBQUMyQyxPQUFELENBQUwsRUFBZTtBQUFDLG1CQUFPM0MsRUFBRSxDQUFDNkwsUUFBRCxDQUFUO0FBQXFCOztBQUNsUSxjQUFHN0wsRUFBRSxDQUFDSyxNQUFILEdBQVUsQ0FBYixFQUFlO0FBQUNxUSxnQkFBSSxHQUFDLENBQUNELE1BQUQsRUFBUUEsTUFBUixFQUFlLEVBQWYsRUFBa0I1RSxRQUFsQixDQUFMO0FBQWlDLG1CQUFPcEgsSUFBSSxDQUFDa00sVUFBTCxDQUFnQjVTLGNBQWhCLENBQStCMFMsTUFBTSxDQUFDbE0sV0FBUCxFQUEvQixJQUFxRHFHLFlBQVksQ0FBQyxVQUFTM0IsSUFBVCxFQUFjbkYsT0FBZCxFQUFzQjtBQUFDLGtCQUFJOE0sR0FBSjtBQUFBLGtCQUFRQyxPQUFPLEdBQUM3USxFQUFFLENBQUNpSixJQUFELEVBQU00QyxRQUFOLENBQWxCO0FBQUEsa0JBQWtDN00sQ0FBQyxHQUFDNlIsT0FBTyxDQUFDeFEsTUFBNUM7O0FBQW1ELHFCQUFNckIsQ0FBQyxFQUFQLEVBQVU7QUFBQzRSLG1CQUFHLEdBQUNqVCxPQUFPLENBQUNzTCxJQUFELEVBQU00SCxPQUFPLENBQUM3UixDQUFELENBQWIsQ0FBWDtBQUE2QmlLLG9CQUFJLENBQUMySCxHQUFELENBQUosR0FBVSxFQUFFOU0sT0FBTyxDQUFDOE0sR0FBRCxDQUFQLEdBQWFDLE9BQU8sQ0FBQzdSLENBQUQsQ0FBdEIsQ0FBVjtBQUFzQztBQUFDLGFBQTFKLENBQWpFLEdBQTZOLFVBQVNpQyxJQUFULEVBQWM7QUFBQyxxQkFBT2pCLEVBQUUsQ0FBQ2lCLElBQUQsRUFBTSxDQUFOLEVBQVF5UCxJQUFSLENBQVQ7QUFBd0IsYUFBM1E7QUFBNlE7O0FBQzlULGlCQUFPMVEsRUFBUDtBQUFXO0FBVnVCLE9BTEM7QUFldEIyRyxhQUFPLEVBQUM7QUFBQyxlQUFNaUUsWUFBWSxDQUFDLFVBQVM5SyxRQUFULEVBQWtCO0FBQUMsY0FBSW9OLEtBQUssR0FBQyxFQUFWO0FBQUEsY0FBYXpKLE9BQU8sR0FBQyxFQUFyQjtBQUFBLGNBQXdCcU4sT0FBTyxHQUFDak0sT0FBTyxDQUFDL0UsUUFBUSxDQUFDZ0QsT0FBVCxDQUFpQmdFLEtBQWpCLEVBQXVCLElBQXZCLENBQUQsQ0FBdkM7QUFBc0UsaUJBQU9nSyxPQUFPLENBQUNuTyxPQUFELENBQVAsR0FBaUJpSSxZQUFZLENBQUMsVUFBUzNCLElBQVQsRUFBY25GLE9BQWQsRUFBc0JpTSxRQUF0QixFQUErQkMsR0FBL0IsRUFBbUM7QUFBQyxnQkFBSS9PLElBQUo7QUFBQSxnQkFBUzhQLFNBQVMsR0FBQ0QsT0FBTyxDQUFDN0gsSUFBRCxFQUFNLElBQU4sRUFBVytHLEdBQVgsRUFBZSxFQUFmLENBQTFCO0FBQUEsZ0JBQTZDaFIsQ0FBQyxHQUFDaUssSUFBSSxDQUFDNUksTUFBcEQ7O0FBQTJELG1CQUFNckIsQ0FBQyxFQUFQLEVBQVU7QUFBQyxrQkFBSWlDLElBQUksR0FBQzhQLFNBQVMsQ0FBQy9SLENBQUQsQ0FBbEIsRUFBdUI7QUFBQ2lLLG9CQUFJLENBQUNqSyxDQUFELENBQUosR0FBUSxFQUFFOEUsT0FBTyxDQUFDOUUsQ0FBRCxDQUFQLEdBQVdpQyxJQUFiLENBQVI7QUFBNEI7QUFBQztBQUFDLFdBQWpLLENBQTdCLEdBQWdNLFVBQVNBLElBQVQsRUFBYzhPLFFBQWQsRUFBdUJDLEdBQXZCLEVBQTJCO0FBQUM5QyxpQkFBSyxDQUFDLENBQUQsQ0FBTCxHQUFTak0sSUFBVDtBQUFjNlAsbUJBQU8sQ0FBQzVELEtBQUQsRUFBTyxJQUFQLEVBQVk4QyxHQUFaLEVBQWdCdk0sT0FBaEIsQ0FBUDtBQUFnQ3lKLGlCQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVMsSUFBVDtBQUFjLG1CQUFNLENBQUN6SixPQUFPLENBQUMyQyxHQUFSLEVBQVA7QUFBc0IsV0FBclQ7QUFBdVQsU0FBalosQ0FBbkI7QUFBc2EsZUFBTXdFLFlBQVksQ0FBQyxVQUFTOUssUUFBVCxFQUFrQjtBQUFDLGlCQUFPLFVBQVNtQixJQUFULEVBQWM7QUFBQyxtQkFBT3VELE1BQU0sQ0FBQzFFLFFBQUQsRUFBVW1CLElBQVYsQ0FBTixDQUFzQlosTUFBdEIsR0FBNkIsQ0FBcEM7QUFBdUMsV0FBN0Q7QUFBK0QsU0FBbkYsQ0FBeGI7QUFBNmdCLG9CQUFXdUssWUFBWSxDQUFDLFVBQVN4TCxJQUFULEVBQWM7QUFBQ0EsY0FBSSxHQUFDQSxJQUFJLENBQUMwRCxPQUFMLENBQWE2RSxTQUFiLEVBQXVCQyxTQUF2QixDQUFMO0FBQXVDLGlCQUFPLFVBQVMzRyxJQUFULEVBQWM7QUFBQyxtQkFBTSxDQUFDQSxJQUFJLENBQUMyTixXQUFMLElBQWtCbEssT0FBTyxDQUFDekQsSUFBRCxDQUExQixFQUFrQ3RELE9BQWxDLENBQTBDeUIsSUFBMUMsSUFBZ0QsQ0FBQyxDQUF2RDtBQUEwRCxXQUFoRjtBQUFrRixTQUF6SSxDQUFwaUI7QUFBK3FCLGdCQUFPd0wsWUFBWSxDQUFDLFVBQVNvRyxJQUFULEVBQWM7QUFBQyxjQUFHLENBQUM3SixXQUFXLENBQUM0QyxJQUFaLENBQWlCaUgsSUFBSSxJQUFFLEVBQXZCLENBQUosRUFBK0I7QUFBQ3hNLGtCQUFNLENBQUN4QixLQUFQLENBQWEsdUJBQXFCZ08sSUFBbEM7QUFBeUM7O0FBQ2h6QkEsY0FBSSxHQUFDQSxJQUFJLENBQUNsTyxPQUFMLENBQWE2RSxTQUFiLEVBQXVCQyxTQUF2QixFQUFrQ3JELFdBQWxDLEVBQUw7QUFBcUQsaUJBQU8sVUFBU3RELElBQVQsRUFBYztBQUFDLGdCQUFJZ1EsUUFBSjs7QUFBYSxlQUFFO0FBQUMsa0JBQUlBLFFBQVEsR0FBQzdMLGNBQWMsR0FBQ25FLElBQUksQ0FBQytQLElBQU4sR0FBVy9QLElBQUksQ0FBQzVCLFlBQUwsQ0FBa0IsVUFBbEIsS0FBK0I0QixJQUFJLENBQUM1QixZQUFMLENBQWtCLE1BQWxCLENBQXJFLEVBQWdHO0FBQUM0Uix3QkFBUSxHQUFDQSxRQUFRLENBQUMxTSxXQUFULEVBQVQ7QUFBZ0MsdUJBQU8wTSxRQUFRLEtBQUdELElBQVgsSUFBaUJDLFFBQVEsQ0FBQ3RULE9BQVQsQ0FBaUJxVCxJQUFJLEdBQUMsR0FBdEIsTUFBNkIsQ0FBckQ7QUFBd0Q7QUFBQyxhQUE3TCxRQUFtTSxDQUFDL1AsSUFBSSxHQUFDQSxJQUFJLENBQUN4QixVQUFYLEtBQXdCd0IsSUFBSSxDQUFDNUMsUUFBTCxLQUFnQixDQUEzTzs7QUFBOE8sbUJBQU8sS0FBUDtBQUFjLFdBQS9SO0FBQWlTLFNBRGlZLENBQWxzQjtBQUNtVSxrQkFBUyxnQkFBUzRDLElBQVQsRUFBYztBQUFDLGNBQUlpUSxJQUFJLEdBQUNwVSxNQUFNLENBQUNxVSxRQUFQLElBQWlCclUsTUFBTSxDQUFDcVUsUUFBUCxDQUFnQkQsSUFBMUM7QUFBK0MsaUJBQU9BLElBQUksSUFBRUEsSUFBSSxDQUFDOVQsS0FBTCxDQUFXLENBQVgsTUFBZ0I2RCxJQUFJLENBQUMwSSxFQUFsQztBQUFzQyxTQURoYjtBQUNpYixnQkFBTyxjQUFTMUksSUFBVCxFQUFjO0FBQUMsaUJBQU9BLElBQUksS0FBR2tFLE9BQWQ7QUFBdUIsU0FEOWQ7QUFDK2QsaUJBQVEsZUFBU2xFLElBQVQsRUFBYztBQUFDLGlCQUFPQSxJQUFJLEtBQUd0RSxRQUFRLENBQUN5VSxhQUFoQixLQUFnQyxDQUFDelUsUUFBUSxDQUFDMFUsUUFBVixJQUFvQjFVLFFBQVEsQ0FBQzBVLFFBQVQsRUFBcEQsS0FBMEUsQ0FBQyxFQUFFcFEsSUFBSSxDQUFDekMsSUFBTCxJQUFXeUMsSUFBSSxDQUFDcVEsSUFBaEIsSUFBc0IsQ0FBQ3JRLElBQUksQ0FBQ3NRLFFBQTlCLENBQWxGO0FBQTJILFNBRGpuQjtBQUNrbkIsbUJBQVU3RixvQkFBb0IsQ0FBQyxLQUFELENBRGhwQjtBQUN3cEIsb0JBQVdBLG9CQUFvQixDQUFDLElBQUQsQ0FEdnJCO0FBQzhyQixtQkFBVSxpQkFBU3pLLElBQVQsRUFBYztBQUFDLGNBQUkwSCxRQUFRLEdBQUMxSCxJQUFJLENBQUMwSCxRQUFMLENBQWNwRSxXQUFkLEVBQWI7QUFBeUMsaUJBQU9vRSxRQUFRLEtBQUcsT0FBWCxJQUFvQixDQUFDLENBQUMxSCxJQUFJLENBQUN1USxPQUE1QixJQUF1QzdJLFFBQVEsS0FBRyxRQUFYLElBQXFCLENBQUMsQ0FBQzFILElBQUksQ0FBQ3dRLFFBQXpFO0FBQW9GLFNBRHAxQjtBQUNxMUIsb0JBQVcsa0JBQVN4USxJQUFULEVBQWM7QUFBQyxjQUFHQSxJQUFJLENBQUN4QixVQUFSLEVBQW1CO0FBQUN3QixnQkFBSSxDQUFDeEIsVUFBTCxDQUFnQmlTLGFBQWhCO0FBQStCOztBQUN2N0IsaUJBQU96USxJQUFJLENBQUN3USxRQUFMLEtBQWdCLElBQXZCO0FBQTZCLFNBRlI7QUFFUyxpQkFBUSxlQUFTeFEsSUFBVCxFQUFjO0FBQUMsZUFBSUEsSUFBSSxHQUFDQSxJQUFJLENBQUM0TixVQUFkLEVBQXlCNU4sSUFBekIsRUFBOEJBLElBQUksR0FBQ0EsSUFBSSxDQUFDc0ssV0FBeEMsRUFBb0Q7QUFBQyxnQkFBR3RLLElBQUksQ0FBQzVDLFFBQUwsR0FBYyxDQUFqQixFQUFtQjtBQUFDLHFCQUFPLEtBQVA7QUFBYztBQUFDOztBQUM3SSxpQkFBTyxJQUFQO0FBQWEsU0FIUTtBQUdQLGtCQUFTLGdCQUFTNEMsSUFBVCxFQUFjO0FBQUMsaUJBQU0sQ0FBQ3dELElBQUksQ0FBQ2tDLE9BQUwsQ0FBYSxPQUFiLEVBQXNCMUYsSUFBdEIsQ0FBUDtBQUFvQyxTQUhyRDtBQUdzRCxrQkFBUyxnQkFBU0EsSUFBVCxFQUFjO0FBQUMsaUJBQU9zRyxPQUFPLENBQUN3QyxJQUFSLENBQWE5SSxJQUFJLENBQUMwSCxRQUFsQixDQUFQO0FBQW9DLFNBSGxIO0FBR21ILGlCQUFRLGVBQVMxSCxJQUFULEVBQWM7QUFBQyxpQkFBT3FHLE9BQU8sQ0FBQ3lDLElBQVIsQ0FBYTlJLElBQUksQ0FBQzBILFFBQWxCLENBQVA7QUFBb0MsU0FIOUs7QUFHK0ssa0JBQVMsZ0JBQVMxSCxJQUFULEVBQWM7QUFBQyxjQUFJZ0IsSUFBSSxHQUFDaEIsSUFBSSxDQUFDMEgsUUFBTCxDQUFjcEUsV0FBZCxFQUFUO0FBQXFDLGlCQUFPdEMsSUFBSSxLQUFHLE9BQVAsSUFBZ0JoQixJQUFJLENBQUN6QyxJQUFMLEtBQVksUUFBNUIsSUFBc0N5RCxJQUFJLEtBQUcsUUFBcEQ7QUFBOEQsU0FIMVM7QUFHMlMsZ0JBQU8sY0FBU2hCLElBQVQsRUFBYztBQUFDLGNBQUlvTixJQUFKO0FBQVMsaUJBQU9wTixJQUFJLENBQUMwSCxRQUFMLENBQWNwRSxXQUFkLE9BQThCLE9BQTlCLElBQXVDdEQsSUFBSSxDQUFDekMsSUFBTCxLQUFZLE1BQW5ELEtBQTRELENBQUM2UCxJQUFJLEdBQUNwTixJQUFJLENBQUM1QixZQUFMLENBQWtCLE1BQWxCLENBQU4sS0FBa0MsSUFBbEMsSUFBd0NnUCxJQUFJLENBQUM5SixXQUFMLE9BQXFCLE1BQXpILENBQVA7QUFBeUksU0FIbmQ7QUFHb2QsaUJBQVFxSCxzQkFBc0IsQ0FBQyxZQUFVO0FBQUMsaUJBQU0sQ0FBQyxDQUFELENBQU47QUFBVyxTQUF2QixDQUhsZjtBQUcyZ0IsZ0JBQU9BLHNCQUFzQixDQUFDLFVBQVMrRixhQUFULEVBQXVCdFIsTUFBdkIsRUFBOEI7QUFBQyxpQkFBTSxDQUFDQSxNQUFNLEdBQUMsQ0FBUixDQUFOO0FBQWtCLFNBQWxELENBSHhpQjtBQUc0bEIsY0FBS3VMLHNCQUFzQixDQUFDLFVBQVMrRixhQUFULEVBQXVCdFIsTUFBdkIsRUFBOEJ3TCxRQUE5QixFQUF1QztBQUFDLGlCQUFNLENBQUNBLFFBQVEsR0FBQyxDQUFULEdBQVdBLFFBQVEsR0FBQ3hMLE1BQXBCLEdBQTJCd0wsUUFBNUIsQ0FBTjtBQUE2QyxTQUF0RixDQUh2bkI7QUFHK3NCLGdCQUFPRCxzQkFBc0IsQ0FBQyxVQUFTRSxZQUFULEVBQXNCekwsTUFBdEIsRUFBNkI7QUFBQyxjQUFJckIsQ0FBQyxHQUFDLENBQU47O0FBQVEsaUJBQUtBLENBQUMsR0FBQ3FCLE1BQVAsRUFBY3JCLENBQUMsSUFBRSxDQUFqQixFQUFtQjtBQUFDOE0sd0JBQVksQ0FBQ3BPLElBQWIsQ0FBa0JzQixDQUFsQjtBQUFzQjs7QUFDbDFCLGlCQUFPOE0sWUFBUDtBQUFxQixTQUQ0dUIsQ0FINXVCO0FBSUUsZUFBTUYsc0JBQXNCLENBQUMsVUFBU0UsWUFBVCxFQUFzQnpMLE1BQXRCLEVBQTZCO0FBQUMsY0FBSXJCLENBQUMsR0FBQyxDQUFOOztBQUFRLGlCQUFLQSxDQUFDLEdBQUNxQixNQUFQLEVBQWNyQixDQUFDLElBQUUsQ0FBakIsRUFBbUI7QUFBQzhNLHdCQUFZLENBQUNwTyxJQUFiLENBQWtCc0IsQ0FBbEI7QUFBc0I7O0FBQ3BJLGlCQUFPOE0sWUFBUDtBQUFxQixTQUQ4QixDQUo5QjtBQUtFLGNBQUtGLHNCQUFzQixDQUFDLFVBQVNFLFlBQVQsRUFBc0J6TCxNQUF0QixFQUE2QndMLFFBQTdCLEVBQXNDO0FBQUMsY0FBSTdNLENBQUMsR0FBQzZNLFFBQVEsR0FBQyxDQUFULEdBQVdBLFFBQVEsR0FBQ3hMLE1BQXBCLEdBQTJCd0wsUUFBUSxHQUFDeEwsTUFBVCxHQUFnQkEsTUFBaEIsR0FBdUJ3TCxRQUF4RDs7QUFBaUUsaUJBQUssRUFBRTdNLENBQUYsSUFBSyxDQUFWLEdBQWE7QUFBQzhNLHdCQUFZLENBQUNwTyxJQUFiLENBQWtCc0IsQ0FBbEI7QUFBc0I7O0FBQy9MLGlCQUFPOE0sWUFBUDtBQUFxQixTQUQ2QixDQUw3QjtBQU1FLGNBQUtGLHNCQUFzQixDQUFDLFVBQVNFLFlBQVQsRUFBc0J6TCxNQUF0QixFQUE2QndMLFFBQTdCLEVBQXNDO0FBQUMsY0FBSTdNLENBQUMsR0FBQzZNLFFBQVEsR0FBQyxDQUFULEdBQVdBLFFBQVEsR0FBQ3hMLE1BQXBCLEdBQTJCd0wsUUFBakM7O0FBQTBDLGlCQUFLLEVBQUU3TSxDQUFGLEdBQUlxQixNQUFULEdBQWlCO0FBQUN5TCx3QkFBWSxDQUFDcE8sSUFBYixDQUFrQnNCLENBQWxCO0FBQXNCOztBQUM1SyxpQkFBTzhNLFlBQVA7QUFBcUIsU0FENkI7QUFON0I7QUFmYyxLQUF0QjtBQXNCWXJILFFBQUksQ0FBQ2tDLE9BQUwsQ0FBYSxLQUFiLElBQW9CbEMsSUFBSSxDQUFDa0MsT0FBTCxDQUFhLElBQWIsQ0FBcEI7O0FBQXVDLFNBQUkzSCxDQUFKLElBQVE7QUFBQzRTLFdBQUssRUFBQyxJQUFQO0FBQVlDLGNBQVEsRUFBQyxJQUFyQjtBQUEwQkMsVUFBSSxFQUFDLElBQS9CO0FBQW9DQyxjQUFRLEVBQUMsSUFBN0M7QUFBa0RDLFdBQUssRUFBQztBQUF4RCxLQUFSLEVBQXNFO0FBQUN2TixVQUFJLENBQUNrQyxPQUFMLENBQWEzSCxDQUFiLElBQWdCd00saUJBQWlCLENBQUN4TSxDQUFELENBQWpDO0FBQXNDOztBQUM3SyxTQUFJQSxDQUFKLElBQVE7QUFBQ2lULFlBQU0sRUFBQyxJQUFSO0FBQWFDLFdBQUssRUFBQztBQUFuQixLQUFSLEVBQWlDO0FBQUN6TixVQUFJLENBQUNrQyxPQUFMLENBQWEzSCxDQUFiLElBQWdCeU0sa0JBQWtCLENBQUN6TSxDQUFELENBQWxDO0FBQXVDOztBQUN6RSxhQUFTMlIsVUFBVCxHQUFxQixDQUFFOztBQUN2QkEsY0FBVSxDQUFDelEsU0FBWCxHQUFxQnVFLElBQUksQ0FBQzBOLE9BQUwsR0FBYTFOLElBQUksQ0FBQ2tDLE9BQXZDO0FBQStDbEMsUUFBSSxDQUFDa00sVUFBTCxHQUFnQixJQUFJQSxVQUFKLEVBQWhCOztBQUFpQy9MLFlBQVEsR0FBQ0osTUFBTSxDQUFDSSxRQUFQLEdBQWdCLFVBQVM5RSxRQUFULEVBQWtCc1MsU0FBbEIsRUFBNEI7QUFBQyxVQUFJdkIsT0FBSjtBQUFBLFVBQVl6SCxLQUFaO0FBQUEsVUFBa0JpSixNQUFsQjtBQUFBLFVBQXlCN1QsSUFBekI7QUFBQSxVQUE4QjhULEtBQTlCO0FBQUEsVUFBb0NqSixNQUFwQztBQUFBLFVBQTJDa0osVUFBM0M7QUFBQSxVQUFzREMsTUFBTSxHQUFDMU0sVUFBVSxDQUFDaEcsUUFBUSxHQUFDLEdBQVYsQ0FBdkU7O0FBQXNGLFVBQUcwUyxNQUFILEVBQVU7QUFBQyxlQUFPSixTQUFTLEdBQUMsQ0FBRCxHQUFHSSxNQUFNLENBQUNwVixLQUFQLENBQWEsQ0FBYixDQUFuQjtBQUFvQzs7QUFDM1FrVixXQUFLLEdBQUN4UyxRQUFOO0FBQWV1SixZQUFNLEdBQUMsRUFBUDtBQUFVa0osZ0JBQVUsR0FBQzlOLElBQUksQ0FBQ3lLLFNBQWhCOztBQUEwQixhQUFNb0QsS0FBTixFQUFZO0FBQUMsWUFBRyxDQUFDekIsT0FBRCxLQUFXekgsS0FBSyxHQUFDckMsTUFBTSxDQUFDMEMsSUFBUCxDQUFZNkksS0FBWixDQUFqQixDQUFILEVBQXdDO0FBQUMsY0FBR2xKLEtBQUgsRUFBUztBQUFDa0osaUJBQUssR0FBQ0EsS0FBSyxDQUFDbFYsS0FBTixDQUFZZ00sS0FBSyxDQUFDLENBQUQsQ0FBTCxDQUFTL0ksTUFBckIsS0FBOEJpUyxLQUFwQztBQUEyQzs7QUFDOUpqSixnQkFBTSxDQUFDM0wsSUFBUCxDQUFhMlUsTUFBTSxHQUFDLEVBQXBCO0FBQTBCOztBQUMxQnhCLGVBQU8sR0FBQyxLQUFSOztBQUFjLFlBQUl6SCxLQUFLLEdBQUNwQyxZQUFZLENBQUN5QyxJQUFiLENBQWtCNkksS0FBbEIsQ0FBVixFQUFvQztBQUFDekIsaUJBQU8sR0FBQ3pILEtBQUssQ0FBQ3VCLEtBQU4sRUFBUjtBQUFzQjBILGdCQUFNLENBQUMzVSxJQUFQLENBQVk7QUFBQ3VHLGlCQUFLLEVBQUM0TSxPQUFQO0FBQWVyUyxnQkFBSSxFQUFDNEssS0FBSyxDQUFDLENBQUQsQ0FBTCxDQUFTdEcsT0FBVCxDQUFpQmdFLEtBQWpCLEVBQXVCLEdBQXZCO0FBQXBCLFdBQVo7QUFBOER3TCxlQUFLLEdBQUNBLEtBQUssQ0FBQ2xWLEtBQU4sQ0FBWXlULE9BQU8sQ0FBQ3hRLE1BQXBCLENBQU47QUFBbUM7O0FBQzFLLGFBQUk3QixJQUFKLElBQVlpRyxJQUFJLENBQUNtSSxNQUFqQixFQUF3QjtBQUFDLGNBQUcsQ0FBQ3hELEtBQUssR0FBQ2hDLFNBQVMsQ0FBQzVJLElBQUQsQ0FBVCxDQUFnQmlMLElBQWhCLENBQXFCNkksS0FBckIsQ0FBUCxNQUFzQyxDQUFDQyxVQUFVLENBQUMvVCxJQUFELENBQVgsS0FBb0I0SyxLQUFLLEdBQUNtSixVQUFVLENBQUMvVCxJQUFELENBQVYsQ0FBaUI0SyxLQUFqQixDQUExQixDQUF0QyxDQUFILEVBQTZGO0FBQUN5SCxtQkFBTyxHQUFDekgsS0FBSyxDQUFDdUIsS0FBTixFQUFSO0FBQXNCMEgsa0JBQU0sQ0FBQzNVLElBQVAsQ0FBWTtBQUFDdUcsbUJBQUssRUFBQzRNLE9BQVA7QUFBZXJTLGtCQUFJLEVBQUNBLElBQXBCO0FBQXlCc0YscUJBQU8sRUFBQ3NGO0FBQWpDLGFBQVo7QUFBcURrSixpQkFBSyxHQUFDQSxLQUFLLENBQUNsVixLQUFOLENBQVl5VCxPQUFPLENBQUN4USxNQUFwQixDQUFOO0FBQW1DO0FBQUM7O0FBQ3RPLFlBQUcsQ0FBQ3dRLE9BQUosRUFBWTtBQUFDO0FBQU87QUFBQzs7QUFDckIsYUFBT3VCLFNBQVMsR0FBQ0UsS0FBSyxDQUFDalMsTUFBUCxHQUFjaVMsS0FBSyxHQUFDOU4sTUFBTSxDQUFDeEIsS0FBUCxDQUFhbEQsUUFBYixDQUFELEdBQXdCZ0csVUFBVSxDQUFDaEcsUUFBRCxFQUFVdUosTUFBVixDQUFWLENBQTRCak0sS0FBNUIsQ0FBa0MsQ0FBbEMsQ0FBM0Q7QUFBaUcsS0FOakI7O0FBTWtCLGFBQVM4TSxVQUFULENBQW9CbUksTUFBcEIsRUFBMkI7QUFBQyxVQUFJclQsQ0FBQyxHQUFDLENBQU47QUFBQSxVQUFRMEMsR0FBRyxHQUFDMlEsTUFBTSxDQUFDaFMsTUFBbkI7QUFBQSxVQUEwQlAsUUFBUSxHQUFDLEVBQW5DOztBQUFzQyxhQUFLZCxDQUFDLEdBQUMwQyxHQUFQLEVBQVcxQyxDQUFDLEVBQVosRUFBZTtBQUFDYyxnQkFBUSxJQUFFdVMsTUFBTSxDQUFDclQsQ0FBRCxDQUFOLENBQVVpRixLQUFwQjtBQUEyQjs7QUFDL00sYUFBT25FLFFBQVA7QUFBaUI7O0FBQ2pCLGFBQVMySSxhQUFULENBQXVCcUksT0FBdkIsRUFBK0IyQixVQUEvQixFQUEwQ0MsSUFBMUMsRUFBK0M7QUFBQyxVQUFJOUosR0FBRyxHQUFDNkosVUFBVSxDQUFDN0osR0FBbkI7QUFBQSxVQUF1QitKLElBQUksR0FBQ0YsVUFBVSxDQUFDNUosSUFBdkM7QUFBQSxVQUE0QzRCLEdBQUcsR0FBQ2tJLElBQUksSUFBRS9KLEdBQXREO0FBQUEsVUFBMERnSyxnQkFBZ0IsR0FBQ0YsSUFBSSxJQUFFakksR0FBRyxLQUFHLFlBQXZGO0FBQUEsVUFBb0dvSSxRQUFRLEdBQUNsTixJQUFJLEVBQWpIO0FBQW9ILGFBQU84TSxVQUFVLENBQUN0UixLQUFYLEdBQWlCLFVBQVNGLElBQVQsRUFBY2xCLE9BQWQsRUFBc0JpUSxHQUF0QixFQUEwQjtBQUFDLGVBQU8vTyxJQUFJLEdBQUNBLElBQUksQ0FBQzJILEdBQUQsQ0FBaEIsRUFBdUI7QUFBQyxjQUFHM0gsSUFBSSxDQUFDNUMsUUFBTCxLQUFnQixDQUFoQixJQUFtQnVVLGdCQUF0QixFQUF1QztBQUFDLG1CQUFPOUIsT0FBTyxDQUFDN1AsSUFBRCxFQUFNbEIsT0FBTixFQUFjaVEsR0FBZCxDQUFkO0FBQWtDO0FBQUM7O0FBQzFULGVBQU8sS0FBUDtBQUFjLE9BRDZKLEdBQzVKLFVBQVMvTyxJQUFULEVBQWNsQixPQUFkLEVBQXNCaVEsR0FBdEIsRUFBMEI7QUFBQyxZQUFJOEMsUUFBSjtBQUFBLFlBQWE3QyxXQUFiO0FBQUEsWUFBeUJDLFVBQXpCO0FBQUEsWUFBb0M2QyxRQUFRLEdBQUMsQ0FBQ3JOLE9BQUQsRUFBU21OLFFBQVQsQ0FBN0M7O0FBQWdFLFlBQUc3QyxHQUFILEVBQU87QUFBQyxpQkFBTy9PLElBQUksR0FBQ0EsSUFBSSxDQUFDMkgsR0FBRCxDQUFoQixFQUF1QjtBQUFDLGdCQUFHM0gsSUFBSSxDQUFDNUMsUUFBTCxLQUFnQixDQUFoQixJQUFtQnVVLGdCQUF0QixFQUF1QztBQUFDLGtCQUFHOUIsT0FBTyxDQUFDN1AsSUFBRCxFQUFNbEIsT0FBTixFQUFjaVEsR0FBZCxDQUFWLEVBQTZCO0FBQUMsdUJBQU8sSUFBUDtBQUFhO0FBQUM7QUFBQztBQUFDLFNBQXRILE1BQTBIO0FBQUMsaUJBQU8vTyxJQUFJLEdBQUNBLElBQUksQ0FBQzJILEdBQUQsQ0FBaEIsRUFBdUI7QUFBQyxnQkFBRzNILElBQUksQ0FBQzVDLFFBQUwsS0FBZ0IsQ0FBaEIsSUFBbUJ1VSxnQkFBdEIsRUFBdUM7QUFBQzFDLHdCQUFVLEdBQUNqUCxJQUFJLENBQUMwQixPQUFELENBQUosS0FBZ0IxQixJQUFJLENBQUMwQixPQUFELENBQUosR0FBYyxFQUE5QixDQUFYO0FBQTZDc04seUJBQVcsR0FBQ0MsVUFBVSxDQUFDalAsSUFBSSxDQUFDdVAsUUFBTixDQUFWLEtBQTRCTixVQUFVLENBQUNqUCxJQUFJLENBQUN1UCxRQUFOLENBQVYsR0FBMEIsRUFBdEQsQ0FBWjs7QUFBc0Usa0JBQUdtQyxJQUFJLElBQUVBLElBQUksS0FBRzFSLElBQUksQ0FBQzBILFFBQUwsQ0FBY3BFLFdBQWQsRUFBaEIsRUFBNEM7QUFBQ3RELG9CQUFJLEdBQUNBLElBQUksQ0FBQzJILEdBQUQsQ0FBSixJQUFXM0gsSUFBaEI7QUFBc0IsZUFBbkUsTUFBd0UsSUFBRyxDQUFDNlIsUUFBUSxHQUFDN0MsV0FBVyxDQUFDeEYsR0FBRCxDQUFyQixLQUE2QnFJLFFBQVEsQ0FBQyxDQUFELENBQVIsS0FBY3BOLE9BQTNDLElBQW9Eb04sUUFBUSxDQUFDLENBQUQsQ0FBUixLQUFjRCxRQUFyRSxFQUE4RTtBQUFDLHVCQUFPRSxRQUFRLENBQUMsQ0FBRCxDQUFSLEdBQVlELFFBQVEsQ0FBQyxDQUFELENBQTNCO0FBQWlDLGVBQWhILE1BQW9IO0FBQUM3QywyQkFBVyxDQUFDeEYsR0FBRCxDQUFYLEdBQWlCc0ksUUFBakI7O0FBQTBCLG9CQUFJQSxRQUFRLENBQUMsQ0FBRCxDQUFSLEdBQVlqQyxPQUFPLENBQUM3UCxJQUFELEVBQU1sQixPQUFOLEVBQWNpUSxHQUFkLENBQXZCLEVBQTJDO0FBQUMseUJBQU8sSUFBUDtBQUFhO0FBQUM7QUFBQztBQUFDO0FBQUM7O0FBQzVxQixlQUFPLEtBQVA7QUFBYyxPQUZzSjtBQUVwSjs7QUFDaEIsYUFBU2dELGNBQVQsQ0FBd0JDLFFBQXhCLEVBQWlDO0FBQUMsYUFBT0EsUUFBUSxDQUFDNVMsTUFBVCxHQUFnQixDQUFoQixHQUFrQixVQUFTWSxJQUFULEVBQWNsQixPQUFkLEVBQXNCaVEsR0FBdEIsRUFBMEI7QUFBQyxZQUFJaFIsQ0FBQyxHQUFDaVUsUUFBUSxDQUFDNVMsTUFBZjs7QUFBc0IsZUFBTXJCLENBQUMsRUFBUCxFQUFVO0FBQUMsY0FBRyxDQUFDaVUsUUFBUSxDQUFDalUsQ0FBRCxDQUFSLENBQVlpQyxJQUFaLEVBQWlCbEIsT0FBakIsRUFBeUJpUSxHQUF6QixDQUFKLEVBQWtDO0FBQUMsbUJBQU8sS0FBUDtBQUFjO0FBQUM7O0FBQ3pLLGVBQU8sSUFBUDtBQUFhLE9BRDRCLEdBQzNCaUQsUUFBUSxDQUFDLENBQUQsQ0FEWTtBQUNQOztBQUMzQixhQUFTQyxnQkFBVCxDQUEwQnBULFFBQTFCLEVBQW1DcVQsUUFBbkMsRUFBNEMxUCxPQUE1QyxFQUFvRDtBQUFDLFVBQUl6RSxDQUFDLEdBQUMsQ0FBTjtBQUFBLFVBQVEwQyxHQUFHLEdBQUN5UixRQUFRLENBQUM5UyxNQUFyQjs7QUFBNEIsYUFBS3JCLENBQUMsR0FBQzBDLEdBQVAsRUFBVzFDLENBQUMsRUFBWixFQUFlO0FBQUN3RixjQUFNLENBQUMxRSxRQUFELEVBQVVxVCxRQUFRLENBQUNuVSxDQUFELENBQWxCLEVBQXNCeUUsT0FBdEIsQ0FBTjtBQUFzQzs7QUFDdkksYUFBT0EsT0FBUDtBQUFnQjs7QUFDaEIsYUFBUzJQLFFBQVQsQ0FBa0JyQyxTQUFsQixFQUE0Qi9QLEdBQTVCLEVBQWdDNEwsTUFBaEMsRUFBdUM3TSxPQUF2QyxFQUErQ2lRLEdBQS9DLEVBQW1EO0FBQUMsVUFBSS9PLElBQUo7QUFBQSxVQUFTb1MsWUFBWSxHQUFDLEVBQXRCO0FBQUEsVUFBeUJyVSxDQUFDLEdBQUMsQ0FBM0I7QUFBQSxVQUE2QjBDLEdBQUcsR0FBQ3FQLFNBQVMsQ0FBQzFRLE1BQTNDO0FBQUEsVUFBa0RpVCxNQUFNLEdBQUN0UyxHQUFHLElBQUUsSUFBOUQ7O0FBQW1FLGFBQUtoQyxDQUFDLEdBQUMwQyxHQUFQLEVBQVcxQyxDQUFDLEVBQVosRUFBZTtBQUFDLFlBQUlpQyxJQUFJLEdBQUM4UCxTQUFTLENBQUMvUixDQUFELENBQWxCLEVBQXVCO0FBQUMsY0FBRyxDQUFDNE4sTUFBRCxJQUFTQSxNQUFNLENBQUMzTCxJQUFELEVBQU1sQixPQUFOLEVBQWNpUSxHQUFkLENBQWxCLEVBQXFDO0FBQUNxRCx3QkFBWSxDQUFDM1YsSUFBYixDQUFrQnVELElBQWxCOztBQUF3QixnQkFBR3FTLE1BQUgsRUFBVTtBQUFDdFMsaUJBQUcsQ0FBQ3RELElBQUosQ0FBU3NCLENBQVQ7QUFBYTtBQUFDO0FBQUM7QUFBQzs7QUFDeFAsYUFBT3FVLFlBQVA7QUFBcUI7O0FBQ3JCLGFBQVNFLFVBQVQsQ0FBb0JyRSxTQUFwQixFQUE4QnBQLFFBQTlCLEVBQXVDZ1IsT0FBdkMsRUFBK0MwQyxVQUEvQyxFQUEwREMsVUFBMUQsRUFBcUVDLFlBQXJFLEVBQWtGO0FBQUMsVUFBR0YsVUFBVSxJQUFFLENBQUNBLFVBQVUsQ0FBQzdRLE9BQUQsQ0FBMUIsRUFBb0M7QUFBQzZRLGtCQUFVLEdBQUNELFVBQVUsQ0FBQ0MsVUFBRCxDQUFyQjtBQUFtQzs7QUFDM0osVUFBR0MsVUFBVSxJQUFFLENBQUNBLFVBQVUsQ0FBQzlRLE9BQUQsQ0FBMUIsRUFBb0M7QUFBQzhRLGtCQUFVLEdBQUNGLFVBQVUsQ0FBQ0UsVUFBRCxFQUFZQyxZQUFaLENBQXJCO0FBQWdEOztBQUNyRixhQUFPOUksWUFBWSxDQUFDLFVBQVMzQixJQUFULEVBQWN4RixPQUFkLEVBQXNCMUQsT0FBdEIsRUFBOEJpUSxHQUE5QixFQUFrQztBQUFDLFlBQUkyRCxJQUFKO0FBQUEsWUFBUzNVLENBQVQ7QUFBQSxZQUFXaUMsSUFBWDtBQUFBLFlBQWdCMlMsTUFBTSxHQUFDLEVBQXZCO0FBQUEsWUFBMEJDLE9BQU8sR0FBQyxFQUFsQztBQUFBLFlBQXFDQyxXQUFXLEdBQUNyUSxPQUFPLENBQUNwRCxNQUF6RDtBQUFBLFlBQWdFSyxLQUFLLEdBQUN1SSxJQUFJLElBQUVpSyxnQkFBZ0IsQ0FBQ3BULFFBQVEsSUFBRSxHQUFYLEVBQWVDLE9BQU8sQ0FBQzFCLFFBQVIsR0FBaUIsQ0FBQzBCLE9BQUQsQ0FBakIsR0FBMkJBLE9BQTFDLEVBQWtELEVBQWxELENBQTVGO0FBQUEsWUFBa0pnVSxTQUFTLEdBQUM3RSxTQUFTLEtBQUdqRyxJQUFJLElBQUUsQ0FBQ25KLFFBQVYsQ0FBVCxHQUE2QnNULFFBQVEsQ0FBQzFTLEtBQUQsRUFBT2tULE1BQVAsRUFBYzFFLFNBQWQsRUFBd0JuUCxPQUF4QixFQUFnQ2lRLEdBQWhDLENBQXJDLEdBQTBFdFAsS0FBdE87QUFBQSxZQUE0T3NULFVBQVUsR0FBQ2xELE9BQU8sR0FBQzJDLFVBQVUsS0FBR3hLLElBQUksR0FBQ2lHLFNBQUQsR0FBVzRFLFdBQVcsSUFBRU4sVUFBL0IsQ0FBVixHQUFxRCxFQUFyRCxHQUF3RC9QLE9BQXpELEdBQWlFc1EsU0FBL1Q7O0FBQXlVLFlBQUdqRCxPQUFILEVBQVc7QUFBQ0EsaUJBQU8sQ0FBQ2lELFNBQUQsRUFBV0MsVUFBWCxFQUFzQmpVLE9BQXRCLEVBQThCaVEsR0FBOUIsQ0FBUDtBQUEyQzs7QUFDdmIsWUFBR3dELFVBQUgsRUFBYztBQUFDRyxjQUFJLEdBQUNQLFFBQVEsQ0FBQ1ksVUFBRCxFQUFZSCxPQUFaLENBQWI7QUFBa0NMLG9CQUFVLENBQUNHLElBQUQsRUFBTSxFQUFOLEVBQVM1VCxPQUFULEVBQWlCaVEsR0FBakIsQ0FBVjtBQUFnQ2hSLFdBQUMsR0FBQzJVLElBQUksQ0FBQ3RULE1BQVA7O0FBQWMsaUJBQU1yQixDQUFDLEVBQVAsRUFBVTtBQUFDLGdCQUFJaUMsSUFBSSxHQUFDMFMsSUFBSSxDQUFDM1UsQ0FBRCxDQUFiLEVBQWtCO0FBQUNnVix3QkFBVSxDQUFDSCxPQUFPLENBQUM3VSxDQUFELENBQVIsQ0FBVixHQUF1QixFQUFFK1UsU0FBUyxDQUFDRixPQUFPLENBQUM3VSxDQUFELENBQVIsQ0FBVCxHQUFzQmlDLElBQXhCLENBQXZCO0FBQXNEO0FBQUM7QUFBQzs7QUFDckwsWUFBR2dJLElBQUgsRUFBUTtBQUFDLGNBQUd3SyxVQUFVLElBQUV2RSxTQUFmLEVBQXlCO0FBQUMsZ0JBQUd1RSxVQUFILEVBQWM7QUFBQ0Usa0JBQUksR0FBQyxFQUFMO0FBQVEzVSxlQUFDLEdBQUNnVixVQUFVLENBQUMzVCxNQUFiOztBQUFvQixxQkFBTXJCLENBQUMsRUFBUCxFQUFVO0FBQUMsb0JBQUlpQyxJQUFJLEdBQUMrUyxVQUFVLENBQUNoVixDQUFELENBQW5CLEVBQXdCO0FBQUMyVSxzQkFBSSxDQUFDalcsSUFBTCxDQUFXcVcsU0FBUyxDQUFDL1UsQ0FBRCxDQUFULEdBQWFpQyxJQUF4QjtBQUFnQztBQUFDOztBQUNuSndTLHdCQUFVLENBQUMsSUFBRCxFQUFPTyxVQUFVLEdBQUMsRUFBbEIsRUFBc0JMLElBQXRCLEVBQTJCM0QsR0FBM0IsQ0FBVjtBQUEyQzs7QUFDM0NoUixhQUFDLEdBQUNnVixVQUFVLENBQUMzVCxNQUFiOztBQUFvQixtQkFBTXJCLENBQUMsRUFBUCxFQUFVO0FBQUMsa0JBQUcsQ0FBQ2lDLElBQUksR0FBQytTLFVBQVUsQ0FBQ2hWLENBQUQsQ0FBaEIsS0FBc0IsQ0FBQzJVLElBQUksR0FBQ0YsVUFBVSxHQUFDOVYsT0FBTyxDQUFDc0wsSUFBRCxFQUFNaEksSUFBTixDQUFSLEdBQW9CMlMsTUFBTSxDQUFDNVUsQ0FBRCxDQUExQyxJQUErQyxDQUFDLENBQXpFLEVBQTJFO0FBQUNpSyxvQkFBSSxDQUFDMEssSUFBRCxDQUFKLEdBQVcsRUFBRWxRLE9BQU8sQ0FBQ2tRLElBQUQsQ0FBUCxHQUFjMVMsSUFBaEIsQ0FBWDtBQUFrQztBQUFDO0FBQUM7QUFBQyxTQUZoSixNQUVvSjtBQUFDK1Msb0JBQVUsR0FBQ1osUUFBUSxDQUFDWSxVQUFVLEtBQUd2USxPQUFiLEdBQXFCdVEsVUFBVSxDQUFDbFMsTUFBWCxDQUFrQmdTLFdBQWxCLEVBQThCRSxVQUFVLENBQUMzVCxNQUF6QyxDQUFyQixHQUFzRTJULFVBQXZFLENBQW5COztBQUFzRyxjQUFHUCxVQUFILEVBQWM7QUFBQ0Esc0JBQVUsQ0FBQyxJQUFELEVBQU1oUSxPQUFOLEVBQWN1USxVQUFkLEVBQXlCaEUsR0FBekIsQ0FBVjtBQUF5QyxXQUF4RCxNQUE0RDtBQUFDdFMsZ0JBQUksQ0FBQ0QsS0FBTCxDQUFXZ0csT0FBWCxFQUFtQnVRLFVBQW5CO0FBQWdDO0FBQUM7QUFBQyxPQUp2VSxDQUFuQjtBQUk2Vjs7QUFDN1YsYUFBU0MsaUJBQVQsQ0FBMkI1QixNQUEzQixFQUFrQztBQUFDLFVBQUk2QixZQUFKO0FBQUEsVUFBaUJwRCxPQUFqQjtBQUFBLFVBQXlCblAsQ0FBekI7QUFBQSxVQUEyQkQsR0FBRyxHQUFDMlEsTUFBTSxDQUFDaFMsTUFBdEM7QUFBQSxVQUE2QzhULGVBQWUsR0FBQzFQLElBQUksQ0FBQ3dLLFFBQUwsQ0FBY29ELE1BQU0sQ0FBQyxDQUFELENBQU4sQ0FBVTdULElBQXhCLENBQTdEO0FBQUEsVUFBMkY0VixnQkFBZ0IsR0FBQ0QsZUFBZSxJQUFFMVAsSUFBSSxDQUFDd0ssUUFBTCxDQUFjLEdBQWQsQ0FBN0g7QUFBQSxVQUFnSmpRLENBQUMsR0FBQ21WLGVBQWUsR0FBQyxDQUFELEdBQUcsQ0FBcEs7QUFBQSxVQUFzS0UsWUFBWSxHQUFDNUwsYUFBYSxDQUFDLFVBQVN4SCxJQUFULEVBQWM7QUFBQyxlQUFPQSxJQUFJLEtBQUdpVCxZQUFkO0FBQTRCLE9BQTVDLEVBQTZDRSxnQkFBN0MsRUFBOEQsSUFBOUQsQ0FBaE07QUFBQSxVQUFvUUUsZUFBZSxHQUFDN0wsYUFBYSxDQUFDLFVBQVN4SCxJQUFULEVBQWM7QUFBQyxlQUFPdEQsT0FBTyxDQUFDdVcsWUFBRCxFQUFjalQsSUFBZCxDQUFQLEdBQTJCLENBQUMsQ0FBbkM7QUFBc0MsT0FBdEQsRUFBdURtVCxnQkFBdkQsRUFBd0UsSUFBeEUsQ0FBalM7QUFBQSxVQUErV25CLFFBQVEsR0FBQyxDQUFDLFVBQVNoUyxJQUFULEVBQWNsQixPQUFkLEVBQXNCaVEsR0FBdEIsRUFBMEI7QUFBQyxZQUFJclAsR0FBRyxHQUFFLENBQUN3VCxlQUFELEtBQW1CbkUsR0FBRyxJQUFFalEsT0FBTyxLQUFHZ0YsZ0JBQWxDLENBQUQsS0FBd0QsQ0FBQ21QLFlBQVksR0FBQ25VLE9BQWQsRUFBdUIxQixRQUF2QixHQUFnQ2dXLFlBQVksQ0FBQ3BULElBQUQsRUFBTWxCLE9BQU4sRUFBY2lRLEdBQWQsQ0FBNUMsR0FBK0RzRSxlQUFlLENBQUNyVCxJQUFELEVBQU1sQixPQUFOLEVBQWNpUSxHQUFkLENBQXRJLENBQVI7QUFBa0trRSxvQkFBWSxHQUFDLElBQWI7QUFBa0IsZUFBT3ZULEdBQVA7QUFBWSxPQUE1TixDQUF4WDs7QUFBc2xCLGFBQUszQixDQUFDLEdBQUMwQyxHQUFQLEVBQVcxQyxDQUFDLEVBQVosRUFBZTtBQUFDLFlBQUk4UixPQUFPLEdBQUNyTSxJQUFJLENBQUN3SyxRQUFMLENBQWNvRCxNQUFNLENBQUNyVCxDQUFELENBQU4sQ0FBVVIsSUFBeEIsQ0FBWixFQUEyQztBQUFDeVUsa0JBQVEsR0FBQyxDQUFDeEssYUFBYSxDQUFDdUssY0FBYyxDQUFDQyxRQUFELENBQWYsRUFBMEJuQyxPQUExQixDQUFkLENBQVQ7QUFBNEQsU0FBeEcsTUFBNEc7QUFBQ0EsaUJBQU8sR0FBQ3JNLElBQUksQ0FBQ21JLE1BQUwsQ0FBWXlGLE1BQU0sQ0FBQ3JULENBQUQsQ0FBTixDQUFVUixJQUF0QixFQUE0QmYsS0FBNUIsQ0FBa0MsSUFBbEMsRUFBdUM0VSxNQUFNLENBQUNyVCxDQUFELENBQU4sQ0FBVThFLE9BQWpELENBQVI7O0FBQWtFLGNBQUdnTixPQUFPLENBQUNuTyxPQUFELENBQVYsRUFBb0I7QUFBQ2hCLGFBQUMsR0FBQyxFQUFFM0MsQ0FBSjs7QUFBTSxtQkFBSzJDLENBQUMsR0FBQ0QsR0FBUCxFQUFXQyxDQUFDLEVBQVosRUFBZTtBQUFDLGtCQUFHOEMsSUFBSSxDQUFDd0ssUUFBTCxDQUFjb0QsTUFBTSxDQUFDMVEsQ0FBRCxDQUFOLENBQVVuRCxJQUF4QixDQUFILEVBQWlDO0FBQUM7QUFBTztBQUFDOztBQUM3NEIsbUJBQU8rVSxVQUFVLENBQUN2VSxDQUFDLEdBQUMsQ0FBRixJQUFLZ1UsY0FBYyxDQUFDQyxRQUFELENBQXBCLEVBQStCalUsQ0FBQyxHQUFDLENBQUYsSUFBS2tMLFVBQVUsQ0FBQ21JLE1BQU0sQ0FBQ2pWLEtBQVAsQ0FBYSxDQUFiLEVBQWU0QixDQUFDLEdBQUMsQ0FBakIsRUFBb0J4QixNQUFwQixDQUEyQjtBQUFDeUcsbUJBQUssRUFBQ29PLE1BQU0sQ0FBQ3JULENBQUMsR0FBQyxDQUFILENBQU4sQ0FBWVIsSUFBWixLQUFtQixHQUFuQixHQUF1QixHQUF2QixHQUEyQjtBQUFsQyxhQUEzQixDQUFELENBQVYsQ0FBOEVzRSxPQUE5RSxDQUFzRmdFLEtBQXRGLEVBQTRGLElBQTVGLENBQXBDLEVBQXNJZ0ssT0FBdEksRUFBOEk5UixDQUFDLEdBQUMyQyxDQUFGLElBQUtzUyxpQkFBaUIsQ0FBQzVCLE1BQU0sQ0FBQ2pWLEtBQVAsQ0FBYTRCLENBQWIsRUFBZTJDLENBQWYsQ0FBRCxDQUFwSyxFQUF3TEEsQ0FBQyxHQUFDRCxHQUFGLElBQU91UyxpQkFBaUIsQ0FBRTVCLE1BQU0sR0FBQ0EsTUFBTSxDQUFDalYsS0FBUCxDQUFhdUUsQ0FBYixDQUFULENBQWhOLEVBQTJPQSxDQUFDLEdBQUNELEdBQUYsSUFBT3dJLFVBQVUsQ0FBQ21JLE1BQUQsQ0FBNVAsQ0FBakI7QUFBd1I7O0FBQ3hSWSxrQkFBUSxDQUFDdlYsSUFBVCxDQUFjb1QsT0FBZDtBQUF3QjtBQUFDOztBQUN6QixhQUFPa0MsY0FBYyxDQUFDQyxRQUFELENBQXJCO0FBQWlDOztBQUNqQyxhQUFTc0Isd0JBQVQsQ0FBa0NDLGVBQWxDLEVBQWtEQyxXQUFsRCxFQUE4RDtBQUFDLFVBQUlDLEtBQUssR0FBQ0QsV0FBVyxDQUFDcFUsTUFBWixHQUFtQixDQUE3QjtBQUFBLFVBQStCc1UsU0FBUyxHQUFDSCxlQUFlLENBQUNuVSxNQUFoQixHQUF1QixDQUFoRTtBQUFBLFVBQWtFdVUsWUFBWSxHQUFDLFNBQWJBLFlBQWEsQ0FBUzNMLElBQVQsRUFBY2xKLE9BQWQsRUFBc0JpUSxHQUF0QixFQUEwQnZNLE9BQTFCLEVBQWtDb1IsU0FBbEMsRUFBNEM7QUFBQyxZQUFJNVQsSUFBSjtBQUFBLFlBQVNVLENBQVQ7QUFBQSxZQUFXbVAsT0FBWDtBQUFBLFlBQW1CZ0UsWUFBWSxHQUFDLENBQWhDO0FBQUEsWUFBa0M5VixDQUFDLEdBQUMsR0FBcEM7QUFBQSxZQUF3QytSLFNBQVMsR0FBQzlILElBQUksSUFBRSxFQUF4RDtBQUFBLFlBQTJEOEwsVUFBVSxHQUFDLEVBQXRFO0FBQUEsWUFBeUVDLGFBQWEsR0FBQ2pRLGdCQUF2RjtBQUFBLFlBQXdHckUsS0FBSyxHQUFDdUksSUFBSSxJQUFFMEwsU0FBUyxJQUFFbFEsSUFBSSxDQUFDcUksSUFBTCxDQUFVLEtBQVYsRUFBaUIsR0FBakIsRUFBcUIrSCxTQUFyQixDQUEvSDtBQUFBLFlBQStKSSxhQUFhLEdBQUV2UCxPQUFPLElBQUVzUCxhQUFhLElBQUUsSUFBZixHQUFvQixDQUFwQixHQUFzQnBTLElBQUksQ0FBQ0MsTUFBTCxNQUFlLEdBQTVOO0FBQUEsWUFBaU9uQixHQUFHLEdBQUNoQixLQUFLLENBQUNMLE1BQTNPOztBQUFrUCxZQUFHd1UsU0FBSCxFQUFhO0FBQUM5UCwwQkFBZ0IsR0FBQ2hGLE9BQU8sSUFBRXBELFFBQVQsSUFBbUJvRCxPQUFuQixJQUE0QjhVLFNBQTdDO0FBQXdEOztBQUNuZixlQUFLN1YsQ0FBQyxLQUFHMEMsR0FBSixJQUFTLENBQUNULElBQUksR0FBQ1AsS0FBSyxDQUFDMUIsQ0FBRCxDQUFYLEtBQWlCLElBQS9CLEVBQW9DQSxDQUFDLEVBQXJDLEVBQXdDO0FBQUMsY0FBRzJWLFNBQVMsSUFBRTFULElBQWQsRUFBbUI7QUFBQ1UsYUFBQyxHQUFDLENBQUY7O0FBQUksZ0JBQUcsQ0FBQzVCLE9BQUQsSUFBVWtCLElBQUksQ0FBQ3VJLGFBQUwsSUFBb0I3TSxRQUFqQyxFQUEwQztBQUFDdUkseUJBQVcsQ0FBQ2pFLElBQUQsQ0FBWDtBQUFrQitPLGlCQUFHLEdBQUMsQ0FBQzVLLGNBQUw7QUFBcUI7O0FBQ25KLG1CQUFPMEwsT0FBTyxHQUFDMEQsZUFBZSxDQUFDN1MsQ0FBQyxFQUFGLENBQTlCLEVBQXFDO0FBQUMsa0JBQUdtUCxPQUFPLENBQUM3UCxJQUFELEVBQU1sQixPQUFPLElBQUVwRCxRQUFmLEVBQXdCcVQsR0FBeEIsQ0FBVixFQUF1QztBQUFDdk0sdUJBQU8sQ0FBQy9GLElBQVIsQ0FBYXVELElBQWI7QUFBbUI7QUFBTztBQUFDOztBQUN6RyxnQkFBRzRULFNBQUgsRUFBYTtBQUFDblAscUJBQU8sR0FBQ3VQLGFBQVI7QUFBdUI7QUFBQzs7QUFDdEMsY0FBR1AsS0FBSCxFQUFTO0FBQUMsZ0JBQUl6VCxJQUFJLEdBQUMsQ0FBQzZQLE9BQUQsSUFBVTdQLElBQW5CLEVBQXlCO0FBQUM2VCwwQkFBWTtBQUFJOztBQUNwRCxnQkFBRzdMLElBQUgsRUFBUTtBQUFDOEgsdUJBQVMsQ0FBQ3JULElBQVYsQ0FBZXVELElBQWY7QUFBc0I7QUFBQztBQUFDOztBQUNqQzZULG9CQUFZLElBQUU5VixDQUFkOztBQUFnQixZQUFHMFYsS0FBSyxJQUFFMVYsQ0FBQyxLQUFHOFYsWUFBZCxFQUEyQjtBQUFDblQsV0FBQyxHQUFDLENBQUY7O0FBQUksaUJBQU9tUCxPQUFPLEdBQUMyRCxXQUFXLENBQUM5UyxDQUFDLEVBQUYsQ0FBMUIsRUFBaUM7QUFBQ21QLG1CQUFPLENBQUNDLFNBQUQsRUFBV2dFLFVBQVgsRUFBc0JoVixPQUF0QixFQUE4QmlRLEdBQTlCLENBQVA7QUFBMkM7O0FBQzdILGNBQUcvRyxJQUFILEVBQVE7QUFBQyxnQkFBRzZMLFlBQVksR0FBQyxDQUFoQixFQUFrQjtBQUFDLHFCQUFNOVYsQ0FBQyxFQUFQLEVBQVU7QUFBQyxvQkFBRyxFQUFFK1IsU0FBUyxDQUFDL1IsQ0FBRCxDQUFULElBQWMrVixVQUFVLENBQUMvVixDQUFELENBQTFCLENBQUgsRUFBa0M7QUFBQytWLDRCQUFVLENBQUMvVixDQUFELENBQVYsR0FBY29ILEdBQUcsQ0FBQzdJLElBQUosQ0FBU2tHLE9BQVQsQ0FBZDtBQUFpQztBQUFDO0FBQUM7O0FBQzdHc1Isc0JBQVUsR0FBQzNCLFFBQVEsQ0FBQzJCLFVBQUQsQ0FBbkI7QUFBaUM7O0FBQ2pDclgsY0FBSSxDQUFDRCxLQUFMLENBQVdnRyxPQUFYLEVBQW1Cc1IsVUFBbkI7O0FBQStCLGNBQUdGLFNBQVMsSUFBRSxDQUFDNUwsSUFBWixJQUFrQjhMLFVBQVUsQ0FBQzFVLE1BQVgsR0FBa0IsQ0FBcEMsSUFBd0N5VSxZQUFZLEdBQUNMLFdBQVcsQ0FBQ3BVLE1BQTFCLEdBQWtDLENBQTVFLEVBQThFO0FBQUNtRSxrQkFBTSxDQUFDZ0ssVUFBUCxDQUFrQi9LLE9BQWxCO0FBQTRCO0FBQUM7O0FBQzNJLFlBQUdvUixTQUFILEVBQWE7QUFBQ25QLGlCQUFPLEdBQUN1UCxhQUFSO0FBQXNCbFEsMEJBQWdCLEdBQUNpUSxhQUFqQjtBQUFnQzs7QUFDcEUsZUFBT2pFLFNBQVA7QUFBa0IsT0FYNkM7O0FBVzVDLGFBQU8yRCxLQUFLLEdBQUM5SixZQUFZLENBQUNnSyxZQUFELENBQWIsR0FBNEJBLFlBQXhDO0FBQXNEOztBQUN6RS9QLFdBQU8sR0FBQ0wsTUFBTSxDQUFDSyxPQUFQLEdBQWUsVUFBUy9FLFFBQVQsRUFBa0JzSixLQUFsQixFQUF3QjtBQUFDLFVBQUlwSyxDQUFKO0FBQUEsVUFBTXlWLFdBQVcsR0FBQyxFQUFsQjtBQUFBLFVBQXFCRCxlQUFlLEdBQUMsRUFBckM7QUFBQSxVQUF3Q2hDLE1BQU0sR0FBQ3pNLGFBQWEsQ0FBQ2pHLFFBQVEsR0FBQyxHQUFWLENBQTVEOztBQUEyRSxVQUFHLENBQUMwUyxNQUFKLEVBQVc7QUFBQyxZQUFHLENBQUNwSixLQUFKLEVBQVU7QUFBQ0EsZUFBSyxHQUFDeEUsUUFBUSxDQUFDOUUsUUFBRCxDQUFkO0FBQTBCOztBQUM1S2QsU0FBQyxHQUFDb0ssS0FBSyxDQUFDL0ksTUFBUjs7QUFBZSxlQUFNckIsQ0FBQyxFQUFQLEVBQVU7QUFBQ3dULGdCQUFNLEdBQUN5QixpQkFBaUIsQ0FBQzdLLEtBQUssQ0FBQ3BLLENBQUQsQ0FBTixDQUF4Qjs7QUFBbUMsY0FBR3dULE1BQU0sQ0FBQzdQLE9BQUQsQ0FBVCxFQUFtQjtBQUFDOFIsdUJBQVcsQ0FBQy9XLElBQVosQ0FBaUI4VSxNQUFqQjtBQUEwQixXQUE5QyxNQUFrRDtBQUFDZ0MsMkJBQWUsQ0FBQzlXLElBQWhCLENBQXFCOFUsTUFBckI7QUFBOEI7QUFBQzs7QUFDL0lBLGNBQU0sR0FBQ3pNLGFBQWEsQ0FBQ2pHLFFBQUQsRUFBVXlVLHdCQUF3QixDQUFDQyxlQUFELEVBQWlCQyxXQUFqQixDQUFsQyxDQUFwQjtBQUFxRmpDLGNBQU0sQ0FBQzFTLFFBQVAsR0FBZ0JBLFFBQWhCO0FBQTBCOztBQUMvRyxhQUFPMFMsTUFBUDtBQUFlLEtBSGY7O0FBR2dCMU4sVUFBTSxHQUFDTixNQUFNLENBQUNNLE1BQVAsR0FBYyxVQUFTaEYsUUFBVCxFQUFrQkMsT0FBbEIsRUFBMEIwRCxPQUExQixFQUFrQ3dGLElBQWxDLEVBQXVDO0FBQUMsVUFBSWpLLENBQUo7QUFBQSxVQUFNcVQsTUFBTjtBQUFBLFVBQWE2QyxLQUFiO0FBQUEsVUFBbUIxVyxJQUFuQjtBQUFBLFVBQXdCc08sSUFBeEI7QUFBQSxVQUE2QnFJLFFBQVEsR0FBQyxPQUFPclYsUUFBUCxLQUFrQixVQUFsQixJQUE4QkEsUUFBcEU7QUFBQSxVQUE2RXNKLEtBQUssR0FBQyxDQUFDSCxJQUFELElBQU9yRSxRQUFRLENBQUU5RSxRQUFRLEdBQUNxVixRQUFRLENBQUNyVixRQUFULElBQW1CQSxRQUE5QixDQUFsRztBQUEySTJELGFBQU8sR0FBQ0EsT0FBTyxJQUFFLEVBQWpCOztBQUFvQixVQUFHMkYsS0FBSyxDQUFDL0ksTUFBTixLQUFlLENBQWxCLEVBQW9CO0FBQUNnUyxjQUFNLEdBQUNqSixLQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVNBLEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBU2hNLEtBQVQsQ0FBZSxDQUFmLENBQWhCOztBQUFrQyxZQUFHaVYsTUFBTSxDQUFDaFMsTUFBUCxHQUFjLENBQWQsSUFBaUIsQ0FBQzZVLEtBQUssR0FBQzdDLE1BQU0sQ0FBQyxDQUFELENBQWIsRUFBa0I3VCxJQUFsQixLQUF5QixJQUExQyxJQUFnRHVCLE9BQU8sQ0FBQzFCLFFBQVIsS0FBbUIsQ0FBbkUsSUFBc0UrRyxjQUF0RSxJQUFzRlgsSUFBSSxDQUFDd0ssUUFBTCxDQUFjb0QsTUFBTSxDQUFDLENBQUQsQ0FBTixDQUFVN1QsSUFBeEIsQ0FBekYsRUFBdUg7QUFBQ3VCLGlCQUFPLEdBQUMsQ0FBQzBFLElBQUksQ0FBQ3FJLElBQUwsQ0FBVSxJQUFWLEVBQWdCb0ksS0FBSyxDQUFDcFIsT0FBTixDQUFjLENBQWQsRUFBaUJoQixPQUFqQixDQUF5QjZFLFNBQXpCLEVBQW1DQyxTQUFuQyxDQUFoQixFQUE4RDdILE9BQTlELEtBQXdFLEVBQXpFLEVBQTZFLENBQTdFLENBQVI7O0FBQXdGLGNBQUcsQ0FBQ0EsT0FBSixFQUFZO0FBQUMsbUJBQU8wRCxPQUFQO0FBQWdCLFdBQTdCLE1BQWtDLElBQUcwUixRQUFILEVBQVk7QUFBQ3BWLG1CQUFPLEdBQUNBLE9BQU8sQ0FBQ04sVUFBaEI7QUFBNEI7O0FBQzlqQkssa0JBQVEsR0FBQ0EsUUFBUSxDQUFDMUMsS0FBVCxDQUFlaVYsTUFBTSxDQUFDMUgsS0FBUCxHQUFlMUcsS0FBZixDQUFxQjVELE1BQXBDLENBQVQ7QUFBc0Q7O0FBQ3REckIsU0FBQyxHQUFDb0ksU0FBUyxDQUFDLGNBQUQsQ0FBVCxDQUEwQjJDLElBQTFCLENBQStCakssUUFBL0IsSUFBeUMsQ0FBekMsR0FBMkN1UyxNQUFNLENBQUNoUyxNQUFwRDs7QUFBMkQsZUFBTXJCLENBQUMsRUFBUCxFQUFVO0FBQUNrVyxlQUFLLEdBQUM3QyxNQUFNLENBQUNyVCxDQUFELENBQVo7O0FBQWdCLGNBQUd5RixJQUFJLENBQUN3SyxRQUFMLENBQWV6USxJQUFJLEdBQUMwVyxLQUFLLENBQUMxVyxJQUExQixDQUFILEVBQW9DO0FBQUM7QUFBTzs7QUFDbEksY0FBSXNPLElBQUksR0FBQ3JJLElBQUksQ0FBQ3FJLElBQUwsQ0FBVXRPLElBQVYsQ0FBVCxFQUEwQjtBQUFDLGdCQUFJeUssSUFBSSxHQUFDNkQsSUFBSSxDQUFDb0ksS0FBSyxDQUFDcFIsT0FBTixDQUFjLENBQWQsRUFBaUJoQixPQUFqQixDQUF5QjZFLFNBQXpCLEVBQW1DQyxTQUFuQyxDQUFELEVBQStDRixRQUFRLENBQUNxQyxJQUFULENBQWNzSSxNQUFNLENBQUMsQ0FBRCxDQUFOLENBQVU3VCxJQUF4QixLQUErQndMLFdBQVcsQ0FBQ2pLLE9BQU8sQ0FBQ04sVUFBVCxDQUExQyxJQUFnRU0sT0FBL0csQ0FBYixFQUFzSTtBQUFDc1Msb0JBQU0sQ0FBQ3ZRLE1BQVAsQ0FBYzlDLENBQWQsRUFBZ0IsQ0FBaEI7QUFBbUJjLHNCQUFRLEdBQUNtSixJQUFJLENBQUM1SSxNQUFMLElBQWE2SixVQUFVLENBQUNtSSxNQUFELENBQWhDOztBQUF5QyxrQkFBRyxDQUFDdlMsUUFBSixFQUFhO0FBQUNwQyxvQkFBSSxDQUFDRCxLQUFMLENBQVdnRyxPQUFYLEVBQW1Cd0YsSUFBbkI7QUFBeUIsdUJBQU94RixPQUFQO0FBQWdCOztBQUNyUjtBQUFPO0FBQUM7QUFBQztBQUFDOztBQUNWLE9BQUMwUixRQUFRLElBQUV0USxPQUFPLENBQUMvRSxRQUFELEVBQVVzSixLQUFWLENBQWxCLEVBQW9DSCxJQUFwQyxFQUF5Q2xKLE9BQXpDLEVBQWlELENBQUNxRixjQUFsRCxFQUFpRTNCLE9BQWpFLEVBQXlFLENBQUMxRCxPQUFELElBQVUySCxRQUFRLENBQUNxQyxJQUFULENBQWNqSyxRQUFkLEtBQXlCa0ssV0FBVyxDQUFDakssT0FBTyxDQUFDTixVQUFULENBQTlDLElBQW9FTSxPQUE3STtBQUFzSixhQUFPMEQsT0FBUDtBQUFnQixLQUx0Sjs7QUFLdUp2RixXQUFPLENBQUN5USxVQUFSLEdBQW1CaE0sT0FBTyxDQUFDMEIsS0FBUixDQUFjLEVBQWQsRUFBa0J4QyxJQUFsQixDQUF1Qm9FLFNBQXZCLEVBQWtDa0UsSUFBbEMsQ0FBdUMsRUFBdkMsTUFBNkN4SCxPQUFoRTtBQUF3RXpFLFdBQU8sQ0FBQ3dRLGdCQUFSLEdBQXlCLENBQUMsQ0FBQ3pKLFlBQTNCO0FBQXdDQyxlQUFXO0FBQUdoSCxXQUFPLENBQUM0UCxZQUFSLEdBQXFCakQsTUFBTSxDQUFDLFVBQVNDLEVBQVQsRUFBWTtBQUFDLGFBQU9BLEVBQUUsQ0FBQzRDLHVCQUFILENBQTJCL1EsUUFBUSxDQUFDd0MsYUFBVCxDQUF1QixVQUF2QixDQUEzQixJQUErRCxDQUF0RTtBQUF5RSxLQUF2RixDQUEzQjs7QUFBb0gsUUFBRyxDQUFDMEwsTUFBTSxDQUFDLFVBQVNDLEVBQVQsRUFBWTtBQUFDQSxRQUFFLENBQUNxQyxTQUFILEdBQWEsa0JBQWI7QUFBZ0MsYUFBT3JDLEVBQUUsQ0FBQytELFVBQUgsQ0FBY3hQLFlBQWQsQ0FBMkIsTUFBM0IsTUFBcUMsR0FBNUM7QUFBaUQsS0FBL0YsQ0FBVixFQUEyRztBQUFDMEwsZUFBUyxDQUFDLHdCQUFELEVBQTBCLFVBQVM5SixJQUFULEVBQWNnQixJQUFkLEVBQW1CMEMsS0FBbkIsRUFBeUI7QUFBQyxZQUFHLENBQUNBLEtBQUosRUFBVTtBQUFDLGlCQUFPMUQsSUFBSSxDQUFDNUIsWUFBTCxDQUFrQjRDLElBQWxCLEVBQXVCQSxJQUFJLENBQUNzQyxXQUFMLE9BQXFCLE1BQXJCLEdBQTRCLENBQTVCLEdBQThCLENBQXJELENBQVA7QUFBZ0U7QUFBQyxPQUFoSSxDQUFUO0FBQTRJOztBQUNqcEIsUUFBRyxDQUFDckcsT0FBTyxDQUFDd0ksVUFBVCxJQUFxQixDQUFDbUUsTUFBTSxDQUFDLFVBQVNDLEVBQVQsRUFBWTtBQUFDQSxRQUFFLENBQUNxQyxTQUFILEdBQWEsVUFBYjtBQUF3QnJDLFFBQUUsQ0FBQytELFVBQUgsQ0FBY3ZQLFlBQWQsQ0FBMkIsT0FBM0IsRUFBbUMsRUFBbkM7QUFBdUMsYUFBT3dMLEVBQUUsQ0FBQytELFVBQUgsQ0FBY3hQLFlBQWQsQ0FBMkIsT0FBM0IsTUFBc0MsRUFBN0M7QUFBaUQsS0FBOUgsQ0FBL0IsRUFBK0o7QUFBQzBMLGVBQVMsQ0FBQyxPQUFELEVBQVMsVUFBUzlKLElBQVQsRUFBY21VLEtBQWQsRUFBb0J6USxLQUFwQixFQUEwQjtBQUFDLFlBQUcsQ0FBQ0EsS0FBRCxJQUFRMUQsSUFBSSxDQUFDMEgsUUFBTCxDQUFjcEUsV0FBZCxPQUE4QixPQUF6QyxFQUFpRDtBQUFDLGlCQUFPdEQsSUFBSSxDQUFDb1UsWUFBWjtBQUEwQjtBQUFDLE9BQWpILENBQVQ7QUFBNkg7O0FBQzdSLFFBQUcsQ0FBQ3hLLE1BQU0sQ0FBQyxVQUFTQyxFQUFULEVBQVk7QUFBQyxhQUFPQSxFQUFFLENBQUN6TCxZQUFILENBQWdCLFVBQWhCLEtBQTZCLElBQXBDO0FBQTBDLEtBQXhELENBQVYsRUFBb0U7QUFBQzBMLGVBQVMsQ0FBQ3hFLFFBQUQsRUFBVSxVQUFTdEYsSUFBVCxFQUFjZ0IsSUFBZCxFQUFtQjBDLEtBQW5CLEVBQXlCO0FBQUMsWUFBSTFGLEdBQUo7O0FBQVEsWUFBRyxDQUFDMEYsS0FBSixFQUFVO0FBQUMsaUJBQU8xRCxJQUFJLENBQUNnQixJQUFELENBQUosS0FBYSxJQUFiLEdBQWtCQSxJQUFJLENBQUNzQyxXQUFMLEVBQWxCLEdBQXFDLENBQUN0RixHQUFHLEdBQUNnQyxJQUFJLENBQUM4TCxnQkFBTCxDQUFzQjlLLElBQXRCLENBQUwsS0FBbUNoRCxHQUFHLENBQUNxUCxTQUF2QyxHQUFpRHJQLEdBQUcsQ0FBQ2dGLEtBQXJELEdBQTJELElBQXZHO0FBQTZHO0FBQUMsT0FBckssQ0FBVDtBQUFpTDs7QUFDdFAsV0FBT08sTUFBUDtBQUFlLEdBckpmLENBcUppQjFILE1BckpqQixDQVhBOztBQWdLeUIrQyxRQUFNLENBQUNpTixJQUFQLEdBQVl0SSxNQUFaO0FBQW1CM0UsUUFBTSxDQUFDc08sSUFBUCxHQUFZM0osTUFBTSxDQUFDdUssU0FBbkI7QUFBNkJsUCxRQUFNLENBQUNzTyxJQUFQLENBQVksR0FBWixJQUFpQnRPLE1BQU0sQ0FBQ3NPLElBQVAsQ0FBWXhILE9BQTdCO0FBQXFDOUcsUUFBTSxDQUFDMk8sVUFBUCxHQUFrQjNPLE1BQU0sQ0FBQ3lWLE1BQVAsR0FBYzlRLE1BQU0sQ0FBQ2dLLFVBQXZDO0FBQWtEM08sUUFBTSxDQUFDVCxJQUFQLEdBQVlvRixNQUFNLENBQUNFLE9BQW5CO0FBQTJCN0UsUUFBTSxDQUFDMFYsUUFBUCxHQUFnQi9RLE1BQU0sQ0FBQ0csS0FBdkI7QUFBNkI5RSxRQUFNLENBQUMwRixRQUFQLEdBQWdCZixNQUFNLENBQUNlLFFBQXZCO0FBQWdDMUYsUUFBTSxDQUFDMlYsY0FBUCxHQUFzQmhSLE1BQU0sQ0FBQ3FELE1BQTdCOztBQUFvQyxNQUFJZSxHQUFHLEdBQUMsYUFBUzNILElBQVQsRUFBYzJILElBQWQsRUFBa0I2TSxLQUFsQixFQUF3QjtBQUFDLFFBQUk1RSxPQUFPLEdBQUMsRUFBWjtBQUFBLFFBQWU2RSxRQUFRLEdBQUNELEtBQUssS0FBRy9TLFNBQWhDOztBQUEwQyxXQUFNLENBQUN6QixJQUFJLEdBQUNBLElBQUksQ0FBQzJILElBQUQsQ0FBVixLQUFrQjNILElBQUksQ0FBQzVDLFFBQUwsS0FBZ0IsQ0FBeEMsRUFBMEM7QUFBQyxVQUFHNEMsSUFBSSxDQUFDNUMsUUFBTCxLQUFnQixDQUFuQixFQUFxQjtBQUFDLFlBQUdxWCxRQUFRLElBQUU3VixNQUFNLENBQUNvQixJQUFELENBQU4sQ0FBYTBVLEVBQWIsQ0FBZ0JGLEtBQWhCLENBQWIsRUFBb0M7QUFBQztBQUFPOztBQUNwZDVFLGVBQU8sQ0FBQ25ULElBQVIsQ0FBYXVELElBQWI7QUFBb0I7QUFBQzs7QUFDckIsV0FBTzRQLE9BQVA7QUFBZ0IsR0FGNFE7O0FBRTNRLE1BQUkrRSxTQUFRLEdBQUMsU0FBVEEsUUFBUyxDQUFTQyxDQUFULEVBQVc1VSxJQUFYLEVBQWdCO0FBQUMsUUFBSTRQLE9BQU8sR0FBQyxFQUFaOztBQUFlLFdBQUtnRixDQUFMLEVBQU9BLENBQUMsR0FBQ0EsQ0FBQyxDQUFDdEssV0FBWCxFQUF1QjtBQUFDLFVBQUdzSyxDQUFDLENBQUN4WCxRQUFGLEtBQWEsQ0FBYixJQUFnQndYLENBQUMsS0FBRzVVLElBQXZCLEVBQTRCO0FBQUM0UCxlQUFPLENBQUNuVCxJQUFSLENBQWFtWSxDQUFiO0FBQWlCO0FBQUM7O0FBQ3JJLFdBQU9oRixPQUFQO0FBQWdCLEdBREM7O0FBQ0EsTUFBSWlGLGFBQWEsR0FBQ2pXLE1BQU0sQ0FBQ3NPLElBQVAsQ0FBWS9FLEtBQVosQ0FBa0IyTSxZQUFwQzs7QUFBaUQsV0FBU3BOLFFBQVQsQ0FBa0IxSCxJQUFsQixFQUF1QmdCLElBQXZCLEVBQTRCO0FBQUMsV0FBT2hCLElBQUksQ0FBQzBILFFBQUwsSUFBZTFILElBQUksQ0FBQzBILFFBQUwsQ0FBY3BFLFdBQWQsT0FBOEJ0QyxJQUFJLENBQUNzQyxXQUFMLEVBQXBEO0FBQXdFOztBQUFBO0FBQUMsTUFBSXlSLFVBQVUsR0FBRSxpRUFBaEI7O0FBQW1GLFdBQVNDLE1BQVQsQ0FBZ0I3SCxRQUFoQixFQUF5QjhILFNBQXpCLEVBQW1DQyxHQUFuQyxFQUF1QztBQUFDLFFBQUdoWSxVQUFVLENBQUMrWCxTQUFELENBQWIsRUFBeUI7QUFBQyxhQUFPclcsTUFBTSxDQUFDMEIsSUFBUCxDQUFZNk0sUUFBWixFQUFxQixVQUFTbk4sSUFBVCxFQUFjakMsQ0FBZCxFQUFnQjtBQUFDLGVBQU0sQ0FBQyxDQUFDa1gsU0FBUyxDQUFDM1ksSUFBVixDQUFlMEQsSUFBZixFQUFvQmpDLENBQXBCLEVBQXNCaUMsSUFBdEIsQ0FBRixLQUFnQ2tWLEdBQXRDO0FBQTJDLE9BQWpGLENBQVA7QUFBMkY7O0FBQ3haLFFBQUdELFNBQVMsQ0FBQzdYLFFBQWIsRUFBc0I7QUFBQyxhQUFPd0IsTUFBTSxDQUFDMEIsSUFBUCxDQUFZNk0sUUFBWixFQUFxQixVQUFTbk4sSUFBVCxFQUFjO0FBQUMsZUFBT0EsSUFBSSxLQUFHaVYsU0FBUixLQUFxQkMsR0FBM0I7QUFBZ0MsT0FBcEUsQ0FBUDtBQUE4RTs7QUFDckcsUUFBRyxPQUFPRCxTQUFQLEtBQW1CLFFBQXRCLEVBQStCO0FBQUMsYUFBT3JXLE1BQU0sQ0FBQzBCLElBQVAsQ0FBWTZNLFFBQVosRUFBcUIsVUFBU25OLElBQVQsRUFBYztBQUFDLGVBQU90RCxPQUFPLENBQUNKLElBQVIsQ0FBYTJZLFNBQWIsRUFBdUJqVixJQUF2QixJQUE2QixDQUFDLENBQS9CLEtBQW9Da1YsR0FBMUM7QUFBK0MsT0FBbkYsQ0FBUDtBQUE2Rjs7QUFDN0gsV0FBT3RXLE1BQU0sQ0FBQytNLE1BQVAsQ0FBY3NKLFNBQWQsRUFBd0I5SCxRQUF4QixFQUFpQytILEdBQWpDLENBQVA7QUFBOEM7O0FBQzlDdFcsUUFBTSxDQUFDK00sTUFBUCxHQUFjLFVBQVN1QixJQUFULEVBQWN6TixLQUFkLEVBQW9CeVYsR0FBcEIsRUFBd0I7QUFBQyxRQUFJbFYsSUFBSSxHQUFDUCxLQUFLLENBQUMsQ0FBRCxDQUFkOztBQUFrQixRQUFHeVYsR0FBSCxFQUFPO0FBQUNoSSxVQUFJLEdBQUMsVUFBUUEsSUFBUixHQUFhLEdBQWxCO0FBQXVCOztBQUN4RixRQUFHek4sS0FBSyxDQUFDTCxNQUFOLEtBQWUsQ0FBZixJQUFrQlksSUFBSSxDQUFDNUMsUUFBTCxLQUFnQixDQUFyQyxFQUF1QztBQUFDLGFBQU93QixNQUFNLENBQUNpTixJQUFQLENBQVlNLGVBQVosQ0FBNEJuTSxJQUE1QixFQUFpQ2tOLElBQWpDLElBQXVDLENBQUNsTixJQUFELENBQXZDLEdBQThDLEVBQXJEO0FBQXlEOztBQUNqRyxXQUFPcEIsTUFBTSxDQUFDaU4sSUFBUCxDQUFZaEosT0FBWixDQUFvQnFLLElBQXBCLEVBQXlCdE8sTUFBTSxDQUFDMEIsSUFBUCxDQUFZYixLQUFaLEVBQWtCLFVBQVNPLElBQVQsRUFBYztBQUFDLGFBQU9BLElBQUksQ0FBQzVDLFFBQUwsS0FBZ0IsQ0FBdkI7QUFBMEIsS0FBM0QsQ0FBekIsQ0FBUDtBQUErRixHQUYvRjs7QUFFZ0d3QixRQUFNLENBQUNHLEVBQVAsQ0FBVStCLE1BQVYsQ0FBaUI7QUFBQytLLFFBQUksRUFBQyxjQUFTaE4sUUFBVCxFQUFrQjtBQUFDLFVBQUlkLENBQUo7QUFBQSxVQUFNMkIsR0FBTjtBQUFBLFVBQVVlLEdBQUcsR0FBQyxLQUFLckIsTUFBbkI7QUFBQSxVQUEwQitWLElBQUksR0FBQyxJQUEvQjs7QUFBb0MsVUFBRyxPQUFPdFcsUUFBUCxLQUFrQixRQUFyQixFQUE4QjtBQUFDLGVBQU8sS0FBS1csU0FBTCxDQUFlWixNQUFNLENBQUNDLFFBQUQsQ0FBTixDQUFpQjhNLE1BQWpCLENBQXdCLFlBQVU7QUFBQyxlQUFJNU4sQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDMEMsR0FBVixFQUFjMUMsQ0FBQyxFQUFmLEVBQWtCO0FBQUMsZ0JBQUdhLE1BQU0sQ0FBQzBGLFFBQVAsQ0FBZ0I2USxJQUFJLENBQUNwWCxDQUFELENBQXBCLEVBQXdCLElBQXhCLENBQUgsRUFBaUM7QUFBQyxxQkFBTyxJQUFQO0FBQWE7QUFBQztBQUFDLFNBQXZHLENBQWYsQ0FBUDtBQUFpSTs7QUFDOVUyQixTQUFHLEdBQUMsS0FBS0YsU0FBTCxDQUFlLEVBQWYsQ0FBSjs7QUFBdUIsV0FBSXpCLENBQUMsR0FBQyxDQUFOLEVBQVFBLENBQUMsR0FBQzBDLEdBQVYsRUFBYzFDLENBQUMsRUFBZixFQUFrQjtBQUFDYSxjQUFNLENBQUNpTixJQUFQLENBQVloTixRQUFaLEVBQXFCc1csSUFBSSxDQUFDcFgsQ0FBRCxDQUF6QixFQUE2QjJCLEdBQTdCO0FBQW1DOztBQUM3RSxhQUFPZSxHQUFHLEdBQUMsQ0FBSixHQUFNN0IsTUFBTSxDQUFDMk8sVUFBUCxDQUFrQjdOLEdBQWxCLENBQU4sR0FBNkJBLEdBQXBDO0FBQXlDLEtBRndFO0FBRXZFaU0sVUFBTSxFQUFDLGdCQUFTOU0sUUFBVCxFQUFrQjtBQUFDLGFBQU8sS0FBS1csU0FBTCxDQUFld1YsTUFBTSxDQUFDLElBQUQsRUFBTW5XLFFBQVEsSUFBRSxFQUFoQixFQUFtQixLQUFuQixDQUFyQixDQUFQO0FBQXdELEtBRlg7QUFFWXFXLE9BQUcsRUFBQyxhQUFTclcsUUFBVCxFQUFrQjtBQUFDLGFBQU8sS0FBS1csU0FBTCxDQUFld1YsTUFBTSxDQUFDLElBQUQsRUFBTW5XLFFBQVEsSUFBRSxFQUFoQixFQUFtQixJQUFuQixDQUFyQixDQUFQO0FBQXVELEtBRjFGO0FBRTJGNlYsTUFBRSxFQUFDLFlBQVM3VixRQUFULEVBQWtCO0FBQUMsYUFBTSxDQUFDLENBQUNtVyxNQUFNLENBQUMsSUFBRCxFQUFNLE9BQU9uVyxRQUFQLEtBQWtCLFFBQWxCLElBQTRCZ1csYUFBYSxDQUFDL0wsSUFBZCxDQUFtQmpLLFFBQW5CLENBQTVCLEdBQXlERCxNQUFNLENBQUNDLFFBQUQsQ0FBL0QsR0FBMEVBLFFBQVEsSUFBRSxFQUExRixFQUE2RixLQUE3RixDQUFOLENBQTBHTyxNQUFsSDtBQUEwSDtBQUYzTyxHQUFqQjs7QUFFK1AsTUFBSWdXLFVBQUo7QUFBQSxNQUFlNU8sVUFBVSxHQUFDLHFDQUExQjtBQUFBLE1BQWdFeEgsSUFBSSxHQUFDSixNQUFNLENBQUNHLEVBQVAsQ0FBVUMsSUFBVixHQUFlLFVBQVNILFFBQVQsRUFBa0JDLE9BQWxCLEVBQTBCdVcsSUFBMUIsRUFBK0I7QUFBQyxRQUFJbE4sS0FBSixFQUFVbkksSUFBVjs7QUFBZSxRQUFHLENBQUNuQixRQUFKLEVBQWE7QUFBQyxhQUFPLElBQVA7QUFBYTs7QUFDN2Z3VyxRQUFJLEdBQUNBLElBQUksSUFBRUQsVUFBWDs7QUFBc0IsUUFBRyxPQUFPdlcsUUFBUCxLQUFrQixRQUFyQixFQUE4QjtBQUFDLFVBQUdBLFFBQVEsQ0FBQyxDQUFELENBQVIsS0FBYyxHQUFkLElBQW1CQSxRQUFRLENBQUNBLFFBQVEsQ0FBQ08sTUFBVCxHQUFnQixDQUFqQixDQUFSLEtBQThCLEdBQWpELElBQXNEUCxRQUFRLENBQUNPLE1BQVQsSUFBaUIsQ0FBMUUsRUFBNEU7QUFBQytJLGFBQUssR0FBQyxDQUFDLElBQUQsRUFBTXRKLFFBQU4sRUFBZSxJQUFmLENBQU47QUFBNEIsT0FBekcsTUFBNkc7QUFBQ3NKLGFBQUssR0FBQzNCLFVBQVUsQ0FBQ2dDLElBQVgsQ0FBZ0IzSixRQUFoQixDQUFOO0FBQWlDOztBQUNwTSxVQUFHc0osS0FBSyxLQUFHQSxLQUFLLENBQUMsQ0FBRCxDQUFMLElBQVUsQ0FBQ3JKLE9BQWQsQ0FBUixFQUErQjtBQUFDLFlBQUdxSixLQUFLLENBQUMsQ0FBRCxDQUFSLEVBQVk7QUFBQ3JKLGlCQUFPLEdBQUNBLE9BQU8sWUFBWUYsTUFBbkIsR0FBMEJFLE9BQU8sQ0FBQyxDQUFELENBQWpDLEdBQXFDQSxPQUE3QztBQUFxREYsZ0JBQU0sQ0FBQ2UsS0FBUCxDQUFhLElBQWIsRUFBa0JmLE1BQU0sQ0FBQzBXLFNBQVAsQ0FBaUJuTixLQUFLLENBQUMsQ0FBRCxDQUF0QixFQUEwQnJKLE9BQU8sSUFBRUEsT0FBTyxDQUFDMUIsUUFBakIsR0FBMEIwQixPQUFPLENBQUN5SixhQUFSLElBQXVCekosT0FBakQsR0FBeURwRCxRQUFuRixFQUE0RixJQUE1RixDQUFsQjs7QUFBcUgsY0FBR3FaLFVBQVUsQ0FBQ2pNLElBQVgsQ0FBZ0JYLEtBQUssQ0FBQyxDQUFELENBQXJCLEtBQTJCdkosTUFBTSxDQUFDMEMsYUFBUCxDQUFxQnhDLE9BQXJCLENBQTlCLEVBQTREO0FBQUMsaUJBQUlxSixLQUFKLElBQWFySixPQUFiLEVBQXFCO0FBQUMsa0JBQUc1QixVQUFVLENBQUMsS0FBS2lMLEtBQUwsQ0FBRCxDQUFiLEVBQTJCO0FBQUMscUJBQUtBLEtBQUwsRUFBWXJKLE9BQU8sQ0FBQ3FKLEtBQUQsQ0FBbkI7QUFBNkIsZUFBekQsTUFBNkQ7QUFBQyxxQkFBS2lGLElBQUwsQ0FBVWpGLEtBQVYsRUFBZ0JySixPQUFPLENBQUNxSixLQUFELENBQXZCO0FBQWlDO0FBQUM7QUFBQzs7QUFDM1ksaUJBQU8sSUFBUDtBQUFhLFNBRG1CLE1BQ2Y7QUFBQ25JLGNBQUksR0FBQ3RFLFFBQVEsQ0FBQytNLGNBQVQsQ0FBd0JOLEtBQUssQ0FBQyxDQUFELENBQTdCLENBQUw7O0FBQXVDLGNBQUduSSxJQUFILEVBQVE7QUFBQyxpQkFBSyxDQUFMLElBQVFBLElBQVI7QUFBYSxpQkFBS1osTUFBTCxHQUFZLENBQVo7QUFBZTs7QUFDOUYsaUJBQU8sSUFBUDtBQUFhO0FBQUMsT0FGZCxNQUVtQixJQUFHLENBQUNOLE9BQUQsSUFBVUEsT0FBTyxDQUFDSSxNQUFyQixFQUE0QjtBQUFDLGVBQU0sQ0FBQ0osT0FBTyxJQUFFdVcsSUFBVixFQUFnQnhKLElBQWhCLENBQXFCaE4sUUFBckIsQ0FBTjtBQUFzQyxPQUFuRSxNQUF1RTtBQUFDLGVBQU8sS0FBS00sV0FBTCxDQUFpQkwsT0FBakIsRUFBMEIrTSxJQUExQixDQUErQmhOLFFBQS9CLENBQVA7QUFBaUQ7QUFBQyxLQUh2SCxNQUc0SCxJQUFHQSxRQUFRLENBQUN6QixRQUFaLEVBQXFCO0FBQUMsV0FBSyxDQUFMLElBQVF5QixRQUFSO0FBQWlCLFdBQUtPLE1BQUwsR0FBWSxDQUFaO0FBQWMsYUFBTyxJQUFQO0FBQWEsS0FBbEUsTUFBdUUsSUFBR2xDLFVBQVUsQ0FBQzJCLFFBQUQsQ0FBYixFQUF3QjtBQUFDLGFBQU93VyxJQUFJLENBQUNFLEtBQUwsS0FBYTlULFNBQWIsR0FBdUI0VCxJQUFJLENBQUNFLEtBQUwsQ0FBVzFXLFFBQVgsQ0FBdkIsR0FBNENBLFFBQVEsQ0FBQ0QsTUFBRCxDQUEzRDtBQUFxRTs7QUFDdlQsV0FBT0EsTUFBTSxDQUFDMkQsU0FBUCxDQUFpQjFELFFBQWpCLEVBQTBCLElBQTFCLENBQVA7QUFBd0MsR0FMdVQ7O0FBS3RURyxNQUFJLENBQUNDLFNBQUwsR0FBZUwsTUFBTSxDQUFDRyxFQUF0QjtBQUF5QnFXLFlBQVUsR0FBQ3hXLE1BQU0sQ0FBQ2xELFFBQUQsQ0FBakI7QUFBNEIsTUFBSThaLFlBQVksR0FBQyxnQ0FBakI7QUFBQSxNQUFrREMsZ0JBQWdCLEdBQUM7QUFBQ0MsWUFBUSxFQUFDLElBQVY7QUFBZUMsWUFBUSxFQUFDLElBQXhCO0FBQTZCL04sUUFBSSxFQUFDLElBQWxDO0FBQXVDZ08sUUFBSSxFQUFDO0FBQTVDLEdBQW5FO0FBQXFIaFgsUUFBTSxDQUFDRyxFQUFQLENBQVUrQixNQUFWLENBQWlCO0FBQUMrVSxPQUFHLEVBQUMsYUFBU3pVLE1BQVQsRUFBZ0I7QUFBQyxVQUFJMFUsT0FBTyxHQUFDbFgsTUFBTSxDQUFDd0MsTUFBRCxFQUFRLElBQVIsQ0FBbEI7QUFBQSxVQUFnQzJVLENBQUMsR0FBQ0QsT0FBTyxDQUFDMVcsTUFBMUM7QUFBaUQsYUFBTyxLQUFLdU0sTUFBTCxDQUFZLFlBQVU7QUFBQyxZQUFJNU4sQ0FBQyxHQUFDLENBQU47O0FBQVEsZUFBS0EsQ0FBQyxHQUFDZ1ksQ0FBUCxFQUFTaFksQ0FBQyxFQUFWLEVBQWE7QUFBQyxjQUFHYSxNQUFNLENBQUMwRixRQUFQLENBQWdCLElBQWhCLEVBQXFCd1IsT0FBTyxDQUFDL1gsQ0FBRCxDQUE1QixDQUFILEVBQW9DO0FBQUMsbUJBQU8sSUFBUDtBQUFhO0FBQUM7QUFBQyxPQUFqRyxDQUFQO0FBQTJHLEtBQWxMO0FBQW1MaVksV0FBTyxFQUFDLGlCQUFTbEksU0FBVCxFQUFtQmhQLE9BQW5CLEVBQTJCO0FBQUMsVUFBSXFMLEdBQUo7QUFBQSxVQUFRcE0sQ0FBQyxHQUFDLENBQVY7QUFBQSxVQUFZZ1ksQ0FBQyxHQUFDLEtBQUszVyxNQUFuQjtBQUFBLFVBQTBCd1EsT0FBTyxHQUFDLEVBQWxDO0FBQUEsVUFBcUNrRyxPQUFPLEdBQUMsT0FBT2hJLFNBQVAsS0FBbUIsUUFBbkIsSUFBNkJsUCxNQUFNLENBQUNrUCxTQUFELENBQWhGOztBQUE0RixVQUFHLENBQUMrRyxhQUFhLENBQUMvTCxJQUFkLENBQW1CZ0YsU0FBbkIsQ0FBSixFQUFrQztBQUFDLGVBQUsvUCxDQUFDLEdBQUNnWSxDQUFQLEVBQVNoWSxDQUFDLEVBQVYsRUFBYTtBQUFDLGVBQUlvTSxHQUFHLEdBQUMsS0FBS3BNLENBQUwsQ0FBUixFQUFnQm9NLEdBQUcsSUFBRUEsR0FBRyxLQUFHckwsT0FBM0IsRUFBbUNxTCxHQUFHLEdBQUNBLEdBQUcsQ0FBQzNMLFVBQTNDLEVBQXNEO0FBQUMsZ0JBQUcyTCxHQUFHLENBQUMvTSxRQUFKLEdBQWEsRUFBYixLQUFrQjBZLE9BQU8sR0FBQ0EsT0FBTyxDQUFDRyxLQUFSLENBQWM5TCxHQUFkLElBQW1CLENBQUMsQ0FBckIsR0FBdUJBLEdBQUcsQ0FBQy9NLFFBQUosS0FBZSxDQUFmLElBQWtCd0IsTUFBTSxDQUFDaU4sSUFBUCxDQUFZTSxlQUFaLENBQTRCaEMsR0FBNUIsRUFBZ0MyRCxTQUFoQyxDQUFsRSxDQUFILEVBQWlIO0FBQUM4QixxQkFBTyxDQUFDblQsSUFBUixDQUFhME4sR0FBYjtBQUFrQjtBQUFPO0FBQUM7QUFBQztBQUFDOztBQUM3d0IsYUFBTyxLQUFLM0ssU0FBTCxDQUFlb1EsT0FBTyxDQUFDeFEsTUFBUixHQUFlLENBQWYsR0FBaUJSLE1BQU0sQ0FBQzJPLFVBQVAsQ0FBa0JxQyxPQUFsQixDQUFqQixHQUE0Q0EsT0FBM0QsQ0FBUDtBQUE0RSxLQUR3SjtBQUN2SnFHLFNBQUssRUFBQyxlQUFTalcsSUFBVCxFQUFjO0FBQUMsVUFBRyxDQUFDQSxJQUFKLEVBQVM7QUFBQyxlQUFPLEtBQUssQ0FBTCxLQUFTLEtBQUssQ0FBTCxFQUFReEIsVUFBbEIsR0FBOEIsS0FBSzBCLEtBQUwsR0FBYWdXLE9BQWIsR0FBdUI5VyxNQUFyRCxHQUE0RCxDQUFDLENBQW5FO0FBQXNFOztBQUNsTCxVQUFHLE9BQU9ZLElBQVAsS0FBYyxRQUFqQixFQUEwQjtBQUFDLGVBQU90RCxPQUFPLENBQUNKLElBQVIsQ0FBYXNDLE1BQU0sQ0FBQ29CLElBQUQsQ0FBbkIsRUFBMEIsS0FBSyxDQUFMLENBQTFCLENBQVA7QUFBMkM7O0FBQ3RFLGFBQU90RCxPQUFPLENBQUNKLElBQVIsQ0FBYSxJQUFiLEVBQWtCMEQsSUFBSSxDQUFDZCxNQUFMLEdBQVljLElBQUksQ0FBQyxDQUFELENBQWhCLEdBQW9CQSxJQUF0QyxDQUFQO0FBQW9ELEtBSGdMO0FBRy9LbVcsT0FBRyxFQUFDLGFBQVN0WCxRQUFULEVBQWtCQyxPQUFsQixFQUEwQjtBQUFDLGFBQU8sS0FBS1UsU0FBTCxDQUFlWixNQUFNLENBQUMyTyxVQUFQLENBQWtCM08sTUFBTSxDQUFDZSxLQUFQLENBQWEsS0FBS0wsR0FBTCxFQUFiLEVBQXdCVixNQUFNLENBQUNDLFFBQUQsRUFBVUMsT0FBVixDQUE5QixDQUFsQixDQUFmLENBQVA7QUFBNkYsS0FIbUQ7QUFHbERzWCxXQUFPLEVBQUMsaUJBQVN2WCxRQUFULEVBQWtCO0FBQUMsYUFBTyxLQUFLc1gsR0FBTCxDQUFTdFgsUUFBUSxJQUFFLElBQVYsR0FBZSxLQUFLZSxVQUFwQixHQUErQixLQUFLQSxVQUFMLENBQWdCK0wsTUFBaEIsQ0FBdUI5TSxRQUF2QixDQUF4QyxDQUFQO0FBQWtGO0FBSDNELEdBQWpCOztBQUcrRSxXQUFTd1gsT0FBVCxDQUFpQmxNLEdBQWpCLEVBQXFCeEMsR0FBckIsRUFBeUI7QUFBQyxXQUFNLENBQUN3QyxHQUFHLEdBQUNBLEdBQUcsQ0FBQ3hDLEdBQUQsQ0FBUixLQUFnQndDLEdBQUcsQ0FBQy9NLFFBQUosS0FBZSxDQUFyQyxFQUF1QyxDQUFFOztBQUNyVyxXQUFPK00sR0FBUDtBQUFZOztBQUNadkwsUUFBTSxDQUFDaUIsSUFBUCxDQUFZO0FBQUN1UCxVQUFNLEVBQUMsZ0JBQVNwUCxJQUFULEVBQWM7QUFBQyxVQUFJb1AsTUFBTSxHQUFDcFAsSUFBSSxDQUFDeEIsVUFBaEI7QUFBMkIsYUFBTzRRLE1BQU0sSUFBRUEsTUFBTSxDQUFDaFMsUUFBUCxLQUFrQixFQUExQixHQUE2QmdTLE1BQTdCLEdBQW9DLElBQTNDO0FBQWlELEtBQW5HO0FBQW9Ha0gsV0FBTyxFQUFDLGlCQUFTdFcsSUFBVCxFQUFjO0FBQUMsYUFBTzJILEdBQUcsQ0FBQzNILElBQUQsRUFBTSxZQUFOLENBQVY7QUFBK0IsS0FBMUo7QUFBMkp1VyxnQkFBWSxFQUFDLHNCQUFTdlcsSUFBVCxFQUFjcUQsRUFBZCxFQUFpQm1SLEtBQWpCLEVBQXVCO0FBQUMsYUFBTzdNLEdBQUcsQ0FBQzNILElBQUQsRUFBTSxZQUFOLEVBQW1Cd1UsS0FBbkIsQ0FBVjtBQUFxQyxLQUFyTztBQUFzTzVNLFFBQUksRUFBQyxjQUFTNUgsSUFBVCxFQUFjO0FBQUMsYUFBT3FXLE9BQU8sQ0FBQ3JXLElBQUQsRUFBTSxhQUFOLENBQWQ7QUFBb0MsS0FBOVI7QUFBK1I0VixRQUFJLEVBQUMsY0FBUzVWLElBQVQsRUFBYztBQUFDLGFBQU9xVyxPQUFPLENBQUNyVyxJQUFELEVBQU0saUJBQU4sQ0FBZDtBQUF3QyxLQUEzVjtBQUE0VndXLFdBQU8sRUFBQyxpQkFBU3hXLElBQVQsRUFBYztBQUFDLGFBQU8ySCxHQUFHLENBQUMzSCxJQUFELEVBQU0sYUFBTixDQUFWO0FBQWdDLEtBQW5aO0FBQW9aa1csV0FBTyxFQUFDLGlCQUFTbFcsSUFBVCxFQUFjO0FBQUMsYUFBTzJILEdBQUcsQ0FBQzNILElBQUQsRUFBTSxpQkFBTixDQUFWO0FBQW9DLEtBQS9jO0FBQWdkeVcsYUFBUyxFQUFDLG1CQUFTelcsSUFBVCxFQUFjcUQsRUFBZCxFQUFpQm1SLEtBQWpCLEVBQXVCO0FBQUMsYUFBTzdNLEdBQUcsQ0FBQzNILElBQUQsRUFBTSxhQUFOLEVBQW9Cd1UsS0FBcEIsQ0FBVjtBQUFzQyxLQUF4aEI7QUFBeWhCa0MsYUFBUyxFQUFDLG1CQUFTMVcsSUFBVCxFQUFjcUQsRUFBZCxFQUFpQm1SLEtBQWpCLEVBQXVCO0FBQUMsYUFBTzdNLEdBQUcsQ0FBQzNILElBQUQsRUFBTSxpQkFBTixFQUF3QndVLEtBQXhCLENBQVY7QUFBMEMsS0FBcm1CO0FBQXNtQkcsWUFBUSxFQUFDLGtCQUFTM1UsSUFBVCxFQUFjO0FBQUMsYUFBTzJVLFNBQVEsQ0FBQyxDQUFDM1UsSUFBSSxDQUFDeEIsVUFBTCxJQUFpQixFQUFsQixFQUFzQm9QLFVBQXZCLEVBQWtDNU4sSUFBbEMsQ0FBZjtBQUF3RCxLQUF0ckI7QUFBdXJCMFYsWUFBUSxFQUFDLGtCQUFTMVYsSUFBVCxFQUFjO0FBQUMsYUFBTzJVLFNBQVEsQ0FBQzNVLElBQUksQ0FBQzROLFVBQU4sQ0FBZjtBQUFrQyxLQUFqdkI7QUFBa3ZCK0gsWUFBUSxFQUFDLGtCQUFTM1YsSUFBVCxFQUFjO0FBQUMsVUFBR0EsSUFBSSxDQUFDMlcsZUFBTCxJQUFzQixJQUF0QixJQUE0QjNhLFFBQVEsQ0FBQ2dFLElBQUksQ0FBQzJXLGVBQU4sQ0FBdkMsRUFBOEQ7QUFBQyxlQUFPM1csSUFBSSxDQUFDMlcsZUFBWjtBQUE2Qjs7QUFDbDNCLFVBQUdqUCxRQUFRLENBQUMxSCxJQUFELEVBQU0sVUFBTixDQUFYLEVBQTZCO0FBQUNBLFlBQUksR0FBQ0EsSUFBSSxDQUFDNFcsT0FBTCxJQUFjNVcsSUFBbkI7QUFBeUI7O0FBQ3ZELGFBQU9wQixNQUFNLENBQUNlLEtBQVAsQ0FBYSxFQUFiLEVBQWdCSyxJQUFJLENBQUM2SCxVQUFyQixDQUFQO0FBQXlDO0FBRjdCLEdBQVosRUFFMkMsVUFBUzdHLElBQVQsRUFBY2pDLEVBQWQsRUFBaUI7QUFBQ0gsVUFBTSxDQUFDRyxFQUFQLENBQVVpQyxJQUFWLElBQWdCLFVBQVN3VCxLQUFULEVBQWUzVixRQUFmLEVBQXdCO0FBQUMsVUFBSStRLE9BQU8sR0FBQ2hSLE1BQU0sQ0FBQ21CLEdBQVAsQ0FBVyxJQUFYLEVBQWdCaEIsRUFBaEIsRUFBbUJ5VixLQUFuQixDQUFaOztBQUFzQyxVQUFHeFQsSUFBSSxDQUFDN0UsS0FBTCxDQUFXLENBQUMsQ0FBWixNQUFpQixPQUFwQixFQUE0QjtBQUFDMEMsZ0JBQVEsR0FBQzJWLEtBQVQ7QUFBZ0I7O0FBQ3pMLFVBQUczVixRQUFRLElBQUUsT0FBT0EsUUFBUCxLQUFrQixRQUEvQixFQUF3QztBQUFDK1EsZUFBTyxHQUFDaFIsTUFBTSxDQUFDK00sTUFBUCxDQUFjOU0sUUFBZCxFQUF1QitRLE9BQXZCLENBQVI7QUFBeUM7O0FBQ2xGLFVBQUcsS0FBS3hRLE1BQUwsR0FBWSxDQUFmLEVBQWlCO0FBQUMsWUFBRyxDQUFDcVcsZ0JBQWdCLENBQUN6VSxJQUFELENBQXBCLEVBQTJCO0FBQUNwQyxnQkFBTSxDQUFDMk8sVUFBUCxDQUFrQnFDLE9BQWxCO0FBQTRCOztBQUMxRSxZQUFHNEYsWUFBWSxDQUFDMU0sSUFBYixDQUFrQjlILElBQWxCLENBQUgsRUFBMkI7QUFBQzRPLGlCQUFPLENBQUNpSCxPQUFSO0FBQW1CO0FBQUM7O0FBQ2hELGFBQU8sS0FBS3JYLFNBQUwsQ0FBZW9RLE9BQWYsQ0FBUDtBQUFnQyxLQUo2QjtBQUkzQixHQU5sQztBQU1vQyxNQUFJa0gsYUFBYSxHQUFFLG1CQUFuQjs7QUFBd0MsV0FBU0MsYUFBVCxDQUF1QmhXLE9BQXZCLEVBQStCO0FBQUMsUUFBSWlXLE1BQU0sR0FBQyxFQUFYO0FBQWNwWSxVQUFNLENBQUNpQixJQUFQLENBQVlrQixPQUFPLENBQUNvSCxLQUFSLENBQWMyTyxhQUFkLEtBQThCLEVBQTFDLEVBQTZDLFVBQVNHLENBQVQsRUFBV0MsSUFBWCxFQUFnQjtBQUFDRixZQUFNLENBQUNFLElBQUQsQ0FBTixHQUFhLElBQWI7QUFBbUIsS0FBakY7QUFBbUYsV0FBT0YsTUFBUDtBQUFlOztBQUM1TnBZLFFBQU0sQ0FBQ3VZLFNBQVAsR0FBaUIsVUFBU3BXLE9BQVQsRUFBaUI7QUFBQ0EsV0FBTyxHQUFDLE9BQU9BLE9BQVAsS0FBaUIsUUFBakIsR0FBMEJnVyxhQUFhLENBQUNoVyxPQUFELENBQXZDLEdBQWlEbkMsTUFBTSxDQUFDa0MsTUFBUCxDQUFjLEVBQWQsRUFBaUJDLE9BQWpCLENBQXpEOztBQUFtRixRQUN0SHFXLE1BRHNIO0FBQUEsUUFDL0dDLE1BRCtHO0FBQUEsUUFDeEdDLE1BRHdHO0FBQUEsUUFDbEdDLE9BRGtHO0FBQUEsUUFDM0ZsUyxJQUFJLEdBQUMsRUFEc0Y7QUFBQSxRQUNuRm1TLEtBQUssR0FBQyxFQUQ2RTtBQUFBLFFBQzFFQyxXQUFXLEdBQUMsQ0FBQyxDQUQ2RDtBQUFBLFFBQzNEQyxJQUFJLEdBQUMsU0FBTEEsSUFBSyxHQUFVO0FBQUNILGFBQU0sR0FBQ0EsT0FBTSxJQUFFeFcsT0FBTyxDQUFDNFcsSUFBdkI7QUFBNEJMLFlBQUssR0FBQ0YsTUFBTSxHQUFDLElBQWI7O0FBQWtCLGFBQUtJLEtBQUssQ0FBQ3BZLE1BQVgsRUFBa0JxWSxXQUFXLEdBQUMsQ0FBQyxDQUEvQixFQUFpQztBQUFDSixjQUFNLEdBQUNHLEtBQUssQ0FBQzlOLEtBQU4sRUFBUDs7QUFBcUIsZUFBTSxFQUFFK04sV0FBRixHQUFjcFMsSUFBSSxDQUFDakcsTUFBekIsRUFBZ0M7QUFBQyxjQUFHaUcsSUFBSSxDQUFDb1MsV0FBRCxDQUFKLENBQWtCamIsS0FBbEIsQ0FBd0I2YSxNQUFNLENBQUMsQ0FBRCxDQUE5QixFQUFrQ0EsTUFBTSxDQUFDLENBQUQsQ0FBeEMsTUFBK0MsS0FBL0MsSUFBc0R0VyxPQUFPLENBQUM2VyxXQUFqRSxFQUE2RTtBQUFDSCx1QkFBVyxHQUFDcFMsSUFBSSxDQUFDakcsTUFBakI7QUFBd0JpWSxrQkFBTSxHQUFDLEtBQVA7QUFBYztBQUFDO0FBQUM7O0FBQ3ZVLFVBQUcsQ0FBQ3RXLE9BQU8sQ0FBQ3NXLE1BQVosRUFBbUI7QUFBQ0EsY0FBTSxHQUFDLEtBQVA7QUFBYzs7QUFDbENELFlBQU0sR0FBQyxLQUFQOztBQUFhLFVBQUdHLE9BQUgsRUFBVTtBQUFDLFlBQUdGLE1BQUgsRUFBVTtBQUFDaFMsY0FBSSxHQUFDLEVBQUw7QUFBUyxTQUFwQixNQUF3QjtBQUFDQSxjQUFJLEdBQUMsRUFBTDtBQUFTO0FBQUM7QUFBQyxLQUgwRDtBQUFBLFFBR3pEOFAsSUFBSSxHQUFDO0FBQUNnQixTQUFHLEVBQUMsZUFBVTtBQUFDLFlBQUc5USxJQUFILEVBQVE7QUFBQyxjQUFHZ1MsTUFBTSxJQUFFLENBQUNELE1BQVosRUFBbUI7QUFBQ0ssdUJBQVcsR0FBQ3BTLElBQUksQ0FBQ2pHLE1BQUwsR0FBWSxDQUF4QjtBQUEwQm9ZLGlCQUFLLENBQUMvYSxJQUFOLENBQVc0YSxNQUFYO0FBQW9COztBQUM3SixXQUFDLFNBQVNsQixHQUFULENBQWExRyxJQUFiLEVBQWtCO0FBQUM3USxrQkFBTSxDQUFDaUIsSUFBUCxDQUFZNFAsSUFBWixFQUFpQixVQUFTd0gsQ0FBVCxFQUFXbFUsR0FBWCxFQUFlO0FBQUMsa0JBQUc3RixVQUFVLENBQUM2RixHQUFELENBQWIsRUFBbUI7QUFBQyxvQkFBRyxDQUFDaEMsT0FBTyxDQUFDc1QsTUFBVCxJQUFpQixDQUFDYyxJQUFJLENBQUNVLEdBQUwsQ0FBUzlTLEdBQVQsQ0FBckIsRUFBbUM7QUFBQ3NDLHNCQUFJLENBQUM1SSxJQUFMLENBQVVzRyxHQUFWO0FBQWdCO0FBQUMsZUFBekUsTUFBOEUsSUFBR0EsR0FBRyxJQUFFQSxHQUFHLENBQUMzRCxNQUFULElBQWlCVixNQUFNLENBQUNxRSxHQUFELENBQU4sS0FBYyxRQUFsQyxFQUEyQztBQUFDb1QsbUJBQUcsQ0FBQ3BULEdBQUQsQ0FBSDtBQUFVO0FBQUMsYUFBdEs7QUFBeUssV0FBN0wsRUFBK0w5QyxTQUEvTDs7QUFBME0sY0FBR29YLE1BQU0sSUFBRSxDQUFDRCxNQUFaLEVBQW1CO0FBQUNNLGdCQUFJO0FBQUk7QUFBQzs7QUFDdk8sZUFBTyxJQUFQO0FBQWEsT0FGcUQ7QUFFcERHLFlBQU0sRUFBQyxrQkFBVTtBQUFDalosY0FBTSxDQUFDaUIsSUFBUCxDQUFZSSxTQUFaLEVBQXNCLFVBQVNnWCxDQUFULEVBQVdsVSxHQUFYLEVBQWU7QUFBQyxjQUFJa1QsS0FBSjs7QUFBVSxpQkFBTSxDQUFDQSxLQUFLLEdBQUNyWCxNQUFNLENBQUM2RCxPQUFQLENBQWVNLEdBQWYsRUFBbUJzQyxJQUFuQixFQUF3QjRRLEtBQXhCLENBQVAsSUFBdUMsQ0FBQyxDQUE5QyxFQUFnRDtBQUFDNVEsZ0JBQUksQ0FBQ3hFLE1BQUwsQ0FBWW9WLEtBQVosRUFBa0IsQ0FBbEI7O0FBQXFCLGdCQUFHQSxLQUFLLElBQUV3QixXQUFWLEVBQXNCO0FBQUNBLHlCQUFXO0FBQUk7QUFBQztBQUFDLFNBQTlKO0FBQWdLLGVBQU8sSUFBUDtBQUFhLE9BRjNJO0FBRTRJNUIsU0FBRyxFQUFDLGFBQVM5VyxFQUFULEVBQVk7QUFBQyxlQUFPQSxFQUFFLEdBQUNILE1BQU0sQ0FBQzZELE9BQVAsQ0FBZTFELEVBQWYsRUFBa0JzRyxJQUFsQixJQUF3QixDQUFDLENBQTFCLEdBQTRCQSxJQUFJLENBQUNqRyxNQUFMLEdBQVksQ0FBakQ7QUFBb0QsT0FGak47QUFFa04wWSxXQUFLLEVBQUMsaUJBQVU7QUFBQyxZQUFHelMsSUFBSCxFQUFRO0FBQUNBLGNBQUksR0FBQyxFQUFMO0FBQVM7O0FBQ3ZULGVBQU8sSUFBUDtBQUFhLE9BSHFEO0FBR3BEMFMsYUFBTyxFQUFDLG1CQUFVO0FBQUNSLGVBQU0sR0FBQ0MsS0FBSyxHQUFDLEVBQWI7QUFBZ0JuUyxZQUFJLEdBQUNnUyxNQUFNLEdBQUMsRUFBWjtBQUFlLGVBQU8sSUFBUDtBQUFhLE9BSFg7QUFHWTVQLGNBQVEsRUFBQyxvQkFBVTtBQUFDLGVBQU0sQ0FBQ3BDLElBQVA7QUFBYSxPQUg3QztBQUc4QzJTLFVBQUksRUFBQyxnQkFBVTtBQUFDVCxlQUFNLEdBQUNDLEtBQUssR0FBQyxFQUFiOztBQUFnQixZQUFHLENBQUNILE1BQUQsSUFBUyxDQUFDRCxNQUFiLEVBQW9CO0FBQUMvUixjQUFJLEdBQUNnUyxNQUFNLEdBQUMsRUFBWjtBQUFnQjs7QUFDckwsZUFBTyxJQUFQO0FBQWEsT0FKcUQ7QUFJcERFLFlBQU0sRUFBQyxrQkFBVTtBQUFDLGVBQU0sQ0FBQyxDQUFDQSxPQUFSO0FBQWdCLE9BSmtCO0FBSWpCVSxjQUFRLEVBQUMsa0JBQVNuWixPQUFULEVBQWlCMlEsSUFBakIsRUFBc0I7QUFBQyxZQUFHLENBQUM4SCxPQUFKLEVBQVc7QUFBQzlILGNBQUksR0FBQ0EsSUFBSSxJQUFFLEVBQVg7QUFBY0EsY0FBSSxHQUFDLENBQUMzUSxPQUFELEVBQVMyUSxJQUFJLENBQUN0VCxLQUFMLEdBQVdzVCxJQUFJLENBQUN0VCxLQUFMLEVBQVgsR0FBd0JzVCxJQUFqQyxDQUFMO0FBQTRDK0gsZUFBSyxDQUFDL2EsSUFBTixDQUFXZ1QsSUFBWDs7QUFBaUIsY0FBRyxDQUFDMkgsTUFBSixFQUFXO0FBQUNNLGdCQUFJO0FBQUk7QUFBQzs7QUFDN0wsZUFBTyxJQUFQO0FBQWEsT0FMcUQ7QUFLcERBLFVBQUksRUFBQyxnQkFBVTtBQUFDdkMsWUFBSSxDQUFDOEMsUUFBTCxDQUFjLElBQWQsRUFBbUJoWSxTQUFuQjtBQUE4QixlQUFPLElBQVA7QUFBYSxPQUxQO0FBS1FxWCxXQUFLLEVBQUMsaUJBQVU7QUFBQyxlQUFNLENBQUMsQ0FBQ0EsTUFBUjtBQUFlO0FBTHhDLEtBSG9EOztBQVFWLFdBQU9uQyxJQUFQO0FBQWEsR0FSekg7O0FBUTBILFdBQVMrQyxRQUFULENBQWtCQyxDQUFsQixFQUFvQjtBQUFDLFdBQU9BLENBQVA7QUFBVTs7QUFDekosV0FBU0MsT0FBVCxDQUFpQkMsRUFBakIsRUFBb0I7QUFBQyxVQUFNQSxFQUFOO0FBQVU7O0FBQy9CLFdBQVNDLFVBQVQsQ0FBb0J0VixLQUFwQixFQUEwQnVWLE9BQTFCLEVBQWtDQyxNQUFsQyxFQUF5Q0MsT0FBekMsRUFBaUQ7QUFBQyxRQUFJQyxNQUFKOztBQUFXLFFBQUc7QUFBQyxVQUFHMVYsS0FBSyxJQUFFOUYsVUFBVSxDQUFFd2IsTUFBTSxHQUFDMVYsS0FBSyxDQUFDMlYsT0FBZixDQUFwQixFQUE2QztBQUFDRCxjQUFNLENBQUNwYyxJQUFQLENBQVkwRyxLQUFaLEVBQW1CMEIsSUFBbkIsQ0FBd0I2VCxPQUF4QixFQUFpQ0ssSUFBakMsQ0FBc0NKLE1BQXRDO0FBQStDLE9BQTdGLE1BQWtHLElBQUd4VixLQUFLLElBQUU5RixVQUFVLENBQUV3YixNQUFNLEdBQUMxVixLQUFLLENBQUM2VixJQUFmLENBQXBCLEVBQTBDO0FBQUNILGNBQU0sQ0FBQ3BjLElBQVAsQ0FBWTBHLEtBQVosRUFBa0J1VixPQUFsQixFQUEwQkMsTUFBMUI7QUFBbUMsT0FBOUUsTUFBa0Y7QUFBQ0QsZUFBTyxDQUFDL2IsS0FBUixDQUFjaUYsU0FBZCxFQUF3QixDQUFDdUIsS0FBRCxFQUFRN0csS0FBUixDQUFjc2MsT0FBZCxDQUF4QjtBQUFpRDtBQUFDLEtBQTNPLENBQTJPLE9BQU16VixLQUFOLEVBQVk7QUFBQ3dWLFlBQU0sQ0FBQ2hjLEtBQVAsQ0FBYWlGLFNBQWIsRUFBdUIsQ0FBQ3VCLEtBQUQsQ0FBdkI7QUFBaUM7QUFBQzs7QUFDdlZwRSxRQUFNLENBQUNrQyxNQUFQLENBQWM7QUFBQ2dZLFlBQVEsRUFBQyxrQkFBU0MsSUFBVCxFQUFjO0FBQUMsVUFBSUMsTUFBTSxHQUFDLENBQUMsQ0FBQyxRQUFELEVBQVUsVUFBVixFQUFxQnBhLE1BQU0sQ0FBQ3VZLFNBQVAsQ0FBaUIsUUFBakIsQ0FBckIsRUFBZ0R2WSxNQUFNLENBQUN1WSxTQUFQLENBQWlCLFFBQWpCLENBQWhELEVBQTJFLENBQTNFLENBQUQsRUFBK0UsQ0FBQyxTQUFELEVBQVcsTUFBWCxFQUFrQnZZLE1BQU0sQ0FBQ3VZLFNBQVAsQ0FBaUIsYUFBakIsQ0FBbEIsRUFBa0R2WSxNQUFNLENBQUN1WSxTQUFQLENBQWlCLGFBQWpCLENBQWxELEVBQWtGLENBQWxGLEVBQW9GLFVBQXBGLENBQS9FLEVBQStLLENBQUMsUUFBRCxFQUFVLE1BQVYsRUFBaUJ2WSxNQUFNLENBQUN1WSxTQUFQLENBQWlCLGFBQWpCLENBQWpCLEVBQWlEdlksTUFBTSxDQUFDdVksU0FBUCxDQUFpQixhQUFqQixDQUFqRCxFQUFpRixDQUFqRixFQUFtRixVQUFuRixDQUEvSyxDQUFYO0FBQUEsVUFBMFI4QixNQUFLLEdBQUMsU0FBaFM7QUFBQSxVQUEwU04sUUFBTyxHQUFDO0FBQUNNLGFBQUssRUFBQyxpQkFBVTtBQUFDLGlCQUFPQSxNQUFQO0FBQWMsU0FBaEM7QUFBaUNDLGNBQU0sRUFBQyxrQkFBVTtBQUFDQyxrQkFBUSxDQUFDelUsSUFBVCxDQUFjekUsU0FBZCxFQUF5QjJZLElBQXpCLENBQThCM1ksU0FBOUI7QUFBeUMsaUJBQU8sSUFBUDtBQUFhLFNBQXpHO0FBQTBHLGlCQUFRLGdCQUFTbEIsRUFBVCxFQUFZO0FBQUMsaUJBQU80WixRQUFPLENBQUNFLElBQVIsQ0FBYSxJQUFiLEVBQWtCOVosRUFBbEIsQ0FBUDtBQUE4QixTQUE3SjtBQUE4SnFhLFlBQUksRUFBQyxnQkFBVTtBQUFDLGNBQUlDLEdBQUcsR0FBQ3BaLFNBQVI7QUFBa0IsaUJBQU9yQixNQUFNLENBQUNrYSxRQUFQLENBQWdCLFVBQVNRLFFBQVQsRUFBa0I7QUFBQzFhLGtCQUFNLENBQUNpQixJQUFQLENBQVltWixNQUFaLEVBQW1CLFVBQVMzVixFQUFULEVBQVlrVyxLQUFaLEVBQWtCO0FBQUMsa0JBQUl4YSxFQUFFLEdBQUM3QixVQUFVLENBQUNtYyxHQUFHLENBQUNFLEtBQUssQ0FBQyxDQUFELENBQU4sQ0FBSixDQUFWLElBQTJCRixHQUFHLENBQUNFLEtBQUssQ0FBQyxDQUFELENBQU4sQ0FBckM7QUFBZ0RKLHNCQUFRLENBQUNJLEtBQUssQ0FBQyxDQUFELENBQU4sQ0FBUixDQUFtQixZQUFVO0FBQUMsb0JBQUlDLFFBQVEsR0FBQ3phLEVBQUUsSUFBRUEsRUFBRSxDQUFDdkMsS0FBSCxDQUFTLElBQVQsRUFBY3lELFNBQWQsQ0FBakI7O0FBQTBDLG9CQUFHdVosUUFBUSxJQUFFdGMsVUFBVSxDQUFDc2MsUUFBUSxDQUFDYixPQUFWLENBQXZCLEVBQTBDO0FBQUNhLDBCQUFRLENBQUNiLE9BQVQsR0FBbUJjLFFBQW5CLENBQTRCSCxRQUFRLENBQUNJLE1BQXJDLEVBQTZDaFYsSUFBN0MsQ0FBa0Q0VSxRQUFRLENBQUNmLE9BQTNELEVBQW9FSyxJQUFwRSxDQUF5RVUsUUFBUSxDQUFDZCxNQUFsRjtBQUEyRixpQkFBdEksTUFBMEk7QUFBQ2MsMEJBQVEsQ0FBQ0MsS0FBSyxDQUFDLENBQUQsQ0FBTCxHQUFTLE1BQVYsQ0FBUixDQUEwQixJQUExQixFQUErQnhhLEVBQUUsR0FBQyxDQUFDeWEsUUFBRCxDQUFELEdBQVl2WixTQUE3QztBQUF5RDtBQUFDLGVBQTdRO0FBQWdSLGFBQXRXO0FBQXdXb1osZUFBRyxHQUFDLElBQUo7QUFBVSxXQUFyWixFQUF1WlYsT0FBdlosRUFBUDtBQUF5YSxTQUF6bUI7QUFBMG1CRSxZQUFJLEVBQUMsY0FBU2MsV0FBVCxFQUFxQkMsVUFBckIsRUFBZ0NDLFVBQWhDLEVBQTJDO0FBQUMsY0FBSUMsUUFBUSxHQUFDLENBQWI7O0FBQWUsbUJBQVN2QixPQUFULENBQWlCd0IsS0FBakIsRUFBdUJaLFFBQXZCLEVBQWdDblAsT0FBaEMsRUFBd0NnUSxPQUF4QyxFQUFnRDtBQUFDLG1CQUFPLFlBQVU7QUFBQyxrQkFBSUMsSUFBSSxHQUFDLElBQVQ7QUFBQSxrQkFBY3hLLElBQUksR0FBQ3hQLFNBQW5CO0FBQUEsa0JBQTZCaWEsVUFBVSxHQUFDLFNBQVhBLFVBQVcsR0FBVTtBQUFDLG9CQUFJVixRQUFKLEVBQWFYLElBQWI7O0FBQWtCLG9CQUFHa0IsS0FBSyxHQUFDRCxRQUFULEVBQWtCO0FBQUM7QUFBUTs7QUFDdHFDTix3QkFBUSxHQUFDeFAsT0FBTyxDQUFDeE4sS0FBUixDQUFjeWQsSUFBZCxFQUFtQnhLLElBQW5CLENBQVQ7O0FBQWtDLG9CQUFHK0osUUFBUSxLQUFHTCxRQUFRLENBQUNSLE9BQVQsRUFBZCxFQUFpQztBQUFDLHdCQUFNLElBQUl3QixTQUFKLENBQWMsMEJBQWQsQ0FBTjtBQUFpRDs7QUFDckh0QixvQkFBSSxHQUFDVyxRQUFRLEtBQUcsUUFBT0EsUUFBUCxNQUFrQixRQUFsQixJQUE0QixPQUFPQSxRQUFQLEtBQWtCLFVBQWpELENBQVIsSUFBc0VBLFFBQVEsQ0FBQ1gsSUFBcEY7O0FBQXlGLG9CQUFHM2IsVUFBVSxDQUFDMmIsSUFBRCxDQUFiLEVBQW9CO0FBQUMsc0JBQUdtQixPQUFILEVBQVc7QUFBQ25CLHdCQUFJLENBQUN2YyxJQUFMLENBQVVrZCxRQUFWLEVBQW1CakIsT0FBTyxDQUFDdUIsUUFBRCxFQUFVWCxRQUFWLEVBQW1CakIsUUFBbkIsRUFBNEI4QixPQUE1QixDQUExQixFQUErRHpCLE9BQU8sQ0FBQ3VCLFFBQUQsRUFBVVgsUUFBVixFQUFtQmYsT0FBbkIsRUFBMkI0QixPQUEzQixDQUF0RTtBQUE0RyxtQkFBeEgsTUFBNEg7QUFBQ0YsNEJBQVE7QUFBR2pCLHdCQUFJLENBQUN2YyxJQUFMLENBQVVrZCxRQUFWLEVBQW1CakIsT0FBTyxDQUFDdUIsUUFBRCxFQUFVWCxRQUFWLEVBQW1CakIsUUFBbkIsRUFBNEI4QixPQUE1QixDQUExQixFQUErRHpCLE9BQU8sQ0FBQ3VCLFFBQUQsRUFBVVgsUUFBVixFQUFtQmYsT0FBbkIsRUFBMkI0QixPQUEzQixDQUF0RSxFQUEwR3pCLE9BQU8sQ0FBQ3VCLFFBQUQsRUFBVVgsUUFBVixFQUFtQmpCLFFBQW5CLEVBQTRCaUIsUUFBUSxDQUFDaUIsVUFBckMsQ0FBakg7QUFBb0s7QUFBQyxpQkFBbFUsTUFBc1U7QUFBQyxzQkFBR3BRLE9BQU8sS0FBR2tPLFFBQWIsRUFBc0I7QUFBQytCLHdCQUFJLEdBQUN4WSxTQUFMO0FBQWVnTyx3QkFBSSxHQUFDLENBQUMrSixRQUFELENBQUw7QUFBaUI7O0FBQ3ZkLG1CQUFDUSxPQUFPLElBQUViLFFBQVEsQ0FBQ2tCLFdBQW5CLEVBQWdDSixJQUFoQyxFQUFxQ3hLLElBQXJDO0FBQTRDO0FBQUMsZUFIeWhDO0FBQUEsa0JBR3hoQzZLLE9BQU8sR0FBQ04sT0FBTyxHQUFDRSxVQUFELEdBQVksWUFBVTtBQUFDLG9CQUFHO0FBQUNBLDRCQUFVO0FBQUksaUJBQWxCLENBQWtCLE9BQU1wUyxDQUFOLEVBQVE7QUFBQyxzQkFBR2xKLE1BQU0sQ0FBQ2thLFFBQVAsQ0FBZ0J5QixhQUFuQixFQUFpQztBQUFDM2IsMEJBQU0sQ0FBQ2thLFFBQVAsQ0FBZ0J5QixhQUFoQixDQUE4QnpTLENBQTlCLEVBQWdDd1MsT0FBTyxDQUFDRSxVQUF4QztBQUFxRDs7QUFDdE0sc0JBQUdULEtBQUssR0FBQyxDQUFOLElBQVNELFFBQVosRUFBcUI7QUFBQyx3QkFBRzlQLE9BQU8sS0FBR29PLE9BQWIsRUFBcUI7QUFBQzZCLDBCQUFJLEdBQUN4WSxTQUFMO0FBQWVnTywwQkFBSSxHQUFDLENBQUMzSCxDQUFELENBQUw7QUFBVTs7QUFDckVxUiw0QkFBUSxDQUFDc0IsVUFBVCxDQUFvQlIsSUFBcEIsRUFBeUJ4SyxJQUF6QjtBQUFnQztBQUFDO0FBQUMsZUFMb2lDOztBQUtuaUMsa0JBQUdzSyxLQUFILEVBQVM7QUFBQ08sdUJBQU87QUFBSSxlQUFyQixNQUF5QjtBQUFDLG9CQUFHMWIsTUFBTSxDQUFDa2EsUUFBUCxDQUFnQjRCLFlBQW5CLEVBQWdDO0FBQUNKLHlCQUFPLENBQUNFLFVBQVIsR0FBbUI1YixNQUFNLENBQUNrYSxRQUFQLENBQWdCNEIsWUFBaEIsRUFBbkI7QUFBbUQ7O0FBQ2pKN2Usc0JBQU0sQ0FBQzhlLFVBQVAsQ0FBa0JMLE9BQWxCO0FBQTRCO0FBQUMsYUFOdWhDO0FBTXJoQzs7QUFDL0IsaUJBQU8xYixNQUFNLENBQUNrYSxRQUFQLENBQWdCLFVBQVNRLFFBQVQsRUFBa0I7QUFBQ04sa0JBQU0sQ0FBQyxDQUFELENBQU4sQ0FBVSxDQUFWLEVBQWE3QyxHQUFiLENBQWlCb0MsT0FBTyxDQUFDLENBQUQsRUFBR2UsUUFBSCxFQUFZcGMsVUFBVSxDQUFDMmMsVUFBRCxDQUFWLEdBQXVCQSxVQUF2QixHQUFrQzNCLFFBQTlDLEVBQXVEb0IsUUFBUSxDQUFDYyxVQUFoRSxDQUF4QjtBQUFxR3BCLGtCQUFNLENBQUMsQ0FBRCxDQUFOLENBQVUsQ0FBVixFQUFhN0MsR0FBYixDQUFpQm9DLE9BQU8sQ0FBQyxDQUFELEVBQUdlLFFBQUgsRUFBWXBjLFVBQVUsQ0FBQ3ljLFdBQUQsQ0FBVixHQUF3QkEsV0FBeEIsR0FBb0N6QixRQUFoRCxDQUF4QjtBQUFtRmMsa0JBQU0sQ0FBQyxDQUFELENBQU4sQ0FBVSxDQUFWLEVBQWE3QyxHQUFiLENBQWlCb0MsT0FBTyxDQUFDLENBQUQsRUFBR2UsUUFBSCxFQUFZcGMsVUFBVSxDQUFDMGMsVUFBRCxDQUFWLEdBQXVCQSxVQUF2QixHQUFrQ3hCLE9BQTlDLENBQXhCO0FBQWlGLFdBQTVTLEVBQThTTyxPQUE5UyxFQUFQO0FBQWdVLFNBUHlCO0FBT3hCQSxlQUFPLEVBQUMsaUJBQVN4YixHQUFULEVBQWE7QUFBQyxpQkFBT0EsR0FBRyxJQUFFLElBQUwsR0FBVXlCLE1BQU0sQ0FBQ2tDLE1BQVAsQ0FBYzNELEdBQWQsRUFBa0J3YixRQUFsQixDQUFWLEdBQXFDQSxRQUE1QztBQUFxRDtBQVBuRCxPQUFsVDtBQUFBLFVBT3VXUSxRQUFRLEdBQUMsRUFQaFg7QUFPbVh2YSxZQUFNLENBQUNpQixJQUFQLENBQVltWixNQUFaLEVBQW1CLFVBQVNqYixDQUFULEVBQVd3YixLQUFYLEVBQWlCO0FBQUMsWUFBSWxVLElBQUksR0FBQ2tVLEtBQUssQ0FBQyxDQUFELENBQWQ7QUFBQSxZQUFrQnFCLFdBQVcsR0FBQ3JCLEtBQUssQ0FBQyxDQUFELENBQW5DO0FBQXVDWixnQkFBTyxDQUFDWSxLQUFLLENBQUMsQ0FBRCxDQUFOLENBQVAsR0FBa0JsVSxJQUFJLENBQUM4USxHQUF2Qjs7QUFBMkIsWUFBR3lFLFdBQUgsRUFBZTtBQUFDdlYsY0FBSSxDQUFDOFEsR0FBTCxDQUFTLFlBQVU7QUFBQzhDLGtCQUFLLEdBQUMyQixXQUFOO0FBQW1CLFdBQXZDLEVBQXdDNUIsTUFBTSxDQUFDLElBQUVqYixDQUFILENBQU4sQ0FBWSxDQUFaLEVBQWVnYSxPQUF2RCxFQUErRGlCLE1BQU0sQ0FBQyxJQUFFamIsQ0FBSCxDQUFOLENBQVksQ0FBWixFQUFlZ2EsT0FBOUUsRUFBc0ZpQixNQUFNLENBQUMsQ0FBRCxDQUFOLENBQVUsQ0FBVixFQUFhaEIsSUFBbkcsRUFBd0dnQixNQUFNLENBQUMsQ0FBRCxDQUFOLENBQVUsQ0FBVixFQUFhaEIsSUFBckg7QUFBNEg7O0FBQzdvQjNTLFlBQUksQ0FBQzhRLEdBQUwsQ0FBU29ELEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBUzdCLElBQWxCOztBQUF3QnlCLGdCQUFRLENBQUNJLEtBQUssQ0FBQyxDQUFELENBQU4sQ0FBUixHQUFtQixZQUFVO0FBQUNKLGtCQUFRLENBQUNJLEtBQUssQ0FBQyxDQUFELENBQUwsR0FBUyxNQUFWLENBQVIsQ0FBMEIsU0FBT0osUUFBUCxHQUFnQjFYLFNBQWhCLEdBQTBCLElBQXBELEVBQXlEeEIsU0FBekQ7QUFBb0UsaUJBQU8sSUFBUDtBQUFhLFNBQS9HOztBQUFnSGtaLGdCQUFRLENBQUNJLEtBQUssQ0FBQyxDQUFELENBQUwsR0FBUyxNQUFWLENBQVIsR0FBMEJsVSxJQUFJLENBQUM0UyxRQUEvQjtBQUF5QyxPQUR5Tzs7QUFDdk9VLGNBQU8sQ0FBQ0EsT0FBUixDQUFnQlEsUUFBaEI7O0FBQTBCLFVBQUdKLElBQUgsRUFBUTtBQUFDQSxZQUFJLENBQUN6YyxJQUFMLENBQVU2YyxRQUFWLEVBQW1CQSxRQUFuQjtBQUE4Qjs7QUFDcFAsYUFBT0EsUUFBUDtBQUFpQixLQVRIO0FBU0kwQixRQUFJLEVBQUMsY0FBU0MsV0FBVCxFQUFxQjtBQUFDLFVBQzdDQyxTQUFTLEdBQUM5YSxTQUFTLENBQUNiLE1BRHlCO0FBQUEsVUFDbEJyQixDQUFDLEdBQUNnZCxTQURnQjtBQUFBLFVBQ05DLGVBQWUsR0FBQ3paLEtBQUssQ0FBQ3hELENBQUQsQ0FEZjtBQUFBLFVBQ21Ca2QsYUFBYSxHQUFDOWUsTUFBSyxDQUFDRyxJQUFOLENBQVcyRCxTQUFYLENBRGpDO0FBQUEsVUFDdURpYixNQUFNLEdBQUN0YyxNQUFNLENBQUNrYSxRQUFQLEVBRDlEO0FBQUEsVUFDZ0ZxQyxVQUFVLEdBQUMsU0FBWEEsVUFBVyxDQUFTcGQsQ0FBVCxFQUFXO0FBQUMsZUFBTyxVQUFTaUYsS0FBVCxFQUFlO0FBQUNnWSx5QkFBZSxDQUFDamQsQ0FBRCxDQUFmLEdBQW1CLElBQW5CO0FBQXdCa2QsdUJBQWEsQ0FBQ2xkLENBQUQsQ0FBYixHQUFpQmtDLFNBQVMsQ0FBQ2IsTUFBVixHQUFpQixDQUFqQixHQUFtQmpELE1BQUssQ0FBQ0csSUFBTixDQUFXMkQsU0FBWCxDQUFuQixHQUF5QytDLEtBQTFEOztBQUFnRSxjQUFHLENBQUUsR0FBRStYLFNBQVAsRUFBa0I7QUFBQ0csa0JBQU0sQ0FBQ2IsV0FBUCxDQUFtQlcsZUFBbkIsRUFBbUNDLGFBQW5DO0FBQW1EO0FBQUMsU0FBdEw7QUFBd0wsT0FEL1I7O0FBQ2dTLFVBQUdGLFNBQVMsSUFBRSxDQUFkLEVBQWdCO0FBQUN6QyxrQkFBVSxDQUFDd0MsV0FBRCxFQUFhSSxNQUFNLENBQUN4VyxJQUFQLENBQVl5VyxVQUFVLENBQUNwZCxDQUFELENBQXRCLEVBQTJCd2EsT0FBeEMsRUFBZ0QyQyxNQUFNLENBQUMxQyxNQUF2RCxFQUE4RCxDQUFDdUMsU0FBL0QsQ0FBVjs7QUFBb0YsWUFBR0csTUFBTSxDQUFDakMsS0FBUCxPQUFpQixTQUFqQixJQUE0Qi9iLFVBQVUsQ0FBQytkLGFBQWEsQ0FBQ2xkLENBQUQsQ0FBYixJQUFrQmtkLGFBQWEsQ0FBQ2xkLENBQUQsQ0FBYixDQUFpQjhhLElBQXBDLENBQXpDLEVBQW1GO0FBQUMsaUJBQU9xQyxNQUFNLENBQUNyQyxJQUFQLEVBQVA7QUFBc0I7QUFBQzs7QUFDN2hCLGFBQU05YSxDQUFDLEVBQVAsRUFBVTtBQUFDdWEsa0JBQVUsQ0FBQzJDLGFBQWEsQ0FBQ2xkLENBQUQsQ0FBZCxFQUFrQm9kLFVBQVUsQ0FBQ3BkLENBQUQsQ0FBNUIsRUFBZ0NtZCxNQUFNLENBQUMxQyxNQUF2QyxDQUFWO0FBQTBEOztBQUNyRSxhQUFPMEMsTUFBTSxDQUFDdkMsT0FBUCxFQUFQO0FBQXlCO0FBWlgsR0FBZDtBQVk0QixNQUFJeUMsV0FBVyxHQUFDLHdEQUFoQjs7QUFBeUV4YyxRQUFNLENBQUNrYSxRQUFQLENBQWdCeUIsYUFBaEIsR0FBOEIsVUFBU3hZLEtBQVQsRUFBZXNaLEtBQWYsRUFBcUI7QUFBQyxRQUFHeGYsTUFBTSxDQUFDeWYsT0FBUCxJQUFnQnpmLE1BQU0sQ0FBQ3lmLE9BQVAsQ0FBZUMsSUFBL0IsSUFBcUN4WixLQUFyQyxJQUE0Q3FaLFdBQVcsQ0FBQ3RTLElBQVosQ0FBaUIvRyxLQUFLLENBQUNmLElBQXZCLENBQS9DLEVBQTRFO0FBQUNuRixZQUFNLENBQUN5ZixPQUFQLENBQWVDLElBQWYsQ0FBb0IsZ0NBQThCeFosS0FBSyxDQUFDeVosT0FBeEQsRUFBZ0V6WixLQUFLLENBQUNzWixLQUF0RSxFQUE0RUEsS0FBNUU7QUFBb0Y7QUFBQyxHQUF0Tjs7QUFBdU56YyxRQUFNLENBQUM2YyxjQUFQLEdBQXNCLFVBQVMxWixLQUFULEVBQWU7QUFBQ2xHLFVBQU0sQ0FBQzhlLFVBQVAsQ0FBa0IsWUFBVTtBQUFDLFlBQU01WSxLQUFOO0FBQWEsS0FBMUM7QUFBNkMsR0FBbkY7O0FBQW9GLE1BQUkyWixTQUFTLEdBQUM5YyxNQUFNLENBQUNrYSxRQUFQLEVBQWQ7O0FBQWdDbGEsUUFBTSxDQUFDRyxFQUFQLENBQVV3VyxLQUFWLEdBQWdCLFVBQVN4VyxFQUFULEVBQVk7QUFBQzJjLGFBQVMsQ0FBQzdDLElBQVYsQ0FBZTlaLEVBQWYsV0FBeUIsVUFBU2dELEtBQVQsRUFBZTtBQUFDbkQsWUFBTSxDQUFDNmMsY0FBUCxDQUFzQjFaLEtBQXRCO0FBQThCLEtBQXZFO0FBQXlFLFdBQU8sSUFBUDtBQUFhLEdBQW5IOztBQUFvSG5ELFFBQU0sQ0FBQ2tDLE1BQVAsQ0FBYztBQUFDZ0IsV0FBTyxFQUFDLEtBQVQ7QUFBZTZaLGFBQVMsRUFBQyxDQUF6QjtBQUEyQnBHLFNBQUssRUFBQyxlQUFTcUcsSUFBVCxFQUFjO0FBQUMsVUFBR0EsSUFBSSxLQUFHLElBQVAsR0FBWSxFQUFFaGQsTUFBTSxDQUFDK2MsU0FBckIsR0FBK0IvYyxNQUFNLENBQUNrRCxPQUF6QyxFQUFpRDtBQUFDO0FBQVE7O0FBQzVwQmxELFlBQU0sQ0FBQ2tELE9BQVAsR0FBZSxJQUFmOztBQUFvQixVQUFHOFosSUFBSSxLQUFHLElBQVAsSUFBYSxFQUFFaGQsTUFBTSxDQUFDK2MsU0FBVCxHQUFtQixDQUFuQyxFQUFxQztBQUFDO0FBQVE7O0FBQ2xFRCxlQUFTLENBQUNyQixXQUFWLENBQXNCM2UsUUFBdEIsRUFBK0IsQ0FBQ2tELE1BQUQsQ0FBL0I7QUFBMEM7QUFGd2dCLEdBQWQ7QUFFdmZBLFFBQU0sQ0FBQzJXLEtBQVAsQ0FBYXNELElBQWIsR0FBa0I2QyxTQUFTLENBQUM3QyxJQUE1Qjs7QUFBaUMsV0FBU2dELFNBQVQsR0FBb0I7QUFBQ25nQixZQUFRLENBQUNvZ0IsbUJBQVQsQ0FBNkIsa0JBQTdCLEVBQWdERCxTQUFoRDtBQUEyRGhnQixVQUFNLENBQUNpZ0IsbUJBQVAsQ0FBMkIsTUFBM0IsRUFBa0NELFNBQWxDO0FBQTZDamQsVUFBTSxDQUFDMlcsS0FBUDtBQUFnQjs7QUFDM04sTUFBRzdaLFFBQVEsQ0FBQ3FnQixVQUFULEtBQXNCLFVBQXRCLElBQW1DcmdCLFFBQVEsQ0FBQ3FnQixVQUFULEtBQXNCLFNBQXRCLElBQWlDLENBQUNyZ0IsUUFBUSxDQUFDc1AsZUFBVCxDQUF5QmdSLFFBQWpHLEVBQTJHO0FBQUNuZ0IsVUFBTSxDQUFDOGUsVUFBUCxDQUFrQi9iLE1BQU0sQ0FBQzJXLEtBQXpCO0FBQWlDLEdBQTdJLE1BQWlKO0FBQUM3WixZQUFRLENBQUMyUCxnQkFBVCxDQUEwQixrQkFBMUIsRUFBNkN3USxTQUE3QztBQUF3RGhnQixVQUFNLENBQUN3UCxnQkFBUCxDQUF3QixNQUF4QixFQUErQndRLFNBQS9CO0FBQTJDOztBQUNyUCxNQUFJSSxNQUFNLEdBQUMsU0FBUEEsTUFBTyxDQUFTeGMsS0FBVCxFQUFlVixFQUFmLEVBQWtCeUssR0FBbEIsRUFBc0J4RyxLQUF0QixFQUE0QmtaLFNBQTVCLEVBQXNDQyxRQUF0QyxFQUErQ0MsR0FBL0MsRUFBbUQ7QUFBQyxRQUFJcmUsQ0FBQyxHQUFDLENBQU47QUFBQSxRQUFRMEMsR0FBRyxHQUFDaEIsS0FBSyxDQUFDTCxNQUFsQjtBQUFBLFFBQXlCaWQsSUFBSSxHQUFDN1MsR0FBRyxJQUFFLElBQW5DOztBQUF3QyxRQUFHOUssTUFBTSxDQUFDOEssR0FBRCxDQUFOLEtBQWMsUUFBakIsRUFBMEI7QUFBQzBTLGVBQVMsR0FBQyxJQUFWOztBQUFlLFdBQUluZSxDQUFKLElBQVN5TCxHQUFULEVBQWE7QUFBQ3lTLGNBQU0sQ0FBQ3hjLEtBQUQsRUFBT1YsRUFBUCxFQUFVaEIsQ0FBVixFQUFZeUwsR0FBRyxDQUFDekwsQ0FBRCxDQUFmLEVBQW1CLElBQW5CLEVBQXdCb2UsUUFBeEIsRUFBaUNDLEdBQWpDLENBQU47QUFBNkM7QUFBQyxLQUF0RyxNQUEyRyxJQUFHcFosS0FBSyxLQUFHdkIsU0FBWCxFQUFxQjtBQUFDeWEsZUFBUyxHQUFDLElBQVY7O0FBQWUsVUFBRyxDQUFDaGYsVUFBVSxDQUFDOEYsS0FBRCxDQUFkLEVBQXNCO0FBQUNvWixXQUFHLEdBQUMsSUFBSjtBQUFVOztBQUN4UixVQUFHQyxJQUFILEVBQVE7QUFBQyxZQUFHRCxHQUFILEVBQU87QUFBQ3JkLFlBQUUsQ0FBQ3pDLElBQUgsQ0FBUW1ELEtBQVIsRUFBY3VELEtBQWQ7QUFBcUJqRSxZQUFFLEdBQUMsSUFBSDtBQUFTLFNBQXRDLE1BQTBDO0FBQUNzZCxjQUFJLEdBQUN0ZCxFQUFMOztBQUFRQSxZQUFFLEdBQUMsWUFBU2lCLElBQVQsRUFBY3NjLElBQWQsRUFBbUJ0WixLQUFuQixFQUF5QjtBQUFDLG1CQUFPcVosSUFBSSxDQUFDL2YsSUFBTCxDQUFVc0MsTUFBTSxDQUFDb0IsSUFBRCxDQUFoQixFQUF1QmdELEtBQXZCLENBQVA7QUFBc0MsV0FBbkU7QUFBcUU7QUFBQzs7QUFDbEksVUFBR2pFLEVBQUgsRUFBTTtBQUFDLGVBQUtoQixDQUFDLEdBQUMwQyxHQUFQLEVBQVcxQyxDQUFDLEVBQVosRUFBZTtBQUFDZ0IsWUFBRSxDQUFDVSxLQUFLLENBQUMxQixDQUFELENBQU4sRUFBVXlMLEdBQVYsRUFBYzRTLEdBQUcsR0FBQ3BaLEtBQUQsR0FBT0EsS0FBSyxDQUFDMUcsSUFBTixDQUFXbUQsS0FBSyxDQUFDMUIsQ0FBRCxDQUFoQixFQUFvQkEsQ0FBcEIsRUFBc0JnQixFQUFFLENBQUNVLEtBQUssQ0FBQzFCLENBQUQsQ0FBTixFQUFVeUwsR0FBVixDQUF4QixDQUF4QixDQUFGO0FBQW9FO0FBQUM7QUFBQzs7QUFDN0YsUUFBRzBTLFNBQUgsRUFBYTtBQUFDLGFBQU96YyxLQUFQO0FBQWM7O0FBQzVCLFFBQUc0YyxJQUFILEVBQVE7QUFBQyxhQUFPdGQsRUFBRSxDQUFDekMsSUFBSCxDQUFRbUQsS0FBUixDQUFQO0FBQXVCOztBQUNoQyxXQUFPZ0IsR0FBRyxHQUFDMUIsRUFBRSxDQUFDVSxLQUFLLENBQUMsQ0FBRCxDQUFOLEVBQVUrSixHQUFWLENBQUgsR0FBa0IyUyxRQUE1QjtBQUFzQyxHQUx0Qzs7QUFLdUMsTUFBSUksU0FBUyxHQUFDLE9BQWQ7QUFBQSxNQUFzQkMsVUFBVSxHQUFDLFdBQWpDOztBQUE2QyxXQUFTQyxVQUFULENBQW9CQyxJQUFwQixFQUF5QkMsTUFBekIsRUFBZ0M7QUFBQyxXQUFPQSxNQUFNLENBQUNDLFdBQVAsRUFBUDtBQUE2Qjs7QUFDbEosV0FBU0MsU0FBVCxDQUFtQkMsTUFBbkIsRUFBMEI7QUFBQyxXQUFPQSxNQUFNLENBQUNqYixPQUFQLENBQWUwYSxTQUFmLEVBQXlCLEtBQXpCLEVBQWdDMWEsT0FBaEMsQ0FBd0MyYSxVQUF4QyxFQUFtREMsVUFBbkQsQ0FBUDtBQUF1RTs7QUFDbEcsTUFBSU0sVUFBVSxHQUFDLFNBQVhBLFVBQVcsQ0FBU0MsS0FBVCxFQUFlO0FBQUMsV0FBT0EsS0FBSyxDQUFDNWYsUUFBTixLQUFpQixDQUFqQixJQUFvQjRmLEtBQUssQ0FBQzVmLFFBQU4sS0FBaUIsQ0FBckMsSUFBd0MsQ0FBRSxDQUFDNGYsS0FBSyxDQUFDNWYsUUFBeEQ7QUFBbUUsR0FBbEc7O0FBQW1HLFdBQVM2ZixJQUFULEdBQWU7QUFBQyxTQUFLdmIsT0FBTCxHQUFhOUMsTUFBTSxDQUFDOEMsT0FBUCxHQUFldWIsSUFBSSxDQUFDQyxHQUFMLEVBQTVCO0FBQXdDOztBQUMzSkQsTUFBSSxDQUFDQyxHQUFMLEdBQVMsQ0FBVDtBQUFXRCxNQUFJLENBQUNoZSxTQUFMLEdBQWU7QUFBQ3NLLFNBQUssRUFBQyxlQUFTeVQsS0FBVCxFQUFlO0FBQUMsVUFBSWhhLEtBQUssR0FBQ2dhLEtBQUssQ0FBQyxLQUFLdGIsT0FBTixDQUFmOztBQUE4QixVQUFHLENBQUNzQixLQUFKLEVBQVU7QUFBQ0EsYUFBSyxHQUFDLEVBQU47O0FBQVMsWUFBRytaLFVBQVUsQ0FBQ0MsS0FBRCxDQUFiLEVBQXFCO0FBQUMsY0FBR0EsS0FBSyxDQUFDNWYsUUFBVCxFQUFrQjtBQUFDNGYsaUJBQUssQ0FBQyxLQUFLdGIsT0FBTixDQUFMLEdBQW9Cc0IsS0FBcEI7QUFBMkIsV0FBOUMsTUFBa0Q7QUFBQy9HLGtCQUFNLENBQUNraEIsY0FBUCxDQUFzQkgsS0FBdEIsRUFBNEIsS0FBS3RiLE9BQWpDLEVBQXlDO0FBQUNzQixtQkFBSyxFQUFDQSxLQUFQO0FBQWFvYSwwQkFBWSxFQUFDO0FBQTFCLGFBQXpDO0FBQTJFO0FBQUM7QUFBQzs7QUFDelAsYUFBT3BhLEtBQVA7QUFBYyxLQURZO0FBQ1hxYSxPQUFHLEVBQUMsYUFBU0wsS0FBVCxFQUFlTSxJQUFmLEVBQW9CdGEsS0FBcEIsRUFBMEI7QUFBQyxVQUFJdWEsSUFBSjtBQUFBLFVBQVNoVSxLQUFLLEdBQUMsS0FBS0EsS0FBTCxDQUFXeVQsS0FBWCxDQUFmOztBQUFpQyxVQUFHLE9BQU9NLElBQVAsS0FBYyxRQUFqQixFQUEwQjtBQUFDL1QsYUFBSyxDQUFDc1QsU0FBUyxDQUFDUyxJQUFELENBQVYsQ0FBTCxHQUF1QnRhLEtBQXZCO0FBQThCLE9BQXpELE1BQTZEO0FBQUMsYUFBSXVhLElBQUosSUFBWUQsSUFBWixFQUFpQjtBQUFDL1QsZUFBSyxDQUFDc1QsU0FBUyxDQUFDVSxJQUFELENBQVYsQ0FBTCxHQUF1QkQsSUFBSSxDQUFDQyxJQUFELENBQTNCO0FBQW1DO0FBQUM7O0FBQ25NLGFBQU9oVSxLQUFQO0FBQWMsS0FGWTtBQUVYakssT0FBRyxFQUFDLGFBQVMwZCxLQUFULEVBQWV4VCxHQUFmLEVBQW1CO0FBQUMsYUFBT0EsR0FBRyxLQUFHL0gsU0FBTixHQUFnQixLQUFLOEgsS0FBTCxDQUFXeVQsS0FBWCxDQUFoQixHQUFrQ0EsS0FBSyxDQUFDLEtBQUt0YixPQUFOLENBQUwsSUFBcUJzYixLQUFLLENBQUMsS0FBS3RiLE9BQU4sQ0FBTCxDQUFvQm1iLFNBQVMsQ0FBQ3JULEdBQUQsQ0FBN0IsQ0FBOUQ7QUFBbUcsS0FGaEg7QUFFaUh5UyxVQUFNLEVBQUMsZ0JBQVNlLEtBQVQsRUFBZXhULEdBQWYsRUFBbUJ4RyxLQUFuQixFQUF5QjtBQUFDLFVBQUd3RyxHQUFHLEtBQUcvSCxTQUFOLElBQW1CK0gsR0FBRyxJQUFFLE9BQU9BLEdBQVAsS0FBYSxRQUFuQixJQUE4QnhHLEtBQUssS0FBR3ZCLFNBQTNELEVBQXNFO0FBQUMsZUFBTyxLQUFLbkMsR0FBTCxDQUFTMGQsS0FBVCxFQUFleFQsR0FBZixDQUFQO0FBQTRCOztBQUMvUSxXQUFLNlQsR0FBTCxDQUFTTCxLQUFULEVBQWV4VCxHQUFmLEVBQW1CeEcsS0FBbkI7QUFBMEIsYUFBT0EsS0FBSyxLQUFHdkIsU0FBUixHQUFrQnVCLEtBQWxCLEdBQXdCd0csR0FBL0I7QUFBb0MsS0FIcEM7QUFHcUNxTyxVQUFNLEVBQUMsZ0JBQVNtRixLQUFULEVBQWV4VCxHQUFmLEVBQW1CO0FBQUMsVUFBSXpMLENBQUo7QUFBQSxVQUFNd0wsS0FBSyxHQUFDeVQsS0FBSyxDQUFDLEtBQUt0YixPQUFOLENBQWpCOztBQUFnQyxVQUFHNkgsS0FBSyxLQUFHOUgsU0FBWCxFQUFxQjtBQUFDO0FBQVE7O0FBQ3hKLFVBQUcrSCxHQUFHLEtBQUcvSCxTQUFULEVBQW1CO0FBQUMsWUFBR0YsS0FBSyxDQUFDQyxPQUFOLENBQWNnSSxHQUFkLENBQUgsRUFBc0I7QUFBQ0EsYUFBRyxHQUFDQSxHQUFHLENBQUN6SixHQUFKLENBQVE4YyxTQUFSLENBQUo7QUFBd0IsU0FBL0MsTUFBbUQ7QUFBQ3JULGFBQUcsR0FBQ3FULFNBQVMsQ0FBQ3JULEdBQUQsQ0FBYjtBQUFtQkEsYUFBRyxHQUFDQSxHQUFHLElBQUlELEtBQVAsR0FBYSxDQUFDQyxHQUFELENBQWIsR0FBb0JBLEdBQUcsQ0FBQ3JCLEtBQUosQ0FBVTJPLGFBQVYsS0FBMEIsRUFBbEQ7QUFBdUQ7O0FBQ2xKL1ksU0FBQyxHQUFDeUwsR0FBRyxDQUFDcEssTUFBTjs7QUFBYSxlQUFNckIsQ0FBQyxFQUFQLEVBQVU7QUFBQyxpQkFBT3dMLEtBQUssQ0FBQ0MsR0FBRyxDQUFDekwsQ0FBRCxDQUFKLENBQVo7QUFBc0I7QUFBQzs7QUFDL0MsVUFBR3lMLEdBQUcsS0FBRy9ILFNBQU4sSUFBaUI3QyxNQUFNLENBQUN3RCxhQUFQLENBQXFCbUgsS0FBckIsQ0FBcEIsRUFBZ0Q7QUFBQyxZQUFHeVQsS0FBSyxDQUFDNWYsUUFBVCxFQUFrQjtBQUFDNGYsZUFBSyxDQUFDLEtBQUt0YixPQUFOLENBQUwsR0FBb0JELFNBQXBCO0FBQStCLFNBQWxELE1BQXNEO0FBQUMsaUJBQU91YixLQUFLLENBQUMsS0FBS3RiLE9BQU4sQ0FBWjtBQUE0QjtBQUFDO0FBQUMsS0FONUc7QUFNNkc4YixXQUFPLEVBQUMsaUJBQVNSLEtBQVQsRUFBZTtBQUFDLFVBQUl6VCxLQUFLLEdBQUN5VCxLQUFLLENBQUMsS0FBS3RiLE9BQU4sQ0FBZjtBQUE4QixhQUFPNkgsS0FBSyxLQUFHOUgsU0FBUixJQUFtQixDQUFDN0MsTUFBTSxDQUFDd0QsYUFBUCxDQUFxQm1ILEtBQXJCLENBQTNCO0FBQXdEO0FBTjNOLEdBQWY7QUFNNE8sTUFBSWtVLFFBQVEsR0FBQyxJQUFJUixJQUFKLEVBQWI7QUFBd0IsTUFBSVMsUUFBUSxHQUFDLElBQUlULElBQUosRUFBYjtBQUF3QixNQUFJVSxNQUFNLEdBQUMsK0JBQVg7QUFBQSxNQUEyQ0MsVUFBVSxHQUFDLFFBQXREOztBQUErRCxXQUFTQyxPQUFULENBQWlCUCxJQUFqQixFQUFzQjtBQUFDLFFBQUdBLElBQUksS0FBRyxNQUFWLEVBQWlCO0FBQUMsYUFBTyxJQUFQO0FBQWE7O0FBQzVaLFFBQUdBLElBQUksS0FBRyxPQUFWLEVBQWtCO0FBQUMsYUFBTyxLQUFQO0FBQWM7O0FBQ2pDLFFBQUdBLElBQUksS0FBRyxNQUFWLEVBQWlCO0FBQUMsYUFBTyxJQUFQO0FBQWE7O0FBQy9CLFFBQUdBLElBQUksS0FBRyxDQUFDQSxJQUFELEdBQU0sRUFBaEIsRUFBbUI7QUFBQyxhQUFNLENBQUNBLElBQVA7QUFBYTs7QUFDakMsUUFBR0ssTUFBTSxDQUFDN1UsSUFBUCxDQUFZd1UsSUFBWixDQUFILEVBQXFCO0FBQUMsYUFBT1EsSUFBSSxDQUFDQyxLQUFMLENBQVdULElBQVgsQ0FBUDtBQUF5Qjs7QUFDL0MsV0FBT0EsSUFBUDtBQUFhOztBQUNiLFdBQVNVLFFBQVQsQ0FBa0JoZSxJQUFsQixFQUF1QndKLEdBQXZCLEVBQTJCOFQsSUFBM0IsRUFBZ0M7QUFBQyxRQUFJdGMsSUFBSjs7QUFBUyxRQUFHc2MsSUFBSSxLQUFHN2IsU0FBUCxJQUFrQnpCLElBQUksQ0FBQzVDLFFBQUwsS0FBZ0IsQ0FBckMsRUFBdUM7QUFBQzRELFVBQUksR0FBQyxVQUFRd0ksR0FBRyxDQUFDM0gsT0FBSixDQUFZK2IsVUFBWixFQUF1QixLQUF2QixFQUE4QnRhLFdBQTlCLEVBQWI7QUFBeURnYSxVQUFJLEdBQUN0ZCxJQUFJLENBQUM1QixZQUFMLENBQWtCNEMsSUFBbEIsQ0FBTDs7QUFBNkIsVUFBRyxPQUFPc2MsSUFBUCxLQUFjLFFBQWpCLEVBQTBCO0FBQUMsWUFBRztBQUFDQSxjQUFJLEdBQUNPLE9BQU8sQ0FBQ1AsSUFBRCxDQUFaO0FBQW9CLFNBQXhCLENBQXdCLE9BQU14VixDQUFOLEVBQVEsQ0FBRTs7QUFDck80VixnQkFBUSxDQUFDTCxHQUFULENBQWFyZCxJQUFiLEVBQWtCd0osR0FBbEIsRUFBc0I4VCxJQUF0QjtBQUE2QixPQUQySSxNQUN2STtBQUFDQSxZQUFJLEdBQUM3YixTQUFMO0FBQWdCO0FBQUM7O0FBQ25ELFdBQU82YixJQUFQO0FBQWE7O0FBQ2IxZSxRQUFNLENBQUNrQyxNQUFQLENBQWM7QUFBQzBjLFdBQU8sRUFBQyxpQkFBU3hkLElBQVQsRUFBYztBQUFDLGFBQU8wZCxRQUFRLENBQUNGLE9BQVQsQ0FBaUJ4ZCxJQUFqQixLQUF3QnlkLFFBQVEsQ0FBQ0QsT0FBVCxDQUFpQnhkLElBQWpCLENBQS9CO0FBQXVELEtBQS9FO0FBQWdGc2QsUUFBSSxFQUFDLGNBQVN0ZCxJQUFULEVBQWNnQixJQUFkLEVBQW1Cc2MsS0FBbkIsRUFBd0I7QUFBQyxhQUFPSSxRQUFRLENBQUN6QixNQUFULENBQWdCamMsSUFBaEIsRUFBcUJnQixJQUFyQixFQUEwQnNjLEtBQTFCLENBQVA7QUFBd0MsS0FBdEo7QUFBdUpXLGNBQVUsRUFBQyxvQkFBU2plLElBQVQsRUFBY2dCLElBQWQsRUFBbUI7QUFBQzBjLGNBQVEsQ0FBQzdGLE1BQVQsQ0FBZ0I3WCxJQUFoQixFQUFxQmdCLElBQXJCO0FBQTRCLEtBQWxOO0FBQW1Oa2QsU0FBSyxFQUFDLGVBQVNsZSxJQUFULEVBQWNnQixJQUFkLEVBQW1Cc2MsSUFBbkIsRUFBd0I7QUFBQyxhQUFPRyxRQUFRLENBQUN4QixNQUFULENBQWdCamMsSUFBaEIsRUFBcUJnQixJQUFyQixFQUEwQnNjLElBQTFCLENBQVA7QUFBd0MsS0FBMVI7QUFBMlJhLGVBQVcsRUFBQyxxQkFBU25lLElBQVQsRUFBY2dCLElBQWQsRUFBbUI7QUFBQ3ljLGNBQVEsQ0FBQzVGLE1BQVQsQ0FBZ0I3WCxJQUFoQixFQUFxQmdCLElBQXJCO0FBQTRCO0FBQXZWLEdBQWQ7QUFBd1dwQyxRQUFNLENBQUNHLEVBQVAsQ0FBVStCLE1BQVYsQ0FBaUI7QUFBQ3djLFFBQUksRUFBQyxjQUFTOVQsR0FBVCxFQUFheEcsS0FBYixFQUFtQjtBQUFDLFVBQUlqRixDQUFKO0FBQUEsVUFBTWlELElBQU47QUFBQSxVQUFXc2MsSUFBWDtBQUFBLFVBQWdCdGQsSUFBSSxHQUFDLEtBQUssQ0FBTCxDQUFyQjtBQUFBLFVBQTZCK0osS0FBSyxHQUFDL0osSUFBSSxJQUFFQSxJQUFJLENBQUN5RixVQUE5Qzs7QUFBeUQsVUFBRytELEdBQUcsS0FBRy9ILFNBQVQsRUFBbUI7QUFBQyxZQUFHLEtBQUtyQyxNQUFSLEVBQWU7QUFBQ2tlLGNBQUksR0FBQ0ksUUFBUSxDQUFDcGUsR0FBVCxDQUFhVSxJQUFiLENBQUw7O0FBQXdCLGNBQUdBLElBQUksQ0FBQzVDLFFBQUwsS0FBZ0IsQ0FBaEIsSUFBbUIsQ0FBQ3FnQixRQUFRLENBQUNuZSxHQUFULENBQWFVLElBQWIsRUFBa0IsY0FBbEIsQ0FBdkIsRUFBeUQ7QUFBQ2pDLGFBQUMsR0FBQ2dNLEtBQUssQ0FBQzNLLE1BQVI7O0FBQWUsbUJBQU1yQixDQUFDLEVBQVAsRUFBVTtBQUFDLGtCQUFHZ00sS0FBSyxDQUFDaE0sQ0FBRCxDQUFSLEVBQVk7QUFBQ2lELG9CQUFJLEdBQUMrSSxLQUFLLENBQUNoTSxDQUFELENBQUwsQ0FBU2lELElBQWQ7O0FBQW1CLG9CQUFHQSxJQUFJLENBQUN0RSxPQUFMLENBQWEsT0FBYixNQUF3QixDQUEzQixFQUE2QjtBQUFDc0Usc0JBQUksR0FBQzZiLFNBQVMsQ0FBQzdiLElBQUksQ0FBQzdFLEtBQUwsQ0FBVyxDQUFYLENBQUQsQ0FBZDtBQUE4QjZoQiwwQkFBUSxDQUFDaGUsSUFBRCxFQUFNZ0IsSUFBTixFQUFXc2MsSUFBSSxDQUFDdGMsSUFBRCxDQUFmLENBQVI7QUFBZ0M7QUFBQztBQUFDOztBQUMxdEJ5YyxvQkFBUSxDQUFDSixHQUFULENBQWFyZCxJQUFiLEVBQWtCLGNBQWxCLEVBQWlDLElBQWpDO0FBQXdDO0FBQUM7O0FBQ3pDLGVBQU9zZCxJQUFQO0FBQWE7O0FBQ2IsVUFBRyxRQUFPOVQsR0FBUCxNQUFhLFFBQWhCLEVBQXlCO0FBQUMsZUFBTyxLQUFLM0osSUFBTCxDQUFVLFlBQVU7QUFBQzZkLGtCQUFRLENBQUNMLEdBQVQsQ0FBYSxJQUFiLEVBQWtCN1QsR0FBbEI7QUFBd0IsU0FBN0MsQ0FBUDtBQUF1RDs7QUFDakYsYUFBT3lTLE1BQU0sQ0FBQyxJQUFELEVBQU0sVUFBU2paLEtBQVQsRUFBZTtBQUFDLFlBQUlzYSxJQUFKOztBQUFTLFlBQUd0ZCxJQUFJLElBQUVnRCxLQUFLLEtBQUd2QixTQUFqQixFQUEyQjtBQUFDNmIsY0FBSSxHQUFDSSxRQUFRLENBQUNwZSxHQUFULENBQWFVLElBQWIsRUFBa0J3SixHQUFsQixDQUFMOztBQUE0QixjQUFHOFQsSUFBSSxLQUFHN2IsU0FBVixFQUFvQjtBQUFDLG1CQUFPNmIsSUFBUDtBQUFhOztBQUN0SUEsY0FBSSxHQUFDVSxRQUFRLENBQUNoZSxJQUFELEVBQU13SixHQUFOLENBQWI7O0FBQXdCLGNBQUc4VCxJQUFJLEtBQUc3YixTQUFWLEVBQW9CO0FBQUMsbUJBQU82YixJQUFQO0FBQWE7O0FBQzFEO0FBQVE7O0FBQ1IsYUFBS3pkLElBQUwsQ0FBVSxZQUFVO0FBQUM2ZCxrQkFBUSxDQUFDTCxHQUFULENBQWEsSUFBYixFQUFrQjdULEdBQWxCLEVBQXNCeEcsS0FBdEI7QUFBOEIsU0FBbkQ7QUFBc0QsT0FIekMsRUFHMEMsSUFIMUMsRUFHK0NBLEtBSC9DLEVBR3FEL0MsU0FBUyxDQUFDYixNQUFWLEdBQWlCLENBSHRFLEVBR3dFLElBSHhFLEVBRzZFLElBSDdFLENBQWI7QUFHaUcsS0FQd1I7QUFPdlI2ZSxjQUFVLEVBQUMsb0JBQVN6VSxHQUFULEVBQWE7QUFBQyxhQUFPLEtBQUszSixJQUFMLENBQVUsWUFBVTtBQUFDNmQsZ0JBQVEsQ0FBQzdGLE1BQVQsQ0FBZ0IsSUFBaEIsRUFBcUJyTyxHQUFyQjtBQUEyQixPQUFoRCxDQUFQO0FBQTBEO0FBUG9NLEdBQWpCO0FBT2hMNUssUUFBTSxDQUFDa0MsTUFBUCxDQUFjO0FBQUMwVyxTQUFLLEVBQUMsZUFBU3hYLElBQVQsRUFBY3pDLElBQWQsRUFBbUIrZixJQUFuQixFQUF3QjtBQUFDLFVBQUk5RixLQUFKOztBQUFVLFVBQUd4WCxJQUFILEVBQVE7QUFBQ3pDLFlBQUksR0FBQyxDQUFDQSxJQUFJLElBQUUsSUFBUCxJQUFhLE9BQWxCO0FBQTBCaWEsYUFBSyxHQUFDaUcsUUFBUSxDQUFDbmUsR0FBVCxDQUFhVSxJQUFiLEVBQWtCekMsSUFBbEIsQ0FBTjs7QUFBOEIsWUFBRytmLElBQUgsRUFBUTtBQUFDLGNBQUcsQ0FBQzlGLEtBQUQsSUFBUWpXLEtBQUssQ0FBQ0MsT0FBTixDQUFjOGIsSUFBZCxDQUFYLEVBQStCO0FBQUM5RixpQkFBSyxHQUFDaUcsUUFBUSxDQUFDeEIsTUFBVCxDQUFnQmpjLElBQWhCLEVBQXFCekMsSUFBckIsRUFBMEJxQixNQUFNLENBQUMyRCxTQUFQLENBQWlCK2EsSUFBakIsQ0FBMUIsQ0FBTjtBQUF5RCxXQUF6RixNQUE2RjtBQUFDOUYsaUJBQUssQ0FBQy9hLElBQU4sQ0FBVzZnQixJQUFYO0FBQWtCO0FBQUM7O0FBQzNhLGVBQU85RixLQUFLLElBQUUsRUFBZDtBQUFrQjtBQUFDLEtBRG1MO0FBQ2xMNEcsV0FBTyxFQUFDLGlCQUFTcGUsSUFBVCxFQUFjekMsSUFBZCxFQUFtQjtBQUFDQSxVQUFJLEdBQUNBLElBQUksSUFBRSxJQUFYOztBQUFnQixVQUFJaWEsS0FBSyxHQUFDNVksTUFBTSxDQUFDNFksS0FBUCxDQUFheFgsSUFBYixFQUFrQnpDLElBQWxCLENBQVY7QUFBQSxVQUFrQzhnQixXQUFXLEdBQUM3RyxLQUFLLENBQUNwWSxNQUFwRDtBQUFBLFVBQTJETCxFQUFFLEdBQUN5WSxLQUFLLENBQUM5TixLQUFOLEVBQTlEO0FBQUEsVUFBNEU0VSxLQUFLLEdBQUMxZixNQUFNLENBQUMyZixXQUFQLENBQW1CdmUsSUFBbkIsRUFBd0J6QyxJQUF4QixDQUFsRjtBQUFBLFVBQWdIcUssSUFBSSxHQUFDLFNBQUxBLElBQUssR0FBVTtBQUFDaEosY0FBTSxDQUFDd2YsT0FBUCxDQUFlcGUsSUFBZixFQUFvQnpDLElBQXBCO0FBQTJCLE9BQTNKOztBQUE0SixVQUFHd0IsRUFBRSxLQUFHLFlBQVIsRUFBcUI7QUFBQ0EsVUFBRSxHQUFDeVksS0FBSyxDQUFDOU4sS0FBTixFQUFIO0FBQWlCMlUsbUJBQVc7QUFBSTs7QUFDbFIsVUFBR3RmLEVBQUgsRUFBTTtBQUFDLFlBQUd4QixJQUFJLEtBQUcsSUFBVixFQUFlO0FBQUNpYSxlQUFLLENBQUN2SyxPQUFOLENBQWMsWUFBZDtBQUE2Qjs7QUFDcEQsZUFBT3FSLEtBQUssQ0FBQ0UsSUFBYjtBQUFrQnpmLFVBQUUsQ0FBQ3pDLElBQUgsQ0FBUTBELElBQVIsRUFBYTRILElBQWIsRUFBa0IwVyxLQUFsQjtBQUEwQjs7QUFDNUMsVUFBRyxDQUFDRCxXQUFELElBQWNDLEtBQWpCLEVBQXVCO0FBQUNBLGFBQUssQ0FBQ3hHLEtBQU4sQ0FBWUosSUFBWjtBQUFvQjtBQUFDLEtBSnlKO0FBSXhKNkcsZUFBVyxFQUFDLHFCQUFTdmUsSUFBVCxFQUFjekMsSUFBZCxFQUFtQjtBQUFDLFVBQUlpTSxHQUFHLEdBQUNqTSxJQUFJLEdBQUMsWUFBYjtBQUEwQixhQUFPa2dCLFFBQVEsQ0FBQ25lLEdBQVQsQ0FBYVUsSUFBYixFQUFrQndKLEdBQWxCLEtBQXdCaVUsUUFBUSxDQUFDeEIsTUFBVCxDQUFnQmpjLElBQWhCLEVBQXFCd0osR0FBckIsRUFBeUI7QUFBQ3NPLGFBQUssRUFBQ2xaLE1BQU0sQ0FBQ3VZLFNBQVAsQ0FBaUIsYUFBakIsRUFBZ0NoQixHQUFoQyxDQUFvQyxZQUFVO0FBQUNzSCxrQkFBUSxDQUFDNUYsTUFBVCxDQUFnQjdYLElBQWhCLEVBQXFCLENBQUN6QyxJQUFJLEdBQUMsT0FBTixFQUFjaU0sR0FBZCxDQUFyQjtBQUEwQyxTQUF6RjtBQUFQLE9BQXpCLENBQS9CO0FBQTZKO0FBSi9ELEdBQWQ7QUFJZ0Y1SyxRQUFNLENBQUNHLEVBQVAsQ0FBVStCLE1BQVYsQ0FBaUI7QUFBQzBXLFNBQUssRUFBQyxlQUFTamEsSUFBVCxFQUFjK2YsSUFBZCxFQUFtQjtBQUFDLFVBQUltQixNQUFNLEdBQUMsQ0FBWDs7QUFBYSxVQUFHLE9BQU9saEIsSUFBUCxLQUFjLFFBQWpCLEVBQTBCO0FBQUMrZixZQUFJLEdBQUMvZixJQUFMO0FBQVVBLFlBQUksR0FBQyxJQUFMO0FBQVVraEIsY0FBTTtBQUFJOztBQUMxWCxVQUFHeGUsU0FBUyxDQUFDYixNQUFWLEdBQWlCcWYsTUFBcEIsRUFBMkI7QUFBQyxlQUFPN2YsTUFBTSxDQUFDNFksS0FBUCxDQUFhLEtBQUssQ0FBTCxDQUFiLEVBQXFCamEsSUFBckIsQ0FBUDtBQUFtQzs7QUFDL0QsYUFBTytmLElBQUksS0FBRzdiLFNBQVAsR0FBaUIsSUFBakIsR0FBc0IsS0FBSzVCLElBQUwsQ0FBVSxZQUFVO0FBQUMsWUFBSTJYLEtBQUssR0FBQzVZLE1BQU0sQ0FBQzRZLEtBQVAsQ0FBYSxJQUFiLEVBQWtCamEsSUFBbEIsRUFBdUIrZixJQUF2QixDQUFWOztBQUF1QzFlLGNBQU0sQ0FBQzJmLFdBQVAsQ0FBbUIsSUFBbkIsRUFBd0JoaEIsSUFBeEI7O0FBQThCLFlBQUdBLElBQUksS0FBRyxJQUFQLElBQWFpYSxLQUFLLENBQUMsQ0FBRCxDQUFMLEtBQVcsWUFBM0IsRUFBd0M7QUFBQzVZLGdCQUFNLENBQUN3ZixPQUFQLENBQWUsSUFBZixFQUFvQjdnQixJQUFwQjtBQUEyQjtBQUFDLE9BQS9KLENBQTdCO0FBQStMLEtBRjBGO0FBRXpGNmdCLFdBQU8sRUFBQyxpQkFBUzdnQixJQUFULEVBQWM7QUFBQyxhQUFPLEtBQUtzQyxJQUFMLENBQVUsWUFBVTtBQUFDakIsY0FBTSxDQUFDd2YsT0FBUCxDQUFlLElBQWYsRUFBb0I3Z0IsSUFBcEI7QUFBMkIsT0FBaEQsQ0FBUDtBQUEwRCxLQUZRO0FBRVBtaEIsY0FBVSxFQUFDLG9CQUFTbmhCLElBQVQsRUFBYztBQUFDLGFBQU8sS0FBS2lhLEtBQUwsQ0FBV2phLElBQUksSUFBRSxJQUFqQixFQUFzQixFQUF0QixDQUFQO0FBQWtDLEtBRnJEO0FBRXNEb2IsV0FBTyxFQUFDLGlCQUFTcGIsSUFBVCxFQUFjSixHQUFkLEVBQWtCO0FBQUMsVUFBSTZPLEdBQUo7QUFBQSxVQUFRMlMsS0FBSyxHQUFDLENBQWQ7QUFBQSxVQUFnQkMsS0FBSyxHQUFDaGdCLE1BQU0sQ0FBQ2thLFFBQVAsRUFBdEI7QUFBQSxVQUF3QzNMLFFBQVEsR0FBQyxJQUFqRDtBQUFBLFVBQXNEcFAsQ0FBQyxHQUFDLEtBQUtxQixNQUE3RDtBQUFBLFVBQW9FbVosT0FBTyxHQUFDLFNBQVJBLE9BQVEsR0FBVTtBQUFDLFlBQUcsQ0FBRSxHQUFFb0csS0FBUCxFQUFjO0FBQUNDLGVBQUssQ0FBQ3ZFLFdBQU4sQ0FBa0JsTixRQUFsQixFQUEyQixDQUFDQSxRQUFELENBQTNCO0FBQXdDO0FBQUMsT0FBL0k7O0FBQWdKLFVBQUcsT0FBTzVQLElBQVAsS0FBYyxRQUFqQixFQUEwQjtBQUFDSixXQUFHLEdBQUNJLElBQUo7QUFBU0EsWUFBSSxHQUFDa0UsU0FBTDtBQUFnQjs7QUFDOWlCbEUsVUFBSSxHQUFDQSxJQUFJLElBQUUsSUFBWDs7QUFBZ0IsYUFBTVEsQ0FBQyxFQUFQLEVBQVU7QUFBQ2lPLFdBQUcsR0FBQ3lSLFFBQVEsQ0FBQ25lLEdBQVQsQ0FBYTZOLFFBQVEsQ0FBQ3BQLENBQUQsQ0FBckIsRUFBeUJSLElBQUksR0FBQyxZQUE5QixDQUFKOztBQUFnRCxZQUFHeU8sR0FBRyxJQUFFQSxHQUFHLENBQUM4TCxLQUFaLEVBQWtCO0FBQUM2RyxlQUFLO0FBQUczUyxhQUFHLENBQUM4TCxLQUFKLENBQVUzQixHQUFWLENBQWNvQyxPQUFkO0FBQXdCO0FBQUM7O0FBQy9IQSxhQUFPO0FBQUcsYUFBT3FHLEtBQUssQ0FBQ2pHLE9BQU4sQ0FBY3hiLEdBQWQsQ0FBUDtBQUEyQjtBQUpvUCxHQUFqQjtBQUloTyxNQUFJMGhCLElBQUksR0FBRSxxQ0FBRCxDQUF3Q0MsTUFBakQ7QUFBd0QsTUFBSUMsT0FBTyxHQUFDLElBQUluWixNQUFKLENBQVcsbUJBQWlCaVosSUFBakIsR0FBc0IsYUFBakMsRUFBK0MsR0FBL0MsQ0FBWjtBQUFnRSxNQUFJRyxTQUFTLEdBQUMsQ0FBQyxLQUFELEVBQU8sT0FBUCxFQUFlLFFBQWYsRUFBd0IsTUFBeEIsQ0FBZDtBQUE4QyxNQUFJaFUsZUFBZSxHQUFDdFAsUUFBUSxDQUFDc1AsZUFBN0I7O0FBQTZDLE1BQUlpVSxVQUFVLEdBQUMsb0JBQVNqZixJQUFULEVBQWM7QUFBQyxXQUFPcEIsTUFBTSxDQUFDMEYsUUFBUCxDQUFnQnRFLElBQUksQ0FBQ3VJLGFBQXJCLEVBQW1DdkksSUFBbkMsQ0FBUDtBQUFpRCxHQUEvRTtBQUFBLE1BQWdGa2YsUUFBUSxHQUFDO0FBQUNBLFlBQVEsRUFBQztBQUFWLEdBQXpGOztBQUF5RyxNQUFHbFUsZUFBZSxDQUFDbVUsV0FBbkIsRUFBK0I7QUFBQ0YsY0FBVSxHQUFDLG9CQUFTamYsSUFBVCxFQUFjO0FBQUMsYUFBT3BCLE1BQU0sQ0FBQzBGLFFBQVAsQ0FBZ0J0RSxJQUFJLENBQUN1SSxhQUFyQixFQUFtQ3ZJLElBQW5DLEtBQTBDQSxJQUFJLENBQUNtZixXQUFMLENBQWlCRCxRQUFqQixNQUE2QmxmLElBQUksQ0FBQ3VJLGFBQW5GO0FBQWtHLEtBQTVIO0FBQThIOztBQUNsZ0IsTUFBSTZXLGtCQUFrQixHQUFDLFNBQW5CQSxrQkFBbUIsQ0FBU3BmLElBQVQsRUFBYzZKLEVBQWQsRUFBaUI7QUFBQzdKLFFBQUksR0FBQzZKLEVBQUUsSUFBRTdKLElBQVQ7QUFBYyxXQUFPQSxJQUFJLENBQUNxZixLQUFMLENBQVdDLE9BQVgsS0FBcUIsTUFBckIsSUFBNkJ0ZixJQUFJLENBQUNxZixLQUFMLENBQVdDLE9BQVgsS0FBcUIsRUFBckIsSUFBeUJMLFVBQVUsQ0FBQ2pmLElBQUQsQ0FBbkMsSUFBMkNwQixNQUFNLENBQUMyZ0IsR0FBUCxDQUFXdmYsSUFBWCxFQUFnQixTQUFoQixNQUE2QixNQUE1RztBQUFvSCxHQUEzSzs7QUFBNEssV0FBU3dmLFNBQVQsQ0FBbUJ4ZixJQUFuQixFQUF3QnVkLElBQXhCLEVBQTZCa0MsVUFBN0IsRUFBd0NDLEtBQXhDLEVBQThDO0FBQUMsUUFBSUMsUUFBSjtBQUFBLFFBQWFDLEtBQWI7QUFBQSxRQUFtQkMsYUFBYSxHQUFDLEVBQWpDO0FBQUEsUUFBb0NDLFlBQVksR0FBQ0osS0FBSyxHQUFDLFlBQVU7QUFBQyxhQUFPQSxLQUFLLENBQUN2VixHQUFOLEVBQVA7QUFBb0IsS0FBaEMsR0FBaUMsWUFBVTtBQUFDLGFBQU92TCxNQUFNLENBQUMyZ0IsR0FBUCxDQUFXdmYsSUFBWCxFQUFnQnVkLElBQWhCLEVBQXFCLEVBQXJCLENBQVA7QUFBaUMsS0FBbkk7QUFBQSxRQUFvSXdDLE9BQU8sR0FBQ0QsWUFBWSxFQUF4SjtBQUFBLFFBQTJKRSxJQUFJLEdBQUNQLFVBQVUsSUFBRUEsVUFBVSxDQUFDLENBQUQsQ0FBdEIsS0FBNEI3Z0IsTUFBTSxDQUFDcWhCLFNBQVAsQ0FBaUIxQyxJQUFqQixJQUF1QixFQUF2QixHQUEwQixJQUF0RCxDQUFoSztBQUFBLFFBQTROMkMsYUFBYSxHQUFDbGdCLElBQUksQ0FBQzVDLFFBQUwsS0FBZ0J3QixNQUFNLENBQUNxaEIsU0FBUCxDQUFpQjFDLElBQWpCLEtBQXdCeUMsSUFBSSxLQUFHLElBQVAsSUFBYSxDQUFDRCxPQUF0RCxLQUFnRWhCLE9BQU8sQ0FBQ3ZXLElBQVIsQ0FBYTVKLE1BQU0sQ0FBQzJnQixHQUFQLENBQVd2ZixJQUFYLEVBQWdCdWQsSUFBaEIsQ0FBYixDQUExUzs7QUFBOFUsUUFBRzJDLGFBQWEsSUFBRUEsYUFBYSxDQUFDLENBQUQsQ0FBYixLQUFtQkYsSUFBckMsRUFBMEM7QUFBQ0QsYUFBTyxHQUFDQSxPQUFPLEdBQUMsQ0FBaEI7QUFBa0JDLFVBQUksR0FBQ0EsSUFBSSxJQUFFRSxhQUFhLENBQUMsQ0FBRCxDQUF4QjtBQUE0QkEsbUJBQWEsR0FBQyxDQUFDSCxPQUFELElBQVUsQ0FBeEI7O0FBQTBCLGFBQU1GLGFBQWEsRUFBbkIsRUFBc0I7QUFBQ2poQixjQUFNLENBQUN5Z0IsS0FBUCxDQUFhcmYsSUFBYixFQUFrQnVkLElBQWxCLEVBQXVCMkMsYUFBYSxHQUFDRixJQUFyQzs7QUFBMkMsWUFBRyxDQUFDLElBQUVKLEtBQUgsS0FBVyxLQUFHQSxLQUFLLEdBQUNFLFlBQVksS0FBR0MsT0FBZixJQUF3QixHQUFqQyxDQUFYLEtBQW1ELENBQXRELEVBQXdEO0FBQUNGLHVCQUFhLEdBQUMsQ0FBZDtBQUFpQjs7QUFDeHlCSyxxQkFBYSxHQUFDQSxhQUFhLEdBQUNOLEtBQTVCO0FBQW1DOztBQUNuQ00sbUJBQWEsR0FBQ0EsYUFBYSxHQUFDLENBQTVCO0FBQThCdGhCLFlBQU0sQ0FBQ3lnQixLQUFQLENBQWFyZixJQUFiLEVBQWtCdWQsSUFBbEIsRUFBdUIyQyxhQUFhLEdBQUNGLElBQXJDO0FBQTJDUCxnQkFBVSxHQUFDQSxVQUFVLElBQUUsRUFBdkI7QUFBMkI7O0FBQ3BHLFFBQUdBLFVBQUgsRUFBYztBQUFDUyxtQkFBYSxHQUFDLENBQUNBLGFBQUQsSUFBZ0IsQ0FBQ0gsT0FBakIsSUFBMEIsQ0FBeEM7QUFBMENKLGNBQVEsR0FBQ0YsVUFBVSxDQUFDLENBQUQsQ0FBVixHQUFjUyxhQUFhLEdBQUMsQ0FBQ1QsVUFBVSxDQUFDLENBQUQsQ0FBVixHQUFjLENBQWYsSUFBa0JBLFVBQVUsQ0FBQyxDQUFELENBQXhELEdBQTRELENBQUNBLFVBQVUsQ0FBQyxDQUFELENBQWhGOztBQUFvRixVQUFHQyxLQUFILEVBQVM7QUFBQ0EsYUFBSyxDQUFDTSxJQUFOLEdBQVdBLElBQVg7QUFBZ0JOLGFBQUssQ0FBQ3ZRLEtBQU4sR0FBWStRLGFBQVo7QUFBMEJSLGFBQUssQ0FBQy9lLEdBQU4sR0FBVWdmLFFBQVY7QUFBb0I7QUFBQzs7QUFDdE4sV0FBT0EsUUFBUDtBQUFpQjs7QUFDakIsTUFBSVEsaUJBQWlCLEdBQUMsRUFBdEI7O0FBQXlCLFdBQVNDLGlCQUFULENBQTJCcGdCLElBQTNCLEVBQWdDO0FBQUMsUUFBSTBTLElBQUo7QUFBQSxRQUFTNVUsR0FBRyxHQUFDa0MsSUFBSSxDQUFDdUksYUFBbEI7QUFBQSxRQUFnQ2IsUUFBUSxHQUFDMUgsSUFBSSxDQUFDMEgsUUFBOUM7QUFBQSxRQUF1RDRYLE9BQU8sR0FBQ2EsaUJBQWlCLENBQUN6WSxRQUFELENBQWhGOztBQUEyRixRQUFHNFgsT0FBSCxFQUFXO0FBQUMsYUFBT0EsT0FBUDtBQUFnQjs7QUFDakw1TSxRQUFJLEdBQUM1VSxHQUFHLENBQUN1aUIsSUFBSixDQUFTOWhCLFdBQVQsQ0FBcUJULEdBQUcsQ0FBQ0ksYUFBSixDQUFrQndKLFFBQWxCLENBQXJCLENBQUw7QUFBdUQ0WCxXQUFPLEdBQUMxZ0IsTUFBTSxDQUFDMmdCLEdBQVAsQ0FBVzdNLElBQVgsRUFBZ0IsU0FBaEIsQ0FBUjtBQUFtQ0EsUUFBSSxDQUFDbFUsVUFBTCxDQUFnQkMsV0FBaEIsQ0FBNEJpVSxJQUE1Qjs7QUFBa0MsUUFBRzRNLE9BQU8sS0FBRyxNQUFiLEVBQW9CO0FBQUNBLGFBQU8sR0FBQyxPQUFSO0FBQWlCOztBQUNsS2EscUJBQWlCLENBQUN6WSxRQUFELENBQWpCLEdBQTRCNFgsT0FBNUI7QUFBb0MsV0FBT0EsT0FBUDtBQUFnQjs7QUFDcEQsV0FBU2dCLFFBQVQsQ0FBa0JuVCxRQUFsQixFQUEyQm9ULElBQTNCLEVBQWdDO0FBQUMsUUFBSWpCLE9BQUo7QUFBQSxRQUFZdGYsSUFBWjtBQUFBLFFBQWlCd2dCLE1BQU0sR0FBQyxFQUF4QjtBQUFBLFFBQTJCdkssS0FBSyxHQUFDLENBQWpDO0FBQUEsUUFBbUM3VyxNQUFNLEdBQUMrTixRQUFRLENBQUMvTixNQUFuRDs7QUFBMEQsV0FBSzZXLEtBQUssR0FBQzdXLE1BQVgsRUFBa0I2VyxLQUFLLEVBQXZCLEVBQTBCO0FBQUNqVyxVQUFJLEdBQUNtTixRQUFRLENBQUM4SSxLQUFELENBQWI7O0FBQXFCLFVBQUcsQ0FBQ2pXLElBQUksQ0FBQ3FmLEtBQVQsRUFBZTtBQUFDO0FBQVU7O0FBQ3JLQyxhQUFPLEdBQUN0ZixJQUFJLENBQUNxZixLQUFMLENBQVdDLE9BQW5COztBQUEyQixVQUFHaUIsSUFBSCxFQUFRO0FBQUMsWUFBR2pCLE9BQU8sS0FBRyxNQUFiLEVBQW9CO0FBQUNrQixnQkFBTSxDQUFDdkssS0FBRCxDQUFOLEdBQWN3SCxRQUFRLENBQUNuZSxHQUFULENBQWFVLElBQWIsRUFBa0IsU0FBbEIsS0FBOEIsSUFBNUM7O0FBQWlELGNBQUcsQ0FBQ3dnQixNQUFNLENBQUN2SyxLQUFELENBQVYsRUFBa0I7QUFBQ2pXLGdCQUFJLENBQUNxZixLQUFMLENBQVdDLE9BQVgsR0FBbUIsRUFBbkI7QUFBdUI7QUFBQzs7QUFDckosWUFBR3RmLElBQUksQ0FBQ3FmLEtBQUwsQ0FBV0MsT0FBWCxLQUFxQixFQUFyQixJQUF5QkYsa0JBQWtCLENBQUNwZixJQUFELENBQTlDLEVBQXFEO0FBQUN3Z0IsZ0JBQU0sQ0FBQ3ZLLEtBQUQsQ0FBTixHQUFjbUssaUJBQWlCLENBQUNwZ0IsSUFBRCxDQUEvQjtBQUF1QztBQUFDLE9BRG5FLE1BQ3VFO0FBQUMsWUFBR3NmLE9BQU8sS0FBRyxNQUFiLEVBQW9CO0FBQUNrQixnQkFBTSxDQUFDdkssS0FBRCxDQUFOLEdBQWMsTUFBZDtBQUFxQndILGtCQUFRLENBQUNKLEdBQVQsQ0FBYXJkLElBQWIsRUFBa0IsU0FBbEIsRUFBNEJzZixPQUE1QjtBQUFzQztBQUFDO0FBQUM7O0FBQ3JMLFNBQUlySixLQUFLLEdBQUMsQ0FBVixFQUFZQSxLQUFLLEdBQUM3VyxNQUFsQixFQUF5QjZXLEtBQUssRUFBOUIsRUFBaUM7QUFBQyxVQUFHdUssTUFBTSxDQUFDdkssS0FBRCxDQUFOLElBQWUsSUFBbEIsRUFBdUI7QUFBQzlJLGdCQUFRLENBQUM4SSxLQUFELENBQVIsQ0FBZ0JvSixLQUFoQixDQUFzQkMsT0FBdEIsR0FBOEJrQixNQUFNLENBQUN2SyxLQUFELENBQXBDO0FBQTZDO0FBQUM7O0FBQ3hHLFdBQU85SSxRQUFQO0FBQWlCOztBQUNqQnZPLFFBQU0sQ0FBQ0csRUFBUCxDQUFVK0IsTUFBVixDQUFpQjtBQUFDeWYsUUFBSSxFQUFDLGdCQUFVO0FBQUMsYUFBT0QsUUFBUSxDQUFDLElBQUQsRUFBTSxJQUFOLENBQWY7QUFBNEIsS0FBN0M7QUFBOENHLFFBQUksRUFBQyxnQkFBVTtBQUFDLGFBQU9ILFFBQVEsQ0FBQyxJQUFELENBQWY7QUFBdUIsS0FBckY7QUFBc0ZJLFVBQU0sRUFBQyxnQkFBU3pILEtBQVQsRUFBZTtBQUFDLFVBQUcsT0FBT0EsS0FBUCxLQUFlLFNBQWxCLEVBQTRCO0FBQUMsZUFBT0EsS0FBSyxHQUFDLEtBQUtzSCxJQUFMLEVBQUQsR0FBYSxLQUFLRSxJQUFMLEVBQXpCO0FBQXNDOztBQUNqTSxhQUFPLEtBQUs1Z0IsSUFBTCxDQUFVLFlBQVU7QUFBQyxZQUFHdWYsa0JBQWtCLENBQUMsSUFBRCxDQUFyQixFQUE0QjtBQUFDeGdCLGdCQUFNLENBQUMsSUFBRCxDQUFOLENBQWEyaEIsSUFBYjtBQUFxQixTQUFsRCxNQUFzRDtBQUFDM2hCLGdCQUFNLENBQUMsSUFBRCxDQUFOLENBQWE2aEIsSUFBYjtBQUFxQjtBQUFDLE9BQWxHLENBQVA7QUFBNEc7QUFEM0YsR0FBakI7QUFDK0csTUFBSUUsY0FBYyxHQUFFLHVCQUFwQjtBQUE2QyxNQUFJQyxRQUFRLEdBQUUsZ0NBQWQ7QUFBZ0QsTUFBSUMsV0FBVyxHQUFFLG9DQUFqQjs7QUFBdUQsR0FBQyxZQUFVO0FBQUMsUUFBSUMsUUFBUSxHQUFDcGxCLFFBQVEsQ0FBQ3FsQixzQkFBVCxFQUFiO0FBQUEsUUFBK0NDLEdBQUcsR0FBQ0YsUUFBUSxDQUFDdmlCLFdBQVQsQ0FBcUI3QyxRQUFRLENBQUN3QyxhQUFULENBQXVCLEtBQXZCLENBQXJCLENBQW5EO0FBQUEsUUFBdUcrTixLQUFLLEdBQUN2USxRQUFRLENBQUN3QyxhQUFULENBQXVCLE9BQXZCLENBQTdHO0FBQTZJK04sU0FBSyxDQUFDNU4sWUFBTixDQUFtQixNQUFuQixFQUEwQixPQUExQjtBQUFtQzROLFNBQUssQ0FBQzVOLFlBQU4sQ0FBbUIsU0FBbkIsRUFBNkIsU0FBN0I7QUFBd0M0TixTQUFLLENBQUM1TixZQUFOLENBQW1CLE1BQW5CLEVBQTBCLEdBQTFCO0FBQStCMmlCLE9BQUcsQ0FBQ3ppQixXQUFKLENBQWdCME4sS0FBaEI7QUFBdUJoUCxXQUFPLENBQUNna0IsVUFBUixHQUFtQkQsR0FBRyxDQUFDRSxTQUFKLENBQWMsSUFBZCxFQUFvQkEsU0FBcEIsQ0FBOEIsSUFBOUIsRUFBb0M1UixTQUFwQyxDQUE4Q2lCLE9BQWpFO0FBQXlFeVEsT0FBRyxDQUFDOVUsU0FBSixHQUFjLHdCQUFkO0FBQXVDalAsV0FBTyxDQUFDa2tCLGNBQVIsR0FBdUIsQ0FBQyxDQUFDSCxHQUFHLENBQUNFLFNBQUosQ0FBYyxJQUFkLEVBQW9CNVIsU0FBcEIsQ0FBOEI4RSxZQUF2RDtBQUFvRTRNLE9BQUcsQ0FBQzlVLFNBQUosR0FBYyxtQkFBZDtBQUFrQ2pQLFdBQU8sQ0FBQ21rQixNQUFSLEdBQWUsQ0FBQyxDQUFDSixHQUFHLENBQUMxUixTQUFyQjtBQUFnQyxHQUFoaEI7O0FBQW9oQixNQUFJK1IsT0FBTyxHQUFDO0FBQUNDLFNBQUssRUFBQyxDQUFDLENBQUQsRUFBRyxTQUFILEVBQWEsVUFBYixDQUFQO0FBQWdDQyxPQUFHLEVBQUMsQ0FBQyxDQUFELEVBQUcsbUJBQUgsRUFBdUIscUJBQXZCLENBQXBDO0FBQWtGQyxNQUFFLEVBQUMsQ0FBQyxDQUFELEVBQUcsZ0JBQUgsRUFBb0Isa0JBQXBCLENBQXJGO0FBQTZIQyxNQUFFLEVBQUMsQ0FBQyxDQUFELEVBQUcsb0JBQUgsRUFBd0IsdUJBQXhCLENBQWhJO0FBQWlMQyxZQUFRLEVBQUMsQ0FBQyxDQUFELEVBQUcsRUFBSCxFQUFNLEVBQU47QUFBMUwsR0FBWjtBQUFpTkwsU0FBTyxDQUFDTSxLQUFSLEdBQWNOLE9BQU8sQ0FBQ08sS0FBUixHQUFjUCxPQUFPLENBQUNRLFFBQVIsR0FBaUJSLE9BQU8sQ0FBQ1MsT0FBUixHQUFnQlQsT0FBTyxDQUFDQyxLQUFyRTtBQUEyRUQsU0FBTyxDQUFDVSxFQUFSLEdBQVdWLE9BQU8sQ0FBQ0ksRUFBbkI7O0FBQXNCLE1BQUcsQ0FBQ3hrQixPQUFPLENBQUNta0IsTUFBWixFQUFtQjtBQUFDQyxXQUFPLENBQUNXLFFBQVIsR0FBaUJYLE9BQU8sQ0FBQ0QsTUFBUixHQUFlLENBQUMsQ0FBRCxFQUFHLDhCQUFILEVBQWtDLFdBQWxDLENBQWhDO0FBQWdGOztBQUM3cUMsV0FBU2EsTUFBVCxDQUFnQm5qQixPQUFoQixFQUF3QmlOLEdBQXhCLEVBQTRCO0FBQUMsUUFBSXJNLEdBQUo7O0FBQVEsUUFBRyxPQUFPWixPQUFPLENBQUM2SixvQkFBZixLQUFzQyxXQUF6QyxFQUFxRDtBQUFDakosU0FBRyxHQUFDWixPQUFPLENBQUM2SixvQkFBUixDQUE2Qm9ELEdBQUcsSUFBRSxHQUFsQyxDQUFKO0FBQTRDLEtBQWxHLE1BQXVHLElBQUcsT0FBT2pOLE9BQU8sQ0FBQ3FLLGdCQUFmLEtBQWtDLFdBQXJDLEVBQWlEO0FBQUN6SixTQUFHLEdBQUNaLE9BQU8sQ0FBQ3FLLGdCQUFSLENBQXlCNEMsR0FBRyxJQUFFLEdBQTlCLENBQUo7QUFBd0MsS0FBMUYsTUFBOEY7QUFBQ3JNLFNBQUcsR0FBQyxFQUFKO0FBQVE7O0FBQ25QLFFBQUdxTSxHQUFHLEtBQUd0SyxTQUFOLElBQWlCc0ssR0FBRyxJQUFFckUsUUFBUSxDQUFDNUksT0FBRCxFQUFTaU4sR0FBVCxDQUFqQyxFQUErQztBQUFDLGFBQU9uTixNQUFNLENBQUNlLEtBQVAsQ0FBYSxDQUFDYixPQUFELENBQWIsRUFBdUJZLEdBQXZCLENBQVA7QUFBb0M7O0FBQ3BGLFdBQU9BLEdBQVA7QUFBWTs7QUFDWixXQUFTd2lCLGFBQVQsQ0FBdUJ6aUIsS0FBdkIsRUFBNkIwaUIsV0FBN0IsRUFBeUM7QUFBQyxRQUFJcGtCLENBQUMsR0FBQyxDQUFOO0FBQUEsUUFBUWdZLENBQUMsR0FBQ3RXLEtBQUssQ0FBQ0wsTUFBaEI7O0FBQXVCLFdBQUtyQixDQUFDLEdBQUNnWSxDQUFQLEVBQVNoWSxDQUFDLEVBQVYsRUFBYTtBQUFDMGYsY0FBUSxDQUFDSixHQUFULENBQWE1ZCxLQUFLLENBQUMxQixDQUFELENBQWxCLEVBQXNCLFlBQXRCLEVBQW1DLENBQUNva0IsV0FBRCxJQUFjMUUsUUFBUSxDQUFDbmUsR0FBVCxDQUFhNmlCLFdBQVcsQ0FBQ3BrQixDQUFELENBQXhCLEVBQTRCLFlBQTVCLENBQWpEO0FBQTZGO0FBQUM7O0FBQzdLLE1BQUlxSSxLQUFLLEdBQUMsV0FBVjs7QUFBc0IsV0FBU2djLGFBQVQsQ0FBdUIzaUIsS0FBdkIsRUFBNkJYLE9BQTdCLEVBQXFDdWpCLE9BQXJDLEVBQTZDQyxTQUE3QyxFQUF1REMsT0FBdkQsRUFBK0Q7QUFBQyxRQUFJdmlCLElBQUo7QUFBQSxRQUFTZ00sR0FBVDtBQUFBLFFBQWFELEdBQWI7QUFBQSxRQUFpQnlXLElBQWpCO0FBQUEsUUFBc0JDLFFBQXRCO0FBQUEsUUFBK0IvaEIsQ0FBL0I7QUFBQSxRQUFpQ29nQixRQUFRLEdBQUNoaUIsT0FBTyxDQUFDaWlCLHNCQUFSLEVBQTFDO0FBQUEsUUFBMkUyQixLQUFLLEdBQUMsRUFBakY7QUFBQSxRQUFvRjNrQixDQUFDLEdBQUMsQ0FBdEY7QUFBQSxRQUF3RmdZLENBQUMsR0FBQ3RXLEtBQUssQ0FBQ0wsTUFBaEc7O0FBQXVHLFdBQUtyQixDQUFDLEdBQUNnWSxDQUFQLEVBQVNoWSxDQUFDLEVBQVYsRUFBYTtBQUFDaUMsVUFBSSxHQUFDUCxLQUFLLENBQUMxQixDQUFELENBQVY7O0FBQWMsVUFBR2lDLElBQUksSUFBRUEsSUFBSSxLQUFHLENBQWhCLEVBQWtCO0FBQUMsWUFBR3RCLE1BQU0sQ0FBQ3NCLElBQUQsQ0FBTixLQUFlLFFBQWxCLEVBQTJCO0FBQUNwQixnQkFBTSxDQUFDZSxLQUFQLENBQWEraUIsS0FBYixFQUFtQjFpQixJQUFJLENBQUM1QyxRQUFMLEdBQWMsQ0FBQzRDLElBQUQsQ0FBZCxHQUFxQkEsSUFBeEM7QUFBK0MsU0FBM0UsTUFBZ0YsSUFBRyxDQUFDb0csS0FBSyxDQUFDMEMsSUFBTixDQUFXOUksSUFBWCxDQUFKLEVBQXFCO0FBQUMwaUIsZUFBSyxDQUFDam1CLElBQU4sQ0FBV3FDLE9BQU8sQ0FBQzZqQixjQUFSLENBQXVCM2lCLElBQXZCLENBQVg7QUFBMEMsU0FBaEUsTUFBb0U7QUFBQ2dNLGFBQUcsR0FBQ0EsR0FBRyxJQUFFOFUsUUFBUSxDQUFDdmlCLFdBQVQsQ0FBcUJPLE9BQU8sQ0FBQ1osYUFBUixDQUFzQixLQUF0QixDQUFyQixDQUFUO0FBQTRENk4sYUFBRyxHQUFDLENBQUM2VSxRQUFRLENBQUNwWSxJQUFULENBQWN4SSxJQUFkLEtBQXFCLENBQUMsRUFBRCxFQUFJLEVBQUosQ0FBdEIsRUFBK0IsQ0FBL0IsRUFBa0NzRCxXQUFsQyxFQUFKO0FBQW9Ea2YsY0FBSSxHQUFDbkIsT0FBTyxDQUFDdFYsR0FBRCxDQUFQLElBQWNzVixPQUFPLENBQUNLLFFBQTNCO0FBQW9DMVYsYUFBRyxDQUFDRSxTQUFKLEdBQWNzVyxJQUFJLENBQUMsQ0FBRCxDQUFKLEdBQVE1akIsTUFBTSxDQUFDZ2tCLGFBQVAsQ0FBcUI1aUIsSUFBckIsQ0FBUixHQUFtQ3dpQixJQUFJLENBQUMsQ0FBRCxDQUFyRDtBQUF5RDloQixXQUFDLEdBQUM4aEIsSUFBSSxDQUFDLENBQUQsQ0FBTjs7QUFBVSxpQkFBTTloQixDQUFDLEVBQVAsRUFBVTtBQUFDc0wsZUFBRyxHQUFDQSxHQUFHLENBQUNzRCxTQUFSO0FBQW1COztBQUN0bkIxUSxnQkFBTSxDQUFDZSxLQUFQLENBQWEraUIsS0FBYixFQUFtQjFXLEdBQUcsQ0FBQ25FLFVBQXZCO0FBQW1DbUUsYUFBRyxHQUFDOFUsUUFBUSxDQUFDbFQsVUFBYjtBQUF3QjVCLGFBQUcsQ0FBQzJCLFdBQUosR0FBZ0IsRUFBaEI7QUFBb0I7QUFBQztBQUFDOztBQUNqRm1ULFlBQVEsQ0FBQ25ULFdBQVQsR0FBcUIsRUFBckI7QUFBd0I1UCxLQUFDLEdBQUMsQ0FBRjs7QUFBSSxXQUFPaUMsSUFBSSxHQUFDMGlCLEtBQUssQ0FBQzNrQixDQUFDLEVBQUYsQ0FBakIsRUFBd0I7QUFBQyxVQUFHdWtCLFNBQVMsSUFBRTFqQixNQUFNLENBQUM2RCxPQUFQLENBQWV6QyxJQUFmLEVBQW9Cc2lCLFNBQXBCLElBQStCLENBQUMsQ0FBOUMsRUFBZ0Q7QUFBQyxZQUFHQyxPQUFILEVBQVc7QUFBQ0EsaUJBQU8sQ0FBQzlsQixJQUFSLENBQWF1RCxJQUFiO0FBQW9COztBQUN0STtBQUFVOztBQUNWeWlCLGNBQVEsR0FBQ3hELFVBQVUsQ0FBQ2pmLElBQUQsQ0FBbkI7QUFBMEJnTSxTQUFHLEdBQUNpVyxNQUFNLENBQUNuQixRQUFRLENBQUN2aUIsV0FBVCxDQUFxQnlCLElBQXJCLENBQUQsRUFBNEIsUUFBNUIsQ0FBVjs7QUFBZ0QsVUFBR3lpQixRQUFILEVBQVk7QUFBQ1AscUJBQWEsQ0FBQ2xXLEdBQUQsQ0FBYjtBQUFvQjs7QUFDM0csVUFBR3FXLE9BQUgsRUFBVztBQUFDM2hCLFNBQUMsR0FBQyxDQUFGOztBQUFJLGVBQU9WLElBQUksR0FBQ2dNLEdBQUcsQ0FBQ3RMLENBQUMsRUFBRixDQUFmLEVBQXNCO0FBQUMsY0FBR21nQixXQUFXLENBQUMvWCxJQUFaLENBQWlCOUksSUFBSSxDQUFDekMsSUFBTCxJQUFXLEVBQTVCLENBQUgsRUFBbUM7QUFBQzhrQixtQkFBTyxDQUFDNWxCLElBQVIsQ0FBYXVELElBQWI7QUFBb0I7QUFBQztBQUFDO0FBQUM7O0FBQ2xHLFdBQU84Z0IsUUFBUDtBQUFpQjs7QUFDakIsTUFDQStCLFNBQVMsR0FBQyxNQURWO0FBQUEsTUFDaUJDLFdBQVcsR0FBQyxnREFEN0I7QUFBQSxNQUM4RUMsY0FBYyxHQUFDLHFCQUQ3Rjs7QUFDbUgsV0FBU0MsVUFBVCxHQUFxQjtBQUFDLFdBQU8sSUFBUDtBQUFhOztBQUN0SixXQUFTQyxXQUFULEdBQXNCO0FBQUMsV0FBTyxLQUFQO0FBQWM7O0FBQ3JDLFdBQVNDLFVBQVQsQ0FBb0JsakIsSUFBcEIsRUFBeUJ6QyxJQUF6QixFQUE4QjtBQUFDLFdBQU95QyxJQUFJLEtBQUdtakIsaUJBQWlCLEVBQXpCLE1BQWdDNWxCLElBQUksS0FBRyxPQUF2QyxDQUFOO0FBQXVEOztBQUN0RixXQUFTNGxCLGlCQUFULEdBQTRCO0FBQUMsUUFBRztBQUFDLGFBQU96bkIsUUFBUSxDQUFDeVUsYUFBaEI7QUFBK0IsS0FBbkMsQ0FBbUMsT0FBTWlULEdBQU4sRUFBVSxDQUFFO0FBQUM7O0FBQzdFLFdBQVNDLEdBQVQsQ0FBWXJqQixJQUFaLEVBQWlCc2pCLEtBQWpCLEVBQXVCemtCLFFBQXZCLEVBQWdDeWUsSUFBaEMsRUFBcUN2ZSxFQUFyQyxFQUF3Q3drQixHQUF4QyxFQUE0QztBQUFDLFFBQUlDLE1BQUosRUFBV2ptQixJQUFYOztBQUFnQixRQUFHLFFBQU8rbEIsS0FBUCxNQUFlLFFBQWxCLEVBQTJCO0FBQUMsVUFBRyxPQUFPemtCLFFBQVAsS0FBa0IsUUFBckIsRUFBOEI7QUFBQ3llLFlBQUksR0FBQ0EsSUFBSSxJQUFFemUsUUFBWDtBQUFvQkEsZ0JBQVEsR0FBQzRDLFNBQVQ7QUFBb0I7O0FBQ2hLLFdBQUlsRSxJQUFKLElBQVkrbEIsS0FBWixFQUFrQjtBQUFDRCxXQUFFLENBQUNyakIsSUFBRCxFQUFNekMsSUFBTixFQUFXc0IsUUFBWCxFQUFvQnllLElBQXBCLEVBQXlCZ0csS0FBSyxDQUFDL2xCLElBQUQsQ0FBOUIsRUFBcUNnbUIsR0FBckMsQ0FBRjtBQUE2Qzs7QUFDaEUsYUFBT3ZqQixJQUFQO0FBQWE7O0FBQ2IsUUFBR3NkLElBQUksSUFBRSxJQUFOLElBQVl2ZSxFQUFFLElBQUUsSUFBbkIsRUFBd0I7QUFBQ0EsUUFBRSxHQUFDRixRQUFIO0FBQVl5ZSxVQUFJLEdBQUN6ZSxRQUFRLEdBQUM0QyxTQUFkO0FBQXlCLEtBQTlELE1BQW1FLElBQUcxQyxFQUFFLElBQUUsSUFBUCxFQUFZO0FBQUMsVUFBRyxPQUFPRixRQUFQLEtBQWtCLFFBQXJCLEVBQThCO0FBQUNFLFVBQUUsR0FBQ3VlLElBQUg7QUFBUUEsWUFBSSxHQUFDN2IsU0FBTDtBQUFnQixPQUF2RCxNQUEyRDtBQUFDMUMsVUFBRSxHQUFDdWUsSUFBSDtBQUFRQSxZQUFJLEdBQUN6ZSxRQUFMO0FBQWNBLGdCQUFRLEdBQUM0QyxTQUFUO0FBQW9CO0FBQUM7O0FBQ3ZMLFFBQUcxQyxFQUFFLEtBQUcsS0FBUixFQUFjO0FBQUNBLFFBQUUsR0FBQ2trQixXQUFIO0FBQWdCLEtBQS9CLE1BQW9DLElBQUcsQ0FBQ2xrQixFQUFKLEVBQU87QUFBQyxhQUFPaUIsSUFBUDtBQUFhOztBQUN6RCxRQUFHdWpCLEdBQUcsS0FBRyxDQUFULEVBQVc7QUFBQ0MsWUFBTSxHQUFDemtCLEVBQVA7O0FBQVVBLFFBQUUsR0FBQyxZQUFTMGtCLEtBQVQsRUFBZTtBQUFDN2tCLGNBQU0sR0FBRzhrQixHQUFULENBQWFELEtBQWI7QUFBb0IsZUFBT0QsTUFBTSxDQUFDaG5CLEtBQVAsQ0FBYSxJQUFiLEVBQWtCeUQsU0FBbEIsQ0FBUDtBQUFxQyxPQUE1RTs7QUFBNkVsQixRQUFFLENBQUNrRSxJQUFILEdBQVF1Z0IsTUFBTSxDQUFDdmdCLElBQVAsS0FBY3VnQixNQUFNLENBQUN2Z0IsSUFBUCxHQUFZckUsTUFBTSxDQUFDcUUsSUFBUCxFQUExQixDQUFSO0FBQWtEOztBQUNySixXQUFPakQsSUFBSSxDQUFDSCxJQUFMLENBQVUsWUFBVTtBQUFDakIsWUFBTSxDQUFDNmtCLEtBQVAsQ0FBYXROLEdBQWIsQ0FBaUIsSUFBakIsRUFBc0JtTixLQUF0QixFQUE0QnZrQixFQUE1QixFQUErQnVlLElBQS9CLEVBQW9DemUsUUFBcEM7QUFBK0MsS0FBcEUsQ0FBUDtBQUE4RTs7QUFDOUVELFFBQU0sQ0FBQzZrQixLQUFQLEdBQWE7QUFBQ25vQixVQUFNLEVBQUMsRUFBUjtBQUFXNmEsT0FBRyxFQUFDLGFBQVNuVyxJQUFULEVBQWNzakIsS0FBZCxFQUFvQnRaLE9BQXBCLEVBQTRCc1QsSUFBNUIsRUFBaUN6ZSxRQUFqQyxFQUEwQztBQUFDLFVBQUk4a0IsV0FBSjtBQUFBLFVBQWdCQyxXQUFoQjtBQUFBLFVBQTRCNVgsR0FBNUI7QUFBQSxVQUFnQzZYLE1BQWhDO0FBQUEsVUFBdUNDLENBQXZDO0FBQUEsVUFBeUNDLFNBQXpDO0FBQUEsVUFBbUQvSixPQUFuRDtBQUFBLFVBQTJEZ0ssUUFBM0Q7QUFBQSxVQUFvRXptQixJQUFwRTtBQUFBLFVBQXlFMG1CLFVBQXpFO0FBQUEsVUFBb0ZDLFFBQXBGO0FBQUEsVUFBNkZDLFFBQVEsR0FBQzFHLFFBQVEsQ0FBQ25lLEdBQVQsQ0FBYVUsSUFBYixDQUF0Rzs7QUFBeUgsVUFBRyxDQUFDK2MsVUFBVSxDQUFDL2MsSUFBRCxDQUFkLEVBQXFCO0FBQUM7QUFBUTs7QUFDOU4sVUFBR2dLLE9BQU8sQ0FBQ0EsT0FBWCxFQUFtQjtBQUFDMlosbUJBQVcsR0FBQzNaLE9BQVo7QUFBb0JBLGVBQU8sR0FBQzJaLFdBQVcsQ0FBQzNaLE9BQXBCO0FBQTRCbkwsZ0JBQVEsR0FBQzhrQixXQUFXLENBQUM5a0IsUUFBckI7QUFBK0I7O0FBQ25HLFVBQUdBLFFBQUgsRUFBWTtBQUFDRCxjQUFNLENBQUNpTixJQUFQLENBQVlNLGVBQVosQ0FBNEJuQixlQUE1QixFQUE0Q25NLFFBQTVDO0FBQXVEOztBQUNwRSxVQUFHLENBQUNtTCxPQUFPLENBQUMvRyxJQUFaLEVBQWlCO0FBQUMrRyxlQUFPLENBQUMvRyxJQUFSLEdBQWFyRSxNQUFNLENBQUNxRSxJQUFQLEVBQWI7QUFBNEI7O0FBQzlDLFVBQUcsRUFBRTRnQixNQUFNLEdBQUNNLFFBQVEsQ0FBQ04sTUFBbEIsQ0FBSCxFQUE2QjtBQUFDQSxjQUFNLEdBQUNNLFFBQVEsQ0FBQ04sTUFBVCxHQUFnQjVuQixNQUFNLENBQUNtb0IsTUFBUCxDQUFjLElBQWQsQ0FBdkI7QUFBNEM7O0FBQzFFLFVBQUcsRUFBRVIsV0FBVyxHQUFDTyxRQUFRLENBQUNFLE1BQXZCLENBQUgsRUFBa0M7QUFBQ1QsbUJBQVcsR0FBQ08sUUFBUSxDQUFDRSxNQUFULEdBQWdCLFVBQVN2YyxDQUFULEVBQVc7QUFBQyxpQkFBTyxPQUFPbEosTUFBUCxLQUFnQixXQUFoQixJQUE2QkEsTUFBTSxDQUFDNmtCLEtBQVAsQ0FBYWEsU0FBYixLQUF5QnhjLENBQUMsQ0FBQ3ZLLElBQXhELEdBQTZEcUIsTUFBTSxDQUFDNmtCLEtBQVAsQ0FBYWMsUUFBYixDQUFzQi9uQixLQUF0QixDQUE0QndELElBQTVCLEVBQWlDQyxTQUFqQyxDQUE3RCxHQUF5R3dCLFNBQWhIO0FBQTJILFNBQW5LO0FBQXFLOztBQUN4TTZoQixXQUFLLEdBQUMsQ0FBQ0EsS0FBSyxJQUFFLEVBQVIsRUFBWW5iLEtBQVosQ0FBa0IyTyxhQUFsQixLQUFrQyxDQUFDLEVBQUQsQ0FBeEM7QUFBNkNnTixPQUFDLEdBQUNSLEtBQUssQ0FBQ2xrQixNQUFSOztBQUFlLGFBQU0wa0IsQ0FBQyxFQUFQLEVBQVU7QUFBQzlYLFdBQUcsR0FBQytXLGNBQWMsQ0FBQ3ZhLElBQWYsQ0FBb0I4YSxLQUFLLENBQUNRLENBQUQsQ0FBekIsS0FBK0IsRUFBbkM7QUFBc0N2bUIsWUFBSSxHQUFDMm1CLFFBQVEsR0FBQ2xZLEdBQUcsQ0FBQyxDQUFELENBQWpCO0FBQXFCaVksa0JBQVUsR0FBQyxDQUFDalksR0FBRyxDQUFDLENBQUQsQ0FBSCxJQUFRLEVBQVQsRUFBYTVJLEtBQWIsQ0FBbUIsR0FBbkIsRUFBd0J4QyxJQUF4QixFQUFYOztBQUEwQyxZQUFHLENBQUNyRCxJQUFKLEVBQVM7QUFBQztBQUFVOztBQUNoTXljLGVBQU8sR0FBQ3BiLE1BQU0sQ0FBQzZrQixLQUFQLENBQWF6SixPQUFiLENBQXFCemMsSUFBckIsS0FBNEIsRUFBcEM7QUFBdUNBLFlBQUksR0FBQyxDQUFDc0IsUUFBUSxHQUFDbWIsT0FBTyxDQUFDd0ssWUFBVCxHQUFzQnhLLE9BQU8sQ0FBQ3lLLFFBQXZDLEtBQWtEbG5CLElBQXZEO0FBQTREeWMsZUFBTyxHQUFDcGIsTUFBTSxDQUFDNmtCLEtBQVAsQ0FBYXpKLE9BQWIsQ0FBcUJ6YyxJQUFyQixLQUE0QixFQUFwQztBQUF1Q3dtQixpQkFBUyxHQUFDbmxCLE1BQU0sQ0FBQ2tDLE1BQVAsQ0FBYztBQUFDdkQsY0FBSSxFQUFDQSxJQUFOO0FBQVcybUIsa0JBQVEsRUFBQ0EsUUFBcEI7QUFBNkI1RyxjQUFJLEVBQUNBLElBQWxDO0FBQXVDdFQsaUJBQU8sRUFBQ0EsT0FBL0M7QUFBdUQvRyxjQUFJLEVBQUMrRyxPQUFPLENBQUMvRyxJQUFwRTtBQUF5RXBFLGtCQUFRLEVBQUNBLFFBQWxGO0FBQTJGaVcsc0JBQVksRUFBQ2pXLFFBQVEsSUFBRUQsTUFBTSxDQUFDc08sSUFBUCxDQUFZL0UsS0FBWixDQUFrQjJNLFlBQWxCLENBQStCaE0sSUFBL0IsQ0FBb0NqSyxRQUFwQyxDQUFsSDtBQUFnS2lNLG1CQUFTLEVBQUNtWixVQUFVLENBQUMvYSxJQUFYLENBQWdCLEdBQWhCO0FBQTFLLFNBQWQsRUFBOE15YSxXQUE5TSxDQUFWOztBQUFxTyxZQUFHLEVBQUVLLFFBQVEsR0FBQ0gsTUFBTSxDQUFDdG1CLElBQUQsQ0FBakIsQ0FBSCxFQUE0QjtBQUFDeW1CLGtCQUFRLEdBQUNILE1BQU0sQ0FBQ3RtQixJQUFELENBQU4sR0FBYSxFQUF0QjtBQUF5QnltQixrQkFBUSxDQUFDVSxhQUFULEdBQXVCLENBQXZCOztBQUF5QixjQUFHLENBQUMxSyxPQUFPLENBQUMySyxLQUFULElBQWdCM0ssT0FBTyxDQUFDMkssS0FBUixDQUFjcm9CLElBQWQsQ0FBbUIwRCxJQUFuQixFQUF3QnNkLElBQXhCLEVBQTZCMkcsVUFBN0IsRUFBd0NMLFdBQXhDLE1BQXVELEtBQTFFLEVBQWdGO0FBQUMsZ0JBQUc1akIsSUFBSSxDQUFDcUwsZ0JBQVIsRUFBeUI7QUFBQ3JMLGtCQUFJLENBQUNxTCxnQkFBTCxDQUFzQjlOLElBQXRCLEVBQTJCcW1CLFdBQTNCO0FBQXlDO0FBQUM7QUFBQzs7QUFDcGxCLFlBQUc1SixPQUFPLENBQUM3RCxHQUFYLEVBQWU7QUFBQzZELGlCQUFPLENBQUM3RCxHQUFSLENBQVk3WixJQUFaLENBQWlCMEQsSUFBakIsRUFBc0IrakIsU0FBdEI7O0FBQWlDLGNBQUcsQ0FBQ0EsU0FBUyxDQUFDL1osT0FBVixDQUFrQi9HLElBQXRCLEVBQTJCO0FBQUM4Z0IscUJBQVMsQ0FBQy9aLE9BQVYsQ0FBa0IvRyxJQUFsQixHQUF1QitHLE9BQU8sQ0FBQy9HLElBQS9CO0FBQXFDO0FBQUM7O0FBQ25ILFlBQUdwRSxRQUFILEVBQVk7QUFBQ21sQixrQkFBUSxDQUFDbmpCLE1BQVQsQ0FBZ0JtakIsUUFBUSxDQUFDVSxhQUFULEVBQWhCLEVBQXlDLENBQXpDLEVBQTJDWCxTQUEzQztBQUF1RCxTQUFwRSxNQUF3RTtBQUFDQyxrQkFBUSxDQUFDdm5CLElBQVQsQ0FBY3NuQixTQUFkO0FBQTBCOztBQUNuR25sQixjQUFNLENBQUM2a0IsS0FBUCxDQUFhbm9CLE1BQWIsQ0FBb0JpQyxJQUFwQixJQUEwQixJQUExQjtBQUFnQztBQUFDLEtBVnBCO0FBVXFCc2EsVUFBTSxFQUFDLGdCQUFTN1gsSUFBVCxFQUFjc2pCLEtBQWQsRUFBb0J0WixPQUFwQixFQUE0Qm5MLFFBQTVCLEVBQXFDK2xCLFdBQXJDLEVBQWlEO0FBQUMsVUFBSWxrQixDQUFKO0FBQUEsVUFBTW1rQixTQUFOO0FBQUEsVUFBZ0I3WSxHQUFoQjtBQUFBLFVBQW9CNlgsTUFBcEI7QUFBQSxVQUEyQkMsQ0FBM0I7QUFBQSxVQUE2QkMsU0FBN0I7QUFBQSxVQUF1Qy9KLE9BQXZDO0FBQUEsVUFBK0NnSyxRQUEvQztBQUFBLFVBQXdEem1CLElBQXhEO0FBQUEsVUFBNkQwbUIsVUFBN0Q7QUFBQSxVQUF3RUMsUUFBeEU7QUFBQSxVQUFpRkMsUUFBUSxHQUFDMUcsUUFBUSxDQUFDRCxPQUFULENBQWlCeGQsSUFBakIsS0FBd0J5ZCxRQUFRLENBQUNuZSxHQUFULENBQWFVLElBQWIsQ0FBbEg7O0FBQXFJLFVBQUcsQ0FBQ21rQixRQUFELElBQVcsRUFBRU4sTUFBTSxHQUFDTSxRQUFRLENBQUNOLE1BQWxCLENBQWQsRUFBd0M7QUFBQztBQUFROztBQUNqUlAsV0FBSyxHQUFDLENBQUNBLEtBQUssSUFBRSxFQUFSLEVBQVluYixLQUFaLENBQWtCMk8sYUFBbEIsS0FBa0MsQ0FBQyxFQUFELENBQXhDO0FBQTZDZ04sT0FBQyxHQUFDUixLQUFLLENBQUNsa0IsTUFBUjs7QUFBZSxhQUFNMGtCLENBQUMsRUFBUCxFQUFVO0FBQUM5WCxXQUFHLEdBQUMrVyxjQUFjLENBQUN2YSxJQUFmLENBQW9COGEsS0FBSyxDQUFDUSxDQUFELENBQXpCLEtBQStCLEVBQW5DO0FBQXNDdm1CLFlBQUksR0FBQzJtQixRQUFRLEdBQUNsWSxHQUFHLENBQUMsQ0FBRCxDQUFqQjtBQUFxQmlZLGtCQUFVLEdBQUMsQ0FBQ2pZLEdBQUcsQ0FBQyxDQUFELENBQUgsSUFBUSxFQUFULEVBQWE1SSxLQUFiLENBQW1CLEdBQW5CLEVBQXdCeEMsSUFBeEIsRUFBWDs7QUFBMEMsWUFBRyxDQUFDckQsSUFBSixFQUFTO0FBQUMsZUFBSUEsSUFBSixJQUFZc21CLE1BQVosRUFBbUI7QUFBQ2psQixrQkFBTSxDQUFDNmtCLEtBQVAsQ0FBYTVMLE1BQWIsQ0FBb0I3WCxJQUFwQixFQUF5QnpDLElBQUksR0FBQytsQixLQUFLLENBQUNRLENBQUQsQ0FBbkMsRUFBdUM5WixPQUF2QyxFQUErQ25MLFFBQS9DLEVBQXdELElBQXhEO0FBQStEOztBQUN6UTtBQUFVOztBQUNWbWIsZUFBTyxHQUFDcGIsTUFBTSxDQUFDNmtCLEtBQVAsQ0FBYXpKLE9BQWIsQ0FBcUJ6YyxJQUFyQixLQUE0QixFQUFwQztBQUF1Q0EsWUFBSSxHQUFDLENBQUNzQixRQUFRLEdBQUNtYixPQUFPLENBQUN3SyxZQUFULEdBQXNCeEssT0FBTyxDQUFDeUssUUFBdkMsS0FBa0RsbkIsSUFBdkQ7QUFBNER5bUIsZ0JBQVEsR0FBQ0gsTUFBTSxDQUFDdG1CLElBQUQsQ0FBTixJQUFjLEVBQXZCO0FBQTBCeU8sV0FBRyxHQUFDQSxHQUFHLENBQUMsQ0FBRCxDQUFILElBQVEsSUFBSXBHLE1BQUosQ0FBVyxZQUFVcWUsVUFBVSxDQUFDL2EsSUFBWCxDQUFnQixlQUFoQixDQUFWLEdBQTJDLFNBQXRELENBQVo7QUFBNkUyYixpQkFBUyxHQUFDbmtCLENBQUMsR0FBQ3NqQixRQUFRLENBQUM1a0IsTUFBckI7O0FBQTRCLGVBQU1zQixDQUFDLEVBQVAsRUFBVTtBQUFDcWpCLG1CQUFTLEdBQUNDLFFBQVEsQ0FBQ3RqQixDQUFELENBQWxCOztBQUFzQixjQUFHLENBQUNra0IsV0FBVyxJQUFFVixRQUFRLEtBQUdILFNBQVMsQ0FBQ0csUUFBbkMsTUFBK0MsQ0FBQ2xhLE9BQUQsSUFBVUEsT0FBTyxDQUFDL0csSUFBUixLQUFlOGdCLFNBQVMsQ0FBQzlnQixJQUFsRixNQUEwRixDQUFDK0ksR0FBRCxJQUFNQSxHQUFHLENBQUNsRCxJQUFKLENBQVNpYixTQUFTLENBQUNqWixTQUFuQixDQUFoRyxNQUFpSSxDQUFDak0sUUFBRCxJQUFXQSxRQUFRLEtBQUdrbEIsU0FBUyxDQUFDbGxCLFFBQWhDLElBQTBDQSxRQUFRLEtBQUcsSUFBWCxJQUFpQmtsQixTQUFTLENBQUNsbEIsUUFBdE0sQ0FBSCxFQUFtTjtBQUFDbWxCLG9CQUFRLENBQUNuakIsTUFBVCxDQUFnQkgsQ0FBaEIsRUFBa0IsQ0FBbEI7O0FBQXFCLGdCQUFHcWpCLFNBQVMsQ0FBQ2xsQixRQUFiLEVBQXNCO0FBQUNtbEIsc0JBQVEsQ0FBQ1UsYUFBVDtBQUEwQjs7QUFDamlCLGdCQUFHMUssT0FBTyxDQUFDbkMsTUFBWCxFQUFrQjtBQUFDbUMscUJBQU8sQ0FBQ25DLE1BQVIsQ0FBZXZiLElBQWYsQ0FBb0IwRCxJQUFwQixFQUF5QitqQixTQUF6QjtBQUFxQztBQUFDO0FBQUM7O0FBQzFELFlBQUdjLFNBQVMsSUFBRSxDQUFDYixRQUFRLENBQUM1a0IsTUFBeEIsRUFBK0I7QUFBQyxjQUFHLENBQUM0YSxPQUFPLENBQUM4SyxRQUFULElBQW1COUssT0FBTyxDQUFDOEssUUFBUixDQUFpQnhvQixJQUFqQixDQUFzQjBELElBQXRCLEVBQTJCaWtCLFVBQTNCLEVBQXNDRSxRQUFRLENBQUNFLE1BQS9DLE1BQXlELEtBQS9FLEVBQXFGO0FBQUN6bEIsa0JBQU0sQ0FBQ21tQixXQUFQLENBQW1CL2tCLElBQW5CLEVBQXdCekMsSUFBeEIsRUFBNkI0bUIsUUFBUSxDQUFDRSxNQUF0QztBQUErQzs7QUFDckssaUJBQU9SLE1BQU0sQ0FBQ3RtQixJQUFELENBQWI7QUFBcUI7QUFBQzs7QUFDdEIsVUFBR3FCLE1BQU0sQ0FBQ3dELGFBQVAsQ0FBcUJ5aEIsTUFBckIsQ0FBSCxFQUFnQztBQUFDcEcsZ0JBQVEsQ0FBQzVGLE1BQVQsQ0FBZ0I3WCxJQUFoQixFQUFxQixlQUFyQjtBQUF1QztBQUFDLEtBakI1RDtBQWlCNkR1a0IsWUFBUSxFQUFDLGtCQUFTUyxXQUFULEVBQXFCO0FBQUMsVUFBSWpuQixDQUFKO0FBQUEsVUFBTTJDLENBQU47QUFBQSxVQUFRaEIsR0FBUjtBQUFBLFVBQVlrUSxPQUFaO0FBQUEsVUFBb0JtVSxTQUFwQjtBQUFBLFVBQThCa0IsWUFBOUI7QUFBQSxVQUEyQ3hWLElBQUksR0FBQyxJQUFJbE8sS0FBSixDQUFVdEIsU0FBUyxDQUFDYixNQUFwQixDQUFoRDtBQUFBLFVBQTRFcWtCLEtBQUssR0FBQzdrQixNQUFNLENBQUM2a0IsS0FBUCxDQUFheUIsR0FBYixDQUFpQkYsV0FBakIsQ0FBbEY7QUFBQSxVQUFnSGhCLFFBQVEsR0FBQyxDQUFDdkcsUUFBUSxDQUFDbmUsR0FBVCxDQUFhLElBQWIsRUFBa0IsUUFBbEIsS0FBNkJyRCxNQUFNLENBQUNtb0IsTUFBUCxDQUFjLElBQWQsQ0FBOUIsRUFBbURYLEtBQUssQ0FBQ2xtQixJQUF6RCxLQUFnRSxFQUF6TDtBQUFBLFVBQTRMeWMsT0FBTyxHQUFDcGIsTUFBTSxDQUFDNmtCLEtBQVAsQ0FBYXpKLE9BQWIsQ0FBcUJ5SixLQUFLLENBQUNsbUIsSUFBM0IsS0FBa0MsRUFBdE87QUFBeU9rUyxVQUFJLENBQUMsQ0FBRCxDQUFKLEdBQVFnVSxLQUFSOztBQUFjLFdBQUkxbEIsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDa0MsU0FBUyxDQUFDYixNQUFwQixFQUEyQnJCLENBQUMsRUFBNUIsRUFBK0I7QUFBQzBSLFlBQUksQ0FBQzFSLENBQUQsQ0FBSixHQUFRa0MsU0FBUyxDQUFDbEMsQ0FBRCxDQUFqQjtBQUFzQjs7QUFDdFowbEIsV0FBSyxDQUFDMEIsY0FBTixHQUFxQixJQUFyQjs7QUFBMEIsVUFBR25MLE9BQU8sQ0FBQ29MLFdBQVIsSUFBcUJwTCxPQUFPLENBQUNvTCxXQUFSLENBQW9COW9CLElBQXBCLENBQXlCLElBQXpCLEVBQThCbW5CLEtBQTlCLE1BQXVDLEtBQS9ELEVBQXFFO0FBQUM7QUFBUTs7QUFDeEd3QixrQkFBWSxHQUFDcm1CLE1BQU0sQ0FBQzZrQixLQUFQLENBQWFPLFFBQWIsQ0FBc0IxbkIsSUFBdEIsQ0FBMkIsSUFBM0IsRUFBZ0NtbkIsS0FBaEMsRUFBc0NPLFFBQXRDLENBQWI7QUFBNkRqbUIsT0FBQyxHQUFDLENBQUY7O0FBQUksYUFBTSxDQUFDNlIsT0FBTyxHQUFDcVYsWUFBWSxDQUFDbG5CLENBQUMsRUFBRixDQUFyQixLQUE2QixDQUFDMGxCLEtBQUssQ0FBQzRCLG9CQUFOLEVBQXBDLEVBQWlFO0FBQUM1QixhQUFLLENBQUM2QixhQUFOLEdBQW9CMVYsT0FBTyxDQUFDNVAsSUFBNUI7QUFBaUNVLFNBQUMsR0FBQyxDQUFGOztBQUFJLGVBQU0sQ0FBQ3FqQixTQUFTLEdBQUNuVSxPQUFPLENBQUNvVSxRQUFSLENBQWlCdGpCLENBQUMsRUFBbEIsQ0FBWCxLQUFtQyxDQUFDK2lCLEtBQUssQ0FBQzhCLDZCQUFOLEVBQTFDLEVBQWdGO0FBQUMsY0FBRyxDQUFDOUIsS0FBSyxDQUFDK0IsVUFBUCxJQUFtQnpCLFNBQVMsQ0FBQ2paLFNBQVYsS0FBc0IsS0FBekMsSUFBZ0QyWSxLQUFLLENBQUMrQixVQUFOLENBQWlCMWMsSUFBakIsQ0FBc0JpYixTQUFTLENBQUNqWixTQUFoQyxDQUFuRCxFQUE4RjtBQUFDMlksaUJBQUssQ0FBQ00sU0FBTixHQUFnQkEsU0FBaEI7QUFBMEJOLGlCQUFLLENBQUNuRyxJQUFOLEdBQVd5RyxTQUFTLENBQUN6RyxJQUFyQjtBQUEwQjVkLGVBQUcsR0FBQyxDQUFDLENBQUNkLE1BQU0sQ0FBQzZrQixLQUFQLENBQWF6SixPQUFiLENBQXFCK0osU0FBUyxDQUFDRyxRQUEvQixLQUEwQyxFQUEzQyxFQUErQ0csTUFBL0MsSUFBdUROLFNBQVMsQ0FBQy9aLE9BQWxFLEVBQTJFeE4sS0FBM0UsQ0FBaUZvVCxPQUFPLENBQUM1UCxJQUF6RixFQUE4RnlQLElBQTlGLENBQUo7O0FBQXdHLGdCQUFHL1AsR0FBRyxLQUFHK0IsU0FBVCxFQUFtQjtBQUFDLGtCQUFHLENBQUNnaUIsS0FBSyxDQUFDalYsTUFBTixHQUFhOU8sR0FBZCxNQUFxQixLQUF4QixFQUE4QjtBQUFDK2pCLHFCQUFLLENBQUNnQyxjQUFOO0FBQXVCaEMscUJBQUssQ0FBQ2lDLGVBQU47QUFBeUI7QUFBQztBQUFDO0FBQUM7QUFBQzs7QUFDM2xCLFVBQUcxTCxPQUFPLENBQUMyTCxZQUFYLEVBQXdCO0FBQUMzTCxlQUFPLENBQUMyTCxZQUFSLENBQXFCcnBCLElBQXJCLENBQTBCLElBQTFCLEVBQStCbW5CLEtBQS9CO0FBQXVDOztBQUNoRSxhQUFPQSxLQUFLLENBQUNqVixNQUFiO0FBQXFCLEtBckJSO0FBcUJTd1YsWUFBUSxFQUFDLGtCQUFTUCxLQUFULEVBQWVPLFNBQWYsRUFBd0I7QUFBQyxVQUFJam1CLENBQUo7QUFBQSxVQUFNZ21CLFNBQU47QUFBQSxVQUFnQnpXLEdBQWhCO0FBQUEsVUFBb0JzWSxlQUFwQjtBQUFBLFVBQW9DQyxnQkFBcEM7QUFBQSxVQUFxRFosWUFBWSxHQUFDLEVBQWxFO0FBQUEsVUFBcUVQLGFBQWEsR0FBQ1YsU0FBUSxDQUFDVSxhQUE1RjtBQUFBLFVBQTBHdmEsR0FBRyxHQUFDc1osS0FBSyxDQUFDcmlCLE1BQXBIOztBQUEySCxVQUFHc2pCLGFBQWEsSUFBRXZhLEdBQUcsQ0FBQy9NLFFBQW5CLElBQTZCLEVBQUVxbUIsS0FBSyxDQUFDbG1CLElBQU4sS0FBYSxPQUFiLElBQXNCa21CLEtBQUssQ0FBQ3FDLE1BQU4sSUFBYyxDQUF0QyxDQUFoQyxFQUF5RTtBQUFDLGVBQUszYixHQUFHLEtBQUcsSUFBWCxFQUFnQkEsR0FBRyxHQUFDQSxHQUFHLENBQUMzTCxVQUFKLElBQWdCLElBQXBDLEVBQXlDO0FBQUMsY0FBRzJMLEdBQUcsQ0FBQy9NLFFBQUosS0FBZSxDQUFmLElBQWtCLEVBQUVxbUIsS0FBSyxDQUFDbG1CLElBQU4sS0FBYSxPQUFiLElBQXNCNE0sR0FBRyxDQUFDMUMsUUFBSixLQUFlLElBQXZDLENBQXJCLEVBQWtFO0FBQUNtZSwyQkFBZSxHQUFDLEVBQWhCO0FBQW1CQyw0QkFBZ0IsR0FBQyxFQUFqQjs7QUFBb0IsaUJBQUk5bkIsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDMm1CLGFBQVYsRUFBd0IzbUIsQ0FBQyxFQUF6QixFQUE0QjtBQUFDZ21CLHVCQUFTLEdBQUNDLFNBQVEsQ0FBQ2ptQixDQUFELENBQWxCO0FBQXNCdVAsaUJBQUcsR0FBQ3lXLFNBQVMsQ0FBQ2xsQixRQUFWLEdBQW1CLEdBQXZCOztBQUEyQixrQkFBR2duQixnQkFBZ0IsQ0FBQ3ZZLEdBQUQsQ0FBaEIsS0FBd0I3TCxTQUEzQixFQUFxQztBQUFDb2tCLGdDQUFnQixDQUFDdlksR0FBRCxDQUFoQixHQUFzQnlXLFNBQVMsQ0FBQ2pQLFlBQVYsR0FBdUJsVyxNQUFNLENBQUMwTyxHQUFELEVBQUssSUFBTCxDQUFOLENBQWlCMkksS0FBakIsQ0FBdUI5TCxHQUF2QixJQUE0QixDQUFDLENBQXBELEdBQXNEdkwsTUFBTSxDQUFDaU4sSUFBUCxDQUFZeUIsR0FBWixFQUFnQixJQUFoQixFQUFxQixJQUFyQixFQUEwQixDQUFDbkQsR0FBRCxDQUExQixFQUFpQy9LLE1BQTdHO0FBQXFIOztBQUMxbkIsa0JBQUd5bUIsZ0JBQWdCLENBQUN2WSxHQUFELENBQW5CLEVBQXlCO0FBQUNzWSwrQkFBZSxDQUFDbnBCLElBQWhCLENBQXFCc25CLFNBQXJCO0FBQWlDO0FBQUM7O0FBQzVELGdCQUFHNkIsZUFBZSxDQUFDeG1CLE1BQW5CLEVBQTBCO0FBQUM2bEIsMEJBQVksQ0FBQ3hvQixJQUFiLENBQWtCO0FBQUN1RCxvQkFBSSxFQUFDbUssR0FBTjtBQUFVNlosd0JBQVEsRUFBQzRCO0FBQW5CLGVBQWxCO0FBQXdEO0FBQUM7QUFBQztBQUFDOztBQUN0RnpiLFNBQUcsR0FBQyxJQUFKOztBQUFTLFVBQUd1YSxhQUFhLEdBQUNWLFNBQVEsQ0FBQzVrQixNQUExQixFQUFpQztBQUFDNmxCLG9CQUFZLENBQUN4b0IsSUFBYixDQUFrQjtBQUFDdUQsY0FBSSxFQUFDbUssR0FBTjtBQUFVNlosa0JBQVEsRUFBQ0EsU0FBUSxDQUFDN25CLEtBQVQsQ0FBZXVvQixhQUFmO0FBQW5CLFNBQWxCO0FBQXNFOztBQUNqSCxhQUFPTyxZQUFQO0FBQXFCLEtBekJSO0FBeUJTYyxXQUFPLEVBQUMsaUJBQVMva0IsSUFBVCxFQUFjZ2xCLElBQWQsRUFBbUI7QUFBQy9wQixZQUFNLENBQUNraEIsY0FBUCxDQUFzQnZlLE1BQU0sQ0FBQ3FuQixLQUFQLENBQWFobkIsU0FBbkMsRUFBNkMrQixJQUE3QyxFQUFrRDtBQUFDa2xCLGtCQUFVLEVBQUMsSUFBWjtBQUFpQjlJLG9CQUFZLEVBQUMsSUFBOUI7QUFBbUM5ZCxXQUFHLEVBQUNwQyxVQUFVLENBQUM4b0IsSUFBRCxDQUFWLEdBQWlCLFlBQVU7QUFBQyxjQUFHLEtBQUtHLGFBQVIsRUFBc0I7QUFBQyxtQkFBT0gsSUFBSSxDQUFDLEtBQUtHLGFBQU4sQ0FBWDtBQUFpQztBQUFDLFNBQXJGLEdBQXNGLFlBQVU7QUFBQyxjQUFHLEtBQUtBLGFBQVIsRUFBc0I7QUFBQyxtQkFBTyxLQUFLQSxhQUFMLENBQW1CbmxCLElBQW5CLENBQVA7QUFBaUM7QUFBQyxTQUFqTTtBQUFrTXFjLFdBQUcsRUFBQyxhQUFTcmEsS0FBVCxFQUFlO0FBQUMvRyxnQkFBTSxDQUFDa2hCLGNBQVAsQ0FBc0IsSUFBdEIsRUFBMkJuYyxJQUEzQixFQUFnQztBQUFDa2xCLHNCQUFVLEVBQUMsSUFBWjtBQUFpQjlJLHdCQUFZLEVBQUMsSUFBOUI7QUFBbUNnSixvQkFBUSxFQUFDLElBQTVDO0FBQWlEcGpCLGlCQUFLLEVBQUNBO0FBQXZELFdBQWhDO0FBQWdHO0FBQXRULE9BQWxEO0FBQTRXLEtBekJqWjtBQXlCa1praUIsT0FBRyxFQUFDLGFBQVNpQixhQUFULEVBQXVCO0FBQUMsYUFBT0EsYUFBYSxDQUFDdm5CLE1BQU0sQ0FBQzhDLE9BQVIsQ0FBYixHQUE4QnlrQixhQUE5QixHQUE0QyxJQUFJdm5CLE1BQU0sQ0FBQ3FuQixLQUFYLENBQWlCRSxhQUFqQixDQUFuRDtBQUFvRixLQXpCbGdCO0FBeUJtZ0JuTSxXQUFPLEVBQUM7QUFBQ3FNLFVBQUksRUFBQztBQUFDQyxnQkFBUSxFQUFDO0FBQVYsT0FBTjtBQUFzQkMsV0FBSyxFQUFDO0FBQUM1QixhQUFLLEVBQUMsZUFBU3JILElBQVQsRUFBYztBQUFDLGNBQUl6VCxFQUFFLEdBQUMsUUFBTXlULElBQWI7O0FBQWtCLGNBQUdxRCxjQUFjLENBQUM3WCxJQUFmLENBQW9CZSxFQUFFLENBQUN0TSxJQUF2QixLQUE4QnNNLEVBQUUsQ0FBQzBjLEtBQWpDLElBQXdDN2UsUUFBUSxDQUFDbUMsRUFBRCxFQUFJLE9BQUosQ0FBbkQsRUFBZ0U7QUFBQzJjLDBCQUFjLENBQUMzYyxFQUFELEVBQUksT0FBSixFQUFZbVosVUFBWixDQUFkO0FBQXVDOztBQUNwc0IsaUJBQU8sS0FBUDtBQUFjLFNBRHNpQjtBQUNyaUJ5RCxlQUFPLEVBQUMsaUJBQVNuSixJQUFULEVBQWM7QUFBQyxjQUFJelQsRUFBRSxHQUFDLFFBQU15VCxJQUFiOztBQUFrQixjQUFHcUQsY0FBYyxDQUFDN1gsSUFBZixDQUFvQmUsRUFBRSxDQUFDdE0sSUFBdkIsS0FBOEJzTSxFQUFFLENBQUMwYyxLQUFqQyxJQUF3QzdlLFFBQVEsQ0FBQ21DLEVBQUQsRUFBSSxPQUFKLENBQW5ELEVBQWdFO0FBQUMyYywwQkFBYyxDQUFDM2MsRUFBRCxFQUFJLE9BQUosQ0FBZDtBQUE0Qjs7QUFDckosaUJBQU8sSUFBUDtBQUFhLFNBRnVpQjtBQUV0aUI2WCxnQkFBUSxFQUFDLGtCQUFTK0IsS0FBVCxFQUFlO0FBQUMsY0FBSXJpQixNQUFNLEdBQUNxaUIsS0FBSyxDQUFDcmlCLE1BQWpCO0FBQXdCLGlCQUFPdWYsY0FBYyxDQUFDN1gsSUFBZixDQUFvQjFILE1BQU0sQ0FBQzdELElBQTNCLEtBQWtDNkQsTUFBTSxDQUFDbWxCLEtBQXpDLElBQWdEN2UsUUFBUSxDQUFDdEcsTUFBRCxFQUFRLE9BQVIsQ0FBeEQsSUFBMEVxYyxRQUFRLENBQUNuZSxHQUFULENBQWE4QixNQUFiLEVBQW9CLE9BQXBCLENBQTFFLElBQXdHc0csUUFBUSxDQUFDdEcsTUFBRCxFQUFRLEdBQVIsQ0FBdkg7QUFBcUk7QUFGZ1gsT0FBNUI7QUFFbFZzbEIsa0JBQVksRUFBQztBQUFDZixvQkFBWSxFQUFDLHNCQUFTbEMsS0FBVCxFQUFlO0FBQUMsY0FBR0EsS0FBSyxDQUFDalYsTUFBTixLQUFlL00sU0FBZixJQUEwQmdpQixLQUFLLENBQUMwQyxhQUFuQyxFQUFpRDtBQUFDMUMsaUJBQUssQ0FBQzBDLGFBQU4sQ0FBb0JRLFdBQXBCLEdBQWdDbEQsS0FBSyxDQUFDalYsTUFBdEM7QUFBOEM7QUFBQztBQUEvSDtBQUZxVTtBQXpCM2dCLEdBQWI7O0FBMkJzVixXQUFTZ1ksY0FBVCxDQUF3QjNjLEVBQXhCLEVBQTJCdE0sSUFBM0IsRUFBZ0MybEIsVUFBaEMsRUFBMkM7QUFBQyxRQUFHLENBQUNBLFVBQUosRUFBZTtBQUFDLFVBQUd6RixRQUFRLENBQUNuZSxHQUFULENBQWF1SyxFQUFiLEVBQWdCdE0sSUFBaEIsTUFBd0JrRSxTQUEzQixFQUFxQztBQUFDN0MsY0FBTSxDQUFDNmtCLEtBQVAsQ0FBYXROLEdBQWIsQ0FBaUJ0TSxFQUFqQixFQUFvQnRNLElBQXBCLEVBQXlCeWxCLFVBQXpCO0FBQXNDOztBQUM5ZDtBQUFROztBQUNSdkYsWUFBUSxDQUFDSixHQUFULENBQWF4VCxFQUFiLEVBQWdCdE0sSUFBaEIsRUFBcUIsS0FBckI7QUFBNEJxQixVQUFNLENBQUM2a0IsS0FBUCxDQUFhdE4sR0FBYixDQUFpQnRNLEVBQWpCLEVBQW9CdE0sSUFBcEIsRUFBeUI7QUFBQ3VOLGVBQVMsRUFBQyxLQUFYO0FBQWlCZCxhQUFPLEVBQUMsaUJBQVN5WixLQUFULEVBQWU7QUFBQyxZQUFJbUQsUUFBSjtBQUFBLFlBQWFwWSxNQUFiO0FBQUEsWUFBb0JxWSxLQUFLLEdBQUNwSixRQUFRLENBQUNuZSxHQUFULENBQWEsSUFBYixFQUFrQi9CLElBQWxCLENBQTFCOztBQUFrRCxZQUFJa21CLEtBQUssQ0FBQ3FELFNBQU4sR0FBZ0IsQ0FBakIsSUFBcUIsS0FBS3ZwQixJQUFMLENBQXhCLEVBQW1DO0FBQUMsY0FBRyxDQUFDc3BCLEtBQUssQ0FBQ3puQixNQUFWLEVBQWlCO0FBQUN5bkIsaUJBQUssR0FBQzFxQixNQUFLLENBQUNHLElBQU4sQ0FBVzJELFNBQVgsQ0FBTjtBQUE0QndkLG9CQUFRLENBQUNKLEdBQVQsQ0FBYSxJQUFiLEVBQWtCOWYsSUFBbEIsRUFBdUJzcEIsS0FBdkI7QUFBOEJELG9CQUFRLEdBQUMxRCxVQUFVLENBQUMsSUFBRCxFQUFNM2xCLElBQU4sQ0FBbkI7QUFBK0IsaUJBQUtBLElBQUw7QUFBYWlSLGtCQUFNLEdBQUNpUCxRQUFRLENBQUNuZSxHQUFULENBQWEsSUFBYixFQUFrQi9CLElBQWxCLENBQVA7O0FBQStCLGdCQUFHc3BCLEtBQUssS0FBR3JZLE1BQVIsSUFBZ0JvWSxRQUFuQixFQUE0QjtBQUFDbkosc0JBQVEsQ0FBQ0osR0FBVCxDQUFhLElBQWIsRUFBa0I5ZixJQUFsQixFQUF1QixLQUF2QjtBQUErQixhQUE1RCxNQUFnRTtBQUFDaVIsb0JBQU0sR0FBQyxFQUFQO0FBQVc7O0FBQ3ZaLGdCQUFHcVksS0FBSyxLQUFHclksTUFBWCxFQUFrQjtBQUFDaVYsbUJBQUssQ0FBQ3NELHdCQUFOO0FBQWlDdEQsbUJBQUssQ0FBQ2dDLGNBQU47QUFBdUIscUJBQU9qWCxNQUFNLENBQUN4TCxLQUFkO0FBQXFCO0FBQUMsV0FEbUYsTUFDOUUsSUFBRyxDQUFDcEUsTUFBTSxDQUFDNmtCLEtBQVAsQ0FBYXpKLE9BQWIsQ0FBcUJ6YyxJQUFyQixLQUE0QixFQUE3QixFQUFpQ2luQixZQUFwQyxFQUFpRDtBQUFDZixpQkFBSyxDQUFDaUMsZUFBTjtBQUF5QjtBQUFDLFNBRGxDLE1BQ3VDLElBQUdtQixLQUFLLENBQUN6bkIsTUFBVCxFQUFnQjtBQUFDcWUsa0JBQVEsQ0FBQ0osR0FBVCxDQUFhLElBQWIsRUFBa0I5ZixJQUFsQixFQUF1QjtBQUFDeUYsaUJBQUssRUFBQ3BFLE1BQU0sQ0FBQzZrQixLQUFQLENBQWFnRCxPQUFiLENBQXFCN25CLE1BQU0sQ0FBQ2tDLE1BQVAsQ0FBYytsQixLQUFLLENBQUMsQ0FBRCxDQUFuQixFQUF1QmpvQixNQUFNLENBQUNxbkIsS0FBUCxDQUFhaG5CLFNBQXBDLENBQXJCLEVBQW9FNG5CLEtBQUssQ0FBQzFxQixLQUFOLENBQVksQ0FBWixDQUFwRSxFQUFtRixJQUFuRjtBQUFQLFdBQXZCO0FBQXlIc25CLGVBQUssQ0FBQ3NELHdCQUFOO0FBQWtDO0FBQUM7QUFEL1MsS0FBekI7QUFDNFU7O0FBQ3hXbm9CLFFBQU0sQ0FBQ21tQixXQUFQLEdBQW1CLFVBQVMva0IsSUFBVCxFQUFjekMsSUFBZCxFQUFtQjhtQixNQUFuQixFQUEwQjtBQUFDLFFBQUdya0IsSUFBSSxDQUFDOGIsbUJBQVIsRUFBNEI7QUFBQzliLFVBQUksQ0FBQzhiLG1CQUFMLENBQXlCdmUsSUFBekIsRUFBOEI4bUIsTUFBOUI7QUFBdUM7QUFBQyxHQUFuSDs7QUFBb0h6bEIsUUFBTSxDQUFDcW5CLEtBQVAsR0FBYSxVQUFTem9CLEdBQVQsRUFBYXdwQixLQUFiLEVBQW1CO0FBQUMsUUFBRyxFQUFFLGdCQUFnQnBvQixNQUFNLENBQUNxbkIsS0FBekIsQ0FBSCxFQUFtQztBQUFDLGFBQU8sSUFBSXJuQixNQUFNLENBQUNxbkIsS0FBWCxDQUFpQnpvQixHQUFqQixFQUFxQndwQixLQUFyQixDQUFQO0FBQW9DOztBQUM3TixRQUFHeHBCLEdBQUcsSUFBRUEsR0FBRyxDQUFDRCxJQUFaLEVBQWlCO0FBQUMsV0FBSzRvQixhQUFMLEdBQW1CM29CLEdBQW5CO0FBQXVCLFdBQUtELElBQUwsR0FBVUMsR0FBRyxDQUFDRCxJQUFkO0FBQW1CLFdBQUswcEIsa0JBQUwsR0FBd0J6cEIsR0FBRyxDQUFDMHBCLGdCQUFKLElBQXNCMXBCLEdBQUcsQ0FBQzBwQixnQkFBSixLQUF1QnpsQixTQUF2QixJQUFrQ2pFLEdBQUcsQ0FBQ21wQixXQUFKLEtBQWtCLEtBQTFFLEdBQWdGM0QsVUFBaEYsR0FBMkZDLFdBQW5IO0FBQStILFdBQUs3aEIsTUFBTCxHQUFhNUQsR0FBRyxDQUFDNEQsTUFBSixJQUFZNUQsR0FBRyxDQUFDNEQsTUFBSixDQUFXaEUsUUFBWCxLQUFzQixDQUFuQyxHQUFzQ0ksR0FBRyxDQUFDNEQsTUFBSixDQUFXNUMsVUFBakQsR0FBNERoQixHQUFHLENBQUM0RCxNQUE1RTtBQUFtRixXQUFLa2tCLGFBQUwsR0FBbUI5bkIsR0FBRyxDQUFDOG5CLGFBQXZCO0FBQXFDLFdBQUs2QixhQUFMLEdBQW1CM3BCLEdBQUcsQ0FBQzJwQixhQUF2QjtBQUFzQyxLQUF6VixNQUE2VjtBQUFDLFdBQUs1cEIsSUFBTCxHQUFVQyxHQUFWO0FBQWU7O0FBQzdXLFFBQUd3cEIsS0FBSCxFQUFTO0FBQUNwb0IsWUFBTSxDQUFDa0MsTUFBUCxDQUFjLElBQWQsRUFBbUJrbUIsS0FBbkI7QUFBMkI7O0FBQ3JDLFNBQUtJLFNBQUwsR0FBZTVwQixHQUFHLElBQUVBLEdBQUcsQ0FBQzRwQixTQUFULElBQW9CN2lCLElBQUksQ0FBQzhpQixHQUFMLEVBQW5DO0FBQThDLFNBQUt6b0IsTUFBTSxDQUFDOEMsT0FBWixJQUFxQixJQUFyQjtBQUEyQixHQUgyQzs7QUFHMUM5QyxRQUFNLENBQUNxbkIsS0FBUCxDQUFhaG5CLFNBQWIsR0FBdUI7QUFBQ0UsZUFBVyxFQUFDUCxNQUFNLENBQUNxbkIsS0FBcEI7QUFBMEJnQixzQkFBa0IsRUFBQ2hFLFdBQTdDO0FBQXlEb0Msd0JBQW9CLEVBQUNwQyxXQUE5RTtBQUEwRnNDLGlDQUE2QixFQUFDdEMsV0FBeEg7QUFBb0lxRSxlQUFXLEVBQUMsS0FBaEo7QUFBc0o3QixrQkFBYyxFQUFDLDBCQUFVO0FBQUMsVUFBSTNkLENBQUMsR0FBQyxLQUFLcWUsYUFBWDtBQUF5QixXQUFLYyxrQkFBTCxHQUF3QmpFLFVBQXhCOztBQUFtQyxVQUFHbGIsQ0FBQyxJQUFFLENBQUMsS0FBS3dmLFdBQVosRUFBd0I7QUFBQ3hmLFNBQUMsQ0FBQzJkLGNBQUY7QUFBb0I7QUFBQyxLQUExUjtBQUEyUkMsbUJBQWUsRUFBQywyQkFBVTtBQUFDLFVBQUk1ZCxDQUFDLEdBQUMsS0FBS3FlLGFBQVg7QUFBeUIsV0FBS2Qsb0JBQUwsR0FBMEJyQyxVQUExQjs7QUFBcUMsVUFBR2xiLENBQUMsSUFBRSxDQUFDLEtBQUt3ZixXQUFaLEVBQXdCO0FBQUN4ZixTQUFDLENBQUM0ZCxlQUFGO0FBQXFCO0FBQUMsS0FBbmE7QUFBb2FxQiw0QkFBd0IsRUFBQyxvQ0FBVTtBQUFDLFVBQUlqZixDQUFDLEdBQUMsS0FBS3FlLGFBQVg7QUFBeUIsV0FBS1osNkJBQUwsR0FBbUN2QyxVQUFuQzs7QUFBOEMsVUFBR2xiLENBQUMsSUFBRSxDQUFDLEtBQUt3ZixXQUFaLEVBQXdCO0FBQUN4ZixTQUFDLENBQUNpZix3QkFBRjtBQUE4Qjs7QUFDdnFCLFdBQUtyQixlQUFMO0FBQXdCO0FBRHlFLEdBQXZCO0FBQ2hEOW1CLFFBQU0sQ0FBQ2lCLElBQVAsQ0FBWTtBQUFDMG5CLFVBQU0sRUFBQyxJQUFSO0FBQWFDLFdBQU8sRUFBQyxJQUFyQjtBQUEwQkMsY0FBVSxFQUFDLElBQXJDO0FBQTBDQyxrQkFBYyxFQUFDLElBQXpEO0FBQThEQyxXQUFPLEVBQUMsSUFBdEU7QUFBMkVDLFVBQU0sRUFBQyxJQUFsRjtBQUF1RkMsY0FBVSxFQUFDLElBQWxHO0FBQXVHQyxXQUFPLEVBQUMsSUFBL0c7QUFBb0hDLFNBQUssRUFBQyxJQUExSDtBQUErSEMsU0FBSyxFQUFDLElBQXJJO0FBQTBJQyxZQUFRLEVBQUMsSUFBbko7QUFBd0pDLFFBQUksRUFBQyxJQUE3SjtBQUFrSyxZQUFPLElBQXpLO0FBQThLdHFCLFFBQUksRUFBQyxJQUFuTDtBQUF3THVxQixZQUFRLEVBQUMsSUFBak07QUFBc00zZSxPQUFHLEVBQUMsSUFBMU07QUFBK000ZSxXQUFPLEVBQUMsSUFBdk47QUFBNE50QyxVQUFNLEVBQUMsSUFBbk87QUFBd091QyxXQUFPLEVBQUMsSUFBaFA7QUFBcVBDLFdBQU8sRUFBQyxJQUE3UDtBQUFrUUMsV0FBTyxFQUFDLElBQTFRO0FBQStRQyxXQUFPLEVBQUMsSUFBdlI7QUFBNFJDLFdBQU8sRUFBQyxJQUFwUztBQUF5U0MsYUFBUyxFQUFDLElBQW5UO0FBQXdUQyxlQUFXLEVBQUMsSUFBcFU7QUFBeVVDLFdBQU8sRUFBQyxJQUFqVjtBQUFzVkMsV0FBTyxFQUFDLElBQTlWO0FBQW1XQyxpQkFBYSxFQUFDLElBQWpYO0FBQXNYQyxhQUFTLEVBQUMsSUFBaFk7QUFBcVlDLFdBQU8sRUFBQyxJQUE3WTtBQUFrWkMsU0FBSyxFQUFDLGVBQVN4RixLQUFULEVBQWU7QUFBQyxVQUFJcUMsTUFBTSxHQUFDckMsS0FBSyxDQUFDcUMsTUFBakI7O0FBQXdCLFVBQUdyQyxLQUFLLENBQUN3RixLQUFOLElBQWEsSUFBYixJQUFtQnBHLFNBQVMsQ0FBQy9aLElBQVYsQ0FBZTJhLEtBQUssQ0FBQ2xtQixJQUFyQixDQUF0QixFQUFpRDtBQUFDLGVBQU9rbUIsS0FBSyxDQUFDMEUsUUFBTixJQUFnQixJQUFoQixHQUFxQjFFLEtBQUssQ0FBQzBFLFFBQTNCLEdBQW9DMUUsS0FBSyxDQUFDMkUsT0FBakQ7QUFBMEQ7O0FBQ2xsQixVQUFHLENBQUMzRSxLQUFLLENBQUN3RixLQUFQLElBQWNuRCxNQUFNLEtBQUdya0IsU0FBdkIsSUFBa0NxaEIsV0FBVyxDQUFDaGEsSUFBWixDQUFpQjJhLEtBQUssQ0FBQ2xtQixJQUF2QixDQUFyQyxFQUFrRTtBQUFDLFlBQUd1b0IsTUFBTSxHQUFDLENBQVYsRUFBWTtBQUFDLGlCQUFPLENBQVA7QUFBVTs7QUFDMUYsWUFBR0EsTUFBTSxHQUFDLENBQVYsRUFBWTtBQUFDLGlCQUFPLENBQVA7QUFBVTs7QUFDdkIsWUFBR0EsTUFBTSxHQUFDLENBQVYsRUFBWTtBQUFDLGlCQUFPLENBQVA7QUFBVTs7QUFDdkIsZUFBTyxDQUFQO0FBQVU7O0FBQ1YsYUFBT3JDLEtBQUssQ0FBQ3dGLEtBQWI7QUFBb0I7QUFMa0IsR0FBWixFQUtKcnFCLE1BQU0sQ0FBQzZrQixLQUFQLENBQWFzQyxPQUxUO0FBS2tCbm5CLFFBQU0sQ0FBQ2lCLElBQVAsQ0FBWTtBQUFDcXBCLFNBQUssRUFBQyxTQUFQO0FBQWlCQyxRQUFJLEVBQUM7QUFBdEIsR0FBWixFQUE4QyxVQUFTNXJCLElBQVQsRUFBY2luQixZQUFkLEVBQTJCO0FBQUM1bEIsVUFBTSxDQUFDNmtCLEtBQVAsQ0FBYXpKLE9BQWIsQ0FBcUJ6YyxJQUFyQixJQUEyQjtBQUFDb25CLFdBQUssRUFBQyxpQkFBVTtBQUFDNkIsc0JBQWMsQ0FBQyxJQUFELEVBQU1qcEIsSUFBTixFQUFXMmxCLFVBQVgsQ0FBZDtBQUFxQyxlQUFPLEtBQVA7QUFBYyxPQUFyRTtBQUFzRXVELGFBQU8sRUFBQyxtQkFBVTtBQUFDRCxzQkFBYyxDQUFDLElBQUQsRUFBTWpwQixJQUFOLENBQWQ7QUFBMEIsZUFBTyxJQUFQO0FBQWEsT0FBaEk7QUFBaUlpbkIsa0JBQVksRUFBQ0E7QUFBOUksS0FBM0I7QUFBd0wsR0FBbFE7QUFBb1E1bEIsUUFBTSxDQUFDaUIsSUFBUCxDQUFZO0FBQUN1cEIsY0FBVSxFQUFDLFdBQVo7QUFBd0JDLGNBQVUsRUFBQyxVQUFuQztBQUE4Q0MsZ0JBQVksRUFBQyxhQUEzRDtBQUF5RUMsZ0JBQVksRUFBQztBQUF0RixHQUFaLEVBQWdILFVBQVNDLElBQVQsRUFBY3RFLEdBQWQsRUFBa0I7QUFBQ3RtQixVQUFNLENBQUM2a0IsS0FBUCxDQUFhekosT0FBYixDQUFxQndQLElBQXJCLElBQTJCO0FBQUNoRixrQkFBWSxFQUFDVSxHQUFkO0FBQWtCVCxjQUFRLEVBQUNTLEdBQTNCO0FBQStCYixZQUFNLEVBQUMsZ0JBQVNaLEtBQVQsRUFBZTtBQUFDLFlBQUkvakIsR0FBSjtBQUFBLFlBQVEwQixNQUFNLEdBQUMsSUFBZjtBQUFBLFlBQW9CcW9CLE9BQU8sR0FBQ2hHLEtBQUssQ0FBQzBELGFBQWxDO0FBQUEsWUFBZ0RwRCxTQUFTLEdBQUNOLEtBQUssQ0FBQ00sU0FBaEU7O0FBQTBFLFlBQUcsQ0FBQzBGLE9BQUQsSUFBV0EsT0FBTyxLQUFHcm9CLE1BQVYsSUFBa0IsQ0FBQ3hDLE1BQU0sQ0FBQzBGLFFBQVAsQ0FBZ0JsRCxNQUFoQixFQUF1QnFvQixPQUF2QixDQUFqQyxFQUFrRTtBQUFDaEcsZUFBSyxDQUFDbG1CLElBQU4sR0FBV3dtQixTQUFTLENBQUNHLFFBQXJCO0FBQThCeGtCLGFBQUcsR0FBQ3FrQixTQUFTLENBQUMvWixPQUFWLENBQWtCeE4sS0FBbEIsQ0FBd0IsSUFBeEIsRUFBNkJ5RCxTQUE3QixDQUFKO0FBQTRDd2pCLGVBQUssQ0FBQ2xtQixJQUFOLEdBQVcybkIsR0FBWDtBQUFnQjs7QUFDM3VCLGVBQU94bEIsR0FBUDtBQUFZO0FBRGtjLEtBQTNCO0FBQ3BhLEdBRGlTO0FBQy9SZCxRQUFNLENBQUNHLEVBQVAsQ0FBVStCLE1BQVYsQ0FBaUI7QUFBQ3VpQixNQUFFLEVBQUMsWUFBU0MsS0FBVCxFQUFlemtCLFFBQWYsRUFBd0J5ZSxJQUF4QixFQUE2QnZlLEVBQTdCLEVBQWdDO0FBQUMsYUFBT3NrQixHQUFFLENBQUMsSUFBRCxFQUFNQyxLQUFOLEVBQVl6a0IsUUFBWixFQUFxQnllLElBQXJCLEVBQTBCdmUsRUFBMUIsQ0FBVDtBQUF3QyxLQUE3RTtBQUE4RXdrQixPQUFHLEVBQUMsYUFBU0QsS0FBVCxFQUFlemtCLFFBQWYsRUFBd0J5ZSxJQUF4QixFQUE2QnZlLEVBQTdCLEVBQWdDO0FBQUMsYUFBT3NrQixHQUFFLENBQUMsSUFBRCxFQUFNQyxLQUFOLEVBQVl6a0IsUUFBWixFQUFxQnllLElBQXJCLEVBQTBCdmUsRUFBMUIsRUFBNkIsQ0FBN0IsQ0FBVDtBQUEwQyxLQUE3SjtBQUE4SjJrQixPQUFHLEVBQUMsYUFBU0osS0FBVCxFQUFlemtCLFFBQWYsRUFBd0JFLEVBQXhCLEVBQTJCO0FBQUMsVUFBSWdsQixTQUFKLEVBQWN4bUIsSUFBZDs7QUFBbUIsVUFBRytsQixLQUFLLElBQUVBLEtBQUssQ0FBQ21DLGNBQWIsSUFBNkJuQyxLQUFLLENBQUNTLFNBQXRDLEVBQWdEO0FBQUNBLGlCQUFTLEdBQUNULEtBQUssQ0FBQ1MsU0FBaEI7QUFBMEJubEIsY0FBTSxDQUFDMGtCLEtBQUssQ0FBQzZCLGNBQVAsQ0FBTixDQUE2QnpCLEdBQTdCLENBQWlDSyxTQUFTLENBQUNqWixTQUFWLEdBQW9CaVosU0FBUyxDQUFDRyxRQUFWLEdBQW1CLEdBQW5CLEdBQXVCSCxTQUFTLENBQUNqWixTQUFyRCxHQUErRGlaLFNBQVMsQ0FBQ0csUUFBMUcsRUFBbUhILFNBQVMsQ0FBQ2xsQixRQUE3SCxFQUFzSWtsQixTQUFTLENBQUMvWixPQUFoSjtBQUF5SixlQUFPLElBQVA7QUFBYTs7QUFDcGUsVUFBRyxRQUFPc1osS0FBUCxNQUFlLFFBQWxCLEVBQTJCO0FBQUMsYUFBSS9sQixJQUFKLElBQVkrbEIsS0FBWixFQUFrQjtBQUFDLGVBQUtJLEdBQUwsQ0FBU25tQixJQUFULEVBQWNzQixRQUFkLEVBQXVCeWtCLEtBQUssQ0FBQy9sQixJQUFELENBQTVCO0FBQXFDOztBQUNwRixlQUFPLElBQVA7QUFBYTs7QUFDYixVQUFHc0IsUUFBUSxLQUFHLEtBQVgsSUFBa0IsT0FBT0EsUUFBUCxLQUFrQixVQUF2QyxFQUFrRDtBQUFDRSxVQUFFLEdBQUNGLFFBQUg7QUFBWUEsZ0JBQVEsR0FBQzRDLFNBQVQ7QUFBb0I7O0FBQ25GLFVBQUcxQyxFQUFFLEtBQUcsS0FBUixFQUFjO0FBQUNBLFVBQUUsR0FBQ2trQixXQUFIO0FBQWdCOztBQUMvQixhQUFPLEtBQUtwakIsSUFBTCxDQUFVLFlBQVU7QUFBQ2pCLGNBQU0sQ0FBQzZrQixLQUFQLENBQWE1TCxNQUFiLENBQW9CLElBQXBCLEVBQXlCeUwsS0FBekIsRUFBK0J2a0IsRUFBL0IsRUFBa0NGLFFBQWxDO0FBQTZDLE9BQWxFLENBQVA7QUFBNEU7QUFMMUMsR0FBakI7QUFLOEQsTUFDL0U2cUIsWUFBWSxHQUFDLHVCQURrRTtBQUFBLE1BQzFDQyxRQUFRLEdBQUMsbUNBRGlDO0FBQUEsTUFDR0MsWUFBWSxHQUFDLDBDQURoQjs7QUFDMkQsV0FBU0Msa0JBQVQsQ0FBNEI3cEIsSUFBNUIsRUFBaUM0VyxPQUFqQyxFQUF5QztBQUFDLFFBQUdsUCxRQUFRLENBQUMxSCxJQUFELEVBQU0sT0FBTixDQUFSLElBQXdCMEgsUUFBUSxDQUFDa1AsT0FBTyxDQUFDeFosUUFBUixLQUFtQixFQUFuQixHQUFzQndaLE9BQXRCLEdBQThCQSxPQUFPLENBQUNoSixVQUF2QyxFQUFrRCxJQUFsRCxDQUFuQyxFQUEyRjtBQUFDLGFBQU9oUCxNQUFNLENBQUNvQixJQUFELENBQU4sQ0FBYTBWLFFBQWIsQ0FBc0IsT0FBdEIsRUFBK0IsQ0FBL0IsS0FBbUMxVixJQUExQztBQUFnRDs7QUFDaFUsV0FBT0EsSUFBUDtBQUFhOztBQUNiLFdBQVM4cEIsYUFBVCxDQUF1QjlwQixJQUF2QixFQUE0QjtBQUFDQSxRQUFJLENBQUN6QyxJQUFMLEdBQVUsQ0FBQ3lDLElBQUksQ0FBQzVCLFlBQUwsQ0FBa0IsTUFBbEIsTUFBNEIsSUFBN0IsSUFBbUMsR0FBbkMsR0FBdUM0QixJQUFJLENBQUN6QyxJQUF0RDtBQUEyRCxXQUFPeUMsSUFBUDtBQUFhOztBQUNyRyxXQUFTK3BCLGFBQVQsQ0FBdUIvcEIsSUFBdkIsRUFBNEI7QUFBQyxRQUFHLENBQUNBLElBQUksQ0FBQ3pDLElBQUwsSUFBVyxFQUFaLEVBQWdCcEIsS0FBaEIsQ0FBc0IsQ0FBdEIsRUFBd0IsQ0FBeEIsTUFBNkIsT0FBaEMsRUFBd0M7QUFBQzZELFVBQUksQ0FBQ3pDLElBQUwsR0FBVXlDLElBQUksQ0FBQ3pDLElBQUwsQ0FBVXBCLEtBQVYsQ0FBZ0IsQ0FBaEIsQ0FBVjtBQUE4QixLQUF2RSxNQUEyRTtBQUFDNkQsVUFBSSxDQUFDcUosZUFBTCxDQUFxQixNQUFyQjtBQUE4Qjs7QUFDdkksV0FBT3JKLElBQVA7QUFBYTs7QUFDYixXQUFTZ3FCLGNBQVQsQ0FBd0J4c0IsR0FBeEIsRUFBNEJ5c0IsSUFBNUIsRUFBaUM7QUFBQyxRQUFJbHNCLENBQUosRUFBTWdZLENBQU4sRUFBUXhZLElBQVIsRUFBYTJzQixRQUFiLEVBQXNCQyxRQUF0QixFQUErQkMsUUFBL0IsRUFBd0N2RyxNQUF4Qzs7QUFBK0MsUUFBR29HLElBQUksQ0FBQzdzQixRQUFMLEtBQWdCLENBQW5CLEVBQXFCO0FBQUM7QUFBUTs7QUFDL0csUUFBR3FnQixRQUFRLENBQUNELE9BQVQsQ0FBaUJoZ0IsR0FBakIsQ0FBSCxFQUF5QjtBQUFDMHNCLGNBQVEsR0FBQ3pNLFFBQVEsQ0FBQ25lLEdBQVQsQ0FBYTlCLEdBQWIsQ0FBVDtBQUEyQnFtQixZQUFNLEdBQUNxRyxRQUFRLENBQUNyRyxNQUFoQjs7QUFBdUIsVUFBR0EsTUFBSCxFQUFVO0FBQUNwRyxnQkFBUSxDQUFDNUYsTUFBVCxDQUFnQm9TLElBQWhCLEVBQXFCLGVBQXJCOztBQUFzQyxhQUFJMXNCLElBQUosSUFBWXNtQixNQUFaLEVBQW1CO0FBQUMsZUFBSTlsQixDQUFDLEdBQUMsQ0FBRixFQUFJZ1ksQ0FBQyxHQUFDOE4sTUFBTSxDQUFDdG1CLElBQUQsQ0FBTixDQUFhNkIsTUFBdkIsRUFBOEJyQixDQUFDLEdBQUNnWSxDQUFoQyxFQUFrQ2hZLENBQUMsRUFBbkMsRUFBc0M7QUFBQ2Esa0JBQU0sQ0FBQzZrQixLQUFQLENBQWF0TixHQUFiLENBQWlCOFQsSUFBakIsRUFBc0Ixc0IsSUFBdEIsRUFBMkJzbUIsTUFBTSxDQUFDdG1CLElBQUQsQ0FBTixDQUFhUSxDQUFiLENBQTNCO0FBQTZDO0FBQUM7QUFBQztBQUFDOztBQUN4TyxRQUFHMmYsUUFBUSxDQUFDRixPQUFULENBQWlCaGdCLEdBQWpCLENBQUgsRUFBeUI7QUFBQzJzQixjQUFRLEdBQUN6TSxRQUFRLENBQUN6QixNQUFULENBQWdCemUsR0FBaEIsQ0FBVDtBQUE4QjRzQixjQUFRLEdBQUN4ckIsTUFBTSxDQUFDa0MsTUFBUCxDQUFjLEVBQWQsRUFBaUJxcEIsUUFBakIsQ0FBVDtBQUFvQ3pNLGNBQVEsQ0FBQ0wsR0FBVCxDQUFhNE0sSUFBYixFQUFrQkcsUUFBbEI7QUFBNkI7QUFBQzs7QUFDMUgsV0FBU0MsUUFBVCxDQUFrQjdzQixHQUFsQixFQUFzQnlzQixJQUF0QixFQUEyQjtBQUFDLFFBQUl2aUIsUUFBUSxHQUFDdWlCLElBQUksQ0FBQ3ZpQixRQUFMLENBQWNwRSxXQUFkLEVBQWI7O0FBQXlDLFFBQUdvRSxRQUFRLEtBQUcsT0FBWCxJQUFvQmlaLGNBQWMsQ0FBQzdYLElBQWYsQ0FBb0J0TCxHQUFHLENBQUNELElBQXhCLENBQXZCLEVBQXFEO0FBQUMwc0IsVUFBSSxDQUFDMVosT0FBTCxHQUFhL1MsR0FBRyxDQUFDK1MsT0FBakI7QUFBMEIsS0FBaEYsTUFBcUYsSUFBRzdJLFFBQVEsS0FBRyxPQUFYLElBQW9CQSxRQUFRLEtBQUcsVUFBbEMsRUFBNkM7QUFBQ3VpQixVQUFJLENBQUM3VixZQUFMLEdBQWtCNVcsR0FBRyxDQUFDNFcsWUFBdEI7QUFBb0M7QUFBQzs7QUFDN08sV0FBU2tXLFFBQVQsQ0FBa0JDLFVBQWxCLEVBQTZCOWEsSUFBN0IsRUFBa0MzUCxRQUFsQyxFQUEyQ3lpQixPQUEzQyxFQUFtRDtBQUFDOVMsUUFBSSxHQUFDclQsSUFBSSxDQUFDcVQsSUFBRCxDQUFUO0FBQWdCLFFBQUlxUixRQUFKO0FBQUEsUUFBYTVnQixLQUFiO0FBQUEsUUFBbUJtaUIsT0FBbkI7QUFBQSxRQUEyQm1JLFVBQTNCO0FBQUEsUUFBc0Mzc0IsSUFBdEM7QUFBQSxRQUEyQ0MsR0FBM0M7QUFBQSxRQUErQ0MsQ0FBQyxHQUFDLENBQWpEO0FBQUEsUUFBbURnWSxDQUFDLEdBQUN3VSxVQUFVLENBQUNuckIsTUFBaEU7QUFBQSxRQUF1RXFyQixRQUFRLEdBQUMxVSxDQUFDLEdBQUMsQ0FBbEY7QUFBQSxRQUFvRi9TLEtBQUssR0FBQ3lNLElBQUksQ0FBQyxDQUFELENBQTlGO0FBQUEsUUFBa0dpYixlQUFlLEdBQUN4dEIsVUFBVSxDQUFDOEYsS0FBRCxDQUE1SDs7QUFBb0ksUUFBRzBuQixlQUFlLElBQUczVSxDQUFDLEdBQUMsQ0FBRixJQUFLLE9BQU8vUyxLQUFQLEtBQWUsUUFBcEIsSUFBOEIsQ0FBQy9GLE9BQU8sQ0FBQ2drQixVQUF2QyxJQUFtRDBJLFFBQVEsQ0FBQzdnQixJQUFULENBQWM5RixLQUFkLENBQXhFLEVBQThGO0FBQUMsYUFBT3VuQixVQUFVLENBQUMxcUIsSUFBWCxDQUFnQixVQUFTb1csS0FBVCxFQUFlO0FBQUMsWUFBSWQsSUFBSSxHQUFDb1YsVUFBVSxDQUFDcHFCLEVBQVgsQ0FBYzhWLEtBQWQsQ0FBVDs7QUFBOEIsWUFBR3lVLGVBQUgsRUFBbUI7QUFBQ2piLGNBQUksQ0FBQyxDQUFELENBQUosR0FBUXpNLEtBQUssQ0FBQzFHLElBQU4sQ0FBVyxJQUFYLEVBQWdCMlosS0FBaEIsRUFBc0JkLElBQUksQ0FBQ3dWLElBQUwsRUFBdEIsQ0FBUjtBQUE0Qzs7QUFDNWFMLGdCQUFRLENBQUNuVixJQUFELEVBQU0xRixJQUFOLEVBQVczUCxRQUFYLEVBQW9CeWlCLE9BQXBCLENBQVI7QUFBc0MsT0FEd1EsQ0FBUDtBQUM5UDs7QUFDekMsUUFBR3hNLENBQUgsRUFBSztBQUFDK0ssY0FBUSxHQUFDc0IsYUFBYSxDQUFDM1MsSUFBRCxFQUFNOGEsVUFBVSxDQUFDLENBQUQsQ0FBVixDQUFjaGlCLGFBQXBCLEVBQWtDLEtBQWxDLEVBQXdDZ2lCLFVBQXhDLEVBQW1EaEksT0FBbkQsQ0FBdEI7QUFBa0ZyaUIsV0FBSyxHQUFDNGdCLFFBQVEsQ0FBQ2xULFVBQWY7O0FBQTBCLFVBQUdrVCxRQUFRLENBQUNqWixVQUFULENBQW9CekksTUFBcEIsS0FBNkIsQ0FBaEMsRUFBa0M7QUFBQzBoQixnQkFBUSxHQUFDNWdCLEtBQVQ7QUFBZ0I7O0FBQ3JLLFVBQUdBLEtBQUssSUFBRXFpQixPQUFWLEVBQWtCO0FBQUNGLGVBQU8sR0FBQ3pqQixNQUFNLENBQUNtQixHQUFQLENBQVdraUIsTUFBTSxDQUFDbkIsUUFBRCxFQUFVLFFBQVYsQ0FBakIsRUFBcUNnSixhQUFyQyxDQUFSO0FBQTREVSxrQkFBVSxHQUFDbkksT0FBTyxDQUFDampCLE1BQW5COztBQUEwQixlQUFLckIsQ0FBQyxHQUFDZ1ksQ0FBUCxFQUFTaFksQ0FBQyxFQUFWLEVBQWE7QUFBQ0YsY0FBSSxHQUFDaWpCLFFBQUw7O0FBQWMsY0FBRy9pQixDQUFDLEtBQUcwc0IsUUFBUCxFQUFnQjtBQUFDNXNCLGdCQUFJLEdBQUNlLE1BQU0sQ0FBQ3VDLEtBQVAsQ0FBYXRELElBQWIsRUFBa0IsSUFBbEIsRUFBdUIsSUFBdkIsQ0FBTDs7QUFBa0MsZ0JBQUcyc0IsVUFBSCxFQUFjO0FBQUM1ckIsb0JBQU0sQ0FBQ2UsS0FBUCxDQUFhMGlCLE9BQWIsRUFBcUJKLE1BQU0sQ0FBQ3BrQixJQUFELEVBQU0sUUFBTixDQUEzQjtBQUE2QztBQUFDOztBQUNyUGlDLGtCQUFRLENBQUN4RCxJQUFULENBQWNpdUIsVUFBVSxDQUFDeHNCLENBQUQsQ0FBeEIsRUFBNEJGLElBQTVCLEVBQWlDRSxDQUFqQztBQUFxQzs7QUFDckMsWUFBR3lzQixVQUFILEVBQWM7QUFBQzFzQixhQUFHLEdBQUN1a0IsT0FBTyxDQUFDQSxPQUFPLENBQUNqakIsTUFBUixHQUFlLENBQWhCLENBQVAsQ0FBMEJtSixhQUE5QjtBQUE0QzNKLGdCQUFNLENBQUNtQixHQUFQLENBQVdzaUIsT0FBWCxFQUFtQjBILGFBQW5COztBQUFrQyxlQUFJaHNCLENBQUMsR0FBQyxDQUFOLEVBQVFBLENBQUMsR0FBQ3lzQixVQUFWLEVBQXFCenNCLENBQUMsRUFBdEIsRUFBeUI7QUFBQ0YsZ0JBQUksR0FBQ3drQixPQUFPLENBQUN0a0IsQ0FBRCxDQUFaOztBQUFnQixnQkFBRzhpQixXQUFXLENBQUMvWCxJQUFaLENBQWlCakwsSUFBSSxDQUFDTixJQUFMLElBQVcsRUFBNUIsS0FBaUMsQ0FBQ2tnQixRQUFRLENBQUN4QixNQUFULENBQWdCcGUsSUFBaEIsRUFBcUIsWUFBckIsQ0FBbEMsSUFBc0VlLE1BQU0sQ0FBQzBGLFFBQVAsQ0FBZ0J4RyxHQUFoQixFQUFvQkQsSUFBcEIsQ0FBekUsRUFBbUc7QUFBQyxrQkFBR0EsSUFBSSxDQUFDTCxHQUFMLElBQVUsQ0FBQ0ssSUFBSSxDQUFDTixJQUFMLElBQVcsRUFBWixFQUFnQitGLFdBQWhCLE9BQWdDLFFBQTdDLEVBQXNEO0FBQUMsb0JBQUcxRSxNQUFNLENBQUNnc0IsUUFBUCxJQUFpQixDQUFDL3NCLElBQUksQ0FBQ0gsUUFBMUIsRUFBbUM7QUFBQ2tCLHdCQUFNLENBQUNnc0IsUUFBUCxDQUFnQi9zQixJQUFJLENBQUNMLEdBQXJCLEVBQXlCO0FBQUNDLHlCQUFLLEVBQUNJLElBQUksQ0FBQ0osS0FBTCxJQUFZSSxJQUFJLENBQUNPLFlBQUwsQ0FBa0IsT0FBbEI7QUFBbkIsbUJBQXpCLEVBQXdFTixHQUF4RTtBQUE4RTtBQUFDLGVBQTFLLE1BQThLO0FBQUNILHVCQUFPLENBQUNFLElBQUksQ0FBQzhQLFdBQUwsQ0FBaUI5TCxPQUFqQixDQUF5QituQixZQUF6QixFQUFzQyxFQUF0QyxDQUFELEVBQTJDL3JCLElBQTNDLEVBQWdEQyxHQUFoRCxDQUFQO0FBQTZEO0FBQUM7QUFBQztBQUFDO0FBQUM7QUFBQzs7QUFDNWQsV0FBT3lzQixVQUFQO0FBQW1COztBQUNuQixXQUFTMVMsT0FBVCxDQUFnQjdYLElBQWhCLEVBQXFCbkIsUUFBckIsRUFBOEJnc0IsUUFBOUIsRUFBdUM7QUFBQyxRQUFJaHRCLElBQUo7QUFBQSxRQUFTNmtCLEtBQUssR0FBQzdqQixRQUFRLEdBQUNELE1BQU0sQ0FBQytNLE1BQVAsQ0FBYzlNLFFBQWQsRUFBdUJtQixJQUF2QixDQUFELEdBQThCQSxJQUFyRDtBQUFBLFFBQTBEakMsQ0FBQyxHQUFDLENBQTVEOztBQUE4RCxXQUFLLENBQUNGLElBQUksR0FBQzZrQixLQUFLLENBQUMza0IsQ0FBRCxDQUFYLEtBQWlCLElBQXRCLEVBQTJCQSxDQUFDLEVBQTVCLEVBQStCO0FBQUMsVUFBRyxDQUFDOHNCLFFBQUQsSUFBV2h0QixJQUFJLENBQUNULFFBQUwsS0FBZ0IsQ0FBOUIsRUFBZ0M7QUFBQ3dCLGNBQU0sQ0FBQ2tzQixTQUFQLENBQWlCN0ksTUFBTSxDQUFDcGtCLElBQUQsQ0FBdkI7QUFBZ0M7O0FBQ3ZNLFVBQUdBLElBQUksQ0FBQ1csVUFBUixFQUFtQjtBQUFDLFlBQUdxc0IsUUFBUSxJQUFFNUwsVUFBVSxDQUFDcGhCLElBQUQsQ0FBdkIsRUFBOEI7QUFBQ3FrQix1QkFBYSxDQUFDRCxNQUFNLENBQUNwa0IsSUFBRCxFQUFNLFFBQU4sQ0FBUCxDQUFiO0FBQXNDOztBQUN6RkEsWUFBSSxDQUFDVyxVQUFMLENBQWdCQyxXQUFoQixDQUE0QlosSUFBNUI7QUFBbUM7QUFBQzs7QUFDcEMsV0FBT21DLElBQVA7QUFBYTs7QUFDYnBCLFFBQU0sQ0FBQ2tDLE1BQVAsQ0FBYztBQUFDOGhCLGlCQUFhLEVBQUMsdUJBQVMrSCxJQUFULEVBQWM7QUFBQyxhQUFPQSxJQUFQO0FBQWEsS0FBM0M7QUFBNEN4cEIsU0FBSyxFQUFDLGVBQVNuQixJQUFULEVBQWMrcUIsYUFBZCxFQUE0QkMsaUJBQTVCLEVBQThDO0FBQUMsVUFBSWp0QixDQUFKO0FBQUEsVUFBTWdZLENBQU47QUFBQSxVQUFRa1YsV0FBUjtBQUFBLFVBQW9CQyxZQUFwQjtBQUFBLFVBQWlDL3BCLEtBQUssR0FBQ25CLElBQUksQ0FBQ2toQixTQUFMLENBQWUsSUFBZixDQUF2QztBQUFBLFVBQTREaUssTUFBTSxHQUFDbE0sVUFBVSxDQUFDamYsSUFBRCxDQUE3RTs7QUFBb0YsVUFBRyxDQUFDL0MsT0FBTyxDQUFDa2tCLGNBQVQsS0FBMEJuaEIsSUFBSSxDQUFDNUMsUUFBTCxLQUFnQixDQUFoQixJQUFtQjRDLElBQUksQ0FBQzVDLFFBQUwsS0FBZ0IsRUFBN0QsS0FBa0UsQ0FBQ3dCLE1BQU0sQ0FBQzBWLFFBQVAsQ0FBZ0J0VSxJQUFoQixDQUF0RSxFQUE0RjtBQUFDa3JCLG9CQUFZLEdBQUNqSixNQUFNLENBQUM5Z0IsS0FBRCxDQUFuQjtBQUEyQjhwQixtQkFBVyxHQUFDaEosTUFBTSxDQUFDamlCLElBQUQsQ0FBbEI7O0FBQXlCLGFBQUlqQyxDQUFDLEdBQUMsQ0FBRixFQUFJZ1ksQ0FBQyxHQUFDa1YsV0FBVyxDQUFDN3JCLE1BQXRCLEVBQTZCckIsQ0FBQyxHQUFDZ1ksQ0FBL0IsRUFBaUNoWSxDQUFDLEVBQWxDLEVBQXFDO0FBQUNzc0Isa0JBQVEsQ0FBQ1ksV0FBVyxDQUFDbHRCLENBQUQsQ0FBWixFQUFnQm10QixZQUFZLENBQUNudEIsQ0FBRCxDQUE1QixDQUFSO0FBQTBDO0FBQUM7O0FBQ3JhLFVBQUdndEIsYUFBSCxFQUFpQjtBQUFDLFlBQUdDLGlCQUFILEVBQXFCO0FBQUNDLHFCQUFXLEdBQUNBLFdBQVcsSUFBRWhKLE1BQU0sQ0FBQ2ppQixJQUFELENBQS9CO0FBQXNDa3JCLHNCQUFZLEdBQUNBLFlBQVksSUFBRWpKLE1BQU0sQ0FBQzlnQixLQUFELENBQWpDOztBQUF5QyxlQUFJcEQsQ0FBQyxHQUFDLENBQUYsRUFBSWdZLENBQUMsR0FBQ2tWLFdBQVcsQ0FBQzdyQixNQUF0QixFQUE2QnJCLENBQUMsR0FBQ2dZLENBQS9CLEVBQWlDaFksQ0FBQyxFQUFsQyxFQUFxQztBQUFDaXNCLDBCQUFjLENBQUNpQixXQUFXLENBQUNsdEIsQ0FBRCxDQUFaLEVBQWdCbXRCLFlBQVksQ0FBQ250QixDQUFELENBQTVCLENBQWQ7QUFBZ0Q7QUFBQyxTQUE1TCxNQUFnTTtBQUFDaXNCLHdCQUFjLENBQUNocUIsSUFBRCxFQUFNbUIsS0FBTixDQUFkO0FBQTRCO0FBQUM7O0FBQ2hQK3BCLGtCQUFZLEdBQUNqSixNQUFNLENBQUM5Z0IsS0FBRCxFQUFPLFFBQVAsQ0FBbkI7O0FBQW9DLFVBQUcrcEIsWUFBWSxDQUFDOXJCLE1BQWIsR0FBb0IsQ0FBdkIsRUFBeUI7QUFBQzhpQixxQkFBYSxDQUFDZ0osWUFBRCxFQUFjLENBQUNDLE1BQUQsSUFBU2xKLE1BQU0sQ0FBQ2ppQixJQUFELEVBQU0sUUFBTixDQUE3QixDQUFiO0FBQTREOztBQUMxSCxhQUFPbUIsS0FBUDtBQUFjLEtBSEE7QUFHQzJwQixhQUFTLEVBQUMsbUJBQVNyckIsS0FBVCxFQUFlO0FBQUMsVUFBSTZkLElBQUo7QUFBQSxVQUFTdGQsSUFBVDtBQUFBLFVBQWN6QyxJQUFkO0FBQUEsVUFBbUJ5YyxPQUFPLEdBQUNwYixNQUFNLENBQUM2a0IsS0FBUCxDQUFhekosT0FBeEM7QUFBQSxVQUFnRGpjLENBQUMsR0FBQyxDQUFsRDs7QUFBb0QsYUFBSyxDQUFDaUMsSUFBSSxHQUFDUCxLQUFLLENBQUMxQixDQUFELENBQVgsTUFBa0IwRCxTQUF2QixFQUFpQzFELENBQUMsRUFBbEMsRUFBcUM7QUFBQyxZQUFHZ2YsVUFBVSxDQUFDL2MsSUFBRCxDQUFiLEVBQW9CO0FBQUMsY0FBSXNkLElBQUksR0FBQ3RkLElBQUksQ0FBQ3lkLFFBQVEsQ0FBQy9iLE9BQVYsQ0FBYixFQUFpQztBQUFDLGdCQUFHNGIsSUFBSSxDQUFDdUcsTUFBUixFQUFlO0FBQUMsbUJBQUl0bUIsSUFBSixJQUFZK2YsSUFBSSxDQUFDdUcsTUFBakIsRUFBd0I7QUFBQyxvQkFBRzdKLE9BQU8sQ0FBQ3pjLElBQUQsQ0FBVixFQUFpQjtBQUFDcUIsd0JBQU0sQ0FBQzZrQixLQUFQLENBQWE1TCxNQUFiLENBQW9CN1gsSUFBcEIsRUFBeUJ6QyxJQUF6QjtBQUFnQyxpQkFBbEQsTUFBc0Q7QUFBQ3FCLHdCQUFNLENBQUNtbUIsV0FBUCxDQUFtQi9rQixJQUFuQixFQUF3QnpDLElBQXhCLEVBQTZCK2YsSUFBSSxDQUFDK0csTUFBbEM7QUFBMkM7QUFBQztBQUFDOztBQUN2VXJrQixnQkFBSSxDQUFDeWQsUUFBUSxDQUFDL2IsT0FBVixDQUFKLEdBQXVCRCxTQUF2QjtBQUFrQzs7QUFDbEMsY0FBR3pCLElBQUksQ0FBQzBkLFFBQVEsQ0FBQ2hjLE9BQVYsQ0FBUCxFQUEwQjtBQUFDMUIsZ0JBQUksQ0FBQzBkLFFBQVEsQ0FBQ2hjLE9BQVYsQ0FBSixHQUF1QkQsU0FBdkI7QUFBa0M7QUFBQztBQUFDO0FBQUM7QUFMbEQsR0FBZDtBQUttRTdDLFFBQU0sQ0FBQ0csRUFBUCxDQUFVK0IsTUFBVixDQUFpQjtBQUFDc3FCLFVBQU0sRUFBQyxnQkFBU3ZzQixRQUFULEVBQWtCO0FBQUMsYUFBT2daLE9BQU0sQ0FBQyxJQUFELEVBQU1oWixRQUFOLEVBQWUsSUFBZixDQUFiO0FBQW1DLEtBQTlEO0FBQStEZ1osVUFBTSxFQUFDLGdCQUFTaFosUUFBVCxFQUFrQjtBQUFDLGFBQU9nWixPQUFNLENBQUMsSUFBRCxFQUFNaFosUUFBTixDQUFiO0FBQThCLEtBQXZIO0FBQXdIVixRQUFJLEVBQUMsY0FBUzZFLEtBQVQsRUFBZTtBQUFDLGFBQU9pWixNQUFNLENBQUMsSUFBRCxFQUFNLFVBQVNqWixLQUFULEVBQWU7QUFBQyxlQUFPQSxLQUFLLEtBQUd2QixTQUFSLEdBQWtCN0MsTUFBTSxDQUFDVCxJQUFQLENBQVksSUFBWixDQUFsQixHQUFvQyxLQUFLMlosS0FBTCxHQUFhalksSUFBYixDQUFrQixZQUFVO0FBQUMsY0FBRyxLQUFLekMsUUFBTCxLQUFnQixDQUFoQixJQUFtQixLQUFLQSxRQUFMLEtBQWdCLEVBQW5DLElBQXVDLEtBQUtBLFFBQUwsS0FBZ0IsQ0FBMUQsRUFBNEQ7QUFBQyxpQkFBS3VRLFdBQUwsR0FBaUIzSyxLQUFqQjtBQUF3QjtBQUFDLFNBQW5ILENBQTNDO0FBQWlLLE9BQXZMLEVBQXdMLElBQXhMLEVBQTZMQSxLQUE3TCxFQUFtTS9DLFNBQVMsQ0FBQ2IsTUFBN00sQ0FBYjtBQUFtTyxLQUFoWDtBQUFpWGlzQixVQUFNLEVBQUMsa0JBQVU7QUFBQyxhQUFPZixRQUFRLENBQUMsSUFBRCxFQUFNcnFCLFNBQU4sRUFBZ0IsVUFBU0QsSUFBVCxFQUFjO0FBQUMsWUFBRyxLQUFLNUMsUUFBTCxLQUFnQixDQUFoQixJQUFtQixLQUFLQSxRQUFMLEtBQWdCLEVBQW5DLElBQXVDLEtBQUtBLFFBQUwsS0FBZ0IsQ0FBMUQsRUFBNEQ7QUFBQyxjQUFJZ0UsTUFBTSxHQUFDeW9CLGtCQUFrQixDQUFDLElBQUQsRUFBTTdwQixJQUFOLENBQTdCO0FBQXlDb0IsZ0JBQU0sQ0FBQzdDLFdBQVAsQ0FBbUJ5QixJQUFuQjtBQUEwQjtBQUFDLE9BQWhLLENBQWY7QUFBa0wsS0FBcmpCO0FBQXNqQnNyQixXQUFPLEVBQUMsbUJBQVU7QUFBQyxhQUFPaEIsUUFBUSxDQUFDLElBQUQsRUFBTXJxQixTQUFOLEVBQWdCLFVBQVNELElBQVQsRUFBYztBQUFDLFlBQUcsS0FBSzVDLFFBQUwsS0FBZ0IsQ0FBaEIsSUFBbUIsS0FBS0EsUUFBTCxLQUFnQixFQUFuQyxJQUF1QyxLQUFLQSxRQUFMLEtBQWdCLENBQTFELEVBQTREO0FBQUMsY0FBSWdFLE1BQU0sR0FBQ3lvQixrQkFBa0IsQ0FBQyxJQUFELEVBQU03cEIsSUFBTixDQUE3QjtBQUF5Q29CLGdCQUFNLENBQUNtcUIsWUFBUCxDQUFvQnZyQixJQUFwQixFQUF5Qm9CLE1BQU0sQ0FBQ3dNLFVBQWhDO0FBQTZDO0FBQUMsT0FBbkwsQ0FBZjtBQUFxTSxLQUE5d0I7QUFBK3dCNGQsVUFBTSxFQUFDLGtCQUFVO0FBQUMsYUFBT2xCLFFBQVEsQ0FBQyxJQUFELEVBQU1ycUIsU0FBTixFQUFnQixVQUFTRCxJQUFULEVBQWM7QUFBQyxZQUFHLEtBQUt4QixVQUFSLEVBQW1CO0FBQUMsZUFBS0EsVUFBTCxDQUFnQitzQixZQUFoQixDQUE2QnZyQixJQUE3QixFQUFrQyxJQUFsQztBQUF5QztBQUFDLE9BQTdGLENBQWY7QUFBK0csS0FBaDVCO0FBQWk1QnlyQixTQUFLLEVBQUMsaUJBQVU7QUFBQyxhQUFPbkIsUUFBUSxDQUFDLElBQUQsRUFBTXJxQixTQUFOLEVBQWdCLFVBQVNELElBQVQsRUFBYztBQUFDLFlBQUcsS0FBS3hCLFVBQVIsRUFBbUI7QUFBQyxlQUFLQSxVQUFMLENBQWdCK3NCLFlBQWhCLENBQTZCdnJCLElBQTdCLEVBQWtDLEtBQUtzSyxXQUF2QztBQUFxRDtBQUFDLE9BQXpHLENBQWY7QUFBMkgsS0FBN2hDO0FBQThoQ3dOLFNBQUssRUFBQyxpQkFBVTtBQUFDLFVBQUk5WCxJQUFKO0FBQUEsVUFBU2pDLENBQUMsR0FBQyxDQUFYOztBQUFhLGFBQUssQ0FBQ2lDLElBQUksR0FBQyxLQUFLakMsQ0FBTCxDQUFOLEtBQWdCLElBQXJCLEVBQTBCQSxDQUFDLEVBQTNCLEVBQThCO0FBQUMsWUFBR2lDLElBQUksQ0FBQzVDLFFBQUwsS0FBZ0IsQ0FBbkIsRUFBcUI7QUFBQ3dCLGdCQUFNLENBQUNrc0IsU0FBUCxDQUFpQjdJLE1BQU0sQ0FBQ2ppQixJQUFELEVBQU0sS0FBTixDQUF2QjtBQUFxQ0EsY0FBSSxDQUFDMk4sV0FBTCxHQUFpQixFQUFqQjtBQUFxQjtBQUFDOztBQUNod0MsYUFBTyxJQUFQO0FBQWEsS0FEdUU7QUFDdEV4TSxTQUFLLEVBQUMsZUFBUzRwQixhQUFULEVBQXVCQyxpQkFBdkIsRUFBeUM7QUFBQ0QsbUJBQWEsR0FBQ0EsYUFBYSxJQUFFLElBQWYsR0FBb0IsS0FBcEIsR0FBMEJBLGFBQXhDO0FBQXNEQyx1QkFBaUIsR0FBQ0EsaUJBQWlCLElBQUUsSUFBbkIsR0FBd0JELGFBQXhCLEdBQXNDQyxpQkFBeEQ7QUFBMEUsYUFBTyxLQUFLanJCLEdBQUwsQ0FBUyxZQUFVO0FBQUMsZUFBT25CLE1BQU0sQ0FBQ3VDLEtBQVAsQ0FBYSxJQUFiLEVBQWtCNHBCLGFBQWxCLEVBQWdDQyxpQkFBaEMsQ0FBUDtBQUEyRCxPQUEvRSxDQUFQO0FBQXlGLEtBRG5NO0FBQ29NTCxRQUFJLEVBQUMsY0FBUzNuQixLQUFULEVBQWU7QUFBQyxhQUFPaVosTUFBTSxDQUFDLElBQUQsRUFBTSxVQUFTalosS0FBVCxFQUFlO0FBQUMsWUFBSWhELElBQUksR0FBQyxLQUFLLENBQUwsS0FBUyxFQUFsQjtBQUFBLFlBQXFCakMsQ0FBQyxHQUFDLENBQXZCO0FBQUEsWUFBeUJnWSxDQUFDLEdBQUMsS0FBSzNXLE1BQWhDOztBQUF1QyxZQUFHNEQsS0FBSyxLQUFHdkIsU0FBUixJQUFtQnpCLElBQUksQ0FBQzVDLFFBQUwsS0FBZ0IsQ0FBdEMsRUFBd0M7QUFBQyxpQkFBTzRDLElBQUksQ0FBQ2tNLFNBQVo7QUFBdUI7O0FBQ3ZiLFlBQUcsT0FBT2xKLEtBQVAsS0FBZSxRQUFmLElBQXlCLENBQUMwbUIsWUFBWSxDQUFDNWdCLElBQWIsQ0FBa0I5RixLQUFsQixDQUExQixJQUFvRCxDQUFDcWUsT0FBTyxDQUFDLENBQUNULFFBQVEsQ0FBQ3BZLElBQVQsQ0FBY3hGLEtBQWQsS0FBc0IsQ0FBQyxFQUFELEVBQUksRUFBSixDQUF2QixFQUFnQyxDQUFoQyxFQUFtQ00sV0FBbkMsRUFBRCxDQUEvRCxFQUFrSDtBQUFDTixlQUFLLEdBQUNwRSxNQUFNLENBQUNna0IsYUFBUCxDQUFxQjVmLEtBQXJCLENBQU47O0FBQWtDLGNBQUc7QUFBQyxtQkFBS2pGLENBQUMsR0FBQ2dZLENBQVAsRUFBU2hZLENBQUMsRUFBVixFQUFhO0FBQUNpQyxrQkFBSSxHQUFDLEtBQUtqQyxDQUFMLEtBQVMsRUFBZDs7QUFBaUIsa0JBQUdpQyxJQUFJLENBQUM1QyxRQUFMLEtBQWdCLENBQW5CLEVBQXFCO0FBQUN3QixzQkFBTSxDQUFDa3NCLFNBQVAsQ0FBaUI3SSxNQUFNLENBQUNqaUIsSUFBRCxFQUFNLEtBQU4sQ0FBdkI7QUFBcUNBLG9CQUFJLENBQUNrTSxTQUFMLEdBQWVsSixLQUFmO0FBQXNCO0FBQUM7O0FBQzFRaEQsZ0JBQUksR0FBQyxDQUFMO0FBQVEsV0FENkksQ0FDN0ksT0FBTThILENBQU4sRUFBUSxDQUFFO0FBQUM7O0FBQ25CLFlBQUc5SCxJQUFILEVBQVE7QUFBQyxlQUFLOFgsS0FBTCxHQUFhdVQsTUFBYixDQUFvQnJvQixLQUFwQjtBQUE0QjtBQUFDLE9BSG9SLEVBR25SLElBSG1SLEVBRzlRQSxLQUg4USxFQUd4US9DLFNBQVMsQ0FBQ2IsTUFIOFAsQ0FBYjtBQUd4TyxLQUplO0FBSWRzc0IsZUFBVyxFQUFDLHVCQUFVO0FBQUMsVUFBSW5KLE9BQU8sR0FBQyxFQUFaO0FBQWUsYUFBTytILFFBQVEsQ0FBQyxJQUFELEVBQU1ycUIsU0FBTixFQUFnQixVQUFTRCxJQUFULEVBQWM7QUFBQyxZQUFJb1AsTUFBTSxHQUFDLEtBQUs1USxVQUFoQjs7QUFBMkIsWUFBR0ksTUFBTSxDQUFDNkQsT0FBUCxDQUFlLElBQWYsRUFBb0I4ZixPQUFwQixJQUE2QixDQUFoQyxFQUFrQztBQUFDM2pCLGdCQUFNLENBQUNrc0IsU0FBUCxDQUFpQjdJLE1BQU0sQ0FBQyxJQUFELENBQXZCOztBQUErQixjQUFHN1MsTUFBSCxFQUFVO0FBQUNBLGtCQUFNLENBQUN1YyxZQUFQLENBQW9CM3JCLElBQXBCLEVBQXlCLElBQXpCO0FBQWdDO0FBQUM7QUFBQyxPQUF6SyxFQUEwS3VpQixPQUExSyxDQUFmO0FBQW1NO0FBSjNOLEdBQWpCO0FBSStPM2pCLFFBQU0sQ0FBQ2lCLElBQVAsQ0FBWTtBQUFDK3JCLFlBQVEsRUFBQyxRQUFWO0FBQW1CQyxhQUFTLEVBQUMsU0FBN0I7QUFBdUNOLGdCQUFZLEVBQUMsUUFBcEQ7QUFBNkRPLGVBQVcsRUFBQyxPQUF6RTtBQUFpRkMsY0FBVSxFQUFDO0FBQTVGLEdBQVosRUFBdUgsVUFBUy9xQixJQUFULEVBQWNnckIsUUFBZCxFQUF1QjtBQUFDcHRCLFVBQU0sQ0FBQ0csRUFBUCxDQUFVaUMsSUFBVixJQUFnQixVQUFTbkMsUUFBVCxFQUFrQjtBQUFDLFVBQUlZLEtBQUo7QUFBQSxVQUFVQyxHQUFHLEdBQUMsRUFBZDtBQUFBLFVBQWlCdXNCLE1BQU0sR0FBQ3J0QixNQUFNLENBQUNDLFFBQUQsQ0FBOUI7QUFBQSxVQUF5Q3VCLElBQUksR0FBQzZyQixNQUFNLENBQUM3c0IsTUFBUCxHQUFjLENBQTVEO0FBQUEsVUFBOERyQixDQUFDLEdBQUMsQ0FBaEU7O0FBQWtFLGFBQUtBLENBQUMsSUFBRXFDLElBQVIsRUFBYXJDLENBQUMsRUFBZCxFQUFpQjtBQUFDMEIsYUFBSyxHQUFDMUIsQ0FBQyxLQUFHcUMsSUFBSixHQUFTLElBQVQsR0FBYyxLQUFLZSxLQUFMLENBQVcsSUFBWCxDQUFwQjtBQUFxQ3ZDLGNBQU0sQ0FBQ3F0QixNQUFNLENBQUNsdUIsQ0FBRCxDQUFQLENBQU4sQ0FBa0JpdUIsUUFBbEIsRUFBNEJ2c0IsS0FBNUI7QUFBbUNoRCxZQUFJLENBQUNELEtBQUwsQ0FBV2tELEdBQVgsRUFBZUQsS0FBSyxDQUFDSCxHQUFOLEVBQWY7QUFBNkI7O0FBQzdwQixhQUFPLEtBQUtFLFNBQUwsQ0FBZUUsR0FBZixDQUFQO0FBQTRCLEtBRHFhO0FBQ25hLEdBRG9SO0FBQ2xSLE1BQUl3c0IsU0FBUyxHQUFDLElBQUl0bUIsTUFBSixDQUFXLE9BQUtpWixJQUFMLEdBQVUsaUJBQXJCLEVBQXVDLEdBQXZDLENBQWQ7O0FBQTBELE1BQUlzTixTQUFTLEdBQUMsU0FBVkEsU0FBVSxDQUFTbnNCLElBQVQsRUFBYztBQUFDLFFBQUlrb0IsSUFBSSxHQUFDbG9CLElBQUksQ0FBQ3VJLGFBQUwsQ0FBbUI0QyxXQUE1Qjs7QUFBd0MsUUFBRyxDQUFDK2MsSUFBRCxJQUFPLENBQUNBLElBQUksQ0FBQ2tFLE1BQWhCLEVBQXVCO0FBQUNsRSxVQUFJLEdBQUNyc0IsTUFBTDtBQUFhOztBQUNwTSxXQUFPcXNCLElBQUksQ0FBQ21FLGdCQUFMLENBQXNCcnNCLElBQXRCLENBQVA7QUFBb0MsR0FEc0Q7O0FBQ3JELE1BQUlzc0IsSUFBSSxHQUFDLFNBQUxBLElBQUssQ0FBU3RzQixJQUFULEVBQWNlLE9BQWQsRUFBc0JqQixRQUF0QixFQUErQjtBQUFDLFFBQUlKLEdBQUo7QUFBQSxRQUFRc0IsSUFBUjtBQUFBLFFBQWF1ckIsR0FBRyxHQUFDLEVBQWpCOztBQUFvQixTQUFJdnJCLElBQUosSUFBWUQsT0FBWixFQUFvQjtBQUFDd3JCLFNBQUcsQ0FBQ3ZyQixJQUFELENBQUgsR0FBVWhCLElBQUksQ0FBQ3FmLEtBQUwsQ0FBV3JlLElBQVgsQ0FBVjtBQUEyQmhCLFVBQUksQ0FBQ3FmLEtBQUwsQ0FBV3JlLElBQVgsSUFBaUJELE9BQU8sQ0FBQ0MsSUFBRCxDQUF4QjtBQUFnQzs7QUFDbEx0QixPQUFHLEdBQUNJLFFBQVEsQ0FBQ3hELElBQVQsQ0FBYzBELElBQWQsQ0FBSjs7QUFBd0IsU0FBSWdCLElBQUosSUFBWUQsT0FBWixFQUFvQjtBQUFDZixVQUFJLENBQUNxZixLQUFMLENBQVdyZSxJQUFYLElBQWlCdXJCLEdBQUcsQ0FBQ3ZyQixJQUFELENBQXBCO0FBQTRCOztBQUN6RSxXQUFPdEIsR0FBUDtBQUFZLEdBRnlCOztBQUV4QixNQUFJOHNCLFNBQVMsR0FBQyxJQUFJNW1CLE1BQUosQ0FBV29aLFNBQVMsQ0FBQzlWLElBQVYsQ0FBZSxHQUFmLENBQVgsRUFBK0IsR0FBL0IsQ0FBZDs7QUFBa0QsR0FBQyxZQUFVO0FBQUMsYUFBU3VqQixpQkFBVCxHQUE0QjtBQUFDLFVBQUcsQ0FBQ3pMLEdBQUosRUFBUTtBQUFDO0FBQVE7O0FBQ3pIMEwsZUFBUyxDQUFDck4sS0FBVixDQUFnQnNOLE9BQWhCLEdBQXdCLGdEQUE4QyxtQ0FBdEU7QUFBMEczTCxTQUFHLENBQUMzQixLQUFKLENBQVVzTixPQUFWLEdBQWtCLDJFQUF5RSxxQ0FBekUsR0FBK0csa0JBQWpJO0FBQW9KM2hCLHFCQUFlLENBQUN6TSxXQUFoQixDQUE0Qm11QixTQUE1QixFQUF1Q251QixXQUF2QyxDQUFtRHlpQixHQUFuRDtBQUF3RCxVQUFJNEwsUUFBUSxHQUFDL3dCLE1BQU0sQ0FBQ3d3QixnQkFBUCxDQUF3QnJMLEdBQXhCLENBQWI7QUFBMEM2TCxzQkFBZ0IsR0FBQ0QsUUFBUSxDQUFDeGhCLEdBQVQsS0FBZSxJQUFoQztBQUFxQzBoQiwyQkFBcUIsR0FBQ0Msa0JBQWtCLENBQUNILFFBQVEsQ0FBQ0ksVUFBVixDQUFsQixLQUEwQyxFQUFoRTtBQUFtRWhNLFNBQUcsQ0FBQzNCLEtBQUosQ0FBVTROLEtBQVYsR0FBZ0IsS0FBaEI7QUFBc0JDLHVCQUFpQixHQUFDSCxrQkFBa0IsQ0FBQ0gsUUFBUSxDQUFDSyxLQUFWLENBQWxCLEtBQXFDLEVBQXZEO0FBQTBERSwwQkFBb0IsR0FBQ0osa0JBQWtCLENBQUNILFFBQVEsQ0FBQ1EsS0FBVixDQUFsQixLQUFxQyxFQUExRDtBQUE2RHBNLFNBQUcsQ0FBQzNCLEtBQUosQ0FBVWdPLFFBQVYsR0FBbUIsVUFBbkI7QUFBOEJDLHNCQUFnQixHQUFDUCxrQkFBa0IsQ0FBQy9MLEdBQUcsQ0FBQ3VNLFdBQUosR0FBZ0IsQ0FBakIsQ0FBbEIsS0FBd0MsRUFBekQ7QUFBNER2aUIscUJBQWUsQ0FBQ3ZNLFdBQWhCLENBQTRCaXVCLFNBQTVCO0FBQXVDMUwsU0FBRyxHQUFDLElBQUo7QUFBVTs7QUFDaHVCLGFBQVMrTCxrQkFBVCxDQUE0QlMsT0FBNUIsRUFBb0M7QUFBQyxhQUFPN3JCLElBQUksQ0FBQzhyQixLQUFMLENBQVdDLFVBQVUsQ0FBQ0YsT0FBRCxDQUFyQixDQUFQO0FBQXdDOztBQUM3RSxRQUFJWCxnQkFBSjtBQUFBLFFBQXFCTSxvQkFBckI7QUFBQSxRQUEwQ0csZ0JBQTFDO0FBQUEsUUFBMkRKLGlCQUEzRDtBQUFBLFFBQTZFUyx1QkFBN0U7QUFBQSxRQUFxR2IscUJBQXJHO0FBQUEsUUFBMkhKLFNBQVMsR0FBQ2h4QixRQUFRLENBQUN3QyxhQUFULENBQXVCLEtBQXZCLENBQXJJO0FBQUEsUUFBbUs4aUIsR0FBRyxHQUFDdGxCLFFBQVEsQ0FBQ3dDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBdks7O0FBQXFNLFFBQUcsQ0FBQzhpQixHQUFHLENBQUMzQixLQUFSLEVBQWM7QUFBQztBQUFROztBQUM1TjJCLE9BQUcsQ0FBQzNCLEtBQUosQ0FBVXVPLGNBQVYsR0FBeUIsYUFBekI7QUFBdUM1TSxPQUFHLENBQUNFLFNBQUosQ0FBYyxJQUFkLEVBQW9CN0IsS0FBcEIsQ0FBMEJ1TyxjQUExQixHQUF5QyxFQUF6QztBQUE0QzN3QixXQUFPLENBQUM0d0IsZUFBUixHQUF3QjdNLEdBQUcsQ0FBQzNCLEtBQUosQ0FBVXVPLGNBQVYsS0FBMkIsYUFBbkQ7QUFBaUVodkIsVUFBTSxDQUFDa0MsTUFBUCxDQUFjN0QsT0FBZCxFQUFzQjtBQUFDNndCLHVCQUFpQixFQUFDLDZCQUFVO0FBQUNyQix5QkFBaUI7QUFBRyxlQUFPVSxvQkFBUDtBQUE2QixPQUEvRTtBQUFnRlksb0JBQWMsRUFBQywwQkFBVTtBQUFDdEIseUJBQWlCO0FBQUcsZUFBT1MsaUJBQVA7QUFBMEIsT0FBeEo7QUFBeUpjLG1CQUFhLEVBQUMseUJBQVU7QUFBQ3ZCLHlCQUFpQjtBQUFHLGVBQU9JLGdCQUFQO0FBQXlCLE9BQS9OO0FBQWdPb0Isd0JBQWtCLEVBQUMsOEJBQVU7QUFBQ3hCLHlCQUFpQjtBQUFHLGVBQU9LLHFCQUFQO0FBQThCLE9BQWhUO0FBQWlUb0IsbUJBQWEsRUFBQyx5QkFBVTtBQUFDekIseUJBQWlCO0FBQUcsZUFBT2EsZ0JBQVA7QUFBeUIsT0FBdlg7QUFBd1hhLDBCQUFvQixFQUFDLGdDQUFVO0FBQUMsWUFBSUMsS0FBSixFQUFVNU0sRUFBVixFQUFhNk0sT0FBYixFQUFxQkMsT0FBckI7O0FBQTZCLFlBQUdYLHVCQUF1QixJQUFFLElBQTVCLEVBQWlDO0FBQUNTLGVBQUssR0FBQzF5QixRQUFRLENBQUN3QyxhQUFULENBQXVCLE9BQXZCLENBQU47QUFBc0NzakIsWUFBRSxHQUFDOWxCLFFBQVEsQ0FBQ3dDLGFBQVQsQ0FBdUIsSUFBdkIsQ0FBSDtBQUFnQ213QixpQkFBTyxHQUFDM3lCLFFBQVEsQ0FBQ3dDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBUjtBQUFzQ2t3QixlQUFLLENBQUMvTyxLQUFOLENBQVlzTixPQUFaLEdBQW9CLGlDQUFwQjtBQUFzRG5MLFlBQUUsQ0FBQ25DLEtBQUgsQ0FBU2tQLE1BQVQsR0FBZ0IsS0FBaEI7QUFBc0JGLGlCQUFPLENBQUNoUCxLQUFSLENBQWNrUCxNQUFkLEdBQXFCLEtBQXJCO0FBQTJCdmpCLHlCQUFlLENBQUN6TSxXQUFoQixDQUE0QjZ2QixLQUE1QixFQUFtQzd2QixXQUFuQyxDQUErQ2lqQixFQUEvQyxFQUFtRGpqQixXQUFuRCxDQUErRDh2QixPQUEvRDtBQUF3RUMsaUJBQU8sR0FBQ3p5QixNQUFNLENBQUN3d0IsZ0JBQVAsQ0FBd0I3SyxFQUF4QixDQUFSO0FBQW9DbU0saUNBQXVCLEdBQUNhLFFBQVEsQ0FBQ0YsT0FBTyxDQUFDQyxNQUFULENBQVIsR0FBeUIsQ0FBakQ7QUFBbUR2akIseUJBQWUsQ0FBQ3ZNLFdBQWhCLENBQTRCMnZCLEtBQTVCO0FBQW9DOztBQUN2aEMsZUFBT1QsdUJBQVA7QUFBZ0M7QUFEMEksS0FBdEI7QUFDaEgsR0FMMkI7O0FBS3ZCLFdBQVNjLE1BQVQsQ0FBZ0J6dUIsSUFBaEIsRUFBcUJnQixJQUFyQixFQUEwQjB0QixRQUExQixFQUFtQztBQUFDLFFBQUl0QixLQUFKO0FBQUEsUUFBVXVCLFFBQVY7QUFBQSxRQUFtQkMsUUFBbkI7QUFBQSxRQUE0Qmx2QixHQUE1QjtBQUFBLFFBQWdDMmYsS0FBSyxHQUFDcmYsSUFBSSxDQUFDcWYsS0FBM0M7QUFBaURxUCxZQUFRLEdBQUNBLFFBQVEsSUFBRXZDLFNBQVMsQ0FBQ25zQixJQUFELENBQTVCOztBQUFtQyxRQUFHMHVCLFFBQUgsRUFBWTtBQUFDaHZCLFNBQUcsR0FBQ2d2QixRQUFRLENBQUNHLGdCQUFULENBQTBCN3RCLElBQTFCLEtBQWlDMHRCLFFBQVEsQ0FBQzF0QixJQUFELENBQTdDOztBQUFvRCxVQUFHdEIsR0FBRyxLQUFHLEVBQU4sSUFBVSxDQUFDdWYsVUFBVSxDQUFDamYsSUFBRCxDQUF4QixFQUErQjtBQUFDTixXQUFHLEdBQUNkLE1BQU0sQ0FBQ3lnQixLQUFQLENBQWFyZixJQUFiLEVBQWtCZ0IsSUFBbEIsQ0FBSjtBQUE2Qjs7QUFDOVIsVUFBRyxDQUFDL0QsT0FBTyxDQUFDOHdCLGNBQVIsRUFBRCxJQUEyQjdCLFNBQVMsQ0FBQ3BqQixJQUFWLENBQWVwSixHQUFmLENBQTNCLElBQWdEOHNCLFNBQVMsQ0FBQzFqQixJQUFWLENBQWU5SCxJQUFmLENBQW5ELEVBQXdFO0FBQUNvc0IsYUFBSyxHQUFDL04sS0FBSyxDQUFDK04sS0FBWjtBQUFrQnVCLGdCQUFRLEdBQUN0UCxLQUFLLENBQUNzUCxRQUFmO0FBQXdCQyxnQkFBUSxHQUFDdlAsS0FBSyxDQUFDdVAsUUFBZjtBQUF3QnZQLGFBQUssQ0FBQ3NQLFFBQU4sR0FBZXRQLEtBQUssQ0FBQ3VQLFFBQU4sR0FBZXZQLEtBQUssQ0FBQytOLEtBQU4sR0FBWTF0QixHQUExQztBQUE4Q0EsV0FBRyxHQUFDZ3ZCLFFBQVEsQ0FBQ3RCLEtBQWI7QUFBbUIvTixhQUFLLENBQUMrTixLQUFOLEdBQVlBLEtBQVo7QUFBa0IvTixhQUFLLENBQUNzUCxRQUFOLEdBQWVBLFFBQWY7QUFBd0J0UCxhQUFLLENBQUN1UCxRQUFOLEdBQWVBLFFBQWY7QUFBeUI7QUFBQzs7QUFDaFIsV0FBT2x2QixHQUFHLEtBQUcrQixTQUFOLEdBQWdCL0IsR0FBRyxHQUFDLEVBQXBCLEdBQXVCQSxHQUE5QjtBQUFtQzs7QUFDbkMsV0FBU292QixZQUFULENBQXNCQyxXQUF0QixFQUFrQ0MsTUFBbEMsRUFBeUM7QUFBQyxXQUFNO0FBQUMxdkIsU0FBRyxFQUFDLGVBQVU7QUFBQyxZQUFHeXZCLFdBQVcsRUFBZCxFQUFpQjtBQUFDLGlCQUFPLEtBQUt6dkIsR0FBWjtBQUFnQjtBQUFROztBQUMxRyxlQUFNLENBQUMsS0FBS0EsR0FBTCxHQUFTMHZCLE1BQVYsRUFBa0J4eUIsS0FBbEIsQ0FBd0IsSUFBeEIsRUFBNkJ5RCxTQUE3QixDQUFOO0FBQStDO0FBREMsS0FBTjtBQUNROztBQUNsRCxNQUFJZ3ZCLFdBQVcsR0FBQyxDQUFDLFFBQUQsRUFBVSxLQUFWLEVBQWdCLElBQWhCLENBQWhCO0FBQUEsTUFBc0NDLFVBQVUsR0FBQ3h6QixRQUFRLENBQUN3QyxhQUFULENBQXVCLEtBQXZCLEVBQThCbWhCLEtBQS9FO0FBQUEsTUFBcUY4UCxXQUFXLEdBQUMsRUFBakc7O0FBQW9HLFdBQVNDLGNBQVQsQ0FBd0JwdUIsSUFBeEIsRUFBNkI7QUFBQyxRQUFJcXVCLE9BQU8sR0FBQ3J1QixJQUFJLENBQUMsQ0FBRCxDQUFKLENBQVE0YixXQUFSLEtBQXNCNWIsSUFBSSxDQUFDN0UsS0FBTCxDQUFXLENBQVgsQ0FBbEM7QUFBQSxRQUFnRDRCLENBQUMsR0FBQ2t4QixXQUFXLENBQUM3dkIsTUFBOUQ7O0FBQXFFLFdBQU1yQixDQUFDLEVBQVAsRUFBVTtBQUFDaUQsVUFBSSxHQUFDaXVCLFdBQVcsQ0FBQ2x4QixDQUFELENBQVgsR0FBZXN4QixPQUFwQjs7QUFBNEIsVUFBR3J1QixJQUFJLElBQUlrdUIsVUFBWCxFQUFzQjtBQUFDLGVBQU9sdUIsSUFBUDtBQUFhO0FBQUM7QUFBQzs7QUFDcFIsV0FBU3N1QixhQUFULENBQXVCdHVCLElBQXZCLEVBQTRCO0FBQUMsUUFBSXV1QixNQUFLLEdBQUMzd0IsTUFBTSxDQUFDNHdCLFFBQVAsQ0FBZ0J4dUIsSUFBaEIsS0FBdUJtdUIsV0FBVyxDQUFDbnVCLElBQUQsQ0FBNUM7O0FBQW1ELFFBQUd1dUIsTUFBSCxFQUFTO0FBQUMsYUFBT0EsTUFBUDtBQUFjOztBQUN4RyxRQUFHdnVCLElBQUksSUFBSWt1QixVQUFYLEVBQXNCO0FBQUMsYUFBT2x1QixJQUFQO0FBQWE7O0FBQ3BDLFdBQU9tdUIsV0FBVyxDQUFDbnVCLElBQUQsQ0FBWCxHQUFrQm91QixjQUFjLENBQUNwdUIsSUFBRCxDQUFkLElBQXNCQSxJQUEvQztBQUFxRDs7QUFDckQsTUFDQXl1QixZQUFZLEdBQUMsMkJBRGI7QUFBQSxNQUN5Q0MsV0FBVyxHQUFDLEtBRHJEO0FBQUEsTUFDMkRDLE9BQU8sR0FBQztBQUFDdEMsWUFBUSxFQUFDLFVBQVY7QUFBcUJ1QyxjQUFVLEVBQUMsUUFBaEM7QUFBeUN0USxXQUFPLEVBQUM7QUFBakQsR0FEbkU7QUFBQSxNQUM2SHVRLGtCQUFrQixHQUFDO0FBQUNDLGlCQUFhLEVBQUMsR0FBZjtBQUFtQkMsY0FBVSxFQUFDO0FBQTlCLEdBRGhKOztBQUNxTCxXQUFTQyxpQkFBVCxDQUEyQnp2QixLQUEzQixFQUFpQ3lDLEtBQWpDLEVBQXVDaXRCLFFBQXZDLEVBQWdEO0FBQUMsUUFBSXB0QixPQUFPLEdBQUNrYyxPQUFPLENBQUN2VyxJQUFSLENBQWF4RixLQUFiLENBQVo7QUFBZ0MsV0FBT0gsT0FBTyxHQUFDbEIsSUFBSSxDQUFDdXVCLEdBQUwsQ0FBUyxDQUFULEVBQVdydEIsT0FBTyxDQUFDLENBQUQsQ0FBUCxJQUFZb3RCLFFBQVEsSUFBRSxDQUF0QixDQUFYLEtBQXNDcHRCLE9BQU8sQ0FBQyxDQUFELENBQVAsSUFBWSxJQUFsRCxDQUFELEdBQXlERyxLQUF2RTtBQUE4RTs7QUFDcFYsV0FBU210QixrQkFBVCxDQUE0Qm53QixJQUE1QixFQUFpQ293QixTQUFqQyxFQUEyQ0MsR0FBM0MsRUFBK0NDLFdBQS9DLEVBQTJEQyxNQUEzRCxFQUFrRUMsV0FBbEUsRUFBOEU7QUFBQyxRQUFJenlCLENBQUMsR0FBQ3F5QixTQUFTLEtBQUcsT0FBWixHQUFvQixDQUFwQixHQUFzQixDQUE1QjtBQUFBLFFBQThCSyxLQUFLLEdBQUMsQ0FBcEM7QUFBQSxRQUFzQ0MsS0FBSyxHQUFDLENBQTVDOztBQUE4QyxRQUFHTCxHQUFHLE1BQUlDLFdBQVcsR0FBQyxRQUFELEdBQVUsU0FBekIsQ0FBTixFQUEwQztBQUFDLGFBQU8sQ0FBUDtBQUFVOztBQUNsTCxXQUFLdnlCLENBQUMsR0FBQyxDQUFQLEVBQVNBLENBQUMsSUFBRSxDQUFaLEVBQWM7QUFBQyxVQUFHc3lCLEdBQUcsS0FBRyxRQUFULEVBQWtCO0FBQUNLLGFBQUssSUFBRTl4QixNQUFNLENBQUMyZ0IsR0FBUCxDQUFXdmYsSUFBWCxFQUFnQnF3QixHQUFHLEdBQUNyUixTQUFTLENBQUNqaEIsQ0FBRCxDQUE3QixFQUFpQyxJQUFqQyxFQUFzQ3d5QixNQUF0QyxDQUFQO0FBQXNEOztBQUN4RixVQUFHLENBQUNELFdBQUosRUFBZ0I7QUFBQ0ksYUFBSyxJQUFFOXhCLE1BQU0sQ0FBQzJnQixHQUFQLENBQVd2ZixJQUFYLEVBQWdCLFlBQVVnZixTQUFTLENBQUNqaEIsQ0FBRCxDQUFuQyxFQUF1QyxJQUF2QyxFQUE0Q3d5QixNQUE1QyxDQUFQOztBQUEyRCxZQUFHRixHQUFHLEtBQUcsU0FBVCxFQUFtQjtBQUFDSyxlQUFLLElBQUU5eEIsTUFBTSxDQUFDMmdCLEdBQVAsQ0FBV3ZmLElBQVgsRUFBZ0IsV0FBU2dmLFNBQVMsQ0FBQ2poQixDQUFELENBQWxCLEdBQXNCLE9BQXRDLEVBQThDLElBQTlDLEVBQW1Ed3lCLE1BQW5ELENBQVA7QUFBbUUsU0FBdkYsTUFBMkY7QUFBQ0UsZUFBSyxJQUFFN3hCLE1BQU0sQ0FBQzJnQixHQUFQLENBQVd2ZixJQUFYLEVBQWdCLFdBQVNnZixTQUFTLENBQUNqaEIsQ0FBRCxDQUFsQixHQUFzQixPQUF0QyxFQUE4QyxJQUE5QyxFQUFtRHd5QixNQUFuRCxDQUFQO0FBQW1FO0FBQUMsT0FBNU8sTUFBZ1A7QUFBQyxZQUFHRixHQUFHLEtBQUcsU0FBVCxFQUFtQjtBQUFDSyxlQUFLLElBQUU5eEIsTUFBTSxDQUFDMmdCLEdBQVAsQ0FBV3ZmLElBQVgsRUFBZ0IsWUFBVWdmLFNBQVMsQ0FBQ2poQixDQUFELENBQW5DLEVBQXVDLElBQXZDLEVBQTRDd3lCLE1BQTVDLENBQVA7QUFBNEQ7O0FBQ2pVLFlBQUdGLEdBQUcsS0FBRyxRQUFULEVBQWtCO0FBQUNLLGVBQUssSUFBRTl4QixNQUFNLENBQUMyZ0IsR0FBUCxDQUFXdmYsSUFBWCxFQUFnQixXQUFTZ2YsU0FBUyxDQUFDamhCLENBQUQsQ0FBbEIsR0FBc0IsT0FBdEMsRUFBOEMsSUFBOUMsRUFBbUR3eUIsTUFBbkQsQ0FBUDtBQUFtRTtBQUFDO0FBQUM7O0FBQ3hGLFFBQUcsQ0FBQ0QsV0FBRCxJQUFjRSxXQUFXLElBQUUsQ0FBOUIsRUFBZ0M7QUFBQ0UsV0FBSyxJQUFFL3VCLElBQUksQ0FBQ3V1QixHQUFMLENBQVMsQ0FBVCxFQUFXdnVCLElBQUksQ0FBQ2d2QixJQUFMLENBQVUzd0IsSUFBSSxDQUFDLFdBQVNvd0IsU0FBUyxDQUFDLENBQUQsQ0FBVCxDQUFheFQsV0FBYixFQUFULEdBQW9Dd1QsU0FBUyxDQUFDajBCLEtBQVYsQ0FBZ0IsQ0FBaEIsQ0FBckMsQ0FBSixHQUM3RHEwQixXQUQ2RCxHQUU3REUsS0FGNkQsR0FHN0RELEtBSDZELEdBSTdELEdBSm1ELENBQVgsS0FJakMsQ0FKMEI7QUFJdkI7O0FBQ1YsV0FBT0MsS0FBUDtBQUFjOztBQUNkLFdBQVNFLGdCQUFULENBQTBCNXdCLElBQTFCLEVBQStCb3dCLFNBQS9CLEVBQXlDSyxLQUF6QyxFQUErQztBQUFDLFFBQUlGLE1BQU0sR0FBQ3BFLFNBQVMsQ0FBQ25zQixJQUFELENBQXBCO0FBQUEsUUFBMkI2d0IsZUFBZSxHQUFDLENBQUM1ekIsT0FBTyxDQUFDNndCLGlCQUFSLEVBQUQsSUFBOEIyQyxLQUF6RTtBQUFBLFFBQStFSCxXQUFXLEdBQUNPLGVBQWUsSUFBRWp5QixNQUFNLENBQUMyZ0IsR0FBUCxDQUFXdmYsSUFBWCxFQUFnQixXQUFoQixFQUE0QixLQUE1QixFQUFrQ3V3QixNQUFsQyxNQUE0QyxZQUF4SjtBQUFBLFFBQXFLTyxnQkFBZ0IsR0FBQ1IsV0FBdEw7QUFBQSxRQUFrTXR5QixHQUFHLEdBQUN5d0IsTUFBTSxDQUFDenVCLElBQUQsRUFBTW93QixTQUFOLEVBQWdCRyxNQUFoQixDQUE1TTtBQUFBLFFBQW9PUSxVQUFVLEdBQUMsV0FBU1gsU0FBUyxDQUFDLENBQUQsQ0FBVCxDQUFheFQsV0FBYixFQUFULEdBQW9Dd1QsU0FBUyxDQUFDajBCLEtBQVYsQ0FBZ0IsQ0FBaEIsQ0FBblI7O0FBQXNTLFFBQUcrdkIsU0FBUyxDQUFDcGpCLElBQVYsQ0FBZTlLLEdBQWYsQ0FBSCxFQUF1QjtBQUFDLFVBQUcsQ0FBQ3l5QixLQUFKLEVBQVU7QUFBQyxlQUFPenlCLEdBQVA7QUFBWTs7QUFDcllBLFNBQUcsR0FBQyxNQUFKO0FBQVk7O0FBQ1osUUFBRyxDQUFDLENBQUNmLE9BQU8sQ0FBQzZ3QixpQkFBUixFQUFELElBQThCd0MsV0FBOUIsSUFBMkMsQ0FBQ3J6QixPQUFPLENBQUNreEIsb0JBQVIsRUFBRCxJQUFpQ3ptQixRQUFRLENBQUMxSCxJQUFELEVBQU0sSUFBTixDQUFwRixJQUFpR2hDLEdBQUcsS0FBRyxNQUF2RyxJQUErRyxDQUFDMHZCLFVBQVUsQ0FBQzF2QixHQUFELENBQVgsSUFBa0JZLE1BQU0sQ0FBQzJnQixHQUFQLENBQVd2ZixJQUFYLEVBQWdCLFNBQWhCLEVBQTBCLEtBQTFCLEVBQWdDdXdCLE1BQWhDLE1BQTBDLFFBQTVLLEtBQXVMdndCLElBQUksQ0FBQ2d4QixjQUFMLEdBQXNCNXhCLE1BQWhOLEVBQXVOO0FBQUNreEIsaUJBQVcsR0FBQzF4QixNQUFNLENBQUMyZ0IsR0FBUCxDQUFXdmYsSUFBWCxFQUFnQixXQUFoQixFQUE0QixLQUE1QixFQUFrQ3V3QixNQUFsQyxNQUE0QyxZQUF4RDtBQUFxRU8sc0JBQWdCLEdBQUNDLFVBQVUsSUFBSS93QixJQUEvQjs7QUFBb0MsVUFBRzh3QixnQkFBSCxFQUFvQjtBQUFDOXlCLFdBQUcsR0FBQ2dDLElBQUksQ0FBQyt3QixVQUFELENBQVI7QUFBc0I7QUFBQzs7QUFDN1cveUIsT0FBRyxHQUFDMHZCLFVBQVUsQ0FBQzF2QixHQUFELENBQVYsSUFBaUIsQ0FBckI7QUFBdUIsV0FBT0EsR0FBRyxHQUNqQ215QixrQkFBa0IsQ0FBQ253QixJQUFELEVBQU1vd0IsU0FBTixFQUFnQkssS0FBSyxLQUFHSCxXQUFXLEdBQUMsUUFBRCxHQUFVLFNBQXhCLENBQXJCLEVBQXdEUSxnQkFBeEQsRUFBeUVQLE1BQXpFLEVBQWdGdnlCLEdBQWhGLENBRFcsR0FDMkUsSUFEakY7QUFDdUY7O0FBQzlHWSxRQUFNLENBQUNrQyxNQUFQLENBQWM7QUFBQ213QixZQUFRLEVBQUM7QUFBQ0MsYUFBTyxFQUFDO0FBQUM1eEIsV0FBRyxFQUFDLGFBQVNVLElBQVQsRUFBYzB1QixRQUFkLEVBQXVCO0FBQUMsY0FBR0EsUUFBSCxFQUFZO0FBQUMsZ0JBQUlodkIsR0FBRyxHQUFDK3VCLE1BQU0sQ0FBQ3p1QixJQUFELEVBQU0sU0FBTixDQUFkO0FBQStCLG1CQUFPTixHQUFHLEtBQUcsRUFBTixHQUFTLEdBQVQsR0FBYUEsR0FBcEI7QUFBeUI7QUFBQztBQUFuRztBQUFULEtBQVY7QUFBeUh1Z0IsYUFBUyxFQUFDO0FBQUMsaUNBQTBCLElBQTNCO0FBQWdDLHFCQUFjLElBQTlDO0FBQW1ELHFCQUFjLElBQWpFO0FBQXNFLGtCQUFXLElBQWpGO0FBQXNGLG9CQUFhLElBQW5HO0FBQXdHLG9CQUFhLElBQXJIO0FBQTBILGtCQUFXLElBQXJJO0FBQTBJLG9CQUFhLElBQXZKO0FBQTRKLHVCQUFnQixJQUE1SztBQUFpTCx5QkFBa0IsSUFBbk07QUFBd00saUJBQVUsSUFBbE47QUFBdU4sb0JBQWEsSUFBcE87QUFBeU8sc0JBQWUsSUFBeFA7QUFBNlAsb0JBQWEsSUFBMVE7QUFBK1EsaUJBQVUsSUFBelI7QUFBOFIsZUFBUSxJQUF0UztBQUEyUyxpQkFBVSxJQUFyVDtBQUEwVCxnQkFBUyxJQUFuVTtBQUF3VSxnQkFBUyxJQUFqVjtBQUFzVixjQUFPO0FBQTdWLEtBQW5JO0FBQXNldVAsWUFBUSxFQUFDLEVBQS9lO0FBQWtmblEsU0FBSyxFQUFDLGVBQVNyZixJQUFULEVBQWNnQixJQUFkLEVBQW1CZ0MsS0FBbkIsRUFBeUJ5dEIsS0FBekIsRUFBK0I7QUFBQyxVQUFHLENBQUN6d0IsSUFBRCxJQUFPQSxJQUFJLENBQUM1QyxRQUFMLEtBQWdCLENBQXZCLElBQTBCNEMsSUFBSSxDQUFDNUMsUUFBTCxLQUFnQixDQUExQyxJQUE2QyxDQUFDNEMsSUFBSSxDQUFDcWYsS0FBdEQsRUFBNEQ7QUFBQztBQUFROztBQUMzbUIsVUFBSTNmLEdBQUo7QUFBQSxVQUFRbkMsSUFBUjtBQUFBLFVBQWErZ0IsS0FBYjtBQUFBLFVBQW1CNlMsUUFBUSxHQUFDdFUsU0FBUyxDQUFDN2IsSUFBRCxDQUFyQztBQUFBLFVBQTRDb3dCLFlBQVksR0FBQzFCLFdBQVcsQ0FBQzVtQixJQUFaLENBQWlCOUgsSUFBakIsQ0FBekQ7QUFBQSxVQUFnRnFlLEtBQUssR0FBQ3JmLElBQUksQ0FBQ3FmLEtBQTNGOztBQUFpRyxVQUFHLENBQUMrUixZQUFKLEVBQWlCO0FBQUNwd0IsWUFBSSxHQUFDc3VCLGFBQWEsQ0FBQzZCLFFBQUQsQ0FBbEI7QUFBOEI7O0FBQ2pKN1MsV0FBSyxHQUFDMWYsTUFBTSxDQUFDcXlCLFFBQVAsQ0FBZ0Jqd0IsSUFBaEIsS0FBdUJwQyxNQUFNLENBQUNxeUIsUUFBUCxDQUFnQkUsUUFBaEIsQ0FBN0I7O0FBQXVELFVBQUdudUIsS0FBSyxLQUFHdkIsU0FBWCxFQUFxQjtBQUFDbEUsWUFBSSxXQUFReUYsS0FBUixDQUFKOztBQUFrQixZQUFHekYsSUFBSSxLQUFHLFFBQVAsS0FBa0JtQyxHQUFHLEdBQUNxZixPQUFPLENBQUN2VyxJQUFSLENBQWF4RixLQUFiLENBQXRCLEtBQTRDdEQsR0FBRyxDQUFDLENBQUQsQ0FBbEQsRUFBc0Q7QUFBQ3NELGVBQUssR0FBQ3djLFNBQVMsQ0FBQ3hmLElBQUQsRUFBTWdCLElBQU4sRUFBV3RCLEdBQVgsQ0FBZjtBQUErQm5DLGNBQUksR0FBQyxRQUFMO0FBQWU7O0FBQ3BNLFlBQUd5RixLQUFLLElBQUUsSUFBUCxJQUFhQSxLQUFLLEtBQUdBLEtBQXhCLEVBQThCO0FBQUM7QUFBUTs7QUFDdkMsWUFBR3pGLElBQUksS0FBRyxRQUFQLElBQWlCLENBQUM2ekIsWUFBckIsRUFBa0M7QUFBQ3B1QixlQUFLLElBQUV0RCxHQUFHLElBQUVBLEdBQUcsQ0FBQyxDQUFELENBQVIsS0FBY2QsTUFBTSxDQUFDcWhCLFNBQVAsQ0FBaUJrUixRQUFqQixJQUEyQixFQUEzQixHQUE4QixJQUE1QyxDQUFQO0FBQTBEOztBQUM3RixZQUFHLENBQUNsMEIsT0FBTyxDQUFDNHdCLGVBQVQsSUFBMEI3cUIsS0FBSyxLQUFHLEVBQWxDLElBQXNDaEMsSUFBSSxDQUFDdEUsT0FBTCxDQUFhLFlBQWIsTUFBNkIsQ0FBdEUsRUFBd0U7QUFBQzJpQixlQUFLLENBQUNyZSxJQUFELENBQUwsR0FBWSxTQUFaO0FBQXVCOztBQUNoRyxZQUFHLENBQUNzZCxLQUFELElBQVEsRUFBRSxTQUFRQSxLQUFWLENBQVIsSUFBMEIsQ0FBQ3RiLEtBQUssR0FBQ3NiLEtBQUssQ0FBQ2pCLEdBQU4sQ0FBVXJkLElBQVYsRUFBZWdELEtBQWYsRUFBcUJ5dEIsS0FBckIsQ0FBUCxNQUFzQ2h2QixTQUFuRSxFQUE2RTtBQUFDLGNBQUcydkIsWUFBSCxFQUFnQjtBQUFDL1IsaUJBQUssQ0FBQ2dTLFdBQU4sQ0FBa0Jyd0IsSUFBbEIsRUFBdUJnQyxLQUF2QjtBQUErQixXQUFoRCxNQUFvRDtBQUFDcWMsaUJBQUssQ0FBQ3JlLElBQUQsQ0FBTCxHQUFZZ0MsS0FBWjtBQUFtQjtBQUFDO0FBQUMsT0FKakcsTUFJcUc7QUFBQyxZQUFHc2IsS0FBSyxJQUFFLFNBQVFBLEtBQWYsSUFBc0IsQ0FBQzVlLEdBQUcsR0FBQzRlLEtBQUssQ0FBQ2hmLEdBQU4sQ0FBVVUsSUFBVixFQUFlLEtBQWYsRUFBcUJ5d0IsS0FBckIsQ0FBTCxNQUFvQ2h2QixTQUE3RCxFQUF1RTtBQUFDLGlCQUFPL0IsR0FBUDtBQUFZOztBQUNqUCxlQUFPMmYsS0FBSyxDQUFDcmUsSUFBRCxDQUFaO0FBQW9CO0FBQUMsS0FQUDtBQU9RdWUsT0FBRyxFQUFDLGFBQVN2ZixJQUFULEVBQWNnQixJQUFkLEVBQW1CeXZCLEtBQW5CLEVBQXlCRixNQUF6QixFQUFnQztBQUFDLFVBQUl2eUIsR0FBSjtBQUFBLFVBQVF1QixHQUFSO0FBQUEsVUFBWStlLEtBQVo7QUFBQSxVQUFrQjZTLFFBQVEsR0FBQ3RVLFNBQVMsQ0FBQzdiLElBQUQsQ0FBcEM7QUFBQSxVQUEyQ293QixZQUFZLEdBQUMxQixXQUFXLENBQUM1bUIsSUFBWixDQUFpQjlILElBQWpCLENBQXhEOztBQUErRSxVQUFHLENBQUNvd0IsWUFBSixFQUFpQjtBQUFDcHdCLFlBQUksR0FBQ3N1QixhQUFhLENBQUM2QixRQUFELENBQWxCO0FBQThCOztBQUMxTDdTLFdBQUssR0FBQzFmLE1BQU0sQ0FBQ3F5QixRQUFQLENBQWdCandCLElBQWhCLEtBQXVCcEMsTUFBTSxDQUFDcXlCLFFBQVAsQ0FBZ0JFLFFBQWhCLENBQTdCOztBQUF1RCxVQUFHN1MsS0FBSyxJQUFFLFNBQVFBLEtBQWxCLEVBQXdCO0FBQUN0Z0IsV0FBRyxHQUFDc2dCLEtBQUssQ0FBQ2hmLEdBQU4sQ0FBVVUsSUFBVixFQUFlLElBQWYsRUFBb0J5d0IsS0FBcEIsQ0FBSjtBQUFnQzs7QUFDaEgsVUFBR3p5QixHQUFHLEtBQUd5RCxTQUFULEVBQW1CO0FBQUN6RCxXQUFHLEdBQUN5d0IsTUFBTSxDQUFDenVCLElBQUQsRUFBTWdCLElBQU4sRUFBV3V2QixNQUFYLENBQVY7QUFBOEI7O0FBQ2xELFVBQUd2eUIsR0FBRyxLQUFHLFFBQU4sSUFBZ0JnRCxJQUFJLElBQUk2dUIsa0JBQTNCLEVBQThDO0FBQUM3eEIsV0FBRyxHQUFDNnhCLGtCQUFrQixDQUFDN3VCLElBQUQsQ0FBdEI7QUFBOEI7O0FBQzdFLFVBQUd5dkIsS0FBSyxLQUFHLEVBQVIsSUFBWUEsS0FBZixFQUFxQjtBQUFDbHhCLFdBQUcsR0FBQ211QixVQUFVLENBQUMxdkIsR0FBRCxDQUFkO0FBQW9CLGVBQU95eUIsS0FBSyxLQUFHLElBQVIsSUFBY2EsUUFBUSxDQUFDL3hCLEdBQUQsQ0FBdEIsR0FBNEJBLEdBQUcsSUFBRSxDQUFqQyxHQUFtQ3ZCLEdBQTFDO0FBQStDOztBQUN6RixhQUFPQSxHQUFQO0FBQVk7QUFaRSxHQUFkO0FBWWVZLFFBQU0sQ0FBQ2lCLElBQVAsQ0FBWSxDQUFDLFFBQUQsRUFBVSxPQUFWLENBQVosRUFBK0IsVUFBU3dELEVBQVQsRUFBWStzQixTQUFaLEVBQXNCO0FBQUN4eEIsVUFBTSxDQUFDcXlCLFFBQVAsQ0FBZ0JiLFNBQWhCLElBQTJCO0FBQUM5d0IsU0FBRyxFQUFDLGFBQVNVLElBQVQsRUFBYzB1QixRQUFkLEVBQXVCK0IsS0FBdkIsRUFBNkI7QUFBQyxZQUFHL0IsUUFBSCxFQUFZO0FBQUMsaUJBQU9lLFlBQVksQ0FBQzNtQixJQUFiLENBQWtCbEssTUFBTSxDQUFDMmdCLEdBQVAsQ0FBV3ZmLElBQVgsRUFBZ0IsU0FBaEIsQ0FBbEIsTUFBZ0QsQ0FBQ0EsSUFBSSxDQUFDZ3hCLGNBQUwsR0FBc0I1eEIsTUFBdkIsSUFBK0IsQ0FBQ1ksSUFBSSxDQUFDdXhCLHFCQUFMLEdBQTZCbkUsS0FBN0csSUFBb0hkLElBQUksQ0FBQ3RzQixJQUFELEVBQU0ydkIsT0FBTixFQUFjLFlBQVU7QUFBQyxtQkFBT2lCLGdCQUFnQixDQUFDNXdCLElBQUQsRUFBTW93QixTQUFOLEVBQWdCSyxLQUFoQixDQUF2QjtBQUErQyxXQUF4RSxDQUF4SCxHQUFrTUcsZ0JBQWdCLENBQUM1d0IsSUFBRCxFQUFNb3dCLFNBQU4sRUFBZ0JLLEtBQWhCLENBQXpOO0FBQWlQO0FBQUMsT0FBbFM7QUFBbVNwVCxTQUFHLEVBQUMsYUFBU3JkLElBQVQsRUFBY2dELEtBQWQsRUFBb0J5dEIsS0FBcEIsRUFBMEI7QUFBQyxZQUFJNXRCLE9BQUo7QUFBQSxZQUFZMHRCLE1BQU0sR0FBQ3BFLFNBQVMsQ0FBQ25zQixJQUFELENBQTVCO0FBQUEsWUFBbUN3eEIsa0JBQWtCLEdBQUMsQ0FBQ3YwQixPQUFPLENBQUNpeEIsYUFBUixFQUFELElBQTBCcUMsTUFBTSxDQUFDbEQsUUFBUCxLQUFrQixVQUFsRztBQUFBLFlBQTZHd0QsZUFBZSxHQUFDVyxrQkFBa0IsSUFBRWYsS0FBako7QUFBQSxZQUF1SkgsV0FBVyxHQUFDTyxlQUFlLElBQUVqeUIsTUFBTSxDQUFDMmdCLEdBQVAsQ0FBV3ZmLElBQVgsRUFBZ0IsV0FBaEIsRUFBNEIsS0FBNUIsRUFBa0N1d0IsTUFBbEMsTUFBNEMsWUFBaE87QUFBQSxZQUE2T04sUUFBUSxHQUFDUSxLQUFLLEdBQUNOLGtCQUFrQixDQUFDbndCLElBQUQsRUFBTW93QixTQUFOLEVBQWdCSyxLQUFoQixFQUFzQkgsV0FBdEIsRUFBa0NDLE1BQWxDLENBQW5CLEdBQTZELENBQXhUOztBQUEwVCxZQUFHRCxXQUFXLElBQUVrQixrQkFBaEIsRUFBbUM7QUFBQ3ZCLGtCQUFRLElBQUV0dUIsSUFBSSxDQUFDZ3ZCLElBQUwsQ0FBVTN3QixJQUFJLENBQUMsV0FBU293QixTQUFTLENBQUMsQ0FBRCxDQUFULENBQWF4VCxXQUFiLEVBQVQsR0FBb0N3VCxTQUFTLENBQUNqMEIsS0FBVixDQUFnQixDQUFoQixDQUFyQyxDQUFKLEdBQ3B4QnV4QixVQUFVLENBQUM2QyxNQUFNLENBQUNILFNBQUQsQ0FBUCxDQUQwd0IsR0FFcHhCRCxrQkFBa0IsQ0FBQ253QixJQUFELEVBQU1vd0IsU0FBTixFQUFnQixRQUFoQixFQUF5QixLQUF6QixFQUErQkcsTUFBL0IsQ0FGa3dCLEdBR3B4QixHQUgwd0IsQ0FBVjtBQUcxdkI7O0FBQ04sWUFBR04sUUFBUSxLQUFHcHRCLE9BQU8sR0FBQ2tjLE9BQU8sQ0FBQ3ZXLElBQVIsQ0FBYXhGLEtBQWIsQ0FBWCxDQUFSLElBQXlDLENBQUNILE9BQU8sQ0FBQyxDQUFELENBQVAsSUFBWSxJQUFiLE1BQXFCLElBQWpFLEVBQXNFO0FBQUM3QyxjQUFJLENBQUNxZixLQUFMLENBQVcrUSxTQUFYLElBQXNCcHRCLEtBQXRCO0FBQTRCQSxlQUFLLEdBQUNwRSxNQUFNLENBQUMyZ0IsR0FBUCxDQUFXdmYsSUFBWCxFQUFnQm93QixTQUFoQixDQUFOO0FBQWtDOztBQUNySSxlQUFPSixpQkFBaUIsQ0FBQ2h3QixJQUFELEVBQU1nRCxLQUFOLEVBQVlpdEIsUUFBWixDQUF4QjtBQUErQztBQUxpRCxLQUEzQjtBQUtuQixHQUxuQztBQUtxQ3J4QixRQUFNLENBQUNxeUIsUUFBUCxDQUFnQmpFLFVBQWhCLEdBQTJCOEIsWUFBWSxDQUFDN3hCLE9BQU8sQ0FBQ2d4QixrQkFBVCxFQUE0QixVQUFTanVCLElBQVQsRUFBYzB1QixRQUFkLEVBQXVCO0FBQUMsUUFBR0EsUUFBSCxFQUFZO0FBQUMsYUFBTSxDQUFDaEIsVUFBVSxDQUFDZSxNQUFNLENBQUN6dUIsSUFBRCxFQUFNLFlBQU4sQ0FBUCxDQUFWLElBQXVDQSxJQUFJLENBQUN1eEIscUJBQUwsR0FBNkJFLElBQTdCLEdBQzFNbkYsSUFBSSxDQUFDdHNCLElBQUQsRUFBTTtBQUFDZ3RCLGtCQUFVLEVBQUM7QUFBWixPQUFOLEVBQXFCLFlBQVU7QUFBQyxlQUFPaHRCLElBQUksQ0FBQ3V4QixxQkFBTCxHQUE2QkUsSUFBcEM7QUFBMEMsT0FBMUUsQ0FEOEosSUFDakYsSUFEMkU7QUFDckU7QUFBQyxHQURHLENBQXZDO0FBQ3NDN3lCLFFBQU0sQ0FBQ2lCLElBQVAsQ0FBWTtBQUFDNnhCLFVBQU0sRUFBQyxFQUFSO0FBQVdDLFdBQU8sRUFBQyxFQUFuQjtBQUFzQkMsVUFBTSxFQUFDO0FBQTdCLEdBQVosRUFBa0QsVUFBU0MsTUFBVCxFQUFnQkMsTUFBaEIsRUFBdUI7QUFBQ2x6QixVQUFNLENBQUNxeUIsUUFBUCxDQUFnQlksTUFBTSxHQUFDQyxNQUF2QixJQUErQjtBQUFDQyxZQUFNLEVBQUMsZ0JBQVMvdUIsS0FBVCxFQUFlO0FBQUMsWUFBSWpGLENBQUMsR0FBQyxDQUFOO0FBQUEsWUFBUWkwQixRQUFRLEdBQUMsRUFBakI7QUFBQSxZQUFvQkMsS0FBSyxHQUFDLE9BQU9qdkIsS0FBUCxLQUFlLFFBQWYsR0FBd0JBLEtBQUssQ0FBQ0ksS0FBTixDQUFZLEdBQVosQ0FBeEIsR0FBeUMsQ0FBQ0osS0FBRCxDQUFuRTs7QUFBMkUsZUFBS2pGLENBQUMsR0FBQyxDQUFQLEVBQVNBLENBQUMsRUFBVixFQUFhO0FBQUNpMEIsa0JBQVEsQ0FBQ0gsTUFBTSxHQUFDN1MsU0FBUyxDQUFDamhCLENBQUQsQ0FBaEIsR0FBb0IrekIsTUFBckIsQ0FBUixHQUFxQ0csS0FBSyxDQUFDbDBCLENBQUQsQ0FBTCxJQUFVazBCLEtBQUssQ0FBQ2wwQixDQUFDLEdBQUMsQ0FBSCxDQUFmLElBQXNCazBCLEtBQUssQ0FBQyxDQUFELENBQWhFO0FBQXFFOztBQUN6WCxlQUFPRCxRQUFQO0FBQWlCO0FBRGtMLEtBQS9COztBQUNqSixRQUFHSCxNQUFNLEtBQUcsUUFBWixFQUFxQjtBQUFDanpCLFlBQU0sQ0FBQ3F5QixRQUFQLENBQWdCWSxNQUFNLEdBQUNDLE1BQXZCLEVBQStCelUsR0FBL0IsR0FBbUMyUyxpQkFBbkM7QUFBc0Q7QUFBQyxHQUROO0FBQ1FweEIsUUFBTSxDQUFDRyxFQUFQLENBQVUrQixNQUFWLENBQWlCO0FBQUN5ZSxPQUFHLEVBQUMsYUFBU3ZlLElBQVQsRUFBY2dDLEtBQWQsRUFBb0I7QUFBQyxhQUFPaVosTUFBTSxDQUFDLElBQUQsRUFBTSxVQUFTamMsSUFBVCxFQUFjZ0IsSUFBZCxFQUFtQmdDLEtBQW5CLEVBQXlCO0FBQUMsWUFBSXV0QixNQUFKO0FBQUEsWUFBVzl2QixHQUFYO0FBQUEsWUFBZVYsR0FBRyxHQUFDLEVBQW5CO0FBQUEsWUFBc0JoQyxDQUFDLEdBQUMsQ0FBeEI7O0FBQTBCLFlBQUd3RCxLQUFLLENBQUNDLE9BQU4sQ0FBY1IsSUFBZCxDQUFILEVBQXVCO0FBQUN1dkIsZ0JBQU0sR0FBQ3BFLFNBQVMsQ0FBQ25zQixJQUFELENBQWhCO0FBQXVCUyxhQUFHLEdBQUNPLElBQUksQ0FBQzVCLE1BQVQ7O0FBQWdCLGlCQUFLckIsQ0FBQyxHQUFDMEMsR0FBUCxFQUFXMUMsQ0FBQyxFQUFaLEVBQWU7QUFBQ2dDLGVBQUcsQ0FBQ2lCLElBQUksQ0FBQ2pELENBQUQsQ0FBTCxDQUFILEdBQWFhLE1BQU0sQ0FBQzJnQixHQUFQLENBQVd2ZixJQUFYLEVBQWdCZ0IsSUFBSSxDQUFDakQsQ0FBRCxDQUFwQixFQUF3QixLQUF4QixFQUE4Qnd5QixNQUE5QixDQUFiO0FBQW9EOztBQUN2VixpQkFBT3h3QixHQUFQO0FBQVk7O0FBQ1osZUFBT2lELEtBQUssS0FBR3ZCLFNBQVIsR0FBa0I3QyxNQUFNLENBQUN5Z0IsS0FBUCxDQUFhcmYsSUFBYixFQUFrQmdCLElBQWxCLEVBQXVCZ0MsS0FBdkIsQ0FBbEIsR0FBZ0RwRSxNQUFNLENBQUMyZ0IsR0FBUCxDQUFXdmYsSUFBWCxFQUFnQmdCLElBQWhCLENBQXZEO0FBQThFLE9BRjRFLEVBRTNFQSxJQUYyRSxFQUV0RWdDLEtBRnNFLEVBRWhFL0MsU0FBUyxDQUFDYixNQUFWLEdBQWlCLENBRitDLENBQWI7QUFFOUI7QUFGSSxHQUFqQjs7QUFFZ0IsV0FBUzh5QixLQUFULENBQWVseUIsSUFBZixFQUFvQmUsT0FBcEIsRUFBNEJ3YyxJQUE1QixFQUFpQzVjLEdBQWpDLEVBQXFDd3hCLE1BQXJDLEVBQTRDO0FBQUMsV0FBTyxJQUFJRCxLQUFLLENBQUNqekIsU0FBTixDQUFnQkQsSUFBcEIsQ0FBeUJnQixJQUF6QixFQUE4QmUsT0FBOUIsRUFBc0N3YyxJQUF0QyxFQUEyQzVjLEdBQTNDLEVBQStDd3hCLE1BQS9DLENBQVA7QUFBK0Q7O0FBQzlOdnpCLFFBQU0sQ0FBQ3N6QixLQUFQLEdBQWFBLEtBQWI7QUFBbUJBLE9BQUssQ0FBQ2p6QixTQUFOLEdBQWdCO0FBQUNFLGVBQVcsRUFBQyt5QixLQUFiO0FBQW1CbHpCLFFBQUksRUFBQyxjQUFTZ0IsSUFBVCxFQUFjZSxPQUFkLEVBQXNCd2MsSUFBdEIsRUFBMkI1YyxHQUEzQixFQUErQnd4QixNQUEvQixFQUFzQ25TLElBQXRDLEVBQTJDO0FBQUMsV0FBS2hnQixJQUFMLEdBQVVBLElBQVY7QUFBZSxXQUFLdWQsSUFBTCxHQUFVQSxJQUFWO0FBQWUsV0FBSzRVLE1BQUwsR0FBWUEsTUFBTSxJQUFFdnpCLE1BQU0sQ0FBQ3V6QixNQUFQLENBQWN6USxRQUFsQztBQUEyQyxXQUFLM2dCLE9BQUwsR0FBYUEsT0FBYjtBQUFxQixXQUFLb08sS0FBTCxHQUFXLEtBQUtrWSxHQUFMLEdBQVMsS0FBS2xkLEdBQUwsRUFBcEI7QUFBK0IsV0FBS3hKLEdBQUwsR0FBU0EsR0FBVDtBQUFhLFdBQUtxZixJQUFMLEdBQVVBLElBQUksS0FBR3BoQixNQUFNLENBQUNxaEIsU0FBUCxDQUFpQjFDLElBQWpCLElBQXVCLEVBQXZCLEdBQTBCLElBQTdCLENBQWQ7QUFBa0QsS0FBaFE7QUFBaVFwVCxPQUFHLEVBQUMsZUFBVTtBQUFDLFVBQUltVSxLQUFLLEdBQUM0VCxLQUFLLENBQUNFLFNBQU4sQ0FBZ0IsS0FBSzdVLElBQXJCLENBQVY7QUFBcUMsYUFBT2UsS0FBSyxJQUFFQSxLQUFLLENBQUNoZixHQUFiLEdBQWlCZ2YsS0FBSyxDQUFDaGYsR0FBTixDQUFVLElBQVYsQ0FBakIsR0FBaUM0eUIsS0FBSyxDQUFDRSxTQUFOLENBQWdCMVEsUUFBaEIsQ0FBeUJwaUIsR0FBekIsQ0FBNkIsSUFBN0IsQ0FBeEM7QUFBNEUsS0FBalk7QUFBa1kreUIsT0FBRyxFQUFDLGFBQVNDLE9BQVQsRUFBaUI7QUFBQyxVQUFJQyxLQUFKO0FBQUEsVUFBVWpVLEtBQUssR0FBQzRULEtBQUssQ0FBQ0UsU0FBTixDQUFnQixLQUFLN1UsSUFBckIsQ0FBaEI7O0FBQTJDLFVBQUcsS0FBS3hjLE9BQUwsQ0FBYXl4QixRQUFoQixFQUF5QjtBQUFDLGFBQUtDLEdBQUwsR0FBU0YsS0FBSyxHQUFDM3pCLE1BQU0sQ0FBQ3V6QixNQUFQLENBQWMsS0FBS0EsTUFBbkIsRUFBMkJHLE9BQTNCLEVBQW1DLEtBQUt2eEIsT0FBTCxDQUFheXhCLFFBQWIsR0FBc0JGLE9BQXpELEVBQWlFLENBQWpFLEVBQW1FLENBQW5FLEVBQXFFLEtBQUt2eEIsT0FBTCxDQUFheXhCLFFBQWxGLENBQWY7QUFBNEcsT0FBdEksTUFBMEk7QUFBQyxhQUFLQyxHQUFMLEdBQVNGLEtBQUssR0FBQ0QsT0FBZjtBQUF3Qjs7QUFDem9CLFdBQUtqTCxHQUFMLEdBQVMsQ0FBQyxLQUFLMW1CLEdBQUwsR0FBUyxLQUFLd08sS0FBZixJQUFzQm9qQixLQUF0QixHQUE0QixLQUFLcGpCLEtBQTFDOztBQUFnRCxVQUFHLEtBQUtwTyxPQUFMLENBQWEyeEIsSUFBaEIsRUFBcUI7QUFBQyxhQUFLM3hCLE9BQUwsQ0FBYTJ4QixJQUFiLENBQWtCcDJCLElBQWxCLENBQXVCLEtBQUswRCxJQUE1QixFQUFpQyxLQUFLcW5CLEdBQXRDLEVBQTBDLElBQTFDO0FBQWlEOztBQUN2SCxVQUFHL0ksS0FBSyxJQUFFQSxLQUFLLENBQUNqQixHQUFoQixFQUFvQjtBQUFDaUIsYUFBSyxDQUFDakIsR0FBTixDQUFVLElBQVY7QUFBaUIsT0FBdEMsTUFBMEM7QUFBQzZVLGFBQUssQ0FBQ0UsU0FBTixDQUFnQjFRLFFBQWhCLENBQXlCckUsR0FBekIsQ0FBNkIsSUFBN0I7QUFBb0M7O0FBQy9FLGFBQU8sSUFBUDtBQUFhO0FBSHNCLEdBQWhCO0FBR0o2VSxPQUFLLENBQUNqekIsU0FBTixDQUFnQkQsSUFBaEIsQ0FBcUJDLFNBQXJCLEdBQStCaXpCLEtBQUssQ0FBQ2p6QixTQUFyQztBQUErQ2l6QixPQUFLLENBQUNFLFNBQU4sR0FBZ0I7QUFBQzFRLFlBQVEsRUFBQztBQUFDcGlCLFNBQUcsRUFBQyxhQUFTb2dCLEtBQVQsRUFBZTtBQUFDLFlBQUlsUixNQUFKOztBQUFXLFlBQUdrUixLQUFLLENBQUMxZixJQUFOLENBQVc1QyxRQUFYLEtBQXNCLENBQXRCLElBQXlCc2lCLEtBQUssQ0FBQzFmLElBQU4sQ0FBVzBmLEtBQUssQ0FBQ25DLElBQWpCLEtBQXdCLElBQXhCLElBQThCbUMsS0FBSyxDQUFDMWYsSUFBTixDQUFXcWYsS0FBWCxDQUFpQkssS0FBSyxDQUFDbkMsSUFBdkIsS0FBOEIsSUFBeEYsRUFBNkY7QUFBQyxpQkFBT21DLEtBQUssQ0FBQzFmLElBQU4sQ0FBVzBmLEtBQUssQ0FBQ25DLElBQWpCLENBQVA7QUFBK0I7O0FBQ3JQL08sY0FBTSxHQUFDNVAsTUFBTSxDQUFDMmdCLEdBQVAsQ0FBV0csS0FBSyxDQUFDMWYsSUFBakIsRUFBc0IwZixLQUFLLENBQUNuQyxJQUE1QixFQUFpQyxFQUFqQyxDQUFQO0FBQTRDLGVBQU0sQ0FBQy9PLE1BQUQsSUFBU0EsTUFBTSxLQUFHLE1BQWxCLEdBQXlCLENBQXpCLEdBQTJCQSxNQUFqQztBQUF5QyxPQURHO0FBQ0Y2TyxTQUFHLEVBQUMsYUFBU3FDLEtBQVQsRUFBZTtBQUFDLFlBQUc5Z0IsTUFBTSxDQUFDK3pCLEVBQVAsQ0FBVUQsSUFBVixDQUFlaFQsS0FBSyxDQUFDbkMsSUFBckIsQ0FBSCxFQUE4QjtBQUFDM2UsZ0JBQU0sQ0FBQyt6QixFQUFQLENBQVVELElBQVYsQ0FBZWhULEtBQUssQ0FBQ25DLElBQXJCLEVBQTJCbUMsS0FBM0I7QUFBbUMsU0FBbEUsTUFBdUUsSUFBR0EsS0FBSyxDQUFDMWYsSUFBTixDQUFXNUMsUUFBWCxLQUFzQixDQUF0QixLQUEwQndCLE1BQU0sQ0FBQ3F5QixRQUFQLENBQWdCdlIsS0FBSyxDQUFDbkMsSUFBdEIsS0FBNkJtQyxLQUFLLENBQUMxZixJQUFOLENBQVdxZixLQUFYLENBQWlCaVEsYUFBYSxDQUFDNVAsS0FBSyxDQUFDbkMsSUFBUCxDQUE5QixLQUE2QyxJQUFwRyxDQUFILEVBQTZHO0FBQUMzZSxnQkFBTSxDQUFDeWdCLEtBQVAsQ0FBYUssS0FBSyxDQUFDMWYsSUFBbkIsRUFBd0IwZixLQUFLLENBQUNuQyxJQUE5QixFQUFtQ21DLEtBQUssQ0FBQzJILEdBQU4sR0FBVTNILEtBQUssQ0FBQ00sSUFBbkQ7QUFBMEQsU0FBeEssTUFBNEs7QUFBQ04sZUFBSyxDQUFDMWYsSUFBTixDQUFXMGYsS0FBSyxDQUFDbkMsSUFBakIsSUFBdUJtQyxLQUFLLENBQUMySCxHQUE3QjtBQUFrQztBQUFDO0FBRHpTO0FBQVYsR0FBaEI7QUFDc1U2SyxPQUFLLENBQUNFLFNBQU4sQ0FBZ0JRLFNBQWhCLEdBQTBCVixLQUFLLENBQUNFLFNBQU4sQ0FBZ0JTLFVBQWhCLEdBQTJCO0FBQUN4VixPQUFHLEVBQUMsYUFBU3FDLEtBQVQsRUFBZTtBQUFDLFVBQUdBLEtBQUssQ0FBQzFmLElBQU4sQ0FBVzVDLFFBQVgsSUFBcUJzaUIsS0FBSyxDQUFDMWYsSUFBTixDQUFXeEIsVUFBbkMsRUFBOEM7QUFBQ2toQixhQUFLLENBQUMxZixJQUFOLENBQVcwZixLQUFLLENBQUNuQyxJQUFqQixJQUF1Qm1DLEtBQUssQ0FBQzJILEdBQTdCO0FBQWtDO0FBQUM7QUFBdkcsR0FBckQ7QUFBOEp6b0IsUUFBTSxDQUFDdXpCLE1BQVAsR0FBYztBQUFDVyxVQUFNLEVBQUMsZ0JBQVNDLENBQVQsRUFBVztBQUFDLGFBQU9BLENBQVA7QUFBVSxLQUE5QjtBQUErQkMsU0FBSyxFQUFDLGVBQVNELENBQVQsRUFBVztBQUFDLGFBQU8sTUFBSXB4QixJQUFJLENBQUNzeEIsR0FBTCxDQUFTRixDQUFDLEdBQUNweEIsSUFBSSxDQUFDdXhCLEVBQWhCLElBQW9CLENBQS9CO0FBQWtDLEtBQW5GO0FBQW9GeFIsWUFBUSxFQUFDO0FBQTdGLEdBQWQ7QUFBb0g5aUIsUUFBTSxDQUFDK3pCLEVBQVAsR0FBVVQsS0FBSyxDQUFDanpCLFNBQU4sQ0FBZ0JELElBQTFCO0FBQStCSixRQUFNLENBQUMrekIsRUFBUCxDQUFVRCxJQUFWLEdBQWUsRUFBZjtBQUFrQixNQUN2c0JTLEtBRHVzQjtBQUFBLE1BQ2pzQkMsVUFEaXNCO0FBQUEsTUFDdHJCQyxRQUFRLEdBQUMsd0JBRDZxQjtBQUFBLE1BQ3BwQkMsSUFBSSxHQUFDLGFBRCtvQjs7QUFDam9CLFdBQVNDLFFBQVQsR0FBbUI7QUFBQyxRQUFHSCxVQUFILEVBQWM7QUFBQyxVQUFHMTNCLFFBQVEsQ0FBQzgzQixNQUFULEtBQWtCLEtBQWxCLElBQXlCMzNCLE1BQU0sQ0FBQzQzQixxQkFBbkMsRUFBeUQ7QUFBQzUzQixjQUFNLENBQUM0M0IscUJBQVAsQ0FBNkJGLFFBQTdCO0FBQXdDLE9BQWxHLE1BQXNHO0FBQUMxM0IsY0FBTSxDQUFDOGUsVUFBUCxDQUFrQjRZLFFBQWxCLEVBQTJCMzBCLE1BQU0sQ0FBQyt6QixFQUFQLENBQVVlLFFBQXJDO0FBQWdEOztBQUNoUTkwQixZQUFNLENBQUMrekIsRUFBUCxDQUFVZ0IsSUFBVjtBQUFrQjtBQUFDOztBQUNuQixXQUFTQyxXQUFULEdBQXNCO0FBQUMvM0IsVUFBTSxDQUFDOGUsVUFBUCxDQUFrQixZQUFVO0FBQUN3WSxXQUFLLEdBQUMxeEIsU0FBTjtBQUFpQixLQUE5QztBQUFnRCxXQUFPMHhCLEtBQUssR0FBQzV1QixJQUFJLENBQUM4aUIsR0FBTCxFQUFiO0FBQTBCOztBQUNqRyxXQUFTd00sS0FBVCxDQUFldDJCLElBQWYsRUFBb0J1MkIsWUFBcEIsRUFBaUM7QUFBQyxRQUFJN0ssS0FBSjtBQUFBLFFBQVVsckIsQ0FBQyxHQUFDLENBQVo7QUFBQSxRQUFjZ00sS0FBSyxHQUFDO0FBQUN3a0IsWUFBTSxFQUFDaHhCO0FBQVIsS0FBcEI7QUFBa0N1MkIsZ0JBQVksR0FBQ0EsWUFBWSxHQUFDLENBQUQsR0FBRyxDQUE1Qjs7QUFBOEIsV0FBSy8xQixDQUFDLEdBQUMsQ0FBUCxFQUFTQSxDQUFDLElBQUUsSUFBRSsxQixZQUFkLEVBQTJCO0FBQUM3SyxXQUFLLEdBQUNqSyxTQUFTLENBQUNqaEIsQ0FBRCxDQUFmO0FBQW1CZ00sV0FBSyxDQUFDLFdBQVNrZixLQUFWLENBQUwsR0FBc0JsZixLQUFLLENBQUMsWUFBVWtmLEtBQVgsQ0FBTCxHQUF1QjFyQixJQUE3QztBQUFtRDs7QUFDcE0sUUFBR3UyQixZQUFILEVBQWdCO0FBQUMvcEIsV0FBSyxDQUFDbW5CLE9BQU4sR0FBY25uQixLQUFLLENBQUNxakIsS0FBTixHQUFZN3ZCLElBQTFCO0FBQWdDOztBQUNqRCxXQUFPd00sS0FBUDtBQUFjOztBQUNkLFdBQVNncUIsV0FBVCxDQUFxQi93QixLQUFyQixFQUEyQnVhLElBQTNCLEVBQWdDeVcsU0FBaEMsRUFBMEM7QUFBQyxRQUFJdFUsS0FBSjtBQUFBLFFBQVU2SyxVQUFVLEdBQUMsQ0FBQzBKLFNBQVMsQ0FBQ0MsUUFBVixDQUFtQjNXLElBQW5CLEtBQTBCLEVBQTNCLEVBQStCaGhCLE1BQS9CLENBQXNDMDNCLFNBQVMsQ0FBQ0MsUUFBVixDQUFtQixHQUFuQixDQUF0QyxDQUFyQjtBQUFBLFFBQW9GamUsS0FBSyxHQUFDLENBQTFGO0FBQUEsUUFBNEY3VyxNQUFNLEdBQUNtckIsVUFBVSxDQUFDbnJCLE1BQTlHOztBQUFxSCxXQUFLNlcsS0FBSyxHQUFDN1csTUFBWCxFQUFrQjZXLEtBQUssRUFBdkIsRUFBMEI7QUFBQyxVQUFJeUosS0FBSyxHQUFDNkssVUFBVSxDQUFDdFUsS0FBRCxDQUFWLENBQWtCM1osSUFBbEIsQ0FBdUIwM0IsU0FBdkIsRUFBaUN6VyxJQUFqQyxFQUFzQ3ZhLEtBQXRDLENBQVYsRUFBd0Q7QUFBQyxlQUFPMGMsS0FBUDtBQUFjO0FBQUM7QUFBQzs7QUFDcFEsV0FBU3lVLGdCQUFULENBQTBCbjBCLElBQTFCLEVBQStCZ25CLEtBQS9CLEVBQXFDb04sSUFBckMsRUFBMEM7QUFBQyxRQUFJN1csSUFBSjtBQUFBLFFBQVN2YSxLQUFUO0FBQUEsUUFBZTBkLE1BQWY7QUFBQSxRQUFzQnBDLEtBQXRCO0FBQUEsUUFBNEIrVixPQUE1QjtBQUFBLFFBQW9DQyxTQUFwQztBQUFBLFFBQThDQyxjQUE5QztBQUFBLFFBQTZEalYsT0FBN0Q7QUFBQSxRQUFxRWtWLEtBQUssR0FBQyxXQUFVeE4sS0FBVixJQUFpQixZQUFXQSxLQUF2RztBQUFBLFFBQTZHeU4sSUFBSSxHQUFDLElBQWxIO0FBQUEsUUFBdUhqTCxJQUFJLEdBQUMsRUFBNUg7QUFBQSxRQUErSG5LLEtBQUssR0FBQ3JmLElBQUksQ0FBQ3FmLEtBQTFJO0FBQUEsUUFBZ0ptVSxNQUFNLEdBQUN4ekIsSUFBSSxDQUFDNUMsUUFBTCxJQUFlZ2lCLGtCQUFrQixDQUFDcGYsSUFBRCxDQUF4TDtBQUFBLFFBQStMMDBCLFFBQVEsR0FBQ2pYLFFBQVEsQ0FBQ25lLEdBQVQsQ0FBYVUsSUFBYixFQUFrQixRQUFsQixDQUF4TTs7QUFBb08sUUFBRyxDQUFDbzBCLElBQUksQ0FBQzVjLEtBQVQsRUFBZTtBQUFDOEcsV0FBSyxHQUFDMWYsTUFBTSxDQUFDMmYsV0FBUCxDQUFtQnZlLElBQW5CLEVBQXdCLElBQXhCLENBQU47O0FBQW9DLFVBQUdzZSxLQUFLLENBQUNxVyxRQUFOLElBQWdCLElBQW5CLEVBQXdCO0FBQUNyVyxhQUFLLENBQUNxVyxRQUFOLEdBQWUsQ0FBZjtBQUFpQk4sZUFBTyxHQUFDL1YsS0FBSyxDQUFDeEcsS0FBTixDQUFZSixJQUFwQjs7QUFBeUI0RyxhQUFLLENBQUN4RyxLQUFOLENBQVlKLElBQVosR0FBaUIsWUFBVTtBQUFDLGNBQUcsQ0FBQzRHLEtBQUssQ0FBQ3FXLFFBQVYsRUFBbUI7QUFBQ04sbUJBQU87QUFBSTtBQUFDLFNBQTVEO0FBQThEOztBQUNwYy9WLFdBQUssQ0FBQ3FXLFFBQU47QUFBaUJGLFVBQUksQ0FBQ3ZiLE1BQUwsQ0FBWSxZQUFVO0FBQUN1YixZQUFJLENBQUN2YixNQUFMLENBQVksWUFBVTtBQUFDb0YsZUFBSyxDQUFDcVcsUUFBTjs7QUFBaUIsY0FBRyxDQUFDLzFCLE1BQU0sQ0FBQzRZLEtBQVAsQ0FBYXhYLElBQWIsRUFBa0IsSUFBbEIsRUFBd0JaLE1BQTVCLEVBQW1DO0FBQUNrZixpQkFBSyxDQUFDeEcsS0FBTixDQUFZSixJQUFaO0FBQW9CO0FBQUMsU0FBakc7QUFBb0csT0FBM0g7QUFBOEg7O0FBQy9JLFNBQUk2RixJQUFKLElBQVl5SixLQUFaLEVBQWtCO0FBQUNoa0IsV0FBSyxHQUFDZ2tCLEtBQUssQ0FBQ3pKLElBQUQsQ0FBWDs7QUFBa0IsVUFBRzhWLFFBQVEsQ0FBQ3ZxQixJQUFULENBQWM5RixLQUFkLENBQUgsRUFBd0I7QUFBQyxlQUFPZ2tCLEtBQUssQ0FBQ3pKLElBQUQsQ0FBWjtBQUFtQm1ELGNBQU0sR0FBQ0EsTUFBTSxJQUFFMWQsS0FBSyxLQUFHLFFBQXZCOztBQUFnQyxZQUFHQSxLQUFLLE1BQUl3d0IsTUFBTSxHQUFDLE1BQUQsR0FBUSxNQUFsQixDQUFSLEVBQWtDO0FBQUMsY0FBR3h3QixLQUFLLEtBQUcsTUFBUixJQUFnQjB4QixRQUFoQixJQUEwQkEsUUFBUSxDQUFDblgsSUFBRCxDQUFSLEtBQWlCOWIsU0FBOUMsRUFBd0Q7QUFBQyt4QixrQkFBTSxHQUFDLElBQVA7QUFBYSxXQUF0RSxNQUEwRTtBQUFDO0FBQVU7QUFBQzs7QUFDMU9oSyxZQUFJLENBQUNqTSxJQUFELENBQUosR0FBV21YLFFBQVEsSUFBRUEsUUFBUSxDQUFDblgsSUFBRCxDQUFsQixJQUEwQjNlLE1BQU0sQ0FBQ3lnQixLQUFQLENBQWFyZixJQUFiLEVBQWtCdWQsSUFBbEIsQ0FBckM7QUFBOEQ7QUFBQzs7QUFDL0QrVyxhQUFTLEdBQUMsQ0FBQzExQixNQUFNLENBQUN3RCxhQUFQLENBQXFCNGtCLEtBQXJCLENBQVg7O0FBQXVDLFFBQUcsQ0FBQ3NOLFNBQUQsSUFBWTExQixNQUFNLENBQUN3RCxhQUFQLENBQXFCb25CLElBQXJCLENBQWYsRUFBMEM7QUFBQztBQUFROztBQUMxRixRQUFHZ0wsS0FBSyxJQUFFeDBCLElBQUksQ0FBQzVDLFFBQUwsS0FBZ0IsQ0FBMUIsRUFBNEI7QUFBQ2czQixVQUFJLENBQUNRLFFBQUwsR0FBYyxDQUFDdlYsS0FBSyxDQUFDdVYsUUFBUCxFQUFnQnZWLEtBQUssQ0FBQ3dWLFNBQXRCLEVBQWdDeFYsS0FBSyxDQUFDeVYsU0FBdEMsQ0FBZDtBQUErRFAsb0JBQWMsR0FBQ0csUUFBUSxJQUFFQSxRQUFRLENBQUNwVixPQUFsQzs7QUFBMEMsVUFBR2lWLGNBQWMsSUFBRSxJQUFuQixFQUF3QjtBQUFDQSxzQkFBYyxHQUFDOVcsUUFBUSxDQUFDbmUsR0FBVCxDQUFhVSxJQUFiLEVBQWtCLFNBQWxCLENBQWY7QUFBNkM7O0FBQzVNc2YsYUFBTyxHQUFDMWdCLE1BQU0sQ0FBQzJnQixHQUFQLENBQVd2ZixJQUFYLEVBQWdCLFNBQWhCLENBQVI7O0FBQW1DLFVBQUdzZixPQUFPLEtBQUcsTUFBYixFQUFvQjtBQUFDLFlBQUdpVixjQUFILEVBQWtCO0FBQUNqVixpQkFBTyxHQUFDaVYsY0FBUjtBQUF3QixTQUEzQyxNQUErQztBQUFDalUsa0JBQVEsQ0FBQyxDQUFDdGdCLElBQUQsQ0FBRCxFQUFRLElBQVIsQ0FBUjtBQUFzQnUwQix3QkFBYyxHQUFDdjBCLElBQUksQ0FBQ3FmLEtBQUwsQ0FBV0MsT0FBWCxJQUFvQmlWLGNBQW5DO0FBQWtEalYsaUJBQU8sR0FBQzFnQixNQUFNLENBQUMyZ0IsR0FBUCxDQUFXdmYsSUFBWCxFQUFnQixTQUFoQixDQUFSO0FBQW1Dc2dCLGtCQUFRLENBQUMsQ0FBQ3RnQixJQUFELENBQUQsQ0FBUjtBQUFrQjtBQUFDOztBQUN0TyxVQUFHc2YsT0FBTyxLQUFHLFFBQVYsSUFBb0JBLE9BQU8sS0FBRyxjQUFWLElBQTBCaVYsY0FBYyxJQUFFLElBQWpFLEVBQXNFO0FBQUMsWUFBRzMxQixNQUFNLENBQUMyZ0IsR0FBUCxDQUFXdmYsSUFBWCxFQUFnQixPQUFoQixNQUEyQixNQUE5QixFQUFxQztBQUFDLGNBQUcsQ0FBQ3MwQixTQUFKLEVBQWM7QUFBQ0csZ0JBQUksQ0FBQy92QixJQUFMLENBQVUsWUFBVTtBQUFDMmEsbUJBQUssQ0FBQ0MsT0FBTixHQUFjaVYsY0FBZDtBQUE4QixhQUFuRDs7QUFBcUQsZ0JBQUdBLGNBQWMsSUFBRSxJQUFuQixFQUF3QjtBQUFDalYscUJBQU8sR0FBQ0QsS0FBSyxDQUFDQyxPQUFkO0FBQXNCaVYsNEJBQWMsR0FBQ2pWLE9BQU8sS0FBRyxNQUFWLEdBQWlCLEVBQWpCLEdBQW9CQSxPQUFuQztBQUE0QztBQUFDOztBQUM3UUQsZUFBSyxDQUFDQyxPQUFOLEdBQWMsY0FBZDtBQUE4QjtBQUFDO0FBQUM7O0FBQ2hDLFFBQUc4VSxJQUFJLENBQUNRLFFBQVIsRUFBaUI7QUFBQ3ZWLFdBQUssQ0FBQ3VWLFFBQU4sR0FBZSxRQUFmO0FBQXdCSCxVQUFJLENBQUN2YixNQUFMLENBQVksWUFBVTtBQUFDbUcsYUFBSyxDQUFDdVYsUUFBTixHQUFlUixJQUFJLENBQUNRLFFBQUwsQ0FBYyxDQUFkLENBQWY7QUFBZ0N2VixhQUFLLENBQUN3VixTQUFOLEdBQWdCVCxJQUFJLENBQUNRLFFBQUwsQ0FBYyxDQUFkLENBQWhCO0FBQWlDdlYsYUFBSyxDQUFDeVYsU0FBTixHQUFnQlYsSUFBSSxDQUFDUSxRQUFMLENBQWMsQ0FBZCxDQUFoQjtBQUFrQyxPQUExSDtBQUE2SDs7QUFDdktOLGFBQVMsR0FBQyxLQUFWOztBQUFnQixTQUFJL1csSUFBSixJQUFZaU0sSUFBWixFQUFpQjtBQUFDLFVBQUcsQ0FBQzhLLFNBQUosRUFBYztBQUFDLFlBQUdJLFFBQUgsRUFBWTtBQUFDLGNBQUcsWUFBV0EsUUFBZCxFQUF1QjtBQUFDbEIsa0JBQU0sR0FBQ2tCLFFBQVEsQ0FBQ2xCLE1BQWhCO0FBQXdCO0FBQUMsU0FBOUQsTUFBa0U7QUFBQ2tCLGtCQUFRLEdBQUNqWCxRQUFRLENBQUN4QixNQUFULENBQWdCamMsSUFBaEIsRUFBcUIsUUFBckIsRUFBOEI7QUFBQ3NmLG1CQUFPLEVBQUNpVjtBQUFULFdBQTlCLENBQVQ7QUFBa0U7O0FBQ3RMLFlBQUc3VCxNQUFILEVBQVU7QUFBQ2dVLGtCQUFRLENBQUNsQixNQUFULEdBQWdCLENBQUNBLE1BQWpCO0FBQXlCOztBQUNwQyxZQUFHQSxNQUFILEVBQVU7QUFBQ2xULGtCQUFRLENBQUMsQ0FBQ3RnQixJQUFELENBQUQsRUFBUSxJQUFSLENBQVI7QUFBdUI7O0FBQ2xDeTBCLFlBQUksQ0FBQy92QixJQUFMLENBQVUsWUFBVTtBQUFDLGNBQUcsQ0FBQzh1QixNQUFKLEVBQVc7QUFBQ2xULG9CQUFRLENBQUMsQ0FBQ3RnQixJQUFELENBQUQsQ0FBUjtBQUFrQjs7QUFDbkR5ZCxrQkFBUSxDQUFDNUYsTUFBVCxDQUFnQjdYLElBQWhCLEVBQXFCLFFBQXJCOztBQUErQixlQUFJdWQsSUFBSixJQUFZaU0sSUFBWixFQUFpQjtBQUFDNXFCLGtCQUFNLENBQUN5Z0IsS0FBUCxDQUFhcmYsSUFBYixFQUFrQnVkLElBQWxCLEVBQXVCaU0sSUFBSSxDQUFDak0sSUFBRCxDQUEzQjtBQUFvQztBQUFDLFNBRHRGO0FBQ3lGOztBQUN6RitXLGVBQVMsR0FBQ1AsV0FBVyxDQUFDUCxNQUFNLEdBQUNrQixRQUFRLENBQUNuWCxJQUFELENBQVQsR0FBZ0IsQ0FBdkIsRUFBeUJBLElBQXpCLEVBQThCa1gsSUFBOUIsQ0FBckI7O0FBQXlELFVBQUcsRUFBRWxYLElBQUksSUFBSW1YLFFBQVYsQ0FBSCxFQUF1QjtBQUFDQSxnQkFBUSxDQUFDblgsSUFBRCxDQUFSLEdBQWUrVyxTQUFTLENBQUNubEIsS0FBekI7O0FBQStCLFlBQUdxa0IsTUFBSCxFQUFVO0FBQUNjLG1CQUFTLENBQUMzekIsR0FBVixHQUFjMnpCLFNBQVMsQ0FBQ25sQixLQUF4QjtBQUE4Qm1sQixtQkFBUyxDQUFDbmxCLEtBQVYsR0FBZ0IsQ0FBaEI7QUFBbUI7QUFBQztBQUFDO0FBQUM7O0FBQy9LLFdBQVM0bEIsVUFBVCxDQUFvQi9OLEtBQXBCLEVBQTBCZ08sYUFBMUIsRUFBd0M7QUFBQyxRQUFJL2UsS0FBSixFQUFValYsSUFBVixFQUFlbXhCLE1BQWYsRUFBc0JudkIsS0FBdEIsRUFBNEJzYixLQUE1Qjs7QUFBa0MsU0FBSXJJLEtBQUosSUFBYStRLEtBQWIsRUFBbUI7QUFBQ2htQixVQUFJLEdBQUM2YixTQUFTLENBQUM1RyxLQUFELENBQWQ7QUFBc0JrYyxZQUFNLEdBQUM2QyxhQUFhLENBQUNoMEIsSUFBRCxDQUFwQjtBQUEyQmdDLFdBQUssR0FBQ2drQixLQUFLLENBQUMvUSxLQUFELENBQVg7O0FBQW1CLFVBQUcxVSxLQUFLLENBQUNDLE9BQU4sQ0FBY3dCLEtBQWQsQ0FBSCxFQUF3QjtBQUFDbXZCLGNBQU0sR0FBQ252QixLQUFLLENBQUMsQ0FBRCxDQUFaO0FBQWdCQSxhQUFLLEdBQUNna0IsS0FBSyxDQUFDL1EsS0FBRCxDQUFMLEdBQWFqVCxLQUFLLENBQUMsQ0FBRCxDQUF4QjtBQUE2Qjs7QUFDek8sVUFBR2lULEtBQUssS0FBR2pWLElBQVgsRUFBZ0I7QUFBQ2dtQixhQUFLLENBQUNobUIsSUFBRCxDQUFMLEdBQVlnQyxLQUFaO0FBQWtCLGVBQU9na0IsS0FBSyxDQUFDL1EsS0FBRCxDQUFaO0FBQXFCOztBQUN4RHFJLFdBQUssR0FBQzFmLE1BQU0sQ0FBQ3F5QixRQUFQLENBQWdCandCLElBQWhCLENBQU47O0FBQTRCLFVBQUdzZCxLQUFLLElBQUUsWUFBV0EsS0FBckIsRUFBMkI7QUFBQ3RiLGFBQUssR0FBQ3NiLEtBQUssQ0FBQ3lULE1BQU4sQ0FBYS91QixLQUFiLENBQU47QUFBMEIsZUFBT2drQixLQUFLLENBQUNobUIsSUFBRCxDQUFaOztBQUFtQixhQUFJaVYsS0FBSixJQUFhalQsS0FBYixFQUFtQjtBQUFDLGNBQUcsRUFBRWlULEtBQUssSUFBSStRLEtBQVgsQ0FBSCxFQUFxQjtBQUFDQSxpQkFBSyxDQUFDL1EsS0FBRCxDQUFMLEdBQWFqVCxLQUFLLENBQUNpVCxLQUFELENBQWxCO0FBQTBCK2UseUJBQWEsQ0FBQy9lLEtBQUQsQ0FBYixHQUFxQmtjLE1BQXJCO0FBQTZCO0FBQUM7QUFBQyxPQUE1SyxNQUFnTDtBQUFDNkMscUJBQWEsQ0FBQ2gwQixJQUFELENBQWIsR0FBb0JteEIsTUFBcEI7QUFBNEI7QUFBQztBQUFDOztBQUMzTyxXQUFTOEIsU0FBVCxDQUFtQmowQixJQUFuQixFQUF3QmkxQixVQUF4QixFQUFtQ2wwQixPQUFuQyxFQUEyQztBQUFDLFFBQUl5TixNQUFKO0FBQUEsUUFBVzBtQixPQUFYO0FBQUEsUUFBbUJqZixLQUFLLEdBQUMsQ0FBekI7QUFBQSxRQUEyQjdXLE1BQU0sR0FBQzYwQixTQUFTLENBQUNrQixVQUFWLENBQXFCLzFCLE1BQXZEO0FBQUEsUUFBOEQrWixRQUFRLEdBQUN2YSxNQUFNLENBQUNrYSxRQUFQLEdBQWtCSSxNQUFsQixDQUF5QixZQUFVO0FBQUMsYUFBT3lhLElBQUksQ0FBQzN6QixJQUFaO0FBQWtCLEtBQXRELENBQXZFO0FBQUEsUUFBK0gyekIsSUFBSSxHQUFDLFNBQUxBLElBQUssR0FBVTtBQUFDLFVBQUd1QixPQUFILEVBQVc7QUFBQyxlQUFPLEtBQVA7QUFBYzs7QUFDck4sVUFBSUUsV0FBVyxHQUFDakMsS0FBSyxJQUFFUyxXQUFXLEVBQWxDO0FBQUEsVUFBcUM3WSxTQUFTLEdBQUNwWixJQUFJLENBQUN1dUIsR0FBTCxDQUFTLENBQVQsRUFBVzhELFNBQVMsQ0FBQ3FCLFNBQVYsR0FBb0JyQixTQUFTLENBQUN4QixRQUE5QixHQUF1QzRDLFdBQWxELENBQS9DO0FBQUEsVUFBOEcxaUIsSUFBSSxHQUFDcUksU0FBUyxHQUFDaVosU0FBUyxDQUFDeEIsUUFBcEIsSUFBOEIsQ0FBako7QUFBQSxVQUFtSkYsT0FBTyxHQUFDLElBQUU1ZixJQUE3SjtBQUFBLFVBQWtLdUQsS0FBSyxHQUFDLENBQXhLO0FBQUEsVUFBMEs3VyxNQUFNLEdBQUM0MEIsU0FBUyxDQUFDc0IsTUFBVixDQUFpQmwyQixNQUFsTTs7QUFBeU0sYUFBSzZXLEtBQUssR0FBQzdXLE1BQVgsRUFBa0I2VyxLQUFLLEVBQXZCLEVBQTBCO0FBQUMrZCxpQkFBUyxDQUFDc0IsTUFBVixDQUFpQnJmLEtBQWpCLEVBQXdCb2MsR0FBeEIsQ0FBNEJDLE9BQTVCO0FBQXNDOztBQUMxUW5aLGNBQVEsQ0FBQ2lCLFVBQVQsQ0FBb0JwYSxJQUFwQixFQUF5QixDQUFDZzBCLFNBQUQsRUFBVzFCLE9BQVgsRUFBbUJ2WCxTQUFuQixDQUF6Qjs7QUFBd0QsVUFBR3VYLE9BQU8sR0FBQyxDQUFSLElBQVdsekIsTUFBZCxFQUFxQjtBQUFDLGVBQU8yYixTQUFQO0FBQWtCOztBQUNoRyxVQUFHLENBQUMzYixNQUFKLEVBQVc7QUFBQytaLGdCQUFRLENBQUNpQixVQUFULENBQW9CcGEsSUFBcEIsRUFBeUIsQ0FBQ2cwQixTQUFELEVBQVcsQ0FBWCxFQUFhLENBQWIsQ0FBekI7QUFBMkM7O0FBQ3ZEN2EsY0FBUSxDQUFDa0IsV0FBVCxDQUFxQnJhLElBQXJCLEVBQTBCLENBQUNnMEIsU0FBRCxDQUExQjtBQUF1QyxhQUFPLEtBQVA7QUFBYyxLQUpUO0FBQUEsUUFJVUEsU0FBUyxHQUFDN2EsUUFBUSxDQUFDUixPQUFULENBQWlCO0FBQUMzWSxVQUFJLEVBQUNBLElBQU47QUFBV2duQixXQUFLLEVBQUNwb0IsTUFBTSxDQUFDa0MsTUFBUCxDQUFjLEVBQWQsRUFBaUJtMEIsVUFBakIsQ0FBakI7QUFBOENiLFVBQUksRUFBQ3gxQixNQUFNLENBQUNrQyxNQUFQLENBQWMsSUFBZCxFQUFtQjtBQUFDazBCLHFCQUFhLEVBQUMsRUFBZjtBQUFrQjdDLGNBQU0sRUFBQ3Z6QixNQUFNLENBQUN1ekIsTUFBUCxDQUFjelE7QUFBdkMsT0FBbkIsRUFBb0UzZ0IsT0FBcEUsQ0FBbkQ7QUFBZ0l3MEIsd0JBQWtCLEVBQUNOLFVBQW5KO0FBQThKTyxxQkFBZSxFQUFDejBCLE9BQTlLO0FBQXNMczBCLGVBQVMsRUFBQ2xDLEtBQUssSUFBRVMsV0FBVyxFQUFsTjtBQUFxTnBCLGNBQVEsRUFBQ3p4QixPQUFPLENBQUN5eEIsUUFBdE87QUFBK084QyxZQUFNLEVBQUMsRUFBdFA7QUFBeVB2QixpQkFBVyxFQUFDLHFCQUFTeFcsSUFBVCxFQUFjNWMsR0FBZCxFQUFrQjtBQUFDLFlBQUkrZSxLQUFLLEdBQUM5Z0IsTUFBTSxDQUFDc3pCLEtBQVAsQ0FBYWx5QixJQUFiLEVBQWtCZzBCLFNBQVMsQ0FBQ0ksSUFBNUIsRUFBaUM3VyxJQUFqQyxFQUFzQzVjLEdBQXRDLEVBQTBDcXpCLFNBQVMsQ0FBQ0ksSUFBVixDQUFlWSxhQUFmLENBQTZCelgsSUFBN0IsS0FBb0N5VyxTQUFTLENBQUNJLElBQVYsQ0FBZWpDLE1BQTdGLENBQVY7QUFBK0c2QixpQkFBUyxDQUFDc0IsTUFBVixDQUFpQjc0QixJQUFqQixDQUFzQmlqQixLQUF0QjtBQUE2QixlQUFPQSxLQUFQO0FBQWMsT0FBbGI7QUFBbWJsQixVQUFJLEVBQUMsY0FBU2lYLE9BQVQsRUFBaUI7QUFBQyxZQUFJeGYsS0FBSyxHQUFDLENBQVY7QUFBQSxZQUFZN1csTUFBTSxHQUFDcTJCLE9BQU8sR0FBQ3pCLFNBQVMsQ0FBQ3NCLE1BQVYsQ0FBaUJsMkIsTUFBbEIsR0FBeUIsQ0FBbkQ7O0FBQXFELFlBQUc4MUIsT0FBSCxFQUFXO0FBQUMsaUJBQU8sSUFBUDtBQUFhOztBQUN6bUJBLGVBQU8sR0FBQyxJQUFSOztBQUFhLGVBQUtqZixLQUFLLEdBQUM3VyxNQUFYLEVBQWtCNlcsS0FBSyxFQUF2QixFQUEwQjtBQUFDK2QsbUJBQVMsQ0FBQ3NCLE1BQVYsQ0FBaUJyZixLQUFqQixFQUF3Qm9jLEdBQXhCLENBQTRCLENBQTVCO0FBQWdDOztBQUN4RSxZQUFHb0QsT0FBSCxFQUFXO0FBQUN0YyxrQkFBUSxDQUFDaUIsVUFBVCxDQUFvQnBhLElBQXBCLEVBQXlCLENBQUNnMEIsU0FBRCxFQUFXLENBQVgsRUFBYSxDQUFiLENBQXpCO0FBQTBDN2Esa0JBQVEsQ0FBQ2tCLFdBQVQsQ0FBcUJyYSxJQUFyQixFQUEwQixDQUFDZzBCLFNBQUQsRUFBV3lCLE9BQVgsQ0FBMUI7QUFBZ0QsU0FBdEcsTUFBMEc7QUFBQ3RjLGtCQUFRLENBQUNzQixVQUFULENBQW9CemEsSUFBcEIsRUFBeUIsQ0FBQ2cwQixTQUFELEVBQVd5QixPQUFYLENBQXpCO0FBQStDOztBQUMxSixlQUFPLElBQVA7QUFBYTtBQUhvRSxLQUFqQixDQUpwQjtBQUFBLFFBTzVCek8sS0FBSyxHQUFDZ04sU0FBUyxDQUFDaE4sS0FQWTs7QUFPTitOLGNBQVUsQ0FBQy9OLEtBQUQsRUFBT2dOLFNBQVMsQ0FBQ0ksSUFBVixDQUFlWSxhQUF0QixDQUFWOztBQUErQyxXQUFLL2UsS0FBSyxHQUFDN1csTUFBWCxFQUFrQjZXLEtBQUssRUFBdkIsRUFBMEI7QUFBQ3pILFlBQU0sR0FBQ3lsQixTQUFTLENBQUNrQixVQUFWLENBQXFCbGYsS0FBckIsRUFBNEIzWixJQUE1QixDQUFpQzAzQixTQUFqQyxFQUEyQ2gwQixJQUEzQyxFQUFnRGduQixLQUFoRCxFQUFzRGdOLFNBQVMsQ0FBQ0ksSUFBaEUsQ0FBUDs7QUFBNkUsVUFBRzVsQixNQUFILEVBQVU7QUFBQyxZQUFHdFIsVUFBVSxDQUFDc1IsTUFBTSxDQUFDZ1EsSUFBUixDQUFiLEVBQTJCO0FBQUM1ZixnQkFBTSxDQUFDMmYsV0FBUCxDQUFtQnlWLFNBQVMsQ0FBQ2gwQixJQUE3QixFQUFrQ2cwQixTQUFTLENBQUNJLElBQVYsQ0FBZTVjLEtBQWpELEVBQXdEZ0gsSUFBeEQsR0FBNkRoUSxNQUFNLENBQUNnUSxJQUFQLENBQVlrWCxJQUFaLENBQWlCbG5CLE1BQWpCLENBQTdEO0FBQXVGOztBQUMzVCxlQUFPQSxNQUFQO0FBQWU7QUFBQzs7QUFDaEI1UCxVQUFNLENBQUNtQixHQUFQLENBQVdpbkIsS0FBWCxFQUFpQitNLFdBQWpCLEVBQTZCQyxTQUE3Qjs7QUFBd0MsUUFBRzkyQixVQUFVLENBQUM4MkIsU0FBUyxDQUFDSSxJQUFWLENBQWVqbEIsS0FBaEIsQ0FBYixFQUFvQztBQUFDNmtCLGVBQVMsQ0FBQ0ksSUFBVixDQUFlamxCLEtBQWYsQ0FBcUI3UyxJQUFyQixDQUEwQjBELElBQTFCLEVBQStCZzBCLFNBQS9CO0FBQTJDOztBQUN4SEEsYUFBUyxDQUFDdmEsUUFBVixDQUFtQnVhLFNBQVMsQ0FBQ0ksSUFBVixDQUFlM2EsUUFBbEMsRUFBNEMvVSxJQUE1QyxDQUFpRHN2QixTQUFTLENBQUNJLElBQVYsQ0FBZTF2QixJQUFoRSxFQUFxRXN2QixTQUFTLENBQUNJLElBQVYsQ0FBZXVCLFFBQXBGLEVBQThGL2MsSUFBOUYsQ0FBbUdvYixTQUFTLENBQUNJLElBQVYsQ0FBZXhiLElBQWxILEVBQXdITSxNQUF4SCxDQUErSDhhLFNBQVMsQ0FBQ0ksSUFBVixDQUFlbGIsTUFBOUk7QUFBc0p0YSxVQUFNLENBQUMrekIsRUFBUCxDQUFVaUQsS0FBVixDQUFnQmgzQixNQUFNLENBQUNrQyxNQUFQLENBQWM2eUIsSUFBZCxFQUFtQjtBQUFDM3pCLFVBQUksRUFBQ0EsSUFBTjtBQUFXeTBCLFVBQUksRUFBQ1QsU0FBaEI7QUFBMEJ4YyxXQUFLLEVBQUN3YyxTQUFTLENBQUNJLElBQVYsQ0FBZTVjO0FBQS9DLEtBQW5CLENBQWhCO0FBQTJGLFdBQU93YyxTQUFQO0FBQWtCOztBQUNuUXAxQixRQUFNLENBQUNxMUIsU0FBUCxHQUFpQnIxQixNQUFNLENBQUNrQyxNQUFQLENBQWNtekIsU0FBZCxFQUF3QjtBQUFDQyxZQUFRLEVBQUM7QUFBQyxXQUFJLENBQUMsVUFBUzNXLElBQVQsRUFBY3ZhLEtBQWQsRUFBb0I7QUFBQyxZQUFJMGMsS0FBSyxHQUFDLEtBQUtxVSxXQUFMLENBQWlCeFcsSUFBakIsRUFBc0J2YSxLQUF0QixDQUFWO0FBQXVDd2MsaUJBQVMsQ0FBQ0UsS0FBSyxDQUFDMWYsSUFBUCxFQUFZdWQsSUFBWixFQUFpQndCLE9BQU8sQ0FBQ3ZXLElBQVIsQ0FBYXhGLEtBQWIsQ0FBakIsRUFBcUMwYyxLQUFyQyxDQUFUO0FBQXFELGVBQU9BLEtBQVA7QUFBYyxPQUFoSTtBQUFMLEtBQVY7QUFBa0ptVyxXQUFPLEVBQUMsaUJBQVM3TyxLQUFULEVBQWVsbkIsUUFBZixFQUF3QjtBQUFDLFVBQUc1QyxVQUFVLENBQUM4cEIsS0FBRCxDQUFiLEVBQXFCO0FBQUNsbkIsZ0JBQVEsR0FBQ2tuQixLQUFUO0FBQWVBLGFBQUssR0FBQyxDQUFDLEdBQUQsQ0FBTjtBQUFhLE9BQWxELE1BQXNEO0FBQUNBLGFBQUssR0FBQ0EsS0FBSyxDQUFDN2UsS0FBTixDQUFZMk8sYUFBWixDQUFOO0FBQWtDOztBQUNyVCxVQUFJeUcsSUFBSjtBQUFBLFVBQVN0SCxLQUFLLEdBQUMsQ0FBZjtBQUFBLFVBQWlCN1csTUFBTSxHQUFDNG5CLEtBQUssQ0FBQzVuQixNQUE5Qjs7QUFBcUMsYUFBSzZXLEtBQUssR0FBQzdXLE1BQVgsRUFBa0I2VyxLQUFLLEVBQXZCLEVBQTBCO0FBQUNzSCxZQUFJLEdBQUN5SixLQUFLLENBQUMvUSxLQUFELENBQVY7QUFBa0JnZSxpQkFBUyxDQUFDQyxRQUFWLENBQW1CM1csSUFBbkIsSUFBeUIwVyxTQUFTLENBQUNDLFFBQVYsQ0FBbUIzVyxJQUFuQixLQUEwQixFQUFuRDtBQUFzRDBXLGlCQUFTLENBQUNDLFFBQVYsQ0FBbUIzVyxJQUFuQixFQUF5QnRRLE9BQXpCLENBQWlDbk4sUUFBakM7QUFBNEM7QUFBQyxLQUQ1STtBQUM2SXExQixjQUFVLEVBQUMsQ0FBQ2hCLGdCQUFELENBRHhKO0FBQzJLMkIsYUFBUyxFQUFDLG1CQUFTaDJCLFFBQVQsRUFBa0J3ckIsT0FBbEIsRUFBMEI7QUFBQyxVQUFHQSxPQUFILEVBQVc7QUFBQzJJLGlCQUFTLENBQUNrQixVQUFWLENBQXFCbG9CLE9BQXJCLENBQTZCbk4sUUFBN0I7QUFBd0MsT0FBcEQsTUFBd0Q7QUFBQ20wQixpQkFBUyxDQUFDa0IsVUFBVixDQUFxQjE0QixJQUFyQixDQUEwQnFELFFBQTFCO0FBQXFDO0FBQUM7QUFEL1MsR0FBeEIsQ0FBakI7O0FBQzJWbEIsUUFBTSxDQUFDbTNCLEtBQVAsR0FBYSxVQUFTQSxLQUFULEVBQWU1RCxNQUFmLEVBQXNCcHpCLEVBQXRCLEVBQXlCO0FBQUMsUUFBSWkzQixHQUFHLEdBQUNELEtBQUssSUFBRSxRQUFPQSxLQUFQLE1BQWUsUUFBdEIsR0FBK0JuM0IsTUFBTSxDQUFDa0MsTUFBUCxDQUFjLEVBQWQsRUFBaUJpMUIsS0FBakIsQ0FBL0IsR0FBdUQ7QUFBQ0osY0FBUSxFQUFDNTJCLEVBQUUsSUFBRSxDQUFDQSxFQUFELElBQUtvekIsTUFBVCxJQUFpQmoxQixVQUFVLENBQUM2NEIsS0FBRCxDQUFWLElBQW1CQSxLQUE5QztBQUFvRHZELGNBQVEsRUFBQ3VELEtBQTdEO0FBQW1FNUQsWUFBTSxFQUFDcHpCLEVBQUUsSUFBRW96QixNQUFKLElBQVlBLE1BQU0sSUFBRSxDQUFDajFCLFVBQVUsQ0FBQ2kxQixNQUFELENBQW5CLElBQTZCQTtBQUFuSCxLQUEvRDs7QUFBMEwsUUFBR3Z6QixNQUFNLENBQUMrekIsRUFBUCxDQUFValAsR0FBYixFQUFpQjtBQUFDc1MsU0FBRyxDQUFDeEQsUUFBSixHQUFhLENBQWI7QUFBZ0IsS0FBbEMsTUFBc0M7QUFBQyxVQUFHLE9BQU93RCxHQUFHLENBQUN4RCxRQUFYLEtBQXNCLFFBQXpCLEVBQWtDO0FBQUMsWUFBR3dELEdBQUcsQ0FBQ3hELFFBQUosSUFBZ0I1ekIsTUFBTSxDQUFDK3pCLEVBQVAsQ0FBVXNELE1BQTdCLEVBQW9DO0FBQUNELGFBQUcsQ0FBQ3hELFFBQUosR0FBYTV6QixNQUFNLENBQUMrekIsRUFBUCxDQUFVc0QsTUFBVixDQUFpQkQsR0FBRyxDQUFDeEQsUUFBckIsQ0FBYjtBQUE2QyxTQUFsRixNQUFzRjtBQUFDd0QsYUFBRyxDQUFDeEQsUUFBSixHQUFhNXpCLE1BQU0sQ0FBQyt6QixFQUFQLENBQVVzRCxNQUFWLENBQWlCdlUsUUFBOUI7QUFBd0M7QUFBQztBQUFDOztBQUN2d0IsUUFBR3NVLEdBQUcsQ0FBQ3hlLEtBQUosSUFBVyxJQUFYLElBQWlCd2UsR0FBRyxDQUFDeGUsS0FBSixLQUFZLElBQWhDLEVBQXFDO0FBQUN3ZSxTQUFHLENBQUN4ZSxLQUFKLEdBQVUsSUFBVjtBQUFnQjs7QUFDdER3ZSxPQUFHLENBQUN6SixHQUFKLEdBQVF5SixHQUFHLENBQUNMLFFBQVo7O0FBQXFCSyxPQUFHLENBQUNMLFFBQUosR0FBYSxZQUFVO0FBQUMsVUFBR3o0QixVQUFVLENBQUM4NEIsR0FBRyxDQUFDekosR0FBTCxDQUFiLEVBQXVCO0FBQUN5SixXQUFHLENBQUN6SixHQUFKLENBQVFqd0IsSUFBUixDQUFhLElBQWI7QUFBb0I7O0FBQ3pGLFVBQUcwNUIsR0FBRyxDQUFDeGUsS0FBUCxFQUFhO0FBQUM1WSxjQUFNLENBQUN3ZixPQUFQLENBQWUsSUFBZixFQUFvQjRYLEdBQUcsQ0FBQ3hlLEtBQXhCO0FBQWdDO0FBQUMsS0FEMUI7O0FBQzJCLFdBQU93ZSxHQUFQO0FBQVksR0FIK1I7O0FBRzlScDNCLFFBQU0sQ0FBQ0csRUFBUCxDQUFVK0IsTUFBVixDQUFpQjtBQUFDbzFCLFVBQU0sRUFBQyxnQkFBU0gsS0FBVCxFQUFlSSxFQUFmLEVBQWtCaEUsTUFBbEIsRUFBeUJyeUIsUUFBekIsRUFBa0M7QUFBQyxhQUFPLEtBQUs2TCxNQUFMLENBQVl5VCxrQkFBWixFQUFnQ0csR0FBaEMsQ0FBb0MsU0FBcEMsRUFBOEMsQ0FBOUMsRUFBaURnQixJQUFqRCxHQUF3RDVmLEdBQXhELEdBQThEeTFCLE9BQTlELENBQXNFO0FBQUNsRixlQUFPLEVBQUNpRjtBQUFULE9BQXRFLEVBQW1GSixLQUFuRixFQUF5RjVELE1BQXpGLEVBQWdHcnlCLFFBQWhHLENBQVA7QUFBa0gsS0FBN0o7QUFBOEpzMkIsV0FBTyxFQUFDLGlCQUFTN1ksSUFBVCxFQUFjd1ksS0FBZCxFQUFvQjVELE1BQXBCLEVBQTJCcnlCLFFBQTNCLEVBQW9DO0FBQUMsVUFBSWdZLEtBQUssR0FBQ2xaLE1BQU0sQ0FBQ3dELGFBQVAsQ0FBcUJtYixJQUFyQixDQUFWO0FBQUEsVUFBcUM4WSxNQUFNLEdBQUN6M0IsTUFBTSxDQUFDbTNCLEtBQVAsQ0FBYUEsS0FBYixFQUFtQjVELE1BQW5CLEVBQTBCcnlCLFFBQTFCLENBQTVDO0FBQUEsVUFBZ0Z3MkIsV0FBVyxHQUFDLFNBQVpBLFdBQVksR0FBVTtBQUFDLFlBQUk3QixJQUFJLEdBQUNSLFNBQVMsQ0FBQyxJQUFELEVBQU1yMUIsTUFBTSxDQUFDa0MsTUFBUCxDQUFjLEVBQWQsRUFBaUJ5YyxJQUFqQixDQUFOLEVBQTZCOFksTUFBN0IsQ0FBbEI7O0FBQXVELFlBQUd2ZSxLQUFLLElBQUUyRixRQUFRLENBQUNuZSxHQUFULENBQWEsSUFBYixFQUFrQixRQUFsQixDQUFWLEVBQXNDO0FBQUNtMUIsY0FBSSxDQUFDalcsSUFBTCxDQUFVLElBQVY7QUFBaUI7QUFBQyxPQUF2Tjs7QUFBd044WCxpQkFBVyxDQUFDQyxNQUFaLEdBQW1CRCxXQUFuQjtBQUErQixhQUFPeGUsS0FBSyxJQUFFdWUsTUFBTSxDQUFDN2UsS0FBUCxLQUFlLEtBQXRCLEdBQTRCLEtBQUszWCxJQUFMLENBQVV5MkIsV0FBVixDQUE1QixHQUFtRCxLQUFLOWUsS0FBTCxDQUFXNmUsTUFBTSxDQUFDN2UsS0FBbEIsRUFBd0I4ZSxXQUF4QixDQUExRDtBQUFnRyxLQUFsaUI7QUFBbWlCOVgsUUFBSSxFQUFDLGNBQVNqaEIsSUFBVCxFQUFjbWhCLFVBQWQsRUFBeUIrVyxPQUF6QixFQUFpQztBQUFDLFVBQUllLFNBQVMsR0FBQyxTQUFWQSxTQUFVLENBQVNsWSxLQUFULEVBQWU7QUFBQyxZQUFJRSxJQUFJLEdBQUNGLEtBQUssQ0FBQ0UsSUFBZjtBQUFvQixlQUFPRixLQUFLLENBQUNFLElBQWI7QUFBa0JBLFlBQUksQ0FBQ2lYLE9BQUQsQ0FBSjtBQUFlLE9BQW5GOztBQUFvRixVQUFHLE9BQU9sNEIsSUFBUCxLQUFjLFFBQWpCLEVBQTBCO0FBQUNrNEIsZUFBTyxHQUFDL1csVUFBUjtBQUFtQkEsa0JBQVUsR0FBQ25oQixJQUFYO0FBQWdCQSxZQUFJLEdBQUNrRSxTQUFMO0FBQWdCOztBQUMxekIsVUFBR2lkLFVBQUgsRUFBYztBQUFDLGFBQUtsSCxLQUFMLENBQVdqYSxJQUFJLElBQUUsSUFBakIsRUFBc0IsRUFBdEI7QUFBMkI7O0FBQzFDLGFBQU8sS0FBS3NDLElBQUwsQ0FBVSxZQUFVO0FBQUMsWUFBSXVlLE9BQU8sR0FBQyxJQUFaO0FBQUEsWUFBaUJuSSxLQUFLLEdBQUMxWSxJQUFJLElBQUUsSUFBTixJQUFZQSxJQUFJLEdBQUMsWUFBeEM7QUFBQSxZQUFxRGs1QixNQUFNLEdBQUM3M0IsTUFBTSxDQUFDNjNCLE1BQW5FO0FBQUEsWUFBMEVuWixJQUFJLEdBQUNHLFFBQVEsQ0FBQ25lLEdBQVQsQ0FBYSxJQUFiLENBQS9FOztBQUFrRyxZQUFHMlcsS0FBSCxFQUFTO0FBQUMsY0FBR3FILElBQUksQ0FBQ3JILEtBQUQsQ0FBSixJQUFhcUgsSUFBSSxDQUFDckgsS0FBRCxDQUFKLENBQVl1SSxJQUE1QixFQUFpQztBQUFDZ1kscUJBQVMsQ0FBQ2xaLElBQUksQ0FBQ3JILEtBQUQsQ0FBTCxDQUFUO0FBQXdCO0FBQUMsU0FBckUsTUFBeUU7QUFBQyxlQUFJQSxLQUFKLElBQWFxSCxJQUFiLEVBQWtCO0FBQUMsZ0JBQUdBLElBQUksQ0FBQ3JILEtBQUQsQ0FBSixJQUFhcUgsSUFBSSxDQUFDckgsS0FBRCxDQUFKLENBQVl1SSxJQUF6QixJQUErQjhVLElBQUksQ0FBQ3hxQixJQUFMLENBQVVtTixLQUFWLENBQWxDLEVBQW1EO0FBQUN1Z0IsdUJBQVMsQ0FBQ2xaLElBQUksQ0FBQ3JILEtBQUQsQ0FBTCxDQUFUO0FBQXdCO0FBQUM7QUFBQzs7QUFDelMsYUFBSUEsS0FBSyxHQUFDd2dCLE1BQU0sQ0FBQ3IzQixNQUFqQixFQUF3QjZXLEtBQUssRUFBN0IsR0FBaUM7QUFBQyxjQUFHd2dCLE1BQU0sQ0FBQ3hnQixLQUFELENBQU4sQ0FBY2pXLElBQWQsS0FBcUIsSUFBckIsS0FBNEJ6QyxJQUFJLElBQUUsSUFBTixJQUFZazVCLE1BQU0sQ0FBQ3hnQixLQUFELENBQU4sQ0FBY3VCLEtBQWQsS0FBc0JqYSxJQUE5RCxDQUFILEVBQXVFO0FBQUNrNUIsa0JBQU0sQ0FBQ3hnQixLQUFELENBQU4sQ0FBY3dlLElBQWQsQ0FBbUJqVyxJQUFuQixDQUF3QmlYLE9BQXhCO0FBQWlDclgsbUJBQU8sR0FBQyxLQUFSO0FBQWNxWSxrQkFBTSxDQUFDNTFCLE1BQVAsQ0FBY29WLEtBQWQsRUFBb0IsQ0FBcEI7QUFBd0I7QUFBQzs7QUFDbEwsWUFBR21JLE9BQU8sSUFBRSxDQUFDcVgsT0FBYixFQUFxQjtBQUFDNzJCLGdCQUFNLENBQUN3ZixPQUFQLENBQWUsSUFBZixFQUFvQjdnQixJQUFwQjtBQUEyQjtBQUFDLE9BRjNDLENBQVA7QUFFcUQsS0FKeUI7QUFJeEJnNUIsVUFBTSxFQUFDLGdCQUFTaDVCLElBQVQsRUFBYztBQUFDLFVBQUdBLElBQUksS0FBRyxLQUFWLEVBQWdCO0FBQUNBLFlBQUksR0FBQ0EsSUFBSSxJQUFFLElBQVg7QUFBaUI7O0FBQzlHLGFBQU8sS0FBS3NDLElBQUwsQ0FBVSxZQUFVO0FBQUMsWUFBSW9XLEtBQUo7QUFBQSxZQUFVcUgsSUFBSSxHQUFDRyxRQUFRLENBQUNuZSxHQUFULENBQWEsSUFBYixDQUFmO0FBQUEsWUFBa0NrWSxLQUFLLEdBQUM4RixJQUFJLENBQUMvZixJQUFJLEdBQUMsT0FBTixDQUE1QztBQUFBLFlBQTJEK2dCLEtBQUssR0FBQ2hCLElBQUksQ0FBQy9mLElBQUksR0FBQyxZQUFOLENBQXJFO0FBQUEsWUFBeUZrNUIsTUFBTSxHQUFDNzNCLE1BQU0sQ0FBQzYzQixNQUF2RztBQUFBLFlBQThHcjNCLE1BQU0sR0FBQ29ZLEtBQUssR0FBQ0EsS0FBSyxDQUFDcFksTUFBUCxHQUFjLENBQXhJO0FBQTBJa2UsWUFBSSxDQUFDaVosTUFBTCxHQUFZLElBQVo7QUFBaUIzM0IsY0FBTSxDQUFDNFksS0FBUCxDQUFhLElBQWIsRUFBa0JqYSxJQUFsQixFQUF1QixFQUF2Qjs7QUFBMkIsWUFBRytnQixLQUFLLElBQUVBLEtBQUssQ0FBQ0UsSUFBaEIsRUFBcUI7QUFBQ0YsZUFBSyxDQUFDRSxJQUFOLENBQVdsaUIsSUFBWCxDQUFnQixJQUFoQixFQUFxQixJQUFyQjtBQUE0Qjs7QUFDcFEsYUFBSTJaLEtBQUssR0FBQ3dnQixNQUFNLENBQUNyM0IsTUFBakIsRUFBd0I2VyxLQUFLLEVBQTdCLEdBQWlDO0FBQUMsY0FBR3dnQixNQUFNLENBQUN4Z0IsS0FBRCxDQUFOLENBQWNqVyxJQUFkLEtBQXFCLElBQXJCLElBQTJCeTJCLE1BQU0sQ0FBQ3hnQixLQUFELENBQU4sQ0FBY3VCLEtBQWQsS0FBc0JqYSxJQUFwRCxFQUF5RDtBQUFDazVCLGtCQUFNLENBQUN4Z0IsS0FBRCxDQUFOLENBQWN3ZSxJQUFkLENBQW1CalcsSUFBbkIsQ0FBd0IsSUFBeEI7QUFBOEJpWSxrQkFBTSxDQUFDNTFCLE1BQVAsQ0FBY29WLEtBQWQsRUFBb0IsQ0FBcEI7QUFBd0I7QUFBQzs7QUFDbkosYUFBSUEsS0FBSyxHQUFDLENBQVYsRUFBWUEsS0FBSyxHQUFDN1csTUFBbEIsRUFBeUI2VyxLQUFLLEVBQTlCLEVBQWlDO0FBQUMsY0FBR3VCLEtBQUssQ0FBQ3ZCLEtBQUQsQ0FBTCxJQUFjdUIsS0FBSyxDQUFDdkIsS0FBRCxDQUFMLENBQWFzZ0IsTUFBOUIsRUFBcUM7QUFBQy9lLGlCQUFLLENBQUN2QixLQUFELENBQUwsQ0FBYXNnQixNQUFiLENBQW9CajZCLElBQXBCLENBQXlCLElBQXpCO0FBQWdDO0FBQUM7O0FBQ3pHLGVBQU9naEIsSUFBSSxDQUFDaVosTUFBWjtBQUFvQixPQUhiLENBQVA7QUFHdUI7QUFSdUQsR0FBakI7QUFRbkMzM0IsUUFBTSxDQUFDaUIsSUFBUCxDQUFZLENBQUMsUUFBRCxFQUFVLE1BQVYsRUFBaUIsTUFBakIsQ0FBWixFQUFxQyxVQUFTd0QsRUFBVCxFQUFZckMsSUFBWixFQUFpQjtBQUFDLFFBQUkwMUIsS0FBSyxHQUFDOTNCLE1BQU0sQ0FBQ0csRUFBUCxDQUFVaUMsSUFBVixDQUFWOztBQUEwQnBDLFVBQU0sQ0FBQ0csRUFBUCxDQUFVaUMsSUFBVixJQUFnQixVQUFTKzBCLEtBQVQsRUFBZTVELE1BQWYsRUFBc0JyeUIsUUFBdEIsRUFBK0I7QUFBQyxhQUFPaTJCLEtBQUssSUFBRSxJQUFQLElBQWEsT0FBT0EsS0FBUCxLQUFlLFNBQTVCLEdBQXNDVyxLQUFLLENBQUNsNkIsS0FBTixDQUFZLElBQVosRUFBaUJ5RCxTQUFqQixDQUF0QyxHQUFrRSxLQUFLbTJCLE9BQUwsQ0FBYXZDLEtBQUssQ0FBQzd5QixJQUFELEVBQU0sSUFBTixDQUFsQixFQUE4QiswQixLQUE5QixFQUFvQzVELE1BQXBDLEVBQTJDcnlCLFFBQTNDLENBQXpFO0FBQStILEtBQS9LO0FBQWlMLEdBQWxRO0FBQW9RbEIsUUFBTSxDQUFDaUIsSUFBUCxDQUFZO0FBQUM4MkIsYUFBUyxFQUFDOUMsS0FBSyxDQUFDLE1BQUQsQ0FBaEI7QUFBeUIrQyxXQUFPLEVBQUMvQyxLQUFLLENBQUMsTUFBRCxDQUF0QztBQUErQ2dELGVBQVcsRUFBQ2hELEtBQUssQ0FBQyxRQUFELENBQWhFO0FBQTJFaUQsVUFBTSxFQUFDO0FBQUM1RixhQUFPLEVBQUM7QUFBVCxLQUFsRjtBQUFtRzZGLFdBQU8sRUFBQztBQUFDN0YsYUFBTyxFQUFDO0FBQVQsS0FBM0c7QUFBNEg4RixjQUFVLEVBQUM7QUFBQzlGLGFBQU8sRUFBQztBQUFUO0FBQXZJLEdBQVosRUFBdUssVUFBU2x3QixJQUFULEVBQWNnbUIsS0FBZCxFQUFvQjtBQUFDcG9CLFVBQU0sQ0FBQ0csRUFBUCxDQUFVaUMsSUFBVixJQUFnQixVQUFTKzBCLEtBQVQsRUFBZTVELE1BQWYsRUFBc0JyeUIsUUFBdEIsRUFBK0I7QUFBQyxhQUFPLEtBQUtzMkIsT0FBTCxDQUFhcFAsS0FBYixFQUFtQitPLEtBQW5CLEVBQXlCNUQsTUFBekIsRUFBZ0NyeUIsUUFBaEMsQ0FBUDtBQUFrRCxLQUFsRztBQUFvRyxHQUFoUztBQUFrU2xCLFFBQU0sQ0FBQzYzQixNQUFQLEdBQWMsRUFBZDs7QUFBaUI3M0IsUUFBTSxDQUFDK3pCLEVBQVAsQ0FBVWdCLElBQVYsR0FBZSxZQUFVO0FBQUMsUUFBSWlDLEtBQUo7QUFBQSxRQUFVNzNCLENBQUMsR0FBQyxDQUFaO0FBQUEsUUFBYzA0QixNQUFNLEdBQUM3M0IsTUFBTSxDQUFDNjNCLE1BQTVCO0FBQW1DdEQsU0FBSyxHQUFDNXVCLElBQUksQ0FBQzhpQixHQUFMLEVBQU47O0FBQWlCLFdBQUt0cEIsQ0FBQyxHQUFDMDRCLE1BQU0sQ0FBQ3IzQixNQUFkLEVBQXFCckIsQ0FBQyxFQUF0QixFQUF5QjtBQUFDNjNCLFdBQUssR0FBQ2EsTUFBTSxDQUFDMTRCLENBQUQsQ0FBWjs7QUFBZ0IsVUFBRyxDQUFDNjNCLEtBQUssRUFBTixJQUFVYSxNQUFNLENBQUMxNEIsQ0FBRCxDQUFOLEtBQVk2M0IsS0FBekIsRUFBK0I7QUFBQ2EsY0FBTSxDQUFDNTFCLE1BQVAsQ0FBYzlDLENBQUMsRUFBZixFQUFrQixDQUFsQjtBQUFzQjtBQUFDOztBQUNod0IsUUFBRyxDQUFDMDRCLE1BQU0sQ0FBQ3IzQixNQUFYLEVBQWtCO0FBQUNSLFlBQU0sQ0FBQyt6QixFQUFQLENBQVVuVSxJQUFWO0FBQWtCOztBQUNyQzJVLFNBQUssR0FBQzF4QixTQUFOO0FBQWlCLEdBRmdrQjs7QUFFL2pCN0MsUUFBTSxDQUFDK3pCLEVBQVAsQ0FBVWlELEtBQVYsR0FBZ0IsVUFBU0EsS0FBVCxFQUFlO0FBQUNoM0IsVUFBTSxDQUFDNjNCLE1BQVAsQ0FBY2g2QixJQUFkLENBQW1CbTVCLEtBQW5CO0FBQTBCaDNCLFVBQU0sQ0FBQyt6QixFQUFQLENBQVV4akIsS0FBVjtBQUFtQixHQUE3RTs7QUFBOEV2USxRQUFNLENBQUMrekIsRUFBUCxDQUFVZSxRQUFWLEdBQW1CLEVBQW5COztBQUFzQjkwQixRQUFNLENBQUMrekIsRUFBUCxDQUFVeGpCLEtBQVYsR0FBZ0IsWUFBVTtBQUFDLFFBQUdpa0IsVUFBSCxFQUFjO0FBQUM7QUFBUTs7QUFDeEtBLGNBQVUsR0FBQyxJQUFYO0FBQWdCRyxZQUFRO0FBQUksR0FEMEY7O0FBQ3pGMzBCLFFBQU0sQ0FBQyt6QixFQUFQLENBQVVuVSxJQUFWLEdBQWUsWUFBVTtBQUFDNFUsY0FBVSxHQUFDLElBQVg7QUFBaUIsR0FBM0M7O0FBQTRDeDBCLFFBQU0sQ0FBQyt6QixFQUFQLENBQVVzRCxNQUFWLEdBQWlCO0FBQUNnQixRQUFJLEVBQUMsR0FBTjtBQUFVQyxRQUFJLEVBQUMsR0FBZjtBQUFtQnhWLFlBQVEsRUFBQztBQUE1QixHQUFqQjs7QUFBa0Q5aUIsUUFBTSxDQUFDRyxFQUFQLENBQVVvNEIsS0FBVixHQUFnQixVQUFTQyxJQUFULEVBQWM3NUIsSUFBZCxFQUFtQjtBQUFDNjVCLFFBQUksR0FBQ3g0QixNQUFNLENBQUMrekIsRUFBUCxHQUFVL3pCLE1BQU0sQ0FBQyt6QixFQUFQLENBQVVzRCxNQUFWLENBQWlCbUIsSUFBakIsS0FBd0JBLElBQWxDLEdBQXVDQSxJQUE1QztBQUFpRDc1QixRQUFJLEdBQUNBLElBQUksSUFBRSxJQUFYO0FBQWdCLFdBQU8sS0FBS2lhLEtBQUwsQ0FBV2phLElBQVgsRUFBZ0IsVUFBU3FLLElBQVQsRUFBYzBXLEtBQWQsRUFBb0I7QUFBQyxVQUFJK1ksT0FBTyxHQUFDeDdCLE1BQU0sQ0FBQzhlLFVBQVAsQ0FBa0IvUyxJQUFsQixFQUF1Qnd2QixJQUF2QixDQUFaOztBQUF5QzlZLFdBQUssQ0FBQ0UsSUFBTixHQUFXLFlBQVU7QUFBQzNpQixjQUFNLENBQUN5N0IsWUFBUCxDQUFvQkQsT0FBcEI7QUFBOEIsT0FBcEQ7QUFBc0QsS0FBcEksQ0FBUDtBQUE4SSxHQUFuUDs7QUFBb1AsR0FBQyxZQUFVO0FBQUMsUUFBSXByQixLQUFLLEdBQUN2USxRQUFRLENBQUN3QyxhQUFULENBQXVCLE9BQXZCLENBQVY7QUFBQSxRQUEwQzJGLE1BQU0sR0FBQ25JLFFBQVEsQ0FBQ3dDLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBakQ7QUFBQSxRQUFrRjgzQixHQUFHLEdBQUNueUIsTUFBTSxDQUFDdEYsV0FBUCxDQUFtQjdDLFFBQVEsQ0FBQ3dDLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBbkIsQ0FBdEY7QUFBMkkrTixTQUFLLENBQUMxTyxJQUFOLEdBQVcsVUFBWDtBQUFzQk4sV0FBTyxDQUFDczZCLE9BQVIsR0FBZ0J0ckIsS0FBSyxDQUFDakosS0FBTixLQUFjLEVBQTlCO0FBQWlDL0YsV0FBTyxDQUFDdTZCLFdBQVIsR0FBb0J4QixHQUFHLENBQUN4bEIsUUFBeEI7QUFBaUN2RSxTQUFLLEdBQUN2USxRQUFRLENBQUN3QyxhQUFULENBQXVCLE9BQXZCLENBQU47QUFBc0MrTixTQUFLLENBQUNqSixLQUFOLEdBQVksR0FBWjtBQUFnQmlKLFNBQUssQ0FBQzFPLElBQU4sR0FBVyxPQUFYO0FBQW1CTixXQUFPLENBQUN3NkIsVUFBUixHQUFtQnhyQixLQUFLLENBQUNqSixLQUFOLEtBQWMsR0FBakM7QUFBc0MsR0FBOVY7O0FBQWtXLE1BQUkwMEIsUUFBSjtBQUFBLE1BQWF6dEIsVUFBVSxHQUFDckwsTUFBTSxDQUFDc08sSUFBUCxDQUFZakQsVUFBcEM7QUFBK0NyTCxRQUFNLENBQUNHLEVBQVAsQ0FBVStCLE1BQVYsQ0FBaUI7QUFBQ3NNLFFBQUksRUFBQyxjQUFTcE0sSUFBVCxFQUFjZ0MsS0FBZCxFQUFvQjtBQUFDLGFBQU9pWixNQUFNLENBQUMsSUFBRCxFQUFNcmQsTUFBTSxDQUFDd08sSUFBYixFQUFrQnBNLElBQWxCLEVBQXVCZ0MsS0FBdkIsRUFBNkIvQyxTQUFTLENBQUNiLE1BQVYsR0FBaUIsQ0FBOUMsQ0FBYjtBQUErRCxLQUExRjtBQUEyRnU0QixjQUFVLEVBQUMsb0JBQVMzMkIsSUFBVCxFQUFjO0FBQUMsYUFBTyxLQUFLbkIsSUFBTCxDQUFVLFlBQVU7QUFBQ2pCLGNBQU0sQ0FBQys0QixVQUFQLENBQWtCLElBQWxCLEVBQXVCMzJCLElBQXZCO0FBQThCLE9BQW5ELENBQVA7QUFBNkQ7QUFBbEwsR0FBakI7QUFBc01wQyxRQUFNLENBQUNrQyxNQUFQLENBQWM7QUFBQ3NNLFFBQUksRUFBQyxjQUFTcE4sSUFBVCxFQUFjZ0IsSUFBZCxFQUFtQmdDLEtBQW5CLEVBQXlCO0FBQUMsVUFBSXRELEdBQUo7QUFBQSxVQUFRNGUsS0FBUjtBQUFBLFVBQWNzWixLQUFLLEdBQUM1M0IsSUFBSSxDQUFDNUMsUUFBekI7O0FBQWtDLFVBQUd3NkIsS0FBSyxLQUFHLENBQVIsSUFBV0EsS0FBSyxLQUFHLENBQW5CLElBQXNCQSxLQUFLLEtBQUcsQ0FBakMsRUFBbUM7QUFBQztBQUFROztBQUNsa0MsVUFBRyxPQUFPNTNCLElBQUksQ0FBQzVCLFlBQVosS0FBMkIsV0FBOUIsRUFBMEM7QUFBQyxlQUFPUSxNQUFNLENBQUMyZSxJQUFQLENBQVl2ZCxJQUFaLEVBQWlCZ0IsSUFBakIsRUFBc0JnQyxLQUF0QixDQUFQO0FBQXFDOztBQUNoRixVQUFHNDBCLEtBQUssS0FBRyxDQUFSLElBQVcsQ0FBQ2g1QixNQUFNLENBQUMwVixRQUFQLENBQWdCdFUsSUFBaEIsQ0FBZixFQUFxQztBQUFDc2UsYUFBSyxHQUFDMWYsTUFBTSxDQUFDaTVCLFNBQVAsQ0FBaUI3MkIsSUFBSSxDQUFDc0MsV0FBTCxFQUFqQixNQUF1QzFFLE1BQU0sQ0FBQ3NPLElBQVAsQ0FBWS9FLEtBQVosQ0FBa0IydkIsSUFBbEIsQ0FBdUJodkIsSUFBdkIsQ0FBNEI5SCxJQUE1QixJQUFrQzAyQixRQUFsQyxHQUEyQ2oyQixTQUFsRixDQUFOO0FBQW9HOztBQUMxSSxVQUFHdUIsS0FBSyxLQUFHdkIsU0FBWCxFQUFxQjtBQUFDLFlBQUd1QixLQUFLLEtBQUcsSUFBWCxFQUFnQjtBQUFDcEUsZ0JBQU0sQ0FBQys0QixVQUFQLENBQWtCMzNCLElBQWxCLEVBQXVCZ0IsSUFBdkI7QUFBNkI7QUFBUTs7QUFDNUUsWUFBR3NkLEtBQUssSUFBRSxTQUFRQSxLQUFmLElBQXNCLENBQUM1ZSxHQUFHLEdBQUM0ZSxLQUFLLENBQUNqQixHQUFOLENBQVVyZCxJQUFWLEVBQWVnRCxLQUFmLEVBQXFCaEMsSUFBckIsQ0FBTCxNQUFtQ1MsU0FBNUQsRUFBc0U7QUFBQyxpQkFBTy9CLEdBQVA7QUFBWTs7QUFDbkZNLFlBQUksQ0FBQzNCLFlBQUwsQ0FBa0IyQyxJQUFsQixFQUF1QmdDLEtBQUssR0FBQyxFQUE3QjtBQUFpQyxlQUFPQSxLQUFQO0FBQWM7O0FBQy9DLFVBQUdzYixLQUFLLElBQUUsU0FBUUEsS0FBZixJQUFzQixDQUFDNWUsR0FBRyxHQUFDNGUsS0FBSyxDQUFDaGYsR0FBTixDQUFVVSxJQUFWLEVBQWVnQixJQUFmLENBQUwsTUFBNkIsSUFBdEQsRUFBMkQ7QUFBQyxlQUFPdEIsR0FBUDtBQUFZOztBQUN4RUEsU0FBRyxHQUFDZCxNQUFNLENBQUNpTixJQUFQLENBQVl1QixJQUFaLENBQWlCcE4sSUFBakIsRUFBc0JnQixJQUF0QixDQUFKO0FBQWdDLGFBQU90QixHQUFHLElBQUUsSUFBTCxHQUFVK0IsU0FBVixHQUFvQi9CLEdBQTNCO0FBQWdDLEtBUG81QjtBQU9uNUJtNEIsYUFBUyxFQUFDO0FBQUN0NkIsVUFBSSxFQUFDO0FBQUM4ZixXQUFHLEVBQUMsYUFBU3JkLElBQVQsRUFBY2dELEtBQWQsRUFBb0I7QUFBQyxjQUFHLENBQUMvRixPQUFPLENBQUN3NkIsVUFBVCxJQUFxQnowQixLQUFLLEtBQUcsT0FBN0IsSUFBc0MwRSxRQUFRLENBQUMxSCxJQUFELEVBQU0sT0FBTixDQUFqRCxFQUFnRTtBQUFDLGdCQUFJaEMsR0FBRyxHQUFDZ0MsSUFBSSxDQUFDZ0QsS0FBYjtBQUFtQmhELGdCQUFJLENBQUMzQixZQUFMLENBQWtCLE1BQWxCLEVBQXlCMkUsS0FBekI7O0FBQWdDLGdCQUFHaEYsR0FBSCxFQUFPO0FBQUNnQyxrQkFBSSxDQUFDZ0QsS0FBTCxHQUFXaEYsR0FBWDtBQUFnQjs7QUFDdlAsbUJBQU9nRixLQUFQO0FBQWM7QUFBQztBQURrRTtBQUFOLEtBUHk0QjtBQVFsOEIyMEIsY0FBVSxFQUFDLG9CQUFTMzNCLElBQVQsRUFBY2dELEtBQWQsRUFBb0I7QUFBQyxVQUFJaEMsSUFBSjtBQUFBLFVBQVNqRCxDQUFDLEdBQUMsQ0FBWDtBQUFBLFVBQWFnNkIsU0FBUyxHQUFDLzBCLEtBQUssSUFBRUEsS0FBSyxDQUFDbUYsS0FBTixDQUFZMk8sYUFBWixDQUE5Qjs7QUFBeUQsVUFBR2loQixTQUFTLElBQUUvM0IsSUFBSSxDQUFDNUMsUUFBTCxLQUFnQixDQUE5QixFQUFnQztBQUFDLGVBQU80RCxJQUFJLEdBQUMrMkIsU0FBUyxDQUFDaDZCLENBQUMsRUFBRixDQUFyQixFQUE0QjtBQUFDaUMsY0FBSSxDQUFDcUosZUFBTCxDQUFxQnJJLElBQXJCO0FBQTRCO0FBQUM7QUFBQztBQVI2d0IsR0FBZDtBQVE1dkIwMkIsVUFBUSxHQUFDO0FBQUNyYSxPQUFHLEVBQUMsYUFBU3JkLElBQVQsRUFBY2dELEtBQWQsRUFBb0JoQyxJQUFwQixFQUF5QjtBQUFDLFVBQUdnQyxLQUFLLEtBQUcsS0FBWCxFQUFpQjtBQUFDcEUsY0FBTSxDQUFDKzRCLFVBQVAsQ0FBa0IzM0IsSUFBbEIsRUFBdUJnQixJQUF2QjtBQUE4QixPQUFoRCxNQUFvRDtBQUFDaEIsWUFBSSxDQUFDM0IsWUFBTCxDQUFrQjJDLElBQWxCLEVBQXVCQSxJQUF2QjtBQUE4Qjs7QUFDclUsYUFBT0EsSUFBUDtBQUFhO0FBRHNNLEdBQVQ7QUFDM0xwQyxRQUFNLENBQUNpQixJQUFQLENBQVlqQixNQUFNLENBQUNzTyxJQUFQLENBQVkvRSxLQUFaLENBQWtCMnZCLElBQWxCLENBQXVCaFosTUFBdkIsQ0FBOEIzVyxLQUE5QixDQUFvQyxNQUFwQyxDQUFaLEVBQXdELFVBQVM5RSxFQUFULEVBQVlyQyxJQUFaLEVBQWlCO0FBQUMsUUFBSWczQixNQUFNLEdBQUMvdEIsVUFBVSxDQUFDakosSUFBRCxDQUFWLElBQWtCcEMsTUFBTSxDQUFDaU4sSUFBUCxDQUFZdUIsSUFBekM7O0FBQThDbkQsY0FBVSxDQUFDakosSUFBRCxDQUFWLEdBQWlCLFVBQVNoQixJQUFULEVBQWNnQixJQUFkLEVBQW1CMEMsS0FBbkIsRUFBeUI7QUFBQyxVQUFJaEUsR0FBSjtBQUFBLFVBQVEya0IsTUFBUjtBQUFBLFVBQWU0VCxhQUFhLEdBQUNqM0IsSUFBSSxDQUFDc0MsV0FBTCxFQUE3Qjs7QUFBZ0QsVUFBRyxDQUFDSSxLQUFKLEVBQVU7QUFBQzJnQixjQUFNLEdBQUNwYSxVQUFVLENBQUNndUIsYUFBRCxDQUFqQjtBQUFpQ2h1QixrQkFBVSxDQUFDZ3VCLGFBQUQsQ0FBVixHQUEwQnY0QixHQUExQjtBQUE4QkEsV0FBRyxHQUFDczRCLE1BQU0sQ0FBQ2g0QixJQUFELEVBQU1nQixJQUFOLEVBQVcwQyxLQUFYLENBQU4sSUFBeUIsSUFBekIsR0FBOEJ1MEIsYUFBOUIsR0FBNEMsSUFBaEQ7QUFBcURodUIsa0JBQVUsQ0FBQ2d1QixhQUFELENBQVYsR0FBMEI1VCxNQUExQjtBQUFrQzs7QUFDblksYUFBTzNrQixHQUFQO0FBQVksS0FEMkg7QUFDekgsR0FEQztBQUNDLE1BQUl3NEIsVUFBVSxHQUFDLHFDQUFmO0FBQUEsTUFBcURDLFVBQVUsR0FBQyxlQUFoRTtBQUFnRnY1QixRQUFNLENBQUNHLEVBQVAsQ0FBVStCLE1BQVYsQ0FBaUI7QUFBQ3ljLFFBQUksRUFBQyxjQUFTdmMsSUFBVCxFQUFjZ0MsS0FBZCxFQUFvQjtBQUFDLGFBQU9pWixNQUFNLENBQUMsSUFBRCxFQUFNcmQsTUFBTSxDQUFDMmUsSUFBYixFQUFrQnZjLElBQWxCLEVBQXVCZ0MsS0FBdkIsRUFBNkIvQyxTQUFTLENBQUNiLE1BQVYsR0FBaUIsQ0FBOUMsQ0FBYjtBQUErRCxLQUExRjtBQUEyRmc1QixjQUFVLEVBQUMsb0JBQVNwM0IsSUFBVCxFQUFjO0FBQUMsYUFBTyxLQUFLbkIsSUFBTCxDQUFVLFlBQVU7QUFBQyxlQUFPLEtBQUtqQixNQUFNLENBQUN5NUIsT0FBUCxDQUFlcjNCLElBQWYsS0FBc0JBLElBQTNCLENBQVA7QUFBeUMsT0FBOUQsQ0FBUDtBQUF3RTtBQUE3TCxHQUFqQjtBQUFpTnBDLFFBQU0sQ0FBQ2tDLE1BQVAsQ0FBYztBQUFDeWMsUUFBSSxFQUFDLGNBQVN2ZCxJQUFULEVBQWNnQixJQUFkLEVBQW1CZ0MsS0FBbkIsRUFBeUI7QUFBQyxVQUFJdEQsR0FBSjtBQUFBLFVBQVE0ZSxLQUFSO0FBQUEsVUFBY3NaLEtBQUssR0FBQzUzQixJQUFJLENBQUM1QyxRQUF6Qjs7QUFBa0MsVUFBR3c2QixLQUFLLEtBQUcsQ0FBUixJQUFXQSxLQUFLLEtBQUcsQ0FBbkIsSUFBc0JBLEtBQUssS0FBRyxDQUFqQyxFQUFtQztBQUFDO0FBQVE7O0FBQzdhLFVBQUdBLEtBQUssS0FBRyxDQUFSLElBQVcsQ0FBQ2g1QixNQUFNLENBQUMwVixRQUFQLENBQWdCdFUsSUFBaEIsQ0FBZixFQUFxQztBQUFDZ0IsWUFBSSxHQUFDcEMsTUFBTSxDQUFDeTVCLE9BQVAsQ0FBZXIzQixJQUFmLEtBQXNCQSxJQUEzQjtBQUFnQ3NkLGFBQUssR0FBQzFmLE1BQU0sQ0FBQ3d6QixTQUFQLENBQWlCcHhCLElBQWpCLENBQU47QUFBOEI7O0FBQ3BHLFVBQUdnQyxLQUFLLEtBQUd2QixTQUFYLEVBQXFCO0FBQUMsWUFBRzZjLEtBQUssSUFBRSxTQUFRQSxLQUFmLElBQXNCLENBQUM1ZSxHQUFHLEdBQUM0ZSxLQUFLLENBQUNqQixHQUFOLENBQVVyZCxJQUFWLEVBQWVnRCxLQUFmLEVBQXFCaEMsSUFBckIsQ0FBTCxNQUFtQ1MsU0FBNUQsRUFBc0U7QUFBQyxpQkFBTy9CLEdBQVA7QUFBWTs7QUFDekcsZUFBT00sSUFBSSxDQUFDZ0IsSUFBRCxDQUFKLEdBQVdnQyxLQUFsQjtBQUEwQjs7QUFDMUIsVUFBR3NiLEtBQUssSUFBRSxTQUFRQSxLQUFmLElBQXNCLENBQUM1ZSxHQUFHLEdBQUM0ZSxLQUFLLENBQUNoZixHQUFOLENBQVVVLElBQVYsRUFBZWdCLElBQWYsQ0FBTCxNQUE2QixJQUF0RCxFQUEyRDtBQUFDLGVBQU90QixHQUFQO0FBQVk7O0FBQ3hFLGFBQU9NLElBQUksQ0FBQ2dCLElBQUQsQ0FBWDtBQUFtQixLQUw0UztBQUszU294QixhQUFTLEVBQUM7QUFBQzloQixjQUFRLEVBQUM7QUFBQ2hSLFdBQUcsRUFBQyxhQUFTVSxJQUFULEVBQWM7QUFBQyxjQUFJczRCLFFBQVEsR0FBQzE1QixNQUFNLENBQUNpTixJQUFQLENBQVl1QixJQUFaLENBQWlCcE4sSUFBakIsRUFBc0IsVUFBdEIsQ0FBYjs7QUFBK0MsY0FBR3M0QixRQUFILEVBQVk7QUFBQyxtQkFBTzlKLFFBQVEsQ0FBQzhKLFFBQUQsRUFBVSxFQUFWLENBQWY7QUFBOEI7O0FBQ3RKLGNBQUdKLFVBQVUsQ0FBQ3B2QixJQUFYLENBQWdCOUksSUFBSSxDQUFDMEgsUUFBckIsS0FBZ0N5d0IsVUFBVSxDQUFDcnZCLElBQVgsQ0FBZ0I5SSxJQUFJLENBQUMwSCxRQUFyQixLQUFnQzFILElBQUksQ0FBQ3FRLElBQXhFLEVBQTZFO0FBQUMsbUJBQU8sQ0FBUDtBQUFVOztBQUN4RixpQkFBTSxDQUFDLENBQVA7QUFBVTtBQUY4QjtBQUFWLEtBTGlTO0FBT2xUZ29CLFdBQU8sRUFBQztBQUFDLGFBQU0sU0FBUDtBQUFpQixlQUFRO0FBQXpCO0FBUDBTLEdBQWQ7O0FBT3BQLE1BQUcsQ0FBQ3A3QixPQUFPLENBQUN1NkIsV0FBWixFQUF3QjtBQUFDNTRCLFVBQU0sQ0FBQ3d6QixTQUFQLENBQWlCNWhCLFFBQWpCLEdBQTBCO0FBQUNsUixTQUFHLEVBQUMsYUFBU1UsSUFBVCxFQUFjO0FBQUMsWUFBSW9QLE1BQU0sR0FBQ3BQLElBQUksQ0FBQ3hCLFVBQWhCOztBQUEyQixZQUFHNFEsTUFBTSxJQUFFQSxNQUFNLENBQUM1USxVQUFsQixFQUE2QjtBQUFDNFEsZ0JBQU0sQ0FBQzVRLFVBQVAsQ0FBa0JpUyxhQUFsQjtBQUFpQzs7QUFDOU4sZUFBTyxJQUFQO0FBQWEsT0FEbUc7QUFDbEc0TSxTQUFHLEVBQUMsYUFBU3JkLElBQVQsRUFBYztBQUFDLFlBQUlvUCxNQUFNLEdBQUNwUCxJQUFJLENBQUN4QixVQUFoQjs7QUFBMkIsWUFBRzRRLE1BQUgsRUFBVTtBQUFDQSxnQkFBTSxDQUFDcUIsYUFBUDs7QUFBcUIsY0FBR3JCLE1BQU0sQ0FBQzVRLFVBQVYsRUFBcUI7QUFBQzRRLGtCQUFNLENBQUM1USxVQUFQLENBQWtCaVMsYUFBbEI7QUFBaUM7QUFBQztBQUFDO0FBRHJDLEtBQTFCO0FBQ2tFOztBQUN4SjdSLFFBQU0sQ0FBQ2lCLElBQVAsQ0FBWSxDQUFDLFVBQUQsRUFBWSxVQUFaLEVBQXVCLFdBQXZCLEVBQW1DLGFBQW5DLEVBQWlELGFBQWpELEVBQStELFNBQS9ELEVBQXlFLFNBQXpFLEVBQW1GLFFBQW5GLEVBQTRGLGFBQTVGLEVBQTBHLGlCQUExRyxDQUFaLEVBQXlJLFlBQVU7QUFBQ2pCLFVBQU0sQ0FBQ3k1QixPQUFQLENBQWUsS0FBSy8wQixXQUFMLEVBQWYsSUFBbUMsSUFBbkM7QUFBeUMsR0FBN0w7O0FBQStMLFdBQVNpMUIsZ0JBQVQsQ0FBMEJ2MUIsS0FBMUIsRUFBZ0M7QUFBQyxRQUFJb08sTUFBTSxHQUFDcE8sS0FBSyxDQUFDbUYsS0FBTixDQUFZMk8sYUFBWixLQUE0QixFQUF2QztBQUEwQyxXQUFPMUYsTUFBTSxDQUFDbEksSUFBUCxDQUFZLEdBQVosQ0FBUDtBQUF5Qjs7QUFDblMsV0FBU3N2QixRQUFULENBQWtCeDRCLElBQWxCLEVBQXVCO0FBQUMsV0FBT0EsSUFBSSxDQUFDNUIsWUFBTCxJQUFtQjRCLElBQUksQ0FBQzVCLFlBQUwsQ0FBa0IsT0FBbEIsQ0FBbkIsSUFBK0MsRUFBdEQ7QUFBMEQ7O0FBQ2xGLFdBQVNxNkIsY0FBVCxDQUF3QnoxQixLQUF4QixFQUE4QjtBQUFDLFFBQUd6QixLQUFLLENBQUNDLE9BQU4sQ0FBY3dCLEtBQWQsQ0FBSCxFQUF3QjtBQUFDLGFBQU9BLEtBQVA7QUFBYzs7QUFDdEUsUUFBRyxPQUFPQSxLQUFQLEtBQWUsUUFBbEIsRUFBMkI7QUFBQyxhQUFPQSxLQUFLLENBQUNtRixLQUFOLENBQVkyTyxhQUFaLEtBQTRCLEVBQW5DO0FBQXVDOztBQUNuRSxXQUFNLEVBQU47QUFBVTs7QUFDVmxZLFFBQU0sQ0FBQ0csRUFBUCxDQUFVK0IsTUFBVixDQUFpQjtBQUFDNDNCLFlBQVEsRUFBQyxrQkFBUzExQixLQUFULEVBQWU7QUFBQyxVQUFJMjFCLE9BQUo7QUFBQSxVQUFZMzRCLElBQVo7QUFBQSxVQUFpQm1LLEdBQWpCO0FBQUEsVUFBcUJ5dUIsUUFBckI7QUFBQSxVQUE4QkMsS0FBOUI7QUFBQSxVQUFvQ240QixDQUFwQztBQUFBLFVBQXNDbzRCLFVBQXRDO0FBQUEsVUFBaUQvNkIsQ0FBQyxHQUFDLENBQW5EOztBQUFxRCxVQUFHYixVQUFVLENBQUM4RixLQUFELENBQWIsRUFBcUI7QUFBQyxlQUFPLEtBQUtuRCxJQUFMLENBQVUsVUFBU2EsQ0FBVCxFQUFXO0FBQUM5QixnQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhODVCLFFBQWIsQ0FBc0IxMUIsS0FBSyxDQUFDMUcsSUFBTixDQUFXLElBQVgsRUFBZ0JvRSxDQUFoQixFQUFrQjgzQixRQUFRLENBQUMsSUFBRCxDQUExQixDQUF0QjtBQUEwRCxTQUFoRixDQUFQO0FBQTBGOztBQUNoTkcsYUFBTyxHQUFDRixjQUFjLENBQUN6MUIsS0FBRCxDQUF0Qjs7QUFBOEIsVUFBRzIxQixPQUFPLENBQUN2NUIsTUFBWCxFQUFrQjtBQUFDLGVBQU9ZLElBQUksR0FBQyxLQUFLakMsQ0FBQyxFQUFOLENBQVosRUFBdUI7QUFBQzY2QixrQkFBUSxHQUFDSixRQUFRLENBQUN4NEIsSUFBRCxDQUFqQjtBQUF3Qm1LLGFBQUcsR0FBQ25LLElBQUksQ0FBQzVDLFFBQUwsS0FBZ0IsQ0FBaEIsSUFBb0IsTUFBSW03QixnQkFBZ0IsQ0FBQ0ssUUFBRCxDQUFwQixHQUErQixHQUF2RDs7QUFBNEQsY0FBR3p1QixHQUFILEVBQU87QUFBQ3pKLGFBQUMsR0FBQyxDQUFGOztBQUFJLG1CQUFPbTRCLEtBQUssR0FBQ0YsT0FBTyxDQUFDajRCLENBQUMsRUFBRixDQUFwQixFQUEyQjtBQUFDLGtCQUFHeUosR0FBRyxDQUFDek4sT0FBSixDQUFZLE1BQUltOEIsS0FBSixHQUFVLEdBQXRCLElBQTJCLENBQTlCLEVBQWdDO0FBQUMxdUIsbUJBQUcsSUFBRTB1QixLQUFLLEdBQUMsR0FBWDtBQUFnQjtBQUFDOztBQUN2UEMsc0JBQVUsR0FBQ1AsZ0JBQWdCLENBQUNwdUIsR0FBRCxDQUEzQjs7QUFBaUMsZ0JBQUd5dUIsUUFBUSxLQUFHRSxVQUFkLEVBQXlCO0FBQUM5NEIsa0JBQUksQ0FBQzNCLFlBQUwsQ0FBa0IsT0FBbEIsRUFBMEJ5NkIsVUFBMUI7QUFBdUM7QUFBQztBQUFDO0FBQUM7O0FBQ3JHLGFBQU8sSUFBUDtBQUFhLEtBSEk7QUFHSEMsZUFBVyxFQUFDLHFCQUFTLzFCLEtBQVQsRUFBZTtBQUFDLFVBQUkyMUIsT0FBSjtBQUFBLFVBQVkzNEIsSUFBWjtBQUFBLFVBQWlCbUssR0FBakI7QUFBQSxVQUFxQnl1QixRQUFyQjtBQUFBLFVBQThCQyxLQUE5QjtBQUFBLFVBQW9DbjRCLENBQXBDO0FBQUEsVUFBc0NvNEIsVUFBdEM7QUFBQSxVQUFpRC82QixDQUFDLEdBQUMsQ0FBbkQ7O0FBQXFELFVBQUdiLFVBQVUsQ0FBQzhGLEtBQUQsQ0FBYixFQUFxQjtBQUFDLGVBQU8sS0FBS25ELElBQUwsQ0FBVSxVQUFTYSxDQUFULEVBQVc7QUFBQzlCLGdCQUFNLENBQUMsSUFBRCxDQUFOLENBQWFtNkIsV0FBYixDQUF5Qi8xQixLQUFLLENBQUMxRyxJQUFOLENBQVcsSUFBWCxFQUFnQm9FLENBQWhCLEVBQWtCODNCLFFBQVEsQ0FBQyxJQUFELENBQTFCLENBQXpCO0FBQTZELFNBQW5GLENBQVA7QUFBNkY7O0FBQ2xOLFVBQUcsQ0FBQ3Y0QixTQUFTLENBQUNiLE1BQWQsRUFBcUI7QUFBQyxlQUFPLEtBQUtnTyxJQUFMLENBQVUsT0FBVixFQUFrQixFQUFsQixDQUFQO0FBQThCOztBQUNwRHVyQixhQUFPLEdBQUNGLGNBQWMsQ0FBQ3oxQixLQUFELENBQXRCOztBQUE4QixVQUFHMjFCLE9BQU8sQ0FBQ3Y1QixNQUFYLEVBQWtCO0FBQUMsZUFBT1ksSUFBSSxHQUFDLEtBQUtqQyxDQUFDLEVBQU4sQ0FBWixFQUF1QjtBQUFDNjZCLGtCQUFRLEdBQUNKLFFBQVEsQ0FBQ3g0QixJQUFELENBQWpCO0FBQXdCbUssYUFBRyxHQUFDbkssSUFBSSxDQUFDNUMsUUFBTCxLQUFnQixDQUFoQixJQUFvQixNQUFJbTdCLGdCQUFnQixDQUFDSyxRQUFELENBQXBCLEdBQStCLEdBQXZEOztBQUE0RCxjQUFHenVCLEdBQUgsRUFBTztBQUFDekosYUFBQyxHQUFDLENBQUY7O0FBQUksbUJBQU9tNEIsS0FBSyxHQUFDRixPQUFPLENBQUNqNEIsQ0FBQyxFQUFGLENBQXBCLEVBQTJCO0FBQUMscUJBQU15SixHQUFHLENBQUN6TixPQUFKLENBQVksTUFBSW04QixLQUFKLEdBQVUsR0FBdEIsSUFBMkIsQ0FBQyxDQUFsQyxFQUFvQztBQUFDMXVCLG1CQUFHLEdBQUNBLEdBQUcsQ0FBQ3RJLE9BQUosQ0FBWSxNQUFJZzNCLEtBQUosR0FBVSxHQUF0QixFQUEwQixHQUExQixDQUFKO0FBQW9DO0FBQUM7O0FBQy9RQyxzQkFBVSxHQUFDUCxnQkFBZ0IsQ0FBQ3B1QixHQUFELENBQTNCOztBQUFpQyxnQkFBR3l1QixRQUFRLEtBQUdFLFVBQWQsRUFBeUI7QUFBQzk0QixrQkFBSSxDQUFDM0IsWUFBTCxDQUFrQixPQUFsQixFQUEwQnk2QixVQUExQjtBQUF1QztBQUFDO0FBQUM7QUFBQzs7QUFDckcsYUFBTyxJQUFQO0FBQWEsS0FQSTtBQU9IRSxlQUFXLEVBQUMscUJBQVNoMkIsS0FBVCxFQUFlaTJCLFFBQWYsRUFBd0I7QUFBQyxVQUFJMTdCLElBQUksV0FBUXlGLEtBQVIsQ0FBUjtBQUFBLFVBQXNCazJCLFlBQVksR0FBQzM3QixJQUFJLEtBQUcsUUFBUCxJQUFpQmdFLEtBQUssQ0FBQ0MsT0FBTixDQUFjd0IsS0FBZCxDQUFwRDs7QUFBeUUsVUFBRyxPQUFPaTJCLFFBQVAsS0FBa0IsU0FBbEIsSUFBNkJDLFlBQWhDLEVBQTZDO0FBQUMsZUFBT0QsUUFBUSxHQUFDLEtBQUtQLFFBQUwsQ0FBYzExQixLQUFkLENBQUQsR0FBc0IsS0FBSysxQixXQUFMLENBQWlCLzFCLEtBQWpCLENBQXJDO0FBQThEOztBQUN4TyxVQUFHOUYsVUFBVSxDQUFDOEYsS0FBRCxDQUFiLEVBQXFCO0FBQUMsZUFBTyxLQUFLbkQsSUFBTCxDQUFVLFVBQVM5QixDQUFULEVBQVc7QUFBQ2EsZ0JBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYW82QixXQUFiLENBQXlCaDJCLEtBQUssQ0FBQzFHLElBQU4sQ0FBVyxJQUFYLEVBQWdCeUIsQ0FBaEIsRUFBa0J5NkIsUUFBUSxDQUFDLElBQUQsQ0FBMUIsRUFBaUNTLFFBQWpDLENBQXpCLEVBQW9FQSxRQUFwRTtBQUErRSxTQUFyRyxDQUFQO0FBQStHOztBQUNySSxhQUFPLEtBQUtwNUIsSUFBTCxDQUFVLFlBQVU7QUFBQyxZQUFJMEwsU0FBSixFQUFjeE4sQ0FBZCxFQUFnQm9YLElBQWhCLEVBQXFCZ2tCLFVBQXJCOztBQUFnQyxZQUFHRCxZQUFILEVBQWdCO0FBQUNuN0IsV0FBQyxHQUFDLENBQUY7QUFBSW9YLGNBQUksR0FBQ3ZXLE1BQU0sQ0FBQyxJQUFELENBQVg7QUFBa0J1NkIsb0JBQVUsR0FBQ1YsY0FBYyxDQUFDejFCLEtBQUQsQ0FBekI7O0FBQWlDLGlCQUFPdUksU0FBUyxHQUFDNHRCLFVBQVUsQ0FBQ3A3QixDQUFDLEVBQUYsQ0FBM0IsRUFBa0M7QUFBQyxnQkFBR29YLElBQUksQ0FBQ2lrQixRQUFMLENBQWM3dEIsU0FBZCxDQUFILEVBQTRCO0FBQUM0SixrQkFBSSxDQUFDNGpCLFdBQUwsQ0FBaUJ4dEIsU0FBakI7QUFBNkIsYUFBMUQsTUFBOEQ7QUFBQzRKLGtCQUFJLENBQUN1akIsUUFBTCxDQUFjbnRCLFNBQWQ7QUFBMEI7QUFBQztBQUFDLFNBQXRNLE1BQTJNLElBQUd2SSxLQUFLLEtBQUd2QixTQUFSLElBQW1CbEUsSUFBSSxLQUFHLFNBQTdCLEVBQXVDO0FBQUNnTyxtQkFBUyxHQUFDaXRCLFFBQVEsQ0FBQyxJQUFELENBQWxCOztBQUF5QixjQUFHanRCLFNBQUgsRUFBYTtBQUFDa1Msb0JBQVEsQ0FBQ0osR0FBVCxDQUFhLElBQWIsRUFBa0IsZUFBbEIsRUFBa0M5UixTQUFsQztBQUE4Qzs7QUFDcFksY0FBRyxLQUFLbE4sWUFBUixFQUFxQjtBQUFDLGlCQUFLQSxZQUFMLENBQWtCLE9BQWxCLEVBQTBCa04sU0FBUyxJQUFFdkksS0FBSyxLQUFHLEtBQW5CLEdBQXlCLEVBQXpCLEdBQTRCeWEsUUFBUSxDQUFDbmUsR0FBVCxDQUFhLElBQWIsRUFBa0IsZUFBbEIsS0FBb0MsRUFBMUY7QUFBK0Y7QUFBQztBQUFDLE9BRGhILENBQVA7QUFDMEgsS0FWekc7QUFVMEc4NUIsWUFBUSxFQUFDLGtCQUFTdjZCLFFBQVQsRUFBa0I7QUFBQyxVQUFJME0sU0FBSjtBQUFBLFVBQWN2TCxJQUFkO0FBQUEsVUFBbUJqQyxDQUFDLEdBQUMsQ0FBckI7QUFBdUJ3TixlQUFTLEdBQUMsTUFBSTFNLFFBQUosR0FBYSxHQUF2Qjs7QUFBMkIsYUFBT21CLElBQUksR0FBQyxLQUFLakMsQ0FBQyxFQUFOLENBQVosRUFBdUI7QUFBQyxZQUFHaUMsSUFBSSxDQUFDNUMsUUFBTCxLQUFnQixDQUFoQixJQUFtQixDQUFDLE1BQUltN0IsZ0JBQWdCLENBQUNDLFFBQVEsQ0FBQ3g0QixJQUFELENBQVQsQ0FBcEIsR0FBcUMsR0FBdEMsRUFBMkN0RCxPQUEzQyxDQUFtRDZPLFNBQW5ELElBQThELENBQUMsQ0FBckYsRUFBdUY7QUFBQyxpQkFBTyxJQUFQO0FBQWE7QUFBQzs7QUFDdlUsYUFBTyxLQUFQO0FBQWM7QUFYRyxHQUFqQjtBQVdpQixNQUFJOHRCLE9BQU8sR0FBQyxLQUFaO0FBQWtCejZCLFFBQU0sQ0FBQ0csRUFBUCxDQUFVK0IsTUFBVixDQUFpQjtBQUFDOUMsT0FBRyxFQUFDLGFBQVNnRixLQUFULEVBQWU7QUFBQyxVQUFJc2IsS0FBSjtBQUFBLFVBQVU1ZSxHQUFWO0FBQUEsVUFBY2dyQixlQUFkO0FBQUEsVUFBOEIxcUIsSUFBSSxHQUFDLEtBQUssQ0FBTCxDQUFuQzs7QUFBMkMsVUFBRyxDQUFDQyxTQUFTLENBQUNiLE1BQWQsRUFBcUI7QUFBQyxZQUFHWSxJQUFILEVBQVE7QUFBQ3NlLGVBQUssR0FBQzFmLE1BQU0sQ0FBQzA2QixRQUFQLENBQWdCdDVCLElBQUksQ0FBQ3pDLElBQXJCLEtBQTRCcUIsTUFBTSxDQUFDMDZCLFFBQVAsQ0FBZ0J0NUIsSUFBSSxDQUFDMEgsUUFBTCxDQUFjcEUsV0FBZCxFQUFoQixDQUFsQzs7QUFBK0UsY0FBR2diLEtBQUssSUFBRSxTQUFRQSxLQUFmLElBQXNCLENBQUM1ZSxHQUFHLEdBQUM0ZSxLQUFLLENBQUNoZixHQUFOLENBQVVVLElBQVYsRUFBZSxPQUFmLENBQUwsTUFBZ0N5QixTQUF6RCxFQUFtRTtBQUFDLG1CQUFPL0IsR0FBUDtBQUFZOztBQUNsVEEsYUFBRyxHQUFDTSxJQUFJLENBQUNnRCxLQUFUOztBQUFlLGNBQUcsT0FBT3RELEdBQVAsS0FBYSxRQUFoQixFQUF5QjtBQUFDLG1CQUFPQSxHQUFHLENBQUNtQyxPQUFKLENBQVl3M0IsT0FBWixFQUFvQixFQUFwQixDQUFQO0FBQWdDOztBQUN6RSxpQkFBTzM1QixHQUFHLElBQUUsSUFBTCxHQUFVLEVBQVYsR0FBYUEsR0FBcEI7QUFBeUI7O0FBQ3pCO0FBQVE7O0FBQ1JnckIscUJBQWUsR0FBQ3h0QixVQUFVLENBQUM4RixLQUFELENBQTFCO0FBQWtDLGFBQU8sS0FBS25ELElBQUwsQ0FBVSxVQUFTOUIsQ0FBVCxFQUFXO0FBQUMsWUFBSUMsR0FBSjs7QUFBUSxZQUFHLEtBQUtaLFFBQUwsS0FBZ0IsQ0FBbkIsRUFBcUI7QUFBQztBQUFROztBQUNyRyxZQUFHc3RCLGVBQUgsRUFBbUI7QUFBQzFzQixhQUFHLEdBQUNnRixLQUFLLENBQUMxRyxJQUFOLENBQVcsSUFBWCxFQUFnQnlCLENBQWhCLEVBQWtCYSxNQUFNLENBQUMsSUFBRCxDQUFOLENBQWFaLEdBQWIsRUFBbEIsQ0FBSjtBQUEyQyxTQUEvRCxNQUFtRTtBQUFDQSxhQUFHLEdBQUNnRixLQUFKO0FBQVc7O0FBQy9FLFlBQUdoRixHQUFHLElBQUUsSUFBUixFQUFhO0FBQUNBLGFBQUcsR0FBQyxFQUFKO0FBQVEsU0FBdEIsTUFBMkIsSUFBRyxPQUFPQSxHQUFQLEtBQWEsUUFBaEIsRUFBeUI7QUFBQ0EsYUFBRyxJQUFFLEVBQUw7QUFBUyxTQUFuQyxNQUF3QyxJQUFHdUQsS0FBSyxDQUFDQyxPQUFOLENBQWN4RCxHQUFkLENBQUgsRUFBc0I7QUFBQ0EsYUFBRyxHQUFDWSxNQUFNLENBQUNtQixHQUFQLENBQVcvQixHQUFYLEVBQWUsVUFBU2dGLEtBQVQsRUFBZTtBQUFDLG1CQUFPQSxLQUFLLElBQUUsSUFBUCxHQUFZLEVBQVosR0FBZUEsS0FBSyxHQUFDLEVBQTVCO0FBQWdDLFdBQS9ELENBQUo7QUFBc0U7O0FBQ2hLc2IsYUFBSyxHQUFDMWYsTUFBTSxDQUFDMDZCLFFBQVAsQ0FBZ0IsS0FBSy83QixJQUFyQixLQUE0QnFCLE1BQU0sQ0FBQzA2QixRQUFQLENBQWdCLEtBQUs1eEIsUUFBTCxDQUFjcEUsV0FBZCxFQUFoQixDQUFsQzs7QUFBK0UsWUFBRyxDQUFDZ2IsS0FBRCxJQUFRLEVBQUUsU0FBUUEsS0FBVixDQUFSLElBQTBCQSxLQUFLLENBQUNqQixHQUFOLENBQVUsSUFBVixFQUFlcmYsR0FBZixFQUFtQixPQUFuQixNQUE4QnlELFNBQTNELEVBQXFFO0FBQUMsZUFBS3VCLEtBQUwsR0FBV2hGLEdBQVg7QUFBZ0I7QUFBQyxPQUg3SCxDQUFQO0FBR3VJO0FBUHJILEdBQWpCO0FBT3lJWSxRQUFNLENBQUNrQyxNQUFQLENBQWM7QUFBQ3c0QixZQUFRLEVBQUM7QUFBQ2xZLFlBQU0sRUFBQztBQUFDOWhCLFdBQUcsRUFBQyxhQUFTVSxJQUFULEVBQWM7QUFBQyxjQUFJaEMsR0FBRyxHQUFDWSxNQUFNLENBQUNpTixJQUFQLENBQVl1QixJQUFaLENBQWlCcE4sSUFBakIsRUFBc0IsT0FBdEIsQ0FBUjtBQUF1QyxpQkFBT2hDLEdBQUcsSUFBRSxJQUFMLEdBQVVBLEdBQVYsR0FBY3U2QixnQkFBZ0IsQ0FBQzM1QixNQUFNLENBQUNULElBQVAsQ0FBWTZCLElBQVosQ0FBRCxDQUFyQztBQUEwRDtBQUFySCxPQUFSO0FBQStINkQsWUFBTSxFQUFDO0FBQUN2RSxXQUFHLEVBQUMsYUFBU1UsSUFBVCxFQUFjO0FBQUMsY0FBSWdELEtBQUo7QUFBQSxjQUFVb2UsTUFBVjtBQUFBLGNBQWlCcmpCLENBQWpCO0FBQUEsY0FBbUJnRCxPQUFPLEdBQUNmLElBQUksQ0FBQ2UsT0FBaEM7QUFBQSxjQUF3Q2tWLEtBQUssR0FBQ2pXLElBQUksQ0FBQ3lRLGFBQW5EO0FBQUEsY0FBaUU4UyxHQUFHLEdBQUN2akIsSUFBSSxDQUFDekMsSUFBTCxLQUFZLFlBQWpGO0FBQUEsY0FBOEZpakIsTUFBTSxHQUFDK0MsR0FBRyxHQUFDLElBQUQsR0FBTSxFQUE5RztBQUFBLGNBQWlIMk0sR0FBRyxHQUFDM00sR0FBRyxHQUFDdE4sS0FBSyxHQUFDLENBQVAsR0FBU2xWLE9BQU8sQ0FBQzNCLE1BQXpJOztBQUFnSixjQUFHNlcsS0FBSyxHQUFDLENBQVQsRUFBVztBQUFDbFksYUFBQyxHQUFDbXlCLEdBQUY7QUFBTyxXQUFuQixNQUF1QjtBQUFDbnlCLGFBQUMsR0FBQ3dsQixHQUFHLEdBQUN0TixLQUFELEdBQU8sQ0FBWjtBQUFlOztBQUNyaEIsaUJBQUtsWSxDQUFDLEdBQUNteUIsR0FBUCxFQUFXbnlCLENBQUMsRUFBWixFQUFlO0FBQUNxakIsa0JBQU0sR0FBQ3JnQixPQUFPLENBQUNoRCxDQUFELENBQWQ7O0FBQWtCLGdCQUFHLENBQUNxakIsTUFBTSxDQUFDNVEsUUFBUCxJQUFpQnpTLENBQUMsS0FBR2tZLEtBQXRCLEtBQThCLENBQUNtTCxNQUFNLENBQUMzWixRQUF0QyxLQUFpRCxDQUFDMlosTUFBTSxDQUFDNWlCLFVBQVAsQ0FBa0JpSixRQUFuQixJQUE2QixDQUFDQyxRQUFRLENBQUMwWixNQUFNLENBQUM1aUIsVUFBUixFQUFtQixVQUFuQixDQUF2RixDQUFILEVBQTBIO0FBQUN3RSxtQkFBSyxHQUFDcEUsTUFBTSxDQUFDd2lCLE1BQUQsQ0FBTixDQUFlcGpCLEdBQWYsRUFBTjs7QUFBMkIsa0JBQUd1bEIsR0FBSCxFQUFPO0FBQUMsdUJBQU92Z0IsS0FBUDtBQUFjOztBQUM5TXdkLG9CQUFNLENBQUMvakIsSUFBUCxDQUFZdUcsS0FBWjtBQUFvQjtBQUFDOztBQUNyQixpQkFBT3dkLE1BQVA7QUFBZSxTQUgyVDtBQUcxVG5ELFdBQUcsRUFBQyxhQUFTcmQsSUFBVCxFQUFjZ0QsS0FBZCxFQUFvQjtBQUFDLGNBQUl1MkIsU0FBSjtBQUFBLGNBQWNuWSxNQUFkO0FBQUEsY0FBcUJyZ0IsT0FBTyxHQUFDZixJQUFJLENBQUNlLE9BQWxDO0FBQUEsY0FBMEN5ZixNQUFNLEdBQUM1aEIsTUFBTSxDQUFDMkQsU0FBUCxDQUFpQlMsS0FBakIsQ0FBakQ7QUFBQSxjQUF5RWpGLENBQUMsR0FBQ2dELE9BQU8sQ0FBQzNCLE1BQW5GOztBQUEwRixpQkFBTXJCLENBQUMsRUFBUCxFQUFVO0FBQUNxakIsa0JBQU0sR0FBQ3JnQixPQUFPLENBQUNoRCxDQUFELENBQWQ7O0FBQWtCLGdCQUFHcWpCLE1BQU0sQ0FBQzVRLFFBQVAsR0FBZ0I1UixNQUFNLENBQUM2RCxPQUFQLENBQWU3RCxNQUFNLENBQUMwNkIsUUFBUCxDQUFnQmxZLE1BQWhCLENBQXVCOWhCLEdBQXZCLENBQTJCOGhCLE1BQTNCLENBQWYsRUFBa0RaLE1BQWxELElBQTBELENBQUMsQ0FBOUUsRUFBZ0Y7QUFBQytZLHVCQUFTLEdBQUMsSUFBVjtBQUFnQjtBQUFDOztBQUNsUSxjQUFHLENBQUNBLFNBQUosRUFBYztBQUFDdjVCLGdCQUFJLENBQUN5USxhQUFMLEdBQW1CLENBQUMsQ0FBcEI7QUFBdUI7O0FBQ3RDLGlCQUFPK1AsTUFBUDtBQUFlO0FBTDJUO0FBQXRJO0FBQVYsR0FBZDtBQUt4SjVoQixRQUFNLENBQUNpQixJQUFQLENBQVksQ0FBQyxPQUFELEVBQVMsVUFBVCxDQUFaLEVBQWlDLFlBQVU7QUFBQ2pCLFVBQU0sQ0FBQzA2QixRQUFQLENBQWdCLElBQWhCLElBQXNCO0FBQUNqYyxTQUFHLEVBQUMsYUFBU3JkLElBQVQsRUFBY2dELEtBQWQsRUFBb0I7QUFBQyxZQUFHekIsS0FBSyxDQUFDQyxPQUFOLENBQWN3QixLQUFkLENBQUgsRUFBd0I7QUFBQyxpQkFBT2hELElBQUksQ0FBQ3VRLE9BQUwsR0FBYTNSLE1BQU0sQ0FBQzZELE9BQVAsQ0FBZTdELE1BQU0sQ0FBQ29CLElBQUQsQ0FBTixDQUFhaEMsR0FBYixFQUFmLEVBQWtDZ0YsS0FBbEMsSUFBeUMsQ0FBQyxDQUE5RDtBQUFrRTtBQUFDO0FBQXRILEtBQXRCOztBQUE4SSxRQUFHLENBQUMvRixPQUFPLENBQUNzNkIsT0FBWixFQUFvQjtBQUFDMzRCLFlBQU0sQ0FBQzA2QixRQUFQLENBQWdCLElBQWhCLEVBQXNCaDZCLEdBQXRCLEdBQTBCLFVBQVNVLElBQVQsRUFBYztBQUFDLGVBQU9BLElBQUksQ0FBQzVCLFlBQUwsQ0FBa0IsT0FBbEIsTUFBNkIsSUFBN0IsR0FBa0MsSUFBbEMsR0FBdUM0QixJQUFJLENBQUNnRCxLQUFuRDtBQUEwRCxPQUFuRztBQUFxRztBQUFDLEdBQXJUO0FBQXVUL0YsU0FBTyxDQUFDdThCLE9BQVIsR0FBZ0IsZUFBYzM5QixNQUE5Qjs7QUFBcUMsTUFBSTQ5QixXQUFXLEdBQUMsaUNBQWhCO0FBQUEsTUFBa0RDLHVCQUF1QixHQUFDLFNBQXhCQSx1QkFBd0IsQ0FBUzV4QixDQUFULEVBQVc7QUFBQ0EsS0FBQyxDQUFDNGQsZUFBRjtBQUFxQixHQUEzRzs7QUFBNEc5bUIsUUFBTSxDQUFDa0MsTUFBUCxDQUFjbEMsTUFBTSxDQUFDNmtCLEtBQXJCLEVBQTJCO0FBQUNnRCxXQUFPLEVBQUMsaUJBQVNoRCxLQUFULEVBQWVuRyxJQUFmLEVBQW9CdGQsSUFBcEIsRUFBeUIyNUIsWUFBekIsRUFBc0M7QUFBQyxVQUFJNTdCLENBQUo7QUFBQSxVQUFNb00sR0FBTjtBQUFBLFVBQVU2QixHQUFWO0FBQUEsVUFBYzR0QixVQUFkO0FBQUEsVUFBeUJDLE1BQXpCO0FBQUEsVUFBZ0N4VixNQUFoQztBQUFBLFVBQXVDckssT0FBdkM7QUFBQSxVQUErQzhmLFdBQS9DO0FBQUEsVUFBMkRDLFNBQVMsR0FBQyxDQUFDLzVCLElBQUksSUFBRXRFLFFBQVAsQ0FBckU7QUFBQSxVQUFzRjZCLElBQUksR0FBQ1YsTUFBTSxDQUFDUCxJQUFQLENBQVltbkIsS0FBWixFQUFrQixNQUFsQixJQUEwQkEsS0FBSyxDQUFDbG1CLElBQWhDLEdBQXFDa21CLEtBQWhJO0FBQUEsVUFBc0lRLFVBQVUsR0FBQ3BuQixNQUFNLENBQUNQLElBQVAsQ0FBWW1uQixLQUFaLEVBQWtCLFdBQWxCLElBQStCQSxLQUFLLENBQUMzWSxTQUFOLENBQWdCMUgsS0FBaEIsQ0FBc0IsR0FBdEIsQ0FBL0IsR0FBMEQsRUFBM007QUFBOE0rRyxTQUFHLEdBQUMydkIsV0FBVyxHQUFDOXRCLEdBQUcsR0FBQ2hNLElBQUksR0FBQ0EsSUFBSSxJQUFFdEUsUUFBL0I7O0FBQXdDLFVBQUdzRSxJQUFJLENBQUM1QyxRQUFMLEtBQWdCLENBQWhCLElBQW1CNEMsSUFBSSxDQUFDNUMsUUFBTCxLQUFnQixDQUF0QyxFQUF3QztBQUFDO0FBQVE7O0FBQzkwQixVQUFHcThCLFdBQVcsQ0FBQzN3QixJQUFaLENBQWlCdkwsSUFBSSxHQUFDcUIsTUFBTSxDQUFDNmtCLEtBQVAsQ0FBYWEsU0FBbkMsQ0FBSCxFQUFpRDtBQUFDO0FBQVE7O0FBQzFELFVBQUcvbUIsSUFBSSxDQUFDYixPQUFMLENBQWEsR0FBYixJQUFrQixDQUFDLENBQXRCLEVBQXdCO0FBQUN1bkIsa0JBQVUsR0FBQzFtQixJQUFJLENBQUM2RixLQUFMLENBQVcsR0FBWCxDQUFYO0FBQTJCN0YsWUFBSSxHQUFDMG1CLFVBQVUsQ0FBQ3ZhLEtBQVgsRUFBTDtBQUF3QnVhLGtCQUFVLENBQUNyakIsSUFBWDtBQUFtQjs7QUFDL0ZpNUIsWUFBTSxHQUFDdDhCLElBQUksQ0FBQ2IsT0FBTCxDQUFhLEdBQWIsSUFBa0IsQ0FBbEIsSUFBcUIsT0FBS2EsSUFBakM7QUFBc0NrbUIsV0FBSyxHQUFDQSxLQUFLLENBQUM3a0IsTUFBTSxDQUFDOEMsT0FBUixDQUFMLEdBQXNCK2hCLEtBQXRCLEdBQTRCLElBQUk3a0IsTUFBTSxDQUFDcW5CLEtBQVgsQ0FBaUIxb0IsSUFBakIsRUFBc0IsUUFBT2ttQixLQUFQLE1BQWUsUUFBZixJQUF5QkEsS0FBL0MsQ0FBbEM7QUFBd0ZBLFdBQUssQ0FBQ3FELFNBQU4sR0FBZ0I2UyxZQUFZLEdBQUMsQ0FBRCxHQUFHLENBQS9CO0FBQWlDbFcsV0FBSyxDQUFDM1ksU0FBTixHQUFnQm1aLFVBQVUsQ0FBQy9hLElBQVgsQ0FBZ0IsR0FBaEIsQ0FBaEI7QUFBcUN1YSxXQUFLLENBQUMrQixVQUFOLEdBQWlCL0IsS0FBSyxDQUFDM1ksU0FBTixHQUFnQixJQUFJbEYsTUFBSixDQUFXLFlBQVVxZSxVQUFVLENBQUMvYSxJQUFYLENBQWdCLGVBQWhCLENBQVYsR0FBMkMsU0FBdEQsQ0FBaEIsR0FBaUYsSUFBbEc7QUFBdUd1YSxXQUFLLENBQUNqVixNQUFOLEdBQWEvTSxTQUFiOztBQUF1QixVQUFHLENBQUNnaUIsS0FBSyxDQUFDcmlCLE1BQVYsRUFBaUI7QUFBQ3FpQixhQUFLLENBQUNyaUIsTUFBTixHQUFhcEIsSUFBYjtBQUFtQjs7QUFDdldzZCxVQUFJLEdBQUNBLElBQUksSUFBRSxJQUFOLEdBQVcsQ0FBQ21HLEtBQUQsQ0FBWCxHQUFtQjdrQixNQUFNLENBQUMyRCxTQUFQLENBQWlCK2EsSUFBakIsRUFBc0IsQ0FBQ21HLEtBQUQsQ0FBdEIsQ0FBeEI7QUFBdUR6SixhQUFPLEdBQUNwYixNQUFNLENBQUM2a0IsS0FBUCxDQUFhekosT0FBYixDQUFxQnpjLElBQXJCLEtBQTRCLEVBQXBDOztBQUF1QyxVQUFHLENBQUNvOEIsWUFBRCxJQUFlM2YsT0FBTyxDQUFDeU0sT0FBdkIsSUFBZ0N6TSxPQUFPLENBQUN5TSxPQUFSLENBQWdCanFCLEtBQWhCLENBQXNCd0QsSUFBdEIsRUFBMkJzZCxJQUEzQixNQUFtQyxLQUF0RSxFQUE0RTtBQUFDO0FBQVE7O0FBQ25MLFVBQUcsQ0FBQ3FjLFlBQUQsSUFBZSxDQUFDM2YsT0FBTyxDQUFDc00sUUFBeEIsSUFBa0MsQ0FBQ2pwQixRQUFRLENBQUMyQyxJQUFELENBQTlDLEVBQXFEO0FBQUM0NUIsa0JBQVUsR0FBQzVmLE9BQU8sQ0FBQ3dLLFlBQVIsSUFBc0JqbkIsSUFBakM7O0FBQXNDLFlBQUcsQ0FBQ2s4QixXQUFXLENBQUMzd0IsSUFBWixDQUFpQjh3QixVQUFVLEdBQUNyOEIsSUFBNUIsQ0FBSixFQUFzQztBQUFDNE0sYUFBRyxHQUFDQSxHQUFHLENBQUMzTCxVQUFSO0FBQW9COztBQUN2SixlQUFLMkwsR0FBTCxFQUFTQSxHQUFHLEdBQUNBLEdBQUcsQ0FBQzNMLFVBQWpCLEVBQTRCO0FBQUN1N0IsbUJBQVMsQ0FBQ3Q5QixJQUFWLENBQWUwTixHQUFmO0FBQW9CNkIsYUFBRyxHQUFDN0IsR0FBSjtBQUFTOztBQUMxRCxZQUFHNkIsR0FBRyxNQUFJaE0sSUFBSSxDQUFDdUksYUFBTCxJQUFvQjdNLFFBQXhCLENBQU4sRUFBd0M7QUFBQ3ErQixtQkFBUyxDQUFDdDlCLElBQVYsQ0FBZXVQLEdBQUcsQ0FBQ2IsV0FBSixJQUFpQmEsR0FBRyxDQUFDZ3VCLFlBQXJCLElBQW1DbitCLE1BQWxEO0FBQTJEO0FBQUM7O0FBQ3JHa0MsT0FBQyxHQUFDLENBQUY7O0FBQUksYUFBTSxDQUFDb00sR0FBRyxHQUFDNHZCLFNBQVMsQ0FBQ2g4QixDQUFDLEVBQUYsQ0FBZCxLQUFzQixDQUFDMGxCLEtBQUssQ0FBQzRCLG9CQUFOLEVBQTdCLEVBQTBEO0FBQUN5VSxtQkFBVyxHQUFDM3ZCLEdBQVo7QUFBZ0JzWixhQUFLLENBQUNsbUIsSUFBTixHQUFXUSxDQUFDLEdBQUMsQ0FBRixHQUFJNjdCLFVBQUosR0FBZTVmLE9BQU8sQ0FBQ3lLLFFBQVIsSUFBa0JsbkIsSUFBNUM7QUFBaUQ4bUIsY0FBTSxHQUFDLENBQUM1RyxRQUFRLENBQUNuZSxHQUFULENBQWE2SyxHQUFiLEVBQWlCLFFBQWpCLEtBQTRCbE8sTUFBTSxDQUFDbW9CLE1BQVAsQ0FBYyxJQUFkLENBQTdCLEVBQWtEWCxLQUFLLENBQUNsbUIsSUFBeEQsS0FBK0RrZ0IsUUFBUSxDQUFDbmUsR0FBVCxDQUFhNkssR0FBYixFQUFpQixRQUFqQixDQUF0RTs7QUFBaUcsWUFBR2thLE1BQUgsRUFBVTtBQUFDQSxnQkFBTSxDQUFDN25CLEtBQVAsQ0FBYTJOLEdBQWIsRUFBaUJtVCxJQUFqQjtBQUF3Qjs7QUFDcFErRyxjQUFNLEdBQUN3VixNQUFNLElBQUUxdkIsR0FBRyxDQUFDMHZCLE1BQUQsQ0FBbEI7O0FBQTJCLFlBQUd4VixNQUFNLElBQUVBLE1BQU0sQ0FBQzduQixLQUFmLElBQXNCdWdCLFVBQVUsQ0FBQzVTLEdBQUQsQ0FBbkMsRUFBeUM7QUFBQ3NaLGVBQUssQ0FBQ2pWLE1BQU4sR0FBYTZWLE1BQU0sQ0FBQzduQixLQUFQLENBQWEyTixHQUFiLEVBQWlCbVQsSUFBakIsQ0FBYjs7QUFBb0MsY0FBR21HLEtBQUssQ0FBQ2pWLE1BQU4sS0FBZSxLQUFsQixFQUF3QjtBQUFDaVYsaUJBQUssQ0FBQ2dDLGNBQU47QUFBd0I7QUFBQztBQUFDOztBQUM1SmhDLFdBQUssQ0FBQ2xtQixJQUFOLEdBQVdBLElBQVg7O0FBQWdCLFVBQUcsQ0FBQ284QixZQUFELElBQWUsQ0FBQ2xXLEtBQUssQ0FBQ3dELGtCQUFOLEVBQW5CLEVBQThDO0FBQUMsWUFBRyxDQUFDLENBQUNqTixPQUFPLENBQUMwSCxRQUFULElBQW1CMUgsT0FBTyxDQUFDMEgsUUFBUixDQUFpQmxsQixLQUFqQixDQUF1QnU5QixTQUFTLENBQUM1MEIsR0FBVixFQUF2QixFQUF1Q21ZLElBQXZDLE1BQStDLEtBQW5FLEtBQTJFUCxVQUFVLENBQUMvYyxJQUFELENBQXhGLEVBQStGO0FBQUMsY0FBRzY1QixNQUFNLElBQUUzOEIsVUFBVSxDQUFDOEMsSUFBSSxDQUFDekMsSUFBRCxDQUFMLENBQWxCLElBQWdDLENBQUNGLFFBQVEsQ0FBQzJDLElBQUQsQ0FBNUMsRUFBbUQ7QUFBQ2dNLGVBQUcsR0FBQ2hNLElBQUksQ0FBQzY1QixNQUFELENBQVI7O0FBQWlCLGdCQUFHN3RCLEdBQUgsRUFBTztBQUFDaE0sa0JBQUksQ0FBQzY1QixNQUFELENBQUosR0FBYSxJQUFiO0FBQW1COztBQUMvUGo3QixrQkFBTSxDQUFDNmtCLEtBQVAsQ0FBYWEsU0FBYixHQUF1Qi9tQixJQUF2Qjs7QUFBNEIsZ0JBQUdrbUIsS0FBSyxDQUFDNEIsb0JBQU4sRUFBSCxFQUFnQztBQUFDeVUseUJBQVcsQ0FBQ3p1QixnQkFBWixDQUE2QjlOLElBQTdCLEVBQWtDbThCLHVCQUFsQztBQUE0RDs7QUFDekgxNUIsZ0JBQUksQ0FBQ3pDLElBQUQsQ0FBSjs7QUFBYSxnQkFBR2ttQixLQUFLLENBQUM0QixvQkFBTixFQUFILEVBQWdDO0FBQUN5VSx5QkFBVyxDQUFDaGUsbUJBQVosQ0FBZ0N2ZSxJQUFoQyxFQUFxQ204Qix1QkFBckM7QUFBK0Q7O0FBQzdHOTZCLGtCQUFNLENBQUM2a0IsS0FBUCxDQUFhYSxTQUFiLEdBQXVCN2lCLFNBQXZCOztBQUFpQyxnQkFBR3VLLEdBQUgsRUFBTztBQUFDaE0sa0JBQUksQ0FBQzY1QixNQUFELENBQUosR0FBYTd0QixHQUFiO0FBQWtCO0FBQUM7QUFBQztBQUFDOztBQUM5RCxhQUFPeVgsS0FBSyxDQUFDalYsTUFBYjtBQUFxQixLQWRrZTtBQWNqZXlyQixZQUFRLEVBQUMsa0JBQVMxOEIsSUFBVCxFQUFjeUMsSUFBZCxFQUFtQnlqQixLQUFuQixFQUF5QjtBQUFDLFVBQUkzYixDQUFDLEdBQUNsSixNQUFNLENBQUNrQyxNQUFQLENBQWMsSUFBSWxDLE1BQU0sQ0FBQ3FuQixLQUFYLEVBQWQsRUFBaUN4QyxLQUFqQyxFQUF1QztBQUFDbG1CLFlBQUksRUFBQ0EsSUFBTjtBQUFXK3BCLG1CQUFXLEVBQUM7QUFBdkIsT0FBdkMsQ0FBTjtBQUEyRTFvQixZQUFNLENBQUM2a0IsS0FBUCxDQUFhZ0QsT0FBYixDQUFxQjNlLENBQXJCLEVBQXVCLElBQXZCLEVBQTRCOUgsSUFBNUI7QUFBbUM7QUFkZ1YsR0FBM0I7QUFjbFRwQixRQUFNLENBQUNHLEVBQVAsQ0FBVStCLE1BQVYsQ0FBaUI7QUFBQzJsQixXQUFPLEVBQUMsaUJBQVNscEIsSUFBVCxFQUFjK2YsSUFBZCxFQUFtQjtBQUFDLGFBQU8sS0FBS3pkLElBQUwsQ0FBVSxZQUFVO0FBQUNqQixjQUFNLENBQUM2a0IsS0FBUCxDQUFhZ0QsT0FBYixDQUFxQmxwQixJQUFyQixFQUEwQitmLElBQTFCLEVBQStCLElBQS9CO0FBQXNDLE9BQTNELENBQVA7QUFBcUUsS0FBbEc7QUFBbUc0YyxrQkFBYyxFQUFDLHdCQUFTMzhCLElBQVQsRUFBYytmLElBQWQsRUFBbUI7QUFBQyxVQUFJdGQsSUFBSSxHQUFDLEtBQUssQ0FBTCxDQUFUOztBQUFpQixVQUFHQSxJQUFILEVBQVE7QUFBQyxlQUFPcEIsTUFBTSxDQUFDNmtCLEtBQVAsQ0FBYWdELE9BQWIsQ0FBcUJscEIsSUFBckIsRUFBMEIrZixJQUExQixFQUErQnRkLElBQS9CLEVBQW9DLElBQXBDLENBQVA7QUFBa0Q7QUFBQztBQUFuTixHQUFqQjs7QUFBdU8sTUFBRyxDQUFDL0MsT0FBTyxDQUFDdThCLE9BQVosRUFBb0I7QUFBQzU2QixVQUFNLENBQUNpQixJQUFQLENBQVk7QUFBQ3FwQixXQUFLLEVBQUMsU0FBUDtBQUFpQkMsVUFBSSxFQUFDO0FBQXRCLEtBQVosRUFBOEMsVUFBU0ssSUFBVCxFQUFjdEUsR0FBZCxFQUFrQjtBQUFDLFVBQUlsYixPQUFPLEdBQUMsU0FBUkEsT0FBUSxDQUFTeVosS0FBVCxFQUFlO0FBQUM3a0IsY0FBTSxDQUFDNmtCLEtBQVAsQ0FBYXdXLFFBQWIsQ0FBc0IvVSxHQUF0QixFQUEwQnpCLEtBQUssQ0FBQ3JpQixNQUFoQyxFQUF1Q3hDLE1BQU0sQ0FBQzZrQixLQUFQLENBQWF5QixHQUFiLENBQWlCekIsS0FBakIsQ0FBdkM7QUFBaUUsT0FBN0Y7O0FBQThGN2tCLFlBQU0sQ0FBQzZrQixLQUFQLENBQWF6SixPQUFiLENBQXFCa0wsR0FBckIsSUFBMEI7QUFBQ1AsYUFBSyxFQUFDLGlCQUFVO0FBQUMsY0FBSTdtQixHQUFHLEdBQUMsS0FBS3lLLGFBQUwsSUFBb0IsS0FBSzdNLFFBQXpCLElBQW1DLElBQTNDO0FBQUEsY0FBZ0R5K0IsUUFBUSxHQUFDMWMsUUFBUSxDQUFDeEIsTUFBVCxDQUFnQm5lLEdBQWhCLEVBQW9Cb25CLEdBQXBCLENBQXpEOztBQUFrRixjQUFHLENBQUNpVixRQUFKLEVBQWE7QUFBQ3I4QixlQUFHLENBQUN1TixnQkFBSixDQUFxQm1lLElBQXJCLEVBQTBCeGYsT0FBMUIsRUFBa0MsSUFBbEM7QUFBeUM7O0FBQzF2QnlULGtCQUFRLENBQUN4QixNQUFULENBQWdCbmUsR0FBaEIsRUFBb0JvbkIsR0FBcEIsRUFBd0IsQ0FBQ2lWLFFBQVEsSUFBRSxDQUFYLElBQWMsQ0FBdEM7QUFBMEMsU0FEcWpCO0FBQ3BqQnJWLGdCQUFRLEVBQUMsb0JBQVU7QUFBQyxjQUFJaG5CLEdBQUcsR0FBQyxLQUFLeUssYUFBTCxJQUFvQixLQUFLN00sUUFBekIsSUFBbUMsSUFBM0M7QUFBQSxjQUFnRHkrQixRQUFRLEdBQUMxYyxRQUFRLENBQUN4QixNQUFULENBQWdCbmUsR0FBaEIsRUFBb0JvbkIsR0FBcEIsSUFBeUIsQ0FBbEY7O0FBQW9GLGNBQUcsQ0FBQ2lWLFFBQUosRUFBYTtBQUFDcjhCLGVBQUcsQ0FBQ2dlLG1CQUFKLENBQXdCME4sSUFBeEIsRUFBNkJ4ZixPQUE3QixFQUFxQyxJQUFyQztBQUEyQ3lULG9CQUFRLENBQUM1RixNQUFULENBQWdCL1osR0FBaEIsRUFBb0JvbkIsR0FBcEI7QUFBMEIsV0FBbkYsTUFBdUY7QUFBQ3pILG9CQUFRLENBQUN4QixNQUFULENBQWdCbmUsR0FBaEIsRUFBb0JvbkIsR0FBcEIsRUFBd0JpVixRQUF4QjtBQUFtQztBQUFDO0FBRGdWLE9BQTFCO0FBQ25ULEtBRG9KO0FBQ2pKOztBQUNyUixNQUFJanFCLFFBQVEsR0FBQ3JVLE1BQU0sQ0FBQ3FVLFFBQXBCO0FBQTZCLE1BQUl6UyxLQUFLLEdBQUM7QUFBQ3dGLFFBQUksRUFBQ3NCLElBQUksQ0FBQzhpQixHQUFMO0FBQU4sR0FBVjtBQUE0QixNQUFJK1MsTUFBTSxHQUFFLElBQVo7O0FBQWtCeDdCLFFBQU0sQ0FBQ3k3QixRQUFQLEdBQWdCLFVBQVMvYyxJQUFULEVBQWM7QUFBQyxRQUFJdk8sR0FBSjs7QUFBUSxRQUFHLENBQUN1TyxJQUFELElBQU8sT0FBT0EsSUFBUCxLQUFjLFFBQXhCLEVBQWlDO0FBQUMsYUFBTyxJQUFQO0FBQWE7O0FBQ2pLLFFBQUc7QUFBQ3ZPLFNBQUcsR0FBRSxJQUFJbFQsTUFBTSxDQUFDeStCLFNBQVgsRUFBRCxDQUF5QkMsZUFBekIsQ0FBeUNqZCxJQUF6QyxFQUE4QyxVQUE5QyxDQUFKO0FBQStELEtBQW5FLENBQW1FLE9BQU14VixDQUFOLEVBQVE7QUFBQ2lILFNBQUcsR0FBQ3ROLFNBQUo7QUFBZTs7QUFDM0YsUUFBRyxDQUFDc04sR0FBRCxJQUFNQSxHQUFHLENBQUNwRyxvQkFBSixDQUF5QixhQUF6QixFQUF3Q3ZKLE1BQWpELEVBQXdEO0FBQUNSLFlBQU0sQ0FBQ21ELEtBQVAsQ0FBYSxrQkFBZ0J1YixJQUE3QjtBQUFvQzs7QUFDN0YsV0FBT3ZPLEdBQVA7QUFBWSxHQUgrRDs7QUFHOUQsTUFDYnlyQixRQUFRLEdBQUMsT0FESTtBQUFBLE1BQ0lDLEtBQUssR0FBQyxRQURWO0FBQUEsTUFDbUJDLGVBQWUsR0FBQyx1Q0FEbkM7QUFBQSxNQUMyRUMsWUFBWSxHQUFDLG9DQUR4Rjs7QUFDNkgsV0FBU0MsV0FBVCxDQUFxQi9JLE1BQXJCLEVBQTRCMTBCLEdBQTVCLEVBQWdDMDlCLFdBQWhDLEVBQTRDMWtCLEdBQTVDLEVBQWdEO0FBQUMsUUFBSW5WLElBQUo7O0FBQVMsUUFBR08sS0FBSyxDQUFDQyxPQUFOLENBQWNyRSxHQUFkLENBQUgsRUFBc0I7QUFBQ3lCLFlBQU0sQ0FBQ2lCLElBQVAsQ0FBWTFDLEdBQVosRUFBZ0IsVUFBU1ksQ0FBVCxFQUFXb2EsQ0FBWCxFQUFhO0FBQUMsWUFBRzBpQixXQUFXLElBQUVMLFFBQVEsQ0FBQzF4QixJQUFULENBQWMrb0IsTUFBZCxDQUFoQixFQUFzQztBQUFDMWIsYUFBRyxDQUFDMGIsTUFBRCxFQUFRMVosQ0FBUixDQUFIO0FBQWUsU0FBdEQsTUFBMEQ7QUFBQ3lpQixxQkFBVyxDQUFDL0ksTUFBTSxHQUFDLEdBQVAsSUFBWSxRQUFPMVosQ0FBUCxNQUFXLFFBQVgsSUFBcUJBLENBQUMsSUFBRSxJQUF4QixHQUE2QnBhLENBQTdCLEdBQStCLEVBQTNDLElBQStDLEdBQWhELEVBQW9Eb2EsQ0FBcEQsRUFBc0QwaUIsV0FBdEQsRUFBa0Uxa0IsR0FBbEUsQ0FBWDtBQUFtRjtBQUFDLE9BQTdLO0FBQWdMLEtBQXZNLE1BQTRNLElBQUcsQ0FBQzBrQixXQUFELElBQWNuOEIsTUFBTSxDQUFDdkIsR0FBRCxDQUFOLEtBQWMsUUFBL0IsRUFBd0M7QUFBQyxXQUFJNkQsSUFBSixJQUFZN0QsR0FBWixFQUFnQjtBQUFDeTlCLG1CQUFXLENBQUMvSSxNQUFNLEdBQUMsR0FBUCxHQUFXN3dCLElBQVgsR0FBZ0IsR0FBakIsRUFBcUI3RCxHQUFHLENBQUM2RCxJQUFELENBQXhCLEVBQStCNjVCLFdBQS9CLEVBQTJDMWtCLEdBQTNDLENBQVg7QUFBNEQ7QUFBQyxLQUF2SCxNQUEySDtBQUFDQSxTQUFHLENBQUMwYixNQUFELEVBQVExMEIsR0FBUixDQUFIO0FBQWlCO0FBQUM7O0FBQzloQnlCLFFBQU0sQ0FBQ2s4QixLQUFQLEdBQWEsVUFBUzcxQixDQUFULEVBQVc0MUIsV0FBWCxFQUF1QjtBQUFDLFFBQUloSixNQUFKO0FBQUEsUUFBV2tKLENBQUMsR0FBQyxFQUFiO0FBQUEsUUFBZ0I1a0IsR0FBRyxHQUFDLFNBQUpBLEdBQUksQ0FBUzNNLEdBQVQsRUFBYXd4QixlQUFiLEVBQTZCO0FBQUMsVUFBSWg0QixLQUFLLEdBQUM5RixVQUFVLENBQUM4OUIsZUFBRCxDQUFWLEdBQTRCQSxlQUFlLEVBQTNDLEdBQThDQSxlQUF4RDtBQUF3RUQsT0FBQyxDQUFDQSxDQUFDLENBQUMzN0IsTUFBSCxDQUFELEdBQVk2N0Isa0JBQWtCLENBQUN6eEIsR0FBRCxDQUFsQixHQUF3QixHQUF4QixHQUMzS3l4QixrQkFBa0IsQ0FBQ2o0QixLQUFLLElBQUUsSUFBUCxHQUFZLEVBQVosR0FBZUEsS0FBaEIsQ0FENkk7QUFDckgsS0FETDs7QUFDTSxRQUFHaUMsQ0FBQyxJQUFFLElBQU4sRUFBVztBQUFDLGFBQU0sRUFBTjtBQUFVOztBQUNqRSxRQUFHMUQsS0FBSyxDQUFDQyxPQUFOLENBQWN5RCxDQUFkLEtBQW1CQSxDQUFDLENBQUMvRixNQUFGLElBQVUsQ0FBQ04sTUFBTSxDQUFDMEMsYUFBUCxDQUFxQjJELENBQXJCLENBQWpDLEVBQTBEO0FBQUNyRyxZQUFNLENBQUNpQixJQUFQLENBQVlvRixDQUFaLEVBQWMsWUFBVTtBQUFDa1IsV0FBRyxDQUFDLEtBQUtuVixJQUFOLEVBQVcsS0FBS2dDLEtBQWhCLENBQUg7QUFBMkIsT0FBcEQ7QUFBdUQsS0FBbEgsTUFBc0g7QUFBQyxXQUFJNnVCLE1BQUosSUFBYzVzQixDQUFkLEVBQWdCO0FBQUMyMUIsbUJBQVcsQ0FBQy9JLE1BQUQsRUFBUTVzQixDQUFDLENBQUM0c0IsTUFBRCxDQUFULEVBQWtCZ0osV0FBbEIsRUFBOEIxa0IsR0FBOUIsQ0FBWDtBQUErQztBQUFDOztBQUN4TCxXQUFPNGtCLENBQUMsQ0FBQzd4QixJQUFGLENBQU8sR0FBUCxDQUFQO0FBQW9CLEdBSHBCOztBQUdxQnRLLFFBQU0sQ0FBQ0csRUFBUCxDQUFVK0IsTUFBVixDQUFpQjtBQUFDbzZCLGFBQVMsRUFBQyxxQkFBVTtBQUFDLGFBQU90OEIsTUFBTSxDQUFDazhCLEtBQVAsQ0FBYSxLQUFLSyxjQUFMLEVBQWIsQ0FBUDtBQUE0QyxLQUFsRTtBQUFtRUEsa0JBQWMsRUFBQywwQkFBVTtBQUFDLGFBQU8sS0FBS3A3QixHQUFMLENBQVMsWUFBVTtBQUFDLFlBQUlvTixRQUFRLEdBQUN2TyxNQUFNLENBQUMyZSxJQUFQLENBQVksSUFBWixFQUFpQixVQUFqQixDQUFiO0FBQTBDLGVBQU9wUSxRQUFRLEdBQUN2TyxNQUFNLENBQUMyRCxTQUFQLENBQWlCNEssUUFBakIsQ0FBRCxHQUE0QixJQUEzQztBQUFpRCxPQUEvRyxFQUFpSHhCLE1BQWpILENBQXdILFlBQVU7QUFBQyxZQUFJcE8sSUFBSSxHQUFDLEtBQUtBLElBQWQ7QUFBbUIsZUFBTyxLQUFLeUQsSUFBTCxJQUFXLENBQUNwQyxNQUFNLENBQUMsSUFBRCxDQUFOLENBQWE4VixFQUFiLENBQWdCLFdBQWhCLENBQVosSUFBMENpbUIsWUFBWSxDQUFDN3hCLElBQWIsQ0FBa0IsS0FBS3BCLFFBQXZCLENBQTFDLElBQTRFLENBQUNnekIsZUFBZSxDQUFDNXhCLElBQWhCLENBQXFCdkwsSUFBckIsQ0FBN0UsS0FBMEcsS0FBS2dULE9BQUwsSUFBYyxDQUFDb1EsY0FBYyxDQUFDN1gsSUFBZixDQUFvQnZMLElBQXBCLENBQXpILENBQVA7QUFBNEosT0FBbFQsRUFBb1R3QyxHQUFwVCxDQUF3VCxVQUFTc0QsRUFBVCxFQUFZckQsSUFBWixFQUFpQjtBQUFDLFlBQUloQyxHQUFHLEdBQUNZLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYVosR0FBYixFQUFSOztBQUEyQixZQUFHQSxHQUFHLElBQUUsSUFBUixFQUFhO0FBQUMsaUJBQU8sSUFBUDtBQUFhOztBQUMxZ0IsWUFBR3VELEtBQUssQ0FBQ0MsT0FBTixDQUFjeEQsR0FBZCxDQUFILEVBQXNCO0FBQUMsaUJBQU9ZLE1BQU0sQ0FBQ21CLEdBQVAsQ0FBVy9CLEdBQVgsRUFBZSxVQUFTQSxHQUFULEVBQWE7QUFBQyxtQkFBTTtBQUFDZ0Qsa0JBQUksRUFBQ2hCLElBQUksQ0FBQ2dCLElBQVg7QUFBZ0JnQyxtQkFBSyxFQUFDaEYsR0FBRyxDQUFDNkQsT0FBSixDQUFZNDRCLEtBQVosRUFBa0IsTUFBbEI7QUFBdEIsYUFBTjtBQUF3RCxXQUFyRixDQUFQO0FBQStGOztBQUN0SCxlQUFNO0FBQUN6NUIsY0FBSSxFQUFDaEIsSUFBSSxDQUFDZ0IsSUFBWDtBQUFnQmdDLGVBQUssRUFBQ2hGLEdBQUcsQ0FBQzZELE9BQUosQ0FBWTQ0QixLQUFaLEVBQWtCLE1BQWxCO0FBQXRCLFNBQU47QUFBd0QsT0FGa0YsRUFFaEZuN0IsR0FGZ0YsRUFBUDtBQUVsRTtBQUYzQixHQUFqQjtBQUUrQyxNQUNwRTg3QixHQUFHLEdBQUMsTUFEZ0U7QUFBQSxNQUN6REMsS0FBSyxHQUFDLE1BRG1EO0FBQUEsTUFDNUNDLFVBQVUsR0FBQyxlQURpQztBQUFBLE1BQ2pCQyxRQUFRLEdBQUMsNEJBRFE7QUFBQSxNQUNxQkMsY0FBYyxHQUFDLDJEQURwQztBQUFBLE1BQ2dHQyxVQUFVLEdBQUMsZ0JBRDNHO0FBQUEsTUFDNEhDLFNBQVMsR0FBQyxPQUR0STtBQUFBLE1BQzhJdkcsVUFBVSxHQUFDLEVBRHpKO0FBQUEsTUFDNEp3RyxVQUFVLEdBQUMsRUFEdks7QUFBQSxNQUMwS0MsUUFBUSxHQUFDLEtBQUtyL0IsTUFBTCxDQUFZLEdBQVosQ0FEbkw7QUFBQSxNQUNvTXMvQixZQUFZLEdBQUNuZ0MsUUFBUSxDQUFDd0MsYUFBVCxDQUF1QixHQUF2QixDQURqTjtBQUM2TzI5QixjQUFZLENBQUN4ckIsSUFBYixHQUFrQkgsUUFBUSxDQUFDRyxJQUEzQjs7QUFBZ0MsV0FBU3lyQiwyQkFBVCxDQUFxQ0MsU0FBckMsRUFBK0M7QUFBQyxXQUFPLFVBQVNDLGtCQUFULEVBQTRCampCLElBQTVCLEVBQWlDO0FBQUMsVUFBRyxPQUFPaWpCLGtCQUFQLEtBQTRCLFFBQS9CLEVBQXdDO0FBQUNqakIsWUFBSSxHQUFDaWpCLGtCQUFMO0FBQXdCQSwwQkFBa0IsR0FBQyxHQUFuQjtBQUF3Qjs7QUFDbmdCLFVBQUlDLFFBQUo7QUFBQSxVQUFhbCtCLENBQUMsR0FBQyxDQUFmO0FBQUEsVUFBaUJtK0IsU0FBUyxHQUFDRixrQkFBa0IsQ0FBQzE0QixXQUFuQixHQUFpQzZFLEtBQWpDLENBQXVDMk8sYUFBdkMsS0FBdUQsRUFBbEY7O0FBQXFGLFVBQUc1WixVQUFVLENBQUM2YixJQUFELENBQWIsRUFBb0I7QUFBQyxlQUFPa2pCLFFBQVEsR0FBQ0MsU0FBUyxDQUFDbitCLENBQUMsRUFBRixDQUF6QixFQUFnQztBQUFDLGNBQUdrK0IsUUFBUSxDQUFDLENBQUQsQ0FBUixLQUFjLEdBQWpCLEVBQXFCO0FBQUNBLG9CQUFRLEdBQUNBLFFBQVEsQ0FBQzkvQixLQUFULENBQWUsQ0FBZixLQUFtQixHQUE1QjtBQUFnQyxhQUFDNC9CLFNBQVMsQ0FBQ0UsUUFBRCxDQUFULEdBQW9CRixTQUFTLENBQUNFLFFBQUQsQ0FBVCxJQUFxQixFQUExQyxFQUE4Q2h2QixPQUE5QyxDQUFzRDhMLElBQXREO0FBQTZELFdBQW5ILE1BQXVIO0FBQUMsYUFBQ2dqQixTQUFTLENBQUNFLFFBQUQsQ0FBVCxHQUFvQkYsU0FBUyxDQUFDRSxRQUFELENBQVQsSUFBcUIsRUFBMUMsRUFBOEN4L0IsSUFBOUMsQ0FBbURzYyxJQUFuRDtBQUEwRDtBQUFDO0FBQUM7QUFBQyxLQURpRTtBQUMvRDs7QUFDbFUsV0FBU29qQiw2QkFBVCxDQUF1Q0osU0FBdkMsRUFBaURoN0IsT0FBakQsRUFBeUR5MEIsZUFBekQsRUFBeUU0RyxLQUF6RSxFQUErRTtBQUFDLFFBQUlDLFNBQVMsR0FBQyxFQUFkO0FBQUEsUUFBaUJDLGdCQUFnQixHQUFFUCxTQUFTLEtBQUdKLFVBQS9DOztBQUEyRCxhQUFTWSxPQUFULENBQWlCTixRQUFqQixFQUEwQjtBQUFDLFVBQUl6ckIsUUFBSjtBQUFhNnJCLGVBQVMsQ0FBQ0osUUFBRCxDQUFULEdBQW9CLElBQXBCO0FBQXlCcjlCLFlBQU0sQ0FBQ2lCLElBQVAsQ0FBWWs4QixTQUFTLENBQUNFLFFBQUQsQ0FBVCxJQUFxQixFQUFqQyxFQUFvQyxVQUFTaGxCLENBQVQsRUFBV3VsQixrQkFBWCxFQUE4QjtBQUFDLFlBQUlDLG1CQUFtQixHQUFDRCxrQkFBa0IsQ0FBQ3o3QixPQUFELEVBQVN5MEIsZUFBVCxFQUF5QjRHLEtBQXpCLENBQTFDOztBQUEwRSxZQUFHLE9BQU9LLG1CQUFQLEtBQTZCLFFBQTdCLElBQXVDLENBQUNILGdCQUF4QyxJQUEwRCxDQUFDRCxTQUFTLENBQUNJLG1CQUFELENBQXZFLEVBQTZGO0FBQUMxN0IsaUJBQU8sQ0FBQ203QixTQUFSLENBQWtCanZCLE9BQWxCLENBQTBCd3ZCLG1CQUExQjtBQUErQ0YsaUJBQU8sQ0FBQ0UsbUJBQUQsQ0FBUDtBQUE2QixpQkFBTyxLQUFQO0FBQWMsU0FBeEwsTUFBNkwsSUFBR0gsZ0JBQUgsRUFBb0I7QUFBQyxpQkFBTSxFQUFFOXJCLFFBQVEsR0FBQ2lzQixtQkFBWCxDQUFOO0FBQXVDO0FBQUMsT0FBdlk7QUFBeVksYUFBT2pzQixRQUFQO0FBQWlCOztBQUN0bUIsV0FBTytyQixPQUFPLENBQUN4N0IsT0FBTyxDQUFDbTdCLFNBQVIsQ0FBa0IsQ0FBbEIsQ0FBRCxDQUFQLElBQStCLENBQUNHLFNBQVMsQ0FBQyxHQUFELENBQVYsSUFBaUJFLE9BQU8sQ0FBQyxHQUFELENBQTlEO0FBQXFFOztBQUNyRSxXQUFTRyxVQUFULENBQW9CdDdCLE1BQXBCLEVBQTJCNUQsR0FBM0IsRUFBK0I7QUFBQyxRQUFJZ00sR0FBSjtBQUFBLFFBQVFuSSxJQUFSO0FBQUEsUUFBYXM3QixXQUFXLEdBQUMvOUIsTUFBTSxDQUFDZytCLFlBQVAsQ0FBb0JELFdBQXBCLElBQWlDLEVBQTFEOztBQUE2RCxTQUFJbnpCLEdBQUosSUFBV2hNLEdBQVgsRUFBZTtBQUFDLFVBQUdBLEdBQUcsQ0FBQ2dNLEdBQUQsQ0FBSCxLQUFXL0gsU0FBZCxFQUF3QjtBQUFDLFNBQUNrN0IsV0FBVyxDQUFDbnpCLEdBQUQsQ0FBWCxHQUFpQnBJLE1BQWpCLEdBQXlCQyxJQUFJLEtBQUdBLElBQUksR0FBQyxFQUFSLENBQTlCLEVBQTRDbUksR0FBNUMsSUFBaURoTSxHQUFHLENBQUNnTSxHQUFELENBQXBEO0FBQTJEO0FBQUM7O0FBQ2xNLFFBQUduSSxJQUFILEVBQVE7QUFBQ3pDLFlBQU0sQ0FBQ2tDLE1BQVAsQ0FBYyxJQUFkLEVBQW1CTSxNQUFuQixFQUEwQkMsSUFBMUI7QUFBaUM7O0FBQzFDLFdBQU9ELE1BQVA7QUFBZTs7QUFDZixXQUFTeTdCLG1CQUFULENBQTZCOUIsQ0FBN0IsRUFBK0JxQixLQUEvQixFQUFxQ1UsU0FBckMsRUFBK0M7QUFBQyxRQUFJQyxFQUFKO0FBQUEsUUFBT3gvQixJQUFQO0FBQUEsUUFBWXkvQixhQUFaO0FBQUEsUUFBMEJDLGFBQTFCO0FBQUEsUUFBd0N0bkIsUUFBUSxHQUFDb2xCLENBQUMsQ0FBQ3BsQixRQUFuRDtBQUFBLFFBQTREdW1CLFNBQVMsR0FBQ25CLENBQUMsQ0FBQ21CLFNBQXhFOztBQUFrRixXQUFNQSxTQUFTLENBQUMsQ0FBRCxDQUFULEtBQWUsR0FBckIsRUFBeUI7QUFBQ0EsZUFBUyxDQUFDeHlCLEtBQVY7O0FBQWtCLFVBQUdxekIsRUFBRSxLQUFHdDdCLFNBQVIsRUFBa0I7QUFBQ3M3QixVQUFFLEdBQUNoQyxDQUFDLENBQUNtQyxRQUFGLElBQVlkLEtBQUssQ0FBQ2UsaUJBQU4sQ0FBd0IsY0FBeEIsQ0FBZjtBQUF3RDtBQUFDOztBQUMxUCxRQUFHSixFQUFILEVBQU07QUFBQyxXQUFJeC9CLElBQUosSUFBWW9ZLFFBQVosRUFBcUI7QUFBQyxZQUFHQSxRQUFRLENBQUNwWSxJQUFELENBQVIsSUFBZ0JvWSxRQUFRLENBQUNwWSxJQUFELENBQVIsQ0FBZXVMLElBQWYsQ0FBb0JpMEIsRUFBcEIsQ0FBbkIsRUFBMkM7QUFBQ2IsbUJBQVMsQ0FBQ2p2QixPQUFWLENBQWtCMVAsSUFBbEI7QUFBd0I7QUFBTztBQUFDO0FBQUM7O0FBQzFHLFFBQUcyK0IsU0FBUyxDQUFDLENBQUQsQ0FBVCxJQUFlWSxTQUFsQixFQUE0QjtBQUFDRSxtQkFBYSxHQUFDZCxTQUFTLENBQUMsQ0FBRCxDQUF2QjtBQUE0QixLQUF6RCxNQUE2RDtBQUFDLFdBQUkzK0IsSUFBSixJQUFZdS9CLFNBQVosRUFBc0I7QUFBQyxZQUFHLENBQUNaLFNBQVMsQ0FBQyxDQUFELENBQVYsSUFBZW5CLENBQUMsQ0FBQ3FDLFVBQUYsQ0FBYTcvQixJQUFJLEdBQUMsR0FBTCxHQUFTMitCLFNBQVMsQ0FBQyxDQUFELENBQS9CLENBQWxCLEVBQXNEO0FBQUNjLHVCQUFhLEdBQUN6L0IsSUFBZDtBQUFtQjtBQUFPOztBQUN0SyxZQUFHLENBQUMwL0IsYUFBSixFQUFrQjtBQUFDQSx1QkFBYSxHQUFDMS9CLElBQWQ7QUFBb0I7QUFBQzs7QUFDeEN5L0IsbUJBQWEsR0FBQ0EsYUFBYSxJQUFFQyxhQUE3QjtBQUE0Qzs7QUFDNUMsUUFBR0QsYUFBSCxFQUFpQjtBQUFDLFVBQUdBLGFBQWEsS0FBR2QsU0FBUyxDQUFDLENBQUQsQ0FBNUIsRUFBZ0M7QUFBQ0EsaUJBQVMsQ0FBQ2p2QixPQUFWLENBQWtCK3ZCLGFBQWxCO0FBQWtDOztBQUNyRixhQUFPRixTQUFTLENBQUNFLGFBQUQsQ0FBaEI7QUFBaUM7QUFBQzs7QUFDbEMsV0FBU0ssV0FBVCxDQUFxQnRDLENBQXJCLEVBQXVCdUMsUUFBdkIsRUFBZ0NsQixLQUFoQyxFQUFzQ21CLFNBQXRDLEVBQWdEO0FBQUMsUUFBSUMsS0FBSjtBQUFBLFFBQVVDLE9BQVY7QUFBQSxRQUFrQkMsSUFBbEI7QUFBQSxRQUF1QjF4QixHQUF2QjtBQUFBLFFBQTJCNEosSUFBM0I7QUFBQSxRQUFnQ3duQixVQUFVLEdBQUMsRUFBM0M7QUFBQSxRQUE4Q2xCLFNBQVMsR0FBQ25CLENBQUMsQ0FBQ21CLFNBQUYsQ0FBWS8vQixLQUFaLEVBQXhEOztBQUE0RSxRQUFHKy9CLFNBQVMsQ0FBQyxDQUFELENBQVosRUFBZ0I7QUFBQyxXQUFJd0IsSUFBSixJQUFZM0MsQ0FBQyxDQUFDcUMsVUFBZCxFQUF5QjtBQUFDQSxrQkFBVSxDQUFDTSxJQUFJLENBQUNwNkIsV0FBTCxFQUFELENBQVYsR0FBK0J5M0IsQ0FBQyxDQUFDcUMsVUFBRixDQUFhTSxJQUFiLENBQS9CO0FBQW1EO0FBQUM7O0FBQzVORCxXQUFPLEdBQUN2QixTQUFTLENBQUN4eUIsS0FBVixFQUFSOztBQUEwQixXQUFNK3pCLE9BQU4sRUFBYztBQUFDLFVBQUcxQyxDQUFDLENBQUM0QyxjQUFGLENBQWlCRixPQUFqQixDQUFILEVBQTZCO0FBQUNyQixhQUFLLENBQUNyQixDQUFDLENBQUM0QyxjQUFGLENBQWlCRixPQUFqQixDQUFELENBQUwsR0FBaUNILFFBQWpDO0FBQTJDOztBQUNsSCxVQUFHLENBQUMxbkIsSUFBRCxJQUFPMm5CLFNBQVAsSUFBa0J4QyxDQUFDLENBQUM2QyxVQUF2QixFQUFrQztBQUFDTixnQkFBUSxHQUFDdkMsQ0FBQyxDQUFDNkMsVUFBRixDQUFhTixRQUFiLEVBQXNCdkMsQ0FBQyxDQUFDa0IsUUFBeEIsQ0FBVDtBQUE0Qzs7QUFDL0VybUIsVUFBSSxHQUFDNm5CLE9BQUw7QUFBYUEsYUFBTyxHQUFDdkIsU0FBUyxDQUFDeHlCLEtBQVYsRUFBUjs7QUFBMEIsVUFBRyt6QixPQUFILEVBQVc7QUFBQyxZQUFHQSxPQUFPLEtBQUcsR0FBYixFQUFpQjtBQUFDQSxpQkFBTyxHQUFDN25CLElBQVI7QUFBYyxTQUFoQyxNQUFxQyxJQUFHQSxJQUFJLEtBQUcsR0FBUCxJQUFZQSxJQUFJLEtBQUc2bkIsT0FBdEIsRUFBOEI7QUFBQ0MsY0FBSSxHQUFDTixVQUFVLENBQUN4bkIsSUFBSSxHQUFDLEdBQUwsR0FBUzZuQixPQUFWLENBQVYsSUFBOEJMLFVBQVUsQ0FBQyxPQUFLSyxPQUFOLENBQTdDOztBQUE0RCxjQUFHLENBQUNDLElBQUosRUFBUztBQUFDLGlCQUFJRixLQUFKLElBQWFKLFVBQWIsRUFBd0I7QUFBQ3B4QixpQkFBRyxHQUFDd3hCLEtBQUssQ0FBQ3A2QixLQUFOLENBQVksR0FBWixDQUFKOztBQUFxQixrQkFBRzRJLEdBQUcsQ0FBQyxDQUFELENBQUgsS0FBU3l4QixPQUFaLEVBQW9CO0FBQUNDLG9CQUFJLEdBQUNOLFVBQVUsQ0FBQ3huQixJQUFJLEdBQUMsR0FBTCxHQUFTNUosR0FBRyxDQUFDLENBQUQsQ0FBYixDQUFWLElBQTZCb3hCLFVBQVUsQ0FBQyxPQUFLcHhCLEdBQUcsQ0FBQyxDQUFELENBQVQsQ0FBNUM7O0FBQTBELG9CQUFHMHhCLElBQUgsRUFBUTtBQUFDLHNCQUFHQSxJQUFJLEtBQUcsSUFBVixFQUFlO0FBQUNBLHdCQUFJLEdBQUNOLFVBQVUsQ0FBQ0ksS0FBRCxDQUFmO0FBQXdCLG1CQUF4QyxNQUE2QyxJQUFHSixVQUFVLENBQUNJLEtBQUQsQ0FBVixLQUFvQixJQUF2QixFQUE0QjtBQUFDQywyQkFBTyxHQUFDenhCLEdBQUcsQ0FBQyxDQUFELENBQVg7QUFBZWt3Qiw2QkFBUyxDQUFDanZCLE9BQVYsQ0FBa0JqQixHQUFHLENBQUMsQ0FBRCxDQUFyQjtBQUEyQjs7QUFDdmI7QUFBTztBQUFDO0FBQUM7QUFBQzs7QUFDVixjQUFHMHhCLElBQUksS0FBRyxJQUFWLEVBQWU7QUFBQyxnQkFBR0EsSUFBSSxJQUFFM0MsQ0FBQyxVQUFWLEVBQWtCO0FBQUN1QyxzQkFBUSxHQUFDSSxJQUFJLENBQUNKLFFBQUQsQ0FBYjtBQUF5QixhQUE1QyxNQUFnRDtBQUFDLGtCQUFHO0FBQUNBLHdCQUFRLEdBQUNJLElBQUksQ0FBQ0osUUFBRCxDQUFiO0FBQXlCLGVBQTdCLENBQTZCLE9BQU14MUIsQ0FBTixFQUFRO0FBQUMsdUJBQU07QUFBQ21SLHVCQUFLLEVBQUMsYUFBUDtBQUFxQmxYLHVCQUFLLEVBQUMyN0IsSUFBSSxHQUFDNTFCLENBQUQsR0FBRyx3QkFBc0I4TixJQUF0QixHQUEyQixNQUEzQixHQUFrQzZuQjtBQUFwRSxpQkFBTjtBQUFvRjtBQUFDO0FBQUM7QUFBQztBQUFDO0FBQUM7O0FBQ2hNLFdBQU07QUFBQ3hrQixXQUFLLEVBQUMsU0FBUDtBQUFpQnFFLFVBQUksRUFBQ2dnQjtBQUF0QixLQUFOO0FBQXVDOztBQUN2QzErQixRQUFNLENBQUNrQyxNQUFQLENBQWM7QUFBQys4QixVQUFNLEVBQUMsQ0FBUjtBQUFVQyxnQkFBWSxFQUFDLEVBQXZCO0FBQTBCQyxRQUFJLEVBQUMsRUFBL0I7QUFBa0NuQixnQkFBWSxFQUFDO0FBQUNvQixTQUFHLEVBQUM5dEIsUUFBUSxDQUFDRyxJQUFkO0FBQW1COVMsVUFBSSxFQUFDLEtBQXhCO0FBQThCMGdDLGFBQU8sRUFBQ3pDLGNBQWMsQ0FBQzF5QixJQUFmLENBQW9Cb0gsUUFBUSxDQUFDZ3VCLFFBQTdCLENBQXRDO0FBQTZFNWlDLFlBQU0sRUFBQyxJQUFwRjtBQUF5RjZpQyxpQkFBVyxFQUFDLElBQXJHO0FBQTBHQyxXQUFLLEVBQUMsSUFBaEg7QUFBcUhDLGlCQUFXLEVBQUMsa0RBQWpJO0FBQW9MQyxhQUFPLEVBQUM7QUFBQyxhQUFJMUMsUUFBTDtBQUFjejlCLFlBQUksRUFBQyxZQUFuQjtBQUFnQ3dzQixZQUFJLEVBQUMsV0FBckM7QUFBaUQ1YixXQUFHLEVBQUMsMkJBQXJEO0FBQWlGd3ZCLFlBQUksRUFBQztBQUF0RixPQUE1TDtBQUF1VDVvQixjQUFRLEVBQUM7QUFBQzVHLFdBQUcsRUFBQyxTQUFMO0FBQWU0YixZQUFJLEVBQUMsUUFBcEI7QUFBNkI0VCxZQUFJLEVBQUM7QUFBbEMsT0FBaFU7QUFBOFdaLG9CQUFjLEVBQUM7QUFBQzV1QixXQUFHLEVBQUMsYUFBTDtBQUFtQjVRLFlBQUksRUFBQyxjQUF4QjtBQUF1Q29nQyxZQUFJLEVBQUM7QUFBNUMsT0FBN1g7QUFBeWJuQixnQkFBVSxFQUFDO0FBQUMsa0JBQVNyMkIsTUFBVjtBQUFpQixxQkFBWSxJQUE3QjtBQUFrQyxxQkFBWStXLElBQUksQ0FBQ0MsS0FBbkQ7QUFBeUQsb0JBQVduZixNQUFNLENBQUN5N0I7QUFBM0UsT0FBcGM7QUFBeWhCc0MsaUJBQVcsRUFBQztBQUFDcUIsV0FBRyxFQUFDLElBQUw7QUFBVWwvQixlQUFPLEVBQUM7QUFBbEI7QUFBcmlCLEtBQS9DO0FBQTZtQjAvQixhQUFTLEVBQUMsbUJBQVNwOUIsTUFBVCxFQUFnQnE5QixRQUFoQixFQUF5QjtBQUFDLGFBQU9BLFFBQVEsR0FBQy9CLFVBQVUsQ0FBQ0EsVUFBVSxDQUFDdDdCLE1BQUQsRUFBUXhDLE1BQU0sQ0FBQ2crQixZQUFmLENBQVgsRUFBd0M2QixRQUF4QyxDQUFYLEdBQTZEL0IsVUFBVSxDQUFDOTlCLE1BQU0sQ0FBQ2crQixZQUFSLEVBQXFCeDdCLE1BQXJCLENBQXRGO0FBQW9ILEtBQXJ3QjtBQUFzd0JzOUIsaUJBQWEsRUFBQzVDLDJCQUEyQixDQUFDM0csVUFBRCxDQUEveUI7QUFBNHpCd0osaUJBQWEsRUFBQzdDLDJCQUEyQixDQUFDSCxVQUFELENBQXIyQjtBQUFrM0JpRCxRQUFJLEVBQUMsY0FBU1osR0FBVCxFQUFhajlCLE9BQWIsRUFBcUI7QUFBQyxVQUFHLFFBQU9pOUIsR0FBUCxNQUFhLFFBQWhCLEVBQXlCO0FBQUNqOUIsZUFBTyxHQUFDaTlCLEdBQVI7QUFBWUEsV0FBRyxHQUFDdjhCLFNBQUo7QUFBZTs7QUFDaDlCVixhQUFPLEdBQUNBLE9BQU8sSUFBRSxFQUFqQjs7QUFBb0IsVUFBSTg5QixTQUFKO0FBQUEsVUFBY0MsUUFBZDtBQUFBLFVBQXVCQyxxQkFBdkI7QUFBQSxVQUE2Q0MsZUFBN0M7QUFBQSxVQUE2REMsWUFBN0Q7QUFBQSxVQUEwRUMsU0FBMUU7QUFBQSxVQUFvRnJqQixTQUFwRjtBQUFBLFVBQThGc2pCLFdBQTlGO0FBQUEsVUFBMEdwaEMsQ0FBMUc7QUFBQSxVQUE0R3FoQyxRQUE1RztBQUFBLFVBQXFIckUsQ0FBQyxHQUFDbjhCLE1BQU0sQ0FBQzQvQixTQUFQLENBQWlCLEVBQWpCLEVBQW9CejlCLE9BQXBCLENBQXZIO0FBQUEsVUFBb0pzK0IsZUFBZSxHQUFDdEUsQ0FBQyxDQUFDajhCLE9BQUYsSUFBV2k4QixDQUEvSztBQUFBLFVBQWlMdUUsa0JBQWtCLEdBQUN2RSxDQUFDLENBQUNqOEIsT0FBRixLQUFZdWdDLGVBQWUsQ0FBQ2ppQyxRQUFoQixJQUEwQmlpQyxlQUFlLENBQUNuZ0MsTUFBdEQsSUFBOEROLE1BQU0sQ0FBQ3lnQyxlQUFELENBQXBFLEdBQXNGemdDLE1BQU0sQ0FBQzZrQixLQUFqUztBQUFBLFVBQXVTdEssUUFBUSxHQUFDdmEsTUFBTSxDQUFDa2EsUUFBUCxFQUFoVDtBQUFBLFVBQWtVeW1CLGdCQUFnQixHQUFDM2dDLE1BQU0sQ0FBQ3VZLFNBQVAsQ0FBaUIsYUFBakIsQ0FBblY7QUFBQSxVQUFtWHFvQixXQUFVLEdBQUN6RSxDQUFDLENBQUN5RSxVQUFGLElBQWMsRUFBNVk7QUFBQSxVQUErWUMsY0FBYyxHQUFDLEVBQTlaO0FBQUEsVUFBaWFDLG1CQUFtQixHQUFDLEVBQXJiO0FBQUEsVUFBd2JDLFFBQVEsR0FBQyxVQUFqYztBQUFBLFVBQTRjdkQsS0FBSyxHQUFDO0FBQUNyZ0Isa0JBQVUsRUFBQyxDQUFaO0FBQWNvaEIseUJBQWlCLEVBQUMsMkJBQVMzekIsR0FBVCxFQUFhO0FBQUMsY0FBSXJCLEtBQUo7O0FBQVUsY0FBRzBULFNBQUgsRUFBYTtBQUFDLGdCQUFHLENBQUNtakIsZUFBSixFQUFvQjtBQUFDQSw2QkFBZSxHQUFDLEVBQWhCOztBQUFtQixxQkFBTzcyQixLQUFLLEdBQUNvekIsUUFBUSxDQUFDL3lCLElBQVQsQ0FBY3UyQixxQkFBZCxDQUFiLEVBQW1EO0FBQUNDLCtCQUFlLENBQUM3MkIsS0FBSyxDQUFDLENBQUQsQ0FBTCxDQUFTN0UsV0FBVCxLQUF1QixHQUF4QixDQUFmLEdBQTRDLENBQUMwN0IsZUFBZSxDQUFDNzJCLEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBUzdFLFdBQVQsS0FBdUIsR0FBeEIsQ0FBZixJQUE2QyxFQUE5QyxFQUFrRC9HLE1BQWxELENBQXlENEwsS0FBSyxDQUFDLENBQUQsQ0FBOUQsQ0FBNUM7QUFBZ0g7QUFBQzs7QUFDenZCQSxpQkFBSyxHQUFDNjJCLGVBQWUsQ0FBQ3gxQixHQUFHLENBQUNsRyxXQUFKLEtBQWtCLEdBQW5CLENBQXJCO0FBQThDOztBQUM5QyxpQkFBTzZFLEtBQUssSUFBRSxJQUFQLEdBQVksSUFBWixHQUFpQkEsS0FBSyxDQUFDZSxJQUFOLENBQVcsSUFBWCxDQUF4QjtBQUEwQyxTQUY0YjtBQUUzYjAyQiw2QkFBcUIsRUFBQyxpQ0FBVTtBQUFDLGlCQUFPL2pCLFNBQVMsR0FBQ2tqQixxQkFBRCxHQUF1QixJQUF2QztBQUE2QyxTQUY2VztBQUU1V2Msd0JBQWdCLEVBQUMsMEJBQVM3K0IsSUFBVCxFQUFjZ0MsS0FBZCxFQUFvQjtBQUFDLGNBQUc2WSxTQUFTLElBQUUsSUFBZCxFQUFtQjtBQUFDN2EsZ0JBQUksR0FBQzArQixtQkFBbUIsQ0FBQzErQixJQUFJLENBQUNzQyxXQUFMLEVBQUQsQ0FBbkIsR0FBd0NvOEIsbUJBQW1CLENBQUMxK0IsSUFBSSxDQUFDc0MsV0FBTCxFQUFELENBQW5CLElBQXlDdEMsSUFBdEY7QUFBMkZ5K0IsMEJBQWMsQ0FBQ3orQixJQUFELENBQWQsR0FBcUJnQyxLQUFyQjtBQUE0Qjs7QUFDM1MsaUJBQU8sSUFBUDtBQUFhLFNBSHlkO0FBR3hkODhCLHdCQUFnQixFQUFDLDBCQUFTdmlDLElBQVQsRUFBYztBQUFDLGNBQUdzZSxTQUFTLElBQUUsSUFBZCxFQUFtQjtBQUFDa2YsYUFBQyxDQUFDbUMsUUFBRixHQUFXMy9CLElBQVg7QUFBaUI7O0FBQ25GLGlCQUFPLElBQVA7QUFBYSxTQUp5ZDtBQUl4ZGlpQyxrQkFBVSxFQUFDLG9CQUFTei9CLEdBQVQsRUFBYTtBQUFDLGNBQUluQyxJQUFKOztBQUFTLGNBQUdtQyxHQUFILEVBQU87QUFBQyxnQkFBRzhiLFNBQUgsRUFBYTtBQUFDdWdCLG1CQUFLLENBQUNsakIsTUFBTixDQUFhblosR0FBRyxDQUFDcThCLEtBQUssQ0FBQzJELE1BQVAsQ0FBaEI7QUFBaUMsYUFBL0MsTUFBbUQ7QUFBQyxtQkFBSW5pQyxJQUFKLElBQVltQyxHQUFaLEVBQWdCO0FBQUN5L0IsMkJBQVUsQ0FBQzVoQyxJQUFELENBQVYsR0FBaUIsQ0FBQzRoQyxXQUFVLENBQUM1aEMsSUFBRCxDQUFYLEVBQWtCbUMsR0FBRyxDQUFDbkMsSUFBRCxDQUFyQixDQUFqQjtBQUErQztBQUFDO0FBQUM7O0FBQzlLLGlCQUFPLElBQVA7QUFBYSxTQUx5ZDtBQUt4ZG9pQyxhQUFLLEVBQUMsZUFBU0MsVUFBVCxFQUFvQjtBQUFDLGNBQUlDLFNBQVMsR0FBQ0QsVUFBVSxJQUFFTixRQUExQjs7QUFBbUMsY0FBR2QsU0FBSCxFQUFhO0FBQUNBLHFCQUFTLENBQUNtQixLQUFWLENBQWdCRSxTQUFoQjtBQUE0Qjs7QUFDdEh4N0IsY0FBSSxDQUFDLENBQUQsRUFBR3c3QixTQUFILENBQUo7QUFBa0IsaUJBQU8sSUFBUDtBQUFhO0FBTnVjLE9BQWxkOztBQU1hL21CLGNBQVEsQ0FBQ1IsT0FBVCxDQUFpQnlqQixLQUFqQjtBQUF3QnJCLE9BQUMsQ0FBQ2lELEdBQUYsR0FBTSxDQUFDLENBQUNBLEdBQUcsSUFBRWpELENBQUMsQ0FBQ2lELEdBQVAsSUFBWTl0QixRQUFRLENBQUNHLElBQXRCLElBQTRCLEVBQTdCLEVBQWlDeE8sT0FBakMsQ0FBeUM2NUIsU0FBekMsRUFBbUR4ckIsUUFBUSxDQUFDZ3VCLFFBQVQsR0FBa0IsSUFBckUsQ0FBTjtBQUFpRm5ELE9BQUMsQ0FBQ3g5QixJQUFGLEdBQU93RCxPQUFPLENBQUMyWCxNQUFSLElBQWdCM1gsT0FBTyxDQUFDeEQsSUFBeEIsSUFBOEJ3OUIsQ0FBQyxDQUFDcmlCLE1BQWhDLElBQXdDcWlCLENBQUMsQ0FBQ3g5QixJQUFqRDtBQUFzRHc5QixPQUFDLENBQUNtQixTQUFGLEdBQVksQ0FBQ25CLENBQUMsQ0FBQ2tCLFFBQUYsSUFBWSxHQUFiLEVBQWtCMzRCLFdBQWxCLEdBQWdDNkUsS0FBaEMsQ0FBc0MyTyxhQUF0QyxLQUFzRCxDQUFDLEVBQUQsQ0FBbEU7O0FBQXVFLFVBQUdpa0IsQ0FBQyxDQUFDb0YsV0FBRixJQUFlLElBQWxCLEVBQXVCO0FBQUNqQixpQkFBUyxHQUFDeGpDLFFBQVEsQ0FBQ3dDLGFBQVQsQ0FBdUIsR0FBdkIsQ0FBVjs7QUFBc0MsWUFBRztBQUFDZ2hDLG1CQUFTLENBQUM3dUIsSUFBVixHQUFlMHFCLENBQUMsQ0FBQ2lELEdBQWpCO0FBQXFCa0IsbUJBQVMsQ0FBQzd1QixJQUFWLEdBQWU2dUIsU0FBUyxDQUFDN3VCLElBQXpCO0FBQThCMHFCLFdBQUMsQ0FBQ29GLFdBQUYsR0FBY3RFLFlBQVksQ0FBQ3FDLFFBQWIsR0FBc0IsSUFBdEIsR0FBMkJyQyxZQUFZLENBQUN1RSxJQUF4QyxLQUErQ2xCLFNBQVMsQ0FBQ2hCLFFBQVYsR0FBbUIsSUFBbkIsR0FBd0JnQixTQUFTLENBQUNrQixJQUEvRjtBQUFxRyxTQUE1SixDQUE0SixPQUFNdDRCLENBQU4sRUFBUTtBQUFDaXpCLFdBQUMsQ0FBQ29GLFdBQUYsR0FBYyxJQUFkO0FBQW9CO0FBQUM7O0FBQy9mLFVBQUdwRixDQUFDLENBQUN6ZCxJQUFGLElBQVF5ZCxDQUFDLENBQUNvRCxXQUFWLElBQXVCLE9BQU9wRCxDQUFDLENBQUN6ZCxJQUFULEtBQWdCLFFBQTFDLEVBQW1EO0FBQUN5ZCxTQUFDLENBQUN6ZCxJQUFGLEdBQU8xZSxNQUFNLENBQUNrOEIsS0FBUCxDQUFhQyxDQUFDLENBQUN6ZCxJQUFmLEVBQW9CeWQsQ0FBQyxDQUFDRixXQUF0QixDQUFQO0FBQTJDOztBQUMvRnNCLG1DQUE2QixDQUFDaEgsVUFBRCxFQUFZNEYsQ0FBWixFQUFjaDZCLE9BQWQsRUFBc0JxN0IsS0FBdEIsQ0FBN0I7O0FBQTBELFVBQUd2Z0IsU0FBSCxFQUFhO0FBQUMsZUFBT3VnQixLQUFQO0FBQWM7O0FBQ3RGK0MsaUJBQVcsR0FBQ3ZnQyxNQUFNLENBQUM2a0IsS0FBUCxJQUFjc1gsQ0FBQyxDQUFDei9CLE1BQTVCOztBQUFtQyxVQUFHNmpDLFdBQVcsSUFBRXZnQyxNQUFNLENBQUNpL0IsTUFBUCxPQUFrQixDQUFsQyxFQUFvQztBQUFDai9CLGNBQU0sQ0FBQzZrQixLQUFQLENBQWFnRCxPQUFiLENBQXFCLFdBQXJCO0FBQW1DOztBQUMzR3NVLE9BQUMsQ0FBQ3g5QixJQUFGLEdBQU93OUIsQ0FBQyxDQUFDeDlCLElBQUYsQ0FBT3FmLFdBQVAsRUFBUDtBQUE0Qm1lLE9BQUMsQ0FBQ3NGLFVBQUYsR0FBYSxDQUFDNUUsVUFBVSxDQUFDM3lCLElBQVgsQ0FBZ0JpeUIsQ0FBQyxDQUFDeDlCLElBQWxCLENBQWQ7QUFBc0N1aEMsY0FBUSxHQUFDL0QsQ0FBQyxDQUFDaUQsR0FBRixDQUFNbjhCLE9BQU4sQ0FBY3c1QixLQUFkLEVBQW9CLEVBQXBCLENBQVQ7O0FBQWlDLFVBQUcsQ0FBQ04sQ0FBQyxDQUFDc0YsVUFBTixFQUFpQjtBQUFDakIsZ0JBQVEsR0FBQ3JFLENBQUMsQ0FBQ2lELEdBQUYsQ0FBTTdoQyxLQUFOLENBQVkyaUMsUUFBUSxDQUFDMS9CLE1BQXJCLENBQVQ7O0FBQXNDLFlBQUcyN0IsQ0FBQyxDQUFDemQsSUFBRixLQUFTeWQsQ0FBQyxDQUFDb0QsV0FBRixJQUFlLE9BQU9wRCxDQUFDLENBQUN6ZCxJQUFULEtBQWdCLFFBQXhDLENBQUgsRUFBcUQ7QUFBQ3doQixrQkFBUSxJQUFFLENBQUMxRSxNQUFNLENBQUN0eEIsSUFBUCxDQUFZZzJCLFFBQVosSUFBc0IsR0FBdEIsR0FBMEIsR0FBM0IsSUFBZ0MvRCxDQUFDLENBQUN6ZCxJQUE1QztBQUFpRCxpQkFBT3lkLENBQUMsQ0FBQ3pkLElBQVQ7QUFBZTs7QUFDalIsWUFBR3lkLENBQUMsQ0FBQ3h4QixLQUFGLEtBQVUsS0FBYixFQUFtQjtBQUFDdTFCLGtCQUFRLEdBQUNBLFFBQVEsQ0FBQ2o5QixPQUFULENBQWlCeTVCLFVBQWpCLEVBQTRCLElBQTVCLENBQVQ7QUFBMkM4RCxrQkFBUSxHQUFDLENBQUNoRixNQUFNLENBQUN0eEIsSUFBUCxDQUFZZzJCLFFBQVosSUFBc0IsR0FBdEIsR0FBMEIsR0FBM0IsSUFBZ0MsSUFBaEMsR0FBc0NyaEMsS0FBSyxDQUFDd0YsSUFBTixFQUF0QyxHQUN4RW04QixRQUQrRDtBQUNyRDs7QUFDVnJFLFNBQUMsQ0FBQ2lELEdBQUYsR0FBTWMsUUFBUSxHQUFDTSxRQUFmO0FBQXlCLE9BSDBFLE1BR3JFLElBQUdyRSxDQUFDLENBQUN6ZCxJQUFGLElBQVF5ZCxDQUFDLENBQUNvRCxXQUFWLElBQXVCLENBQUNwRCxDQUFDLENBQUNzRCxXQUFGLElBQWUsRUFBaEIsRUFBb0IzaEMsT0FBcEIsQ0FBNEIsbUNBQTVCLE1BQW1FLENBQTdGLEVBQStGO0FBQUNxK0IsU0FBQyxDQUFDemQsSUFBRixHQUFPeWQsQ0FBQyxDQUFDemQsSUFBRixDQUFPemIsT0FBUCxDQUFldTVCLEdBQWYsRUFBbUIsR0FBbkIsQ0FBUDtBQUFnQzs7QUFDOUosVUFBR0wsQ0FBQyxDQUFDdUYsVUFBTCxFQUFnQjtBQUFDLFlBQUcxaEMsTUFBTSxDQUFDay9CLFlBQVAsQ0FBb0JnQixRQUFwQixDQUFILEVBQWlDO0FBQUMxQyxlQUFLLENBQUN5RCxnQkFBTixDQUF1QixtQkFBdkIsRUFBMkNqaEMsTUFBTSxDQUFDay9CLFlBQVAsQ0FBb0JnQixRQUFwQixDQUEzQztBQUEyRTs7QUFDOUgsWUFBR2xnQyxNQUFNLENBQUNtL0IsSUFBUCxDQUFZZSxRQUFaLENBQUgsRUFBeUI7QUFBQzFDLGVBQUssQ0FBQ3lELGdCQUFOLENBQXVCLGVBQXZCLEVBQXVDamhDLE1BQU0sQ0FBQ20vQixJQUFQLENBQVllLFFBQVosQ0FBdkM7QUFBK0Q7QUFBQzs7QUFDMUYsVUFBRy9ELENBQUMsQ0FBQ3pkLElBQUYsSUFBUXlkLENBQUMsQ0FBQ3NGLFVBQVYsSUFBc0J0RixDQUFDLENBQUNzRCxXQUFGLEtBQWdCLEtBQXRDLElBQTZDdDlCLE9BQU8sQ0FBQ3M5QixXQUF4RCxFQUFvRTtBQUFDakMsYUFBSyxDQUFDeUQsZ0JBQU4sQ0FBdUIsY0FBdkIsRUFBc0M5RSxDQUFDLENBQUNzRCxXQUF4QztBQUFzRDs7QUFDM0hqQyxXQUFLLENBQUN5RCxnQkFBTixDQUF1QixRQUF2QixFQUFnQzlFLENBQUMsQ0FBQ21CLFNBQUYsQ0FBWSxDQUFaLEtBQWdCbkIsQ0FBQyxDQUFDdUQsT0FBRixDQUFVdkQsQ0FBQyxDQUFDbUIsU0FBRixDQUFZLENBQVosQ0FBVixDQUFoQixHQUEwQ25CLENBQUMsQ0FBQ3VELE9BQUYsQ0FBVXZELENBQUMsQ0FBQ21CLFNBQUYsQ0FBWSxDQUFaLENBQVYsS0FDekVuQixDQUFDLENBQUNtQixTQUFGLENBQVksQ0FBWixNQUFpQixHQUFqQixHQUFxQixPQUFLTixRQUFMLEdBQWMsVUFBbkMsR0FBOEMsRUFEMkIsQ0FBMUMsR0FDbUJiLENBQUMsQ0FBQ3VELE9BQUYsQ0FBVSxHQUFWLENBRG5EOztBQUNtRSxXQUFJdmdDLENBQUosSUFBU2c5QixDQUFDLENBQUN3RixPQUFYLEVBQW1CO0FBQUNuRSxhQUFLLENBQUN5RCxnQkFBTixDQUF1QjloQyxDQUF2QixFQUF5Qmc5QixDQUFDLENBQUN3RixPQUFGLENBQVV4aUMsQ0FBVixDQUF6QjtBQUF3Qzs7QUFDL0gsVUFBR2c5QixDQUFDLENBQUN5RixVQUFGLEtBQWV6RixDQUFDLENBQUN5RixVQUFGLENBQWFsa0MsSUFBYixDQUFrQitpQyxlQUFsQixFQUFrQ2pELEtBQWxDLEVBQXdDckIsQ0FBeEMsTUFBNkMsS0FBN0MsSUFBb0RsZixTQUFuRSxDQUFILEVBQWlGO0FBQUMsZUFBT3VnQixLQUFLLENBQUM0RCxLQUFOLEVBQVA7QUFBc0I7O0FBQ3hHTCxjQUFRLEdBQUMsT0FBVDtBQUFpQkosc0JBQWdCLENBQUNwcEIsR0FBakIsQ0FBcUI0a0IsQ0FBQyxDQUFDcEYsUUFBdkI7QUFBaUN5RyxXQUFLLENBQUMxM0IsSUFBTixDQUFXcTJCLENBQUMsQ0FBQzBGLE9BQWI7QUFBc0JyRSxXQUFLLENBQUN4akIsSUFBTixDQUFXbWlCLENBQUMsQ0FBQ2g1QixLQUFiO0FBQW9CODhCLGVBQVMsR0FBQzFDLDZCQUE2QixDQUFDUixVQUFELEVBQVlaLENBQVosRUFBY2g2QixPQUFkLEVBQXNCcTdCLEtBQXRCLENBQXZDOztBQUFvRSxVQUFHLENBQUN5QyxTQUFKLEVBQWM7QUFBQ242QixZQUFJLENBQUMsQ0FBQyxDQUFGLEVBQUksY0FBSixDQUFKO0FBQXlCLE9BQXhDLE1BQTRDO0FBQUMwM0IsYUFBSyxDQUFDcmdCLFVBQU4sR0FBaUIsQ0FBakI7O0FBQW1CLFlBQUdvakIsV0FBSCxFQUFlO0FBQUNHLDRCQUFrQixDQUFDN1ksT0FBbkIsQ0FBMkIsVUFBM0IsRUFBc0MsQ0FBQzJWLEtBQUQsRUFBT3JCLENBQVAsQ0FBdEM7QUFBa0Q7O0FBQ2xTLFlBQUdsZixTQUFILEVBQWE7QUFBQyxpQkFBT3VnQixLQUFQO0FBQWM7O0FBQzVCLFlBQUdyQixDQUFDLENBQUNxRCxLQUFGLElBQVNyRCxDQUFDLENBQUMxRCxPQUFGLEdBQVUsQ0FBdEIsRUFBd0I7QUFBQzRILHNCQUFZLEdBQUNwakMsTUFBTSxDQUFDOGUsVUFBUCxDQUFrQixZQUFVO0FBQUN5aEIsaUJBQUssQ0FBQzRELEtBQU4sQ0FBWSxTQUFaO0FBQXdCLFdBQXJELEVBQXNEakYsQ0FBQyxDQUFDMUQsT0FBeEQsQ0FBYjtBQUErRTs7QUFDeEcsWUFBRztBQUFDeGIsbUJBQVMsR0FBQyxLQUFWO0FBQWdCZ2pCLG1CQUFTLENBQUM2QixJQUFWLENBQWVqQixjQUFmLEVBQThCLzZCLElBQTlCO0FBQXFDLFNBQXpELENBQXlELE9BQU1vRCxDQUFOLEVBQVE7QUFBQyxjQUFHK1QsU0FBSCxFQUFhO0FBQUMsa0JBQU0vVCxDQUFOO0FBQVM7O0FBQ3pGcEQsY0FBSSxDQUFDLENBQUMsQ0FBRixFQUFJb0QsQ0FBSixDQUFKO0FBQVk7QUFBQzs7QUFDYixlQUFTcEQsSUFBVCxDQUFjcTdCLE1BQWQsRUFBcUJZLGdCQUFyQixFQUFzQzdELFNBQXRDLEVBQWdEeUQsT0FBaEQsRUFBd0Q7QUFBQyxZQUFJaEQsU0FBSjtBQUFBLFlBQWNrRCxPQUFkO0FBQUEsWUFBc0IxK0IsS0FBdEI7QUFBQSxZQUE0QnU3QixRQUE1QjtBQUFBLFlBQXFDc0QsUUFBckM7QUFBQSxZQUE4Q1gsVUFBVSxHQUFDVSxnQkFBekQ7O0FBQTBFLFlBQUc5a0IsU0FBSCxFQUFhO0FBQUM7QUFBUTs7QUFDekpBLGlCQUFTLEdBQUMsSUFBVjs7QUFBZSxZQUFHb2pCLFlBQUgsRUFBZ0I7QUFBQ3BqQyxnQkFBTSxDQUFDeTdCLFlBQVAsQ0FBb0IySCxZQUFwQjtBQUFtQzs7QUFDbkVKLGlCQUFTLEdBQUNwOUIsU0FBVjtBQUFvQnM5Qiw2QkFBcUIsR0FBQ3dCLE9BQU8sSUFBRSxFQUEvQjtBQUFrQ25FLGFBQUssQ0FBQ3JnQixVQUFOLEdBQWlCZ2tCLE1BQU0sR0FBQyxDQUFQLEdBQVMsQ0FBVCxHQUFXLENBQTVCO0FBQThCeEMsaUJBQVMsR0FBQ3dDLE1BQU0sSUFBRSxHQUFSLElBQWFBLE1BQU0sR0FBQyxHQUFwQixJQUF5QkEsTUFBTSxLQUFHLEdBQTVDOztBQUFnRCxZQUFHakQsU0FBSCxFQUFhO0FBQUNRLGtCQUFRLEdBQUNULG1CQUFtQixDQUFDOUIsQ0FBRCxFQUFHcUIsS0FBSCxFQUFTVSxTQUFULENBQTVCO0FBQWlEOztBQUNuTSxZQUFHLENBQUNTLFNBQUQsSUFBWTMrQixNQUFNLENBQUM2RCxPQUFQLENBQWUsUUFBZixFQUF3QnM0QixDQUFDLENBQUNtQixTQUExQixJQUFxQyxDQUFDLENBQXJELEVBQXVEO0FBQUNuQixXQUFDLENBQUNxQyxVQUFGLENBQWEsYUFBYixJQUE0QixZQUFVLENBQUUsQ0FBeEM7QUFBMEM7O0FBQ2xHRSxnQkFBUSxHQUFDRCxXQUFXLENBQUN0QyxDQUFELEVBQUd1QyxRQUFILEVBQVlsQixLQUFaLEVBQWtCbUIsU0FBbEIsQ0FBcEI7O0FBQWlELFlBQUdBLFNBQUgsRUFBYTtBQUFDLGNBQUd4QyxDQUFDLENBQUN1RixVQUFMLEVBQWdCO0FBQUNNLG9CQUFRLEdBQUN4RSxLQUFLLENBQUNlLGlCQUFOLENBQXdCLGVBQXhCLENBQVQ7O0FBQWtELGdCQUFHeUQsUUFBSCxFQUFZO0FBQUNoaUMsb0JBQU0sQ0FBQ2svQixZQUFQLENBQW9CZ0IsUUFBcEIsSUFBOEI4QixRQUE5QjtBQUF3Qzs7QUFDdkxBLG9CQUFRLEdBQUN4RSxLQUFLLENBQUNlLGlCQUFOLENBQXdCLE1BQXhCLENBQVQ7O0FBQXlDLGdCQUFHeUQsUUFBSCxFQUFZO0FBQUNoaUMsb0JBQU0sQ0FBQ20vQixJQUFQLENBQVllLFFBQVosSUFBc0I4QixRQUF0QjtBQUFnQztBQUFDOztBQUN2RixjQUFHYixNQUFNLEtBQUcsR0FBVCxJQUFjaEYsQ0FBQyxDQUFDeDlCLElBQUYsS0FBUyxNQUExQixFQUFpQztBQUFDMGlDLHNCQUFVLEdBQUMsV0FBWDtBQUF3QixXQUExRCxNQUErRCxJQUFHRixNQUFNLEtBQUcsR0FBWixFQUFnQjtBQUFDRSxzQkFBVSxHQUFDLGFBQVg7QUFBMEIsV0FBM0MsTUFBK0M7QUFBQ0Esc0JBQVUsR0FBQzNDLFFBQVEsQ0FBQ3JrQixLQUFwQjtBQUEwQnduQixtQkFBTyxHQUFDbkQsUUFBUSxDQUFDaGdCLElBQWpCO0FBQXNCdmIsaUJBQUssR0FBQ3U3QixRQUFRLENBQUN2N0IsS0FBZjtBQUFxQnc3QixxQkFBUyxHQUFDLENBQUN4N0IsS0FBWDtBQUFrQjtBQUFDLFNBRnRKLE1BRTBKO0FBQUNBLGVBQUssR0FBQ2srQixVQUFOOztBQUFpQixjQUFHRixNQUFNLElBQUUsQ0FBQ0UsVUFBWixFQUF1QjtBQUFDQSxzQkFBVSxHQUFDLE9BQVg7O0FBQW1CLGdCQUFHRixNQUFNLEdBQUMsQ0FBVixFQUFZO0FBQUNBLG9CQUFNLEdBQUMsQ0FBUDtBQUFVO0FBQUM7QUFBQzs7QUFDalMzRCxhQUFLLENBQUMyRCxNQUFOLEdBQWFBLE1BQWI7QUFBb0IzRCxhQUFLLENBQUM2RCxVQUFOLEdBQWlCLENBQUNVLGdCQUFnQixJQUFFVixVQUFuQixJQUErQixFQUFoRDs7QUFBbUQsWUFBRzFDLFNBQUgsRUFBYTtBQUFDcGtCLGtCQUFRLENBQUNrQixXQUFULENBQXFCZ2xCLGVBQXJCLEVBQXFDLENBQUNvQixPQUFELEVBQVNSLFVBQVQsRUFBb0I3RCxLQUFwQixDQUFyQztBQUFrRSxTQUFoRixNQUFvRjtBQUFDampCLGtCQUFRLENBQUNzQixVQUFULENBQW9CNGtCLGVBQXBCLEVBQW9DLENBQUNqRCxLQUFELEVBQU82RCxVQUFQLEVBQWtCbCtCLEtBQWxCLENBQXBDO0FBQStEOztBQUMzTnE2QixhQUFLLENBQUNvRCxVQUFOLENBQWlCQSxXQUFqQjtBQUE2QkEsbUJBQVUsR0FBQy85QixTQUFYOztBQUFxQixZQUFHMDlCLFdBQUgsRUFBZTtBQUFDRyw0QkFBa0IsQ0FBQzdZLE9BQW5CLENBQTJCOFcsU0FBUyxHQUFDLGFBQUQsR0FBZSxXQUFuRCxFQUErRCxDQUFDbkIsS0FBRCxFQUFPckIsQ0FBUCxFQUFTd0MsU0FBUyxHQUFDa0QsT0FBRCxHQUFTMStCLEtBQTNCLENBQS9EO0FBQW1HOztBQUNyS3c5Qix3QkFBZ0IsQ0FBQ3RuQixRQUFqQixDQUEwQm9uQixlQUExQixFQUEwQyxDQUFDakQsS0FBRCxFQUFPNkQsVUFBUCxDQUExQzs7QUFBOEQsWUFBR2QsV0FBSCxFQUFlO0FBQUNHLDRCQUFrQixDQUFDN1ksT0FBbkIsQ0FBMkIsY0FBM0IsRUFBMEMsQ0FBQzJWLEtBQUQsRUFBT3JCLENBQVAsQ0FBMUM7O0FBQXFELGNBQUcsQ0FBRSxHQUFFbjhCLE1BQU0sQ0FBQ2kvQixNQUFkLEVBQXNCO0FBQUNqL0Isa0JBQU0sQ0FBQzZrQixLQUFQLENBQWFnRCxPQUFiLENBQXFCLFVBQXJCO0FBQWtDO0FBQUM7QUFBQzs7QUFDOUwsYUFBTzJWLEtBQVA7QUFBYyxLQXBDQTtBQW9DQ3lFLFdBQU8sRUFBQyxpQkFBUzdDLEdBQVQsRUFBYTFnQixJQUFiLEVBQWtCeGQsUUFBbEIsRUFBMkI7QUFBQyxhQUFPbEIsTUFBTSxDQUFDVSxHQUFQLENBQVcwK0IsR0FBWCxFQUFlMWdCLElBQWYsRUFBb0J4ZCxRQUFwQixFQUE2QixNQUE3QixDQUFQO0FBQTZDLEtBcENsRjtBQW9DbUZnaEMsYUFBUyxFQUFDLG1CQUFTOUMsR0FBVCxFQUFhbCtCLFFBQWIsRUFBc0I7QUFBQyxhQUFPbEIsTUFBTSxDQUFDVSxHQUFQLENBQVcwK0IsR0FBWCxFQUFldjhCLFNBQWYsRUFBeUIzQixRQUF6QixFQUFrQyxRQUFsQyxDQUFQO0FBQW9EO0FBcEN4SyxHQUFkO0FBb0N5TGxCLFFBQU0sQ0FBQ2lCLElBQVAsQ0FBWSxDQUFDLEtBQUQsRUFBTyxNQUFQLENBQVosRUFBMkIsVUFBU3dELEVBQVQsRUFBWXFWLE1BQVosRUFBbUI7QUFBQzlaLFVBQU0sQ0FBQzhaLE1BQUQsQ0FBTixHQUFlLFVBQVNzbEIsR0FBVCxFQUFhMWdCLElBQWIsRUFBa0J4ZCxRQUFsQixFQUEyQnZDLElBQTNCLEVBQWdDO0FBQUMsVUFBR0wsVUFBVSxDQUFDb2dCLElBQUQsQ0FBYixFQUFvQjtBQUFDL2YsWUFBSSxHQUFDQSxJQUFJLElBQUV1QyxRQUFYO0FBQW9CQSxnQkFBUSxHQUFDd2QsSUFBVDtBQUFjQSxZQUFJLEdBQUM3YixTQUFMO0FBQWdCOztBQUMvVixhQUFPN0MsTUFBTSxDQUFDZ2dDLElBQVAsQ0FBWWhnQyxNQUFNLENBQUNrQyxNQUFQLENBQWM7QUFBQ2s5QixXQUFHLEVBQUNBLEdBQUw7QUFBU3pnQyxZQUFJLEVBQUNtYixNQUFkO0FBQXFCdWpCLGdCQUFRLEVBQUMxK0IsSUFBOUI7QUFBbUMrZixZQUFJLEVBQUNBLElBQXhDO0FBQTZDbWpCLGVBQU8sRUFBQzNnQztBQUFyRCxPQUFkLEVBQTZFbEIsTUFBTSxDQUFDMEMsYUFBUCxDQUFxQjA4QixHQUFyQixLQUEyQkEsR0FBeEcsQ0FBWixDQUFQO0FBQWtJLEtBRHNHO0FBQ3BHLEdBRHFEO0FBQ25EcC9CLFFBQU0sQ0FBQzgvQixhQUFQLENBQXFCLFVBQVMzRCxDQUFULEVBQVc7QUFBQyxRQUFJaDlCLENBQUo7O0FBQU0sU0FBSUEsQ0FBSixJQUFTZzlCLENBQUMsQ0FBQ3dGLE9BQVgsRUFBbUI7QUFBQyxVQUFHeGlDLENBQUMsQ0FBQ3VGLFdBQUYsT0FBa0IsY0FBckIsRUFBb0M7QUFBQ3kzQixTQUFDLENBQUNzRCxXQUFGLEdBQWN0RCxDQUFDLENBQUN3RixPQUFGLENBQVV4aUMsQ0FBVixLQUFjLEVBQTVCO0FBQWdDO0FBQUM7QUFBQyxHQUFsSTs7QUFBb0lhLFFBQU0sQ0FBQ2dzQixRQUFQLEdBQWdCLFVBQVNvVCxHQUFULEVBQWFqOUIsT0FBYixFQUFxQmpELEdBQXJCLEVBQXlCO0FBQUMsV0FBT2MsTUFBTSxDQUFDZ2dDLElBQVAsQ0FBWTtBQUFDWixTQUFHLEVBQUNBLEdBQUw7QUFBU3pnQyxVQUFJLEVBQUMsS0FBZDtBQUFvQjArQixjQUFRLEVBQUMsUUFBN0I7QUFBc0MxeUIsV0FBSyxFQUFDLElBQTVDO0FBQWlENjBCLFdBQUssRUFBQyxLQUF2RDtBQUE2RDlpQyxZQUFNLEVBQUMsS0FBcEU7QUFBMEU4aEMsZ0JBQVUsRUFBQztBQUFDLHVCQUFjLHNCQUFVLENBQUU7QUFBM0IsT0FBckY7QUFBa0hRLGdCQUFVLEVBQUMsb0JBQVNOLFFBQVQsRUFBa0I7QUFBQzErQixjQUFNLENBQUN5RCxVQUFQLENBQWtCaTdCLFFBQWxCLEVBQTJCdjhCLE9BQTNCLEVBQW1DakQsR0FBbkM7QUFBeUM7QUFBekwsS0FBWixDQUFQO0FBQWdOLEdBQTFQOztBQUEyUGMsUUFBTSxDQUFDRyxFQUFQLENBQVUrQixNQUFWLENBQWlCO0FBQUNpZ0MsV0FBTyxFQUFDLGlCQUFTcFcsSUFBVCxFQUFjO0FBQUMsVUFBSW5JLElBQUo7O0FBQVMsVUFBRyxLQUFLLENBQUwsQ0FBSCxFQUFXO0FBQUMsWUFBR3RsQixVQUFVLENBQUN5dEIsSUFBRCxDQUFiLEVBQW9CO0FBQUNBLGNBQUksR0FBQ0EsSUFBSSxDQUFDcnVCLElBQUwsQ0FBVSxLQUFLLENBQUwsQ0FBVixDQUFMO0FBQXlCOztBQUNqbkJrbUIsWUFBSSxHQUFDNWpCLE1BQU0sQ0FBQytyQixJQUFELEVBQU0sS0FBSyxDQUFMLEVBQVFwaUIsYUFBZCxDQUFOLENBQW1DcEksRUFBbkMsQ0FBc0MsQ0FBdEMsRUFBeUNnQixLQUF6QyxDQUErQyxJQUEvQyxDQUFMOztBQUEwRCxZQUFHLEtBQUssQ0FBTCxFQUFRM0MsVUFBWCxFQUFzQjtBQUFDZ2tCLGNBQUksQ0FBQytJLFlBQUwsQ0FBa0IsS0FBSyxDQUFMLENBQWxCO0FBQTRCOztBQUM3Ry9JLFlBQUksQ0FBQ3ppQixHQUFMLENBQVMsWUFBVTtBQUFDLGNBQUlDLElBQUksR0FBQyxJQUFUOztBQUFjLGlCQUFNQSxJQUFJLENBQUNnaEMsaUJBQVgsRUFBNkI7QUFBQ2hoQyxnQkFBSSxHQUFDQSxJQUFJLENBQUNnaEMsaUJBQVY7QUFBNkI7O0FBQzdGLGlCQUFPaGhDLElBQVA7QUFBYSxTQURiLEVBQ2VxckIsTUFEZixDQUNzQixJQUR0QjtBQUM2Qjs7QUFDN0IsYUFBTyxJQUFQO0FBQWEsS0FKeWdCO0FBSXhnQjRWLGFBQVMsRUFBQyxtQkFBU3RXLElBQVQsRUFBYztBQUFDLFVBQUd6dEIsVUFBVSxDQUFDeXRCLElBQUQsQ0FBYixFQUFvQjtBQUFDLGVBQU8sS0FBSzlxQixJQUFMLENBQVUsVUFBUzlCLENBQVQsRUFBVztBQUFDYSxnQkFBTSxDQUFDLElBQUQsQ0FBTixDQUFhcWlDLFNBQWIsQ0FBdUJ0VyxJQUFJLENBQUNydUIsSUFBTCxDQUFVLElBQVYsRUFBZXlCLENBQWYsQ0FBdkI7QUFBMkMsU0FBakUsQ0FBUDtBQUEyRTs7QUFDdkksYUFBTyxLQUFLOEIsSUFBTCxDQUFVLFlBQVU7QUFBQyxZQUFJc1YsSUFBSSxHQUFDdlcsTUFBTSxDQUFDLElBQUQsQ0FBZjtBQUFBLFlBQXNCK1csUUFBUSxHQUFDUixJQUFJLENBQUNRLFFBQUwsRUFBL0I7O0FBQStDLFlBQUdBLFFBQVEsQ0FBQ3ZXLE1BQVosRUFBbUI7QUFBQ3VXLGtCQUFRLENBQUNvckIsT0FBVCxDQUFpQnBXLElBQWpCO0FBQXdCLFNBQTVDLE1BQWdEO0FBQUN4VixjQUFJLENBQUNrVyxNQUFMLENBQVlWLElBQVo7QUFBbUI7QUFBQyxPQUF6SSxDQUFQO0FBQW1KLEtBTG1ZO0FBS2xZbkksUUFBSSxFQUFDLGNBQVNtSSxJQUFULEVBQWM7QUFBQyxVQUFJdVcsY0FBYyxHQUFDaGtDLFVBQVUsQ0FBQ3l0QixJQUFELENBQTdCO0FBQW9DLGFBQU8sS0FBSzlxQixJQUFMLENBQVUsVUFBUzlCLENBQVQsRUFBVztBQUFDYSxjQUFNLENBQUMsSUFBRCxDQUFOLENBQWFtaUMsT0FBYixDQUFxQkcsY0FBYyxHQUFDdlcsSUFBSSxDQUFDcnVCLElBQUwsQ0FBVSxJQUFWLEVBQWV5QixDQUFmLENBQUQsR0FBbUI0c0IsSUFBdEQ7QUFBNkQsT0FBbkYsQ0FBUDtBQUE2RixLQUw2TztBQUs1T3dXLFVBQU0sRUFBQyxnQkFBU3RpQyxRQUFULEVBQWtCO0FBQUMsV0FBS3VRLE1BQUwsQ0FBWXZRLFFBQVosRUFBc0JxVyxHQUF0QixDQUEwQixNQUExQixFQUFrQ3JWLElBQWxDLENBQXVDLFlBQVU7QUFBQ2pCLGNBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYThzQixXQUFiLENBQXlCLEtBQUs3akIsVUFBOUI7QUFBMkMsT0FBN0Y7QUFBK0YsYUFBTyxJQUFQO0FBQWE7QUFMc0csR0FBakI7O0FBS2xGakosUUFBTSxDQUFDc08sSUFBUCxDQUFZeEgsT0FBWixDQUFvQjh0QixNQUFwQixHQUEyQixVQUFTeHpCLElBQVQsRUFBYztBQUFDLFdBQU0sQ0FBQ3BCLE1BQU0sQ0FBQ3NPLElBQVAsQ0FBWXhILE9BQVosQ0FBb0IwN0IsT0FBcEIsQ0FBNEJwaEMsSUFBNUIsQ0FBUDtBQUEwQyxHQUFwRjs7QUFBcUZwQixRQUFNLENBQUNzTyxJQUFQLENBQVl4SCxPQUFaLENBQW9CMDdCLE9BQXBCLEdBQTRCLFVBQVNwaEMsSUFBVCxFQUFjO0FBQUMsV0FBTSxDQUFDLEVBQUVBLElBQUksQ0FBQ3V0QixXQUFMLElBQWtCdnRCLElBQUksQ0FBQ3FoQyxZQUF2QixJQUFxQ3JoQyxJQUFJLENBQUNneEIsY0FBTCxHQUFzQjV4QixNQUE3RCxDQUFQO0FBQTZFLEdBQXhIOztBQUF5SFIsUUFBTSxDQUFDZytCLFlBQVAsQ0FBb0IwRSxHQUFwQixHQUF3QixZQUFVO0FBQUMsUUFBRztBQUFDLGFBQU8sSUFBSXpsQyxNQUFNLENBQUMwbEMsY0FBWCxFQUFQO0FBQW9DLEtBQXhDLENBQXdDLE9BQU16NUIsQ0FBTixFQUFRLENBQUU7QUFBQyxHQUF0Rjs7QUFBdUYsTUFBSTA1QixnQkFBZ0IsR0FBQztBQUFDLE9BQUUsR0FBSDtBQUFPLFVBQUs7QUFBWixHQUFyQjtBQUFBLE1BQXNDQyxZQUFZLEdBQUM3aUMsTUFBTSxDQUFDZytCLFlBQVAsQ0FBb0IwRSxHQUFwQixFQUFuRDtBQUE2RXJrQyxTQUFPLENBQUN5a0MsSUFBUixHQUFhLENBQUMsQ0FBQ0QsWUFBRixJQUFpQixxQkFBb0JBLFlBQWxEO0FBQWdFeGtDLFNBQU8sQ0FBQzJoQyxJQUFSLEdBQWE2QyxZQUFZLEdBQUMsQ0FBQyxDQUFDQSxZQUE1QjtBQUF5QzdpQyxRQUFNLENBQUMrL0IsYUFBUCxDQUFxQixVQUFTNTlCLE9BQVQsRUFBaUI7QUFBQyxRQUFJakIsU0FBSixFQUFhNmhDLGFBQWI7O0FBQTJCLFFBQUcxa0MsT0FBTyxDQUFDeWtDLElBQVIsSUFBY0QsWUFBWSxJQUFFLENBQUMxZ0MsT0FBTyxDQUFDby9CLFdBQXhDLEVBQW9EO0FBQUMsYUFBTTtBQUFDTyxZQUFJLEVBQUMsY0FBU0gsT0FBVCxFQUFpQjVLLFFBQWpCLEVBQTBCO0FBQUMsY0FBSTUzQixDQUFKO0FBQUEsY0FBTXVqQyxHQUFHLEdBQUN2Z0MsT0FBTyxDQUFDdWdDLEdBQVIsRUFBVjtBQUF3QkEsYUFBRyxDQUFDTSxJQUFKLENBQVM3Z0MsT0FBTyxDQUFDeEQsSUFBakIsRUFBc0J3RCxPQUFPLENBQUNpOUIsR0FBOUIsRUFBa0NqOUIsT0FBTyxDQUFDcTlCLEtBQTFDLEVBQWdEcjlCLE9BQU8sQ0FBQzhnQyxRQUF4RCxFQUFpRTlnQyxPQUFPLENBQUMrUCxRQUF6RTs7QUFBbUYsY0FBRy9QLE9BQU8sQ0FBQytnQyxTQUFYLEVBQXFCO0FBQUMsaUJBQUkvakMsQ0FBSixJQUFTZ0QsT0FBTyxDQUFDK2dDLFNBQWpCLEVBQTJCO0FBQUNSLGlCQUFHLENBQUN2akMsQ0FBRCxDQUFILEdBQU9nRCxPQUFPLENBQUMrZ0MsU0FBUixDQUFrQi9qQyxDQUFsQixDQUFQO0FBQTZCO0FBQUM7O0FBQ3Z1QyxjQUFHZ0QsT0FBTyxDQUFDbThCLFFBQVIsSUFBa0JvRSxHQUFHLENBQUN4QixnQkFBekIsRUFBMEM7QUFBQ3dCLGVBQUcsQ0FBQ3hCLGdCQUFKLENBQXFCLytCLE9BQU8sQ0FBQ204QixRQUE3QjtBQUF3Qzs7QUFDbkYsY0FBRyxDQUFDbjhCLE9BQU8sQ0FBQ28vQixXQUFULElBQXNCLENBQUNJLE9BQU8sQ0FBQyxrQkFBRCxDQUFqQyxFQUFzRDtBQUFDQSxtQkFBTyxDQUFDLGtCQUFELENBQVAsR0FBNEIsZ0JBQTVCO0FBQThDOztBQUNyRyxlQUFJeGlDLENBQUosSUFBU3dpQyxPQUFULEVBQWlCO0FBQUNlLGVBQUcsQ0FBQ3pCLGdCQUFKLENBQXFCOWhDLENBQXJCLEVBQXVCd2lDLE9BQU8sQ0FBQ3hpQyxDQUFELENBQTlCO0FBQW9DOztBQUN0RCtCLG1CQUFRLEdBQUMsa0JBQVN2QyxJQUFULEVBQWM7QUFBQyxtQkFBTyxZQUFVO0FBQUMsa0JBQUd1QyxTQUFILEVBQVk7QUFBQ0EseUJBQVEsR0FBQzZoQyxhQUFhLEdBQUNMLEdBQUcsQ0FBQ1MsTUFBSixHQUFXVCxHQUFHLENBQUNVLE9BQUosR0FBWVYsR0FBRyxDQUFDVyxPQUFKLEdBQVlYLEdBQUcsQ0FBQ1ksU0FBSixHQUFjWixHQUFHLENBQUNhLGtCQUFKLEdBQXVCLElBQS9GOztBQUFvRyxvQkFBRzVrQyxJQUFJLEtBQUcsT0FBVixFQUFrQjtBQUFDK2pDLHFCQUFHLENBQUN0QixLQUFKO0FBQWEsaUJBQWhDLE1BQXFDLElBQUd6aUMsSUFBSSxLQUFHLE9BQVYsRUFBa0I7QUFBQyxzQkFBRyxPQUFPK2pDLEdBQUcsQ0FBQ3ZCLE1BQVgsS0FBb0IsUUFBdkIsRUFBZ0M7QUFBQ3BLLDRCQUFRLENBQUMsQ0FBRCxFQUFHLE9BQUgsQ0FBUjtBQUFxQixtQkFBdEQsTUFBMEQ7QUFBQ0EsNEJBQVEsQ0FBQzJMLEdBQUcsQ0FBQ3ZCLE1BQUwsRUFBWXVCLEdBQUcsQ0FBQ3JCLFVBQWhCLENBQVI7QUFBcUM7QUFBQyxpQkFBcEgsTUFBd0g7QUFBQ3RLLDBCQUFRLENBQUM2TCxnQkFBZ0IsQ0FBQ0YsR0FBRyxDQUFDdkIsTUFBTCxDQUFoQixJQUE4QnVCLEdBQUcsQ0FBQ3ZCLE1BQW5DLEVBQTBDdUIsR0FBRyxDQUFDckIsVUFBOUMsRUFBeUQsQ0FBQ3FCLEdBQUcsQ0FBQ2MsWUFBSixJQUFrQixNQUFuQixNQUE2QixNQUE3QixJQUFxQyxPQUFPZCxHQUFHLENBQUNlLFlBQVgsS0FBMEIsUUFBL0QsR0FBd0U7QUFBQ0MsMEJBQU0sRUFBQ2hCLEdBQUcsQ0FBQ2hFO0FBQVosbUJBQXhFLEdBQThGO0FBQUNuL0Isd0JBQUksRUFBQ21qQyxHQUFHLENBQUNlO0FBQVYsbUJBQXZKLEVBQStLZixHQUFHLENBQUMxQixxQkFBSixFQUEvSyxDQUFSO0FBQXFOO0FBQUM7QUFBQyxhQUF4ZjtBQUEwZixXQUFsaEI7O0FBQW1oQjBCLGFBQUcsQ0FBQ1MsTUFBSixHQUFXamlDLFNBQVEsRUFBbkI7QUFBc0I2aEMsdUJBQWEsR0FBQ0wsR0FBRyxDQUFDVSxPQUFKLEdBQVlWLEdBQUcsQ0FBQ1ksU0FBSixHQUFjcGlDLFNBQVEsQ0FBQyxPQUFELENBQWhEOztBQUEwRCxjQUFHd2hDLEdBQUcsQ0FBQ1csT0FBSixLQUFjeGdDLFNBQWpCLEVBQTJCO0FBQUM2L0IsZUFBRyxDQUFDVyxPQUFKLEdBQVlOLGFBQVo7QUFBMkIsV0FBdkQsTUFBMkQ7QUFBQ0wsZUFBRyxDQUFDYSxrQkFBSixHQUF1QixZQUFVO0FBQUMsa0JBQUdiLEdBQUcsQ0FBQ3ZsQixVQUFKLEtBQWlCLENBQXBCLEVBQXNCO0FBQUNsZ0Isc0JBQU0sQ0FBQzhlLFVBQVAsQ0FBa0IsWUFBVTtBQUFDLHNCQUFHN2EsU0FBSCxFQUFZO0FBQUM2aEMsaUNBQWE7QUFBSTtBQUFDLGlCQUE1RDtBQUErRDtBQUFDLGFBQXpIO0FBQTJIOztBQUMxeEI3aEMsbUJBQVEsR0FBQ0EsU0FBUSxDQUFDLE9BQUQsQ0FBakI7O0FBQTJCLGNBQUc7QUFBQ3doQyxlQUFHLENBQUNaLElBQUosQ0FBUzMvQixPQUFPLENBQUNzL0IsVUFBUixJQUFvQnQvQixPQUFPLENBQUN1YyxJQUE1QixJQUFrQyxJQUEzQztBQUFrRCxXQUF0RCxDQUFzRCxPQUFNeFYsQ0FBTixFQUFRO0FBQUMsZ0JBQUdoSSxTQUFILEVBQVk7QUFBQyxvQkFBTWdJLENBQU47QUFBUztBQUFDO0FBQUMsU0FMeTVCO0FBS3g1Qms0QixhQUFLLEVBQUMsaUJBQVU7QUFBQyxjQUFHbGdDLFNBQUgsRUFBWTtBQUFDQSxxQkFBUTtBQUFJO0FBQUM7QUFMNjJCLE9BQU47QUFLcDJCO0FBQUMsR0FMNHVCO0FBSzF1QmxCLFFBQU0sQ0FBQzgvQixhQUFQLENBQXFCLFVBQVMzRCxDQUFULEVBQVc7QUFBQyxRQUFHQSxDQUFDLENBQUNvRixXQUFMLEVBQWlCO0FBQUNwRixPQUFDLENBQUNwbEIsUUFBRixDQUFXMVgsTUFBWCxHQUFrQixLQUFsQjtBQUF5QjtBQUFDLEdBQTdFO0FBQStFVyxRQUFNLENBQUM0L0IsU0FBUCxDQUFpQjtBQUFDRixXQUFPLEVBQUM7QUFBQ3JnQyxZQUFNLEVBQUMsOENBQTRDO0FBQXBELEtBQVQ7QUFBaUgwWCxZQUFRLEVBQUM7QUFBQzFYLFlBQU0sRUFBQztBQUFSLEtBQTFIO0FBQTZKbS9CLGNBQVUsRUFBQztBQUFDLHFCQUFjLG9CQUFTai9CLElBQVQsRUFBYztBQUFDUyxjQUFNLENBQUN5RCxVQUFQLENBQWtCbEUsSUFBbEI7QUFBd0IsZUFBT0EsSUFBUDtBQUFhO0FBQW5FO0FBQXhLLEdBQWpCO0FBQWdRUyxRQUFNLENBQUM4L0IsYUFBUCxDQUFxQixRQUFyQixFQUE4QixVQUFTM0QsQ0FBVCxFQUFXO0FBQUMsUUFBR0EsQ0FBQyxDQUFDeHhCLEtBQUYsS0FBVTlILFNBQWIsRUFBdUI7QUFBQ3M1QixPQUFDLENBQUN4eEIsS0FBRixHQUFRLEtBQVI7QUFBZTs7QUFDcGtCLFFBQUd3eEIsQ0FBQyxDQUFDb0YsV0FBTCxFQUFpQjtBQUFDcEYsT0FBQyxDQUFDeDlCLElBQUYsR0FBTyxLQUFQO0FBQWM7QUFBQyxHQURrZDtBQUNoZHFCLFFBQU0sQ0FBQysvQixhQUFQLENBQXFCLFFBQXJCLEVBQThCLFVBQVM1RCxDQUFULEVBQVc7QUFBQyxRQUFHQSxDQUFDLENBQUNvRixXQUFGLElBQWVwRixDQUFDLENBQUN3SCxXQUFwQixFQUFnQztBQUFDLFVBQUl0a0MsTUFBSixFQUFXNkIsVUFBWDs7QUFBb0IsYUFBTTtBQUFDNGdDLFlBQUksRUFBQyxjQUFTenBCLENBQVQsRUFBVzBlLFFBQVgsRUFBb0I7QUFBQzEzQixnQkFBTSxHQUFDVyxNQUFNLENBQUMsVUFBRCxDQUFOLENBQW1Cd08sSUFBbkIsQ0FBd0IydEIsQ0FBQyxDQUFDd0gsV0FBRixJQUFlLEVBQXZDLEVBQTJDaGxCLElBQTNDLENBQWdEO0FBQUNpbEIsbUJBQU8sRUFBQ3pILENBQUMsQ0FBQzBILGFBQVg7QUFBeUJqbEMsZUFBRyxFQUFDdTlCLENBQUMsQ0FBQ2lEO0FBQS9CLFdBQWhELEVBQXFGM2EsRUFBckYsQ0FBd0YsWUFBeEYsRUFBcUd2akIsVUFBUSxHQUFDLGtCQUFTNGlDLEdBQVQsRUFBYTtBQUFDemtDLGtCQUFNLENBQUM0WixNQUFQO0FBQWdCL1gsc0JBQVEsR0FBQyxJQUFUOztBQUFjLGdCQUFHNGlDLEdBQUgsRUFBTztBQUFDL00sc0JBQVEsQ0FBQytNLEdBQUcsQ0FBQ25sQyxJQUFKLEtBQVcsT0FBWCxHQUFtQixHQUFuQixHQUF1QixHQUF4QixFQUE0Qm1sQyxHQUFHLENBQUNubEMsSUFBaEMsQ0FBUjtBQUErQztBQUFDLFdBQWxOLENBQVA7QUFBMk43QixrQkFBUSxDQUFDNEMsSUFBVCxDQUFjQyxXQUFkLENBQTBCTixNQUFNLENBQUMsQ0FBRCxDQUFoQztBQUFzQyxTQUE1UjtBQUE2UitoQyxhQUFLLEVBQUMsaUJBQVU7QUFBQyxjQUFHbGdDLFVBQUgsRUFBWTtBQUFDQSxzQkFBUTtBQUFJO0FBQUM7QUFBeFUsT0FBTjtBQUFpVjtBQUFDLEdBQWpiO0FBQW1iLE1BQUk2aUMsWUFBWSxHQUFDLEVBQWpCO0FBQUEsTUFBb0JDLE1BQU0sR0FBQyxtQkFBM0I7QUFBK0Noa0MsUUFBTSxDQUFDNC9CLFNBQVAsQ0FBaUI7QUFBQ3FFLFNBQUssRUFBQyxVQUFQO0FBQWtCQyxpQkFBYSxFQUFDLHlCQUFVO0FBQUMsVUFBSWhqQyxRQUFRLEdBQUM2aUMsWUFBWSxDQUFDeDlCLEdBQWIsTUFBcUJ2RyxNQUFNLENBQUM4QyxPQUFQLEdBQWUsR0FBZixHQUFvQmpFLEtBQUssQ0FBQ3dGLElBQU4sRUFBdEQ7QUFBcUUsV0FBS25ELFFBQUwsSUFBZSxJQUFmO0FBQW9CLGFBQU9BLFFBQVA7QUFBaUI7QUFBckosR0FBakI7QUFBeUtsQixRQUFNLENBQUM4L0IsYUFBUCxDQUFxQixZQUFyQixFQUFrQyxVQUFTM0QsQ0FBVCxFQUFXZ0ksZ0JBQVgsRUFBNEIzRyxLQUE1QixFQUFrQztBQUFDLFFBQUk0RyxZQUFKO0FBQUEsUUFBaUJDLFdBQWpCO0FBQUEsUUFBNkJDLGlCQUE3QjtBQUFBLFFBQStDQyxRQUFRLEdBQUNwSSxDQUFDLENBQUM4SCxLQUFGLEtBQVUsS0FBVixLQUFrQkQsTUFBTSxDQUFDOTVCLElBQVAsQ0FBWWl5QixDQUFDLENBQUNpRCxHQUFkLElBQW1CLEtBQW5CLEdBQXlCLE9BQU9qRCxDQUFDLENBQUN6ZCxJQUFULEtBQWdCLFFBQWhCLElBQTBCLENBQUN5ZCxDQUFDLENBQUNzRCxXQUFGLElBQWUsRUFBaEIsRUFBb0IzaEMsT0FBcEIsQ0FBNEIsbUNBQTVCLE1BQW1FLENBQTdGLElBQWdHa21DLE1BQU0sQ0FBQzk1QixJQUFQLENBQVlpeUIsQ0FBQyxDQUFDemQsSUFBZCxDQUFoRyxJQUFxSCxNQUFoSyxDQUF4RDs7QUFBZ08sUUFBRzZsQixRQUFRLElBQUVwSSxDQUFDLENBQUNtQixTQUFGLENBQVksQ0FBWixNQUFpQixPQUE5QixFQUFzQztBQUFDOEcsa0JBQVksR0FBQ2pJLENBQUMsQ0FBQytILGFBQUYsR0FBZ0I1bEMsVUFBVSxDQUFDNjlCLENBQUMsQ0FBQytILGFBQUgsQ0FBVixHQUE0Qi9ILENBQUMsQ0FBQytILGFBQUYsRUFBNUIsR0FBOEMvSCxDQUFDLENBQUMrSCxhQUE3RTs7QUFBMkYsVUFBR0ssUUFBSCxFQUFZO0FBQUNwSSxTQUFDLENBQUNvSSxRQUFELENBQUQsR0FBWXBJLENBQUMsQ0FBQ29JLFFBQUQsQ0FBRCxDQUFZdGhDLE9BQVosQ0FBb0IrZ0MsTUFBcEIsRUFBMkIsT0FBS0ksWUFBaEMsQ0FBWjtBQUEyRCxPQUF4RSxNQUE2RSxJQUFHakksQ0FBQyxDQUFDOEgsS0FBRixLQUFVLEtBQWIsRUFBbUI7QUFBQzlILFNBQUMsQ0FBQ2lELEdBQUYsSUFBTyxDQUFDNUQsTUFBTSxDQUFDdHhCLElBQVAsQ0FBWWl5QixDQUFDLENBQUNpRCxHQUFkLElBQW1CLEdBQW5CLEdBQXVCLEdBQXhCLElBQTZCakQsQ0FBQyxDQUFDOEgsS0FBL0IsR0FBcUMsR0FBckMsR0FBeUNHLFlBQWhEO0FBQThEOztBQUNwdkNqSSxPQUFDLENBQUNxQyxVQUFGLENBQWEsYUFBYixJQUE0QixZQUFVO0FBQUMsWUFBRyxDQUFDOEYsaUJBQUosRUFBc0I7QUFBQ3RrQyxnQkFBTSxDQUFDbUQsS0FBUCxDQUFhaWhDLFlBQVksR0FBQyxpQkFBMUI7QUFBOEM7O0FBQzVHLGVBQU9FLGlCQUFpQixDQUFDLENBQUQsQ0FBeEI7QUFBNkIsT0FEN0I7O0FBQzhCbkksT0FBQyxDQUFDbUIsU0FBRixDQUFZLENBQVosSUFBZSxNQUFmO0FBQXNCK0csaUJBQVcsR0FBQ3BuQyxNQUFNLENBQUNtbkMsWUFBRCxDQUFsQjs7QUFBaUNubkMsWUFBTSxDQUFDbW5DLFlBQUQsQ0FBTixHQUFxQixZQUFVO0FBQUNFLHlCQUFpQixHQUFDampDLFNBQWxCO0FBQTZCLE9BQTdEOztBQUE4RG04QixXQUFLLENBQUNsakIsTUFBTixDQUFhLFlBQVU7QUFBQyxZQUFHK3BCLFdBQVcsS0FBR3hoQyxTQUFqQixFQUEyQjtBQUFDN0MsZ0JBQU0sQ0FBQy9DLE1BQUQsQ0FBTixDQUFldThCLFVBQWYsQ0FBMEI0SyxZQUExQjtBQUF5QyxTQUFyRSxNQUF5RTtBQUFDbm5DLGdCQUFNLENBQUNtbkMsWUFBRCxDQUFOLEdBQXFCQyxXQUFyQjtBQUFrQzs7QUFDdlIsWUFBR2xJLENBQUMsQ0FBQ2lJLFlBQUQsQ0FBSixFQUFtQjtBQUFDakksV0FBQyxDQUFDK0gsYUFBRixHQUFnQkMsZ0JBQWdCLENBQUNELGFBQWpDO0FBQStDSCxzQkFBWSxDQUFDbG1DLElBQWIsQ0FBa0J1bUMsWUFBbEI7QUFBaUM7O0FBQ3BHLFlBQUdFLGlCQUFpQixJQUFFaG1DLFVBQVUsQ0FBQytsQyxXQUFELENBQWhDLEVBQThDO0FBQUNBLHFCQUFXLENBQUNDLGlCQUFpQixDQUFDLENBQUQsQ0FBbEIsQ0FBWDtBQUFtQzs7QUFDbEZBLHlCQUFpQixHQUFDRCxXQUFXLEdBQUN4aEMsU0FBOUI7QUFBeUMsT0FIMEc7QUFHeEcsYUFBTSxRQUFOO0FBQWdCO0FBQUMsR0FMa25COztBQUtobkJ4RSxTQUFPLENBQUNtbUMsa0JBQVIsR0FBNEIsWUFBVTtBQUFDLFFBQUkvaUIsSUFBSSxHQUFDM2tCLFFBQVEsQ0FBQzJuQyxjQUFULENBQXdCRCxrQkFBeEIsQ0FBMkMsRUFBM0MsRUFBK0MvaUIsSUFBeEQ7QUFBNkRBLFFBQUksQ0FBQ25VLFNBQUwsR0FBZSw0QkFBZjtBQUE0QyxXQUFPbVUsSUFBSSxDQUFDeFksVUFBTCxDQUFnQnpJLE1BQWhCLEtBQXlCLENBQWhDO0FBQW1DLEdBQXhKLEVBQTNCOztBQUF1TFIsUUFBTSxDQUFDMFcsU0FBUCxHQUFpQixVQUFTZ0ksSUFBVCxFQUFjeGUsT0FBZCxFQUFzQndrQyxXQUF0QixFQUFrQztBQUFDLFFBQUcsT0FBT2htQixJQUFQLEtBQWMsUUFBakIsRUFBMEI7QUFBQyxhQUFNLEVBQU47QUFBVTs7QUFDOVUsUUFBRyxPQUFPeGUsT0FBUCxLQUFpQixTQUFwQixFQUE4QjtBQUFDd2tDLGlCQUFXLEdBQUN4a0MsT0FBWjtBQUFvQkEsYUFBTyxHQUFDLEtBQVI7QUFBZTs7QUFDbEUsUUFBSTJTLElBQUosRUFBUzh4QixNQUFULEVBQWdCbGhCLE9BQWhCOztBQUF3QixRQUFHLENBQUN2akIsT0FBSixFQUFZO0FBQUMsVUFBRzdCLE9BQU8sQ0FBQ21tQyxrQkFBWCxFQUE4QjtBQUFDdGtDLGVBQU8sR0FBQ3BELFFBQVEsQ0FBQzJuQyxjQUFULENBQXdCRCxrQkFBeEIsQ0FBMkMsRUFBM0MsQ0FBUjtBQUF1RDN4QixZQUFJLEdBQUMzUyxPQUFPLENBQUNaLGFBQVIsQ0FBc0IsTUFBdEIsQ0FBTDtBQUFtQ3VULFlBQUksQ0FBQ3BCLElBQUwsR0FBVTNVLFFBQVEsQ0FBQ3dVLFFBQVQsQ0FBa0JHLElBQTVCO0FBQWlDdlIsZUFBTyxDQUFDUixJQUFSLENBQWFDLFdBQWIsQ0FBeUJrVCxJQUF6QjtBQUFnQyxPQUExTCxNQUE4TDtBQUFDM1MsZUFBTyxHQUFDcEQsUUFBUjtBQUFrQjtBQUFDOztBQUN2UDZuQyxVQUFNLEdBQUN4dUIsVUFBVSxDQUFDdk0sSUFBWCxDQUFnQjhVLElBQWhCLENBQVA7QUFBNkIrRSxXQUFPLEdBQUMsQ0FBQ2loQixXQUFELElBQWMsRUFBdEI7O0FBQXlCLFFBQUdDLE1BQUgsRUFBVTtBQUFDLGFBQU0sQ0FBQ3prQyxPQUFPLENBQUNaLGFBQVIsQ0FBc0JxbEMsTUFBTSxDQUFDLENBQUQsQ0FBNUIsQ0FBRCxDQUFOO0FBQTBDOztBQUMzR0EsVUFBTSxHQUFDbmhCLGFBQWEsQ0FBQyxDQUFDOUUsSUFBRCxDQUFELEVBQVF4ZSxPQUFSLEVBQWdCdWpCLE9BQWhCLENBQXBCOztBQUE2QyxRQUFHQSxPQUFPLElBQUVBLE9BQU8sQ0FBQ2pqQixNQUFwQixFQUEyQjtBQUFDUixZQUFNLENBQUN5akIsT0FBRCxDQUFOLENBQWdCeEssTUFBaEI7QUFBMEI7O0FBQ25HLFdBQU9qWixNQUFNLENBQUNlLEtBQVAsQ0FBYSxFQUFiLEVBQWdCNGpDLE1BQU0sQ0FBQzE3QixVQUF2QixDQUFQO0FBQTJDLEdBTDBNOztBQUt6TWpKLFFBQU0sQ0FBQ0csRUFBUCxDQUFVc25CLElBQVYsR0FBZSxVQUFTMlgsR0FBVCxFQUFhd0YsTUFBYixFQUFvQjFqQyxRQUFwQixFQUE2QjtBQUFDLFFBQUlqQixRQUFKO0FBQUEsUUFBYXRCLElBQWI7QUFBQSxRQUFrQisvQixRQUFsQjtBQUFBLFFBQTJCbm9CLElBQUksR0FBQyxJQUFoQztBQUFBLFFBQXFDdU8sR0FBRyxHQUFDc2EsR0FBRyxDQUFDdGhDLE9BQUosQ0FBWSxHQUFaLENBQXpDOztBQUEwRCxRQUFHZ25CLEdBQUcsR0FBQyxDQUFDLENBQVIsRUFBVTtBQUFDN2tCLGNBQVEsR0FBQzA1QixnQkFBZ0IsQ0FBQ3lGLEdBQUcsQ0FBQzdoQyxLQUFKLENBQVV1bkIsR0FBVixDQUFELENBQXpCO0FBQTBDc2EsU0FBRyxHQUFDQSxHQUFHLENBQUM3aEMsS0FBSixDQUFVLENBQVYsRUFBWXVuQixHQUFaLENBQUo7QUFBc0I7O0FBQzlOLFFBQUd4bUIsVUFBVSxDQUFDc21DLE1BQUQsQ0FBYixFQUFzQjtBQUFDMWpDLGNBQVEsR0FBQzBqQyxNQUFUO0FBQWdCQSxZQUFNLEdBQUMvaEMsU0FBUDtBQUFrQixLQUF6RCxNQUE4RCxJQUFHK2hDLE1BQU0sSUFBRSxRQUFPQSxNQUFQLE1BQWdCLFFBQTNCLEVBQW9DO0FBQUNqbUMsVUFBSSxHQUFDLE1BQUw7QUFBYTs7QUFDaEgsUUFBRzRYLElBQUksQ0FBQy9WLE1BQUwsR0FBWSxDQUFmLEVBQWlCO0FBQUNSLFlBQU0sQ0FBQ2dnQyxJQUFQLENBQVk7QUFBQ1osV0FBRyxFQUFDQSxHQUFMO0FBQVN6Z0MsWUFBSSxFQUFDQSxJQUFJLElBQUUsS0FBcEI7QUFBMEIwK0IsZ0JBQVEsRUFBQyxNQUFuQztBQUEwQzNlLFlBQUksRUFBQ2ttQjtBQUEvQyxPQUFaLEVBQW9FOStCLElBQXBFLENBQXlFLFVBQVMyOUIsWUFBVCxFQUFzQjtBQUFDL0UsZ0JBQVEsR0FBQ3I5QixTQUFUO0FBQW1Ca1YsWUFBSSxDQUFDd1YsSUFBTCxDQUFVOXJCLFFBQVEsR0FBQ0QsTUFBTSxDQUFDLE9BQUQsQ0FBTixDQUFnQnlzQixNQUFoQixDQUF1QnpzQixNQUFNLENBQUMwVyxTQUFQLENBQWlCK3NCLFlBQWpCLENBQXZCLEVBQXVEeDJCLElBQXZELENBQTREaE4sUUFBNUQsQ0FBRCxHQUF1RXdqQyxZQUF6RjtBQUF3RyxPQUEzTixFQUE2Tm5wQixNQUE3TixDQUFvT3BaLFFBQVEsSUFBRSxVQUFTczhCLEtBQVQsRUFBZTJELE1BQWYsRUFBc0I7QUFBQzVxQixZQUFJLENBQUN0VixJQUFMLENBQVUsWUFBVTtBQUFDQyxrQkFBUSxDQUFDdEQsS0FBVCxDQUFlLElBQWYsRUFBb0I4Z0MsUUFBUSxJQUFFLENBQUNsQixLQUFLLENBQUNpRyxZQUFQLEVBQW9CdEMsTUFBcEIsRUFBMkIzRCxLQUEzQixDQUE5QjtBQUFrRSxTQUF2RjtBQUEwRixPQUEvVjtBQUFrVzs7QUFDcFgsV0FBTyxJQUFQO0FBQWEsR0FIK0I7O0FBRzlCeDlCLFFBQU0sQ0FBQ3NPLElBQVAsQ0FBWXhILE9BQVosQ0FBb0IrOUIsUUFBcEIsR0FBNkIsVUFBU3pqQyxJQUFULEVBQWM7QUFBQyxXQUFPcEIsTUFBTSxDQUFDMEIsSUFBUCxDQUFZMUIsTUFBTSxDQUFDNjNCLE1BQW5CLEVBQTBCLFVBQVMxM0IsRUFBVCxFQUFZO0FBQUMsYUFBT2lCLElBQUksS0FBR2pCLEVBQUUsQ0FBQ2lCLElBQWpCO0FBQXVCLEtBQTlELEVBQWdFWixNQUF2RTtBQUErRSxHQUEzSDs7QUFBNEhSLFFBQU0sQ0FBQzhrQyxNQUFQLEdBQWM7QUFBQ0MsYUFBUyxFQUFDLG1CQUFTM2pDLElBQVQsRUFBY2UsT0FBZCxFQUFzQmhELENBQXRCLEVBQXdCO0FBQUMsVUFBSTZsQyxXQUFKO0FBQUEsVUFBZ0JDLE9BQWhCO0FBQUEsVUFBd0JDLFNBQXhCO0FBQUEsVUFBa0NDLE1BQWxDO0FBQUEsVUFBeUNDLFNBQXpDO0FBQUEsVUFBbURDLFVBQW5EO0FBQUEsVUFBOERDLGlCQUE5RDtBQUFBLFVBQWdGN1csUUFBUSxHQUFDenVCLE1BQU0sQ0FBQzJnQixHQUFQLENBQVd2ZixJQUFYLEVBQWdCLFVBQWhCLENBQXpGO0FBQUEsVUFBcUhta0MsT0FBTyxHQUFDdmxDLE1BQU0sQ0FBQ29CLElBQUQsQ0FBbkk7QUFBQSxVQUEwSWduQixLQUFLLEdBQUMsRUFBaEo7O0FBQW1KLFVBQUdxRyxRQUFRLEtBQUcsUUFBZCxFQUF1QjtBQUFDcnRCLFlBQUksQ0FBQ3FmLEtBQUwsQ0FBV2dPLFFBQVgsR0FBb0IsVUFBcEI7QUFBZ0M7O0FBQ3ZZMlcsZUFBUyxHQUFDRyxPQUFPLENBQUNULE1BQVIsRUFBVjtBQUEyQkksZUFBUyxHQUFDbGxDLE1BQU0sQ0FBQzJnQixHQUFQLENBQVd2ZixJQUFYLEVBQWdCLEtBQWhCLENBQVY7QUFBaUNpa0MsZ0JBQVUsR0FBQ3JsQyxNQUFNLENBQUMyZ0IsR0FBUCxDQUFXdmYsSUFBWCxFQUFnQixNQUFoQixDQUFYO0FBQW1Da2tDLHVCQUFpQixHQUFDLENBQUM3VyxRQUFRLEtBQUcsVUFBWCxJQUF1QkEsUUFBUSxLQUFHLE9BQW5DLEtBQTZDLENBQUN5VyxTQUFTLEdBQUNHLFVBQVgsRUFBdUJ2bkMsT0FBdkIsQ0FBK0IsTUFBL0IsSUFBdUMsQ0FBQyxDQUF2Rzs7QUFBeUcsVUFBR3duQyxpQkFBSCxFQUFxQjtBQUFDTixtQkFBVyxHQUFDTyxPQUFPLENBQUM5VyxRQUFSLEVBQVo7QUFBK0IwVyxjQUFNLEdBQUNILFdBQVcsQ0FBQ3g0QixHQUFuQjtBQUF1Qnk0QixlQUFPLEdBQUNELFdBQVcsQ0FBQ25TLElBQXBCO0FBQTBCLE9BQXRHLE1BQTBHO0FBQUNzUyxjQUFNLEdBQUNyVyxVQUFVLENBQUNvVyxTQUFELENBQVYsSUFBdUIsQ0FBOUI7QUFBZ0NELGVBQU8sR0FBQ25XLFVBQVUsQ0FBQ3VXLFVBQUQsQ0FBVixJQUF3QixDQUFoQztBQUFtQzs7QUFDdFgsVUFBRy9tQyxVQUFVLENBQUM2RCxPQUFELENBQWIsRUFBdUI7QUFBQ0EsZUFBTyxHQUFDQSxPQUFPLENBQUN6RSxJQUFSLENBQWEwRCxJQUFiLEVBQWtCakMsQ0FBbEIsRUFBb0JhLE1BQU0sQ0FBQ2tDLE1BQVAsQ0FBYyxFQUFkLEVBQWlCa2pDLFNBQWpCLENBQXBCLENBQVI7QUFBMEQ7O0FBQ2xGLFVBQUdqakMsT0FBTyxDQUFDcUssR0FBUixJQUFhLElBQWhCLEVBQXFCO0FBQUM0YixhQUFLLENBQUM1YixHQUFOLEdBQVdySyxPQUFPLENBQUNxSyxHQUFSLEdBQVk0NEIsU0FBUyxDQUFDNTRCLEdBQXZCLEdBQTRCMjRCLE1BQXRDO0FBQThDOztBQUNwRSxVQUFHaGpDLE9BQU8sQ0FBQzB3QixJQUFSLElBQWMsSUFBakIsRUFBc0I7QUFBQ3pLLGFBQUssQ0FBQ3lLLElBQU4sR0FBWTF3QixPQUFPLENBQUMwd0IsSUFBUixHQUFhdVMsU0FBUyxDQUFDdlMsSUFBeEIsR0FBOEJvUyxPQUF6QztBQUFrRDs7QUFDekUsVUFBRyxXQUFVOWlDLE9BQWIsRUFBcUI7QUFBQ0EsZUFBTyxDQUFDcWpDLEtBQVIsQ0FBYzluQyxJQUFkLENBQW1CMEQsSUFBbkIsRUFBd0JnbkIsS0FBeEI7QUFBZ0MsT0FBdEQsTUFBMEQ7QUFBQyxZQUFHLE9BQU9BLEtBQUssQ0FBQzViLEdBQWIsS0FBbUIsUUFBdEIsRUFBK0I7QUFBQzRiLGVBQUssQ0FBQzViLEdBQU4sSUFBVyxJQUFYO0FBQWlCOztBQUM1RyxZQUFHLE9BQU80YixLQUFLLENBQUN5SyxJQUFiLEtBQW9CLFFBQXZCLEVBQWdDO0FBQUN6SyxlQUFLLENBQUN5SyxJQUFOLElBQVksSUFBWjtBQUFrQjs7QUFDbkQwUyxlQUFPLENBQUM1a0IsR0FBUixDQUFZeUgsS0FBWjtBQUFvQjtBQUFDO0FBUG1JLEdBQWQ7QUFPbkhwb0IsUUFBTSxDQUFDRyxFQUFQLENBQVUrQixNQUFWLENBQWlCO0FBQUM0aUMsVUFBTSxFQUFDLGdCQUFTM2lDLE9BQVQsRUFBaUI7QUFBQyxVQUFHZCxTQUFTLENBQUNiLE1BQWIsRUFBb0I7QUFBQyxlQUFPMkIsT0FBTyxLQUFHVSxTQUFWLEdBQW9CLElBQXBCLEdBQXlCLEtBQUs1QixJQUFMLENBQVUsVUFBUzlCLENBQVQsRUFBVztBQUFDYSxnQkFBTSxDQUFDOGtDLE1BQVAsQ0FBY0MsU0FBZCxDQUF3QixJQUF4QixFQUE2QjVpQyxPQUE3QixFQUFxQ2hELENBQXJDO0FBQXlDLFNBQS9ELENBQWhDO0FBQWtHOztBQUN6TCxVQUFJc21DLElBQUo7QUFBQSxVQUFTQyxHQUFUO0FBQUEsVUFBYXRrQyxJQUFJLEdBQUMsS0FBSyxDQUFMLENBQWxCOztBQUEwQixVQUFHLENBQUNBLElBQUosRUFBUztBQUFDO0FBQVE7O0FBQzVDLFVBQUcsQ0FBQ0EsSUFBSSxDQUFDZ3hCLGNBQUwsR0FBc0I1eEIsTUFBMUIsRUFBaUM7QUFBQyxlQUFNO0FBQUNnTSxhQUFHLEVBQUMsQ0FBTDtBQUFPcW1CLGNBQUksRUFBQztBQUFaLFNBQU47QUFBc0I7O0FBQ3hENFMsVUFBSSxHQUFDcmtDLElBQUksQ0FBQ3V4QixxQkFBTCxFQUFMO0FBQWtDK1MsU0FBRyxHQUFDdGtDLElBQUksQ0FBQ3VJLGFBQUwsQ0FBbUI0QyxXQUF2QjtBQUFtQyxhQUFNO0FBQUNDLFdBQUcsRUFBQ2k1QixJQUFJLENBQUNqNUIsR0FBTCxHQUFTazVCLEdBQUcsQ0FBQ0MsV0FBbEI7QUFBOEI5UyxZQUFJLEVBQUM0UyxJQUFJLENBQUM1UyxJQUFMLEdBQVU2UyxHQUFHLENBQUNFO0FBQWpELE9BQU47QUFBcUUsS0FIbEc7QUFHbUduWCxZQUFRLEVBQUMsb0JBQVU7QUFBQyxVQUFHLENBQUMsS0FBSyxDQUFMLENBQUosRUFBWTtBQUFDO0FBQVE7O0FBQ3BMLFVBQUlvWCxZQUFKO0FBQUEsVUFBaUJmLE1BQWpCO0FBQUEsVUFBd0I1bEMsR0FBeEI7QUFBQSxVQUE0QmtDLElBQUksR0FBQyxLQUFLLENBQUwsQ0FBakM7QUFBQSxVQUF5QzBrQyxZQUFZLEdBQUM7QUFBQ3Q1QixXQUFHLEVBQUMsQ0FBTDtBQUFPcW1CLFlBQUksRUFBQztBQUFaLE9BQXREOztBQUFxRSxVQUFHN3lCLE1BQU0sQ0FBQzJnQixHQUFQLENBQVd2ZixJQUFYLEVBQWdCLFVBQWhCLE1BQThCLE9BQWpDLEVBQXlDO0FBQUMwakMsY0FBTSxHQUFDMWpDLElBQUksQ0FBQ3V4QixxQkFBTCxFQUFQO0FBQXFDLE9BQS9FLE1BQW1GO0FBQUNtUyxjQUFNLEdBQUMsS0FBS0EsTUFBTCxFQUFQO0FBQXFCNWxDLFdBQUcsR0FBQ2tDLElBQUksQ0FBQ3VJLGFBQVQ7QUFBdUJrOEIsb0JBQVksR0FBQ3prQyxJQUFJLENBQUN5a0MsWUFBTCxJQUFtQjNtQyxHQUFHLENBQUNrTixlQUFwQzs7QUFBb0QsZUFBTXk1QixZQUFZLEtBQUdBLFlBQVksS0FBRzNtQyxHQUFHLENBQUN1aUIsSUFBbkIsSUFBeUJva0IsWUFBWSxLQUFHM21DLEdBQUcsQ0FBQ2tOLGVBQS9DLENBQVosSUFBNkVwTSxNQUFNLENBQUMyZ0IsR0FBUCxDQUFXa2xCLFlBQVgsRUFBd0IsVUFBeEIsTUFBc0MsUUFBekgsRUFBa0k7QUFBQ0Esc0JBQVksR0FBQ0EsWUFBWSxDQUFDam1DLFVBQTFCO0FBQXNDOztBQUNsYSxZQUFHaW1DLFlBQVksSUFBRUEsWUFBWSxLQUFHemtDLElBQTdCLElBQW1DeWtDLFlBQVksQ0FBQ3JuQyxRQUFiLEtBQXdCLENBQTlELEVBQWdFO0FBQUNzbkMsc0JBQVksR0FBQzlsQyxNQUFNLENBQUM2bEMsWUFBRCxDQUFOLENBQXFCZixNQUFyQixFQUFiO0FBQTJDZ0Isc0JBQVksQ0FBQ3Q1QixHQUFiLElBQWtCeE0sTUFBTSxDQUFDMmdCLEdBQVAsQ0FBV2tsQixZQUFYLEVBQXdCLGdCQUF4QixFQUF5QyxJQUF6QyxDQUFsQjtBQUFpRUMsc0JBQVksQ0FBQ2pULElBQWIsSUFBbUI3eUIsTUFBTSxDQUFDMmdCLEdBQVAsQ0FBV2tsQixZQUFYLEVBQXdCLGlCQUF4QixFQUEwQyxJQUExQyxDQUFuQjtBQUFvRTtBQUFDOztBQUNsUCxhQUFNO0FBQUNyNUIsV0FBRyxFQUFDczRCLE1BQU0sQ0FBQ3Q0QixHQUFQLEdBQVdzNUIsWUFBWSxDQUFDdDVCLEdBQXhCLEdBQTRCeE0sTUFBTSxDQUFDMmdCLEdBQVAsQ0FBV3ZmLElBQVgsRUFBZ0IsV0FBaEIsRUFBNEIsSUFBNUIsQ0FBakM7QUFBbUV5eEIsWUFBSSxFQUFDaVMsTUFBTSxDQUFDalMsSUFBUCxHQUFZaVQsWUFBWSxDQUFDalQsSUFBekIsR0FBOEI3eUIsTUFBTSxDQUFDMmdCLEdBQVAsQ0FBV3ZmLElBQVgsRUFBZ0IsWUFBaEIsRUFBNkIsSUFBN0I7QUFBdEcsT0FBTjtBQUFpSixLQU56RztBQU0wR3lrQyxnQkFBWSxFQUFDLHdCQUFVO0FBQUMsYUFBTyxLQUFLMWtDLEdBQUwsQ0FBUyxZQUFVO0FBQUMsWUFBSTBrQyxZQUFZLEdBQUMsS0FBS0EsWUFBdEI7O0FBQW1DLGVBQU1BLFlBQVksSUFBRTdsQyxNQUFNLENBQUMyZ0IsR0FBUCxDQUFXa2xCLFlBQVgsRUFBd0IsVUFBeEIsTUFBc0MsUUFBMUQsRUFBbUU7QUFBQ0Esc0JBQVksR0FBQ0EsWUFBWSxDQUFDQSxZQUExQjtBQUF3Qzs7QUFDcFYsZUFBT0EsWUFBWSxJQUFFejVCLGVBQXJCO0FBQXNDLE9BRDJJLENBQVA7QUFDakk7QUFQRCxHQUFqQjtBQU9xQnBNLFFBQU0sQ0FBQ2lCLElBQVAsQ0FBWTtBQUFDZ3pCLGNBQVUsRUFBQyxhQUFaO0FBQTBCRCxhQUFTLEVBQUM7QUFBcEMsR0FBWixFQUErRCxVQUFTbGEsTUFBVCxFQUFnQjZFLElBQWhCLEVBQXFCO0FBQUMsUUFBSW5TLEdBQUcsR0FBQyxrQkFBZ0JtUyxJQUF4Qjs7QUFBNkIzZSxVQUFNLENBQUNHLEVBQVAsQ0FBVTJaLE1BQVYsSUFBa0IsVUFBUzFhLEdBQVQsRUFBYTtBQUFDLGFBQU9pZSxNQUFNLENBQUMsSUFBRCxFQUFNLFVBQVNqYyxJQUFULEVBQWMwWSxNQUFkLEVBQXFCMWEsR0FBckIsRUFBeUI7QUFBQyxZQUFJc21DLEdBQUo7O0FBQVEsWUFBR2puQyxRQUFRLENBQUMyQyxJQUFELENBQVgsRUFBa0I7QUFBQ3NrQyxhQUFHLEdBQUN0a0MsSUFBSjtBQUFVLFNBQTdCLE1BQWtDLElBQUdBLElBQUksQ0FBQzVDLFFBQUwsS0FBZ0IsQ0FBbkIsRUFBcUI7QUFBQ2tuQyxhQUFHLEdBQUN0a0MsSUFBSSxDQUFDbUwsV0FBVDtBQUFzQjs7QUFDalUsWUFBR25OLEdBQUcsS0FBR3lELFNBQVQsRUFBbUI7QUFBQyxpQkFBTzZpQyxHQUFHLEdBQUNBLEdBQUcsQ0FBQy9tQixJQUFELENBQUosR0FBV3ZkLElBQUksQ0FBQzBZLE1BQUQsQ0FBekI7QUFBbUM7O0FBQ3ZELFlBQUc0ckIsR0FBSCxFQUFPO0FBQUNBLGFBQUcsQ0FBQ0ssUUFBSixDQUFhLENBQUN2NUIsR0FBRCxHQUFLcE4sR0FBTCxHQUFTc21DLEdBQUcsQ0FBQ0UsV0FBMUIsRUFBc0NwNUIsR0FBRyxHQUFDcE4sR0FBRCxHQUFLc21DLEdBQUcsQ0FBQ0MsV0FBbEQ7QUFBZ0UsU0FBeEUsTUFBNEU7QUFBQ3ZrQyxjQUFJLENBQUMwWSxNQUFELENBQUosR0FBYTFhLEdBQWI7QUFBa0I7QUFBQyxPQUYyRyxFQUUxRzBhLE1BRjBHLEVBRW5HMWEsR0FGbUcsRUFFL0ZpQyxTQUFTLENBQUNiLE1BRnFGLENBQWI7QUFFL0QsS0FGK0I7QUFFN0IsR0FGckY7QUFFdUZSLFFBQU0sQ0FBQ2lCLElBQVAsQ0FBWSxDQUFDLEtBQUQsRUFBTyxNQUFQLENBQVosRUFBMkIsVUFBU3dELEVBQVQsRUFBWWthLElBQVosRUFBaUI7QUFBQzNlLFVBQU0sQ0FBQ3F5QixRQUFQLENBQWdCMVQsSUFBaEIsSUFBc0J1UixZQUFZLENBQUM3eEIsT0FBTyxDQUFDK3dCLGFBQVQsRUFBdUIsVUFBU2h1QixJQUFULEVBQWMwdUIsUUFBZCxFQUF1QjtBQUFDLFVBQUdBLFFBQUgsRUFBWTtBQUFDQSxnQkFBUSxHQUFDRCxNQUFNLENBQUN6dUIsSUFBRCxFQUFNdWQsSUFBTixDQUFmO0FBQTJCLGVBQU8yTyxTQUFTLENBQUNwakIsSUFBVixDQUFlNGxCLFFBQWYsSUFBeUI5dkIsTUFBTSxDQUFDb0IsSUFBRCxDQUFOLENBQWFxdEIsUUFBYixHQUF3QjlQLElBQXhCLElBQThCLElBQXZELEdBQTREbVIsUUFBbkU7QUFBNkU7QUFBQyxLQUFySyxDQUFsQztBQUEwTSxHQUF2UDtBQUF5UDl2QixRQUFNLENBQUNpQixJQUFQLENBQVk7QUFBQytrQyxVQUFNLEVBQUMsUUFBUjtBQUFpQkMsU0FBSyxFQUFDO0FBQXZCLEdBQVosRUFBNEMsVUFBUzdqQyxJQUFULEVBQWN6RCxJQUFkLEVBQW1CO0FBQUNxQixVQUFNLENBQUNpQixJQUFQLENBQVk7QUFBQzh4QixhQUFPLEVBQUMsVUFBUTN3QixJQUFqQjtBQUFzQjRWLGFBQU8sRUFBQ3JaLElBQTlCO0FBQW1DLFVBQUcsVUFBUXlEO0FBQTlDLEtBQVosRUFBZ0UsVUFBUzhqQyxZQUFULEVBQXNCQyxRQUF0QixFQUErQjtBQUFDbm1DLFlBQU0sQ0FBQ0csRUFBUCxDQUFVZ21DLFFBQVYsSUFBb0IsVUFBU3JULE1BQVQsRUFBZ0IxdUIsS0FBaEIsRUFBc0I7QUFBQyxZQUFJa1osU0FBUyxHQUFDamMsU0FBUyxDQUFDYixNQUFWLEtBQW1CMGxDLFlBQVksSUFBRSxPQUFPcFQsTUFBUCxLQUFnQixTQUFqRCxDQUFkO0FBQUEsWUFBMEVqQixLQUFLLEdBQUNxVSxZQUFZLEtBQUdwVCxNQUFNLEtBQUcsSUFBVCxJQUFlMXVCLEtBQUssS0FBRyxJQUF2QixHQUE0QixRQUE1QixHQUFxQyxRQUF4QyxDQUE1RjtBQUE4SSxlQUFPaVosTUFBTSxDQUFDLElBQUQsRUFBTSxVQUFTamMsSUFBVCxFQUFjekMsSUFBZCxFQUFtQnlGLEtBQW5CLEVBQXlCO0FBQUMsY0FBSWxGLEdBQUo7O0FBQVEsY0FBR1QsUUFBUSxDQUFDMkMsSUFBRCxDQUFYLEVBQWtCO0FBQUMsbUJBQU8ra0MsUUFBUSxDQUFDcm9DLE9BQVQsQ0FBaUIsT0FBakIsTUFBNEIsQ0FBNUIsR0FBOEJzRCxJQUFJLENBQUMsVUFBUWdCLElBQVQsQ0FBbEMsR0FBaURoQixJQUFJLENBQUN0RSxRQUFMLENBQWNzUCxlQUFkLENBQThCLFdBQVNoSyxJQUF2QyxDQUF4RDtBQUFzRzs7QUFDbjRCLGNBQUdoQixJQUFJLENBQUM1QyxRQUFMLEtBQWdCLENBQW5CLEVBQXFCO0FBQUNVLGVBQUcsR0FBQ2tDLElBQUksQ0FBQ2dMLGVBQVQ7QUFBeUIsbUJBQU9ySixJQUFJLENBQUN1dUIsR0FBTCxDQUFTbHdCLElBQUksQ0FBQ3FnQixJQUFMLENBQVUsV0FBU3JmLElBQW5CLENBQVQsRUFBa0NsRCxHQUFHLENBQUMsV0FBU2tELElBQVYsQ0FBckMsRUFBcURoQixJQUFJLENBQUNxZ0IsSUFBTCxDQUFVLFdBQVNyZixJQUFuQixDQUFyRCxFQUE4RWxELEdBQUcsQ0FBQyxXQUFTa0QsSUFBVixDQUFqRixFQUFpR2xELEdBQUcsQ0FBQyxXQUFTa0QsSUFBVixDQUFwRyxDQUFQO0FBQTZIOztBQUM1SyxpQkFBT2dDLEtBQUssS0FBR3ZCLFNBQVIsR0FBa0I3QyxNQUFNLENBQUMyZ0IsR0FBUCxDQUFXdmYsSUFBWCxFQUFnQnpDLElBQWhCLEVBQXFCa3pCLEtBQXJCLENBQWxCLEdBQThDN3hCLE1BQU0sQ0FBQ3lnQixLQUFQLENBQWFyZixJQUFiLEVBQWtCekMsSUFBbEIsRUFBdUJ5RixLQUF2QixFQUE2Qnl0QixLQUE3QixDQUFyRDtBQUEwRixTQUZ3b0IsRUFFdm9CbHpCLElBRnVvQixFQUVsb0IyZSxTQUFTLEdBQUN3VixNQUFELEdBQVFqd0IsU0FGaW5CLEVBRXZtQnlhLFNBRnVtQixDQUFiO0FBRTlrQixPQUZxWjtBQUVuWixLQUZtVDtBQUVoVCxHQUZnUDtBQUU5T3RkLFFBQU0sQ0FBQ2lCLElBQVAsQ0FBWSxDQUFDLFdBQUQsRUFBYSxVQUFiLEVBQXdCLGNBQXhCLEVBQXVDLFdBQXZDLEVBQW1ELGFBQW5ELEVBQWlFLFVBQWpFLENBQVosRUFBeUYsVUFBU3dELEVBQVQsRUFBWTlGLElBQVosRUFBaUI7QUFBQ3FCLFVBQU0sQ0FBQ0csRUFBUCxDQUFVeEIsSUFBVixJQUFnQixVQUFTd0IsRUFBVCxFQUFZO0FBQUMsYUFBTyxLQUFLc2tCLEVBQUwsQ0FBUTlsQixJQUFSLEVBQWF3QixFQUFiLENBQVA7QUFBeUIsS0FBdEQ7QUFBd0QsR0FBbks7QUFBcUtILFFBQU0sQ0FBQ0csRUFBUCxDQUFVK0IsTUFBVixDQUFpQjtBQUFDNDBCLFFBQUksRUFBQyxjQUFTcFMsS0FBVCxFQUFlaEcsSUFBZixFQUFvQnZlLEVBQXBCLEVBQXVCO0FBQUMsYUFBTyxLQUFLc2tCLEVBQUwsQ0FBUUMsS0FBUixFQUFjLElBQWQsRUFBbUJoRyxJQUFuQixFQUF3QnZlLEVBQXhCLENBQVA7QUFBb0MsS0FBbEU7QUFBbUVpbUMsVUFBTSxFQUFDLGdCQUFTMWhCLEtBQVQsRUFBZXZrQixFQUFmLEVBQWtCO0FBQUMsYUFBTyxLQUFLMmtCLEdBQUwsQ0FBU0osS0FBVCxFQUFlLElBQWYsRUFBb0J2a0IsRUFBcEIsQ0FBUDtBQUFnQyxLQUE3SDtBQUE4SGttQyxZQUFRLEVBQUMsa0JBQVNwbUMsUUFBVCxFQUFrQnlrQixLQUFsQixFQUF3QmhHLElBQXhCLEVBQTZCdmUsRUFBN0IsRUFBZ0M7QUFBQyxhQUFPLEtBQUtza0IsRUFBTCxDQUFRQyxLQUFSLEVBQWN6a0IsUUFBZCxFQUF1QnllLElBQXZCLEVBQTRCdmUsRUFBNUIsQ0FBUDtBQUF3QyxLQUFoTjtBQUFpTm1tQyxjQUFVLEVBQUMsb0JBQVNybUMsUUFBVCxFQUFrQnlrQixLQUFsQixFQUF3QnZrQixFQUF4QixFQUEyQjtBQUFDLGFBQU9rQixTQUFTLENBQUNiLE1BQVYsS0FBbUIsQ0FBbkIsR0FBcUIsS0FBS3NrQixHQUFMLENBQVM3a0IsUUFBVCxFQUFrQixJQUFsQixDQUFyQixHQUE2QyxLQUFLNmtCLEdBQUwsQ0FBU0osS0FBVCxFQUFlemtCLFFBQVEsSUFBRSxJQUF6QixFQUE4QkUsRUFBOUIsQ0FBcEQ7QUFBdUYsS0FBL1U7QUFBZ1ZvbUMsU0FBSyxFQUFDLGVBQVNDLE1BQVQsRUFBZ0JDLEtBQWhCLEVBQXNCO0FBQUMsYUFBTyxLQUFLamMsVUFBTCxDQUFnQmdjLE1BQWhCLEVBQXdCL2IsVUFBeEIsQ0FBbUNnYyxLQUFLLElBQUVELE1BQTFDLENBQVA7QUFBMEQ7QUFBdmEsR0FBakI7QUFBMmJ4bUMsUUFBTSxDQUFDaUIsSUFBUCxDQUFZLENBQUMsOERBQTRELHVFQUE1RCxHQUFvSSx5REFBckksRUFBZ011RCxLQUFoTSxDQUFzTSxHQUF0TSxDQUFaLEVBQXVOLFVBQVNDLEVBQVQsRUFBWXJDLElBQVosRUFBaUI7QUFBQ3BDLFVBQU0sQ0FBQ0csRUFBUCxDQUFVaUMsSUFBVixJQUFnQixVQUFTc2MsSUFBVCxFQUFjdmUsRUFBZCxFQUFpQjtBQUFDLGFBQU9rQixTQUFTLENBQUNiLE1BQVYsR0FBaUIsQ0FBakIsR0FBbUIsS0FBS2lrQixFQUFMLENBQVFyaUIsSUFBUixFQUFhLElBQWIsRUFBa0JzYyxJQUFsQixFQUF1QnZlLEVBQXZCLENBQW5CLEdBQThDLEtBQUswbkIsT0FBTCxDQUFhemxCLElBQWIsQ0FBckQ7QUFBeUUsS0FBM0c7QUFBNkcsR0FBdFY7QUFBd1YsTUFBSTZFLEtBQUssR0FBQyxvQ0FBVjs7QUFBK0NqSCxRQUFNLENBQUMwbUMsS0FBUCxHQUFhLFVBQVN2bUMsRUFBVCxFQUFZRCxPQUFaLEVBQW9CO0FBQUMsUUFBSWtOLEdBQUosRUFBUXlELElBQVIsRUFBYTYxQixLQUFiOztBQUFtQixRQUFHLE9BQU94bUMsT0FBUCxLQUFpQixRQUFwQixFQUE2QjtBQUFDa04sU0FBRyxHQUFDak4sRUFBRSxDQUFDRCxPQUFELENBQU47QUFBZ0JBLGFBQU8sR0FBQ0MsRUFBUjtBQUFXQSxRQUFFLEdBQUNpTixHQUFIO0FBQVE7O0FBQzN1QyxRQUFHLENBQUM5TyxVQUFVLENBQUM2QixFQUFELENBQWQsRUFBbUI7QUFBQyxhQUFPMEMsU0FBUDtBQUFrQjs7QUFDdENnTyxRQUFJLEdBQUN0VCxNQUFLLENBQUNHLElBQU4sQ0FBVzJELFNBQVgsRUFBcUIsQ0FBckIsQ0FBTDs7QUFBNkJxbEMsU0FBSyxHQUFDLGlCQUFVO0FBQUMsYUFBT3ZtQyxFQUFFLENBQUN2QyxLQUFILENBQVNzQyxPQUFPLElBQUUsSUFBbEIsRUFBdUIyUSxJQUFJLENBQUNsVCxNQUFMLENBQVlKLE1BQUssQ0FBQ0csSUFBTixDQUFXMkQsU0FBWCxDQUFaLENBQXZCLENBQVA7QUFBbUUsS0FBcEY7O0FBQXFGcWxDLFNBQUssQ0FBQ3JpQyxJQUFOLEdBQVdsRSxFQUFFLENBQUNrRSxJQUFILEdBQVFsRSxFQUFFLENBQUNrRSxJQUFILElBQVNyRSxNQUFNLENBQUNxRSxJQUFQLEVBQTVCO0FBQTBDLFdBQU9xaUMsS0FBUDtBQUFjLEdBRjI4Qjs7QUFFMThCMW1DLFFBQU0sQ0FBQzJtQyxTQUFQLEdBQWlCLFVBQVNDLElBQVQsRUFBYztBQUFDLFFBQUdBLElBQUgsRUFBUTtBQUFDNW1DLFlBQU0sQ0FBQytjLFNBQVA7QUFBb0IsS0FBN0IsTUFBaUM7QUFBQy9jLFlBQU0sQ0FBQzJXLEtBQVAsQ0FBYSxJQUFiO0FBQW9CO0FBQUMsR0FBdkY7O0FBQXdGM1csUUFBTSxDQUFDNEMsT0FBUCxHQUFlRCxLQUFLLENBQUNDLE9BQXJCO0FBQTZCNUMsUUFBTSxDQUFDNm1DLFNBQVAsR0FBaUIzbkIsSUFBSSxDQUFDQyxLQUF0QjtBQUE0Qm5mLFFBQU0sQ0FBQzhJLFFBQVAsR0FBZ0JBLFFBQWhCO0FBQXlCOUksUUFBTSxDQUFDMUIsVUFBUCxHQUFrQkEsVUFBbEI7QUFBNkIwQixRQUFNLENBQUN2QixRQUFQLEdBQWdCQSxRQUFoQjtBQUF5QnVCLFFBQU0sQ0FBQ2llLFNBQVAsR0FBaUJBLFNBQWpCO0FBQTJCamUsUUFBTSxDQUFDckIsSUFBUCxHQUFZbUIsTUFBWjtBQUFtQkUsUUFBTSxDQUFDeW9CLEdBQVAsR0FBVzlpQixJQUFJLENBQUM4aUIsR0FBaEI7O0FBQW9Cem9CLFFBQU0sQ0FBQzhtQyxTQUFQLEdBQWlCLFVBQVN2b0MsR0FBVCxFQUFhO0FBQUMsUUFBSUksSUFBSSxHQUFDcUIsTUFBTSxDQUFDckIsSUFBUCxDQUFZSixHQUFaLENBQVQ7QUFBMEIsV0FBTSxDQUFDSSxJQUFJLEtBQUcsUUFBUCxJQUFpQkEsSUFBSSxLQUFHLFFBQXpCLEtBQW9DLENBQUNvb0MsS0FBSyxDQUFDeG9DLEdBQUcsR0FBQ3V3QixVQUFVLENBQUN2d0IsR0FBRCxDQUFmLENBQWhEO0FBQXVFLEdBQWhJOztBQUFpSXlCLFFBQU0sQ0FBQ2duQyxJQUFQLEdBQVksVUFBU3puQyxJQUFULEVBQWM7QUFBQyxXQUFPQSxJQUFJLElBQUUsSUFBTixHQUFXLEVBQVgsR0FBYyxDQUFDQSxJQUFJLEdBQUMsRUFBTixFQUFVMEQsT0FBVixDQUFrQmdFLEtBQWxCLEVBQXdCLEVBQXhCLENBQXJCO0FBQWtELEdBQTdFOztBQUE4RSxNQUFHLElBQUgsRUFBMEM7QUFBQ2dnQyxxQ0FBZ0IsRUFBVixpQ0FBYSxZQUFVO0FBQUMsYUFBT2puQyxNQUFQO0FBQWUsS0FBdkMsZ0RBQU47QUFBZ0Q7O0FBQ3Z2QixNQUNBa25DLE9BQU8sR0FBQ2pxQyxNQUFNLENBQUMrQyxNQURmO0FBQUEsTUFDc0JtbkMsRUFBRSxHQUFDbHFDLE1BQU0sQ0FBQ21xQyxDQURoQzs7QUFDa0NwbkMsUUFBTSxDQUFDcW5DLFVBQVAsR0FBa0IsVUFBUzVrQyxJQUFULEVBQWM7QUFBQyxRQUFHeEYsTUFBTSxDQUFDbXFDLENBQVAsS0FBV3BuQyxNQUFkLEVBQXFCO0FBQUMvQyxZQUFNLENBQUNtcUMsQ0FBUCxHQUFTRCxFQUFUO0FBQWE7O0FBQ3RHLFFBQUcxa0MsSUFBSSxJQUFFeEYsTUFBTSxDQUFDK0MsTUFBUCxLQUFnQkEsTUFBekIsRUFBZ0M7QUFBQy9DLFlBQU0sQ0FBQytDLE1BQVAsR0FBY2tuQyxPQUFkO0FBQXVCOztBQUN4RCxXQUFPbG5DLE1BQVA7QUFBZSxHQUZtQjs7QUFFbEIsTUFBRyxPQUFPOUMsUUFBUCxLQUFrQixXQUFyQixFQUFpQztBQUFDRCxVQUFNLENBQUMrQyxNQUFQLEdBQWMvQyxNQUFNLENBQUNtcUMsQ0FBUCxHQUFTcG5DLE1BQXZCO0FBQStCOztBQUNqRixTQUFPQSxNQUFQO0FBQWUsQ0EvcUJmOztBQStxQmlCQSxNQUFNLENBQUNxbkMsVUFBUDtBQUFvQjtBQUNyQztBQUNBO0FBQ0E7QUFDQTs7QUFDQSxDQUFDLFVBQVMxcUMsT0FBVCxFQUFpQjtBQUFDOztBQUFhLE1BQUcsSUFBSCxFQUEwQztBQUFDc3FDLHFDQUFPLENBQUMsMEJBQUQsQ0FBRCxtQ0FBWSxVQUFTam5DLE1BQVQsRUFBZ0I7QUFBQyxhQUFPckQsT0FBTyxDQUFDcUQsTUFBRCxFQUFRL0MsTUFBUixDQUFkO0FBQStCLEtBQTVEO0FBQUEsb0dBQU47QUFBcUUsR0FBaEgsTUFBcUgsRUFBNEg7QUFBQyxDQUFsUixFQUFvUixVQUFTK0MsTUFBVCxFQUFnQi9DLE1BQWhCLEVBQXVCO0FBQUM7O0FBQWErQyxRQUFNLENBQUNzbkMsY0FBUCxHQUFzQixPQUF0Qjs7QUFBOEIsV0FBU0MsZUFBVCxDQUF5QkMsRUFBekIsRUFBNEJDLEVBQTVCLEVBQStCO0FBQUMsUUFBSXRvQyxDQUFKO0FBQUEsUUFBTXVvQyxhQUFhLEdBQUMsc0JBQXBCO0FBQUEsUUFBMkNDLEdBQUcsR0FBQ0QsYUFBYSxDQUFDOTlCLElBQWQsQ0FBbUI0OUIsRUFBbkIsS0FBd0IsRUFBdkU7QUFBQSxRQUEwRUksR0FBRyxHQUFDRixhQUFhLENBQUM5OUIsSUFBZCxDQUFtQjY5QixFQUFuQixLQUF3QixFQUF0Rzs7QUFBeUcsU0FBSXRvQyxDQUFDLEdBQUMsQ0FBTixFQUFRQSxDQUFDLElBQUUsQ0FBWCxFQUFhQSxDQUFDLEVBQWQsRUFBaUI7QUFBQyxVQUFHLENBQUN3b0MsR0FBRyxDQUFDeG9DLENBQUQsQ0FBSixHQUFRLENBQUN5b0MsR0FBRyxDQUFDem9DLENBQUQsQ0FBZixFQUFtQjtBQUFDLGVBQU8sQ0FBUDtBQUFVOztBQUNoaEIsVUFBRyxDQUFDd29DLEdBQUcsQ0FBQ3hvQyxDQUFELENBQUosR0FBUSxDQUFDeW9DLEdBQUcsQ0FBQ3pvQyxDQUFELENBQWYsRUFBbUI7QUFBQyxlQUFNLENBQUMsQ0FBUDtBQUFVO0FBQUM7O0FBQy9CLFdBQU8sQ0FBUDtBQUFVOztBQUNWLFdBQVMwb0Msa0JBQVQsQ0FBNEI5bkMsT0FBNUIsRUFBb0M7QUFBQyxXQUFPd25DLGVBQWUsQ0FBQ3ZuQyxNQUFNLENBQUNHLEVBQVAsQ0FBVUcsTUFBWCxFQUFrQlAsT0FBbEIsQ0FBZixJQUEyQyxDQUFsRDtBQUFxRDs7QUFDMUYsR0FBQyxZQUFVO0FBQUMsUUFBRyxDQUFDOUMsTUFBTSxDQUFDeWYsT0FBUixJQUFpQixDQUFDemYsTUFBTSxDQUFDeWYsT0FBUCxDQUFlb3JCLEdBQXBDLEVBQXdDO0FBQUM7QUFBUTs7QUFDN0QsUUFBRyxDQUFDOW5DLE1BQUQsSUFBUyxDQUFDNm5DLGtCQUFrQixDQUFDLE9BQUQsQ0FBL0IsRUFBeUM7QUFBQzVxQyxZQUFNLENBQUN5ZixPQUFQLENBQWVvckIsR0FBZixDQUFtQixtQ0FBbkI7QUFBeUQ7O0FBQ25HLFFBQUc5bkMsTUFBTSxDQUFDK25DLGVBQVYsRUFBMEI7QUFBQzlxQyxZQUFNLENBQUN5ZixPQUFQLENBQWVvckIsR0FBZixDQUFtQixpREFBbkI7QUFBdUU7O0FBQ2xHN3FDLFVBQU0sQ0FBQ3lmLE9BQVAsQ0FBZW9yQixHQUFmLENBQW1CLHFDQUNsQjluQyxNQUFNLENBQUNnb0MsV0FBUCxHQUFtQixFQUFuQixHQUFzQixzQkFESixJQUM0QixZQUQ1QixHQUN5Q2hvQyxNQUFNLENBQUNzbkMsY0FEbkU7QUFDb0YsR0FKcEY7O0FBSXdGLE1BQUlXLFdBQVcsR0FBQyxFQUFoQjtBQUFtQmpvQyxRQUFNLENBQUNrb0MsMEJBQVAsR0FBa0MsSUFBbEM7QUFBdUNsb0MsUUFBTSxDQUFDK25DLGVBQVAsR0FBdUIsRUFBdkI7O0FBQTBCLE1BQUcvbkMsTUFBTSxDQUFDbW9DLFlBQVAsS0FBc0J0bEMsU0FBekIsRUFBbUM7QUFBQzdDLFVBQU0sQ0FBQ21vQyxZQUFQLEdBQW9CLElBQXBCO0FBQTBCOztBQUMxT25vQyxRQUFNLENBQUNvb0MsWUFBUCxHQUFvQixZQUFVO0FBQUNILGVBQVcsR0FBQyxFQUFaO0FBQWVqb0MsVUFBTSxDQUFDK25DLGVBQVAsQ0FBdUJ2bkMsTUFBdkIsR0FBOEIsQ0FBOUI7QUFBaUMsR0FBL0U7O0FBQWdGLFdBQVM2bkMsV0FBVCxDQUFxQmpsQyxHQUFyQixFQUF5QjtBQUFDLFFBQUlzWixPQUFPLEdBQUN6ZixNQUFNLENBQUN5ZixPQUFuQjs7QUFBMkIsUUFBRyxDQUFDMWMsTUFBTSxDQUFDa29DLDBCQUFSLElBQW9DLENBQUNELFdBQVcsQ0FBQzdrQyxHQUFELENBQW5ELEVBQXlEO0FBQUM2a0MsaUJBQVcsQ0FBQzdrQyxHQUFELENBQVgsR0FBaUIsSUFBakI7QUFBc0JwRCxZQUFNLENBQUMrbkMsZUFBUCxDQUF1QmxxQyxJQUF2QixDQUE0QnVGLEdBQTVCOztBQUFpQyxVQUFHc1osT0FBTyxJQUFFQSxPQUFPLENBQUNDLElBQWpCLElBQXVCLENBQUMzYyxNQUFNLENBQUNnb0MsV0FBbEMsRUFBOEM7QUFBQ3RyQixlQUFPLENBQUNDLElBQVIsQ0FBYSxnQkFBY3ZaLEdBQTNCOztBQUFnQyxZQUFHcEQsTUFBTSxDQUFDbW9DLFlBQVAsSUFBcUJ6ckIsT0FBTyxDQUFDNHJCLEtBQWhDLEVBQXNDO0FBQUM1ckIsaUJBQU8sQ0FBQzRyQixLQUFSO0FBQWlCO0FBQUM7QUFBQztBQUFDOztBQUNoWSxXQUFTQyxlQUFULENBQXlCaHFDLEdBQXpCLEVBQTZCb2dCLElBQTdCLEVBQWtDdmEsS0FBbEMsRUFBd0NoQixHQUF4QyxFQUE0QztBQUFDL0YsVUFBTSxDQUFDa2hCLGNBQVAsQ0FBc0JoZ0IsR0FBdEIsRUFBMEJvZ0IsSUFBMUIsRUFBK0I7QUFBQ0gsa0JBQVksRUFBQyxJQUFkO0FBQW1COEksZ0JBQVUsRUFBQyxJQUE5QjtBQUFtQzVtQixTQUFHLEVBQUMsZUFBVTtBQUFDMm5DLG1CQUFXLENBQUNqbEMsR0FBRCxDQUFYO0FBQWlCLGVBQU9nQixLQUFQO0FBQWMsT0FBakY7QUFBa0ZxYSxTQUFHLEVBQUMsYUFBUytwQixRQUFULEVBQWtCO0FBQUNILG1CQUFXLENBQUNqbEMsR0FBRCxDQUFYO0FBQWlCZ0IsYUFBSyxHQUFDb2tDLFFBQU47QUFBZ0I7QUFBMUksS0FBL0I7QUFBNks7O0FBQzFOLFdBQVNDLGVBQVQsQ0FBeUJscUMsR0FBekIsRUFBNkJvZ0IsSUFBN0IsRUFBa0MrcEIsT0FBbEMsRUFBMEN0bEMsR0FBMUMsRUFBOEM7QUFBQzdFLE9BQUcsQ0FBQ29nQixJQUFELENBQUgsR0FBVSxZQUFVO0FBQUMwcEIsaUJBQVcsQ0FBQ2psQyxHQUFELENBQVg7QUFBaUIsYUFBT3NsQyxPQUFPLENBQUM5cUMsS0FBUixDQUFjLElBQWQsRUFBbUJ5RCxTQUFuQixDQUFQO0FBQXNDLEtBQTVFO0FBQThFOztBQUM3SCxNQUFHcEUsTUFBTSxDQUFDSCxRQUFQLENBQWdCNnJDLFVBQWhCLEtBQTZCLFlBQWhDLEVBQTZDO0FBQUNOLGVBQVcsQ0FBQywyQ0FBRCxDQUFYO0FBQTBEOztBQUN4RyxNQUFJTyxRQUFKO0FBQUEsTUFBYTdxQyxVQUFVLEdBQUMsRUFBeEI7QUFBQSxNQUEyQjhxQyxPQUFPLEdBQUM3b0MsTUFBTSxDQUFDRyxFQUFQLENBQVVDLElBQTdDO0FBQUEsTUFBa0Qwb0MsT0FBTyxHQUFDOW9DLE1BQU0sQ0FBQ2lOLElBQWpFO0FBQUEsTUFBc0U4N0IsYUFBYSxHQUFDLHVEQUFwRjtBQUFBLE1BQTRJQyxhQUFhLEdBQUMsd0RBQTFKO0FBQUEsTUFBbU4vaEMsS0FBSyxHQUFDLG9DQUF6Tjs7QUFBOFBqSCxRQUFNLENBQUNHLEVBQVAsQ0FBVUMsSUFBVixHQUFlLFVBQVM2b0MsSUFBVCxFQUFjO0FBQUMsUUFBSXA0QixJQUFJLEdBQUNsTyxLQUFLLENBQUN0QyxTQUFOLENBQWdCOUMsS0FBaEIsQ0FBc0JHLElBQXRCLENBQTJCMkQsU0FBM0IsQ0FBVDs7QUFBK0MsUUFBRyxPQUFPNG5DLElBQVAsS0FBYyxRQUFkLElBQXdCQSxJQUFJLEtBQUcsR0FBbEMsRUFBc0M7QUFBQ1osaUJBQVcsQ0FBQyx1Q0FBRCxDQUFYO0FBQXFEeDNCLFVBQUksQ0FBQyxDQUFELENBQUosR0FBUSxFQUFSO0FBQVk7O0FBQ25iLFdBQU9nNEIsT0FBTyxDQUFDanJDLEtBQVIsQ0FBYyxJQUFkLEVBQW1CaVQsSUFBbkIsQ0FBUDtBQUFpQyxHQUQ2Tjs7QUFDNU43USxRQUFNLENBQUNHLEVBQVAsQ0FBVUMsSUFBVixDQUFlQyxTQUFmLEdBQXlCTCxNQUFNLENBQUNHLEVBQWhDOztBQUFtQ0gsUUFBTSxDQUFDaU4sSUFBUCxHQUFZLFVBQVNoTixRQUFULEVBQWtCO0FBQUMsUUFBSTRRLElBQUksR0FBQ2xPLEtBQUssQ0FBQ3RDLFNBQU4sQ0FBZ0I5QyxLQUFoQixDQUFzQkcsSUFBdEIsQ0FBMkIyRCxTQUEzQixDQUFUOztBQUErQyxRQUFHLE9BQU9wQixRQUFQLEtBQWtCLFFBQWxCLElBQTRCOG9DLGFBQWEsQ0FBQzcrQixJQUFkLENBQW1CakssUUFBbkIsQ0FBL0IsRUFBNEQ7QUFBQyxVQUFHO0FBQUNoRCxjQUFNLENBQUNILFFBQVAsQ0FBZ0Jvc0MsYUFBaEIsQ0FBOEJqcEMsUUFBOUI7QUFBeUMsT0FBN0MsQ0FBNkMsT0FBTWtwQyxJQUFOLEVBQVc7QUFBQ2xwQyxnQkFBUSxHQUFDQSxRQUFRLENBQUNnRCxPQUFULENBQWlCK2xDLGFBQWpCLEVBQStCLFVBQVMzd0IsQ0FBVCxFQUFXN0osSUFBWCxFQUFnQjQ2QixFQUFoQixFQUFtQmhsQyxLQUFuQixFQUF5QjtBQUFDLGlCQUFNLE1BQUlvSyxJQUFKLEdBQVM0NkIsRUFBVCxHQUFZLElBQVosR0FBaUJobEMsS0FBakIsR0FBdUIsS0FBN0I7QUFBb0MsU0FBN0YsQ0FBVDs7QUFBd0csWUFBRztBQUFDbkgsZ0JBQU0sQ0FBQ0gsUUFBUCxDQUFnQm9zQyxhQUFoQixDQUE4QmpwQyxRQUE5QjtBQUF3Q29vQyxxQkFBVyxDQUFDLGlEQUErQ3gzQixJQUFJLENBQUMsQ0FBRCxDQUFwRCxDQUFYO0FBQW9FQSxjQUFJLENBQUMsQ0FBRCxDQUFKLEdBQVE1USxRQUFSO0FBQWtCLFNBQWxJLENBQWtJLE9BQU1vcEMsSUFBTixFQUFXO0FBQUNoQixxQkFBVyxDQUFDLGdEQUE4Q3gzQixJQUFJLENBQUMsQ0FBRCxDQUFuRCxDQUFYO0FBQW9FO0FBQUM7QUFBQzs7QUFDcmtCLFdBQU9pNEIsT0FBTyxDQUFDbHJDLEtBQVIsQ0FBYyxJQUFkLEVBQW1CaVQsSUFBbkIsQ0FBUDtBQUFpQyxHQURvQzs7QUFDbkMsT0FBSSszQixRQUFKLElBQWdCRSxPQUFoQixFQUF3QjtBQUFDLFFBQUd6ckMsTUFBTSxDQUFDZ0QsU0FBUCxDQUFpQm5DLGNBQWpCLENBQWdDUixJQUFoQyxDQUFxQ29yQyxPQUFyQyxFQUE2Q0YsUUFBN0MsQ0FBSCxFQUEwRDtBQUFDNW9DLFlBQU0sQ0FBQ2lOLElBQVAsQ0FBWTI3QixRQUFaLElBQXNCRSxPQUFPLENBQUNGLFFBQUQsQ0FBN0I7QUFBeUM7QUFBQzs7QUFDaEtILGlCQUFlLENBQUN6b0MsTUFBTSxDQUFDRyxFQUFSLEVBQVcsTUFBWCxFQUFrQixZQUFVO0FBQUMsV0FBTyxLQUFLSyxNQUFaO0FBQW9CLEdBQWpELEVBQWtELHNFQUFsRCxDQUFmO0FBQXlJaW9DLGlCQUFlLENBQUN6b0MsTUFBRCxFQUFRLFdBQVIsRUFBb0IsWUFBVTtBQUFDLFdBQU9rZixJQUFJLENBQUNDLEtBQUwsQ0FBV3ZoQixLQUFYLENBQWlCLElBQWpCLEVBQXNCeUQsU0FBdEIsQ0FBUDtBQUF5QyxHQUF4RSxFQUF5RSxnREFBekUsQ0FBZjtBQUEwSW9uQyxpQkFBZSxDQUFDem9DLE1BQUQsRUFBUSxXQUFSLEVBQW9CQSxNQUFNLENBQUMybUMsU0FBM0IsRUFBcUMsZ0NBQXJDLENBQWY7QUFBc0Y4QixpQkFBZSxDQUFDem9DLE1BQUQsRUFBUSxRQUFSLEVBQWlCQSxNQUFNLENBQUMyTyxVQUF4QixFQUFtQyxvREFBbkMsQ0FBZjtBQUF3RzQ1QixpQkFBZSxDQUFDdm9DLE1BQU0sQ0FBQ3NPLElBQVIsRUFBYSxTQUFiLEVBQXVCdE8sTUFBTSxDQUFDc08sSUFBUCxDQUFZeEgsT0FBbkMsRUFBMkMsNERBQTNDLENBQWY7QUFBd0h5aEMsaUJBQWUsQ0FBQ3ZvQyxNQUFNLENBQUNzTyxJQUFSLEVBQWEsR0FBYixFQUFpQnRPLE1BQU0sQ0FBQ3NPLElBQVAsQ0FBWXhILE9BQTdCLEVBQXFDLHlEQUFyQyxDQUFmOztBQUErRyxNQUFHK2dDLGtCQUFrQixDQUFDLE9BQUQsQ0FBckIsRUFBK0I7QUFBQ1ksbUJBQWUsQ0FBQ3pvQyxNQUFELEVBQVEsTUFBUixFQUFlLFVBQVNULElBQVQsRUFBYztBQUFDLGFBQU9BLElBQUksSUFBRSxJQUFOLEdBQVcsRUFBWCxHQUFjLENBQUNBLElBQUksR0FBQyxFQUFOLEVBQVUwRCxPQUFWLENBQWtCZ0UsS0FBbEIsRUFBd0IsRUFBeEIsQ0FBckI7QUFBa0QsS0FBaEYsRUFBaUYsc0RBQWpGLENBQWY7QUFBeUo7O0FBQ2ozQixNQUFHNGdDLGtCQUFrQixDQUFDLE9BQUQsQ0FBckIsRUFBK0I7QUFBQ1ksbUJBQWUsQ0FBQ3pvQyxNQUFELEVBQVEsVUFBUixFQUFtQixVQUFTb0IsSUFBVCxFQUFjZ0IsSUFBZCxFQUFtQjtBQUFDLGFBQU9oQixJQUFJLENBQUMwSCxRQUFMLElBQWUxSCxJQUFJLENBQUMwSCxRQUFMLENBQWNwRSxXQUFkLE9BQThCdEMsSUFBSSxDQUFDc0MsV0FBTCxFQUFwRDtBQUF3RSxLQUEvRyxFQUFnSCwrQkFBaEgsQ0FBZjtBQUFnSytqQyxtQkFBZSxDQUFDem9DLE1BQUQsRUFBUSxTQUFSLEVBQWtCMkMsS0FBSyxDQUFDQyxPQUF4QixFQUFnQyxpREFBaEMsQ0FBZjtBQUFtRzs7QUFDblMsTUFBR2lsQyxrQkFBa0IsQ0FBQyxPQUFELENBQXJCLEVBQStCO0FBQUNZLG1CQUFlLENBQUN6b0MsTUFBRCxFQUFRLFdBQVIsRUFBb0IsVUFBU3pCLEdBQVQsRUFBYTtBQUFDLFVBQUlJLElBQUksV0FBUUosR0FBUixDQUFSOztBQUFvQixhQUFNLENBQUNJLElBQUksS0FBRyxRQUFQLElBQWlCQSxJQUFJLEtBQUcsUUFBekIsS0FBb0MsQ0FBQ29vQyxLQUFLLENBQUN4b0MsR0FBRyxHQUFDdXdCLFVBQVUsQ0FBQ3Z3QixHQUFELENBQWYsQ0FBaEQ7QUFBdUUsS0FBN0gsRUFBOEgsa0NBQTlILENBQWY7QUFBaUx5QixVQUFNLENBQUNpQixJQUFQLENBQVksdUVBQXVFdUQsS0FBdkUsQ0FBNkUsR0FBN0UsQ0FBWixFQUE4RixVQUFTNlQsQ0FBVCxFQUFXalcsSUFBWCxFQUFnQjtBQUFDckUsZ0JBQVUsQ0FBQyxhQUFXcUUsSUFBWCxHQUFnQixHQUFqQixDQUFWLEdBQWdDQSxJQUFJLENBQUNzQyxXQUFMLEVBQWhDO0FBQW9ELEtBQW5LO0FBQXFLK2pDLG1CQUFlLENBQUN6b0MsTUFBRCxFQUFRLE1BQVIsRUFBZSxVQUFTekIsR0FBVCxFQUFhO0FBQUMsVUFBR0EsR0FBRyxJQUFFLElBQVIsRUFBYTtBQUFDLGVBQU9BLEdBQUcsR0FBQyxFQUFYO0FBQWU7O0FBQy9iLGFBQU8sUUFBT0EsR0FBUCxNQUFhLFFBQWIsSUFBdUIsT0FBT0EsR0FBUCxLQUFhLFVBQXBDLEdBQStDUixVQUFVLENBQUNWLE1BQU0sQ0FBQ2dELFNBQVAsQ0FBaUJyQyxRQUFqQixDQUEwQk4sSUFBMUIsQ0FBK0JhLEdBQS9CLENBQUQsQ0FBVixJQUFpRCxRQUFoRyxXQUFnSEEsR0FBaEgsQ0FBUDtBQUE0SCxLQUR5USxFQUN4USwyQkFEd1EsQ0FBZjtBQUM1TmtxQyxtQkFBZSxDQUFDem9DLE1BQUQsRUFBUSxZQUFSLEVBQXFCLFVBQVN6QixHQUFULEVBQWE7QUFBQyxhQUFPLE9BQU9BLEdBQVAsS0FBYSxVQUFwQjtBQUFnQyxLQUFuRSxFQUFvRSxtQ0FBcEUsQ0FBZjtBQUF3SGtxQyxtQkFBZSxDQUFDem9DLE1BQUQsRUFBUSxVQUFSLEVBQW1CLFVBQVN6QixHQUFULEVBQWE7QUFBQyxhQUFPQSxHQUFHLElBQUUsSUFBTCxJQUFXQSxHQUFHLEtBQUdBLEdBQUcsQ0FBQ3RCLE1BQTVCO0FBQW9DLEtBQXJFLEVBQXNFLGlDQUF0RSxDQUFmO0FBQXlIOztBQUMzWSxNQUFHK0MsTUFBTSxDQUFDZ2dDLElBQVYsRUFBZTtBQUFDLFFBQUlzSixPQUFPLEdBQUN0cEMsTUFBTSxDQUFDZ2dDLElBQW5CO0FBQUEsUUFBd0JnRSxNQUFNLEdBQUMsbUJBQS9COztBQUFtRGhrQyxVQUFNLENBQUNnZ0MsSUFBUCxHQUFZLFlBQVU7QUFBQyxVQUFJdUosS0FBSyxHQUFDRCxPQUFPLENBQUMxckMsS0FBUixDQUFjLElBQWQsRUFBbUJ5RCxTQUFuQixDQUFWOztBQUF3QyxVQUFHa29DLEtBQUssQ0FBQ3h2QixPQUFULEVBQWlCO0FBQUMwdUIsdUJBQWUsQ0FBQ2MsS0FBRCxFQUFPLFNBQVAsRUFBaUJBLEtBQUssQ0FBQ3pqQyxJQUF2QixFQUE0Qix5Q0FBNUIsQ0FBZjtBQUFzRjJpQyx1QkFBZSxDQUFDYyxLQUFELEVBQU8sT0FBUCxFQUFlQSxLQUFLLENBQUN2dkIsSUFBckIsRUFBMEIsdUNBQTFCLENBQWY7QUFBa0Z5dUIsdUJBQWUsQ0FBQ2MsS0FBRCxFQUFPLFVBQVAsRUFBa0JBLEtBQUssQ0FBQ2p2QixNQUF4QixFQUErQiwwQ0FBL0IsQ0FBZjtBQUEyRjs7QUFDdlosYUFBT2l2QixLQUFQO0FBQWMsS0FEcUQ7O0FBQ3BELFFBQUcsQ0FBQzFCLGtCQUFrQixDQUFDLE9BQUQsQ0FBdEIsRUFBZ0M7QUFBQzduQyxZQUFNLENBQUM4L0IsYUFBUCxDQUFxQixPQUFyQixFQUE2QixVQUFTM0QsQ0FBVCxFQUFXO0FBQUMsWUFBR0EsQ0FBQyxDQUFDOEgsS0FBRixLQUFVLEtBQVYsS0FBa0JELE1BQU0sQ0FBQzk1QixJQUFQLENBQVlpeUIsQ0FBQyxDQUFDaUQsR0FBZCxLQUFvQixPQUFPakQsQ0FBQyxDQUFDemQsSUFBVCxLQUFnQixRQUFoQixJQUEwQixDQUFDeWQsQ0FBQyxDQUFDc0QsV0FBRixJQUFlLEVBQWhCLEVBQW9CM2hDLE9BQXBCLENBQTRCLG1DQUE1QixNQUFtRSxDQUE3RixJQUFnR2ttQyxNQUFNLENBQUM5NUIsSUFBUCxDQUFZaXlCLENBQUMsQ0FBQ3pkLElBQWQsQ0FBdEksQ0FBSCxFQUE4SjtBQUFDMnBCLHFCQUFXLENBQUMsNENBQUQsQ0FBWDtBQUEyRDtBQUFDLE9BQXBRO0FBQXVRO0FBQUM7O0FBQ3hULE1BQUltQixhQUFhLEdBQUN4cEMsTUFBTSxDQUFDRyxFQUFQLENBQVU0NEIsVUFBNUI7QUFBQSxNQUF1QzBRLGNBQWMsR0FBQ3pwQyxNQUFNLENBQUNHLEVBQVAsQ0FBVWk2QixXQUFoRTtBQUFBLE1BQTRFc1AsY0FBYyxHQUFDLE1BQTNGOztBQUFrRzFwQyxRQUFNLENBQUNHLEVBQVAsQ0FBVTQ0QixVQUFWLEdBQXFCLFVBQVMzMkIsSUFBVCxFQUFjO0FBQUMsUUFBSW1VLElBQUksR0FBQyxJQUFUO0FBQWN2VyxVQUFNLENBQUNpQixJQUFQLENBQVltQixJQUFJLENBQUNtSCxLQUFMLENBQVdtZ0MsY0FBWCxDQUFaLEVBQXVDLFVBQVNqbEMsRUFBVCxFQUFZK0osSUFBWixFQUFpQjtBQUFDLFVBQUd4TyxNQUFNLENBQUNzTyxJQUFQLENBQVkvRSxLQUFaLENBQWtCMnZCLElBQWxCLENBQXVCaHZCLElBQXZCLENBQTRCc0UsSUFBNUIsQ0FBSCxFQUFxQztBQUFDNjVCLG1CQUFXLENBQUMsNkRBQTJENzVCLElBQTVELENBQVg7QUFBNkUrSCxZQUFJLENBQUNvSSxJQUFMLENBQVVuUSxJQUFWLEVBQWUsS0FBZjtBQUF1QjtBQUFDLEtBQXBNO0FBQXNNLFdBQU9nN0IsYUFBYSxDQUFDNXJDLEtBQWQsQ0FBb0IsSUFBcEIsRUFBeUJ5RCxTQUF6QixDQUFQO0FBQTRDLEdBQXBTOztBQUFxU3JCLFFBQU0sQ0FBQ0csRUFBUCxDQUFVaTZCLFdBQVYsR0FBc0IsVUFBUy9mLEtBQVQsRUFBZTtBQUFDLFFBQUdBLEtBQUssS0FBR3hYLFNBQVIsSUFBbUIsT0FBT3dYLEtBQVAsS0FBZSxTQUFyQyxFQUErQztBQUFDLGFBQU9vdkIsY0FBYyxDQUFDN3JDLEtBQWYsQ0FBcUIsSUFBckIsRUFBMEJ5RCxTQUExQixDQUFQO0FBQTZDOztBQUMxZ0JnbkMsZUFBVyxDQUFDLGdEQUFELENBQVg7QUFBOEQsV0FBTyxLQUFLcG5DLElBQUwsQ0FBVSxZQUFVO0FBQUMsVUFBSTBMLFNBQVMsR0FBQyxLQUFLbk4sWUFBTCxJQUFtQixLQUFLQSxZQUFMLENBQWtCLE9BQWxCLENBQW5CLElBQStDLEVBQTdEOztBQUFnRSxVQUFHbU4sU0FBSCxFQUFhO0FBQUMzTSxjQUFNLENBQUMwZSxJQUFQLENBQVksSUFBWixFQUFpQixlQUFqQixFQUFpQy9SLFNBQWpDO0FBQTZDOztBQUNyTixVQUFHLEtBQUtsTixZQUFSLEVBQXFCO0FBQUMsYUFBS0EsWUFBTCxDQUFrQixPQUFsQixFQUEwQmtOLFNBQVMsSUFBRTBOLEtBQUssS0FBRyxLQUFuQixHQUF5QixFQUF6QixHQUE0QnJhLE1BQU0sQ0FBQzBlLElBQVAsQ0FBWSxJQUFaLEVBQWlCLGVBQWpCLEtBQW1DLEVBQXpGO0FBQThGO0FBQUMsS0FEaEQsQ0FBUDtBQUMwRCxHQUYrUTs7QUFFOVEsV0FBU1QsU0FBVCxDQUFtQkMsTUFBbkIsRUFBMEI7QUFBQyxXQUFPQSxNQUFNLENBQUNqYixPQUFQLENBQWUsV0FBZixFQUEyQixVQUFTb1YsQ0FBVCxFQUFXMEYsTUFBWCxFQUFrQjtBQUFDLGFBQU9BLE1BQU0sQ0FBQ0MsV0FBUCxFQUFQO0FBQTZCLEtBQTNFLENBQVA7QUFBcUY7O0FBQ3pPLE1BQUkyckIsUUFBSjtBQUFBLE1BQWFDLGdCQUFnQixHQUFDLEtBQTlCO0FBQUEsTUFBb0NDLFdBQVcsR0FBQyxRQUFoRDtBQUFBLE1BQXlEQyxPQUFPLEdBQUMsNkhBQWpFOztBQUErTCxNQUFHOXBDLE1BQU0sQ0FBQzB0QixJQUFWLEVBQWU7QUFBQzF0QixVQUFNLENBQUNpQixJQUFQLENBQVksQ0FBQyxRQUFELEVBQVUsT0FBVixFQUFrQixxQkFBbEIsQ0FBWixFQUFxRCxVQUFTb1gsQ0FBVCxFQUFXalcsSUFBWCxFQUFnQjtBQUFDLFVBQUkybkMsT0FBTyxHQUFDL3BDLE1BQU0sQ0FBQ3F5QixRQUFQLENBQWdCandCLElBQWhCLEtBQXVCcEMsTUFBTSxDQUFDcXlCLFFBQVAsQ0FBZ0Jqd0IsSUFBaEIsRUFBc0IxQixHQUF6RDs7QUFBNkQsVUFBR3FwQyxPQUFILEVBQVc7QUFBQy9wQyxjQUFNLENBQUNxeUIsUUFBUCxDQUFnQmp3QixJQUFoQixFQUFzQjFCLEdBQXRCLEdBQTBCLFlBQVU7QUFBQyxjQUFJSSxHQUFKO0FBQVE4b0MsMEJBQWdCLEdBQUMsSUFBakI7QUFBc0I5b0MsYUFBRyxHQUFDaXBDLE9BQU8sQ0FBQ25zQyxLQUFSLENBQWMsSUFBZCxFQUFtQnlELFNBQW5CLENBQUo7QUFBa0N1b0MsMEJBQWdCLEdBQUMsS0FBakI7QUFBdUIsaUJBQU85b0MsR0FBUDtBQUFZLFNBQXhJO0FBQTBJO0FBQUMsS0FBMVI7QUFBNlI7O0FBQzVlZCxRQUFNLENBQUMwdEIsSUFBUCxHQUFZLFVBQVN0c0IsSUFBVCxFQUFjZSxPQUFkLEVBQXNCakIsUUFBdEIsRUFBK0IyUCxJQUEvQixFQUFvQztBQUFDLFFBQUkvUCxHQUFKO0FBQUEsUUFBUXNCLElBQVI7QUFBQSxRQUFhdXJCLEdBQUcsR0FBQyxFQUFqQjs7QUFBb0IsUUFBRyxDQUFDaWMsZ0JBQUosRUFBcUI7QUFBQ3ZCLGlCQUFXLENBQUMsOENBQUQsQ0FBWDtBQUE2RDs7QUFDeEosU0FBSWptQyxJQUFKLElBQVlELE9BQVosRUFBb0I7QUFBQ3dyQixTQUFHLENBQUN2ckIsSUFBRCxDQUFILEdBQVVoQixJQUFJLENBQUNxZixLQUFMLENBQVdyZSxJQUFYLENBQVY7QUFBMkJoQixVQUFJLENBQUNxZixLQUFMLENBQVdyZSxJQUFYLElBQWlCRCxPQUFPLENBQUNDLElBQUQsQ0FBeEI7QUFBZ0M7O0FBQ2hGdEIsT0FBRyxHQUFDSSxRQUFRLENBQUN0RCxLQUFULENBQWV3RCxJQUFmLEVBQW9CeVAsSUFBSSxJQUFFLEVBQTFCLENBQUo7O0FBQWtDLFNBQUl6TyxJQUFKLElBQVlELE9BQVosRUFBb0I7QUFBQ2YsVUFBSSxDQUFDcWYsS0FBTCxDQUFXcmUsSUFBWCxJQUFpQnVyQixHQUFHLENBQUN2ckIsSUFBRCxDQUFwQjtBQUE0Qjs7QUFDbkYsV0FBT3RCLEdBQVA7QUFBWSxHQUhaOztBQUdhLE1BQUcrbUMsa0JBQWtCLENBQUMsT0FBRCxDQUFsQixJQUE2QixPQUFPbUMsS0FBUCxLQUFlLFdBQS9DLEVBQTJEO0FBQUNocUMsVUFBTSxDQUFDNHdCLFFBQVAsR0FBZ0IsSUFBSW9aLEtBQUosQ0FBVWhxQyxNQUFNLENBQUM0d0IsUUFBUCxJQUFpQixFQUEzQixFQUE4QjtBQUFDblMsU0FBRyxFQUFDLGVBQVU7QUFBQzRwQixtQkFBVyxDQUFDLDBDQUFELENBQVg7QUFBd0QsZUFBTzRCLE9BQU8sQ0FBQ3hyQixHQUFSLENBQVk3Z0IsS0FBWixDQUFrQixJQUFsQixFQUF1QnlELFNBQXZCLENBQVA7QUFBMEM7QUFBbEgsS0FBOUIsQ0FBaEI7QUFBb0s7O0FBQzdPLE1BQUcsQ0FBQ3JCLE1BQU0sQ0FBQ3FoQixTQUFYLEVBQXFCO0FBQUNyaEIsVUFBTSxDQUFDcWhCLFNBQVAsR0FBaUIsRUFBakI7QUFBcUI7O0FBQzNDLFdBQVM2b0IsUUFBVCxDQUFrQnZyQixJQUFsQixFQUF1QjtBQUFDLFdBQU9rckIsV0FBVyxDQUFDMy9CLElBQVosQ0FBaUJ5VSxJQUFqQixLQUF3Qm1yQixPQUFPLENBQUM1L0IsSUFBUixDQUFheVUsSUFBSSxDQUFDLENBQUQsQ0FBSixDQUFRWCxXQUFSLEtBQXNCVyxJQUFJLENBQUNwaEIsS0FBTCxDQUFXLENBQVgsQ0FBbkMsQ0FBL0I7QUFBa0Y7O0FBQzFHb3NDLFVBQVEsR0FBQzNwQyxNQUFNLENBQUNHLEVBQVAsQ0FBVXdnQixHQUFuQjs7QUFBdUIzZ0IsUUFBTSxDQUFDRyxFQUFQLENBQVV3Z0IsR0FBVixHQUFjLFVBQVN2ZSxJQUFULEVBQWNnQyxLQUFkLEVBQW9CO0FBQUMsUUFBSStsQyxTQUFKO0FBQUEsUUFBY0MsUUFBUSxHQUFDLElBQXZCOztBQUE0QixRQUFHaG9DLElBQUksSUFBRSxRQUFPQSxJQUFQLE1BQWMsUUFBcEIsSUFBOEIsQ0FBQ08sS0FBSyxDQUFDQyxPQUFOLENBQWNSLElBQWQsQ0FBbEMsRUFBc0Q7QUFBQ3BDLFlBQU0sQ0FBQ2lCLElBQVAsQ0FBWW1CLElBQVosRUFBaUIsVUFBUzRULENBQVQsRUFBV3VELENBQVgsRUFBYTtBQUFDdlosY0FBTSxDQUFDRyxFQUFQLENBQVV3Z0IsR0FBVixDQUFjampCLElBQWQsQ0FBbUIwc0MsUUFBbkIsRUFBNEJwMEIsQ0FBNUIsRUFBOEJ1RCxDQUE5QjtBQUFrQyxPQUFqRTtBQUFtRSxhQUFPLElBQVA7QUFBYTs7QUFDN04sUUFBRyxPQUFPblYsS0FBUCxLQUFlLFFBQWxCLEVBQTJCO0FBQUMrbEMsZUFBUyxHQUFDbHNCLFNBQVMsQ0FBQzdiLElBQUQsQ0FBbkI7O0FBQTBCLFVBQUcsQ0FBQzhuQyxRQUFRLENBQUNDLFNBQUQsQ0FBVCxJQUFzQixDQUFDbnFDLE1BQU0sQ0FBQ3FoQixTQUFQLENBQWlCOG9CLFNBQWpCLENBQTFCLEVBQXNEO0FBQUM5QixtQkFBVyxDQUFDLDZEQUN6SGptQyxJQUR5SCxHQUNwSCxhQURtSCxDQUFYO0FBQ3hGO0FBQUM7O0FBQ3RCLFdBQU91bkMsUUFBUSxDQUFDL3JDLEtBQVQsQ0FBZSxJQUFmLEVBQW9CeUQsU0FBcEIsQ0FBUDtBQUF1QyxHQUhoQjs7QUFHaUIsTUFBSWdwQyxPQUFPLEdBQUNycUMsTUFBTSxDQUFDMGUsSUFBbkI7O0FBQXdCMWUsUUFBTSxDQUFDMGUsSUFBUCxHQUFZLFVBQVN0ZCxJQUFULEVBQWNnQixJQUFkLEVBQW1CZ0MsS0FBbkIsRUFBeUI7QUFBQyxRQUFJa21DLE9BQUosRUFBWUMsUUFBWixFQUFxQjMvQixHQUFyQjs7QUFBeUIsUUFBR3hJLElBQUksSUFBRSxRQUFPQSxJQUFQLE1BQWMsUUFBcEIsSUFBOEJmLFNBQVMsQ0FBQ2IsTUFBVixLQUFtQixDQUFwRCxFQUFzRDtBQUFDOHBDLGFBQU8sR0FBQ3RxQyxNQUFNLENBQUM0ZSxPQUFQLENBQWV4ZCxJQUFmLEtBQXNCaXBDLE9BQU8sQ0FBQzNzQyxJQUFSLENBQWEsSUFBYixFQUFrQjBELElBQWxCLENBQTlCO0FBQXNEbXBDLGNBQVEsR0FBQyxFQUFUOztBQUFZLFdBQUkzL0IsR0FBSixJQUFXeEksSUFBWCxFQUFnQjtBQUFDLFlBQUd3SSxHQUFHLEtBQUdxVCxTQUFTLENBQUNyVCxHQUFELENBQWxCLEVBQXdCO0FBQUN5OUIscUJBQVcsQ0FBQyxzREFBb0R6OUIsR0FBckQsQ0FBWDtBQUFxRTAvQixpQkFBTyxDQUFDMS9CLEdBQUQsQ0FBUCxHQUFheEksSUFBSSxDQUFDd0ksR0FBRCxDQUFqQjtBQUF3QixTQUF0SCxNQUEwSDtBQUFDMi9CLGtCQUFRLENBQUMzL0IsR0FBRCxDQUFSLEdBQWN4SSxJQUFJLENBQUN3SSxHQUFELENBQWxCO0FBQXlCO0FBQUM7O0FBQzlaeS9CLGFBQU8sQ0FBQzNzQyxJQUFSLENBQWEsSUFBYixFQUFrQjBELElBQWxCLEVBQXVCbXBDLFFBQXZCO0FBQWlDLGFBQU9ub0MsSUFBUDtBQUFhOztBQUM5QyxRQUFHQSxJQUFJLElBQUUsT0FBT0EsSUFBUCxLQUFjLFFBQXBCLElBQThCQSxJQUFJLEtBQUc2YixTQUFTLENBQUM3YixJQUFELENBQWpELEVBQXdEO0FBQUNrb0MsYUFBTyxHQUFDdHFDLE1BQU0sQ0FBQzRlLE9BQVAsQ0FBZXhkLElBQWYsS0FBc0JpcEMsT0FBTyxDQUFDM3NDLElBQVIsQ0FBYSxJQUFiLEVBQWtCMEQsSUFBbEIsQ0FBOUI7O0FBQXNELFVBQUdrcEMsT0FBTyxJQUFFbG9DLElBQUksSUFBSWtvQyxPQUFwQixFQUE0QjtBQUFDakMsbUJBQVcsQ0FBQyxzREFBb0RqbUMsSUFBckQsQ0FBWDs7QUFBc0UsWUFBR2YsU0FBUyxDQUFDYixNQUFWLEdBQWlCLENBQXBCLEVBQXNCO0FBQUM4cEMsaUJBQU8sQ0FBQ2xvQyxJQUFELENBQVAsR0FBY2dDLEtBQWQ7QUFBcUI7O0FBQzlQLGVBQU9rbUMsT0FBTyxDQUFDbG9DLElBQUQsQ0FBZDtBQUFzQjtBQUFDOztBQUN2QixXQUFPaW9DLE9BQU8sQ0FBQ3pzQyxLQUFSLENBQWMsSUFBZCxFQUFtQnlELFNBQW5CLENBQVA7QUFBc0MsR0FKMEI7O0FBSXpCLE1BQUdyQixNQUFNLENBQUMrekIsRUFBVixFQUFhO0FBQUMsUUFBSXlXLGFBQUo7QUFBQSxRQUFrQkMsV0FBbEI7QUFBQSxRQUE4QkMsV0FBVyxHQUFDMXFDLE1BQU0sQ0FBQ3N6QixLQUFQLENBQWFqekIsU0FBYixDQUF1Qm96QixHQUFqRTtBQUFBLFFBQXFFa1gsWUFBWSxHQUFDLFNBQWJBLFlBQWEsQ0FBU0MsR0FBVCxFQUFhO0FBQUMsYUFBT0EsR0FBUDtBQUFZLEtBQTVHOztBQUE2RzVxQyxVQUFNLENBQUNzekIsS0FBUCxDQUFhanpCLFNBQWIsQ0FBdUJvekIsR0FBdkIsR0FBMkIsWUFBVTtBQUFDLFVBQUd6ekIsTUFBTSxDQUFDdXpCLE1BQVAsQ0FBYyxLQUFLQSxNQUFuQixFQUEyQi95QixNQUEzQixHQUFrQyxDQUFyQyxFQUF1QztBQUFDNm5DLG1CQUFXLENBQUMsb0JBQWtCLEtBQUs5VSxNQUFMLENBQVl2MUIsUUFBWixFQUFsQixHQUF5QyxnQ0FBMUMsQ0FBWDtBQUF1RmdDLGNBQU0sQ0FBQ3V6QixNQUFQLENBQWMsS0FBS0EsTUFBbkIsSUFBMkJvWCxZQUEzQjtBQUF5Qzs7QUFDaFhELGlCQUFXLENBQUM5c0MsS0FBWixDQUFrQixJQUFsQixFQUF1QnlELFNBQXZCO0FBQW1DLEtBRCtIOztBQUM5SG1wQyxpQkFBYSxHQUFDeHFDLE1BQU0sQ0FBQyt6QixFQUFQLENBQVVlLFFBQVYsSUFBb0IsRUFBbEM7QUFBcUMyVixlQUFXLEdBQUMsa0NBQVo7O0FBQStDLFFBQUd4dEMsTUFBTSxDQUFDNDNCLHFCQUFWLEVBQWdDO0FBQUN4M0IsWUFBTSxDQUFDa2hCLGNBQVAsQ0FBc0J2ZSxNQUFNLENBQUMrekIsRUFBN0IsRUFBZ0MsVUFBaEMsRUFBMkM7QUFBQ3ZWLG9CQUFZLEVBQUMsSUFBZDtBQUFtQjhJLGtCQUFVLEVBQUMsSUFBOUI7QUFBbUM1bUIsV0FBRyxFQUFDLGVBQVU7QUFBQyxjQUFHLENBQUN6RCxNQUFNLENBQUNILFFBQVAsQ0FBZ0I4M0IsTUFBcEIsRUFBMkI7QUFBQ3lULHVCQUFXLENBQUNvQyxXQUFELENBQVg7QUFBMEI7O0FBQzVTLGlCQUFPRCxhQUFQO0FBQXNCLFNBRDhLO0FBQzdLL3JCLFdBQUcsRUFBQyxhQUFTK3BCLFFBQVQsRUFBa0I7QUFBQ0gscUJBQVcsQ0FBQ29DLFdBQUQsQ0FBWDtBQUF5QkQsdUJBQWEsR0FBQ2hDLFFBQWQ7QUFBd0I7QUFEcUcsT0FBM0M7QUFDdEQ7QUFBQzs7QUFDcEcsTUFBSXFDLE9BQU8sR0FBQzdxQyxNQUFNLENBQUNHLEVBQVAsQ0FBVXNuQixJQUF0QjtBQUFBLE1BQTJCcWpCLFdBQVcsR0FBQzlxQyxNQUFNLENBQUM2a0IsS0FBUCxDQUFhdE4sR0FBcEQ7QUFBQSxNQUF3RHd6QixXQUFXLEdBQUMvcUMsTUFBTSxDQUFDNmtCLEtBQVAsQ0FBYXlCLEdBQWpGO0FBQXFGdG1CLFFBQU0sQ0FBQzZrQixLQUFQLENBQWF1RCxLQUFiLEdBQW1CLEVBQW5CO0FBQXNCcG9CLFFBQU0sQ0FBQzZrQixLQUFQLENBQWFtbUIsUUFBYixHQUFzQixFQUF0QjtBQUF5QnpDLGlCQUFlLENBQUN2b0MsTUFBTSxDQUFDNmtCLEtBQVAsQ0FBYXVELEtBQWQsRUFBb0IsUUFBcEIsRUFBNkJwb0IsTUFBTSxDQUFDNmtCLEtBQVAsQ0FBYXVELEtBQWIsQ0FBbUJ6cUIsTUFBaEQsRUFBdUQsdURBQXZELENBQWY7O0FBQStIcUMsUUFBTSxDQUFDNmtCLEtBQVAsQ0FBYXlCLEdBQWIsR0FBaUIsVUFBU2lCLGFBQVQsRUFBdUI7QUFBQyxRQUFJMUMsS0FBSjtBQUFBLFFBQVVsbUIsSUFBSSxHQUFDNG9CLGFBQWEsQ0FBQzVvQixJQUE3QjtBQUFBLFFBQWtDc3NDLE9BQU8sR0FBQyxLQUFLRCxRQUFMLENBQWNyc0MsSUFBZCxDQUExQztBQUFBLFFBQThEeXBCLEtBQUssR0FBQ3BvQixNQUFNLENBQUM2a0IsS0FBUCxDQUFhdUQsS0FBakY7O0FBQXVGLFFBQUdBLEtBQUssQ0FBQzVuQixNQUFULEVBQWdCO0FBQUM2bkMsaUJBQVcsQ0FBQyxvREFBa0RqZ0IsS0FBSyxDQUFDOWQsSUFBTixFQUFuRCxDQUFYOztBQUE0RSxhQUFNOGQsS0FBSyxDQUFDNW5CLE1BQVosRUFBbUI7QUFBQ1IsY0FBTSxDQUFDNmtCLEtBQVAsQ0FBYXNDLE9BQWIsQ0FBcUJpQixLQUFLLENBQUM3aEIsR0FBTixFQUFyQjtBQUFtQztBQUFDOztBQUN4aEIsUUFBRzBrQyxPQUFPLElBQUUsQ0FBQ0EsT0FBTyxDQUFDQyxVQUFyQixFQUFnQztBQUFDRCxhQUFPLENBQUNDLFVBQVIsR0FBbUIsSUFBbkI7QUFBd0I3QyxpQkFBVyxDQUFDLHVEQUFxRDFwQyxJQUF0RCxDQUFYOztBQUF1RSxVQUFHLENBQUN5cEIsS0FBSyxHQUFDNmlCLE9BQU8sQ0FBQzdpQixLQUFmLEtBQXVCQSxLQUFLLENBQUM1bkIsTUFBaEMsRUFBdUM7QUFBQyxlQUFNNG5CLEtBQUssQ0FBQzVuQixNQUFaLEVBQW1CO0FBQUNSLGdCQUFNLENBQUM2a0IsS0FBUCxDQUFhc0MsT0FBYixDQUFxQmlCLEtBQUssQ0FBQzdoQixHQUFOLEVBQXJCO0FBQW1DO0FBQUM7QUFBQzs7QUFDak9zZSxTQUFLLEdBQUNrbUIsV0FBVyxDQUFDcnRDLElBQVosQ0FBaUIsSUFBakIsRUFBc0I2cEIsYUFBdEIsQ0FBTjtBQUEyQyxXQUFPMGpCLE9BQU8sSUFBRUEsT0FBTyxDQUFDbCtCLE1BQWpCLEdBQXdCaytCLE9BQU8sQ0FBQ2wrQixNQUFSLENBQWU4WCxLQUFmLEVBQXFCMEMsYUFBckIsQ0FBeEIsR0FBNEQxQyxLQUFuRTtBQUEwRSxHQUY4STs7QUFFN0k3a0IsUUFBTSxDQUFDNmtCLEtBQVAsQ0FBYXROLEdBQWIsR0FBaUIsVUFBU25XLElBQVQsRUFBY3NqQixLQUFkLEVBQW9CO0FBQUMsUUFBR3RqQixJQUFJLEtBQUduRSxNQUFQLElBQWV5bkIsS0FBSyxLQUFHLE1BQXZCLElBQStCem5CLE1BQU0sQ0FBQ0gsUUFBUCxDQUFnQnFnQixVQUFoQixLQUE2QixVQUEvRCxFQUEwRTtBQUFDa3JCLGlCQUFXLENBQUMsK0RBQUQsQ0FBWDtBQUE4RTs7QUFDclQsV0FBT3lDLFdBQVcsQ0FBQ2x0QyxLQUFaLENBQWtCLElBQWxCLEVBQXVCeUQsU0FBdkIsQ0FBUDtBQUEwQyxHQUQ0RTs7QUFDM0VyQixRQUFNLENBQUNpQixJQUFQLENBQVksQ0FBQyxNQUFELEVBQVEsUUFBUixFQUFpQixPQUFqQixDQUFaLEVBQXNDLFVBQVNvWCxDQUFULEVBQVdqVyxJQUFYLEVBQWdCO0FBQUNwQyxVQUFNLENBQUNHLEVBQVAsQ0FBVWlDLElBQVYsSUFBZ0IsWUFBVTtBQUFDLFVBQUl5TyxJQUFJLEdBQUNsTyxLQUFLLENBQUN0QyxTQUFOLENBQWdCOUMsS0FBaEIsQ0FBc0JHLElBQXRCLENBQTJCMkQsU0FBM0IsRUFBcUMsQ0FBckMsQ0FBVDs7QUFBaUQsVUFBR2UsSUFBSSxLQUFHLE1BQVAsSUFBZSxPQUFPeU8sSUFBSSxDQUFDLENBQUQsQ0FBWCxLQUFpQixRQUFuQyxFQUE0QztBQUFDLGVBQU9nNkIsT0FBTyxDQUFDanRDLEtBQVIsQ0FBYyxJQUFkLEVBQW1CaVQsSUFBbkIsQ0FBUDtBQUFpQzs7QUFDNVB3M0IsaUJBQVcsQ0FBQyxlQUFham1DLElBQWIsR0FBa0Isa0JBQW5CLENBQVg7QUFBa0R5TyxVQUFJLENBQUM1TyxNQUFMLENBQVksQ0FBWixFQUFjLENBQWQsRUFBZ0JHLElBQWhCOztBQUFzQixVQUFHZixTQUFTLENBQUNiLE1BQWIsRUFBb0I7QUFBQyxlQUFPLEtBQUtpa0IsRUFBTCxDQUFRN21CLEtBQVIsQ0FBYyxJQUFkLEVBQW1CaVQsSUFBbkIsQ0FBUDtBQUFpQzs7QUFDOUgsV0FBS3lxQixjQUFMLENBQW9CMTlCLEtBQXBCLENBQTBCLElBQTFCLEVBQStCaVQsSUFBL0I7QUFBcUMsYUFBTyxJQUFQO0FBQWEsS0FGZ0Q7QUFFOUMsR0FGVDtBQUVXN1EsUUFBTSxDQUFDaUIsSUFBUCxDQUFZLENBQUMsOERBQTRELHVFQUE1RCxHQUFvSSx5REFBckksRUFBZ011RCxLQUFoTSxDQUFzTSxHQUF0TSxDQUFaLEVBQXVOLFVBQVNDLEVBQVQsRUFBWXJDLElBQVosRUFBaUI7QUFBQ3BDLFVBQU0sQ0FBQ0csRUFBUCxDQUFVaUMsSUFBVixJQUFnQixVQUFTc2MsSUFBVCxFQUFjdmUsRUFBZCxFQUFpQjtBQUFDa29DLGlCQUFXLENBQUMsZUFBYWptQyxJQUFiLEdBQWtCLGtDQUFuQixDQUFYO0FBQWtFLGFBQU9mLFNBQVMsQ0FBQ2IsTUFBVixHQUFpQixDQUFqQixHQUFtQixLQUFLaWtCLEVBQUwsQ0FBUXJpQixJQUFSLEVBQWEsSUFBYixFQUFrQnNjLElBQWxCLEVBQXVCdmUsRUFBdkIsQ0FBbkIsR0FBOEMsS0FBSzBuQixPQUFMLENBQWF6bEIsSUFBYixDQUFyRDtBQUF5RSxLQUE3SztBQUErSyxHQUF4WjtBQUEwWnBDLFFBQU0sQ0FBQyxZQUFVO0FBQUNBLFVBQU0sQ0FBQy9DLE1BQU0sQ0FBQ0gsUUFBUixDQUFOLENBQXdCdytCLGNBQXhCLENBQXVDLE9BQXZDO0FBQWlELEdBQTdELENBQU47QUFBcUV0N0IsUUFBTSxDQUFDNmtCLEtBQVAsQ0FBYXpKLE9BQWIsQ0FBcUJ6RSxLQUFyQixHQUEyQjtBQUFDb1AsU0FBSyxFQUFDLGlCQUFVO0FBQUMsVUFBRyxTQUFPOW9CLE1BQU0sQ0FBQ0gsUUFBakIsRUFBMEI7QUFBQ3VyQyxtQkFBVyxDQUFDLDZCQUFELENBQVg7QUFBNEM7QUFBQztBQUExRixHQUEzQjtBQUF1SHJvQyxRQUFNLENBQUNHLEVBQVAsQ0FBVStCLE1BQVYsQ0FBaUI7QUFBQzQwQixRQUFJLEVBQUMsY0FBU3BTLEtBQVQsRUFBZWhHLElBQWYsRUFBb0J2ZSxFQUFwQixFQUF1QjtBQUFDa29DLGlCQUFXLENBQUMsZ0NBQUQsQ0FBWDtBQUE4QyxhQUFPLEtBQUs1akIsRUFBTCxDQUFRQyxLQUFSLEVBQWMsSUFBZCxFQUFtQmhHLElBQW5CLEVBQXdCdmUsRUFBeEIsQ0FBUDtBQUFvQyxLQUFoSDtBQUFpSGltQyxVQUFNLEVBQUMsZ0JBQVMxaEIsS0FBVCxFQUFldmtCLEVBQWYsRUFBa0I7QUFBQ2tvQyxpQkFBVyxDQUFDLGtDQUFELENBQVg7QUFBZ0QsYUFBTyxLQUFLdmpCLEdBQUwsQ0FBU0osS0FBVCxFQUFlLElBQWYsRUFBb0J2a0IsRUFBcEIsQ0FBUDtBQUFnQyxLQUEzTjtBQUE0TmttQyxZQUFRLEVBQUMsa0JBQVNwbUMsUUFBVCxFQUFrQnlrQixLQUFsQixFQUF3QmhHLElBQXhCLEVBQTZCdmUsRUFBN0IsRUFBZ0M7QUFBQ2tvQyxpQkFBVyxDQUFDLG9DQUFELENBQVg7QUFBa0QsYUFBTyxLQUFLNWpCLEVBQUwsQ0FBUUMsS0FBUixFQUFjemtCLFFBQWQsRUFBdUJ5ZSxJQUF2QixFQUE0QnZlLEVBQTVCLENBQVA7QUFBd0MsS0FBaFc7QUFBaVdtbUMsY0FBVSxFQUFDLG9CQUFTcm1DLFFBQVQsRUFBa0J5a0IsS0FBbEIsRUFBd0J2a0IsRUFBeEIsRUFBMkI7QUFBQ2tvQyxpQkFBVyxDQUFDLHNDQUFELENBQVg7QUFBb0QsYUFBT2huQyxTQUFTLENBQUNiLE1BQVYsS0FBbUIsQ0FBbkIsR0FBcUIsS0FBS3NrQixHQUFMLENBQVM3a0IsUUFBVCxFQUFrQixJQUFsQixDQUFyQixHQUE2QyxLQUFLNmtCLEdBQUwsQ0FBU0osS0FBVCxFQUFlemtCLFFBQVEsSUFBRSxJQUF6QixFQUE4QkUsRUFBOUIsQ0FBcEQ7QUFBdUYsS0FBbmhCO0FBQW9oQm9tQyxTQUFLLEVBQUMsZUFBU0MsTUFBVCxFQUFnQkMsS0FBaEIsRUFBc0I7QUFBQzRCLGlCQUFXLENBQUMsaUNBQUQsQ0FBWDtBQUErQyxhQUFPLEtBQUs1akIsRUFBTCxDQUFRLFlBQVIsRUFBcUIraEIsTUFBckIsRUFBNkIvaEIsRUFBN0IsQ0FBZ0MsWUFBaEMsRUFBNkNnaUIsS0FBSyxJQUFFRCxNQUFwRCxDQUFQO0FBQW9FO0FBQXBxQixHQUFqQjs7QUFBd3JCLE1BQUkyRSxTQUFTLEdBQUMsNkZBQWQ7QUFBQSxNQUE0R0MsaUJBQWlCLEdBQUNwckMsTUFBTSxDQUFDZ2tCLGFBQXJJO0FBQUEsTUFBbUpxbkIsVUFBVSxHQUFDLFNBQVhBLFVBQVcsQ0FBU3RmLElBQVQsRUFBYztBQUFDLFFBQUk3c0IsR0FBRyxHQUFDakMsTUFBTSxDQUFDSCxRQUFQLENBQWdCMm5DLGNBQWhCLENBQStCRCxrQkFBL0IsQ0FBa0QsRUFBbEQsQ0FBUjtBQUE4RHRsQyxPQUFHLENBQUN1aUIsSUFBSixDQUFTblUsU0FBVCxHQUFtQnllLElBQW5CO0FBQXdCLFdBQU83c0IsR0FBRyxDQUFDdWlCLElBQUosSUFBVXZpQixHQUFHLENBQUN1aUIsSUFBSixDQUFTblUsU0FBMUI7QUFBcUMsR0FBeFM7QUFBQSxNQUF5U2crQixhQUFhLEdBQUMsU0FBZEEsYUFBYyxDQUFTdmYsSUFBVCxFQUFjO0FBQUMsUUFBSXdmLE9BQU8sR0FBQ3hmLElBQUksQ0FBQzlvQixPQUFMLENBQWFrb0MsU0FBYixFQUF1QixXQUF2QixDQUFaOztBQUFnRCxRQUFHSSxPQUFPLEtBQUd4ZixJQUFWLElBQWdCc2YsVUFBVSxDQUFDdGYsSUFBRCxDQUFWLEtBQW1Cc2YsVUFBVSxDQUFDRSxPQUFELENBQWhELEVBQTBEO0FBQUNsRCxpQkFBVyxDQUFDLG1EQUFpRHRjLElBQWxELENBQVg7QUFBb0U7QUFBQyxHQUF0Zjs7QUFBdWYvckIsUUFBTSxDQUFDd3JDLGlDQUFQLEdBQXlDLFlBQVU7QUFBQ3hyQyxVQUFNLENBQUNna0IsYUFBUCxHQUFxQixVQUFTK0gsSUFBVCxFQUFjO0FBQUN1ZixtQkFBYSxDQUFDdmYsSUFBRCxDQUFiO0FBQW9CLGFBQU9BLElBQUksQ0FBQzlvQixPQUFMLENBQWFrb0MsU0FBYixFQUF1QixXQUF2QixDQUFQO0FBQTRDLEtBQXBHO0FBQXNHLEdBQTFKOztBQUEySm5yQyxRQUFNLENBQUNna0IsYUFBUCxHQUFxQixVQUFTK0gsSUFBVCxFQUFjO0FBQUN1ZixpQkFBYSxDQUFDdmYsSUFBRCxDQUFiO0FBQW9CLFdBQU9xZixpQkFBaUIsQ0FBQ3JmLElBQUQsQ0FBeEI7QUFBZ0MsR0FBeEY7O0FBQXlGLE1BQUkwZixTQUFTLEdBQUN6ckMsTUFBTSxDQUFDRyxFQUFQLENBQVUya0MsTUFBeEI7O0FBQStCOWtDLFFBQU0sQ0FBQ0csRUFBUCxDQUFVMmtDLE1BQVYsR0FBaUIsWUFBVTtBQUFDLFFBQUkxakMsSUFBSSxHQUFDLEtBQUssQ0FBTCxDQUFUOztBQUFpQixRQUFHQSxJQUFJLEtBQUcsQ0FBQ0EsSUFBSSxDQUFDNUMsUUFBTixJQUFnQixDQUFDNEMsSUFBSSxDQUFDdXhCLHFCQUF6QixDQUFQLEVBQXVEO0FBQUMwVixpQkFBVyxDQUFDLGlEQUFELENBQVg7QUFBK0QsYUFBT2huQyxTQUFTLENBQUNiLE1BQVYsR0FBaUIsSUFBakIsR0FBc0JxQyxTQUE3QjtBQUF3Qzs7QUFDMXhFLFdBQU80b0MsU0FBUyxDQUFDN3RDLEtBQVYsQ0FBZ0IsSUFBaEIsRUFBcUJ5RCxTQUFyQixDQUFQO0FBQXdDLEdBRHNpRTs7QUFDcmlFLE1BQUdyQixNQUFNLENBQUNnZ0MsSUFBVixFQUFlO0FBQUMsUUFBSTBMLFFBQVEsR0FBQzFyQyxNQUFNLENBQUNrOEIsS0FBcEI7O0FBQTBCbDhCLFVBQU0sQ0FBQ2s4QixLQUFQLEdBQWEsVUFBU3hkLElBQVQsRUFBY3VkLFdBQWQsRUFBMEI7QUFBQyxVQUFJMFAsZUFBZSxHQUFDM3JDLE1BQU0sQ0FBQ2crQixZQUFQLElBQXFCaCtCLE1BQU0sQ0FBQ2crQixZQUFQLENBQW9CL0IsV0FBN0Q7O0FBQXlFLFVBQUdBLFdBQVcsS0FBR3A1QixTQUFkLElBQXlCOG9DLGVBQTVCLEVBQTRDO0FBQUN0RCxtQkFBVyxDQUFDLCtEQUFELENBQVg7QUFBNkVwTSxtQkFBVyxHQUFDMFAsZUFBWjtBQUE2Qjs7QUFDM1YsYUFBT0QsUUFBUSxDQUFDaHVDLElBQVQsQ0FBYyxJQUFkLEVBQW1CZ2hCLElBQW5CLEVBQXdCdWQsV0FBeEIsQ0FBUDtBQUE2QyxLQURzQztBQUNwQzs7QUFDL0MsTUFBSTJQLE9BQU8sR0FBQzVyQyxNQUFNLENBQUNHLEVBQVAsQ0FBVTByQyxPQUFWLElBQW1CN3JDLE1BQU0sQ0FBQ0csRUFBUCxDQUFVcVgsT0FBekM7O0FBQWlEeFgsUUFBTSxDQUFDRyxFQUFQLENBQVUwckMsT0FBVixHQUFrQixZQUFVO0FBQUN4RCxlQUFXLENBQUMsd0VBQUQsQ0FBWDtBQUFzRixXQUFPdUQsT0FBTyxDQUFDaHVDLEtBQVIsQ0FBYyxJQUFkLEVBQW1CeUQsU0FBbkIsQ0FBUDtBQUFzQyxHQUF6Sjs7QUFBMEosTUFBR3JCLE1BQU0sQ0FBQ2thLFFBQVYsRUFBbUI7QUFBQyxRQUFJNHhCLFdBQVcsR0FBQzlyQyxNQUFNLENBQUNrYSxRQUF2QjtBQUFBLFFBQWdDRSxNQUFNLEdBQUMsQ0FBQyxDQUFDLFNBQUQsRUFBVyxNQUFYLEVBQWtCcGEsTUFBTSxDQUFDdVksU0FBUCxDQUFpQixhQUFqQixDQUFsQixFQUFrRHZZLE1BQU0sQ0FBQ3VZLFNBQVAsQ0FBaUIsYUFBakIsQ0FBbEQsRUFBa0YsVUFBbEYsQ0FBRCxFQUErRixDQUFDLFFBQUQsRUFBVSxNQUFWLEVBQWlCdlksTUFBTSxDQUFDdVksU0FBUCxDQUFpQixhQUFqQixDQUFqQixFQUFpRHZZLE1BQU0sQ0FBQ3VZLFNBQVAsQ0FBaUIsYUFBakIsQ0FBakQsRUFBaUYsVUFBakYsQ0FBL0YsRUFBNEwsQ0FBQyxRQUFELEVBQVUsVUFBVixFQUFxQnZZLE1BQU0sQ0FBQ3VZLFNBQVAsQ0FBaUIsUUFBakIsQ0FBckIsRUFBZ0R2WSxNQUFNLENBQUN1WSxTQUFQLENBQWlCLFFBQWpCLENBQWhELENBQTVMLENBQXZDOztBQUFnVHZZLFVBQU0sQ0FBQ2thLFFBQVAsR0FBZ0IsVUFBU0MsSUFBVCxFQUFjO0FBQUMsVUFBSUksUUFBUSxHQUFDdXhCLFdBQVcsRUFBeEI7QUFBQSxVQUEyQi94QixPQUFPLEdBQUNRLFFBQVEsQ0FBQ1IsT0FBVCxFQUFuQzs7QUFBc0RRLGNBQVEsQ0FBQ0MsSUFBVCxHQUFjVCxPQUFPLENBQUNTLElBQVIsR0FBYSxZQUFVO0FBQUMsWUFBSUMsR0FBRyxHQUFDcFosU0FBUjtBQUFrQmduQyxtQkFBVyxDQUFDLCtCQUFELENBQVg7QUFBNkMsZUFBT3JvQyxNQUFNLENBQUNrYSxRQUFQLENBQWdCLFVBQVNRLFFBQVQsRUFBa0I7QUFBQzFhLGdCQUFNLENBQUNpQixJQUFQLENBQVltWixNQUFaLEVBQW1CLFVBQVNqYixDQUFULEVBQVd3YixLQUFYLEVBQWlCO0FBQUMsZ0JBQUl4YSxFQUFFLEdBQUMsT0FBT3NhLEdBQUcsQ0FBQ3RiLENBQUQsQ0FBVixLQUFnQixVQUFoQixJQUE0QnNiLEdBQUcsQ0FBQ3RiLENBQUQsQ0FBdEM7QUFBMENvYixvQkFBUSxDQUFDSSxLQUFLLENBQUMsQ0FBRCxDQUFOLENBQVIsQ0FBbUIsWUFBVTtBQUFDLGtCQUFJQyxRQUFRLEdBQUN6YSxFQUFFLElBQUVBLEVBQUUsQ0FBQ3ZDLEtBQUgsQ0FBUyxJQUFULEVBQWN5RCxTQUFkLENBQWpCOztBQUEwQyxrQkFBR3VaLFFBQVEsSUFBRSxPQUFPQSxRQUFRLENBQUNiLE9BQWhCLEtBQTBCLFVBQXZDLEVBQWtEO0FBQUNhLHdCQUFRLENBQUNiLE9BQVQsR0FBbUJqVSxJQUFuQixDQUF3QjRVLFFBQVEsQ0FBQ2YsT0FBakMsRUFBMENLLElBQTFDLENBQStDVSxRQUFRLENBQUNkLE1BQXhELEVBQWdFaUIsUUFBaEUsQ0FBeUVILFFBQVEsQ0FBQ0ksTUFBbEY7QUFBMkYsZUFBOUksTUFBa0o7QUFBQ0osd0JBQVEsQ0FBQ0MsS0FBSyxDQUFDLENBQUQsQ0FBTCxHQUFTLE1BQVYsQ0FBUixDQUEwQixTQUFPWixPQUFQLEdBQWVXLFFBQVEsQ0FBQ1gsT0FBVCxFQUFmLEdBQWtDLElBQTVELEVBQWlFNVosRUFBRSxHQUFDLENBQUN5YSxRQUFELENBQUQsR0FBWXZaLFNBQS9FO0FBQTJGO0FBQUMsYUFBdlQ7QUFBMFQsV0FBelk7QUFBMllvWixhQUFHLEdBQUMsSUFBSjtBQUFVLFNBQXhiLEVBQTBiVixPQUExYixFQUFQO0FBQTRjLE9BQWpqQjs7QUFBa2pCLFVBQUdJLElBQUgsRUFBUTtBQUFDQSxZQUFJLENBQUN6YyxJQUFMLENBQVU2YyxRQUFWLEVBQW1CQSxRQUFuQjtBQUE4Qjs7QUFDN3JDLGFBQU9BLFFBQVA7QUFBaUIsS0FEOGY7O0FBQzdmdmEsVUFBTSxDQUFDa2EsUUFBUCxDQUFnQnlCLGFBQWhCLEdBQThCbXdCLFdBQVcsQ0FBQ253QixhQUExQztBQUF5RDs7QUFDM0UsU0FBTzNiLE1BQVA7QUFBZSxDQXBEZixFIiwiZmlsZSI6IjMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcbiAqIGpRdWVyeSBKYXZhU2NyaXB0IExpYnJhcnkgdjMuNS4xXG4gKiBodHRwczovL2pxdWVyeS5jb20vXG4gKlxuICogSW5jbHVkZXMgU2l6emxlLmpzXG4gKiBodHRwczovL3NpenpsZWpzLmNvbS9cbiAqXG4gKiBDb3B5cmlnaHQgSlMgRm91bmRhdGlvbiBhbmQgb3RoZXIgY29udHJpYnV0b3JzXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2VcbiAqIGh0dHBzOi8vanF1ZXJ5Lm9yZy9saWNlbnNlXG4gKlxuICogRGF0ZTogMjAyMC0wNS0wNFQyMjo0OVpcbiAqL1xuKGZ1bmN0aW9uKGdsb2JhbCxmYWN0b3J5KXtcInVzZSBzdHJpY3RcIjtpZih0eXBlb2YgbW9kdWxlPT09XCJvYmplY3RcIiYmdHlwZW9mIG1vZHVsZS5leHBvcnRzPT09XCJvYmplY3RcIil7bW9kdWxlLmV4cG9ydHM9Z2xvYmFsLmRvY3VtZW50P2ZhY3RvcnkoZ2xvYmFsLHRydWUpOmZ1bmN0aW9uKHcpe2lmKCF3LmRvY3VtZW50KXt0aHJvdyBuZXcgRXJyb3IoXCJqUXVlcnkgcmVxdWlyZXMgYSB3aW5kb3cgd2l0aCBhIGRvY3VtZW50XCIpO31cbnJldHVybiBmYWN0b3J5KHcpO307fWVsc2V7ZmFjdG9yeShnbG9iYWwpO319KSh0eXBlb2Ygd2luZG93IT09XCJ1bmRlZmluZWRcIj93aW5kb3c6dGhpcyxmdW5jdGlvbih3aW5kb3csbm9HbG9iYWwpe1widXNlIHN0cmljdFwiO3ZhciBhcnI9W107dmFyIGdldFByb3RvPU9iamVjdC5nZXRQcm90b3R5cGVPZjt2YXIgc2xpY2U9YXJyLnNsaWNlO3ZhciBmbGF0PWFyci5mbGF0P2Z1bmN0aW9uKGFycmF5KXtyZXR1cm4gYXJyLmZsYXQuY2FsbChhcnJheSk7fTpmdW5jdGlvbihhcnJheSl7cmV0dXJuIGFyci5jb25jYXQuYXBwbHkoW10sYXJyYXkpO307dmFyIHB1c2g9YXJyLnB1c2g7dmFyIGluZGV4T2Y9YXJyLmluZGV4T2Y7dmFyIGNsYXNzMnR5cGU9e307dmFyIHRvU3RyaW5nPWNsYXNzMnR5cGUudG9TdHJpbmc7dmFyIGhhc093bj1jbGFzczJ0eXBlLmhhc093blByb3BlcnR5O3ZhciBmblRvU3RyaW5nPWhhc093bi50b1N0cmluZzt2YXIgT2JqZWN0RnVuY3Rpb25TdHJpbmc9Zm5Ub1N0cmluZy5jYWxsKE9iamVjdCk7dmFyIHN1cHBvcnQ9e307dmFyIGlzRnVuY3Rpb249ZnVuY3Rpb24gaXNGdW5jdGlvbihvYmope3JldHVybiB0eXBlb2Ygb2JqPT09XCJmdW5jdGlvblwiJiZ0eXBlb2Ygb2JqLm5vZGVUeXBlIT09XCJudW1iZXJcIjt9O3ZhciBpc1dpbmRvdz1mdW5jdGlvbiBpc1dpbmRvdyhvYmope3JldHVybiBvYmohPW51bGwmJm9iaj09PW9iai53aW5kb3c7fTt2YXIgZG9jdW1lbnQ9d2luZG93LmRvY3VtZW50O3ZhciBwcmVzZXJ2ZWRTY3JpcHRBdHRyaWJ1dGVzPXt0eXBlOnRydWUsc3JjOnRydWUsbm9uY2U6dHJ1ZSxub01vZHVsZTp0cnVlfTtmdW5jdGlvbiBET01FdmFsKGNvZGUsbm9kZSxkb2Mpe2RvYz1kb2N8fGRvY3VtZW50O3ZhciBpLHZhbCxzY3JpcHQ9ZG9jLmNyZWF0ZUVsZW1lbnQoXCJzY3JpcHRcIik7c2NyaXB0LnRleHQ9Y29kZTtpZihub2RlKXtmb3IoaSBpbiBwcmVzZXJ2ZWRTY3JpcHRBdHRyaWJ1dGVzKXt2YWw9bm9kZVtpXXx8bm9kZS5nZXRBdHRyaWJ1dGUmJm5vZGUuZ2V0QXR0cmlidXRlKGkpO2lmKHZhbCl7c2NyaXB0LnNldEF0dHJpYnV0ZShpLHZhbCk7fX19XG5kb2MuaGVhZC5hcHBlbmRDaGlsZChzY3JpcHQpLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc2NyaXB0KTt9XG5mdW5jdGlvbiB0b1R5cGUob2JqKXtpZihvYmo9PW51bGwpe3JldHVybiBvYmorXCJcIjt9XG5yZXR1cm4gdHlwZW9mIG9iaj09PVwib2JqZWN0XCJ8fHR5cGVvZiBvYmo9PT1cImZ1bmN0aW9uXCI/Y2xhc3MydHlwZVt0b1N0cmluZy5jYWxsKG9iaildfHxcIm9iamVjdFwiOnR5cGVvZiBvYmo7fVxudmFyXG52ZXJzaW9uPVwiMy41LjFcIixqUXVlcnk9ZnVuY3Rpb24oc2VsZWN0b3IsY29udGV4dCl7cmV0dXJuIG5ldyBqUXVlcnkuZm4uaW5pdChzZWxlY3Rvcixjb250ZXh0KTt9O2pRdWVyeS5mbj1qUXVlcnkucHJvdG90eXBlPXtqcXVlcnk6dmVyc2lvbixjb25zdHJ1Y3RvcjpqUXVlcnksbGVuZ3RoOjAsdG9BcnJheTpmdW5jdGlvbigpe3JldHVybiBzbGljZS5jYWxsKHRoaXMpO30sZ2V0OmZ1bmN0aW9uKG51bSl7aWYobnVtPT1udWxsKXtyZXR1cm4gc2xpY2UuY2FsbCh0aGlzKTt9XG5yZXR1cm4gbnVtPDA/dGhpc1tudW0rdGhpcy5sZW5ndGhdOnRoaXNbbnVtXTt9LHB1c2hTdGFjazpmdW5jdGlvbihlbGVtcyl7dmFyIHJldD1qUXVlcnkubWVyZ2UodGhpcy5jb25zdHJ1Y3RvcigpLGVsZW1zKTtyZXQucHJldk9iamVjdD10aGlzO3JldHVybiByZXQ7fSxlYWNoOmZ1bmN0aW9uKGNhbGxiYWNrKXtyZXR1cm4galF1ZXJ5LmVhY2godGhpcyxjYWxsYmFjayk7fSxtYXA6ZnVuY3Rpb24oY2FsbGJhY2spe3JldHVybiB0aGlzLnB1c2hTdGFjayhqUXVlcnkubWFwKHRoaXMsZnVuY3Rpb24oZWxlbSxpKXtyZXR1cm4gY2FsbGJhY2suY2FsbChlbGVtLGksZWxlbSk7fSkpO30sc2xpY2U6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5wdXNoU3RhY2soc2xpY2UuYXBwbHkodGhpcyxhcmd1bWVudHMpKTt9LGZpcnN0OmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuZXEoMCk7fSxsYXN0OmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuZXEoLTEpO30sZXZlbjpmdW5jdGlvbigpe3JldHVybiB0aGlzLnB1c2hTdGFjayhqUXVlcnkuZ3JlcCh0aGlzLGZ1bmN0aW9uKF9lbGVtLGkpe3JldHVybihpKzEpJTI7fSkpO30sb2RkOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMucHVzaFN0YWNrKGpRdWVyeS5ncmVwKHRoaXMsZnVuY3Rpb24oX2VsZW0saSl7cmV0dXJuIGklMjt9KSk7fSxlcTpmdW5jdGlvbihpKXt2YXIgbGVuPXRoaXMubGVuZ3RoLGo9K2krKGk8MD9sZW46MCk7cmV0dXJuIHRoaXMucHVzaFN0YWNrKGo+PTAmJmo8bGVuP1t0aGlzW2pdXTpbXSk7fSxlbmQ6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5wcmV2T2JqZWN0fHx0aGlzLmNvbnN0cnVjdG9yKCk7fSxwdXNoOnB1c2gsc29ydDphcnIuc29ydCxzcGxpY2U6YXJyLnNwbGljZX07alF1ZXJ5LmV4dGVuZD1qUXVlcnkuZm4uZXh0ZW5kPWZ1bmN0aW9uKCl7dmFyIG9wdGlvbnMsbmFtZSxzcmMsY29weSxjb3B5SXNBcnJheSxjbG9uZSx0YXJnZXQ9YXJndW1lbnRzWzBdfHx7fSxpPTEsbGVuZ3RoPWFyZ3VtZW50cy5sZW5ndGgsZGVlcD1mYWxzZTtpZih0eXBlb2YgdGFyZ2V0PT09XCJib29sZWFuXCIpe2RlZXA9dGFyZ2V0O3RhcmdldD1hcmd1bWVudHNbaV18fHt9O2krKzt9XG5pZih0eXBlb2YgdGFyZ2V0IT09XCJvYmplY3RcIiYmIWlzRnVuY3Rpb24odGFyZ2V0KSl7dGFyZ2V0PXt9O31cbmlmKGk9PT1sZW5ndGgpe3RhcmdldD10aGlzO2ktLTt9XG5mb3IoO2k8bGVuZ3RoO2krKyl7aWYoKG9wdGlvbnM9YXJndW1lbnRzW2ldKSE9bnVsbCl7Zm9yKG5hbWUgaW4gb3B0aW9ucyl7Y29weT1vcHRpb25zW25hbWVdO2lmKG5hbWU9PT1cIl9fcHJvdG9fX1wifHx0YXJnZXQ9PT1jb3B5KXtjb250aW51ZTt9XG5pZihkZWVwJiZjb3B5JiYoalF1ZXJ5LmlzUGxhaW5PYmplY3QoY29weSl8fChjb3B5SXNBcnJheT1BcnJheS5pc0FycmF5KGNvcHkpKSkpe3NyYz10YXJnZXRbbmFtZV07aWYoY29weUlzQXJyYXkmJiFBcnJheS5pc0FycmF5KHNyYykpe2Nsb25lPVtdO31lbHNlIGlmKCFjb3B5SXNBcnJheSYmIWpRdWVyeS5pc1BsYWluT2JqZWN0KHNyYykpe2Nsb25lPXt9O31lbHNle2Nsb25lPXNyYzt9XG5jb3B5SXNBcnJheT1mYWxzZTt0YXJnZXRbbmFtZV09alF1ZXJ5LmV4dGVuZChkZWVwLGNsb25lLGNvcHkpO31lbHNlIGlmKGNvcHkhPT11bmRlZmluZWQpe3RhcmdldFtuYW1lXT1jb3B5O319fX1cbnJldHVybiB0YXJnZXQ7fTtqUXVlcnkuZXh0ZW5kKHtleHBhbmRvOlwialF1ZXJ5XCIrKHZlcnNpb24rTWF0aC5yYW5kb20oKSkucmVwbGFjZSgvXFxEL2csXCJcIiksaXNSZWFkeTp0cnVlLGVycm9yOmZ1bmN0aW9uKG1zZyl7dGhyb3cgbmV3IEVycm9yKG1zZyk7fSxub29wOmZ1bmN0aW9uKCl7fSxpc1BsYWluT2JqZWN0OmZ1bmN0aW9uKG9iail7dmFyIHByb3RvLEN0b3I7aWYoIW9ianx8dG9TdHJpbmcuY2FsbChvYmopIT09XCJbb2JqZWN0IE9iamVjdF1cIil7cmV0dXJuIGZhbHNlO31cbnByb3RvPWdldFByb3RvKG9iaik7aWYoIXByb3RvKXtyZXR1cm4gdHJ1ZTt9XG5DdG9yPWhhc093bi5jYWxsKHByb3RvLFwiY29uc3RydWN0b3JcIikmJnByb3RvLmNvbnN0cnVjdG9yO3JldHVybiB0eXBlb2YgQ3Rvcj09PVwiZnVuY3Rpb25cIiYmZm5Ub1N0cmluZy5jYWxsKEN0b3IpPT09T2JqZWN0RnVuY3Rpb25TdHJpbmc7fSxpc0VtcHR5T2JqZWN0OmZ1bmN0aW9uKG9iail7dmFyIG5hbWU7Zm9yKG5hbWUgaW4gb2JqKXtyZXR1cm4gZmFsc2U7fVxucmV0dXJuIHRydWU7fSxnbG9iYWxFdmFsOmZ1bmN0aW9uKGNvZGUsb3B0aW9ucyxkb2Mpe0RPTUV2YWwoY29kZSx7bm9uY2U6b3B0aW9ucyYmb3B0aW9ucy5ub25jZX0sZG9jKTt9LGVhY2g6ZnVuY3Rpb24ob2JqLGNhbGxiYWNrKXt2YXIgbGVuZ3RoLGk9MDtpZihpc0FycmF5TGlrZShvYmopKXtsZW5ndGg9b2JqLmxlbmd0aDtmb3IoO2k8bGVuZ3RoO2krKyl7aWYoY2FsbGJhY2suY2FsbChvYmpbaV0saSxvYmpbaV0pPT09ZmFsc2Upe2JyZWFrO319fWVsc2V7Zm9yKGkgaW4gb2JqKXtpZihjYWxsYmFjay5jYWxsKG9ialtpXSxpLG9ialtpXSk9PT1mYWxzZSl7YnJlYWs7fX19XG5yZXR1cm4gb2JqO30sbWFrZUFycmF5OmZ1bmN0aW9uKGFycixyZXN1bHRzKXt2YXIgcmV0PXJlc3VsdHN8fFtdO2lmKGFyciE9bnVsbCl7aWYoaXNBcnJheUxpa2UoT2JqZWN0KGFycikpKXtqUXVlcnkubWVyZ2UocmV0LHR5cGVvZiBhcnI9PT1cInN0cmluZ1wiP1thcnJdOmFycik7fWVsc2V7cHVzaC5jYWxsKHJldCxhcnIpO319XG5yZXR1cm4gcmV0O30saW5BcnJheTpmdW5jdGlvbihlbGVtLGFycixpKXtyZXR1cm4gYXJyPT1udWxsPy0xOmluZGV4T2YuY2FsbChhcnIsZWxlbSxpKTt9LG1lcmdlOmZ1bmN0aW9uKGZpcnN0LHNlY29uZCl7dmFyIGxlbj0rc2Vjb25kLmxlbmd0aCxqPTAsaT1maXJzdC5sZW5ndGg7Zm9yKDtqPGxlbjtqKyspe2ZpcnN0W2krK109c2Vjb25kW2pdO31cbmZpcnN0Lmxlbmd0aD1pO3JldHVybiBmaXJzdDt9LGdyZXA6ZnVuY3Rpb24oZWxlbXMsY2FsbGJhY2ssaW52ZXJ0KXt2YXIgY2FsbGJhY2tJbnZlcnNlLG1hdGNoZXM9W10saT0wLGxlbmd0aD1lbGVtcy5sZW5ndGgsY2FsbGJhY2tFeHBlY3Q9IWludmVydDtmb3IoO2k8bGVuZ3RoO2krKyl7Y2FsbGJhY2tJbnZlcnNlPSFjYWxsYmFjayhlbGVtc1tpXSxpKTtpZihjYWxsYmFja0ludmVyc2UhPT1jYWxsYmFja0V4cGVjdCl7bWF0Y2hlcy5wdXNoKGVsZW1zW2ldKTt9fVxucmV0dXJuIG1hdGNoZXM7fSxtYXA6ZnVuY3Rpb24oZWxlbXMsY2FsbGJhY2ssYXJnKXt2YXIgbGVuZ3RoLHZhbHVlLGk9MCxyZXQ9W107aWYoaXNBcnJheUxpa2UoZWxlbXMpKXtsZW5ndGg9ZWxlbXMubGVuZ3RoO2Zvcig7aTxsZW5ndGg7aSsrKXt2YWx1ZT1jYWxsYmFjayhlbGVtc1tpXSxpLGFyZyk7aWYodmFsdWUhPW51bGwpe3JldC5wdXNoKHZhbHVlKTt9fX1lbHNle2ZvcihpIGluIGVsZW1zKXt2YWx1ZT1jYWxsYmFjayhlbGVtc1tpXSxpLGFyZyk7aWYodmFsdWUhPW51bGwpe3JldC5wdXNoKHZhbHVlKTt9fX1cbnJldHVybiBmbGF0KHJldCk7fSxndWlkOjEsc3VwcG9ydDpzdXBwb3J0fSk7aWYodHlwZW9mIFN5bWJvbD09PVwiZnVuY3Rpb25cIil7alF1ZXJ5LmZuW1N5bWJvbC5pdGVyYXRvcl09YXJyW1N5bWJvbC5pdGVyYXRvcl07fVxualF1ZXJ5LmVhY2goXCJCb29sZWFuIE51bWJlciBTdHJpbmcgRnVuY3Rpb24gQXJyYXkgRGF0ZSBSZWdFeHAgT2JqZWN0IEVycm9yIFN5bWJvbFwiLnNwbGl0KFwiIFwiKSxmdW5jdGlvbihfaSxuYW1lKXtjbGFzczJ0eXBlW1wiW29iamVjdCBcIituYW1lK1wiXVwiXT1uYW1lLnRvTG93ZXJDYXNlKCk7fSk7ZnVuY3Rpb24gaXNBcnJheUxpa2Uob2JqKXt2YXIgbGVuZ3RoPSEhb2JqJiZcImxlbmd0aFwiaW4gb2JqJiZvYmoubGVuZ3RoLHR5cGU9dG9UeXBlKG9iaik7aWYoaXNGdW5jdGlvbihvYmopfHxpc1dpbmRvdyhvYmopKXtyZXR1cm4gZmFsc2U7fVxucmV0dXJuIHR5cGU9PT1cImFycmF5XCJ8fGxlbmd0aD09PTB8fHR5cGVvZiBsZW5ndGg9PT1cIm51bWJlclwiJiZsZW5ndGg+MCYmKGxlbmd0aC0xKWluIG9iajt9XG52YXIgU2l6emxlPVxuLyohXG4gKiBTaXp6bGUgQ1NTIFNlbGVjdG9yIEVuZ2luZSB2Mi4zLjVcbiAqIGh0dHBzOi8vc2l6emxlanMuY29tL1xuICpcbiAqIENvcHlyaWdodCBKUyBGb3VuZGF0aW9uIGFuZCBvdGhlciBjb250cmlidXRvcnNcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxuICogaHR0cHM6Ly9qcy5mb3VuZGF0aW9uL1xuICpcbiAqIERhdGU6IDIwMjAtMDMtMTRcbiAqL1xuKGZ1bmN0aW9uKHdpbmRvdyl7dmFyIGksc3VwcG9ydCxFeHByLGdldFRleHQsaXNYTUwsdG9rZW5pemUsY29tcGlsZSxzZWxlY3Qsb3V0ZXJtb3N0Q29udGV4dCxzb3J0SW5wdXQsaGFzRHVwbGljYXRlLHNldERvY3VtZW50LGRvY3VtZW50LGRvY0VsZW0sZG9jdW1lbnRJc0hUTUwscmJ1Z2d5UVNBLHJidWdneU1hdGNoZXMsbWF0Y2hlcyxjb250YWlucyxleHBhbmRvPVwic2l6emxlXCIrMSpuZXcgRGF0ZSgpLHByZWZlcnJlZERvYz13aW5kb3cuZG9jdW1lbnQsZGlycnVucz0wLGRvbmU9MCxjbGFzc0NhY2hlPWNyZWF0ZUNhY2hlKCksdG9rZW5DYWNoZT1jcmVhdGVDYWNoZSgpLGNvbXBpbGVyQ2FjaGU9Y3JlYXRlQ2FjaGUoKSxub25uYXRpdmVTZWxlY3RvckNhY2hlPWNyZWF0ZUNhY2hlKCksc29ydE9yZGVyPWZ1bmN0aW9uKGEsYil7aWYoYT09PWIpe2hhc0R1cGxpY2F0ZT10cnVlO31cbnJldHVybiAwO30saGFzT3duPSh7fSkuaGFzT3duUHJvcGVydHksYXJyPVtdLHBvcD1hcnIucG9wLHB1c2hOYXRpdmU9YXJyLnB1c2gscHVzaD1hcnIucHVzaCxzbGljZT1hcnIuc2xpY2UsaW5kZXhPZj1mdW5jdGlvbihsaXN0LGVsZW0pe3ZhciBpPTAsbGVuPWxpc3QubGVuZ3RoO2Zvcig7aTxsZW47aSsrKXtpZihsaXN0W2ldPT09ZWxlbSl7cmV0dXJuIGk7fX1cbnJldHVybi0xO30sYm9vbGVhbnM9XCJjaGVja2VkfHNlbGVjdGVkfGFzeW5jfGF1dG9mb2N1c3xhdXRvcGxheXxjb250cm9sc3xkZWZlcnxkaXNhYmxlZHxoaWRkZW58XCIrXCJpc21hcHxsb29wfG11bHRpcGxlfG9wZW58cmVhZG9ubHl8cmVxdWlyZWR8c2NvcGVkXCIsd2hpdGVzcGFjZT1cIltcXFxceDIwXFxcXHRcXFxcclxcXFxuXFxcXGZdXCIsaWRlbnRpZmllcj1cIig/OlxcXFxcXFxcW1xcXFxkYS1mQS1GXXsxLDZ9XCIrd2hpdGVzcGFjZStcIj98XFxcXFxcXFxbXlxcXFxyXFxcXG5cXFxcZl18W1xcXFx3LV18W15cXDAtXFxcXHg3Zl0pK1wiLGF0dHJpYnV0ZXM9XCJcXFxcW1wiK3doaXRlc3BhY2UrXCIqKFwiK2lkZW50aWZpZXIrXCIpKD86XCIrd2hpdGVzcGFjZStcIiooWypeJHwhfl0/PSlcIit3aGl0ZXNwYWNlK1wiKig/OicoKD86XFxcXFxcXFwufFteXFxcXFxcXFwnXSkqKSd8XFxcIigoPzpcXFxcXFxcXC58W15cXFxcXFxcXFxcXCJdKSopXFxcInwoXCIraWRlbnRpZmllcitcIikpfClcIitcbndoaXRlc3BhY2UrXCIqXFxcXF1cIixwc2V1ZG9zPVwiOihcIitpZGVudGlmaWVyK1wiKSg/OlxcXFwoKFwiK1wiKCcoKD86XFxcXFxcXFwufFteXFxcXFxcXFwnXSkqKSd8XFxcIigoPzpcXFxcXFxcXC58W15cXFxcXFxcXFxcXCJdKSopXFxcIil8XCIrXCIoKD86XFxcXFxcXFwufFteXFxcXFxcXFwoKVtcXFxcXV18XCIrYXR0cmlidXRlcytcIikqKXxcIitcIi4qXCIrXCIpXFxcXCl8KVwiLHJ3aGl0ZXNwYWNlPW5ldyBSZWdFeHAod2hpdGVzcGFjZStcIitcIixcImdcIikscnRyaW09bmV3IFJlZ0V4cChcIl5cIit3aGl0ZXNwYWNlK1wiK3woKD86XnxbXlxcXFxcXFxcXSkoPzpcXFxcXFxcXC4pKilcIitcbndoaXRlc3BhY2UrXCIrJFwiLFwiZ1wiKSxyY29tbWE9bmV3IFJlZ0V4cChcIl5cIit3aGl0ZXNwYWNlK1wiKixcIit3aGl0ZXNwYWNlK1wiKlwiKSxyY29tYmluYXRvcnM9bmV3IFJlZ0V4cChcIl5cIit3aGl0ZXNwYWNlK1wiKihbPit+XXxcIit3aGl0ZXNwYWNlK1wiKVwiK3doaXRlc3BhY2UrXCIqXCIpLHJkZXNjZW5kPW5ldyBSZWdFeHAod2hpdGVzcGFjZStcInw+XCIpLHJwc2V1ZG89bmV3IFJlZ0V4cChwc2V1ZG9zKSxyaWRlbnRpZmllcj1uZXcgUmVnRXhwKFwiXlwiK2lkZW50aWZpZXIrXCIkXCIpLG1hdGNoRXhwcj17XCJJRFwiOm5ldyBSZWdFeHAoXCJeIyhcIitpZGVudGlmaWVyK1wiKVwiKSxcIkNMQVNTXCI6bmV3IFJlZ0V4cChcIl5cXFxcLihcIitpZGVudGlmaWVyK1wiKVwiKSxcIlRBR1wiOm5ldyBSZWdFeHAoXCJeKFwiK2lkZW50aWZpZXIrXCJ8WypdKVwiKSxcIkFUVFJcIjpuZXcgUmVnRXhwKFwiXlwiK2F0dHJpYnV0ZXMpLFwiUFNFVURPXCI6bmV3IFJlZ0V4cChcIl5cIitwc2V1ZG9zKSxcIkNISUxEXCI6bmV3IFJlZ0V4cChcIl46KG9ubHl8Zmlyc3R8bGFzdHxudGh8bnRoLWxhc3QpLShjaGlsZHxvZi10eXBlKSg/OlxcXFwoXCIrXG53aGl0ZXNwYWNlK1wiKihldmVufG9kZHwoKFsrLV18KShcXFxcZCopbnwpXCIrd2hpdGVzcGFjZStcIiooPzooWystXXwpXCIrXG53aGl0ZXNwYWNlK1wiKihcXFxcZCspfCkpXCIrd2hpdGVzcGFjZStcIipcXFxcKXwpXCIsXCJpXCIpLFwiYm9vbFwiOm5ldyBSZWdFeHAoXCJeKD86XCIrYm9vbGVhbnMrXCIpJFwiLFwiaVwiKSxcIm5lZWRzQ29udGV4dFwiOm5ldyBSZWdFeHAoXCJeXCIrd2hpdGVzcGFjZStcIipbPit+XXw6KGV2ZW58b2RkfGVxfGd0fGx0fG50aHxmaXJzdHxsYXN0KSg/OlxcXFwoXCIrd2hpdGVzcGFjZStcIiooKD86LVxcXFxkKT9cXFxcZCopXCIrd2hpdGVzcGFjZStcIipcXFxcKXwpKD89W14tXXwkKVwiLFwiaVwiKX0scmh0bWw9L0hUTUwkL2kscmlucHV0cz0vXig/OmlucHV0fHNlbGVjdHx0ZXh0YXJlYXxidXR0b24pJC9pLHJoZWFkZXI9L15oXFxkJC9pLHJuYXRpdmU9L15bXntdK1xce1xccypcXFtuYXRpdmUgXFx3LyxycXVpY2tFeHByPS9eKD86IyhbXFx3LV0rKXwoXFx3Kyl8XFwuKFtcXHctXSspKSQvLHJzaWJsaW5nPS9bK35dLyxydW5lc2NhcGU9bmV3IFJlZ0V4cChcIlxcXFxcXFxcW1xcXFxkYS1mQS1GXXsxLDZ9XCIrd2hpdGVzcGFjZStcIj98XFxcXFxcXFwoW15cXFxcclxcXFxuXFxcXGZdKVwiLFwiZ1wiKSxmdW5lc2NhcGU9ZnVuY3Rpb24oZXNjYXBlLG5vbkhleCl7dmFyIGhpZ2g9XCIweFwiK2VzY2FwZS5zbGljZSgxKS0weDEwMDAwO3JldHVybiBub25IZXg/bm9uSGV4OmhpZ2g8MD9TdHJpbmcuZnJvbUNoYXJDb2RlKGhpZ2grMHgxMDAwMCk6U3RyaW5nLmZyb21DaGFyQ29kZShoaWdoPj4xMHwweEQ4MDAsaGlnaCYweDNGRnwweERDMDApO30scmNzc2VzY2FwZT0vKFtcXDAtXFx4MWZcXHg3Zl18Xi0/XFxkKXxeLSR8W15cXDAtXFx4MWZcXHg3Zi1cXHVGRkZGXFx3LV0vZyxmY3NzZXNjYXBlPWZ1bmN0aW9uKGNoLGFzQ29kZVBvaW50KXtpZihhc0NvZGVQb2ludCl7aWYoY2g9PT1cIlxcMFwiKXtyZXR1cm5cIlxcdUZGRkRcIjt9XG5yZXR1cm4gY2guc2xpY2UoMCwtMSkrXCJcXFxcXCIrXG5jaC5jaGFyQ29kZUF0KGNoLmxlbmd0aC0xKS50b1N0cmluZygxNikrXCIgXCI7fVxucmV0dXJuXCJcXFxcXCIrY2g7fSx1bmxvYWRIYW5kbGVyPWZ1bmN0aW9uKCl7c2V0RG9jdW1lbnQoKTt9LGluRGlzYWJsZWRGaWVsZHNldD1hZGRDb21iaW5hdG9yKGZ1bmN0aW9uKGVsZW0pe3JldHVybiBlbGVtLmRpc2FibGVkPT09dHJ1ZSYmZWxlbS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpPT09XCJmaWVsZHNldFwiO30se2RpcjpcInBhcmVudE5vZGVcIixuZXh0OlwibGVnZW5kXCJ9KTt0cnl7cHVzaC5hcHBseSgoYXJyPXNsaWNlLmNhbGwocHJlZmVycmVkRG9jLmNoaWxkTm9kZXMpKSxwcmVmZXJyZWREb2MuY2hpbGROb2Rlcyk7YXJyW3ByZWZlcnJlZERvYy5jaGlsZE5vZGVzLmxlbmd0aF0ubm9kZVR5cGU7fWNhdGNoKGUpe3B1c2g9e2FwcGx5OmFyci5sZW5ndGg/ZnVuY3Rpb24odGFyZ2V0LGVscyl7cHVzaE5hdGl2ZS5hcHBseSh0YXJnZXQsc2xpY2UuY2FsbChlbHMpKTt9OmZ1bmN0aW9uKHRhcmdldCxlbHMpe3ZhciBqPXRhcmdldC5sZW5ndGgsaT0wO3doaWxlKCh0YXJnZXRbaisrXT1lbHNbaSsrXSkpe31cbnRhcmdldC5sZW5ndGg9ai0xO319O31cbmZ1bmN0aW9uIFNpenpsZShzZWxlY3Rvcixjb250ZXh0LHJlc3VsdHMsc2VlZCl7dmFyIG0saSxlbGVtLG5pZCxtYXRjaCxncm91cHMsbmV3U2VsZWN0b3IsbmV3Q29udGV4dD1jb250ZXh0JiZjb250ZXh0Lm93bmVyRG9jdW1lbnQsbm9kZVR5cGU9Y29udGV4dD9jb250ZXh0Lm5vZGVUeXBlOjk7cmVzdWx0cz1yZXN1bHRzfHxbXTtpZih0eXBlb2Ygc2VsZWN0b3IhPT1cInN0cmluZ1wifHwhc2VsZWN0b3J8fG5vZGVUeXBlIT09MSYmbm9kZVR5cGUhPT05JiZub2RlVHlwZSE9PTExKXtyZXR1cm4gcmVzdWx0czt9XG5pZighc2VlZCl7c2V0RG9jdW1lbnQoY29udGV4dCk7Y29udGV4dD1jb250ZXh0fHxkb2N1bWVudDtpZihkb2N1bWVudElzSFRNTCl7aWYobm9kZVR5cGUhPT0xMSYmKG1hdGNoPXJxdWlja0V4cHIuZXhlYyhzZWxlY3RvcikpKXtpZigobT1tYXRjaFsxXSkpe2lmKG5vZGVUeXBlPT09OSl7aWYoKGVsZW09Y29udGV4dC5nZXRFbGVtZW50QnlJZChtKSkpe2lmKGVsZW0uaWQ9PT1tKXtyZXN1bHRzLnB1c2goZWxlbSk7cmV0dXJuIHJlc3VsdHM7fX1lbHNle3JldHVybiByZXN1bHRzO319ZWxzZXtpZihuZXdDb250ZXh0JiYoZWxlbT1uZXdDb250ZXh0LmdldEVsZW1lbnRCeUlkKG0pKSYmY29udGFpbnMoY29udGV4dCxlbGVtKSYmZWxlbS5pZD09PW0pe3Jlc3VsdHMucHVzaChlbGVtKTtyZXR1cm4gcmVzdWx0czt9fX1lbHNlIGlmKG1hdGNoWzJdKXtwdXNoLmFwcGx5KHJlc3VsdHMsY29udGV4dC5nZXRFbGVtZW50c0J5VGFnTmFtZShzZWxlY3RvcikpO3JldHVybiByZXN1bHRzO31lbHNlIGlmKChtPW1hdGNoWzNdKSYmc3VwcG9ydC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lJiZjb250ZXh0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUpe3B1c2guYXBwbHkocmVzdWx0cyxjb250ZXh0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUobSkpO3JldHVybiByZXN1bHRzO319XG5pZihzdXBwb3J0LnFzYSYmIW5vbm5hdGl2ZVNlbGVjdG9yQ2FjaGVbc2VsZWN0b3IrXCIgXCJdJiYoIXJidWdneVFTQXx8IXJidWdneVFTQS50ZXN0KHNlbGVjdG9yKSkmJihub2RlVHlwZSE9PTF8fGNvbnRleHQubm9kZU5hbWUudG9Mb3dlckNhc2UoKSE9PVwib2JqZWN0XCIpKXtuZXdTZWxlY3Rvcj1zZWxlY3RvcjtuZXdDb250ZXh0PWNvbnRleHQ7aWYobm9kZVR5cGU9PT0xJiYocmRlc2NlbmQudGVzdChzZWxlY3Rvcil8fHJjb21iaW5hdG9ycy50ZXN0KHNlbGVjdG9yKSkpe25ld0NvbnRleHQ9cnNpYmxpbmcudGVzdChzZWxlY3RvcikmJnRlc3RDb250ZXh0KGNvbnRleHQucGFyZW50Tm9kZSl8fGNvbnRleHQ7aWYobmV3Q29udGV4dCE9PWNvbnRleHR8fCFzdXBwb3J0LnNjb3BlKXtpZigobmlkPWNvbnRleHQuZ2V0QXR0cmlidXRlKFwiaWRcIikpKXtuaWQ9bmlkLnJlcGxhY2UocmNzc2VzY2FwZSxmY3NzZXNjYXBlKTt9ZWxzZXtjb250ZXh0LnNldEF0dHJpYnV0ZShcImlkXCIsKG5pZD1leHBhbmRvKSk7fX1cbmdyb3Vwcz10b2tlbml6ZShzZWxlY3Rvcik7aT1ncm91cHMubGVuZ3RoO3doaWxlKGktLSl7Z3JvdXBzW2ldPShuaWQ/XCIjXCIrbmlkOlwiOnNjb3BlXCIpK1wiIFwiK1xudG9TZWxlY3Rvcihncm91cHNbaV0pO31cbm5ld1NlbGVjdG9yPWdyb3Vwcy5qb2luKFwiLFwiKTt9XG50cnl7cHVzaC5hcHBseShyZXN1bHRzLG5ld0NvbnRleHQucXVlcnlTZWxlY3RvckFsbChuZXdTZWxlY3RvcikpO3JldHVybiByZXN1bHRzO31jYXRjaChxc2FFcnJvcil7bm9ubmF0aXZlU2VsZWN0b3JDYWNoZShzZWxlY3Rvcix0cnVlKTt9ZmluYWxseXtpZihuaWQ9PT1leHBhbmRvKXtjb250ZXh0LnJlbW92ZUF0dHJpYnV0ZShcImlkXCIpO319fX19XG5yZXR1cm4gc2VsZWN0KHNlbGVjdG9yLnJlcGxhY2UocnRyaW0sXCIkMVwiKSxjb250ZXh0LHJlc3VsdHMsc2VlZCk7fVxuZnVuY3Rpb24gY3JlYXRlQ2FjaGUoKXt2YXIga2V5cz1bXTtmdW5jdGlvbiBjYWNoZShrZXksdmFsdWUpe2lmKGtleXMucHVzaChrZXkrXCIgXCIpPkV4cHIuY2FjaGVMZW5ndGgpe2RlbGV0ZSBjYWNoZVtrZXlzLnNoaWZ0KCldO31cbnJldHVybihjYWNoZVtrZXkrXCIgXCJdPXZhbHVlKTt9XG5yZXR1cm4gY2FjaGU7fVxuZnVuY3Rpb24gbWFya0Z1bmN0aW9uKGZuKXtmbltleHBhbmRvXT10cnVlO3JldHVybiBmbjt9XG5mdW5jdGlvbiBhc3NlcnQoZm4pe3ZhciBlbD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZmllbGRzZXRcIik7dHJ5e3JldHVybiEhZm4oZWwpO31jYXRjaChlKXtyZXR1cm4gZmFsc2U7fWZpbmFsbHl7aWYoZWwucGFyZW50Tm9kZSl7ZWwucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChlbCk7fVxuZWw9bnVsbDt9fVxuZnVuY3Rpb24gYWRkSGFuZGxlKGF0dHJzLGhhbmRsZXIpe3ZhciBhcnI9YXR0cnMuc3BsaXQoXCJ8XCIpLGk9YXJyLmxlbmd0aDt3aGlsZShpLS0pe0V4cHIuYXR0ckhhbmRsZVthcnJbaV1dPWhhbmRsZXI7fX1cbmZ1bmN0aW9uIHNpYmxpbmdDaGVjayhhLGIpe3ZhciBjdXI9YiYmYSxkaWZmPWN1ciYmYS5ub2RlVHlwZT09PTEmJmIubm9kZVR5cGU9PT0xJiZhLnNvdXJjZUluZGV4LWIuc291cmNlSW5kZXg7aWYoZGlmZil7cmV0dXJuIGRpZmY7fVxuaWYoY3VyKXt3aGlsZSgoY3VyPWN1ci5uZXh0U2libGluZykpe2lmKGN1cj09PWIpe3JldHVybi0xO319fVxucmV0dXJuIGE/MTotMTt9XG5mdW5jdGlvbiBjcmVhdGVJbnB1dFBzZXVkbyh0eXBlKXtyZXR1cm4gZnVuY3Rpb24oZWxlbSl7dmFyIG5hbWU9ZWxlbS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpO3JldHVybiBuYW1lPT09XCJpbnB1dFwiJiZlbGVtLnR5cGU9PT10eXBlO307fVxuZnVuY3Rpb24gY3JlYXRlQnV0dG9uUHNldWRvKHR5cGUpe3JldHVybiBmdW5jdGlvbihlbGVtKXt2YXIgbmFtZT1lbGVtLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk7cmV0dXJuKG5hbWU9PT1cImlucHV0XCJ8fG5hbWU9PT1cImJ1dHRvblwiKSYmZWxlbS50eXBlPT09dHlwZTt9O31cbmZ1bmN0aW9uIGNyZWF0ZURpc2FibGVkUHNldWRvKGRpc2FibGVkKXtyZXR1cm4gZnVuY3Rpb24oZWxlbSl7aWYoXCJmb3JtXCJpbiBlbGVtKXtpZihlbGVtLnBhcmVudE5vZGUmJmVsZW0uZGlzYWJsZWQ9PT1mYWxzZSl7aWYoXCJsYWJlbFwiaW4gZWxlbSl7aWYoXCJsYWJlbFwiaW4gZWxlbS5wYXJlbnROb2RlKXtyZXR1cm4gZWxlbS5wYXJlbnROb2RlLmRpc2FibGVkPT09ZGlzYWJsZWQ7fWVsc2V7cmV0dXJuIGVsZW0uZGlzYWJsZWQ9PT1kaXNhYmxlZDt9fVxucmV0dXJuIGVsZW0uaXNEaXNhYmxlZD09PWRpc2FibGVkfHxlbGVtLmlzRGlzYWJsZWQhPT0hZGlzYWJsZWQmJmluRGlzYWJsZWRGaWVsZHNldChlbGVtKT09PWRpc2FibGVkO31cbnJldHVybiBlbGVtLmRpc2FibGVkPT09ZGlzYWJsZWQ7fWVsc2UgaWYoXCJsYWJlbFwiaW4gZWxlbSl7cmV0dXJuIGVsZW0uZGlzYWJsZWQ9PT1kaXNhYmxlZDt9XG5yZXR1cm4gZmFsc2U7fTt9XG5mdW5jdGlvbiBjcmVhdGVQb3NpdGlvbmFsUHNldWRvKGZuKXtyZXR1cm4gbWFya0Z1bmN0aW9uKGZ1bmN0aW9uKGFyZ3VtZW50KXthcmd1bWVudD0rYXJndW1lbnQ7cmV0dXJuIG1hcmtGdW5jdGlvbihmdW5jdGlvbihzZWVkLG1hdGNoZXMpe3ZhciBqLG1hdGNoSW5kZXhlcz1mbihbXSxzZWVkLmxlbmd0aCxhcmd1bWVudCksaT1tYXRjaEluZGV4ZXMubGVuZ3RoO3doaWxlKGktLSl7aWYoc2VlZFsoaj1tYXRjaEluZGV4ZXNbaV0pXSl7c2VlZFtqXT0hKG1hdGNoZXNbal09c2VlZFtqXSk7fX19KTt9KTt9XG5mdW5jdGlvbiB0ZXN0Q29udGV4dChjb250ZXh0KXtyZXR1cm4gY29udGV4dCYmdHlwZW9mIGNvbnRleHQuZ2V0RWxlbWVudHNCeVRhZ05hbWUhPT1cInVuZGVmaW5lZFwiJiZjb250ZXh0O31cbnN1cHBvcnQ9U2l6emxlLnN1cHBvcnQ9e307aXNYTUw9U2l6emxlLmlzWE1MPWZ1bmN0aW9uKGVsZW0pe3ZhciBuYW1lc3BhY2U9ZWxlbS5uYW1lc3BhY2VVUkksZG9jRWxlbT0oZWxlbS5vd25lckRvY3VtZW50fHxlbGVtKS5kb2N1bWVudEVsZW1lbnQ7cmV0dXJuIXJodG1sLnRlc3QobmFtZXNwYWNlfHxkb2NFbGVtJiZkb2NFbGVtLm5vZGVOYW1lfHxcIkhUTUxcIik7fTtzZXREb2N1bWVudD1TaXp6bGUuc2V0RG9jdW1lbnQ9ZnVuY3Rpb24obm9kZSl7dmFyIGhhc0NvbXBhcmUsc3ViV2luZG93LGRvYz1ub2RlP25vZGUub3duZXJEb2N1bWVudHx8bm9kZTpwcmVmZXJyZWREb2M7aWYoZG9jPT1kb2N1bWVudHx8ZG9jLm5vZGVUeXBlIT09OXx8IWRvYy5kb2N1bWVudEVsZW1lbnQpe3JldHVybiBkb2N1bWVudDt9XG5kb2N1bWVudD1kb2M7ZG9jRWxlbT1kb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7ZG9jdW1lbnRJc0hUTUw9IWlzWE1MKGRvY3VtZW50KTtpZihwcmVmZXJyZWREb2MhPWRvY3VtZW50JiYoc3ViV2luZG93PWRvY3VtZW50LmRlZmF1bHRWaWV3KSYmc3ViV2luZG93LnRvcCE9PXN1YldpbmRvdyl7aWYoc3ViV2luZG93LmFkZEV2ZW50TGlzdGVuZXIpe3N1YldpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwidW5sb2FkXCIsdW5sb2FkSGFuZGxlcixmYWxzZSk7fWVsc2UgaWYoc3ViV2luZG93LmF0dGFjaEV2ZW50KXtzdWJXaW5kb3cuYXR0YWNoRXZlbnQoXCJvbnVubG9hZFwiLHVubG9hZEhhbmRsZXIpO319XG5zdXBwb3J0LnNjb3BlPWFzc2VydChmdW5jdGlvbihlbCl7ZG9jRWxlbS5hcHBlbmRDaGlsZChlbCkuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKSk7cmV0dXJuIHR5cGVvZiBlbC5xdWVyeVNlbGVjdG9yQWxsIT09XCJ1bmRlZmluZWRcIiYmIWVsLnF1ZXJ5U2VsZWN0b3JBbGwoXCI6c2NvcGUgZmllbGRzZXQgZGl2XCIpLmxlbmd0aDt9KTtzdXBwb3J0LmF0dHJpYnV0ZXM9YXNzZXJ0KGZ1bmN0aW9uKGVsKXtlbC5jbGFzc05hbWU9XCJpXCI7cmV0dXJuIWVsLmdldEF0dHJpYnV0ZShcImNsYXNzTmFtZVwiKTt9KTtzdXBwb3J0LmdldEVsZW1lbnRzQnlUYWdOYW1lPWFzc2VydChmdW5jdGlvbihlbCl7ZWwuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlQ29tbWVudChcIlwiKSk7cmV0dXJuIWVsLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiKlwiKS5sZW5ndGg7fSk7c3VwcG9ydC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lPXJuYXRpdmUudGVzdChkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKTtzdXBwb3J0LmdldEJ5SWQ9YXNzZXJ0KGZ1bmN0aW9uKGVsKXtkb2NFbGVtLmFwcGVuZENoaWxkKGVsKS5pZD1leHBhbmRvO3JldHVybiFkb2N1bWVudC5nZXRFbGVtZW50c0J5TmFtZXx8IWRvY3VtZW50LmdldEVsZW1lbnRzQnlOYW1lKGV4cGFuZG8pLmxlbmd0aDt9KTtpZihzdXBwb3J0LmdldEJ5SWQpe0V4cHIuZmlsdGVyW1wiSURcIl09ZnVuY3Rpb24oaWQpe3ZhciBhdHRySWQ9aWQucmVwbGFjZShydW5lc2NhcGUsZnVuZXNjYXBlKTtyZXR1cm4gZnVuY3Rpb24oZWxlbSl7cmV0dXJuIGVsZW0uZ2V0QXR0cmlidXRlKFwiaWRcIik9PT1hdHRySWQ7fTt9O0V4cHIuZmluZFtcIklEXCJdPWZ1bmN0aW9uKGlkLGNvbnRleHQpe2lmKHR5cGVvZiBjb250ZXh0LmdldEVsZW1lbnRCeUlkIT09XCJ1bmRlZmluZWRcIiYmZG9jdW1lbnRJc0hUTUwpe3ZhciBlbGVtPWNvbnRleHQuZ2V0RWxlbWVudEJ5SWQoaWQpO3JldHVybiBlbGVtP1tlbGVtXTpbXTt9fTt9ZWxzZXtFeHByLmZpbHRlcltcIklEXCJdPWZ1bmN0aW9uKGlkKXt2YXIgYXR0cklkPWlkLnJlcGxhY2UocnVuZXNjYXBlLGZ1bmVzY2FwZSk7cmV0dXJuIGZ1bmN0aW9uKGVsZW0pe3ZhciBub2RlPXR5cGVvZiBlbGVtLmdldEF0dHJpYnV0ZU5vZGUhPT1cInVuZGVmaW5lZFwiJiZlbGVtLmdldEF0dHJpYnV0ZU5vZGUoXCJpZFwiKTtyZXR1cm4gbm9kZSYmbm9kZS52YWx1ZT09PWF0dHJJZDt9O307RXhwci5maW5kW1wiSURcIl09ZnVuY3Rpb24oaWQsY29udGV4dCl7aWYodHlwZW9mIGNvbnRleHQuZ2V0RWxlbWVudEJ5SWQhPT1cInVuZGVmaW5lZFwiJiZkb2N1bWVudElzSFRNTCl7dmFyIG5vZGUsaSxlbGVtcyxlbGVtPWNvbnRleHQuZ2V0RWxlbWVudEJ5SWQoaWQpO2lmKGVsZW0pe25vZGU9ZWxlbS5nZXRBdHRyaWJ1dGVOb2RlKFwiaWRcIik7aWYobm9kZSYmbm9kZS52YWx1ZT09PWlkKXtyZXR1cm5bZWxlbV07fVxuZWxlbXM9Y29udGV4dC5nZXRFbGVtZW50c0J5TmFtZShpZCk7aT0wO3doaWxlKChlbGVtPWVsZW1zW2krK10pKXtub2RlPWVsZW0uZ2V0QXR0cmlidXRlTm9kZShcImlkXCIpO2lmKG5vZGUmJm5vZGUudmFsdWU9PT1pZCl7cmV0dXJuW2VsZW1dO319fVxucmV0dXJuW107fX07fVxuRXhwci5maW5kW1wiVEFHXCJdPXN1cHBvcnQuZ2V0RWxlbWVudHNCeVRhZ05hbWU/ZnVuY3Rpb24odGFnLGNvbnRleHQpe2lmKHR5cGVvZiBjb250ZXh0LmdldEVsZW1lbnRzQnlUYWdOYW1lIT09XCJ1bmRlZmluZWRcIil7cmV0dXJuIGNvbnRleHQuZ2V0RWxlbWVudHNCeVRhZ05hbWUodGFnKTt9ZWxzZSBpZihzdXBwb3J0LnFzYSl7cmV0dXJuIGNvbnRleHQucXVlcnlTZWxlY3RvckFsbCh0YWcpO319OmZ1bmN0aW9uKHRhZyxjb250ZXh0KXt2YXIgZWxlbSx0bXA9W10saT0wLHJlc3VsdHM9Y29udGV4dC5nZXRFbGVtZW50c0J5VGFnTmFtZSh0YWcpO2lmKHRhZz09PVwiKlwiKXt3aGlsZSgoZWxlbT1yZXN1bHRzW2krK10pKXtpZihlbGVtLm5vZGVUeXBlPT09MSl7dG1wLnB1c2goZWxlbSk7fX1cbnJldHVybiB0bXA7fVxucmV0dXJuIHJlc3VsdHM7fTtFeHByLmZpbmRbXCJDTEFTU1wiXT1zdXBwb3J0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUmJmZ1bmN0aW9uKGNsYXNzTmFtZSxjb250ZXh0KXtpZih0eXBlb2YgY29udGV4dC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lIT09XCJ1bmRlZmluZWRcIiYmZG9jdW1lbnRJc0hUTUwpe3JldHVybiBjb250ZXh0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoY2xhc3NOYW1lKTt9fTtyYnVnZ3lNYXRjaGVzPVtdO3JidWdneVFTQT1bXTtpZigoc3VwcG9ydC5xc2E9cm5hdGl2ZS50ZXN0KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwpKSl7YXNzZXJ0KGZ1bmN0aW9uKGVsKXt2YXIgaW5wdXQ7ZG9jRWxlbS5hcHBlbmRDaGlsZChlbCkuaW5uZXJIVE1MPVwiPGEgaWQ9J1wiK2V4cGFuZG8rXCInPjwvYT5cIitcIjxzZWxlY3QgaWQ9J1wiK2V4cGFuZG8rXCItXFxyXFxcXCcgbXNhbGxvd2NhcHR1cmU9Jyc+XCIrXCI8b3B0aW9uIHNlbGVjdGVkPScnPjwvb3B0aW9uPjwvc2VsZWN0PlwiO2lmKGVsLnF1ZXJ5U2VsZWN0b3JBbGwoXCJbbXNhbGxvd2NhcHR1cmVePScnXVwiKS5sZW5ndGgpe3JidWdneVFTQS5wdXNoKFwiWypeJF09XCIrd2hpdGVzcGFjZStcIiooPzonJ3xcXFwiXFxcIilcIik7fVxuaWYoIWVsLnF1ZXJ5U2VsZWN0b3JBbGwoXCJbc2VsZWN0ZWRdXCIpLmxlbmd0aCl7cmJ1Z2d5UVNBLnB1c2goXCJcXFxcW1wiK3doaXRlc3BhY2UrXCIqKD86dmFsdWV8XCIrYm9vbGVhbnMrXCIpXCIpO31cbmlmKCFlbC5xdWVyeVNlbGVjdG9yQWxsKFwiW2lkfj1cIitleHBhbmRvK1wiLV1cIikubGVuZ3RoKXtyYnVnZ3lRU0EucHVzaChcIn49XCIpO31cbmlucHV0PWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiKTtpbnB1dC5zZXRBdHRyaWJ1dGUoXCJuYW1lXCIsXCJcIik7ZWwuYXBwZW5kQ2hpbGQoaW5wdXQpO2lmKCFlbC5xdWVyeVNlbGVjdG9yQWxsKFwiW25hbWU9JyddXCIpLmxlbmd0aCl7cmJ1Z2d5UVNBLnB1c2goXCJcXFxcW1wiK3doaXRlc3BhY2UrXCIqbmFtZVwiK3doaXRlc3BhY2UrXCIqPVwiK1xud2hpdGVzcGFjZStcIiooPzonJ3xcXFwiXFxcIilcIik7fVxuaWYoIWVsLnF1ZXJ5U2VsZWN0b3JBbGwoXCI6Y2hlY2tlZFwiKS5sZW5ndGgpe3JidWdneVFTQS5wdXNoKFwiOmNoZWNrZWRcIik7fVxuaWYoIWVsLnF1ZXJ5U2VsZWN0b3JBbGwoXCJhI1wiK2V4cGFuZG8rXCIrKlwiKS5sZW5ndGgpe3JidWdneVFTQS5wdXNoKFwiLiMuK1srfl1cIik7fVxuZWwucXVlcnlTZWxlY3RvckFsbChcIlxcXFxcXGZcIik7cmJ1Z2d5UVNBLnB1c2goXCJbXFxcXHJcXFxcblxcXFxmXVwiKTt9KTthc3NlcnQoZnVuY3Rpb24oZWwpe2VsLmlubmVySFRNTD1cIjxhIGhyZWY9JycgZGlzYWJsZWQ9J2Rpc2FibGVkJz48L2E+XCIrXCI8c2VsZWN0IGRpc2FibGVkPSdkaXNhYmxlZCc+PG9wdGlvbi8+PC9zZWxlY3Q+XCI7dmFyIGlucHV0PWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiKTtpbnB1dC5zZXRBdHRyaWJ1dGUoXCJ0eXBlXCIsXCJoaWRkZW5cIik7ZWwuYXBwZW5kQ2hpbGQoaW5wdXQpLnNldEF0dHJpYnV0ZShcIm5hbWVcIixcIkRcIik7aWYoZWwucXVlcnlTZWxlY3RvckFsbChcIltuYW1lPWRdXCIpLmxlbmd0aCl7cmJ1Z2d5UVNBLnB1c2goXCJuYW1lXCIrd2hpdGVzcGFjZStcIipbKl4kfCF+XT89XCIpO31cbmlmKGVsLnF1ZXJ5U2VsZWN0b3JBbGwoXCI6ZW5hYmxlZFwiKS5sZW5ndGghPT0yKXtyYnVnZ3lRU0EucHVzaChcIjplbmFibGVkXCIsXCI6ZGlzYWJsZWRcIik7fVxuZG9jRWxlbS5hcHBlbmRDaGlsZChlbCkuZGlzYWJsZWQ9dHJ1ZTtpZihlbC5xdWVyeVNlbGVjdG9yQWxsKFwiOmRpc2FibGVkXCIpLmxlbmd0aCE9PTIpe3JidWdneVFTQS5wdXNoKFwiOmVuYWJsZWRcIixcIjpkaXNhYmxlZFwiKTt9XG5lbC5xdWVyeVNlbGVjdG9yQWxsKFwiKiw6eFwiKTtyYnVnZ3lRU0EucHVzaChcIiwuKjpcIik7fSk7fVxuaWYoKHN1cHBvcnQubWF0Y2hlc1NlbGVjdG9yPXJuYXRpdmUudGVzdCgobWF0Y2hlcz1kb2NFbGVtLm1hdGNoZXN8fGRvY0VsZW0ud2Via2l0TWF0Y2hlc1NlbGVjdG9yfHxkb2NFbGVtLm1vek1hdGNoZXNTZWxlY3Rvcnx8ZG9jRWxlbS5vTWF0Y2hlc1NlbGVjdG9yfHxkb2NFbGVtLm1zTWF0Y2hlc1NlbGVjdG9yKSkpKXthc3NlcnQoZnVuY3Rpb24oZWwpe3N1cHBvcnQuZGlzY29ubmVjdGVkTWF0Y2g9bWF0Y2hlcy5jYWxsKGVsLFwiKlwiKTttYXRjaGVzLmNhbGwoZWwsXCJbcyE9JyddOnhcIik7cmJ1Z2d5TWF0Y2hlcy5wdXNoKFwiIT1cIixwc2V1ZG9zKTt9KTt9XG5yYnVnZ3lRU0E9cmJ1Z2d5UVNBLmxlbmd0aCYmbmV3IFJlZ0V4cChyYnVnZ3lRU0Euam9pbihcInxcIikpO3JidWdneU1hdGNoZXM9cmJ1Z2d5TWF0Y2hlcy5sZW5ndGgmJm5ldyBSZWdFeHAocmJ1Z2d5TWF0Y2hlcy5qb2luKFwifFwiKSk7aGFzQ29tcGFyZT1ybmF0aXZlLnRlc3QoZG9jRWxlbS5jb21wYXJlRG9jdW1lbnRQb3NpdGlvbik7Y29udGFpbnM9aGFzQ29tcGFyZXx8cm5hdGl2ZS50ZXN0KGRvY0VsZW0uY29udGFpbnMpP2Z1bmN0aW9uKGEsYil7dmFyIGFkb3duPWEubm9kZVR5cGU9PT05P2EuZG9jdW1lbnRFbGVtZW50OmEsYnVwPWImJmIucGFyZW50Tm9kZTtyZXR1cm4gYT09PWJ1cHx8ISEoYnVwJiZidXAubm9kZVR5cGU9PT0xJiYoYWRvd24uY29udGFpbnM/YWRvd24uY29udGFpbnMoYnVwKTphLmNvbXBhcmVEb2N1bWVudFBvc2l0aW9uJiZhLmNvbXBhcmVEb2N1bWVudFBvc2l0aW9uKGJ1cCkmMTYpKTt9OmZ1bmN0aW9uKGEsYil7aWYoYil7d2hpbGUoKGI9Yi5wYXJlbnROb2RlKSl7aWYoYj09PWEpe3JldHVybiB0cnVlO319fVxucmV0dXJuIGZhbHNlO307c29ydE9yZGVyPWhhc0NvbXBhcmU/ZnVuY3Rpb24oYSxiKXtpZihhPT09Yil7aGFzRHVwbGljYXRlPXRydWU7cmV0dXJuIDA7fVxudmFyIGNvbXBhcmU9IWEuY29tcGFyZURvY3VtZW50UG9zaXRpb24tIWIuY29tcGFyZURvY3VtZW50UG9zaXRpb247aWYoY29tcGFyZSl7cmV0dXJuIGNvbXBhcmU7fVxuY29tcGFyZT0oYS5vd25lckRvY3VtZW50fHxhKT09KGIub3duZXJEb2N1bWVudHx8Yik/YS5jb21wYXJlRG9jdW1lbnRQb3NpdGlvbihiKToxO2lmKGNvbXBhcmUmMXx8KCFzdXBwb3J0LnNvcnREZXRhY2hlZCYmYi5jb21wYXJlRG9jdW1lbnRQb3NpdGlvbihhKT09PWNvbXBhcmUpKXtpZihhPT1kb2N1bWVudHx8YS5vd25lckRvY3VtZW50PT1wcmVmZXJyZWREb2MmJmNvbnRhaW5zKHByZWZlcnJlZERvYyxhKSl7cmV0dXJuLTE7fVxuaWYoYj09ZG9jdW1lbnR8fGIub3duZXJEb2N1bWVudD09cHJlZmVycmVkRG9jJiZjb250YWlucyhwcmVmZXJyZWREb2MsYikpe3JldHVybiAxO31cbnJldHVybiBzb3J0SW5wdXQ/KGluZGV4T2Yoc29ydElucHV0LGEpLWluZGV4T2Yoc29ydElucHV0LGIpKTowO31cbnJldHVybiBjb21wYXJlJjQ/LTE6MTt9OmZ1bmN0aW9uKGEsYil7aWYoYT09PWIpe2hhc0R1cGxpY2F0ZT10cnVlO3JldHVybiAwO31cbnZhciBjdXIsaT0wLGF1cD1hLnBhcmVudE5vZGUsYnVwPWIucGFyZW50Tm9kZSxhcD1bYV0sYnA9W2JdO2lmKCFhdXB8fCFidXApe3JldHVybiBhPT1kb2N1bWVudD8tMTpiPT1kb2N1bWVudD8xOmF1cD8tMTpidXA/MTpzb3J0SW5wdXQ/KGluZGV4T2Yoc29ydElucHV0LGEpLWluZGV4T2Yoc29ydElucHV0LGIpKTowO31lbHNlIGlmKGF1cD09PWJ1cCl7cmV0dXJuIHNpYmxpbmdDaGVjayhhLGIpO31cbmN1cj1hO3doaWxlKChjdXI9Y3VyLnBhcmVudE5vZGUpKXthcC51bnNoaWZ0KGN1cik7fVxuY3VyPWI7d2hpbGUoKGN1cj1jdXIucGFyZW50Tm9kZSkpe2JwLnVuc2hpZnQoY3VyKTt9XG53aGlsZShhcFtpXT09PWJwW2ldKXtpKys7fVxucmV0dXJuIGk/c2libGluZ0NoZWNrKGFwW2ldLGJwW2ldKTphcFtpXT09cHJlZmVycmVkRG9jPy0xOmJwW2ldPT1wcmVmZXJyZWREb2M/MTowO307cmV0dXJuIGRvY3VtZW50O307U2l6emxlLm1hdGNoZXM9ZnVuY3Rpb24oZXhwcixlbGVtZW50cyl7cmV0dXJuIFNpenpsZShleHByLG51bGwsbnVsbCxlbGVtZW50cyk7fTtTaXp6bGUubWF0Y2hlc1NlbGVjdG9yPWZ1bmN0aW9uKGVsZW0sZXhwcil7c2V0RG9jdW1lbnQoZWxlbSk7aWYoc3VwcG9ydC5tYXRjaGVzU2VsZWN0b3ImJmRvY3VtZW50SXNIVE1MJiYhbm9ubmF0aXZlU2VsZWN0b3JDYWNoZVtleHByK1wiIFwiXSYmKCFyYnVnZ3lNYXRjaGVzfHwhcmJ1Z2d5TWF0Y2hlcy50ZXN0KGV4cHIpKSYmKCFyYnVnZ3lRU0F8fCFyYnVnZ3lRU0EudGVzdChleHByKSkpe3RyeXt2YXIgcmV0PW1hdGNoZXMuY2FsbChlbGVtLGV4cHIpO2lmKHJldHx8c3VwcG9ydC5kaXNjb25uZWN0ZWRNYXRjaHx8ZWxlbS5kb2N1bWVudCYmZWxlbS5kb2N1bWVudC5ub2RlVHlwZSE9PTExKXtyZXR1cm4gcmV0O319Y2F0Y2goZSl7bm9ubmF0aXZlU2VsZWN0b3JDYWNoZShleHByLHRydWUpO319XG5yZXR1cm4gU2l6emxlKGV4cHIsZG9jdW1lbnQsbnVsbCxbZWxlbV0pLmxlbmd0aD4wO307U2l6emxlLmNvbnRhaW5zPWZ1bmN0aW9uKGNvbnRleHQsZWxlbSl7aWYoKGNvbnRleHQub3duZXJEb2N1bWVudHx8Y29udGV4dCkhPWRvY3VtZW50KXtzZXREb2N1bWVudChjb250ZXh0KTt9XG5yZXR1cm4gY29udGFpbnMoY29udGV4dCxlbGVtKTt9O1NpenpsZS5hdHRyPWZ1bmN0aW9uKGVsZW0sbmFtZSl7aWYoKGVsZW0ub3duZXJEb2N1bWVudHx8ZWxlbSkhPWRvY3VtZW50KXtzZXREb2N1bWVudChlbGVtKTt9XG52YXIgZm49RXhwci5hdHRySGFuZGxlW25hbWUudG9Mb3dlckNhc2UoKV0sdmFsPWZuJiZoYXNPd24uY2FsbChFeHByLmF0dHJIYW5kbGUsbmFtZS50b0xvd2VyQ2FzZSgpKT9mbihlbGVtLG5hbWUsIWRvY3VtZW50SXNIVE1MKTp1bmRlZmluZWQ7cmV0dXJuIHZhbCE9PXVuZGVmaW5lZD92YWw6c3VwcG9ydC5hdHRyaWJ1dGVzfHwhZG9jdW1lbnRJc0hUTUw/ZWxlbS5nZXRBdHRyaWJ1dGUobmFtZSk6KHZhbD1lbGVtLmdldEF0dHJpYnV0ZU5vZGUobmFtZSkpJiZ2YWwuc3BlY2lmaWVkP3ZhbC52YWx1ZTpudWxsO307U2l6emxlLmVzY2FwZT1mdW5jdGlvbihzZWwpe3JldHVybihzZWwrXCJcIikucmVwbGFjZShyY3NzZXNjYXBlLGZjc3Nlc2NhcGUpO307U2l6emxlLmVycm9yPWZ1bmN0aW9uKG1zZyl7dGhyb3cgbmV3IEVycm9yKFwiU3ludGF4IGVycm9yLCB1bnJlY29nbml6ZWQgZXhwcmVzc2lvbjogXCIrbXNnKTt9O1NpenpsZS51bmlxdWVTb3J0PWZ1bmN0aW9uKHJlc3VsdHMpe3ZhciBlbGVtLGR1cGxpY2F0ZXM9W10saj0wLGk9MDtoYXNEdXBsaWNhdGU9IXN1cHBvcnQuZGV0ZWN0RHVwbGljYXRlcztzb3J0SW5wdXQ9IXN1cHBvcnQuc29ydFN0YWJsZSYmcmVzdWx0cy5zbGljZSgwKTtyZXN1bHRzLnNvcnQoc29ydE9yZGVyKTtpZihoYXNEdXBsaWNhdGUpe3doaWxlKChlbGVtPXJlc3VsdHNbaSsrXSkpe2lmKGVsZW09PT1yZXN1bHRzW2ldKXtqPWR1cGxpY2F0ZXMucHVzaChpKTt9fVxud2hpbGUoai0tKXtyZXN1bHRzLnNwbGljZShkdXBsaWNhdGVzW2pdLDEpO319XG5zb3J0SW5wdXQ9bnVsbDtyZXR1cm4gcmVzdWx0czt9O2dldFRleHQ9U2l6emxlLmdldFRleHQ9ZnVuY3Rpb24oZWxlbSl7dmFyIG5vZGUscmV0PVwiXCIsaT0wLG5vZGVUeXBlPWVsZW0ubm9kZVR5cGU7aWYoIW5vZGVUeXBlKXt3aGlsZSgobm9kZT1lbGVtW2krK10pKXtyZXQrPWdldFRleHQobm9kZSk7fX1lbHNlIGlmKG5vZGVUeXBlPT09MXx8bm9kZVR5cGU9PT05fHxub2RlVHlwZT09PTExKXtpZih0eXBlb2YgZWxlbS50ZXh0Q29udGVudD09PVwic3RyaW5nXCIpe3JldHVybiBlbGVtLnRleHRDb250ZW50O31lbHNle2ZvcihlbGVtPWVsZW0uZmlyc3RDaGlsZDtlbGVtO2VsZW09ZWxlbS5uZXh0U2libGluZyl7cmV0Kz1nZXRUZXh0KGVsZW0pO319fWVsc2UgaWYobm9kZVR5cGU9PT0zfHxub2RlVHlwZT09PTQpe3JldHVybiBlbGVtLm5vZGVWYWx1ZTt9XG5yZXR1cm4gcmV0O307RXhwcj1TaXp6bGUuc2VsZWN0b3JzPXtjYWNoZUxlbmd0aDo1MCxjcmVhdGVQc2V1ZG86bWFya0Z1bmN0aW9uLG1hdGNoOm1hdGNoRXhwcixhdHRySGFuZGxlOnt9LGZpbmQ6e30scmVsYXRpdmU6e1wiPlwiOntkaXI6XCJwYXJlbnROb2RlXCIsZmlyc3Q6dHJ1ZX0sXCIgXCI6e2RpcjpcInBhcmVudE5vZGVcIn0sXCIrXCI6e2RpcjpcInByZXZpb3VzU2libGluZ1wiLGZpcnN0OnRydWV9LFwiflwiOntkaXI6XCJwcmV2aW91c1NpYmxpbmdcIn19LHByZUZpbHRlcjp7XCJBVFRSXCI6ZnVuY3Rpb24obWF0Y2gpe21hdGNoWzFdPW1hdGNoWzFdLnJlcGxhY2UocnVuZXNjYXBlLGZ1bmVzY2FwZSk7bWF0Y2hbM109KG1hdGNoWzNdfHxtYXRjaFs0XXx8bWF0Y2hbNV18fFwiXCIpLnJlcGxhY2UocnVuZXNjYXBlLGZ1bmVzY2FwZSk7aWYobWF0Y2hbMl09PT1cIn49XCIpe21hdGNoWzNdPVwiIFwiK21hdGNoWzNdK1wiIFwiO31cbnJldHVybiBtYXRjaC5zbGljZSgwLDQpO30sXCJDSElMRFwiOmZ1bmN0aW9uKG1hdGNoKXttYXRjaFsxXT1tYXRjaFsxXS50b0xvd2VyQ2FzZSgpO2lmKG1hdGNoWzFdLnNsaWNlKDAsMyk9PT1cIm50aFwiKXtpZighbWF0Y2hbM10pe1NpenpsZS5lcnJvcihtYXRjaFswXSk7fVxubWF0Y2hbNF09KyhtYXRjaFs0XT9tYXRjaFs1XSsobWF0Y2hbNl18fDEpOjIqKG1hdGNoWzNdPT09XCJldmVuXCJ8fG1hdGNoWzNdPT09XCJvZGRcIikpO21hdGNoWzVdPSsoKG1hdGNoWzddK21hdGNoWzhdKXx8bWF0Y2hbM109PT1cIm9kZFwiKTt9ZWxzZSBpZihtYXRjaFszXSl7U2l6emxlLmVycm9yKG1hdGNoWzBdKTt9XG5yZXR1cm4gbWF0Y2g7fSxcIlBTRVVET1wiOmZ1bmN0aW9uKG1hdGNoKXt2YXIgZXhjZXNzLHVucXVvdGVkPSFtYXRjaFs2XSYmbWF0Y2hbMl07aWYobWF0Y2hFeHByW1wiQ0hJTERcIl0udGVzdChtYXRjaFswXSkpe3JldHVybiBudWxsO31cbmlmKG1hdGNoWzNdKXttYXRjaFsyXT1tYXRjaFs0XXx8bWF0Y2hbNV18fFwiXCI7fWVsc2UgaWYodW5xdW90ZWQmJnJwc2V1ZG8udGVzdCh1bnF1b3RlZCkmJihleGNlc3M9dG9rZW5pemUodW5xdW90ZWQsdHJ1ZSkpJiYoZXhjZXNzPXVucXVvdGVkLmluZGV4T2YoXCIpXCIsdW5xdW90ZWQubGVuZ3RoLWV4Y2VzcyktdW5xdW90ZWQubGVuZ3RoKSl7bWF0Y2hbMF09bWF0Y2hbMF0uc2xpY2UoMCxleGNlc3MpO21hdGNoWzJdPXVucXVvdGVkLnNsaWNlKDAsZXhjZXNzKTt9XG5yZXR1cm4gbWF0Y2guc2xpY2UoMCwzKTt9fSxmaWx0ZXI6e1wiVEFHXCI6ZnVuY3Rpb24obm9kZU5hbWVTZWxlY3Rvcil7dmFyIG5vZGVOYW1lPW5vZGVOYW1lU2VsZWN0b3IucmVwbGFjZShydW5lc2NhcGUsZnVuZXNjYXBlKS50b0xvd2VyQ2FzZSgpO3JldHVybiBub2RlTmFtZVNlbGVjdG9yPT09XCIqXCI/ZnVuY3Rpb24oKXtyZXR1cm4gdHJ1ZTt9OmZ1bmN0aW9uKGVsZW0pe3JldHVybiBlbGVtLm5vZGVOYW1lJiZlbGVtLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk9PT1ub2RlTmFtZTt9O30sXCJDTEFTU1wiOmZ1bmN0aW9uKGNsYXNzTmFtZSl7dmFyIHBhdHRlcm49Y2xhc3NDYWNoZVtjbGFzc05hbWUrXCIgXCJdO3JldHVybiBwYXR0ZXJufHwocGF0dGVybj1uZXcgUmVnRXhwKFwiKF58XCIrd2hpdGVzcGFjZStcIilcIitjbGFzc05hbWUrXCIoXCIrd2hpdGVzcGFjZStcInwkKVwiKSkmJmNsYXNzQ2FjaGUoY2xhc3NOYW1lLGZ1bmN0aW9uKGVsZW0pe3JldHVybiBwYXR0ZXJuLnRlc3QodHlwZW9mIGVsZW0uY2xhc3NOYW1lPT09XCJzdHJpbmdcIiYmZWxlbS5jbGFzc05hbWV8fHR5cGVvZiBlbGVtLmdldEF0dHJpYnV0ZSE9PVwidW5kZWZpbmVkXCImJmVsZW0uZ2V0QXR0cmlidXRlKFwiY2xhc3NcIil8fFwiXCIpO30pO30sXCJBVFRSXCI6ZnVuY3Rpb24obmFtZSxvcGVyYXRvcixjaGVjayl7cmV0dXJuIGZ1bmN0aW9uKGVsZW0pe3ZhciByZXN1bHQ9U2l6emxlLmF0dHIoZWxlbSxuYW1lKTtpZihyZXN1bHQ9PW51bGwpe3JldHVybiBvcGVyYXRvcj09PVwiIT1cIjt9XG5pZighb3BlcmF0b3Ipe3JldHVybiB0cnVlO31cbnJlc3VsdCs9XCJcIjtyZXR1cm4gb3BlcmF0b3I9PT1cIj1cIj9yZXN1bHQ9PT1jaGVjazpvcGVyYXRvcj09PVwiIT1cIj9yZXN1bHQhPT1jaGVjazpvcGVyYXRvcj09PVwiXj1cIj9jaGVjayYmcmVzdWx0LmluZGV4T2YoY2hlY2spPT09MDpvcGVyYXRvcj09PVwiKj1cIj9jaGVjayYmcmVzdWx0LmluZGV4T2YoY2hlY2spPi0xOm9wZXJhdG9yPT09XCIkPVwiP2NoZWNrJiZyZXN1bHQuc2xpY2UoLWNoZWNrLmxlbmd0aCk9PT1jaGVjazpvcGVyYXRvcj09PVwifj1cIj8oXCIgXCIrcmVzdWx0LnJlcGxhY2UocndoaXRlc3BhY2UsXCIgXCIpK1wiIFwiKS5pbmRleE9mKGNoZWNrKT4tMTpvcGVyYXRvcj09PVwifD1cIj9yZXN1bHQ9PT1jaGVja3x8cmVzdWx0LnNsaWNlKDAsY2hlY2subGVuZ3RoKzEpPT09Y2hlY2srXCItXCI6ZmFsc2U7fTt9LFwiQ0hJTERcIjpmdW5jdGlvbih0eXBlLHdoYXQsX2FyZ3VtZW50LGZpcnN0LGxhc3Qpe3ZhciBzaW1wbGU9dHlwZS5zbGljZSgwLDMpIT09XCJudGhcIixmb3J3YXJkPXR5cGUuc2xpY2UoLTQpIT09XCJsYXN0XCIsb2ZUeXBlPXdoYXQ9PT1cIm9mLXR5cGVcIjtyZXR1cm4gZmlyc3Q9PT0xJiZsYXN0PT09MD9mdW5jdGlvbihlbGVtKXtyZXR1cm4hIWVsZW0ucGFyZW50Tm9kZTt9OmZ1bmN0aW9uKGVsZW0sX2NvbnRleHQseG1sKXt2YXIgY2FjaGUsdW5pcXVlQ2FjaGUsb3V0ZXJDYWNoZSxub2RlLG5vZGVJbmRleCxzdGFydCxkaXI9c2ltcGxlIT09Zm9yd2FyZD9cIm5leHRTaWJsaW5nXCI6XCJwcmV2aW91c1NpYmxpbmdcIixwYXJlbnQ9ZWxlbS5wYXJlbnROb2RlLG5hbWU9b2ZUeXBlJiZlbGVtLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCksdXNlQ2FjaGU9IXhtbCYmIW9mVHlwZSxkaWZmPWZhbHNlO2lmKHBhcmVudCl7aWYoc2ltcGxlKXt3aGlsZShkaXIpe25vZGU9ZWxlbTt3aGlsZSgobm9kZT1ub2RlW2Rpcl0pKXtpZihvZlR5cGU/bm9kZS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpPT09bmFtZTpub2RlLm5vZGVUeXBlPT09MSl7cmV0dXJuIGZhbHNlO319XG5zdGFydD1kaXI9dHlwZT09PVwib25seVwiJiYhc3RhcnQmJlwibmV4dFNpYmxpbmdcIjt9XG5yZXR1cm4gdHJ1ZTt9XG5zdGFydD1bZm9yd2FyZD9wYXJlbnQuZmlyc3RDaGlsZDpwYXJlbnQubGFzdENoaWxkXTtpZihmb3J3YXJkJiZ1c2VDYWNoZSl7bm9kZT1wYXJlbnQ7b3V0ZXJDYWNoZT1ub2RlW2V4cGFuZG9dfHwobm9kZVtleHBhbmRvXT17fSk7dW5pcXVlQ2FjaGU9b3V0ZXJDYWNoZVtub2RlLnVuaXF1ZUlEXXx8KG91dGVyQ2FjaGVbbm9kZS51bmlxdWVJRF09e30pO2NhY2hlPXVuaXF1ZUNhY2hlW3R5cGVdfHxbXTtub2RlSW5kZXg9Y2FjaGVbMF09PT1kaXJydW5zJiZjYWNoZVsxXTtkaWZmPW5vZGVJbmRleCYmY2FjaGVbMl07bm9kZT1ub2RlSW5kZXgmJnBhcmVudC5jaGlsZE5vZGVzW25vZGVJbmRleF07d2hpbGUoKG5vZGU9Kytub2RlSW5kZXgmJm5vZGUmJm5vZGVbZGlyXXx8KGRpZmY9bm9kZUluZGV4PTApfHxzdGFydC5wb3AoKSkpe2lmKG5vZGUubm9kZVR5cGU9PT0xJiYrK2RpZmYmJm5vZGU9PT1lbGVtKXt1bmlxdWVDYWNoZVt0eXBlXT1bZGlycnVucyxub2RlSW5kZXgsZGlmZl07YnJlYWs7fX19ZWxzZXtpZih1c2VDYWNoZSl7bm9kZT1lbGVtO291dGVyQ2FjaGU9bm9kZVtleHBhbmRvXXx8KG5vZGVbZXhwYW5kb109e30pO3VuaXF1ZUNhY2hlPW91dGVyQ2FjaGVbbm9kZS51bmlxdWVJRF18fChvdXRlckNhY2hlW25vZGUudW5pcXVlSURdPXt9KTtjYWNoZT11bmlxdWVDYWNoZVt0eXBlXXx8W107bm9kZUluZGV4PWNhY2hlWzBdPT09ZGlycnVucyYmY2FjaGVbMV07ZGlmZj1ub2RlSW5kZXg7fVxuaWYoZGlmZj09PWZhbHNlKXt3aGlsZSgobm9kZT0rK25vZGVJbmRleCYmbm9kZSYmbm9kZVtkaXJdfHwoZGlmZj1ub2RlSW5kZXg9MCl8fHN0YXJ0LnBvcCgpKSl7aWYoKG9mVHlwZT9ub2RlLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk9PT1uYW1lOm5vZGUubm9kZVR5cGU9PT0xKSYmKytkaWZmKXtpZih1c2VDYWNoZSl7b3V0ZXJDYWNoZT1ub2RlW2V4cGFuZG9dfHwobm9kZVtleHBhbmRvXT17fSk7dW5pcXVlQ2FjaGU9b3V0ZXJDYWNoZVtub2RlLnVuaXF1ZUlEXXx8KG91dGVyQ2FjaGVbbm9kZS51bmlxdWVJRF09e30pO3VuaXF1ZUNhY2hlW3R5cGVdPVtkaXJydW5zLGRpZmZdO31cbmlmKG5vZGU9PT1lbGVtKXticmVhazt9fX19fVxuZGlmZi09bGFzdDtyZXR1cm4gZGlmZj09PWZpcnN0fHwoZGlmZiVmaXJzdD09PTAmJmRpZmYvZmlyc3Q+PTApO319O30sXCJQU0VVRE9cIjpmdW5jdGlvbihwc2V1ZG8sYXJndW1lbnQpe3ZhciBhcmdzLGZuPUV4cHIucHNldWRvc1twc2V1ZG9dfHxFeHByLnNldEZpbHRlcnNbcHNldWRvLnRvTG93ZXJDYXNlKCldfHxTaXp6bGUuZXJyb3IoXCJ1bnN1cHBvcnRlZCBwc2V1ZG86IFwiK3BzZXVkbyk7aWYoZm5bZXhwYW5kb10pe3JldHVybiBmbihhcmd1bWVudCk7fVxuaWYoZm4ubGVuZ3RoPjEpe2FyZ3M9W3BzZXVkbyxwc2V1ZG8sXCJcIixhcmd1bWVudF07cmV0dXJuIEV4cHIuc2V0RmlsdGVycy5oYXNPd25Qcm9wZXJ0eShwc2V1ZG8udG9Mb3dlckNhc2UoKSk/bWFya0Z1bmN0aW9uKGZ1bmN0aW9uKHNlZWQsbWF0Y2hlcyl7dmFyIGlkeCxtYXRjaGVkPWZuKHNlZWQsYXJndW1lbnQpLGk9bWF0Y2hlZC5sZW5ndGg7d2hpbGUoaS0tKXtpZHg9aW5kZXhPZihzZWVkLG1hdGNoZWRbaV0pO3NlZWRbaWR4XT0hKG1hdGNoZXNbaWR4XT1tYXRjaGVkW2ldKTt9fSk6ZnVuY3Rpb24oZWxlbSl7cmV0dXJuIGZuKGVsZW0sMCxhcmdzKTt9O31cbnJldHVybiBmbjt9fSxwc2V1ZG9zOntcIm5vdFwiOm1hcmtGdW5jdGlvbihmdW5jdGlvbihzZWxlY3Rvcil7dmFyIGlucHV0PVtdLHJlc3VsdHM9W10sbWF0Y2hlcj1jb21waWxlKHNlbGVjdG9yLnJlcGxhY2UocnRyaW0sXCIkMVwiKSk7cmV0dXJuIG1hdGNoZXJbZXhwYW5kb10/bWFya0Z1bmN0aW9uKGZ1bmN0aW9uKHNlZWQsbWF0Y2hlcyxfY29udGV4dCx4bWwpe3ZhciBlbGVtLHVubWF0Y2hlZD1tYXRjaGVyKHNlZWQsbnVsbCx4bWwsW10pLGk9c2VlZC5sZW5ndGg7d2hpbGUoaS0tKXtpZigoZWxlbT11bm1hdGNoZWRbaV0pKXtzZWVkW2ldPSEobWF0Y2hlc1tpXT1lbGVtKTt9fX0pOmZ1bmN0aW9uKGVsZW0sX2NvbnRleHQseG1sKXtpbnB1dFswXT1lbGVtO21hdGNoZXIoaW5wdXQsbnVsbCx4bWwscmVzdWx0cyk7aW5wdXRbMF09bnVsbDtyZXR1cm4hcmVzdWx0cy5wb3AoKTt9O30pLFwiaGFzXCI6bWFya0Z1bmN0aW9uKGZ1bmN0aW9uKHNlbGVjdG9yKXtyZXR1cm4gZnVuY3Rpb24oZWxlbSl7cmV0dXJuIFNpenpsZShzZWxlY3RvcixlbGVtKS5sZW5ndGg+MDt9O30pLFwiY29udGFpbnNcIjptYXJrRnVuY3Rpb24oZnVuY3Rpb24odGV4dCl7dGV4dD10ZXh0LnJlcGxhY2UocnVuZXNjYXBlLGZ1bmVzY2FwZSk7cmV0dXJuIGZ1bmN0aW9uKGVsZW0pe3JldHVybihlbGVtLnRleHRDb250ZW50fHxnZXRUZXh0KGVsZW0pKS5pbmRleE9mKHRleHQpPi0xO307fSksXCJsYW5nXCI6bWFya0Z1bmN0aW9uKGZ1bmN0aW9uKGxhbmcpe2lmKCFyaWRlbnRpZmllci50ZXN0KGxhbmd8fFwiXCIpKXtTaXp6bGUuZXJyb3IoXCJ1bnN1cHBvcnRlZCBsYW5nOiBcIitsYW5nKTt9XG5sYW5nPWxhbmcucmVwbGFjZShydW5lc2NhcGUsZnVuZXNjYXBlKS50b0xvd2VyQ2FzZSgpO3JldHVybiBmdW5jdGlvbihlbGVtKXt2YXIgZWxlbUxhbmc7ZG97aWYoKGVsZW1MYW5nPWRvY3VtZW50SXNIVE1MP2VsZW0ubGFuZzplbGVtLmdldEF0dHJpYnV0ZShcInhtbDpsYW5nXCIpfHxlbGVtLmdldEF0dHJpYnV0ZShcImxhbmdcIikpKXtlbGVtTGFuZz1lbGVtTGFuZy50b0xvd2VyQ2FzZSgpO3JldHVybiBlbGVtTGFuZz09PWxhbmd8fGVsZW1MYW5nLmluZGV4T2YobGFuZytcIi1cIik9PT0wO319d2hpbGUoKGVsZW09ZWxlbS5wYXJlbnROb2RlKSYmZWxlbS5ub2RlVHlwZT09PTEpO3JldHVybiBmYWxzZTt9O30pLFwidGFyZ2V0XCI6ZnVuY3Rpb24oZWxlbSl7dmFyIGhhc2g9d2luZG93LmxvY2F0aW9uJiZ3aW5kb3cubG9jYXRpb24uaGFzaDtyZXR1cm4gaGFzaCYmaGFzaC5zbGljZSgxKT09PWVsZW0uaWQ7fSxcInJvb3RcIjpmdW5jdGlvbihlbGVtKXtyZXR1cm4gZWxlbT09PWRvY0VsZW07fSxcImZvY3VzXCI6ZnVuY3Rpb24oZWxlbSl7cmV0dXJuIGVsZW09PT1kb2N1bWVudC5hY3RpdmVFbGVtZW50JiYoIWRvY3VtZW50Lmhhc0ZvY3VzfHxkb2N1bWVudC5oYXNGb2N1cygpKSYmISEoZWxlbS50eXBlfHxlbGVtLmhyZWZ8fH5lbGVtLnRhYkluZGV4KTt9LFwiZW5hYmxlZFwiOmNyZWF0ZURpc2FibGVkUHNldWRvKGZhbHNlKSxcImRpc2FibGVkXCI6Y3JlYXRlRGlzYWJsZWRQc2V1ZG8odHJ1ZSksXCJjaGVja2VkXCI6ZnVuY3Rpb24oZWxlbSl7dmFyIG5vZGVOYW1lPWVsZW0ubm9kZU5hbWUudG9Mb3dlckNhc2UoKTtyZXR1cm4obm9kZU5hbWU9PT1cImlucHV0XCImJiEhZWxlbS5jaGVja2VkKXx8KG5vZGVOYW1lPT09XCJvcHRpb25cIiYmISFlbGVtLnNlbGVjdGVkKTt9LFwic2VsZWN0ZWRcIjpmdW5jdGlvbihlbGVtKXtpZihlbGVtLnBhcmVudE5vZGUpe2VsZW0ucGFyZW50Tm9kZS5zZWxlY3RlZEluZGV4O31cbnJldHVybiBlbGVtLnNlbGVjdGVkPT09dHJ1ZTt9LFwiZW1wdHlcIjpmdW5jdGlvbihlbGVtKXtmb3IoZWxlbT1lbGVtLmZpcnN0Q2hpbGQ7ZWxlbTtlbGVtPWVsZW0ubmV4dFNpYmxpbmcpe2lmKGVsZW0ubm9kZVR5cGU8Nil7cmV0dXJuIGZhbHNlO319XG5yZXR1cm4gdHJ1ZTt9LFwicGFyZW50XCI6ZnVuY3Rpb24oZWxlbSl7cmV0dXJuIUV4cHIucHNldWRvc1tcImVtcHR5XCJdKGVsZW0pO30sXCJoZWFkZXJcIjpmdW5jdGlvbihlbGVtKXtyZXR1cm4gcmhlYWRlci50ZXN0KGVsZW0ubm9kZU5hbWUpO30sXCJpbnB1dFwiOmZ1bmN0aW9uKGVsZW0pe3JldHVybiByaW5wdXRzLnRlc3QoZWxlbS5ub2RlTmFtZSk7fSxcImJ1dHRvblwiOmZ1bmN0aW9uKGVsZW0pe3ZhciBuYW1lPWVsZW0ubm9kZU5hbWUudG9Mb3dlckNhc2UoKTtyZXR1cm4gbmFtZT09PVwiaW5wdXRcIiYmZWxlbS50eXBlPT09XCJidXR0b25cInx8bmFtZT09PVwiYnV0dG9uXCI7fSxcInRleHRcIjpmdW5jdGlvbihlbGVtKXt2YXIgYXR0cjtyZXR1cm4gZWxlbS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpPT09XCJpbnB1dFwiJiZlbGVtLnR5cGU9PT1cInRleHRcIiYmKChhdHRyPWVsZW0uZ2V0QXR0cmlidXRlKFwidHlwZVwiKSk9PW51bGx8fGF0dHIudG9Mb3dlckNhc2UoKT09PVwidGV4dFwiKTt9LFwiZmlyc3RcIjpjcmVhdGVQb3NpdGlvbmFsUHNldWRvKGZ1bmN0aW9uKCl7cmV0dXJuWzBdO30pLFwibGFzdFwiOmNyZWF0ZVBvc2l0aW9uYWxQc2V1ZG8oZnVuY3Rpb24oX21hdGNoSW5kZXhlcyxsZW5ndGgpe3JldHVybltsZW5ndGgtMV07fSksXCJlcVwiOmNyZWF0ZVBvc2l0aW9uYWxQc2V1ZG8oZnVuY3Rpb24oX21hdGNoSW5kZXhlcyxsZW5ndGgsYXJndW1lbnQpe3JldHVyblthcmd1bWVudDwwP2FyZ3VtZW50K2xlbmd0aDphcmd1bWVudF07fSksXCJldmVuXCI6Y3JlYXRlUG9zaXRpb25hbFBzZXVkbyhmdW5jdGlvbihtYXRjaEluZGV4ZXMsbGVuZ3RoKXt2YXIgaT0wO2Zvcig7aTxsZW5ndGg7aSs9Mil7bWF0Y2hJbmRleGVzLnB1c2goaSk7fVxucmV0dXJuIG1hdGNoSW5kZXhlczt9KSxcIm9kZFwiOmNyZWF0ZVBvc2l0aW9uYWxQc2V1ZG8oZnVuY3Rpb24obWF0Y2hJbmRleGVzLGxlbmd0aCl7dmFyIGk9MTtmb3IoO2k8bGVuZ3RoO2krPTIpe21hdGNoSW5kZXhlcy5wdXNoKGkpO31cbnJldHVybiBtYXRjaEluZGV4ZXM7fSksXCJsdFwiOmNyZWF0ZVBvc2l0aW9uYWxQc2V1ZG8oZnVuY3Rpb24obWF0Y2hJbmRleGVzLGxlbmd0aCxhcmd1bWVudCl7dmFyIGk9YXJndW1lbnQ8MD9hcmd1bWVudCtsZW5ndGg6YXJndW1lbnQ+bGVuZ3RoP2xlbmd0aDphcmd1bWVudDtmb3IoOy0taT49MDspe21hdGNoSW5kZXhlcy5wdXNoKGkpO31cbnJldHVybiBtYXRjaEluZGV4ZXM7fSksXCJndFwiOmNyZWF0ZVBvc2l0aW9uYWxQc2V1ZG8oZnVuY3Rpb24obWF0Y2hJbmRleGVzLGxlbmd0aCxhcmd1bWVudCl7dmFyIGk9YXJndW1lbnQ8MD9hcmd1bWVudCtsZW5ndGg6YXJndW1lbnQ7Zm9yKDsrK2k8bGVuZ3RoOyl7bWF0Y2hJbmRleGVzLnB1c2goaSk7fVxucmV0dXJuIG1hdGNoSW5kZXhlczt9KX19O0V4cHIucHNldWRvc1tcIm50aFwiXT1FeHByLnBzZXVkb3NbXCJlcVwiXTtmb3IoaSBpbntyYWRpbzp0cnVlLGNoZWNrYm94OnRydWUsZmlsZTp0cnVlLHBhc3N3b3JkOnRydWUsaW1hZ2U6dHJ1ZX0pe0V4cHIucHNldWRvc1tpXT1jcmVhdGVJbnB1dFBzZXVkbyhpKTt9XG5mb3IoaSBpbntzdWJtaXQ6dHJ1ZSxyZXNldDp0cnVlfSl7RXhwci5wc2V1ZG9zW2ldPWNyZWF0ZUJ1dHRvblBzZXVkbyhpKTt9XG5mdW5jdGlvbiBzZXRGaWx0ZXJzKCl7fVxuc2V0RmlsdGVycy5wcm90b3R5cGU9RXhwci5maWx0ZXJzPUV4cHIucHNldWRvcztFeHByLnNldEZpbHRlcnM9bmV3IHNldEZpbHRlcnMoKTt0b2tlbml6ZT1TaXp6bGUudG9rZW5pemU9ZnVuY3Rpb24oc2VsZWN0b3IscGFyc2VPbmx5KXt2YXIgbWF0Y2hlZCxtYXRjaCx0b2tlbnMsdHlwZSxzb0Zhcixncm91cHMscHJlRmlsdGVycyxjYWNoZWQ9dG9rZW5DYWNoZVtzZWxlY3RvcitcIiBcIl07aWYoY2FjaGVkKXtyZXR1cm4gcGFyc2VPbmx5PzA6Y2FjaGVkLnNsaWNlKDApO31cbnNvRmFyPXNlbGVjdG9yO2dyb3Vwcz1bXTtwcmVGaWx0ZXJzPUV4cHIucHJlRmlsdGVyO3doaWxlKHNvRmFyKXtpZighbWF0Y2hlZHx8KG1hdGNoPXJjb21tYS5leGVjKHNvRmFyKSkpe2lmKG1hdGNoKXtzb0Zhcj1zb0Zhci5zbGljZShtYXRjaFswXS5sZW5ndGgpfHxzb0Zhcjt9XG5ncm91cHMucHVzaCgodG9rZW5zPVtdKSk7fVxubWF0Y2hlZD1mYWxzZTtpZigobWF0Y2g9cmNvbWJpbmF0b3JzLmV4ZWMoc29GYXIpKSl7bWF0Y2hlZD1tYXRjaC5zaGlmdCgpO3Rva2Vucy5wdXNoKHt2YWx1ZTptYXRjaGVkLHR5cGU6bWF0Y2hbMF0ucmVwbGFjZShydHJpbSxcIiBcIil9KTtzb0Zhcj1zb0Zhci5zbGljZShtYXRjaGVkLmxlbmd0aCk7fVxuZm9yKHR5cGUgaW4gRXhwci5maWx0ZXIpe2lmKChtYXRjaD1tYXRjaEV4cHJbdHlwZV0uZXhlYyhzb0ZhcikpJiYoIXByZUZpbHRlcnNbdHlwZV18fChtYXRjaD1wcmVGaWx0ZXJzW3R5cGVdKG1hdGNoKSkpKXttYXRjaGVkPW1hdGNoLnNoaWZ0KCk7dG9rZW5zLnB1c2goe3ZhbHVlOm1hdGNoZWQsdHlwZTp0eXBlLG1hdGNoZXM6bWF0Y2h9KTtzb0Zhcj1zb0Zhci5zbGljZShtYXRjaGVkLmxlbmd0aCk7fX1cbmlmKCFtYXRjaGVkKXticmVhazt9fVxucmV0dXJuIHBhcnNlT25seT9zb0Zhci5sZW5ndGg6c29GYXI/U2l6emxlLmVycm9yKHNlbGVjdG9yKTp0b2tlbkNhY2hlKHNlbGVjdG9yLGdyb3Vwcykuc2xpY2UoMCk7fTtmdW5jdGlvbiB0b1NlbGVjdG9yKHRva2Vucyl7dmFyIGk9MCxsZW49dG9rZW5zLmxlbmd0aCxzZWxlY3Rvcj1cIlwiO2Zvcig7aTxsZW47aSsrKXtzZWxlY3Rvcis9dG9rZW5zW2ldLnZhbHVlO31cbnJldHVybiBzZWxlY3Rvcjt9XG5mdW5jdGlvbiBhZGRDb21iaW5hdG9yKG1hdGNoZXIsY29tYmluYXRvcixiYXNlKXt2YXIgZGlyPWNvbWJpbmF0b3IuZGlyLHNraXA9Y29tYmluYXRvci5uZXh0LGtleT1za2lwfHxkaXIsY2hlY2tOb25FbGVtZW50cz1iYXNlJiZrZXk9PT1cInBhcmVudE5vZGVcIixkb25lTmFtZT1kb25lKys7cmV0dXJuIGNvbWJpbmF0b3IuZmlyc3Q/ZnVuY3Rpb24oZWxlbSxjb250ZXh0LHhtbCl7d2hpbGUoKGVsZW09ZWxlbVtkaXJdKSl7aWYoZWxlbS5ub2RlVHlwZT09PTF8fGNoZWNrTm9uRWxlbWVudHMpe3JldHVybiBtYXRjaGVyKGVsZW0sY29udGV4dCx4bWwpO319XG5yZXR1cm4gZmFsc2U7fTpmdW5jdGlvbihlbGVtLGNvbnRleHQseG1sKXt2YXIgb2xkQ2FjaGUsdW5pcXVlQ2FjaGUsb3V0ZXJDYWNoZSxuZXdDYWNoZT1bZGlycnVucyxkb25lTmFtZV07aWYoeG1sKXt3aGlsZSgoZWxlbT1lbGVtW2Rpcl0pKXtpZihlbGVtLm5vZGVUeXBlPT09MXx8Y2hlY2tOb25FbGVtZW50cyl7aWYobWF0Y2hlcihlbGVtLGNvbnRleHQseG1sKSl7cmV0dXJuIHRydWU7fX19fWVsc2V7d2hpbGUoKGVsZW09ZWxlbVtkaXJdKSl7aWYoZWxlbS5ub2RlVHlwZT09PTF8fGNoZWNrTm9uRWxlbWVudHMpe291dGVyQ2FjaGU9ZWxlbVtleHBhbmRvXXx8KGVsZW1bZXhwYW5kb109e30pO3VuaXF1ZUNhY2hlPW91dGVyQ2FjaGVbZWxlbS51bmlxdWVJRF18fChvdXRlckNhY2hlW2VsZW0udW5pcXVlSURdPXt9KTtpZihza2lwJiZza2lwPT09ZWxlbS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpKXtlbGVtPWVsZW1bZGlyXXx8ZWxlbTt9ZWxzZSBpZigob2xkQ2FjaGU9dW5pcXVlQ2FjaGVba2V5XSkmJm9sZENhY2hlWzBdPT09ZGlycnVucyYmb2xkQ2FjaGVbMV09PT1kb25lTmFtZSl7cmV0dXJuKG5ld0NhY2hlWzJdPW9sZENhY2hlWzJdKTt9ZWxzZXt1bmlxdWVDYWNoZVtrZXldPW5ld0NhY2hlO2lmKChuZXdDYWNoZVsyXT1tYXRjaGVyKGVsZW0sY29udGV4dCx4bWwpKSl7cmV0dXJuIHRydWU7fX19fX1cbnJldHVybiBmYWxzZTt9O31cbmZ1bmN0aW9uIGVsZW1lbnRNYXRjaGVyKG1hdGNoZXJzKXtyZXR1cm4gbWF0Y2hlcnMubGVuZ3RoPjE/ZnVuY3Rpb24oZWxlbSxjb250ZXh0LHhtbCl7dmFyIGk9bWF0Y2hlcnMubGVuZ3RoO3doaWxlKGktLSl7aWYoIW1hdGNoZXJzW2ldKGVsZW0sY29udGV4dCx4bWwpKXtyZXR1cm4gZmFsc2U7fX1cbnJldHVybiB0cnVlO306bWF0Y2hlcnNbMF07fVxuZnVuY3Rpb24gbXVsdGlwbGVDb250ZXh0cyhzZWxlY3Rvcixjb250ZXh0cyxyZXN1bHRzKXt2YXIgaT0wLGxlbj1jb250ZXh0cy5sZW5ndGg7Zm9yKDtpPGxlbjtpKyspe1NpenpsZShzZWxlY3Rvcixjb250ZXh0c1tpXSxyZXN1bHRzKTt9XG5yZXR1cm4gcmVzdWx0czt9XG5mdW5jdGlvbiBjb25kZW5zZSh1bm1hdGNoZWQsbWFwLGZpbHRlcixjb250ZXh0LHhtbCl7dmFyIGVsZW0sbmV3VW5tYXRjaGVkPVtdLGk9MCxsZW49dW5tYXRjaGVkLmxlbmd0aCxtYXBwZWQ9bWFwIT1udWxsO2Zvcig7aTxsZW47aSsrKXtpZigoZWxlbT11bm1hdGNoZWRbaV0pKXtpZighZmlsdGVyfHxmaWx0ZXIoZWxlbSxjb250ZXh0LHhtbCkpe25ld1VubWF0Y2hlZC5wdXNoKGVsZW0pO2lmKG1hcHBlZCl7bWFwLnB1c2goaSk7fX19fVxucmV0dXJuIG5ld1VubWF0Y2hlZDt9XG5mdW5jdGlvbiBzZXRNYXRjaGVyKHByZUZpbHRlcixzZWxlY3RvcixtYXRjaGVyLHBvc3RGaWx0ZXIscG9zdEZpbmRlcixwb3N0U2VsZWN0b3Ipe2lmKHBvc3RGaWx0ZXImJiFwb3N0RmlsdGVyW2V4cGFuZG9dKXtwb3N0RmlsdGVyPXNldE1hdGNoZXIocG9zdEZpbHRlcik7fVxuaWYocG9zdEZpbmRlciYmIXBvc3RGaW5kZXJbZXhwYW5kb10pe3Bvc3RGaW5kZXI9c2V0TWF0Y2hlcihwb3N0RmluZGVyLHBvc3RTZWxlY3Rvcik7fVxucmV0dXJuIG1hcmtGdW5jdGlvbihmdW5jdGlvbihzZWVkLHJlc3VsdHMsY29udGV4dCx4bWwpe3ZhciB0ZW1wLGksZWxlbSxwcmVNYXA9W10scG9zdE1hcD1bXSxwcmVleGlzdGluZz1yZXN1bHRzLmxlbmd0aCxlbGVtcz1zZWVkfHxtdWx0aXBsZUNvbnRleHRzKHNlbGVjdG9yfHxcIipcIixjb250ZXh0Lm5vZGVUeXBlP1tjb250ZXh0XTpjb250ZXh0LFtdKSxtYXRjaGVySW49cHJlRmlsdGVyJiYoc2VlZHx8IXNlbGVjdG9yKT9jb25kZW5zZShlbGVtcyxwcmVNYXAscHJlRmlsdGVyLGNvbnRleHQseG1sKTplbGVtcyxtYXRjaGVyT3V0PW1hdGNoZXI/cG9zdEZpbmRlcnx8KHNlZWQ/cHJlRmlsdGVyOnByZWV4aXN0aW5nfHxwb3N0RmlsdGVyKT9bXTpyZXN1bHRzOm1hdGNoZXJJbjtpZihtYXRjaGVyKXttYXRjaGVyKG1hdGNoZXJJbixtYXRjaGVyT3V0LGNvbnRleHQseG1sKTt9XG5pZihwb3N0RmlsdGVyKXt0ZW1wPWNvbmRlbnNlKG1hdGNoZXJPdXQscG9zdE1hcCk7cG9zdEZpbHRlcih0ZW1wLFtdLGNvbnRleHQseG1sKTtpPXRlbXAubGVuZ3RoO3doaWxlKGktLSl7aWYoKGVsZW09dGVtcFtpXSkpe21hdGNoZXJPdXRbcG9zdE1hcFtpXV09IShtYXRjaGVySW5bcG9zdE1hcFtpXV09ZWxlbSk7fX19XG5pZihzZWVkKXtpZihwb3N0RmluZGVyfHxwcmVGaWx0ZXIpe2lmKHBvc3RGaW5kZXIpe3RlbXA9W107aT1tYXRjaGVyT3V0Lmxlbmd0aDt3aGlsZShpLS0pe2lmKChlbGVtPW1hdGNoZXJPdXRbaV0pKXt0ZW1wLnB1c2goKG1hdGNoZXJJbltpXT1lbGVtKSk7fX1cbnBvc3RGaW5kZXIobnVsbCwobWF0Y2hlck91dD1bXSksdGVtcCx4bWwpO31cbmk9bWF0Y2hlck91dC5sZW5ndGg7d2hpbGUoaS0tKXtpZigoZWxlbT1tYXRjaGVyT3V0W2ldKSYmKHRlbXA9cG9zdEZpbmRlcj9pbmRleE9mKHNlZWQsZWxlbSk6cHJlTWFwW2ldKT4tMSl7c2VlZFt0ZW1wXT0hKHJlc3VsdHNbdGVtcF09ZWxlbSk7fX19fWVsc2V7bWF0Y2hlck91dD1jb25kZW5zZShtYXRjaGVyT3V0PT09cmVzdWx0cz9tYXRjaGVyT3V0LnNwbGljZShwcmVleGlzdGluZyxtYXRjaGVyT3V0Lmxlbmd0aCk6bWF0Y2hlck91dCk7aWYocG9zdEZpbmRlcil7cG9zdEZpbmRlcihudWxsLHJlc3VsdHMsbWF0Y2hlck91dCx4bWwpO31lbHNle3B1c2guYXBwbHkocmVzdWx0cyxtYXRjaGVyT3V0KTt9fX0pO31cbmZ1bmN0aW9uIG1hdGNoZXJGcm9tVG9rZW5zKHRva2Vucyl7dmFyIGNoZWNrQ29udGV4dCxtYXRjaGVyLGosbGVuPXRva2Vucy5sZW5ndGgsbGVhZGluZ1JlbGF0aXZlPUV4cHIucmVsYXRpdmVbdG9rZW5zWzBdLnR5cGVdLGltcGxpY2l0UmVsYXRpdmU9bGVhZGluZ1JlbGF0aXZlfHxFeHByLnJlbGF0aXZlW1wiIFwiXSxpPWxlYWRpbmdSZWxhdGl2ZT8xOjAsbWF0Y2hDb250ZXh0PWFkZENvbWJpbmF0b3IoZnVuY3Rpb24oZWxlbSl7cmV0dXJuIGVsZW09PT1jaGVja0NvbnRleHQ7fSxpbXBsaWNpdFJlbGF0aXZlLHRydWUpLG1hdGNoQW55Q29udGV4dD1hZGRDb21iaW5hdG9yKGZ1bmN0aW9uKGVsZW0pe3JldHVybiBpbmRleE9mKGNoZWNrQ29udGV4dCxlbGVtKT4tMTt9LGltcGxpY2l0UmVsYXRpdmUsdHJ1ZSksbWF0Y2hlcnM9W2Z1bmN0aW9uKGVsZW0sY29udGV4dCx4bWwpe3ZhciByZXQ9KCFsZWFkaW5nUmVsYXRpdmUmJih4bWx8fGNvbnRleHQhPT1vdXRlcm1vc3RDb250ZXh0KSl8fCgoY2hlY2tDb250ZXh0PWNvbnRleHQpLm5vZGVUeXBlP21hdGNoQ29udGV4dChlbGVtLGNvbnRleHQseG1sKTptYXRjaEFueUNvbnRleHQoZWxlbSxjb250ZXh0LHhtbCkpO2NoZWNrQ29udGV4dD1udWxsO3JldHVybiByZXQ7fV07Zm9yKDtpPGxlbjtpKyspe2lmKChtYXRjaGVyPUV4cHIucmVsYXRpdmVbdG9rZW5zW2ldLnR5cGVdKSl7bWF0Y2hlcnM9W2FkZENvbWJpbmF0b3IoZWxlbWVudE1hdGNoZXIobWF0Y2hlcnMpLG1hdGNoZXIpXTt9ZWxzZXttYXRjaGVyPUV4cHIuZmlsdGVyW3Rva2Vuc1tpXS50eXBlXS5hcHBseShudWxsLHRva2Vuc1tpXS5tYXRjaGVzKTtpZihtYXRjaGVyW2V4cGFuZG9dKXtqPSsraTtmb3IoO2o8bGVuO2orKyl7aWYoRXhwci5yZWxhdGl2ZVt0b2tlbnNbal0udHlwZV0pe2JyZWFrO319XG5yZXR1cm4gc2V0TWF0Y2hlcihpPjEmJmVsZW1lbnRNYXRjaGVyKG1hdGNoZXJzKSxpPjEmJnRvU2VsZWN0b3IodG9rZW5zLnNsaWNlKDAsaS0xKS5jb25jYXQoe3ZhbHVlOnRva2Vuc1tpLTJdLnR5cGU9PT1cIiBcIj9cIipcIjpcIlwifSkpLnJlcGxhY2UocnRyaW0sXCIkMVwiKSxtYXRjaGVyLGk8aiYmbWF0Y2hlckZyb21Ub2tlbnModG9rZW5zLnNsaWNlKGksaikpLGo8bGVuJiZtYXRjaGVyRnJvbVRva2VucygodG9rZW5zPXRva2Vucy5zbGljZShqKSkpLGo8bGVuJiZ0b1NlbGVjdG9yKHRva2VucykpO31cbm1hdGNoZXJzLnB1c2gobWF0Y2hlcik7fX1cbnJldHVybiBlbGVtZW50TWF0Y2hlcihtYXRjaGVycyk7fVxuZnVuY3Rpb24gbWF0Y2hlckZyb21Hcm91cE1hdGNoZXJzKGVsZW1lbnRNYXRjaGVycyxzZXRNYXRjaGVycyl7dmFyIGJ5U2V0PXNldE1hdGNoZXJzLmxlbmd0aD4wLGJ5RWxlbWVudD1lbGVtZW50TWF0Y2hlcnMubGVuZ3RoPjAsc3VwZXJNYXRjaGVyPWZ1bmN0aW9uKHNlZWQsY29udGV4dCx4bWwscmVzdWx0cyxvdXRlcm1vc3Qpe3ZhciBlbGVtLGosbWF0Y2hlcixtYXRjaGVkQ291bnQ9MCxpPVwiMFwiLHVubWF0Y2hlZD1zZWVkJiZbXSxzZXRNYXRjaGVkPVtdLGNvbnRleHRCYWNrdXA9b3V0ZXJtb3N0Q29udGV4dCxlbGVtcz1zZWVkfHxieUVsZW1lbnQmJkV4cHIuZmluZFtcIlRBR1wiXShcIipcIixvdXRlcm1vc3QpLGRpcnJ1bnNVbmlxdWU9KGRpcnJ1bnMrPWNvbnRleHRCYWNrdXA9PW51bGw/MTpNYXRoLnJhbmRvbSgpfHwwLjEpLGxlbj1lbGVtcy5sZW5ndGg7aWYob3V0ZXJtb3N0KXtvdXRlcm1vc3RDb250ZXh0PWNvbnRleHQ9PWRvY3VtZW50fHxjb250ZXh0fHxvdXRlcm1vc3Q7fVxuZm9yKDtpIT09bGVuJiYoZWxlbT1lbGVtc1tpXSkhPW51bGw7aSsrKXtpZihieUVsZW1lbnQmJmVsZW0pe2o9MDtpZighY29udGV4dCYmZWxlbS5vd25lckRvY3VtZW50IT1kb2N1bWVudCl7c2V0RG9jdW1lbnQoZWxlbSk7eG1sPSFkb2N1bWVudElzSFRNTDt9XG53aGlsZSgobWF0Y2hlcj1lbGVtZW50TWF0Y2hlcnNbaisrXSkpe2lmKG1hdGNoZXIoZWxlbSxjb250ZXh0fHxkb2N1bWVudCx4bWwpKXtyZXN1bHRzLnB1c2goZWxlbSk7YnJlYWs7fX1cbmlmKG91dGVybW9zdCl7ZGlycnVucz1kaXJydW5zVW5pcXVlO319XG5pZihieVNldCl7aWYoKGVsZW09IW1hdGNoZXImJmVsZW0pKXttYXRjaGVkQ291bnQtLTt9XG5pZihzZWVkKXt1bm1hdGNoZWQucHVzaChlbGVtKTt9fX1cbm1hdGNoZWRDb3VudCs9aTtpZihieVNldCYmaSE9PW1hdGNoZWRDb3VudCl7aj0wO3doaWxlKChtYXRjaGVyPXNldE1hdGNoZXJzW2orK10pKXttYXRjaGVyKHVubWF0Y2hlZCxzZXRNYXRjaGVkLGNvbnRleHQseG1sKTt9XG5pZihzZWVkKXtpZihtYXRjaGVkQ291bnQ+MCl7d2hpbGUoaS0tKXtpZighKHVubWF0Y2hlZFtpXXx8c2V0TWF0Y2hlZFtpXSkpe3NldE1hdGNoZWRbaV09cG9wLmNhbGwocmVzdWx0cyk7fX19XG5zZXRNYXRjaGVkPWNvbmRlbnNlKHNldE1hdGNoZWQpO31cbnB1c2guYXBwbHkocmVzdWx0cyxzZXRNYXRjaGVkKTtpZihvdXRlcm1vc3QmJiFzZWVkJiZzZXRNYXRjaGVkLmxlbmd0aD4wJiYobWF0Y2hlZENvdW50K3NldE1hdGNoZXJzLmxlbmd0aCk+MSl7U2l6emxlLnVuaXF1ZVNvcnQocmVzdWx0cyk7fX1cbmlmKG91dGVybW9zdCl7ZGlycnVucz1kaXJydW5zVW5pcXVlO291dGVybW9zdENvbnRleHQ9Y29udGV4dEJhY2t1cDt9XG5yZXR1cm4gdW5tYXRjaGVkO307cmV0dXJuIGJ5U2V0P21hcmtGdW5jdGlvbihzdXBlck1hdGNoZXIpOnN1cGVyTWF0Y2hlcjt9XG5jb21waWxlPVNpenpsZS5jb21waWxlPWZ1bmN0aW9uKHNlbGVjdG9yLG1hdGNoKXt2YXIgaSxzZXRNYXRjaGVycz1bXSxlbGVtZW50TWF0Y2hlcnM9W10sY2FjaGVkPWNvbXBpbGVyQ2FjaGVbc2VsZWN0b3IrXCIgXCJdO2lmKCFjYWNoZWQpe2lmKCFtYXRjaCl7bWF0Y2g9dG9rZW5pemUoc2VsZWN0b3IpO31cbmk9bWF0Y2gubGVuZ3RoO3doaWxlKGktLSl7Y2FjaGVkPW1hdGNoZXJGcm9tVG9rZW5zKG1hdGNoW2ldKTtpZihjYWNoZWRbZXhwYW5kb10pe3NldE1hdGNoZXJzLnB1c2goY2FjaGVkKTt9ZWxzZXtlbGVtZW50TWF0Y2hlcnMucHVzaChjYWNoZWQpO319XG5jYWNoZWQ9Y29tcGlsZXJDYWNoZShzZWxlY3RvcixtYXRjaGVyRnJvbUdyb3VwTWF0Y2hlcnMoZWxlbWVudE1hdGNoZXJzLHNldE1hdGNoZXJzKSk7Y2FjaGVkLnNlbGVjdG9yPXNlbGVjdG9yO31cbnJldHVybiBjYWNoZWQ7fTtzZWxlY3Q9U2l6emxlLnNlbGVjdD1mdW5jdGlvbihzZWxlY3Rvcixjb250ZXh0LHJlc3VsdHMsc2VlZCl7dmFyIGksdG9rZW5zLHRva2VuLHR5cGUsZmluZCxjb21waWxlZD10eXBlb2Ygc2VsZWN0b3I9PT1cImZ1bmN0aW9uXCImJnNlbGVjdG9yLG1hdGNoPSFzZWVkJiZ0b2tlbml6ZSgoc2VsZWN0b3I9Y29tcGlsZWQuc2VsZWN0b3J8fHNlbGVjdG9yKSk7cmVzdWx0cz1yZXN1bHRzfHxbXTtpZihtYXRjaC5sZW5ndGg9PT0xKXt0b2tlbnM9bWF0Y2hbMF09bWF0Y2hbMF0uc2xpY2UoMCk7aWYodG9rZW5zLmxlbmd0aD4yJiYodG9rZW49dG9rZW5zWzBdKS50eXBlPT09XCJJRFwiJiZjb250ZXh0Lm5vZGVUeXBlPT09OSYmZG9jdW1lbnRJc0hUTUwmJkV4cHIucmVsYXRpdmVbdG9rZW5zWzFdLnR5cGVdKXtjb250ZXh0PShFeHByLmZpbmRbXCJJRFwiXSh0b2tlbi5tYXRjaGVzWzBdLnJlcGxhY2UocnVuZXNjYXBlLGZ1bmVzY2FwZSksY29udGV4dCl8fFtdKVswXTtpZighY29udGV4dCl7cmV0dXJuIHJlc3VsdHM7fWVsc2UgaWYoY29tcGlsZWQpe2NvbnRleHQ9Y29udGV4dC5wYXJlbnROb2RlO31cbnNlbGVjdG9yPXNlbGVjdG9yLnNsaWNlKHRva2Vucy5zaGlmdCgpLnZhbHVlLmxlbmd0aCk7fVxuaT1tYXRjaEV4cHJbXCJuZWVkc0NvbnRleHRcIl0udGVzdChzZWxlY3Rvcik/MDp0b2tlbnMubGVuZ3RoO3doaWxlKGktLSl7dG9rZW49dG9rZW5zW2ldO2lmKEV4cHIucmVsYXRpdmVbKHR5cGU9dG9rZW4udHlwZSldKXticmVhazt9XG5pZigoZmluZD1FeHByLmZpbmRbdHlwZV0pKXtpZigoc2VlZD1maW5kKHRva2VuLm1hdGNoZXNbMF0ucmVwbGFjZShydW5lc2NhcGUsZnVuZXNjYXBlKSxyc2libGluZy50ZXN0KHRva2Vuc1swXS50eXBlKSYmdGVzdENvbnRleHQoY29udGV4dC5wYXJlbnROb2RlKXx8Y29udGV4dCkpKXt0b2tlbnMuc3BsaWNlKGksMSk7c2VsZWN0b3I9c2VlZC5sZW5ndGgmJnRvU2VsZWN0b3IodG9rZW5zKTtpZighc2VsZWN0b3Ipe3B1c2guYXBwbHkocmVzdWx0cyxzZWVkKTtyZXR1cm4gcmVzdWx0czt9XG5icmVhazt9fX19XG4oY29tcGlsZWR8fGNvbXBpbGUoc2VsZWN0b3IsbWF0Y2gpKShzZWVkLGNvbnRleHQsIWRvY3VtZW50SXNIVE1MLHJlc3VsdHMsIWNvbnRleHR8fHJzaWJsaW5nLnRlc3Qoc2VsZWN0b3IpJiZ0ZXN0Q29udGV4dChjb250ZXh0LnBhcmVudE5vZGUpfHxjb250ZXh0KTtyZXR1cm4gcmVzdWx0czt9O3N1cHBvcnQuc29ydFN0YWJsZT1leHBhbmRvLnNwbGl0KFwiXCIpLnNvcnQoc29ydE9yZGVyKS5qb2luKFwiXCIpPT09ZXhwYW5kbztzdXBwb3J0LmRldGVjdER1cGxpY2F0ZXM9ISFoYXNEdXBsaWNhdGU7c2V0RG9jdW1lbnQoKTtzdXBwb3J0LnNvcnREZXRhY2hlZD1hc3NlcnQoZnVuY3Rpb24oZWwpe3JldHVybiBlbC5jb21wYXJlRG9jdW1lbnRQb3NpdGlvbihkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZmllbGRzZXRcIikpJjE7fSk7aWYoIWFzc2VydChmdW5jdGlvbihlbCl7ZWwuaW5uZXJIVE1MPVwiPGEgaHJlZj0nIyc+PC9hPlwiO3JldHVybiBlbC5maXJzdENoaWxkLmdldEF0dHJpYnV0ZShcImhyZWZcIik9PT1cIiNcIjt9KSl7YWRkSGFuZGxlKFwidHlwZXxocmVmfGhlaWdodHx3aWR0aFwiLGZ1bmN0aW9uKGVsZW0sbmFtZSxpc1hNTCl7aWYoIWlzWE1MKXtyZXR1cm4gZWxlbS5nZXRBdHRyaWJ1dGUobmFtZSxuYW1lLnRvTG93ZXJDYXNlKCk9PT1cInR5cGVcIj8xOjIpO319KTt9XG5pZighc3VwcG9ydC5hdHRyaWJ1dGVzfHwhYXNzZXJ0KGZ1bmN0aW9uKGVsKXtlbC5pbm5lckhUTUw9XCI8aW5wdXQvPlwiO2VsLmZpcnN0Q2hpbGQuc2V0QXR0cmlidXRlKFwidmFsdWVcIixcIlwiKTtyZXR1cm4gZWwuZmlyc3RDaGlsZC5nZXRBdHRyaWJ1dGUoXCJ2YWx1ZVwiKT09PVwiXCI7fSkpe2FkZEhhbmRsZShcInZhbHVlXCIsZnVuY3Rpb24oZWxlbSxfbmFtZSxpc1hNTCl7aWYoIWlzWE1MJiZlbGVtLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk9PT1cImlucHV0XCIpe3JldHVybiBlbGVtLmRlZmF1bHRWYWx1ZTt9fSk7fVxuaWYoIWFzc2VydChmdW5jdGlvbihlbCl7cmV0dXJuIGVsLmdldEF0dHJpYnV0ZShcImRpc2FibGVkXCIpPT1udWxsO30pKXthZGRIYW5kbGUoYm9vbGVhbnMsZnVuY3Rpb24oZWxlbSxuYW1lLGlzWE1MKXt2YXIgdmFsO2lmKCFpc1hNTCl7cmV0dXJuIGVsZW1bbmFtZV09PT10cnVlP25hbWUudG9Mb3dlckNhc2UoKToodmFsPWVsZW0uZ2V0QXR0cmlidXRlTm9kZShuYW1lKSkmJnZhbC5zcGVjaWZpZWQ/dmFsLnZhbHVlOm51bGw7fX0pO31cbnJldHVybiBTaXp6bGU7fSkod2luZG93KTtqUXVlcnkuZmluZD1TaXp6bGU7alF1ZXJ5LmV4cHI9U2l6emxlLnNlbGVjdG9ycztqUXVlcnkuZXhwcltcIjpcIl09alF1ZXJ5LmV4cHIucHNldWRvcztqUXVlcnkudW5pcXVlU29ydD1qUXVlcnkudW5pcXVlPVNpenpsZS51bmlxdWVTb3J0O2pRdWVyeS50ZXh0PVNpenpsZS5nZXRUZXh0O2pRdWVyeS5pc1hNTERvYz1TaXp6bGUuaXNYTUw7alF1ZXJ5LmNvbnRhaW5zPVNpenpsZS5jb250YWlucztqUXVlcnkuZXNjYXBlU2VsZWN0b3I9U2l6emxlLmVzY2FwZTt2YXIgZGlyPWZ1bmN0aW9uKGVsZW0sZGlyLHVudGlsKXt2YXIgbWF0Y2hlZD1bXSx0cnVuY2F0ZT11bnRpbCE9PXVuZGVmaW5lZDt3aGlsZSgoZWxlbT1lbGVtW2Rpcl0pJiZlbGVtLm5vZGVUeXBlIT09OSl7aWYoZWxlbS5ub2RlVHlwZT09PTEpe2lmKHRydW5jYXRlJiZqUXVlcnkoZWxlbSkuaXModW50aWwpKXticmVhazt9XG5tYXRjaGVkLnB1c2goZWxlbSk7fX1cbnJldHVybiBtYXRjaGVkO307dmFyIHNpYmxpbmdzPWZ1bmN0aW9uKG4sZWxlbSl7dmFyIG1hdGNoZWQ9W107Zm9yKDtuO249bi5uZXh0U2libGluZyl7aWYobi5ub2RlVHlwZT09PTEmJm4hPT1lbGVtKXttYXRjaGVkLnB1c2gobik7fX1cbnJldHVybiBtYXRjaGVkO307dmFyIHJuZWVkc0NvbnRleHQ9alF1ZXJ5LmV4cHIubWF0Y2gubmVlZHNDb250ZXh0O2Z1bmN0aW9uIG5vZGVOYW1lKGVsZW0sbmFtZSl7cmV0dXJuIGVsZW0ubm9kZU5hbWUmJmVsZW0ubm9kZU5hbWUudG9Mb3dlckNhc2UoKT09PW5hbWUudG9Mb3dlckNhc2UoKTt9O3ZhciByc2luZ2xlVGFnPSgvXjwoW2Etel1bXlxcL1xcMD46XFx4MjBcXHRcXHJcXG5cXGZdKilbXFx4MjBcXHRcXHJcXG5cXGZdKlxcLz8+KD86PFxcL1xcMT58KSQvaSk7ZnVuY3Rpb24gd2lubm93KGVsZW1lbnRzLHF1YWxpZmllcixub3Qpe2lmKGlzRnVuY3Rpb24ocXVhbGlmaWVyKSl7cmV0dXJuIGpRdWVyeS5ncmVwKGVsZW1lbnRzLGZ1bmN0aW9uKGVsZW0saSl7cmV0dXJuISFxdWFsaWZpZXIuY2FsbChlbGVtLGksZWxlbSkhPT1ub3Q7fSk7fVxuaWYocXVhbGlmaWVyLm5vZGVUeXBlKXtyZXR1cm4galF1ZXJ5LmdyZXAoZWxlbWVudHMsZnVuY3Rpb24oZWxlbSl7cmV0dXJuKGVsZW09PT1xdWFsaWZpZXIpIT09bm90O30pO31cbmlmKHR5cGVvZiBxdWFsaWZpZXIhPT1cInN0cmluZ1wiKXtyZXR1cm4galF1ZXJ5LmdyZXAoZWxlbWVudHMsZnVuY3Rpb24oZWxlbSl7cmV0dXJuKGluZGV4T2YuY2FsbChxdWFsaWZpZXIsZWxlbSk+LTEpIT09bm90O30pO31cbnJldHVybiBqUXVlcnkuZmlsdGVyKHF1YWxpZmllcixlbGVtZW50cyxub3QpO31cbmpRdWVyeS5maWx0ZXI9ZnVuY3Rpb24oZXhwcixlbGVtcyxub3Qpe3ZhciBlbGVtPWVsZW1zWzBdO2lmKG5vdCl7ZXhwcj1cIjpub3QoXCIrZXhwcitcIilcIjt9XG5pZihlbGVtcy5sZW5ndGg9PT0xJiZlbGVtLm5vZGVUeXBlPT09MSl7cmV0dXJuIGpRdWVyeS5maW5kLm1hdGNoZXNTZWxlY3RvcihlbGVtLGV4cHIpP1tlbGVtXTpbXTt9XG5yZXR1cm4galF1ZXJ5LmZpbmQubWF0Y2hlcyhleHByLGpRdWVyeS5ncmVwKGVsZW1zLGZ1bmN0aW9uKGVsZW0pe3JldHVybiBlbGVtLm5vZGVUeXBlPT09MTt9KSk7fTtqUXVlcnkuZm4uZXh0ZW5kKHtmaW5kOmZ1bmN0aW9uKHNlbGVjdG9yKXt2YXIgaSxyZXQsbGVuPXRoaXMubGVuZ3RoLHNlbGY9dGhpcztpZih0eXBlb2Ygc2VsZWN0b3IhPT1cInN0cmluZ1wiKXtyZXR1cm4gdGhpcy5wdXNoU3RhY2soalF1ZXJ5KHNlbGVjdG9yKS5maWx0ZXIoZnVuY3Rpb24oKXtmb3IoaT0wO2k8bGVuO2krKyl7aWYoalF1ZXJ5LmNvbnRhaW5zKHNlbGZbaV0sdGhpcykpe3JldHVybiB0cnVlO319fSkpO31cbnJldD10aGlzLnB1c2hTdGFjayhbXSk7Zm9yKGk9MDtpPGxlbjtpKyspe2pRdWVyeS5maW5kKHNlbGVjdG9yLHNlbGZbaV0scmV0KTt9XG5yZXR1cm4gbGVuPjE/alF1ZXJ5LnVuaXF1ZVNvcnQocmV0KTpyZXQ7fSxmaWx0ZXI6ZnVuY3Rpb24oc2VsZWN0b3Ipe3JldHVybiB0aGlzLnB1c2hTdGFjayh3aW5ub3codGhpcyxzZWxlY3Rvcnx8W10sZmFsc2UpKTt9LG5vdDpmdW5jdGlvbihzZWxlY3Rvcil7cmV0dXJuIHRoaXMucHVzaFN0YWNrKHdpbm5vdyh0aGlzLHNlbGVjdG9yfHxbXSx0cnVlKSk7fSxpczpmdW5jdGlvbihzZWxlY3Rvcil7cmV0dXJuISF3aW5ub3codGhpcyx0eXBlb2Ygc2VsZWN0b3I9PT1cInN0cmluZ1wiJiZybmVlZHNDb250ZXh0LnRlc3Qoc2VsZWN0b3IpP2pRdWVyeShzZWxlY3Rvcik6c2VsZWN0b3J8fFtdLGZhbHNlKS5sZW5ndGg7fX0pO3ZhciByb290alF1ZXJ5LHJxdWlja0V4cHI9L14oPzpcXHMqKDxbXFx3XFxXXSs+KVtePl0qfCMoW1xcdy1dKykpJC8saW5pdD1qUXVlcnkuZm4uaW5pdD1mdW5jdGlvbihzZWxlY3Rvcixjb250ZXh0LHJvb3Qpe3ZhciBtYXRjaCxlbGVtO2lmKCFzZWxlY3Rvcil7cmV0dXJuIHRoaXM7fVxucm9vdD1yb290fHxyb290alF1ZXJ5O2lmKHR5cGVvZiBzZWxlY3Rvcj09PVwic3RyaW5nXCIpe2lmKHNlbGVjdG9yWzBdPT09XCI8XCImJnNlbGVjdG9yW3NlbGVjdG9yLmxlbmd0aC0xXT09PVwiPlwiJiZzZWxlY3Rvci5sZW5ndGg+PTMpe21hdGNoPVtudWxsLHNlbGVjdG9yLG51bGxdO31lbHNle21hdGNoPXJxdWlja0V4cHIuZXhlYyhzZWxlY3Rvcik7fVxuaWYobWF0Y2gmJihtYXRjaFsxXXx8IWNvbnRleHQpKXtpZihtYXRjaFsxXSl7Y29udGV4dD1jb250ZXh0IGluc3RhbmNlb2YgalF1ZXJ5P2NvbnRleHRbMF06Y29udGV4dDtqUXVlcnkubWVyZ2UodGhpcyxqUXVlcnkucGFyc2VIVE1MKG1hdGNoWzFdLGNvbnRleHQmJmNvbnRleHQubm9kZVR5cGU/Y29udGV4dC5vd25lckRvY3VtZW50fHxjb250ZXh0OmRvY3VtZW50LHRydWUpKTtpZihyc2luZ2xlVGFnLnRlc3QobWF0Y2hbMV0pJiZqUXVlcnkuaXNQbGFpbk9iamVjdChjb250ZXh0KSl7Zm9yKG1hdGNoIGluIGNvbnRleHQpe2lmKGlzRnVuY3Rpb24odGhpc1ttYXRjaF0pKXt0aGlzW21hdGNoXShjb250ZXh0W21hdGNoXSk7fWVsc2V7dGhpcy5hdHRyKG1hdGNoLGNvbnRleHRbbWF0Y2hdKTt9fX1cbnJldHVybiB0aGlzO31lbHNle2VsZW09ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQobWF0Y2hbMl0pO2lmKGVsZW0pe3RoaXNbMF09ZWxlbTt0aGlzLmxlbmd0aD0xO31cbnJldHVybiB0aGlzO319ZWxzZSBpZighY29udGV4dHx8Y29udGV4dC5qcXVlcnkpe3JldHVybihjb250ZXh0fHxyb290KS5maW5kKHNlbGVjdG9yKTt9ZWxzZXtyZXR1cm4gdGhpcy5jb25zdHJ1Y3Rvcihjb250ZXh0KS5maW5kKHNlbGVjdG9yKTt9fWVsc2UgaWYoc2VsZWN0b3Iubm9kZVR5cGUpe3RoaXNbMF09c2VsZWN0b3I7dGhpcy5sZW5ndGg9MTtyZXR1cm4gdGhpczt9ZWxzZSBpZihpc0Z1bmN0aW9uKHNlbGVjdG9yKSl7cmV0dXJuIHJvb3QucmVhZHkhPT11bmRlZmluZWQ/cm9vdC5yZWFkeShzZWxlY3Rvcik6c2VsZWN0b3IoalF1ZXJ5KTt9XG5yZXR1cm4galF1ZXJ5Lm1ha2VBcnJheShzZWxlY3Rvcix0aGlzKTt9O2luaXQucHJvdG90eXBlPWpRdWVyeS5mbjtyb290alF1ZXJ5PWpRdWVyeShkb2N1bWVudCk7dmFyIHJwYXJlbnRzcHJldj0vXig/OnBhcmVudHN8cHJldig/OlVudGlsfEFsbCkpLyxndWFyYW50ZWVkVW5pcXVlPXtjaGlsZHJlbjp0cnVlLGNvbnRlbnRzOnRydWUsbmV4dDp0cnVlLHByZXY6dHJ1ZX07alF1ZXJ5LmZuLmV4dGVuZCh7aGFzOmZ1bmN0aW9uKHRhcmdldCl7dmFyIHRhcmdldHM9alF1ZXJ5KHRhcmdldCx0aGlzKSxsPXRhcmdldHMubGVuZ3RoO3JldHVybiB0aGlzLmZpbHRlcihmdW5jdGlvbigpe3ZhciBpPTA7Zm9yKDtpPGw7aSsrKXtpZihqUXVlcnkuY29udGFpbnModGhpcyx0YXJnZXRzW2ldKSl7cmV0dXJuIHRydWU7fX19KTt9LGNsb3Nlc3Q6ZnVuY3Rpb24oc2VsZWN0b3JzLGNvbnRleHQpe3ZhciBjdXIsaT0wLGw9dGhpcy5sZW5ndGgsbWF0Y2hlZD1bXSx0YXJnZXRzPXR5cGVvZiBzZWxlY3RvcnMhPT1cInN0cmluZ1wiJiZqUXVlcnkoc2VsZWN0b3JzKTtpZighcm5lZWRzQ29udGV4dC50ZXN0KHNlbGVjdG9ycykpe2Zvcig7aTxsO2krKyl7Zm9yKGN1cj10aGlzW2ldO2N1ciYmY3VyIT09Y29udGV4dDtjdXI9Y3VyLnBhcmVudE5vZGUpe2lmKGN1ci5ub2RlVHlwZTwxMSYmKHRhcmdldHM/dGFyZ2V0cy5pbmRleChjdXIpPi0xOmN1ci5ub2RlVHlwZT09PTEmJmpRdWVyeS5maW5kLm1hdGNoZXNTZWxlY3RvcihjdXIsc2VsZWN0b3JzKSkpe21hdGNoZWQucHVzaChjdXIpO2JyZWFrO319fX1cbnJldHVybiB0aGlzLnB1c2hTdGFjayhtYXRjaGVkLmxlbmd0aD4xP2pRdWVyeS51bmlxdWVTb3J0KG1hdGNoZWQpOm1hdGNoZWQpO30saW5kZXg6ZnVuY3Rpb24oZWxlbSl7aWYoIWVsZW0pe3JldHVybih0aGlzWzBdJiZ0aGlzWzBdLnBhcmVudE5vZGUpP3RoaXMuZmlyc3QoKS5wcmV2QWxsKCkubGVuZ3RoOi0xO31cbmlmKHR5cGVvZiBlbGVtPT09XCJzdHJpbmdcIil7cmV0dXJuIGluZGV4T2YuY2FsbChqUXVlcnkoZWxlbSksdGhpc1swXSk7fVxucmV0dXJuIGluZGV4T2YuY2FsbCh0aGlzLGVsZW0uanF1ZXJ5P2VsZW1bMF06ZWxlbSk7fSxhZGQ6ZnVuY3Rpb24oc2VsZWN0b3IsY29udGV4dCl7cmV0dXJuIHRoaXMucHVzaFN0YWNrKGpRdWVyeS51bmlxdWVTb3J0KGpRdWVyeS5tZXJnZSh0aGlzLmdldCgpLGpRdWVyeShzZWxlY3Rvcixjb250ZXh0KSkpKTt9LGFkZEJhY2s6ZnVuY3Rpb24oc2VsZWN0b3Ipe3JldHVybiB0aGlzLmFkZChzZWxlY3Rvcj09bnVsbD90aGlzLnByZXZPYmplY3Q6dGhpcy5wcmV2T2JqZWN0LmZpbHRlcihzZWxlY3RvcikpO319KTtmdW5jdGlvbiBzaWJsaW5nKGN1cixkaXIpe3doaWxlKChjdXI9Y3VyW2Rpcl0pJiZjdXIubm9kZVR5cGUhPT0xKXt9XG5yZXR1cm4gY3VyO31cbmpRdWVyeS5lYWNoKHtwYXJlbnQ6ZnVuY3Rpb24oZWxlbSl7dmFyIHBhcmVudD1lbGVtLnBhcmVudE5vZGU7cmV0dXJuIHBhcmVudCYmcGFyZW50Lm5vZGVUeXBlIT09MTE/cGFyZW50Om51bGw7fSxwYXJlbnRzOmZ1bmN0aW9uKGVsZW0pe3JldHVybiBkaXIoZWxlbSxcInBhcmVudE5vZGVcIik7fSxwYXJlbnRzVW50aWw6ZnVuY3Rpb24oZWxlbSxfaSx1bnRpbCl7cmV0dXJuIGRpcihlbGVtLFwicGFyZW50Tm9kZVwiLHVudGlsKTt9LG5leHQ6ZnVuY3Rpb24oZWxlbSl7cmV0dXJuIHNpYmxpbmcoZWxlbSxcIm5leHRTaWJsaW5nXCIpO30scHJldjpmdW5jdGlvbihlbGVtKXtyZXR1cm4gc2libGluZyhlbGVtLFwicHJldmlvdXNTaWJsaW5nXCIpO30sbmV4dEFsbDpmdW5jdGlvbihlbGVtKXtyZXR1cm4gZGlyKGVsZW0sXCJuZXh0U2libGluZ1wiKTt9LHByZXZBbGw6ZnVuY3Rpb24oZWxlbSl7cmV0dXJuIGRpcihlbGVtLFwicHJldmlvdXNTaWJsaW5nXCIpO30sbmV4dFVudGlsOmZ1bmN0aW9uKGVsZW0sX2ksdW50aWwpe3JldHVybiBkaXIoZWxlbSxcIm5leHRTaWJsaW5nXCIsdW50aWwpO30scHJldlVudGlsOmZ1bmN0aW9uKGVsZW0sX2ksdW50aWwpe3JldHVybiBkaXIoZWxlbSxcInByZXZpb3VzU2libGluZ1wiLHVudGlsKTt9LHNpYmxpbmdzOmZ1bmN0aW9uKGVsZW0pe3JldHVybiBzaWJsaW5ncygoZWxlbS5wYXJlbnROb2RlfHx7fSkuZmlyc3RDaGlsZCxlbGVtKTt9LGNoaWxkcmVuOmZ1bmN0aW9uKGVsZW0pe3JldHVybiBzaWJsaW5ncyhlbGVtLmZpcnN0Q2hpbGQpO30sY29udGVudHM6ZnVuY3Rpb24oZWxlbSl7aWYoZWxlbS5jb250ZW50RG9jdW1lbnQhPW51bGwmJmdldFByb3RvKGVsZW0uY29udGVudERvY3VtZW50KSl7cmV0dXJuIGVsZW0uY29udGVudERvY3VtZW50O31cbmlmKG5vZGVOYW1lKGVsZW0sXCJ0ZW1wbGF0ZVwiKSl7ZWxlbT1lbGVtLmNvbnRlbnR8fGVsZW07fVxucmV0dXJuIGpRdWVyeS5tZXJnZShbXSxlbGVtLmNoaWxkTm9kZXMpO319LGZ1bmN0aW9uKG5hbWUsZm4pe2pRdWVyeS5mbltuYW1lXT1mdW5jdGlvbih1bnRpbCxzZWxlY3Rvcil7dmFyIG1hdGNoZWQ9alF1ZXJ5Lm1hcCh0aGlzLGZuLHVudGlsKTtpZihuYW1lLnNsaWNlKC01KSE9PVwiVW50aWxcIil7c2VsZWN0b3I9dW50aWw7fVxuaWYoc2VsZWN0b3ImJnR5cGVvZiBzZWxlY3Rvcj09PVwic3RyaW5nXCIpe21hdGNoZWQ9alF1ZXJ5LmZpbHRlcihzZWxlY3RvcixtYXRjaGVkKTt9XG5pZih0aGlzLmxlbmd0aD4xKXtpZighZ3VhcmFudGVlZFVuaXF1ZVtuYW1lXSl7alF1ZXJ5LnVuaXF1ZVNvcnQobWF0Y2hlZCk7fVxuaWYocnBhcmVudHNwcmV2LnRlc3QobmFtZSkpe21hdGNoZWQucmV2ZXJzZSgpO319XG5yZXR1cm4gdGhpcy5wdXNoU3RhY2sobWF0Y2hlZCk7fTt9KTt2YXIgcm5vdGh0bWx3aGl0ZT0oL1teXFx4MjBcXHRcXHJcXG5cXGZdKy9nKTtmdW5jdGlvbiBjcmVhdGVPcHRpb25zKG9wdGlvbnMpe3ZhciBvYmplY3Q9e307alF1ZXJ5LmVhY2gob3B0aW9ucy5tYXRjaChybm90aHRtbHdoaXRlKXx8W10sZnVuY3Rpb24oXyxmbGFnKXtvYmplY3RbZmxhZ109dHJ1ZTt9KTtyZXR1cm4gb2JqZWN0O31cbmpRdWVyeS5DYWxsYmFja3M9ZnVuY3Rpb24ob3B0aW9ucyl7b3B0aW9ucz10eXBlb2Ygb3B0aW9ucz09PVwic3RyaW5nXCI/Y3JlYXRlT3B0aW9ucyhvcHRpb25zKTpqUXVlcnkuZXh0ZW5kKHt9LG9wdGlvbnMpO3ZhclxuZmlyaW5nLG1lbW9yeSxmaXJlZCxsb2NrZWQsbGlzdD1bXSxxdWV1ZT1bXSxmaXJpbmdJbmRleD0tMSxmaXJlPWZ1bmN0aW9uKCl7bG9ja2VkPWxvY2tlZHx8b3B0aW9ucy5vbmNlO2ZpcmVkPWZpcmluZz10cnVlO2Zvcig7cXVldWUubGVuZ3RoO2ZpcmluZ0luZGV4PS0xKXttZW1vcnk9cXVldWUuc2hpZnQoKTt3aGlsZSgrK2ZpcmluZ0luZGV4PGxpc3QubGVuZ3RoKXtpZihsaXN0W2ZpcmluZ0luZGV4XS5hcHBseShtZW1vcnlbMF0sbWVtb3J5WzFdKT09PWZhbHNlJiZvcHRpb25zLnN0b3BPbkZhbHNlKXtmaXJpbmdJbmRleD1saXN0Lmxlbmd0aDttZW1vcnk9ZmFsc2U7fX19XG5pZighb3B0aW9ucy5tZW1vcnkpe21lbW9yeT1mYWxzZTt9XG5maXJpbmc9ZmFsc2U7aWYobG9ja2VkKXtpZihtZW1vcnkpe2xpc3Q9W107fWVsc2V7bGlzdD1cIlwiO319fSxzZWxmPXthZGQ6ZnVuY3Rpb24oKXtpZihsaXN0KXtpZihtZW1vcnkmJiFmaXJpbmcpe2ZpcmluZ0luZGV4PWxpc3QubGVuZ3RoLTE7cXVldWUucHVzaChtZW1vcnkpO31cbihmdW5jdGlvbiBhZGQoYXJncyl7alF1ZXJ5LmVhY2goYXJncyxmdW5jdGlvbihfLGFyZyl7aWYoaXNGdW5jdGlvbihhcmcpKXtpZighb3B0aW9ucy51bmlxdWV8fCFzZWxmLmhhcyhhcmcpKXtsaXN0LnB1c2goYXJnKTt9fWVsc2UgaWYoYXJnJiZhcmcubGVuZ3RoJiZ0b1R5cGUoYXJnKSE9PVwic3RyaW5nXCIpe2FkZChhcmcpO319KTt9KShhcmd1bWVudHMpO2lmKG1lbW9yeSYmIWZpcmluZyl7ZmlyZSgpO319XG5yZXR1cm4gdGhpczt9LHJlbW92ZTpmdW5jdGlvbigpe2pRdWVyeS5lYWNoKGFyZ3VtZW50cyxmdW5jdGlvbihfLGFyZyl7dmFyIGluZGV4O3doaWxlKChpbmRleD1qUXVlcnkuaW5BcnJheShhcmcsbGlzdCxpbmRleCkpPi0xKXtsaXN0LnNwbGljZShpbmRleCwxKTtpZihpbmRleDw9ZmlyaW5nSW5kZXgpe2ZpcmluZ0luZGV4LS07fX19KTtyZXR1cm4gdGhpczt9LGhhczpmdW5jdGlvbihmbil7cmV0dXJuIGZuP2pRdWVyeS5pbkFycmF5KGZuLGxpc3QpPi0xOmxpc3QubGVuZ3RoPjA7fSxlbXB0eTpmdW5jdGlvbigpe2lmKGxpc3Qpe2xpc3Q9W107fVxucmV0dXJuIHRoaXM7fSxkaXNhYmxlOmZ1bmN0aW9uKCl7bG9ja2VkPXF1ZXVlPVtdO2xpc3Q9bWVtb3J5PVwiXCI7cmV0dXJuIHRoaXM7fSxkaXNhYmxlZDpmdW5jdGlvbigpe3JldHVybiFsaXN0O30sbG9jazpmdW5jdGlvbigpe2xvY2tlZD1xdWV1ZT1bXTtpZighbWVtb3J5JiYhZmlyaW5nKXtsaXN0PW1lbW9yeT1cIlwiO31cbnJldHVybiB0aGlzO30sbG9ja2VkOmZ1bmN0aW9uKCl7cmV0dXJuISFsb2NrZWQ7fSxmaXJlV2l0aDpmdW5jdGlvbihjb250ZXh0LGFyZ3Mpe2lmKCFsb2NrZWQpe2FyZ3M9YXJnc3x8W107YXJncz1bY29udGV4dCxhcmdzLnNsaWNlP2FyZ3Muc2xpY2UoKTphcmdzXTtxdWV1ZS5wdXNoKGFyZ3MpO2lmKCFmaXJpbmcpe2ZpcmUoKTt9fVxucmV0dXJuIHRoaXM7fSxmaXJlOmZ1bmN0aW9uKCl7c2VsZi5maXJlV2l0aCh0aGlzLGFyZ3VtZW50cyk7cmV0dXJuIHRoaXM7fSxmaXJlZDpmdW5jdGlvbigpe3JldHVybiEhZmlyZWQ7fX07cmV0dXJuIHNlbGY7fTtmdW5jdGlvbiBJZGVudGl0eSh2KXtyZXR1cm4gdjt9XG5mdW5jdGlvbiBUaHJvd2VyKGV4KXt0aHJvdyBleDt9XG5mdW5jdGlvbiBhZG9wdFZhbHVlKHZhbHVlLHJlc29sdmUscmVqZWN0LG5vVmFsdWUpe3ZhciBtZXRob2Q7dHJ5e2lmKHZhbHVlJiZpc0Z1bmN0aW9uKChtZXRob2Q9dmFsdWUucHJvbWlzZSkpKXttZXRob2QuY2FsbCh2YWx1ZSkuZG9uZShyZXNvbHZlKS5mYWlsKHJlamVjdCk7fWVsc2UgaWYodmFsdWUmJmlzRnVuY3Rpb24oKG1ldGhvZD12YWx1ZS50aGVuKSkpe21ldGhvZC5jYWxsKHZhbHVlLHJlc29sdmUscmVqZWN0KTt9ZWxzZXtyZXNvbHZlLmFwcGx5KHVuZGVmaW5lZCxbdmFsdWVdLnNsaWNlKG5vVmFsdWUpKTt9fWNhdGNoKHZhbHVlKXtyZWplY3QuYXBwbHkodW5kZWZpbmVkLFt2YWx1ZV0pO319XG5qUXVlcnkuZXh0ZW5kKHtEZWZlcnJlZDpmdW5jdGlvbihmdW5jKXt2YXIgdHVwbGVzPVtbXCJub3RpZnlcIixcInByb2dyZXNzXCIsalF1ZXJ5LkNhbGxiYWNrcyhcIm1lbW9yeVwiKSxqUXVlcnkuQ2FsbGJhY2tzKFwibWVtb3J5XCIpLDJdLFtcInJlc29sdmVcIixcImRvbmVcIixqUXVlcnkuQ2FsbGJhY2tzKFwib25jZSBtZW1vcnlcIiksalF1ZXJ5LkNhbGxiYWNrcyhcIm9uY2UgbWVtb3J5XCIpLDAsXCJyZXNvbHZlZFwiXSxbXCJyZWplY3RcIixcImZhaWxcIixqUXVlcnkuQ2FsbGJhY2tzKFwib25jZSBtZW1vcnlcIiksalF1ZXJ5LkNhbGxiYWNrcyhcIm9uY2UgbWVtb3J5XCIpLDEsXCJyZWplY3RlZFwiXV0sc3RhdGU9XCJwZW5kaW5nXCIscHJvbWlzZT17c3RhdGU6ZnVuY3Rpb24oKXtyZXR1cm4gc3RhdGU7fSxhbHdheXM6ZnVuY3Rpb24oKXtkZWZlcnJlZC5kb25lKGFyZ3VtZW50cykuZmFpbChhcmd1bWVudHMpO3JldHVybiB0aGlzO30sXCJjYXRjaFwiOmZ1bmN0aW9uKGZuKXtyZXR1cm4gcHJvbWlzZS50aGVuKG51bGwsZm4pO30scGlwZTpmdW5jdGlvbigpe3ZhciBmbnM9YXJndW1lbnRzO3JldHVybiBqUXVlcnkuRGVmZXJyZWQoZnVuY3Rpb24obmV3RGVmZXIpe2pRdWVyeS5lYWNoKHR1cGxlcyxmdW5jdGlvbihfaSx0dXBsZSl7dmFyIGZuPWlzRnVuY3Rpb24oZm5zW3R1cGxlWzRdXSkmJmZuc1t0dXBsZVs0XV07ZGVmZXJyZWRbdHVwbGVbMV1dKGZ1bmN0aW9uKCl7dmFyIHJldHVybmVkPWZuJiZmbi5hcHBseSh0aGlzLGFyZ3VtZW50cyk7aWYocmV0dXJuZWQmJmlzRnVuY3Rpb24ocmV0dXJuZWQucHJvbWlzZSkpe3JldHVybmVkLnByb21pc2UoKS5wcm9ncmVzcyhuZXdEZWZlci5ub3RpZnkpLmRvbmUobmV3RGVmZXIucmVzb2x2ZSkuZmFpbChuZXdEZWZlci5yZWplY3QpO31lbHNle25ld0RlZmVyW3R1cGxlWzBdK1wiV2l0aFwiXSh0aGlzLGZuP1tyZXR1cm5lZF06YXJndW1lbnRzKTt9fSk7fSk7Zm5zPW51bGw7fSkucHJvbWlzZSgpO30sdGhlbjpmdW5jdGlvbihvbkZ1bGZpbGxlZCxvblJlamVjdGVkLG9uUHJvZ3Jlc3Mpe3ZhciBtYXhEZXB0aD0wO2Z1bmN0aW9uIHJlc29sdmUoZGVwdGgsZGVmZXJyZWQsaGFuZGxlcixzcGVjaWFsKXtyZXR1cm4gZnVuY3Rpb24oKXt2YXIgdGhhdD10aGlzLGFyZ3M9YXJndW1lbnRzLG1pZ2h0VGhyb3c9ZnVuY3Rpb24oKXt2YXIgcmV0dXJuZWQsdGhlbjtpZihkZXB0aDxtYXhEZXB0aCl7cmV0dXJuO31cbnJldHVybmVkPWhhbmRsZXIuYXBwbHkodGhhdCxhcmdzKTtpZihyZXR1cm5lZD09PWRlZmVycmVkLnByb21pc2UoKSl7dGhyb3cgbmV3IFR5cGVFcnJvcihcIlRoZW5hYmxlIHNlbGYtcmVzb2x1dGlvblwiKTt9XG50aGVuPXJldHVybmVkJiYodHlwZW9mIHJldHVybmVkPT09XCJvYmplY3RcInx8dHlwZW9mIHJldHVybmVkPT09XCJmdW5jdGlvblwiKSYmcmV0dXJuZWQudGhlbjtpZihpc0Z1bmN0aW9uKHRoZW4pKXtpZihzcGVjaWFsKXt0aGVuLmNhbGwocmV0dXJuZWQscmVzb2x2ZShtYXhEZXB0aCxkZWZlcnJlZCxJZGVudGl0eSxzcGVjaWFsKSxyZXNvbHZlKG1heERlcHRoLGRlZmVycmVkLFRocm93ZXIsc3BlY2lhbCkpO31lbHNle21heERlcHRoKys7dGhlbi5jYWxsKHJldHVybmVkLHJlc29sdmUobWF4RGVwdGgsZGVmZXJyZWQsSWRlbnRpdHksc3BlY2lhbCkscmVzb2x2ZShtYXhEZXB0aCxkZWZlcnJlZCxUaHJvd2VyLHNwZWNpYWwpLHJlc29sdmUobWF4RGVwdGgsZGVmZXJyZWQsSWRlbnRpdHksZGVmZXJyZWQubm90aWZ5V2l0aCkpO319ZWxzZXtpZihoYW5kbGVyIT09SWRlbnRpdHkpe3RoYXQ9dW5kZWZpbmVkO2FyZ3M9W3JldHVybmVkXTt9XG4oc3BlY2lhbHx8ZGVmZXJyZWQucmVzb2x2ZVdpdGgpKHRoYXQsYXJncyk7fX0scHJvY2Vzcz1zcGVjaWFsP21pZ2h0VGhyb3c6ZnVuY3Rpb24oKXt0cnl7bWlnaHRUaHJvdygpO31jYXRjaChlKXtpZihqUXVlcnkuRGVmZXJyZWQuZXhjZXB0aW9uSG9vayl7alF1ZXJ5LkRlZmVycmVkLmV4Y2VwdGlvbkhvb2soZSxwcm9jZXNzLnN0YWNrVHJhY2UpO31cbmlmKGRlcHRoKzE+PW1heERlcHRoKXtpZihoYW5kbGVyIT09VGhyb3dlcil7dGhhdD11bmRlZmluZWQ7YXJncz1bZV07fVxuZGVmZXJyZWQucmVqZWN0V2l0aCh0aGF0LGFyZ3MpO319fTtpZihkZXB0aCl7cHJvY2VzcygpO31lbHNle2lmKGpRdWVyeS5EZWZlcnJlZC5nZXRTdGFja0hvb2spe3Byb2Nlc3Muc3RhY2tUcmFjZT1qUXVlcnkuRGVmZXJyZWQuZ2V0U3RhY2tIb29rKCk7fVxud2luZG93LnNldFRpbWVvdXQocHJvY2Vzcyk7fX07fVxucmV0dXJuIGpRdWVyeS5EZWZlcnJlZChmdW5jdGlvbihuZXdEZWZlcil7dHVwbGVzWzBdWzNdLmFkZChyZXNvbHZlKDAsbmV3RGVmZXIsaXNGdW5jdGlvbihvblByb2dyZXNzKT9vblByb2dyZXNzOklkZW50aXR5LG5ld0RlZmVyLm5vdGlmeVdpdGgpKTt0dXBsZXNbMV1bM10uYWRkKHJlc29sdmUoMCxuZXdEZWZlcixpc0Z1bmN0aW9uKG9uRnVsZmlsbGVkKT9vbkZ1bGZpbGxlZDpJZGVudGl0eSkpO3R1cGxlc1syXVszXS5hZGQocmVzb2x2ZSgwLG5ld0RlZmVyLGlzRnVuY3Rpb24ob25SZWplY3RlZCk/b25SZWplY3RlZDpUaHJvd2VyKSk7fSkucHJvbWlzZSgpO30scHJvbWlzZTpmdW5jdGlvbihvYmope3JldHVybiBvYmohPW51bGw/alF1ZXJ5LmV4dGVuZChvYmoscHJvbWlzZSk6cHJvbWlzZTt9fSxkZWZlcnJlZD17fTtqUXVlcnkuZWFjaCh0dXBsZXMsZnVuY3Rpb24oaSx0dXBsZSl7dmFyIGxpc3Q9dHVwbGVbMl0sc3RhdGVTdHJpbmc9dHVwbGVbNV07cHJvbWlzZVt0dXBsZVsxXV09bGlzdC5hZGQ7aWYoc3RhdGVTdHJpbmcpe2xpc3QuYWRkKGZ1bmN0aW9uKCl7c3RhdGU9c3RhdGVTdHJpbmc7fSx0dXBsZXNbMy1pXVsyXS5kaXNhYmxlLHR1cGxlc1szLWldWzNdLmRpc2FibGUsdHVwbGVzWzBdWzJdLmxvY2ssdHVwbGVzWzBdWzNdLmxvY2spO31cbmxpc3QuYWRkKHR1cGxlWzNdLmZpcmUpO2RlZmVycmVkW3R1cGxlWzBdXT1mdW5jdGlvbigpe2RlZmVycmVkW3R1cGxlWzBdK1wiV2l0aFwiXSh0aGlzPT09ZGVmZXJyZWQ/dW5kZWZpbmVkOnRoaXMsYXJndW1lbnRzKTtyZXR1cm4gdGhpczt9O2RlZmVycmVkW3R1cGxlWzBdK1wiV2l0aFwiXT1saXN0LmZpcmVXaXRoO30pO3Byb21pc2UucHJvbWlzZShkZWZlcnJlZCk7aWYoZnVuYyl7ZnVuYy5jYWxsKGRlZmVycmVkLGRlZmVycmVkKTt9XG5yZXR1cm4gZGVmZXJyZWQ7fSx3aGVuOmZ1bmN0aW9uKHNpbmdsZVZhbHVlKXt2YXJcbnJlbWFpbmluZz1hcmd1bWVudHMubGVuZ3RoLGk9cmVtYWluaW5nLHJlc29sdmVDb250ZXh0cz1BcnJheShpKSxyZXNvbHZlVmFsdWVzPXNsaWNlLmNhbGwoYXJndW1lbnRzKSxtYXN0ZXI9alF1ZXJ5LkRlZmVycmVkKCksdXBkYXRlRnVuYz1mdW5jdGlvbihpKXtyZXR1cm4gZnVuY3Rpb24odmFsdWUpe3Jlc29sdmVDb250ZXh0c1tpXT10aGlzO3Jlc29sdmVWYWx1ZXNbaV09YXJndW1lbnRzLmxlbmd0aD4xP3NsaWNlLmNhbGwoYXJndW1lbnRzKTp2YWx1ZTtpZighKC0tcmVtYWluaW5nKSl7bWFzdGVyLnJlc29sdmVXaXRoKHJlc29sdmVDb250ZXh0cyxyZXNvbHZlVmFsdWVzKTt9fTt9O2lmKHJlbWFpbmluZzw9MSl7YWRvcHRWYWx1ZShzaW5nbGVWYWx1ZSxtYXN0ZXIuZG9uZSh1cGRhdGVGdW5jKGkpKS5yZXNvbHZlLG1hc3Rlci5yZWplY3QsIXJlbWFpbmluZyk7aWYobWFzdGVyLnN0YXRlKCk9PT1cInBlbmRpbmdcInx8aXNGdW5jdGlvbihyZXNvbHZlVmFsdWVzW2ldJiZyZXNvbHZlVmFsdWVzW2ldLnRoZW4pKXtyZXR1cm4gbWFzdGVyLnRoZW4oKTt9fVxud2hpbGUoaS0tKXthZG9wdFZhbHVlKHJlc29sdmVWYWx1ZXNbaV0sdXBkYXRlRnVuYyhpKSxtYXN0ZXIucmVqZWN0KTt9XG5yZXR1cm4gbWFzdGVyLnByb21pc2UoKTt9fSk7dmFyIHJlcnJvck5hbWVzPS9eKEV2YWx8SW50ZXJuYWx8UmFuZ2V8UmVmZXJlbmNlfFN5bnRheHxUeXBlfFVSSSlFcnJvciQvO2pRdWVyeS5EZWZlcnJlZC5leGNlcHRpb25Ib29rPWZ1bmN0aW9uKGVycm9yLHN0YWNrKXtpZih3aW5kb3cuY29uc29sZSYmd2luZG93LmNvbnNvbGUud2FybiYmZXJyb3ImJnJlcnJvck5hbWVzLnRlc3QoZXJyb3IubmFtZSkpe3dpbmRvdy5jb25zb2xlLndhcm4oXCJqUXVlcnkuRGVmZXJyZWQgZXhjZXB0aW9uOiBcIitlcnJvci5tZXNzYWdlLGVycm9yLnN0YWNrLHN0YWNrKTt9fTtqUXVlcnkucmVhZHlFeGNlcHRpb249ZnVuY3Rpb24oZXJyb3Ipe3dpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uKCl7dGhyb3cgZXJyb3I7fSk7fTt2YXIgcmVhZHlMaXN0PWpRdWVyeS5EZWZlcnJlZCgpO2pRdWVyeS5mbi5yZWFkeT1mdW5jdGlvbihmbil7cmVhZHlMaXN0LnRoZW4oZm4pLmNhdGNoKGZ1bmN0aW9uKGVycm9yKXtqUXVlcnkucmVhZHlFeGNlcHRpb24oZXJyb3IpO30pO3JldHVybiB0aGlzO307alF1ZXJ5LmV4dGVuZCh7aXNSZWFkeTpmYWxzZSxyZWFkeVdhaXQ6MSxyZWFkeTpmdW5jdGlvbih3YWl0KXtpZih3YWl0PT09dHJ1ZT8tLWpRdWVyeS5yZWFkeVdhaXQ6alF1ZXJ5LmlzUmVhZHkpe3JldHVybjt9XG5qUXVlcnkuaXNSZWFkeT10cnVlO2lmKHdhaXQhPT10cnVlJiYtLWpRdWVyeS5yZWFkeVdhaXQ+MCl7cmV0dXJuO31cbnJlYWR5TGlzdC5yZXNvbHZlV2l0aChkb2N1bWVudCxbalF1ZXJ5XSk7fX0pO2pRdWVyeS5yZWFkeS50aGVuPXJlYWR5TGlzdC50aGVuO2Z1bmN0aW9uIGNvbXBsZXRlZCgpe2RvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsY29tcGxldGVkKTt3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImxvYWRcIixjb21wbGV0ZWQpO2pRdWVyeS5yZWFkeSgpO31cbmlmKGRvY3VtZW50LnJlYWR5U3RhdGU9PT1cImNvbXBsZXRlXCJ8fChkb2N1bWVudC5yZWFkeVN0YXRlIT09XCJsb2FkaW5nXCImJiFkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuZG9TY3JvbGwpKXt3aW5kb3cuc2V0VGltZW91dChqUXVlcnkucmVhZHkpO31lbHNle2RvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsY29tcGxldGVkKTt3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImxvYWRcIixjb21wbGV0ZWQpO31cbnZhciBhY2Nlc3M9ZnVuY3Rpb24oZWxlbXMsZm4sa2V5LHZhbHVlLGNoYWluYWJsZSxlbXB0eUdldCxyYXcpe3ZhciBpPTAsbGVuPWVsZW1zLmxlbmd0aCxidWxrPWtleT09bnVsbDtpZih0b1R5cGUoa2V5KT09PVwib2JqZWN0XCIpe2NoYWluYWJsZT10cnVlO2ZvcihpIGluIGtleSl7YWNjZXNzKGVsZW1zLGZuLGksa2V5W2ldLHRydWUsZW1wdHlHZXQscmF3KTt9fWVsc2UgaWYodmFsdWUhPT11bmRlZmluZWQpe2NoYWluYWJsZT10cnVlO2lmKCFpc0Z1bmN0aW9uKHZhbHVlKSl7cmF3PXRydWU7fVxuaWYoYnVsayl7aWYocmF3KXtmbi5jYWxsKGVsZW1zLHZhbHVlKTtmbj1udWxsO31lbHNle2J1bGs9Zm47Zm49ZnVuY3Rpb24oZWxlbSxfa2V5LHZhbHVlKXtyZXR1cm4gYnVsay5jYWxsKGpRdWVyeShlbGVtKSx2YWx1ZSk7fTt9fVxuaWYoZm4pe2Zvcig7aTxsZW47aSsrKXtmbihlbGVtc1tpXSxrZXkscmF3P3ZhbHVlOnZhbHVlLmNhbGwoZWxlbXNbaV0saSxmbihlbGVtc1tpXSxrZXkpKSk7fX19XG5pZihjaGFpbmFibGUpe3JldHVybiBlbGVtczt9XG5pZihidWxrKXtyZXR1cm4gZm4uY2FsbChlbGVtcyk7fVxucmV0dXJuIGxlbj9mbihlbGVtc1swXSxrZXkpOmVtcHR5R2V0O307dmFyIHJtc1ByZWZpeD0vXi1tcy0vLHJkYXNoQWxwaGE9Ly0oW2Etel0pL2c7ZnVuY3Rpb24gZmNhbWVsQ2FzZShfYWxsLGxldHRlcil7cmV0dXJuIGxldHRlci50b1VwcGVyQ2FzZSgpO31cbmZ1bmN0aW9uIGNhbWVsQ2FzZShzdHJpbmcpe3JldHVybiBzdHJpbmcucmVwbGFjZShybXNQcmVmaXgsXCJtcy1cIikucmVwbGFjZShyZGFzaEFscGhhLGZjYW1lbENhc2UpO31cbnZhciBhY2NlcHREYXRhPWZ1bmN0aW9uKG93bmVyKXtyZXR1cm4gb3duZXIubm9kZVR5cGU9PT0xfHxvd25lci5ub2RlVHlwZT09PTl8fCEoK293bmVyLm5vZGVUeXBlKTt9O2Z1bmN0aW9uIERhdGEoKXt0aGlzLmV4cGFuZG89alF1ZXJ5LmV4cGFuZG8rRGF0YS51aWQrKzt9XG5EYXRhLnVpZD0xO0RhdGEucHJvdG90eXBlPXtjYWNoZTpmdW5jdGlvbihvd25lcil7dmFyIHZhbHVlPW93bmVyW3RoaXMuZXhwYW5kb107aWYoIXZhbHVlKXt2YWx1ZT17fTtpZihhY2NlcHREYXRhKG93bmVyKSl7aWYob3duZXIubm9kZVR5cGUpe293bmVyW3RoaXMuZXhwYW5kb109dmFsdWU7fWVsc2V7T2JqZWN0LmRlZmluZVByb3BlcnR5KG93bmVyLHRoaXMuZXhwYW5kbyx7dmFsdWU6dmFsdWUsY29uZmlndXJhYmxlOnRydWV9KTt9fX1cbnJldHVybiB2YWx1ZTt9LHNldDpmdW5jdGlvbihvd25lcixkYXRhLHZhbHVlKXt2YXIgcHJvcCxjYWNoZT10aGlzLmNhY2hlKG93bmVyKTtpZih0eXBlb2YgZGF0YT09PVwic3RyaW5nXCIpe2NhY2hlW2NhbWVsQ2FzZShkYXRhKV09dmFsdWU7fWVsc2V7Zm9yKHByb3AgaW4gZGF0YSl7Y2FjaGVbY2FtZWxDYXNlKHByb3ApXT1kYXRhW3Byb3BdO319XG5yZXR1cm4gY2FjaGU7fSxnZXQ6ZnVuY3Rpb24ob3duZXIsa2V5KXtyZXR1cm4ga2V5PT09dW5kZWZpbmVkP3RoaXMuY2FjaGUob3duZXIpOm93bmVyW3RoaXMuZXhwYW5kb10mJm93bmVyW3RoaXMuZXhwYW5kb11bY2FtZWxDYXNlKGtleSldO30sYWNjZXNzOmZ1bmN0aW9uKG93bmVyLGtleSx2YWx1ZSl7aWYoa2V5PT09dW5kZWZpbmVkfHwoKGtleSYmdHlwZW9mIGtleT09PVwic3RyaW5nXCIpJiZ2YWx1ZT09PXVuZGVmaW5lZCkpe3JldHVybiB0aGlzLmdldChvd25lcixrZXkpO31cbnRoaXMuc2V0KG93bmVyLGtleSx2YWx1ZSk7cmV0dXJuIHZhbHVlIT09dW5kZWZpbmVkP3ZhbHVlOmtleTt9LHJlbW92ZTpmdW5jdGlvbihvd25lcixrZXkpe3ZhciBpLGNhY2hlPW93bmVyW3RoaXMuZXhwYW5kb107aWYoY2FjaGU9PT11bmRlZmluZWQpe3JldHVybjt9XG5pZihrZXkhPT11bmRlZmluZWQpe2lmKEFycmF5LmlzQXJyYXkoa2V5KSl7a2V5PWtleS5tYXAoY2FtZWxDYXNlKTt9ZWxzZXtrZXk9Y2FtZWxDYXNlKGtleSk7a2V5PWtleSBpbiBjYWNoZT9ba2V5XTooa2V5Lm1hdGNoKHJub3RodG1sd2hpdGUpfHxbXSk7fVxuaT1rZXkubGVuZ3RoO3doaWxlKGktLSl7ZGVsZXRlIGNhY2hlW2tleVtpXV07fX1cbmlmKGtleT09PXVuZGVmaW5lZHx8alF1ZXJ5LmlzRW1wdHlPYmplY3QoY2FjaGUpKXtpZihvd25lci5ub2RlVHlwZSl7b3duZXJbdGhpcy5leHBhbmRvXT11bmRlZmluZWQ7fWVsc2V7ZGVsZXRlIG93bmVyW3RoaXMuZXhwYW5kb107fX19LGhhc0RhdGE6ZnVuY3Rpb24ob3duZXIpe3ZhciBjYWNoZT1vd25lclt0aGlzLmV4cGFuZG9dO3JldHVybiBjYWNoZSE9PXVuZGVmaW5lZCYmIWpRdWVyeS5pc0VtcHR5T2JqZWN0KGNhY2hlKTt9fTt2YXIgZGF0YVByaXY9bmV3IERhdGEoKTt2YXIgZGF0YVVzZXI9bmV3IERhdGEoKTt2YXIgcmJyYWNlPS9eKD86XFx7W1xcd1xcV10qXFx9fFxcW1tcXHdcXFddKlxcXSkkLyxybXVsdGlEYXNoPS9bQS1aXS9nO2Z1bmN0aW9uIGdldERhdGEoZGF0YSl7aWYoZGF0YT09PVwidHJ1ZVwiKXtyZXR1cm4gdHJ1ZTt9XG5pZihkYXRhPT09XCJmYWxzZVwiKXtyZXR1cm4gZmFsc2U7fVxuaWYoZGF0YT09PVwibnVsbFwiKXtyZXR1cm4gbnVsbDt9XG5pZihkYXRhPT09K2RhdGErXCJcIil7cmV0dXJuK2RhdGE7fVxuaWYocmJyYWNlLnRlc3QoZGF0YSkpe3JldHVybiBKU09OLnBhcnNlKGRhdGEpO31cbnJldHVybiBkYXRhO31cbmZ1bmN0aW9uIGRhdGFBdHRyKGVsZW0sa2V5LGRhdGEpe3ZhciBuYW1lO2lmKGRhdGE9PT11bmRlZmluZWQmJmVsZW0ubm9kZVR5cGU9PT0xKXtuYW1lPVwiZGF0YS1cIitrZXkucmVwbGFjZShybXVsdGlEYXNoLFwiLSQmXCIpLnRvTG93ZXJDYXNlKCk7ZGF0YT1lbGVtLmdldEF0dHJpYnV0ZShuYW1lKTtpZih0eXBlb2YgZGF0YT09PVwic3RyaW5nXCIpe3RyeXtkYXRhPWdldERhdGEoZGF0YSk7fWNhdGNoKGUpe31cbmRhdGFVc2VyLnNldChlbGVtLGtleSxkYXRhKTt9ZWxzZXtkYXRhPXVuZGVmaW5lZDt9fVxucmV0dXJuIGRhdGE7fVxualF1ZXJ5LmV4dGVuZCh7aGFzRGF0YTpmdW5jdGlvbihlbGVtKXtyZXR1cm4gZGF0YVVzZXIuaGFzRGF0YShlbGVtKXx8ZGF0YVByaXYuaGFzRGF0YShlbGVtKTt9LGRhdGE6ZnVuY3Rpb24oZWxlbSxuYW1lLGRhdGEpe3JldHVybiBkYXRhVXNlci5hY2Nlc3MoZWxlbSxuYW1lLGRhdGEpO30scmVtb3ZlRGF0YTpmdW5jdGlvbihlbGVtLG5hbWUpe2RhdGFVc2VyLnJlbW92ZShlbGVtLG5hbWUpO30sX2RhdGE6ZnVuY3Rpb24oZWxlbSxuYW1lLGRhdGEpe3JldHVybiBkYXRhUHJpdi5hY2Nlc3MoZWxlbSxuYW1lLGRhdGEpO30sX3JlbW92ZURhdGE6ZnVuY3Rpb24oZWxlbSxuYW1lKXtkYXRhUHJpdi5yZW1vdmUoZWxlbSxuYW1lKTt9fSk7alF1ZXJ5LmZuLmV4dGVuZCh7ZGF0YTpmdW5jdGlvbihrZXksdmFsdWUpe3ZhciBpLG5hbWUsZGF0YSxlbGVtPXRoaXNbMF0sYXR0cnM9ZWxlbSYmZWxlbS5hdHRyaWJ1dGVzO2lmKGtleT09PXVuZGVmaW5lZCl7aWYodGhpcy5sZW5ndGgpe2RhdGE9ZGF0YVVzZXIuZ2V0KGVsZW0pO2lmKGVsZW0ubm9kZVR5cGU9PT0xJiYhZGF0YVByaXYuZ2V0KGVsZW0sXCJoYXNEYXRhQXR0cnNcIikpe2k9YXR0cnMubGVuZ3RoO3doaWxlKGktLSl7aWYoYXR0cnNbaV0pe25hbWU9YXR0cnNbaV0ubmFtZTtpZihuYW1lLmluZGV4T2YoXCJkYXRhLVwiKT09PTApe25hbWU9Y2FtZWxDYXNlKG5hbWUuc2xpY2UoNSkpO2RhdGFBdHRyKGVsZW0sbmFtZSxkYXRhW25hbWVdKTt9fX1cbmRhdGFQcml2LnNldChlbGVtLFwiaGFzRGF0YUF0dHJzXCIsdHJ1ZSk7fX1cbnJldHVybiBkYXRhO31cbmlmKHR5cGVvZiBrZXk9PT1cIm9iamVjdFwiKXtyZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKCl7ZGF0YVVzZXIuc2V0KHRoaXMsa2V5KTt9KTt9XG5yZXR1cm4gYWNjZXNzKHRoaXMsZnVuY3Rpb24odmFsdWUpe3ZhciBkYXRhO2lmKGVsZW0mJnZhbHVlPT09dW5kZWZpbmVkKXtkYXRhPWRhdGFVc2VyLmdldChlbGVtLGtleSk7aWYoZGF0YSE9PXVuZGVmaW5lZCl7cmV0dXJuIGRhdGE7fVxuZGF0YT1kYXRhQXR0cihlbGVtLGtleSk7aWYoZGF0YSE9PXVuZGVmaW5lZCl7cmV0dXJuIGRhdGE7fVxucmV0dXJuO31cbnRoaXMuZWFjaChmdW5jdGlvbigpe2RhdGFVc2VyLnNldCh0aGlzLGtleSx2YWx1ZSk7fSk7fSxudWxsLHZhbHVlLGFyZ3VtZW50cy5sZW5ndGg+MSxudWxsLHRydWUpO30scmVtb3ZlRGF0YTpmdW5jdGlvbihrZXkpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXtkYXRhVXNlci5yZW1vdmUodGhpcyxrZXkpO30pO319KTtqUXVlcnkuZXh0ZW5kKHtxdWV1ZTpmdW5jdGlvbihlbGVtLHR5cGUsZGF0YSl7dmFyIHF1ZXVlO2lmKGVsZW0pe3R5cGU9KHR5cGV8fFwiZnhcIikrXCJxdWV1ZVwiO3F1ZXVlPWRhdGFQcml2LmdldChlbGVtLHR5cGUpO2lmKGRhdGEpe2lmKCFxdWV1ZXx8QXJyYXkuaXNBcnJheShkYXRhKSl7cXVldWU9ZGF0YVByaXYuYWNjZXNzKGVsZW0sdHlwZSxqUXVlcnkubWFrZUFycmF5KGRhdGEpKTt9ZWxzZXtxdWV1ZS5wdXNoKGRhdGEpO319XG5yZXR1cm4gcXVldWV8fFtdO319LGRlcXVldWU6ZnVuY3Rpb24oZWxlbSx0eXBlKXt0eXBlPXR5cGV8fFwiZnhcIjt2YXIgcXVldWU9alF1ZXJ5LnF1ZXVlKGVsZW0sdHlwZSksc3RhcnRMZW5ndGg9cXVldWUubGVuZ3RoLGZuPXF1ZXVlLnNoaWZ0KCksaG9va3M9alF1ZXJ5Ll9xdWV1ZUhvb2tzKGVsZW0sdHlwZSksbmV4dD1mdW5jdGlvbigpe2pRdWVyeS5kZXF1ZXVlKGVsZW0sdHlwZSk7fTtpZihmbj09PVwiaW5wcm9ncmVzc1wiKXtmbj1xdWV1ZS5zaGlmdCgpO3N0YXJ0TGVuZ3RoLS07fVxuaWYoZm4pe2lmKHR5cGU9PT1cImZ4XCIpe3F1ZXVlLnVuc2hpZnQoXCJpbnByb2dyZXNzXCIpO31cbmRlbGV0ZSBob29rcy5zdG9wO2ZuLmNhbGwoZWxlbSxuZXh0LGhvb2tzKTt9XG5pZighc3RhcnRMZW5ndGgmJmhvb2tzKXtob29rcy5lbXB0eS5maXJlKCk7fX0sX3F1ZXVlSG9va3M6ZnVuY3Rpb24oZWxlbSx0eXBlKXt2YXIga2V5PXR5cGUrXCJxdWV1ZUhvb2tzXCI7cmV0dXJuIGRhdGFQcml2LmdldChlbGVtLGtleSl8fGRhdGFQcml2LmFjY2VzcyhlbGVtLGtleSx7ZW1wdHk6alF1ZXJ5LkNhbGxiYWNrcyhcIm9uY2UgbWVtb3J5XCIpLmFkZChmdW5jdGlvbigpe2RhdGFQcml2LnJlbW92ZShlbGVtLFt0eXBlK1wicXVldWVcIixrZXldKTt9KX0pO319KTtqUXVlcnkuZm4uZXh0ZW5kKHtxdWV1ZTpmdW5jdGlvbih0eXBlLGRhdGEpe3ZhciBzZXR0ZXI9MjtpZih0eXBlb2YgdHlwZSE9PVwic3RyaW5nXCIpe2RhdGE9dHlwZTt0eXBlPVwiZnhcIjtzZXR0ZXItLTt9XG5pZihhcmd1bWVudHMubGVuZ3RoPHNldHRlcil7cmV0dXJuIGpRdWVyeS5xdWV1ZSh0aGlzWzBdLHR5cGUpO31cbnJldHVybiBkYXRhPT09dW5kZWZpbmVkP3RoaXM6dGhpcy5lYWNoKGZ1bmN0aW9uKCl7dmFyIHF1ZXVlPWpRdWVyeS5xdWV1ZSh0aGlzLHR5cGUsZGF0YSk7alF1ZXJ5Ll9xdWV1ZUhvb2tzKHRoaXMsdHlwZSk7aWYodHlwZT09PVwiZnhcIiYmcXVldWVbMF0hPT1cImlucHJvZ3Jlc3NcIil7alF1ZXJ5LmRlcXVldWUodGhpcyx0eXBlKTt9fSk7fSxkZXF1ZXVlOmZ1bmN0aW9uKHR5cGUpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXtqUXVlcnkuZGVxdWV1ZSh0aGlzLHR5cGUpO30pO30sY2xlYXJRdWV1ZTpmdW5jdGlvbih0eXBlKXtyZXR1cm4gdGhpcy5xdWV1ZSh0eXBlfHxcImZ4XCIsW10pO30scHJvbWlzZTpmdW5jdGlvbih0eXBlLG9iail7dmFyIHRtcCxjb3VudD0xLGRlZmVyPWpRdWVyeS5EZWZlcnJlZCgpLGVsZW1lbnRzPXRoaXMsaT10aGlzLmxlbmd0aCxyZXNvbHZlPWZ1bmN0aW9uKCl7aWYoISgtLWNvdW50KSl7ZGVmZXIucmVzb2x2ZVdpdGgoZWxlbWVudHMsW2VsZW1lbnRzXSk7fX07aWYodHlwZW9mIHR5cGUhPT1cInN0cmluZ1wiKXtvYmo9dHlwZTt0eXBlPXVuZGVmaW5lZDt9XG50eXBlPXR5cGV8fFwiZnhcIjt3aGlsZShpLS0pe3RtcD1kYXRhUHJpdi5nZXQoZWxlbWVudHNbaV0sdHlwZStcInF1ZXVlSG9va3NcIik7aWYodG1wJiZ0bXAuZW1wdHkpe2NvdW50Kys7dG1wLmVtcHR5LmFkZChyZXNvbHZlKTt9fVxucmVzb2x2ZSgpO3JldHVybiBkZWZlci5wcm9taXNlKG9iaik7fX0pO3ZhciBwbnVtPSgvWystXT8oPzpcXGQqXFwufClcXGQrKD86W2VFXVsrLV0/XFxkK3wpLykuc291cmNlO3ZhciByY3NzTnVtPW5ldyBSZWdFeHAoXCJeKD86KFsrLV0pPXwpKFwiK3BudW0rXCIpKFthLXolXSopJFwiLFwiaVwiKTt2YXIgY3NzRXhwYW5kPVtcIlRvcFwiLFwiUmlnaHRcIixcIkJvdHRvbVwiLFwiTGVmdFwiXTt2YXIgZG9jdW1lbnRFbGVtZW50PWRvY3VtZW50LmRvY3VtZW50RWxlbWVudDt2YXIgaXNBdHRhY2hlZD1mdW5jdGlvbihlbGVtKXtyZXR1cm4galF1ZXJ5LmNvbnRhaW5zKGVsZW0ub3duZXJEb2N1bWVudCxlbGVtKTt9LGNvbXBvc2VkPXtjb21wb3NlZDp0cnVlfTtpZihkb2N1bWVudEVsZW1lbnQuZ2V0Um9vdE5vZGUpe2lzQXR0YWNoZWQ9ZnVuY3Rpb24oZWxlbSl7cmV0dXJuIGpRdWVyeS5jb250YWlucyhlbGVtLm93bmVyRG9jdW1lbnQsZWxlbSl8fGVsZW0uZ2V0Um9vdE5vZGUoY29tcG9zZWQpPT09ZWxlbS5vd25lckRvY3VtZW50O307fVxudmFyIGlzSGlkZGVuV2l0aGluVHJlZT1mdW5jdGlvbihlbGVtLGVsKXtlbGVtPWVsfHxlbGVtO3JldHVybiBlbGVtLnN0eWxlLmRpc3BsYXk9PT1cIm5vbmVcInx8ZWxlbS5zdHlsZS5kaXNwbGF5PT09XCJcIiYmaXNBdHRhY2hlZChlbGVtKSYmalF1ZXJ5LmNzcyhlbGVtLFwiZGlzcGxheVwiKT09PVwibm9uZVwiO307ZnVuY3Rpb24gYWRqdXN0Q1NTKGVsZW0scHJvcCx2YWx1ZVBhcnRzLHR3ZWVuKXt2YXIgYWRqdXN0ZWQsc2NhbGUsbWF4SXRlcmF0aW9ucz0yMCxjdXJyZW50VmFsdWU9dHdlZW4/ZnVuY3Rpb24oKXtyZXR1cm4gdHdlZW4uY3VyKCk7fTpmdW5jdGlvbigpe3JldHVybiBqUXVlcnkuY3NzKGVsZW0scHJvcCxcIlwiKTt9LGluaXRpYWw9Y3VycmVudFZhbHVlKCksdW5pdD12YWx1ZVBhcnRzJiZ2YWx1ZVBhcnRzWzNdfHwoalF1ZXJ5LmNzc051bWJlcltwcm9wXT9cIlwiOlwicHhcIiksaW5pdGlhbEluVW5pdD1lbGVtLm5vZGVUeXBlJiYoalF1ZXJ5LmNzc051bWJlcltwcm9wXXx8dW5pdCE9PVwicHhcIiYmK2luaXRpYWwpJiZyY3NzTnVtLmV4ZWMoalF1ZXJ5LmNzcyhlbGVtLHByb3ApKTtpZihpbml0aWFsSW5Vbml0JiZpbml0aWFsSW5Vbml0WzNdIT09dW5pdCl7aW5pdGlhbD1pbml0aWFsLzI7dW5pdD11bml0fHxpbml0aWFsSW5Vbml0WzNdO2luaXRpYWxJblVuaXQ9K2luaXRpYWx8fDE7d2hpbGUobWF4SXRlcmF0aW9ucy0tKXtqUXVlcnkuc3R5bGUoZWxlbSxwcm9wLGluaXRpYWxJblVuaXQrdW5pdCk7aWYoKDEtc2NhbGUpKigxLShzY2FsZT1jdXJyZW50VmFsdWUoKS9pbml0aWFsfHwwLjUpKTw9MCl7bWF4SXRlcmF0aW9ucz0wO31cbmluaXRpYWxJblVuaXQ9aW5pdGlhbEluVW5pdC9zY2FsZTt9XG5pbml0aWFsSW5Vbml0PWluaXRpYWxJblVuaXQqMjtqUXVlcnkuc3R5bGUoZWxlbSxwcm9wLGluaXRpYWxJblVuaXQrdW5pdCk7dmFsdWVQYXJ0cz12YWx1ZVBhcnRzfHxbXTt9XG5pZih2YWx1ZVBhcnRzKXtpbml0aWFsSW5Vbml0PStpbml0aWFsSW5Vbml0fHwraW5pdGlhbHx8MDthZGp1c3RlZD12YWx1ZVBhcnRzWzFdP2luaXRpYWxJblVuaXQrKHZhbHVlUGFydHNbMV0rMSkqdmFsdWVQYXJ0c1syXTordmFsdWVQYXJ0c1syXTtpZih0d2Vlbil7dHdlZW4udW5pdD11bml0O3R3ZWVuLnN0YXJ0PWluaXRpYWxJblVuaXQ7dHdlZW4uZW5kPWFkanVzdGVkO319XG5yZXR1cm4gYWRqdXN0ZWQ7fVxudmFyIGRlZmF1bHREaXNwbGF5TWFwPXt9O2Z1bmN0aW9uIGdldERlZmF1bHREaXNwbGF5KGVsZW0pe3ZhciB0ZW1wLGRvYz1lbGVtLm93bmVyRG9jdW1lbnQsbm9kZU5hbWU9ZWxlbS5ub2RlTmFtZSxkaXNwbGF5PWRlZmF1bHREaXNwbGF5TWFwW25vZGVOYW1lXTtpZihkaXNwbGF5KXtyZXR1cm4gZGlzcGxheTt9XG50ZW1wPWRvYy5ib2R5LmFwcGVuZENoaWxkKGRvYy5jcmVhdGVFbGVtZW50KG5vZGVOYW1lKSk7ZGlzcGxheT1qUXVlcnkuY3NzKHRlbXAsXCJkaXNwbGF5XCIpO3RlbXAucGFyZW50Tm9kZS5yZW1vdmVDaGlsZCh0ZW1wKTtpZihkaXNwbGF5PT09XCJub25lXCIpe2Rpc3BsYXk9XCJibG9ja1wiO31cbmRlZmF1bHREaXNwbGF5TWFwW25vZGVOYW1lXT1kaXNwbGF5O3JldHVybiBkaXNwbGF5O31cbmZ1bmN0aW9uIHNob3dIaWRlKGVsZW1lbnRzLHNob3cpe3ZhciBkaXNwbGF5LGVsZW0sdmFsdWVzPVtdLGluZGV4PTAsbGVuZ3RoPWVsZW1lbnRzLmxlbmd0aDtmb3IoO2luZGV4PGxlbmd0aDtpbmRleCsrKXtlbGVtPWVsZW1lbnRzW2luZGV4XTtpZighZWxlbS5zdHlsZSl7Y29udGludWU7fVxuZGlzcGxheT1lbGVtLnN0eWxlLmRpc3BsYXk7aWYoc2hvdyl7aWYoZGlzcGxheT09PVwibm9uZVwiKXt2YWx1ZXNbaW5kZXhdPWRhdGFQcml2LmdldChlbGVtLFwiZGlzcGxheVwiKXx8bnVsbDtpZighdmFsdWVzW2luZGV4XSl7ZWxlbS5zdHlsZS5kaXNwbGF5PVwiXCI7fX1cbmlmKGVsZW0uc3R5bGUuZGlzcGxheT09PVwiXCImJmlzSGlkZGVuV2l0aGluVHJlZShlbGVtKSl7dmFsdWVzW2luZGV4XT1nZXREZWZhdWx0RGlzcGxheShlbGVtKTt9fWVsc2V7aWYoZGlzcGxheSE9PVwibm9uZVwiKXt2YWx1ZXNbaW5kZXhdPVwibm9uZVwiO2RhdGFQcml2LnNldChlbGVtLFwiZGlzcGxheVwiLGRpc3BsYXkpO319fVxuZm9yKGluZGV4PTA7aW5kZXg8bGVuZ3RoO2luZGV4Kyspe2lmKHZhbHVlc1tpbmRleF0hPW51bGwpe2VsZW1lbnRzW2luZGV4XS5zdHlsZS5kaXNwbGF5PXZhbHVlc1tpbmRleF07fX1cbnJldHVybiBlbGVtZW50czt9XG5qUXVlcnkuZm4uZXh0ZW5kKHtzaG93OmZ1bmN0aW9uKCl7cmV0dXJuIHNob3dIaWRlKHRoaXMsdHJ1ZSk7fSxoaWRlOmZ1bmN0aW9uKCl7cmV0dXJuIHNob3dIaWRlKHRoaXMpO30sdG9nZ2xlOmZ1bmN0aW9uKHN0YXRlKXtpZih0eXBlb2Ygc3RhdGU9PT1cImJvb2xlYW5cIil7cmV0dXJuIHN0YXRlP3RoaXMuc2hvdygpOnRoaXMuaGlkZSgpO31cbnJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXtpZihpc0hpZGRlbldpdGhpblRyZWUodGhpcykpe2pRdWVyeSh0aGlzKS5zaG93KCk7fWVsc2V7alF1ZXJ5KHRoaXMpLmhpZGUoKTt9fSk7fX0pO3ZhciByY2hlY2thYmxlVHlwZT0oL14oPzpjaGVja2JveHxyYWRpbykkL2kpO3ZhciBydGFnTmFtZT0oLzwoW2Etel1bXlxcL1xcMD5cXHgyMFxcdFxcclxcblxcZl0qKS9pKTt2YXIgcnNjcmlwdFR5cGU9KC9eJHxebW9kdWxlJHxcXC8oPzpqYXZhfGVjbWEpc2NyaXB0L2kpOyhmdW5jdGlvbigpe3ZhciBmcmFnbWVudD1kb2N1bWVudC5jcmVhdGVEb2N1bWVudEZyYWdtZW50KCksZGl2PWZyYWdtZW50LmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIikpLGlucHV0PWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiKTtpbnB1dC5zZXRBdHRyaWJ1dGUoXCJ0eXBlXCIsXCJyYWRpb1wiKTtpbnB1dC5zZXRBdHRyaWJ1dGUoXCJjaGVja2VkXCIsXCJjaGVja2VkXCIpO2lucHV0LnNldEF0dHJpYnV0ZShcIm5hbWVcIixcInRcIik7ZGl2LmFwcGVuZENoaWxkKGlucHV0KTtzdXBwb3J0LmNoZWNrQ2xvbmU9ZGl2LmNsb25lTm9kZSh0cnVlKS5jbG9uZU5vZGUodHJ1ZSkubGFzdENoaWxkLmNoZWNrZWQ7ZGl2LmlubmVySFRNTD1cIjx0ZXh0YXJlYT54PC90ZXh0YXJlYT5cIjtzdXBwb3J0Lm5vQ2xvbmVDaGVja2VkPSEhZGl2LmNsb25lTm9kZSh0cnVlKS5sYXN0Q2hpbGQuZGVmYXVsdFZhbHVlO2Rpdi5pbm5lckhUTUw9XCI8b3B0aW9uPjwvb3B0aW9uPlwiO3N1cHBvcnQub3B0aW9uPSEhZGl2Lmxhc3RDaGlsZDt9KSgpO3ZhciB3cmFwTWFwPXt0aGVhZDpbMSxcIjx0YWJsZT5cIixcIjwvdGFibGU+XCJdLGNvbDpbMixcIjx0YWJsZT48Y29sZ3JvdXA+XCIsXCI8L2NvbGdyb3VwPjwvdGFibGU+XCJdLHRyOlsyLFwiPHRhYmxlPjx0Ym9keT5cIixcIjwvdGJvZHk+PC90YWJsZT5cIl0sdGQ6WzMsXCI8dGFibGU+PHRib2R5Pjx0cj5cIixcIjwvdHI+PC90Ym9keT48L3RhYmxlPlwiXSxfZGVmYXVsdDpbMCxcIlwiLFwiXCJdfTt3cmFwTWFwLnRib2R5PXdyYXBNYXAudGZvb3Q9d3JhcE1hcC5jb2xncm91cD13cmFwTWFwLmNhcHRpb249d3JhcE1hcC50aGVhZDt3cmFwTWFwLnRoPXdyYXBNYXAudGQ7aWYoIXN1cHBvcnQub3B0aW9uKXt3cmFwTWFwLm9wdGdyb3VwPXdyYXBNYXAub3B0aW9uPVsxLFwiPHNlbGVjdCBtdWx0aXBsZT0nbXVsdGlwbGUnPlwiLFwiPC9zZWxlY3Q+XCJdO31cbmZ1bmN0aW9uIGdldEFsbChjb250ZXh0LHRhZyl7dmFyIHJldDtpZih0eXBlb2YgY29udGV4dC5nZXRFbGVtZW50c0J5VGFnTmFtZSE9PVwidW5kZWZpbmVkXCIpe3JldD1jb250ZXh0LmdldEVsZW1lbnRzQnlUYWdOYW1lKHRhZ3x8XCIqXCIpO31lbHNlIGlmKHR5cGVvZiBjb250ZXh0LnF1ZXJ5U2VsZWN0b3JBbGwhPT1cInVuZGVmaW5lZFwiKXtyZXQ9Y29udGV4dC5xdWVyeVNlbGVjdG9yQWxsKHRhZ3x8XCIqXCIpO31lbHNle3JldD1bXTt9XG5pZih0YWc9PT11bmRlZmluZWR8fHRhZyYmbm9kZU5hbWUoY29udGV4dCx0YWcpKXtyZXR1cm4galF1ZXJ5Lm1lcmdlKFtjb250ZXh0XSxyZXQpO31cbnJldHVybiByZXQ7fVxuZnVuY3Rpb24gc2V0R2xvYmFsRXZhbChlbGVtcyxyZWZFbGVtZW50cyl7dmFyIGk9MCxsPWVsZW1zLmxlbmd0aDtmb3IoO2k8bDtpKyspe2RhdGFQcml2LnNldChlbGVtc1tpXSxcImdsb2JhbEV2YWxcIiwhcmVmRWxlbWVudHN8fGRhdGFQcml2LmdldChyZWZFbGVtZW50c1tpXSxcImdsb2JhbEV2YWxcIikpO319XG52YXIgcmh0bWw9Lzx8JiM/XFx3KzsvO2Z1bmN0aW9uIGJ1aWxkRnJhZ21lbnQoZWxlbXMsY29udGV4dCxzY3JpcHRzLHNlbGVjdGlvbixpZ25vcmVkKXt2YXIgZWxlbSx0bXAsdGFnLHdyYXAsYXR0YWNoZWQsaixmcmFnbWVudD1jb250ZXh0LmNyZWF0ZURvY3VtZW50RnJhZ21lbnQoKSxub2Rlcz1bXSxpPTAsbD1lbGVtcy5sZW5ndGg7Zm9yKDtpPGw7aSsrKXtlbGVtPWVsZW1zW2ldO2lmKGVsZW18fGVsZW09PT0wKXtpZih0b1R5cGUoZWxlbSk9PT1cIm9iamVjdFwiKXtqUXVlcnkubWVyZ2Uobm9kZXMsZWxlbS5ub2RlVHlwZT9bZWxlbV06ZWxlbSk7fWVsc2UgaWYoIXJodG1sLnRlc3QoZWxlbSkpe25vZGVzLnB1c2goY29udGV4dC5jcmVhdGVUZXh0Tm9kZShlbGVtKSk7fWVsc2V7dG1wPXRtcHx8ZnJhZ21lbnQuYXBwZW5kQ2hpbGQoY29udGV4dC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpKTt0YWc9KHJ0YWdOYW1lLmV4ZWMoZWxlbSl8fFtcIlwiLFwiXCJdKVsxXS50b0xvd2VyQ2FzZSgpO3dyYXA9d3JhcE1hcFt0YWddfHx3cmFwTWFwLl9kZWZhdWx0O3RtcC5pbm5lckhUTUw9d3JhcFsxXStqUXVlcnkuaHRtbFByZWZpbHRlcihlbGVtKSt3cmFwWzJdO2o9d3JhcFswXTt3aGlsZShqLS0pe3RtcD10bXAubGFzdENoaWxkO31cbmpRdWVyeS5tZXJnZShub2Rlcyx0bXAuY2hpbGROb2Rlcyk7dG1wPWZyYWdtZW50LmZpcnN0Q2hpbGQ7dG1wLnRleHRDb250ZW50PVwiXCI7fX19XG5mcmFnbWVudC50ZXh0Q29udGVudD1cIlwiO2k9MDt3aGlsZSgoZWxlbT1ub2Rlc1tpKytdKSl7aWYoc2VsZWN0aW9uJiZqUXVlcnkuaW5BcnJheShlbGVtLHNlbGVjdGlvbik+LTEpe2lmKGlnbm9yZWQpe2lnbm9yZWQucHVzaChlbGVtKTt9XG5jb250aW51ZTt9XG5hdHRhY2hlZD1pc0F0dGFjaGVkKGVsZW0pO3RtcD1nZXRBbGwoZnJhZ21lbnQuYXBwZW5kQ2hpbGQoZWxlbSksXCJzY3JpcHRcIik7aWYoYXR0YWNoZWQpe3NldEdsb2JhbEV2YWwodG1wKTt9XG5pZihzY3JpcHRzKXtqPTA7d2hpbGUoKGVsZW09dG1wW2orK10pKXtpZihyc2NyaXB0VHlwZS50ZXN0KGVsZW0udHlwZXx8XCJcIikpe3NjcmlwdHMucHVzaChlbGVtKTt9fX19XG5yZXR1cm4gZnJhZ21lbnQ7fVxudmFyXG5ya2V5RXZlbnQ9L15rZXkvLHJtb3VzZUV2ZW50PS9eKD86bW91c2V8cG9pbnRlcnxjb250ZXh0bWVudXxkcmFnfGRyb3ApfGNsaWNrLyxydHlwZW5hbWVzcGFjZT0vXihbXi5dKikoPzpcXC4oLispfCkvO2Z1bmN0aW9uIHJldHVyblRydWUoKXtyZXR1cm4gdHJ1ZTt9XG5mdW5jdGlvbiByZXR1cm5GYWxzZSgpe3JldHVybiBmYWxzZTt9XG5mdW5jdGlvbiBleHBlY3RTeW5jKGVsZW0sdHlwZSl7cmV0dXJuKGVsZW09PT1zYWZlQWN0aXZlRWxlbWVudCgpKT09PSh0eXBlPT09XCJmb2N1c1wiKTt9XG5mdW5jdGlvbiBzYWZlQWN0aXZlRWxlbWVudCgpe3RyeXtyZXR1cm4gZG9jdW1lbnQuYWN0aXZlRWxlbWVudDt9Y2F0Y2goZXJyKXt9fVxuZnVuY3Rpb24gb24oZWxlbSx0eXBlcyxzZWxlY3RvcixkYXRhLGZuLG9uZSl7dmFyIG9yaWdGbix0eXBlO2lmKHR5cGVvZiB0eXBlcz09PVwib2JqZWN0XCIpe2lmKHR5cGVvZiBzZWxlY3RvciE9PVwic3RyaW5nXCIpe2RhdGE9ZGF0YXx8c2VsZWN0b3I7c2VsZWN0b3I9dW5kZWZpbmVkO31cbmZvcih0eXBlIGluIHR5cGVzKXtvbihlbGVtLHR5cGUsc2VsZWN0b3IsZGF0YSx0eXBlc1t0eXBlXSxvbmUpO31cbnJldHVybiBlbGVtO31cbmlmKGRhdGE9PW51bGwmJmZuPT1udWxsKXtmbj1zZWxlY3RvcjtkYXRhPXNlbGVjdG9yPXVuZGVmaW5lZDt9ZWxzZSBpZihmbj09bnVsbCl7aWYodHlwZW9mIHNlbGVjdG9yPT09XCJzdHJpbmdcIil7Zm49ZGF0YTtkYXRhPXVuZGVmaW5lZDt9ZWxzZXtmbj1kYXRhO2RhdGE9c2VsZWN0b3I7c2VsZWN0b3I9dW5kZWZpbmVkO319XG5pZihmbj09PWZhbHNlKXtmbj1yZXR1cm5GYWxzZTt9ZWxzZSBpZighZm4pe3JldHVybiBlbGVtO31cbmlmKG9uZT09PTEpe29yaWdGbj1mbjtmbj1mdW5jdGlvbihldmVudCl7alF1ZXJ5KCkub2ZmKGV2ZW50KTtyZXR1cm4gb3JpZ0ZuLmFwcGx5KHRoaXMsYXJndW1lbnRzKTt9O2ZuLmd1aWQ9b3JpZ0ZuLmd1aWR8fChvcmlnRm4uZ3VpZD1qUXVlcnkuZ3VpZCsrKTt9XG5yZXR1cm4gZWxlbS5lYWNoKGZ1bmN0aW9uKCl7alF1ZXJ5LmV2ZW50LmFkZCh0aGlzLHR5cGVzLGZuLGRhdGEsc2VsZWN0b3IpO30pO31cbmpRdWVyeS5ldmVudD17Z2xvYmFsOnt9LGFkZDpmdW5jdGlvbihlbGVtLHR5cGVzLGhhbmRsZXIsZGF0YSxzZWxlY3Rvcil7dmFyIGhhbmRsZU9iakluLGV2ZW50SGFuZGxlLHRtcCxldmVudHMsdCxoYW5kbGVPYmosc3BlY2lhbCxoYW5kbGVycyx0eXBlLG5hbWVzcGFjZXMsb3JpZ1R5cGUsZWxlbURhdGE9ZGF0YVByaXYuZ2V0KGVsZW0pO2lmKCFhY2NlcHREYXRhKGVsZW0pKXtyZXR1cm47fVxuaWYoaGFuZGxlci5oYW5kbGVyKXtoYW5kbGVPYmpJbj1oYW5kbGVyO2hhbmRsZXI9aGFuZGxlT2JqSW4uaGFuZGxlcjtzZWxlY3Rvcj1oYW5kbGVPYmpJbi5zZWxlY3Rvcjt9XG5pZihzZWxlY3Rvcil7alF1ZXJ5LmZpbmQubWF0Y2hlc1NlbGVjdG9yKGRvY3VtZW50RWxlbWVudCxzZWxlY3Rvcik7fVxuaWYoIWhhbmRsZXIuZ3VpZCl7aGFuZGxlci5ndWlkPWpRdWVyeS5ndWlkKys7fVxuaWYoIShldmVudHM9ZWxlbURhdGEuZXZlbnRzKSl7ZXZlbnRzPWVsZW1EYXRhLmV2ZW50cz1PYmplY3QuY3JlYXRlKG51bGwpO31cbmlmKCEoZXZlbnRIYW5kbGU9ZWxlbURhdGEuaGFuZGxlKSl7ZXZlbnRIYW5kbGU9ZWxlbURhdGEuaGFuZGxlPWZ1bmN0aW9uKGUpe3JldHVybiB0eXBlb2YgalF1ZXJ5IT09XCJ1bmRlZmluZWRcIiYmalF1ZXJ5LmV2ZW50LnRyaWdnZXJlZCE9PWUudHlwZT9qUXVlcnkuZXZlbnQuZGlzcGF0Y2guYXBwbHkoZWxlbSxhcmd1bWVudHMpOnVuZGVmaW5lZDt9O31cbnR5cGVzPSh0eXBlc3x8XCJcIikubWF0Y2gocm5vdGh0bWx3aGl0ZSl8fFtcIlwiXTt0PXR5cGVzLmxlbmd0aDt3aGlsZSh0LS0pe3RtcD1ydHlwZW5hbWVzcGFjZS5leGVjKHR5cGVzW3RdKXx8W107dHlwZT1vcmlnVHlwZT10bXBbMV07bmFtZXNwYWNlcz0odG1wWzJdfHxcIlwiKS5zcGxpdChcIi5cIikuc29ydCgpO2lmKCF0eXBlKXtjb250aW51ZTt9XG5zcGVjaWFsPWpRdWVyeS5ldmVudC5zcGVjaWFsW3R5cGVdfHx7fTt0eXBlPShzZWxlY3Rvcj9zcGVjaWFsLmRlbGVnYXRlVHlwZTpzcGVjaWFsLmJpbmRUeXBlKXx8dHlwZTtzcGVjaWFsPWpRdWVyeS5ldmVudC5zcGVjaWFsW3R5cGVdfHx7fTtoYW5kbGVPYmo9alF1ZXJ5LmV4dGVuZCh7dHlwZTp0eXBlLG9yaWdUeXBlOm9yaWdUeXBlLGRhdGE6ZGF0YSxoYW5kbGVyOmhhbmRsZXIsZ3VpZDpoYW5kbGVyLmd1aWQsc2VsZWN0b3I6c2VsZWN0b3IsbmVlZHNDb250ZXh0OnNlbGVjdG9yJiZqUXVlcnkuZXhwci5tYXRjaC5uZWVkc0NvbnRleHQudGVzdChzZWxlY3RvciksbmFtZXNwYWNlOm5hbWVzcGFjZXMuam9pbihcIi5cIil9LGhhbmRsZU9iakluKTtpZighKGhhbmRsZXJzPWV2ZW50c1t0eXBlXSkpe2hhbmRsZXJzPWV2ZW50c1t0eXBlXT1bXTtoYW5kbGVycy5kZWxlZ2F0ZUNvdW50PTA7aWYoIXNwZWNpYWwuc2V0dXB8fHNwZWNpYWwuc2V0dXAuY2FsbChlbGVtLGRhdGEsbmFtZXNwYWNlcyxldmVudEhhbmRsZSk9PT1mYWxzZSl7aWYoZWxlbS5hZGRFdmVudExpc3RlbmVyKXtlbGVtLmFkZEV2ZW50TGlzdGVuZXIodHlwZSxldmVudEhhbmRsZSk7fX19XG5pZihzcGVjaWFsLmFkZCl7c3BlY2lhbC5hZGQuY2FsbChlbGVtLGhhbmRsZU9iaik7aWYoIWhhbmRsZU9iai5oYW5kbGVyLmd1aWQpe2hhbmRsZU9iai5oYW5kbGVyLmd1aWQ9aGFuZGxlci5ndWlkO319XG5pZihzZWxlY3Rvcil7aGFuZGxlcnMuc3BsaWNlKGhhbmRsZXJzLmRlbGVnYXRlQ291bnQrKywwLGhhbmRsZU9iaik7fWVsc2V7aGFuZGxlcnMucHVzaChoYW5kbGVPYmopO31cbmpRdWVyeS5ldmVudC5nbG9iYWxbdHlwZV09dHJ1ZTt9fSxyZW1vdmU6ZnVuY3Rpb24oZWxlbSx0eXBlcyxoYW5kbGVyLHNlbGVjdG9yLG1hcHBlZFR5cGVzKXt2YXIgaixvcmlnQ291bnQsdG1wLGV2ZW50cyx0LGhhbmRsZU9iaixzcGVjaWFsLGhhbmRsZXJzLHR5cGUsbmFtZXNwYWNlcyxvcmlnVHlwZSxlbGVtRGF0YT1kYXRhUHJpdi5oYXNEYXRhKGVsZW0pJiZkYXRhUHJpdi5nZXQoZWxlbSk7aWYoIWVsZW1EYXRhfHwhKGV2ZW50cz1lbGVtRGF0YS5ldmVudHMpKXtyZXR1cm47fVxudHlwZXM9KHR5cGVzfHxcIlwiKS5tYXRjaChybm90aHRtbHdoaXRlKXx8W1wiXCJdO3Q9dHlwZXMubGVuZ3RoO3doaWxlKHQtLSl7dG1wPXJ0eXBlbmFtZXNwYWNlLmV4ZWModHlwZXNbdF0pfHxbXTt0eXBlPW9yaWdUeXBlPXRtcFsxXTtuYW1lc3BhY2VzPSh0bXBbMl18fFwiXCIpLnNwbGl0KFwiLlwiKS5zb3J0KCk7aWYoIXR5cGUpe2Zvcih0eXBlIGluIGV2ZW50cyl7alF1ZXJ5LmV2ZW50LnJlbW92ZShlbGVtLHR5cGUrdHlwZXNbdF0saGFuZGxlcixzZWxlY3Rvcix0cnVlKTt9XG5jb250aW51ZTt9XG5zcGVjaWFsPWpRdWVyeS5ldmVudC5zcGVjaWFsW3R5cGVdfHx7fTt0eXBlPShzZWxlY3Rvcj9zcGVjaWFsLmRlbGVnYXRlVHlwZTpzcGVjaWFsLmJpbmRUeXBlKXx8dHlwZTtoYW5kbGVycz1ldmVudHNbdHlwZV18fFtdO3RtcD10bXBbMl0mJm5ldyBSZWdFeHAoXCIoXnxcXFxcLilcIituYW1lc3BhY2VzLmpvaW4oXCJcXFxcLig/Oi4qXFxcXC58KVwiKStcIihcXFxcLnwkKVwiKTtvcmlnQ291bnQ9aj1oYW5kbGVycy5sZW5ndGg7d2hpbGUoai0tKXtoYW5kbGVPYmo9aGFuZGxlcnNbal07aWYoKG1hcHBlZFR5cGVzfHxvcmlnVHlwZT09PWhhbmRsZU9iai5vcmlnVHlwZSkmJighaGFuZGxlcnx8aGFuZGxlci5ndWlkPT09aGFuZGxlT2JqLmd1aWQpJiYoIXRtcHx8dG1wLnRlc3QoaGFuZGxlT2JqLm5hbWVzcGFjZSkpJiYoIXNlbGVjdG9yfHxzZWxlY3Rvcj09PWhhbmRsZU9iai5zZWxlY3Rvcnx8c2VsZWN0b3I9PT1cIioqXCImJmhhbmRsZU9iai5zZWxlY3Rvcikpe2hhbmRsZXJzLnNwbGljZShqLDEpO2lmKGhhbmRsZU9iai5zZWxlY3Rvcil7aGFuZGxlcnMuZGVsZWdhdGVDb3VudC0tO31cbmlmKHNwZWNpYWwucmVtb3ZlKXtzcGVjaWFsLnJlbW92ZS5jYWxsKGVsZW0saGFuZGxlT2JqKTt9fX1cbmlmKG9yaWdDb3VudCYmIWhhbmRsZXJzLmxlbmd0aCl7aWYoIXNwZWNpYWwudGVhcmRvd258fHNwZWNpYWwudGVhcmRvd24uY2FsbChlbGVtLG5hbWVzcGFjZXMsZWxlbURhdGEuaGFuZGxlKT09PWZhbHNlKXtqUXVlcnkucmVtb3ZlRXZlbnQoZWxlbSx0eXBlLGVsZW1EYXRhLmhhbmRsZSk7fVxuZGVsZXRlIGV2ZW50c1t0eXBlXTt9fVxuaWYoalF1ZXJ5LmlzRW1wdHlPYmplY3QoZXZlbnRzKSl7ZGF0YVByaXYucmVtb3ZlKGVsZW0sXCJoYW5kbGUgZXZlbnRzXCIpO319LGRpc3BhdGNoOmZ1bmN0aW9uKG5hdGl2ZUV2ZW50KXt2YXIgaSxqLHJldCxtYXRjaGVkLGhhbmRsZU9iaixoYW5kbGVyUXVldWUsYXJncz1uZXcgQXJyYXkoYXJndW1lbnRzLmxlbmd0aCksZXZlbnQ9alF1ZXJ5LmV2ZW50LmZpeChuYXRpdmVFdmVudCksaGFuZGxlcnM9KGRhdGFQcml2LmdldCh0aGlzLFwiZXZlbnRzXCIpfHxPYmplY3QuY3JlYXRlKG51bGwpKVtldmVudC50eXBlXXx8W10sc3BlY2lhbD1qUXVlcnkuZXZlbnQuc3BlY2lhbFtldmVudC50eXBlXXx8e307YXJnc1swXT1ldmVudDtmb3IoaT0xO2k8YXJndW1lbnRzLmxlbmd0aDtpKyspe2FyZ3NbaV09YXJndW1lbnRzW2ldO31cbmV2ZW50LmRlbGVnYXRlVGFyZ2V0PXRoaXM7aWYoc3BlY2lhbC5wcmVEaXNwYXRjaCYmc3BlY2lhbC5wcmVEaXNwYXRjaC5jYWxsKHRoaXMsZXZlbnQpPT09ZmFsc2Upe3JldHVybjt9XG5oYW5kbGVyUXVldWU9alF1ZXJ5LmV2ZW50LmhhbmRsZXJzLmNhbGwodGhpcyxldmVudCxoYW5kbGVycyk7aT0wO3doaWxlKChtYXRjaGVkPWhhbmRsZXJRdWV1ZVtpKytdKSYmIWV2ZW50LmlzUHJvcGFnYXRpb25TdG9wcGVkKCkpe2V2ZW50LmN1cnJlbnRUYXJnZXQ9bWF0Y2hlZC5lbGVtO2o9MDt3aGlsZSgoaGFuZGxlT2JqPW1hdGNoZWQuaGFuZGxlcnNbaisrXSkmJiFldmVudC5pc0ltbWVkaWF0ZVByb3BhZ2F0aW9uU3RvcHBlZCgpKXtpZighZXZlbnQucm5hbWVzcGFjZXx8aGFuZGxlT2JqLm5hbWVzcGFjZT09PWZhbHNlfHxldmVudC5ybmFtZXNwYWNlLnRlc3QoaGFuZGxlT2JqLm5hbWVzcGFjZSkpe2V2ZW50LmhhbmRsZU9iaj1oYW5kbGVPYmo7ZXZlbnQuZGF0YT1oYW5kbGVPYmouZGF0YTtyZXQ9KChqUXVlcnkuZXZlbnQuc3BlY2lhbFtoYW5kbGVPYmoub3JpZ1R5cGVdfHx7fSkuaGFuZGxlfHxoYW5kbGVPYmouaGFuZGxlcikuYXBwbHkobWF0Y2hlZC5lbGVtLGFyZ3MpO2lmKHJldCE9PXVuZGVmaW5lZCl7aWYoKGV2ZW50LnJlc3VsdD1yZXQpPT09ZmFsc2Upe2V2ZW50LnByZXZlbnREZWZhdWx0KCk7ZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7fX19fX1cbmlmKHNwZWNpYWwucG9zdERpc3BhdGNoKXtzcGVjaWFsLnBvc3REaXNwYXRjaC5jYWxsKHRoaXMsZXZlbnQpO31cbnJldHVybiBldmVudC5yZXN1bHQ7fSxoYW5kbGVyczpmdW5jdGlvbihldmVudCxoYW5kbGVycyl7dmFyIGksaGFuZGxlT2JqLHNlbCxtYXRjaGVkSGFuZGxlcnMsbWF0Y2hlZFNlbGVjdG9ycyxoYW5kbGVyUXVldWU9W10sZGVsZWdhdGVDb3VudD1oYW5kbGVycy5kZWxlZ2F0ZUNvdW50LGN1cj1ldmVudC50YXJnZXQ7aWYoZGVsZWdhdGVDb3VudCYmY3VyLm5vZGVUeXBlJiYhKGV2ZW50LnR5cGU9PT1cImNsaWNrXCImJmV2ZW50LmJ1dHRvbj49MSkpe2Zvcig7Y3VyIT09dGhpcztjdXI9Y3VyLnBhcmVudE5vZGV8fHRoaXMpe2lmKGN1ci5ub2RlVHlwZT09PTEmJiEoZXZlbnQudHlwZT09PVwiY2xpY2tcIiYmY3VyLmRpc2FibGVkPT09dHJ1ZSkpe21hdGNoZWRIYW5kbGVycz1bXTttYXRjaGVkU2VsZWN0b3JzPXt9O2ZvcihpPTA7aTxkZWxlZ2F0ZUNvdW50O2krKyl7aGFuZGxlT2JqPWhhbmRsZXJzW2ldO3NlbD1oYW5kbGVPYmouc2VsZWN0b3IrXCIgXCI7aWYobWF0Y2hlZFNlbGVjdG9yc1tzZWxdPT09dW5kZWZpbmVkKXttYXRjaGVkU2VsZWN0b3JzW3NlbF09aGFuZGxlT2JqLm5lZWRzQ29udGV4dD9qUXVlcnkoc2VsLHRoaXMpLmluZGV4KGN1cik+LTE6alF1ZXJ5LmZpbmQoc2VsLHRoaXMsbnVsbCxbY3VyXSkubGVuZ3RoO31cbmlmKG1hdGNoZWRTZWxlY3RvcnNbc2VsXSl7bWF0Y2hlZEhhbmRsZXJzLnB1c2goaGFuZGxlT2JqKTt9fVxuaWYobWF0Y2hlZEhhbmRsZXJzLmxlbmd0aCl7aGFuZGxlclF1ZXVlLnB1c2goe2VsZW06Y3VyLGhhbmRsZXJzOm1hdGNoZWRIYW5kbGVyc30pO319fX1cbmN1cj10aGlzO2lmKGRlbGVnYXRlQ291bnQ8aGFuZGxlcnMubGVuZ3RoKXtoYW5kbGVyUXVldWUucHVzaCh7ZWxlbTpjdXIsaGFuZGxlcnM6aGFuZGxlcnMuc2xpY2UoZGVsZWdhdGVDb3VudCl9KTt9XG5yZXR1cm4gaGFuZGxlclF1ZXVlO30sYWRkUHJvcDpmdW5jdGlvbihuYW1lLGhvb2spe09iamVjdC5kZWZpbmVQcm9wZXJ0eShqUXVlcnkuRXZlbnQucHJvdG90eXBlLG5hbWUse2VudW1lcmFibGU6dHJ1ZSxjb25maWd1cmFibGU6dHJ1ZSxnZXQ6aXNGdW5jdGlvbihob29rKT9mdW5jdGlvbigpe2lmKHRoaXMub3JpZ2luYWxFdmVudCl7cmV0dXJuIGhvb2sodGhpcy5vcmlnaW5hbEV2ZW50KTt9fTpmdW5jdGlvbigpe2lmKHRoaXMub3JpZ2luYWxFdmVudCl7cmV0dXJuIHRoaXMub3JpZ2luYWxFdmVudFtuYW1lXTt9fSxzZXQ6ZnVuY3Rpb24odmFsdWUpe09iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLG5hbWUse2VudW1lcmFibGU6dHJ1ZSxjb25maWd1cmFibGU6dHJ1ZSx3cml0YWJsZTp0cnVlLHZhbHVlOnZhbHVlfSk7fX0pO30sZml4OmZ1bmN0aW9uKG9yaWdpbmFsRXZlbnQpe3JldHVybiBvcmlnaW5hbEV2ZW50W2pRdWVyeS5leHBhbmRvXT9vcmlnaW5hbEV2ZW50Om5ldyBqUXVlcnkuRXZlbnQob3JpZ2luYWxFdmVudCk7fSxzcGVjaWFsOntsb2FkOntub0J1YmJsZTp0cnVlfSxjbGljazp7c2V0dXA6ZnVuY3Rpb24oZGF0YSl7dmFyIGVsPXRoaXN8fGRhdGE7aWYocmNoZWNrYWJsZVR5cGUudGVzdChlbC50eXBlKSYmZWwuY2xpY2smJm5vZGVOYW1lKGVsLFwiaW5wdXRcIikpe2xldmVyYWdlTmF0aXZlKGVsLFwiY2xpY2tcIixyZXR1cm5UcnVlKTt9XG5yZXR1cm4gZmFsc2U7fSx0cmlnZ2VyOmZ1bmN0aW9uKGRhdGEpe3ZhciBlbD10aGlzfHxkYXRhO2lmKHJjaGVja2FibGVUeXBlLnRlc3QoZWwudHlwZSkmJmVsLmNsaWNrJiZub2RlTmFtZShlbCxcImlucHV0XCIpKXtsZXZlcmFnZU5hdGl2ZShlbCxcImNsaWNrXCIpO31cbnJldHVybiB0cnVlO30sX2RlZmF1bHQ6ZnVuY3Rpb24oZXZlbnQpe3ZhciB0YXJnZXQ9ZXZlbnQudGFyZ2V0O3JldHVybiByY2hlY2thYmxlVHlwZS50ZXN0KHRhcmdldC50eXBlKSYmdGFyZ2V0LmNsaWNrJiZub2RlTmFtZSh0YXJnZXQsXCJpbnB1dFwiKSYmZGF0YVByaXYuZ2V0KHRhcmdldCxcImNsaWNrXCIpfHxub2RlTmFtZSh0YXJnZXQsXCJhXCIpO319LGJlZm9yZXVubG9hZDp7cG9zdERpc3BhdGNoOmZ1bmN0aW9uKGV2ZW50KXtpZihldmVudC5yZXN1bHQhPT11bmRlZmluZWQmJmV2ZW50Lm9yaWdpbmFsRXZlbnQpe2V2ZW50Lm9yaWdpbmFsRXZlbnQucmV0dXJuVmFsdWU9ZXZlbnQucmVzdWx0O319fX19O2Z1bmN0aW9uIGxldmVyYWdlTmF0aXZlKGVsLHR5cGUsZXhwZWN0U3luYyl7aWYoIWV4cGVjdFN5bmMpe2lmKGRhdGFQcml2LmdldChlbCx0eXBlKT09PXVuZGVmaW5lZCl7alF1ZXJ5LmV2ZW50LmFkZChlbCx0eXBlLHJldHVyblRydWUpO31cbnJldHVybjt9XG5kYXRhUHJpdi5zZXQoZWwsdHlwZSxmYWxzZSk7alF1ZXJ5LmV2ZW50LmFkZChlbCx0eXBlLHtuYW1lc3BhY2U6ZmFsc2UsaGFuZGxlcjpmdW5jdGlvbihldmVudCl7dmFyIG5vdEFzeW5jLHJlc3VsdCxzYXZlZD1kYXRhUHJpdi5nZXQodGhpcyx0eXBlKTtpZigoZXZlbnQuaXNUcmlnZ2VyJjEpJiZ0aGlzW3R5cGVdKXtpZighc2F2ZWQubGVuZ3RoKXtzYXZlZD1zbGljZS5jYWxsKGFyZ3VtZW50cyk7ZGF0YVByaXYuc2V0KHRoaXMsdHlwZSxzYXZlZCk7bm90QXN5bmM9ZXhwZWN0U3luYyh0aGlzLHR5cGUpO3RoaXNbdHlwZV0oKTtyZXN1bHQ9ZGF0YVByaXYuZ2V0KHRoaXMsdHlwZSk7aWYoc2F2ZWQhPT1yZXN1bHR8fG5vdEFzeW5jKXtkYXRhUHJpdi5zZXQodGhpcyx0eXBlLGZhbHNlKTt9ZWxzZXtyZXN1bHQ9e307fVxuaWYoc2F2ZWQhPT1yZXN1bHQpe2V2ZW50LnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpO2V2ZW50LnByZXZlbnREZWZhdWx0KCk7cmV0dXJuIHJlc3VsdC52YWx1ZTt9fWVsc2UgaWYoKGpRdWVyeS5ldmVudC5zcGVjaWFsW3R5cGVdfHx7fSkuZGVsZWdhdGVUeXBlKXtldmVudC5zdG9wUHJvcGFnYXRpb24oKTt9fWVsc2UgaWYoc2F2ZWQubGVuZ3RoKXtkYXRhUHJpdi5zZXQodGhpcyx0eXBlLHt2YWx1ZTpqUXVlcnkuZXZlbnQudHJpZ2dlcihqUXVlcnkuZXh0ZW5kKHNhdmVkWzBdLGpRdWVyeS5FdmVudC5wcm90b3R5cGUpLHNhdmVkLnNsaWNlKDEpLHRoaXMpfSk7ZXZlbnQuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uKCk7fX19KTt9XG5qUXVlcnkucmVtb3ZlRXZlbnQ9ZnVuY3Rpb24oZWxlbSx0eXBlLGhhbmRsZSl7aWYoZWxlbS5yZW1vdmVFdmVudExpc3RlbmVyKXtlbGVtLnJlbW92ZUV2ZW50TGlzdGVuZXIodHlwZSxoYW5kbGUpO319O2pRdWVyeS5FdmVudD1mdW5jdGlvbihzcmMscHJvcHMpe2lmKCEodGhpcyBpbnN0YW5jZW9mIGpRdWVyeS5FdmVudCkpe3JldHVybiBuZXcgalF1ZXJ5LkV2ZW50KHNyYyxwcm9wcyk7fVxuaWYoc3JjJiZzcmMudHlwZSl7dGhpcy5vcmlnaW5hbEV2ZW50PXNyYzt0aGlzLnR5cGU9c3JjLnR5cGU7dGhpcy5pc0RlZmF1bHRQcmV2ZW50ZWQ9c3JjLmRlZmF1bHRQcmV2ZW50ZWR8fHNyYy5kZWZhdWx0UHJldmVudGVkPT09dW5kZWZpbmVkJiZzcmMucmV0dXJuVmFsdWU9PT1mYWxzZT9yZXR1cm5UcnVlOnJldHVybkZhbHNlO3RoaXMudGFyZ2V0PShzcmMudGFyZ2V0JiZzcmMudGFyZ2V0Lm5vZGVUeXBlPT09Myk/c3JjLnRhcmdldC5wYXJlbnROb2RlOnNyYy50YXJnZXQ7dGhpcy5jdXJyZW50VGFyZ2V0PXNyYy5jdXJyZW50VGFyZ2V0O3RoaXMucmVsYXRlZFRhcmdldD1zcmMucmVsYXRlZFRhcmdldDt9ZWxzZXt0aGlzLnR5cGU9c3JjO31cbmlmKHByb3BzKXtqUXVlcnkuZXh0ZW5kKHRoaXMscHJvcHMpO31cbnRoaXMudGltZVN0YW1wPXNyYyYmc3JjLnRpbWVTdGFtcHx8RGF0ZS5ub3coKTt0aGlzW2pRdWVyeS5leHBhbmRvXT10cnVlO307alF1ZXJ5LkV2ZW50LnByb3RvdHlwZT17Y29uc3RydWN0b3I6alF1ZXJ5LkV2ZW50LGlzRGVmYXVsdFByZXZlbnRlZDpyZXR1cm5GYWxzZSxpc1Byb3BhZ2F0aW9uU3RvcHBlZDpyZXR1cm5GYWxzZSxpc0ltbWVkaWF0ZVByb3BhZ2F0aW9uU3RvcHBlZDpyZXR1cm5GYWxzZSxpc1NpbXVsYXRlZDpmYWxzZSxwcmV2ZW50RGVmYXVsdDpmdW5jdGlvbigpe3ZhciBlPXRoaXMub3JpZ2luYWxFdmVudDt0aGlzLmlzRGVmYXVsdFByZXZlbnRlZD1yZXR1cm5UcnVlO2lmKGUmJiF0aGlzLmlzU2ltdWxhdGVkKXtlLnByZXZlbnREZWZhdWx0KCk7fX0sc3RvcFByb3BhZ2F0aW9uOmZ1bmN0aW9uKCl7dmFyIGU9dGhpcy5vcmlnaW5hbEV2ZW50O3RoaXMuaXNQcm9wYWdhdGlvblN0b3BwZWQ9cmV0dXJuVHJ1ZTtpZihlJiYhdGhpcy5pc1NpbXVsYXRlZCl7ZS5zdG9wUHJvcGFnYXRpb24oKTt9fSxzdG9wSW1tZWRpYXRlUHJvcGFnYXRpb246ZnVuY3Rpb24oKXt2YXIgZT10aGlzLm9yaWdpbmFsRXZlbnQ7dGhpcy5pc0ltbWVkaWF0ZVByb3BhZ2F0aW9uU3RvcHBlZD1yZXR1cm5UcnVlO2lmKGUmJiF0aGlzLmlzU2ltdWxhdGVkKXtlLnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpO31cbnRoaXMuc3RvcFByb3BhZ2F0aW9uKCk7fX07alF1ZXJ5LmVhY2goe2FsdEtleTp0cnVlLGJ1YmJsZXM6dHJ1ZSxjYW5jZWxhYmxlOnRydWUsY2hhbmdlZFRvdWNoZXM6dHJ1ZSxjdHJsS2V5OnRydWUsZGV0YWlsOnRydWUsZXZlbnRQaGFzZTp0cnVlLG1ldGFLZXk6dHJ1ZSxwYWdlWDp0cnVlLHBhZ2VZOnRydWUsc2hpZnRLZXk6dHJ1ZSx2aWV3OnRydWUsXCJjaGFyXCI6dHJ1ZSxjb2RlOnRydWUsY2hhckNvZGU6dHJ1ZSxrZXk6dHJ1ZSxrZXlDb2RlOnRydWUsYnV0dG9uOnRydWUsYnV0dG9uczp0cnVlLGNsaWVudFg6dHJ1ZSxjbGllbnRZOnRydWUsb2Zmc2V0WDp0cnVlLG9mZnNldFk6dHJ1ZSxwb2ludGVySWQ6dHJ1ZSxwb2ludGVyVHlwZTp0cnVlLHNjcmVlblg6dHJ1ZSxzY3JlZW5ZOnRydWUsdGFyZ2V0VG91Y2hlczp0cnVlLHRvRWxlbWVudDp0cnVlLHRvdWNoZXM6dHJ1ZSx3aGljaDpmdW5jdGlvbihldmVudCl7dmFyIGJ1dHRvbj1ldmVudC5idXR0b247aWYoZXZlbnQud2hpY2g9PW51bGwmJnJrZXlFdmVudC50ZXN0KGV2ZW50LnR5cGUpKXtyZXR1cm4gZXZlbnQuY2hhckNvZGUhPW51bGw/ZXZlbnQuY2hhckNvZGU6ZXZlbnQua2V5Q29kZTt9XG5pZighZXZlbnQud2hpY2gmJmJ1dHRvbiE9PXVuZGVmaW5lZCYmcm1vdXNlRXZlbnQudGVzdChldmVudC50eXBlKSl7aWYoYnV0dG9uJjEpe3JldHVybiAxO31cbmlmKGJ1dHRvbiYyKXtyZXR1cm4gMzt9XG5pZihidXR0b24mNCl7cmV0dXJuIDI7fVxucmV0dXJuIDA7fVxucmV0dXJuIGV2ZW50LndoaWNoO319LGpRdWVyeS5ldmVudC5hZGRQcm9wKTtqUXVlcnkuZWFjaCh7Zm9jdXM6XCJmb2N1c2luXCIsYmx1cjpcImZvY3Vzb3V0XCJ9LGZ1bmN0aW9uKHR5cGUsZGVsZWdhdGVUeXBlKXtqUXVlcnkuZXZlbnQuc3BlY2lhbFt0eXBlXT17c2V0dXA6ZnVuY3Rpb24oKXtsZXZlcmFnZU5hdGl2ZSh0aGlzLHR5cGUsZXhwZWN0U3luYyk7cmV0dXJuIGZhbHNlO30sdHJpZ2dlcjpmdW5jdGlvbigpe2xldmVyYWdlTmF0aXZlKHRoaXMsdHlwZSk7cmV0dXJuIHRydWU7fSxkZWxlZ2F0ZVR5cGU6ZGVsZWdhdGVUeXBlfTt9KTtqUXVlcnkuZWFjaCh7bW91c2VlbnRlcjpcIm1vdXNlb3ZlclwiLG1vdXNlbGVhdmU6XCJtb3VzZW91dFwiLHBvaW50ZXJlbnRlcjpcInBvaW50ZXJvdmVyXCIscG9pbnRlcmxlYXZlOlwicG9pbnRlcm91dFwifSxmdW5jdGlvbihvcmlnLGZpeCl7alF1ZXJ5LmV2ZW50LnNwZWNpYWxbb3JpZ109e2RlbGVnYXRlVHlwZTpmaXgsYmluZFR5cGU6Zml4LGhhbmRsZTpmdW5jdGlvbihldmVudCl7dmFyIHJldCx0YXJnZXQ9dGhpcyxyZWxhdGVkPWV2ZW50LnJlbGF0ZWRUYXJnZXQsaGFuZGxlT2JqPWV2ZW50LmhhbmRsZU9iajtpZighcmVsYXRlZHx8KHJlbGF0ZWQhPT10YXJnZXQmJiFqUXVlcnkuY29udGFpbnModGFyZ2V0LHJlbGF0ZWQpKSl7ZXZlbnQudHlwZT1oYW5kbGVPYmoub3JpZ1R5cGU7cmV0PWhhbmRsZU9iai5oYW5kbGVyLmFwcGx5KHRoaXMsYXJndW1lbnRzKTtldmVudC50eXBlPWZpeDt9XG5yZXR1cm4gcmV0O319O30pO2pRdWVyeS5mbi5leHRlbmQoe29uOmZ1bmN0aW9uKHR5cGVzLHNlbGVjdG9yLGRhdGEsZm4pe3JldHVybiBvbih0aGlzLHR5cGVzLHNlbGVjdG9yLGRhdGEsZm4pO30sb25lOmZ1bmN0aW9uKHR5cGVzLHNlbGVjdG9yLGRhdGEsZm4pe3JldHVybiBvbih0aGlzLHR5cGVzLHNlbGVjdG9yLGRhdGEsZm4sMSk7fSxvZmY6ZnVuY3Rpb24odHlwZXMsc2VsZWN0b3IsZm4pe3ZhciBoYW5kbGVPYmosdHlwZTtpZih0eXBlcyYmdHlwZXMucHJldmVudERlZmF1bHQmJnR5cGVzLmhhbmRsZU9iail7aGFuZGxlT2JqPXR5cGVzLmhhbmRsZU9iajtqUXVlcnkodHlwZXMuZGVsZWdhdGVUYXJnZXQpLm9mZihoYW5kbGVPYmoubmFtZXNwYWNlP2hhbmRsZU9iai5vcmlnVHlwZStcIi5cIitoYW5kbGVPYmoubmFtZXNwYWNlOmhhbmRsZU9iai5vcmlnVHlwZSxoYW5kbGVPYmouc2VsZWN0b3IsaGFuZGxlT2JqLmhhbmRsZXIpO3JldHVybiB0aGlzO31cbmlmKHR5cGVvZiB0eXBlcz09PVwib2JqZWN0XCIpe2Zvcih0eXBlIGluIHR5cGVzKXt0aGlzLm9mZih0eXBlLHNlbGVjdG9yLHR5cGVzW3R5cGVdKTt9XG5yZXR1cm4gdGhpczt9XG5pZihzZWxlY3Rvcj09PWZhbHNlfHx0eXBlb2Ygc2VsZWN0b3I9PT1cImZ1bmN0aW9uXCIpe2ZuPXNlbGVjdG9yO3NlbGVjdG9yPXVuZGVmaW5lZDt9XG5pZihmbj09PWZhbHNlKXtmbj1yZXR1cm5GYWxzZTt9XG5yZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKCl7alF1ZXJ5LmV2ZW50LnJlbW92ZSh0aGlzLHR5cGVzLGZuLHNlbGVjdG9yKTt9KTt9fSk7dmFyXG5ybm9Jbm5lcmh0bWw9LzxzY3JpcHR8PHN0eWxlfDxsaW5rL2kscmNoZWNrZWQ9L2NoZWNrZWRcXHMqKD86W149XXw9XFxzKi5jaGVja2VkLikvaSxyY2xlYW5TY3JpcHQ9L15cXHMqPCEoPzpcXFtDREFUQVxcW3wtLSl8KD86XFxdXFxdfC0tKT5cXHMqJC9nO2Z1bmN0aW9uIG1hbmlwdWxhdGlvblRhcmdldChlbGVtLGNvbnRlbnQpe2lmKG5vZGVOYW1lKGVsZW0sXCJ0YWJsZVwiKSYmbm9kZU5hbWUoY29udGVudC5ub2RlVHlwZSE9PTExP2NvbnRlbnQ6Y29udGVudC5maXJzdENoaWxkLFwidHJcIikpe3JldHVybiBqUXVlcnkoZWxlbSkuY2hpbGRyZW4oXCJ0Ym9keVwiKVswXXx8ZWxlbTt9XG5yZXR1cm4gZWxlbTt9XG5mdW5jdGlvbiBkaXNhYmxlU2NyaXB0KGVsZW0pe2VsZW0udHlwZT0oZWxlbS5nZXRBdHRyaWJ1dGUoXCJ0eXBlXCIpIT09bnVsbCkrXCIvXCIrZWxlbS50eXBlO3JldHVybiBlbGVtO31cbmZ1bmN0aW9uIHJlc3RvcmVTY3JpcHQoZWxlbSl7aWYoKGVsZW0udHlwZXx8XCJcIikuc2xpY2UoMCw1KT09PVwidHJ1ZS9cIil7ZWxlbS50eXBlPWVsZW0udHlwZS5zbGljZSg1KTt9ZWxzZXtlbGVtLnJlbW92ZUF0dHJpYnV0ZShcInR5cGVcIik7fVxucmV0dXJuIGVsZW07fVxuZnVuY3Rpb24gY2xvbmVDb3B5RXZlbnQoc3JjLGRlc3Qpe3ZhciBpLGwsdHlwZSxwZGF0YU9sZCx1ZGF0YU9sZCx1ZGF0YUN1cixldmVudHM7aWYoZGVzdC5ub2RlVHlwZSE9PTEpe3JldHVybjt9XG5pZihkYXRhUHJpdi5oYXNEYXRhKHNyYykpe3BkYXRhT2xkPWRhdGFQcml2LmdldChzcmMpO2V2ZW50cz1wZGF0YU9sZC5ldmVudHM7aWYoZXZlbnRzKXtkYXRhUHJpdi5yZW1vdmUoZGVzdCxcImhhbmRsZSBldmVudHNcIik7Zm9yKHR5cGUgaW4gZXZlbnRzKXtmb3IoaT0wLGw9ZXZlbnRzW3R5cGVdLmxlbmd0aDtpPGw7aSsrKXtqUXVlcnkuZXZlbnQuYWRkKGRlc3QsdHlwZSxldmVudHNbdHlwZV1baV0pO319fX1cbmlmKGRhdGFVc2VyLmhhc0RhdGEoc3JjKSl7dWRhdGFPbGQ9ZGF0YVVzZXIuYWNjZXNzKHNyYyk7dWRhdGFDdXI9alF1ZXJ5LmV4dGVuZCh7fSx1ZGF0YU9sZCk7ZGF0YVVzZXIuc2V0KGRlc3QsdWRhdGFDdXIpO319XG5mdW5jdGlvbiBmaXhJbnB1dChzcmMsZGVzdCl7dmFyIG5vZGVOYW1lPWRlc3Qubm9kZU5hbWUudG9Mb3dlckNhc2UoKTtpZihub2RlTmFtZT09PVwiaW5wdXRcIiYmcmNoZWNrYWJsZVR5cGUudGVzdChzcmMudHlwZSkpe2Rlc3QuY2hlY2tlZD1zcmMuY2hlY2tlZDt9ZWxzZSBpZihub2RlTmFtZT09PVwiaW5wdXRcInx8bm9kZU5hbWU9PT1cInRleHRhcmVhXCIpe2Rlc3QuZGVmYXVsdFZhbHVlPXNyYy5kZWZhdWx0VmFsdWU7fX1cbmZ1bmN0aW9uIGRvbU1hbmlwKGNvbGxlY3Rpb24sYXJncyxjYWxsYmFjayxpZ25vcmVkKXthcmdzPWZsYXQoYXJncyk7dmFyIGZyYWdtZW50LGZpcnN0LHNjcmlwdHMsaGFzU2NyaXB0cyxub2RlLGRvYyxpPTAsbD1jb2xsZWN0aW9uLmxlbmd0aCxpTm9DbG9uZT1sLTEsdmFsdWU9YXJnc1swXSx2YWx1ZUlzRnVuY3Rpb249aXNGdW5jdGlvbih2YWx1ZSk7aWYodmFsdWVJc0Z1bmN0aW9ufHwobD4xJiZ0eXBlb2YgdmFsdWU9PT1cInN0cmluZ1wiJiYhc3VwcG9ydC5jaGVja0Nsb25lJiZyY2hlY2tlZC50ZXN0KHZhbHVlKSkpe3JldHVybiBjb2xsZWN0aW9uLmVhY2goZnVuY3Rpb24oaW5kZXgpe3ZhciBzZWxmPWNvbGxlY3Rpb24uZXEoaW5kZXgpO2lmKHZhbHVlSXNGdW5jdGlvbil7YXJnc1swXT12YWx1ZS5jYWxsKHRoaXMsaW5kZXgsc2VsZi5odG1sKCkpO31cbmRvbU1hbmlwKHNlbGYsYXJncyxjYWxsYmFjayxpZ25vcmVkKTt9KTt9XG5pZihsKXtmcmFnbWVudD1idWlsZEZyYWdtZW50KGFyZ3MsY29sbGVjdGlvblswXS5vd25lckRvY3VtZW50LGZhbHNlLGNvbGxlY3Rpb24saWdub3JlZCk7Zmlyc3Q9ZnJhZ21lbnQuZmlyc3RDaGlsZDtpZihmcmFnbWVudC5jaGlsZE5vZGVzLmxlbmd0aD09PTEpe2ZyYWdtZW50PWZpcnN0O31cbmlmKGZpcnN0fHxpZ25vcmVkKXtzY3JpcHRzPWpRdWVyeS5tYXAoZ2V0QWxsKGZyYWdtZW50LFwic2NyaXB0XCIpLGRpc2FibGVTY3JpcHQpO2hhc1NjcmlwdHM9c2NyaXB0cy5sZW5ndGg7Zm9yKDtpPGw7aSsrKXtub2RlPWZyYWdtZW50O2lmKGkhPT1pTm9DbG9uZSl7bm9kZT1qUXVlcnkuY2xvbmUobm9kZSx0cnVlLHRydWUpO2lmKGhhc1NjcmlwdHMpe2pRdWVyeS5tZXJnZShzY3JpcHRzLGdldEFsbChub2RlLFwic2NyaXB0XCIpKTt9fVxuY2FsbGJhY2suY2FsbChjb2xsZWN0aW9uW2ldLG5vZGUsaSk7fVxuaWYoaGFzU2NyaXB0cyl7ZG9jPXNjcmlwdHNbc2NyaXB0cy5sZW5ndGgtMV0ub3duZXJEb2N1bWVudDtqUXVlcnkubWFwKHNjcmlwdHMscmVzdG9yZVNjcmlwdCk7Zm9yKGk9MDtpPGhhc1NjcmlwdHM7aSsrKXtub2RlPXNjcmlwdHNbaV07aWYocnNjcmlwdFR5cGUudGVzdChub2RlLnR5cGV8fFwiXCIpJiYhZGF0YVByaXYuYWNjZXNzKG5vZGUsXCJnbG9iYWxFdmFsXCIpJiZqUXVlcnkuY29udGFpbnMoZG9jLG5vZGUpKXtpZihub2RlLnNyYyYmKG5vZGUudHlwZXx8XCJcIikudG9Mb3dlckNhc2UoKSE9PVwibW9kdWxlXCIpe2lmKGpRdWVyeS5fZXZhbFVybCYmIW5vZGUubm9Nb2R1bGUpe2pRdWVyeS5fZXZhbFVybChub2RlLnNyYyx7bm9uY2U6bm9kZS5ub25jZXx8bm9kZS5nZXRBdHRyaWJ1dGUoXCJub25jZVwiKX0sZG9jKTt9fWVsc2V7RE9NRXZhbChub2RlLnRleHRDb250ZW50LnJlcGxhY2UocmNsZWFuU2NyaXB0LFwiXCIpLG5vZGUsZG9jKTt9fX19fX1cbnJldHVybiBjb2xsZWN0aW9uO31cbmZ1bmN0aW9uIHJlbW92ZShlbGVtLHNlbGVjdG9yLGtlZXBEYXRhKXt2YXIgbm9kZSxub2Rlcz1zZWxlY3Rvcj9qUXVlcnkuZmlsdGVyKHNlbGVjdG9yLGVsZW0pOmVsZW0saT0wO2Zvcig7KG5vZGU9bm9kZXNbaV0pIT1udWxsO2krKyl7aWYoIWtlZXBEYXRhJiZub2RlLm5vZGVUeXBlPT09MSl7alF1ZXJ5LmNsZWFuRGF0YShnZXRBbGwobm9kZSkpO31cbmlmKG5vZGUucGFyZW50Tm9kZSl7aWYoa2VlcERhdGEmJmlzQXR0YWNoZWQobm9kZSkpe3NldEdsb2JhbEV2YWwoZ2V0QWxsKG5vZGUsXCJzY3JpcHRcIikpO31cbm5vZGUucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChub2RlKTt9fVxucmV0dXJuIGVsZW07fVxualF1ZXJ5LmV4dGVuZCh7aHRtbFByZWZpbHRlcjpmdW5jdGlvbihodG1sKXtyZXR1cm4gaHRtbDt9LGNsb25lOmZ1bmN0aW9uKGVsZW0sZGF0YUFuZEV2ZW50cyxkZWVwRGF0YUFuZEV2ZW50cyl7dmFyIGksbCxzcmNFbGVtZW50cyxkZXN0RWxlbWVudHMsY2xvbmU9ZWxlbS5jbG9uZU5vZGUodHJ1ZSksaW5QYWdlPWlzQXR0YWNoZWQoZWxlbSk7aWYoIXN1cHBvcnQubm9DbG9uZUNoZWNrZWQmJihlbGVtLm5vZGVUeXBlPT09MXx8ZWxlbS5ub2RlVHlwZT09PTExKSYmIWpRdWVyeS5pc1hNTERvYyhlbGVtKSl7ZGVzdEVsZW1lbnRzPWdldEFsbChjbG9uZSk7c3JjRWxlbWVudHM9Z2V0QWxsKGVsZW0pO2ZvcihpPTAsbD1zcmNFbGVtZW50cy5sZW5ndGg7aTxsO2krKyl7Zml4SW5wdXQoc3JjRWxlbWVudHNbaV0sZGVzdEVsZW1lbnRzW2ldKTt9fVxuaWYoZGF0YUFuZEV2ZW50cyl7aWYoZGVlcERhdGFBbmRFdmVudHMpe3NyY0VsZW1lbnRzPXNyY0VsZW1lbnRzfHxnZXRBbGwoZWxlbSk7ZGVzdEVsZW1lbnRzPWRlc3RFbGVtZW50c3x8Z2V0QWxsKGNsb25lKTtmb3IoaT0wLGw9c3JjRWxlbWVudHMubGVuZ3RoO2k8bDtpKyspe2Nsb25lQ29weUV2ZW50KHNyY0VsZW1lbnRzW2ldLGRlc3RFbGVtZW50c1tpXSk7fX1lbHNle2Nsb25lQ29weUV2ZW50KGVsZW0sY2xvbmUpO319XG5kZXN0RWxlbWVudHM9Z2V0QWxsKGNsb25lLFwic2NyaXB0XCIpO2lmKGRlc3RFbGVtZW50cy5sZW5ndGg+MCl7c2V0R2xvYmFsRXZhbChkZXN0RWxlbWVudHMsIWluUGFnZSYmZ2V0QWxsKGVsZW0sXCJzY3JpcHRcIikpO31cbnJldHVybiBjbG9uZTt9LGNsZWFuRGF0YTpmdW5jdGlvbihlbGVtcyl7dmFyIGRhdGEsZWxlbSx0eXBlLHNwZWNpYWw9alF1ZXJ5LmV2ZW50LnNwZWNpYWwsaT0wO2Zvcig7KGVsZW09ZWxlbXNbaV0pIT09dW5kZWZpbmVkO2krKyl7aWYoYWNjZXB0RGF0YShlbGVtKSl7aWYoKGRhdGE9ZWxlbVtkYXRhUHJpdi5leHBhbmRvXSkpe2lmKGRhdGEuZXZlbnRzKXtmb3IodHlwZSBpbiBkYXRhLmV2ZW50cyl7aWYoc3BlY2lhbFt0eXBlXSl7alF1ZXJ5LmV2ZW50LnJlbW92ZShlbGVtLHR5cGUpO31lbHNle2pRdWVyeS5yZW1vdmVFdmVudChlbGVtLHR5cGUsZGF0YS5oYW5kbGUpO319fVxuZWxlbVtkYXRhUHJpdi5leHBhbmRvXT11bmRlZmluZWQ7fVxuaWYoZWxlbVtkYXRhVXNlci5leHBhbmRvXSl7ZWxlbVtkYXRhVXNlci5leHBhbmRvXT11bmRlZmluZWQ7fX19fX0pO2pRdWVyeS5mbi5leHRlbmQoe2RldGFjaDpmdW5jdGlvbihzZWxlY3Rvcil7cmV0dXJuIHJlbW92ZSh0aGlzLHNlbGVjdG9yLHRydWUpO30scmVtb3ZlOmZ1bmN0aW9uKHNlbGVjdG9yKXtyZXR1cm4gcmVtb3ZlKHRoaXMsc2VsZWN0b3IpO30sdGV4dDpmdW5jdGlvbih2YWx1ZSl7cmV0dXJuIGFjY2Vzcyh0aGlzLGZ1bmN0aW9uKHZhbHVlKXtyZXR1cm4gdmFsdWU9PT11bmRlZmluZWQ/alF1ZXJ5LnRleHQodGhpcyk6dGhpcy5lbXB0eSgpLmVhY2goZnVuY3Rpb24oKXtpZih0aGlzLm5vZGVUeXBlPT09MXx8dGhpcy5ub2RlVHlwZT09PTExfHx0aGlzLm5vZGVUeXBlPT09OSl7dGhpcy50ZXh0Q29udGVudD12YWx1ZTt9fSk7fSxudWxsLHZhbHVlLGFyZ3VtZW50cy5sZW5ndGgpO30sYXBwZW5kOmZ1bmN0aW9uKCl7cmV0dXJuIGRvbU1hbmlwKHRoaXMsYXJndW1lbnRzLGZ1bmN0aW9uKGVsZW0pe2lmKHRoaXMubm9kZVR5cGU9PT0xfHx0aGlzLm5vZGVUeXBlPT09MTF8fHRoaXMubm9kZVR5cGU9PT05KXt2YXIgdGFyZ2V0PW1hbmlwdWxhdGlvblRhcmdldCh0aGlzLGVsZW0pO3RhcmdldC5hcHBlbmRDaGlsZChlbGVtKTt9fSk7fSxwcmVwZW5kOmZ1bmN0aW9uKCl7cmV0dXJuIGRvbU1hbmlwKHRoaXMsYXJndW1lbnRzLGZ1bmN0aW9uKGVsZW0pe2lmKHRoaXMubm9kZVR5cGU9PT0xfHx0aGlzLm5vZGVUeXBlPT09MTF8fHRoaXMubm9kZVR5cGU9PT05KXt2YXIgdGFyZ2V0PW1hbmlwdWxhdGlvblRhcmdldCh0aGlzLGVsZW0pO3RhcmdldC5pbnNlcnRCZWZvcmUoZWxlbSx0YXJnZXQuZmlyc3RDaGlsZCk7fX0pO30sYmVmb3JlOmZ1bmN0aW9uKCl7cmV0dXJuIGRvbU1hbmlwKHRoaXMsYXJndW1lbnRzLGZ1bmN0aW9uKGVsZW0pe2lmKHRoaXMucGFyZW50Tm9kZSl7dGhpcy5wYXJlbnROb2RlLmluc2VydEJlZm9yZShlbGVtLHRoaXMpO319KTt9LGFmdGVyOmZ1bmN0aW9uKCl7cmV0dXJuIGRvbU1hbmlwKHRoaXMsYXJndW1lbnRzLGZ1bmN0aW9uKGVsZW0pe2lmKHRoaXMucGFyZW50Tm9kZSl7dGhpcy5wYXJlbnROb2RlLmluc2VydEJlZm9yZShlbGVtLHRoaXMubmV4dFNpYmxpbmcpO319KTt9LGVtcHR5OmZ1bmN0aW9uKCl7dmFyIGVsZW0saT0wO2Zvcig7KGVsZW09dGhpc1tpXSkhPW51bGw7aSsrKXtpZihlbGVtLm5vZGVUeXBlPT09MSl7alF1ZXJ5LmNsZWFuRGF0YShnZXRBbGwoZWxlbSxmYWxzZSkpO2VsZW0udGV4dENvbnRlbnQ9XCJcIjt9fVxucmV0dXJuIHRoaXM7fSxjbG9uZTpmdW5jdGlvbihkYXRhQW5kRXZlbnRzLGRlZXBEYXRhQW5kRXZlbnRzKXtkYXRhQW5kRXZlbnRzPWRhdGFBbmRFdmVudHM9PW51bGw/ZmFsc2U6ZGF0YUFuZEV2ZW50cztkZWVwRGF0YUFuZEV2ZW50cz1kZWVwRGF0YUFuZEV2ZW50cz09bnVsbD9kYXRhQW5kRXZlbnRzOmRlZXBEYXRhQW5kRXZlbnRzO3JldHVybiB0aGlzLm1hcChmdW5jdGlvbigpe3JldHVybiBqUXVlcnkuY2xvbmUodGhpcyxkYXRhQW5kRXZlbnRzLGRlZXBEYXRhQW5kRXZlbnRzKTt9KTt9LGh0bWw6ZnVuY3Rpb24odmFsdWUpe3JldHVybiBhY2Nlc3ModGhpcyxmdW5jdGlvbih2YWx1ZSl7dmFyIGVsZW09dGhpc1swXXx8e30saT0wLGw9dGhpcy5sZW5ndGg7aWYodmFsdWU9PT11bmRlZmluZWQmJmVsZW0ubm9kZVR5cGU9PT0xKXtyZXR1cm4gZWxlbS5pbm5lckhUTUw7fVxuaWYodHlwZW9mIHZhbHVlPT09XCJzdHJpbmdcIiYmIXJub0lubmVyaHRtbC50ZXN0KHZhbHVlKSYmIXdyYXBNYXBbKHJ0YWdOYW1lLmV4ZWModmFsdWUpfHxbXCJcIixcIlwiXSlbMV0udG9Mb3dlckNhc2UoKV0pe3ZhbHVlPWpRdWVyeS5odG1sUHJlZmlsdGVyKHZhbHVlKTt0cnl7Zm9yKDtpPGw7aSsrKXtlbGVtPXRoaXNbaV18fHt9O2lmKGVsZW0ubm9kZVR5cGU9PT0xKXtqUXVlcnkuY2xlYW5EYXRhKGdldEFsbChlbGVtLGZhbHNlKSk7ZWxlbS5pbm5lckhUTUw9dmFsdWU7fX1cbmVsZW09MDt9Y2F0Y2goZSl7fX1cbmlmKGVsZW0pe3RoaXMuZW1wdHkoKS5hcHBlbmQodmFsdWUpO319LG51bGwsdmFsdWUsYXJndW1lbnRzLmxlbmd0aCk7fSxyZXBsYWNlV2l0aDpmdW5jdGlvbigpe3ZhciBpZ25vcmVkPVtdO3JldHVybiBkb21NYW5pcCh0aGlzLGFyZ3VtZW50cyxmdW5jdGlvbihlbGVtKXt2YXIgcGFyZW50PXRoaXMucGFyZW50Tm9kZTtpZihqUXVlcnkuaW5BcnJheSh0aGlzLGlnbm9yZWQpPDApe2pRdWVyeS5jbGVhbkRhdGEoZ2V0QWxsKHRoaXMpKTtpZihwYXJlbnQpe3BhcmVudC5yZXBsYWNlQ2hpbGQoZWxlbSx0aGlzKTt9fX0saWdub3JlZCk7fX0pO2pRdWVyeS5lYWNoKHthcHBlbmRUbzpcImFwcGVuZFwiLHByZXBlbmRUbzpcInByZXBlbmRcIixpbnNlcnRCZWZvcmU6XCJiZWZvcmVcIixpbnNlcnRBZnRlcjpcImFmdGVyXCIscmVwbGFjZUFsbDpcInJlcGxhY2VXaXRoXCJ9LGZ1bmN0aW9uKG5hbWUsb3JpZ2luYWwpe2pRdWVyeS5mbltuYW1lXT1mdW5jdGlvbihzZWxlY3Rvcil7dmFyIGVsZW1zLHJldD1bXSxpbnNlcnQ9alF1ZXJ5KHNlbGVjdG9yKSxsYXN0PWluc2VydC5sZW5ndGgtMSxpPTA7Zm9yKDtpPD1sYXN0O2krKyl7ZWxlbXM9aT09PWxhc3Q/dGhpczp0aGlzLmNsb25lKHRydWUpO2pRdWVyeShpbnNlcnRbaV0pW29yaWdpbmFsXShlbGVtcyk7cHVzaC5hcHBseShyZXQsZWxlbXMuZ2V0KCkpO31cbnJldHVybiB0aGlzLnB1c2hTdGFjayhyZXQpO307fSk7dmFyIHJudW1ub25weD1uZXcgUmVnRXhwKFwiXihcIitwbnVtK1wiKSg/IXB4KVthLXolXSskXCIsXCJpXCIpO3ZhciBnZXRTdHlsZXM9ZnVuY3Rpb24oZWxlbSl7dmFyIHZpZXc9ZWxlbS5vd25lckRvY3VtZW50LmRlZmF1bHRWaWV3O2lmKCF2aWV3fHwhdmlldy5vcGVuZXIpe3ZpZXc9d2luZG93O31cbnJldHVybiB2aWV3LmdldENvbXB1dGVkU3R5bGUoZWxlbSk7fTt2YXIgc3dhcD1mdW5jdGlvbihlbGVtLG9wdGlvbnMsY2FsbGJhY2spe3ZhciByZXQsbmFtZSxvbGQ9e307Zm9yKG5hbWUgaW4gb3B0aW9ucyl7b2xkW25hbWVdPWVsZW0uc3R5bGVbbmFtZV07ZWxlbS5zdHlsZVtuYW1lXT1vcHRpb25zW25hbWVdO31cbnJldD1jYWxsYmFjay5jYWxsKGVsZW0pO2ZvcihuYW1lIGluIG9wdGlvbnMpe2VsZW0uc3R5bGVbbmFtZV09b2xkW25hbWVdO31cbnJldHVybiByZXQ7fTt2YXIgcmJveFN0eWxlPW5ldyBSZWdFeHAoY3NzRXhwYW5kLmpvaW4oXCJ8XCIpLFwiaVwiKTsoZnVuY3Rpb24oKXtmdW5jdGlvbiBjb21wdXRlU3R5bGVUZXN0cygpe2lmKCFkaXYpe3JldHVybjt9XG5jb250YWluZXIuc3R5bGUuY3NzVGV4dD1cInBvc2l0aW9uOmFic29sdXRlO2xlZnQ6LTExMTExcHg7d2lkdGg6NjBweDtcIitcIm1hcmdpbi10b3A6MXB4O3BhZGRpbmc6MDtib3JkZXI6MFwiO2Rpdi5zdHlsZS5jc3NUZXh0PVwicG9zaXRpb246cmVsYXRpdmU7ZGlzcGxheTpibG9jaztib3gtc2l6aW5nOmJvcmRlci1ib3g7b3ZlcmZsb3c6c2Nyb2xsO1wiK1wibWFyZ2luOmF1dG87Ym9yZGVyOjFweDtwYWRkaW5nOjFweDtcIitcIndpZHRoOjYwJTt0b3A6MSVcIjtkb2N1bWVudEVsZW1lbnQuYXBwZW5kQ2hpbGQoY29udGFpbmVyKS5hcHBlbmRDaGlsZChkaXYpO3ZhciBkaXZTdHlsZT13aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShkaXYpO3BpeGVsUG9zaXRpb25WYWw9ZGl2U3R5bGUudG9wIT09XCIxJVwiO3JlbGlhYmxlTWFyZ2luTGVmdFZhbD1yb3VuZFBpeGVsTWVhc3VyZXMoZGl2U3R5bGUubWFyZ2luTGVmdCk9PT0xMjtkaXYuc3R5bGUucmlnaHQ9XCI2MCVcIjtwaXhlbEJveFN0eWxlc1ZhbD1yb3VuZFBpeGVsTWVhc3VyZXMoZGl2U3R5bGUucmlnaHQpPT09MzY7Ym94U2l6aW5nUmVsaWFibGVWYWw9cm91bmRQaXhlbE1lYXN1cmVzKGRpdlN0eWxlLndpZHRoKT09PTM2O2Rpdi5zdHlsZS5wb3NpdGlvbj1cImFic29sdXRlXCI7c2Nyb2xsYm94U2l6ZVZhbD1yb3VuZFBpeGVsTWVhc3VyZXMoZGl2Lm9mZnNldFdpZHRoLzMpPT09MTI7ZG9jdW1lbnRFbGVtZW50LnJlbW92ZUNoaWxkKGNvbnRhaW5lcik7ZGl2PW51bGw7fVxuZnVuY3Rpb24gcm91bmRQaXhlbE1lYXN1cmVzKG1lYXN1cmUpe3JldHVybiBNYXRoLnJvdW5kKHBhcnNlRmxvYXQobWVhc3VyZSkpO31cbnZhciBwaXhlbFBvc2l0aW9uVmFsLGJveFNpemluZ1JlbGlhYmxlVmFsLHNjcm9sbGJveFNpemVWYWwscGl4ZWxCb3hTdHlsZXNWYWwscmVsaWFibGVUckRpbWVuc2lvbnNWYWwscmVsaWFibGVNYXJnaW5MZWZ0VmFsLGNvbnRhaW5lcj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpLGRpdj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO2lmKCFkaXYuc3R5bGUpe3JldHVybjt9XG5kaXYuc3R5bGUuYmFja2dyb3VuZENsaXA9XCJjb250ZW50LWJveFwiO2Rpdi5jbG9uZU5vZGUodHJ1ZSkuc3R5bGUuYmFja2dyb3VuZENsaXA9XCJcIjtzdXBwb3J0LmNsZWFyQ2xvbmVTdHlsZT1kaXYuc3R5bGUuYmFja2dyb3VuZENsaXA9PT1cImNvbnRlbnQtYm94XCI7alF1ZXJ5LmV4dGVuZChzdXBwb3J0LHtib3hTaXppbmdSZWxpYWJsZTpmdW5jdGlvbigpe2NvbXB1dGVTdHlsZVRlc3RzKCk7cmV0dXJuIGJveFNpemluZ1JlbGlhYmxlVmFsO30scGl4ZWxCb3hTdHlsZXM6ZnVuY3Rpb24oKXtjb21wdXRlU3R5bGVUZXN0cygpO3JldHVybiBwaXhlbEJveFN0eWxlc1ZhbDt9LHBpeGVsUG9zaXRpb246ZnVuY3Rpb24oKXtjb21wdXRlU3R5bGVUZXN0cygpO3JldHVybiBwaXhlbFBvc2l0aW9uVmFsO30scmVsaWFibGVNYXJnaW5MZWZ0OmZ1bmN0aW9uKCl7Y29tcHV0ZVN0eWxlVGVzdHMoKTtyZXR1cm4gcmVsaWFibGVNYXJnaW5MZWZ0VmFsO30sc2Nyb2xsYm94U2l6ZTpmdW5jdGlvbigpe2NvbXB1dGVTdHlsZVRlc3RzKCk7cmV0dXJuIHNjcm9sbGJveFNpemVWYWw7fSxyZWxpYWJsZVRyRGltZW5zaW9uczpmdW5jdGlvbigpe3ZhciB0YWJsZSx0cix0ckNoaWxkLHRyU3R5bGU7aWYocmVsaWFibGVUckRpbWVuc2lvbnNWYWw9PW51bGwpe3RhYmxlPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJ0YWJsZVwiKTt0cj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwidHJcIik7dHJDaGlsZD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO3RhYmxlLnN0eWxlLmNzc1RleHQ9XCJwb3NpdGlvbjphYnNvbHV0ZTtsZWZ0Oi0xMTExMXB4XCI7dHIuc3R5bGUuaGVpZ2h0PVwiMXB4XCI7dHJDaGlsZC5zdHlsZS5oZWlnaHQ9XCI5cHhcIjtkb2N1bWVudEVsZW1lbnQuYXBwZW5kQ2hpbGQodGFibGUpLmFwcGVuZENoaWxkKHRyKS5hcHBlbmRDaGlsZCh0ckNoaWxkKTt0clN0eWxlPXdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKHRyKTtyZWxpYWJsZVRyRGltZW5zaW9uc1ZhbD1wYXJzZUludCh0clN0eWxlLmhlaWdodCk+Mztkb2N1bWVudEVsZW1lbnQucmVtb3ZlQ2hpbGQodGFibGUpO31cbnJldHVybiByZWxpYWJsZVRyRGltZW5zaW9uc1ZhbDt9fSk7fSkoKTtmdW5jdGlvbiBjdXJDU1MoZWxlbSxuYW1lLGNvbXB1dGVkKXt2YXIgd2lkdGgsbWluV2lkdGgsbWF4V2lkdGgscmV0LHN0eWxlPWVsZW0uc3R5bGU7Y29tcHV0ZWQ9Y29tcHV0ZWR8fGdldFN0eWxlcyhlbGVtKTtpZihjb21wdXRlZCl7cmV0PWNvbXB1dGVkLmdldFByb3BlcnR5VmFsdWUobmFtZSl8fGNvbXB1dGVkW25hbWVdO2lmKHJldD09PVwiXCImJiFpc0F0dGFjaGVkKGVsZW0pKXtyZXQ9alF1ZXJ5LnN0eWxlKGVsZW0sbmFtZSk7fVxuaWYoIXN1cHBvcnQucGl4ZWxCb3hTdHlsZXMoKSYmcm51bW5vbnB4LnRlc3QocmV0KSYmcmJveFN0eWxlLnRlc3QobmFtZSkpe3dpZHRoPXN0eWxlLndpZHRoO21pbldpZHRoPXN0eWxlLm1pbldpZHRoO21heFdpZHRoPXN0eWxlLm1heFdpZHRoO3N0eWxlLm1pbldpZHRoPXN0eWxlLm1heFdpZHRoPXN0eWxlLndpZHRoPXJldDtyZXQ9Y29tcHV0ZWQud2lkdGg7c3R5bGUud2lkdGg9d2lkdGg7c3R5bGUubWluV2lkdGg9bWluV2lkdGg7c3R5bGUubWF4V2lkdGg9bWF4V2lkdGg7fX1cbnJldHVybiByZXQhPT11bmRlZmluZWQ/cmV0K1wiXCI6cmV0O31cbmZ1bmN0aW9uIGFkZEdldEhvb2tJZihjb25kaXRpb25Gbixob29rRm4pe3JldHVybntnZXQ6ZnVuY3Rpb24oKXtpZihjb25kaXRpb25GbigpKXtkZWxldGUgdGhpcy5nZXQ7cmV0dXJuO31cbnJldHVybih0aGlzLmdldD1ob29rRm4pLmFwcGx5KHRoaXMsYXJndW1lbnRzKTt9fTt9XG52YXIgY3NzUHJlZml4ZXM9W1wiV2Via2l0XCIsXCJNb3pcIixcIm1zXCJdLGVtcHR5U3R5bGU9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKS5zdHlsZSx2ZW5kb3JQcm9wcz17fTtmdW5jdGlvbiB2ZW5kb3JQcm9wTmFtZShuYW1lKXt2YXIgY2FwTmFtZT1uYW1lWzBdLnRvVXBwZXJDYXNlKCkrbmFtZS5zbGljZSgxKSxpPWNzc1ByZWZpeGVzLmxlbmd0aDt3aGlsZShpLS0pe25hbWU9Y3NzUHJlZml4ZXNbaV0rY2FwTmFtZTtpZihuYW1lIGluIGVtcHR5U3R5bGUpe3JldHVybiBuYW1lO319fVxuZnVuY3Rpb24gZmluYWxQcm9wTmFtZShuYW1lKXt2YXIgZmluYWw9alF1ZXJ5LmNzc1Byb3BzW25hbWVdfHx2ZW5kb3JQcm9wc1tuYW1lXTtpZihmaW5hbCl7cmV0dXJuIGZpbmFsO31cbmlmKG5hbWUgaW4gZW1wdHlTdHlsZSl7cmV0dXJuIG5hbWU7fVxucmV0dXJuIHZlbmRvclByb3BzW25hbWVdPXZlbmRvclByb3BOYW1lKG5hbWUpfHxuYW1lO31cbnZhclxucmRpc3BsYXlzd2FwPS9eKG5vbmV8dGFibGUoPyEtY1tlYV0pLispLyxyY3VzdG9tUHJvcD0vXi0tLyxjc3NTaG93PXtwb3NpdGlvbjpcImFic29sdXRlXCIsdmlzaWJpbGl0eTpcImhpZGRlblwiLGRpc3BsYXk6XCJibG9ja1wifSxjc3NOb3JtYWxUcmFuc2Zvcm09e2xldHRlclNwYWNpbmc6XCIwXCIsZm9udFdlaWdodDpcIjQwMFwifTtmdW5jdGlvbiBzZXRQb3NpdGl2ZU51bWJlcihfZWxlbSx2YWx1ZSxzdWJ0cmFjdCl7dmFyIG1hdGNoZXM9cmNzc051bS5leGVjKHZhbHVlKTtyZXR1cm4gbWF0Y2hlcz9NYXRoLm1heCgwLG1hdGNoZXNbMl0tKHN1YnRyYWN0fHwwKSkrKG1hdGNoZXNbM118fFwicHhcIik6dmFsdWU7fVxuZnVuY3Rpb24gYm94TW9kZWxBZGp1c3RtZW50KGVsZW0sZGltZW5zaW9uLGJveCxpc0JvcmRlckJveCxzdHlsZXMsY29tcHV0ZWRWYWwpe3ZhciBpPWRpbWVuc2lvbj09PVwid2lkdGhcIj8xOjAsZXh0cmE9MCxkZWx0YT0wO2lmKGJveD09PShpc0JvcmRlckJveD9cImJvcmRlclwiOlwiY29udGVudFwiKSl7cmV0dXJuIDA7fVxuZm9yKDtpPDQ7aSs9Mil7aWYoYm94PT09XCJtYXJnaW5cIil7ZGVsdGErPWpRdWVyeS5jc3MoZWxlbSxib3grY3NzRXhwYW5kW2ldLHRydWUsc3R5bGVzKTt9XG5pZighaXNCb3JkZXJCb3gpe2RlbHRhKz1qUXVlcnkuY3NzKGVsZW0sXCJwYWRkaW5nXCIrY3NzRXhwYW5kW2ldLHRydWUsc3R5bGVzKTtpZihib3ghPT1cInBhZGRpbmdcIil7ZGVsdGErPWpRdWVyeS5jc3MoZWxlbSxcImJvcmRlclwiK2Nzc0V4cGFuZFtpXStcIldpZHRoXCIsdHJ1ZSxzdHlsZXMpO31lbHNle2V4dHJhKz1qUXVlcnkuY3NzKGVsZW0sXCJib3JkZXJcIitjc3NFeHBhbmRbaV0rXCJXaWR0aFwiLHRydWUsc3R5bGVzKTt9fWVsc2V7aWYoYm94PT09XCJjb250ZW50XCIpe2RlbHRhLT1qUXVlcnkuY3NzKGVsZW0sXCJwYWRkaW5nXCIrY3NzRXhwYW5kW2ldLHRydWUsc3R5bGVzKTt9XG5pZihib3ghPT1cIm1hcmdpblwiKXtkZWx0YS09alF1ZXJ5LmNzcyhlbGVtLFwiYm9yZGVyXCIrY3NzRXhwYW5kW2ldK1wiV2lkdGhcIix0cnVlLHN0eWxlcyk7fX19XG5pZighaXNCb3JkZXJCb3gmJmNvbXB1dGVkVmFsPj0wKXtkZWx0YSs9TWF0aC5tYXgoMCxNYXRoLmNlaWwoZWxlbVtcIm9mZnNldFwiK2RpbWVuc2lvblswXS50b1VwcGVyQ2FzZSgpK2RpbWVuc2lvbi5zbGljZSgxKV0tXG5jb21wdXRlZFZhbC1cbmRlbHRhLVxuZXh0cmEtXG4wLjUpKXx8MDt9XG5yZXR1cm4gZGVsdGE7fVxuZnVuY3Rpb24gZ2V0V2lkdGhPckhlaWdodChlbGVtLGRpbWVuc2lvbixleHRyYSl7dmFyIHN0eWxlcz1nZXRTdHlsZXMoZWxlbSksYm94U2l6aW5nTmVlZGVkPSFzdXBwb3J0LmJveFNpemluZ1JlbGlhYmxlKCl8fGV4dHJhLGlzQm9yZGVyQm94PWJveFNpemluZ05lZWRlZCYmalF1ZXJ5LmNzcyhlbGVtLFwiYm94U2l6aW5nXCIsZmFsc2Usc3R5bGVzKT09PVwiYm9yZGVyLWJveFwiLHZhbHVlSXNCb3JkZXJCb3g9aXNCb3JkZXJCb3gsdmFsPWN1ckNTUyhlbGVtLGRpbWVuc2lvbixzdHlsZXMpLG9mZnNldFByb3A9XCJvZmZzZXRcIitkaW1lbnNpb25bMF0udG9VcHBlckNhc2UoKStkaW1lbnNpb24uc2xpY2UoMSk7aWYocm51bW5vbnB4LnRlc3QodmFsKSl7aWYoIWV4dHJhKXtyZXR1cm4gdmFsO31cbnZhbD1cImF1dG9cIjt9XG5pZigoIXN1cHBvcnQuYm94U2l6aW5nUmVsaWFibGUoKSYmaXNCb3JkZXJCb3h8fCFzdXBwb3J0LnJlbGlhYmxlVHJEaW1lbnNpb25zKCkmJm5vZGVOYW1lKGVsZW0sXCJ0clwiKXx8dmFsPT09XCJhdXRvXCJ8fCFwYXJzZUZsb2F0KHZhbCkmJmpRdWVyeS5jc3MoZWxlbSxcImRpc3BsYXlcIixmYWxzZSxzdHlsZXMpPT09XCJpbmxpbmVcIikmJmVsZW0uZ2V0Q2xpZW50UmVjdHMoKS5sZW5ndGgpe2lzQm9yZGVyQm94PWpRdWVyeS5jc3MoZWxlbSxcImJveFNpemluZ1wiLGZhbHNlLHN0eWxlcyk9PT1cImJvcmRlci1ib3hcIjt2YWx1ZUlzQm9yZGVyQm94PW9mZnNldFByb3AgaW4gZWxlbTtpZih2YWx1ZUlzQm9yZGVyQm94KXt2YWw9ZWxlbVtvZmZzZXRQcm9wXTt9fVxudmFsPXBhcnNlRmxvYXQodmFsKXx8MDtyZXR1cm4odmFsK1xuYm94TW9kZWxBZGp1c3RtZW50KGVsZW0sZGltZW5zaW9uLGV4dHJhfHwoaXNCb3JkZXJCb3g/XCJib3JkZXJcIjpcImNvbnRlbnRcIiksdmFsdWVJc0JvcmRlckJveCxzdHlsZXMsdmFsKSkrXCJweFwiO31cbmpRdWVyeS5leHRlbmQoe2Nzc0hvb2tzOntvcGFjaXR5OntnZXQ6ZnVuY3Rpb24oZWxlbSxjb21wdXRlZCl7aWYoY29tcHV0ZWQpe3ZhciByZXQ9Y3VyQ1NTKGVsZW0sXCJvcGFjaXR5XCIpO3JldHVybiByZXQ9PT1cIlwiP1wiMVwiOnJldDt9fX19LGNzc051bWJlcjp7XCJhbmltYXRpb25JdGVyYXRpb25Db3VudFwiOnRydWUsXCJjb2x1bW5Db3VudFwiOnRydWUsXCJmaWxsT3BhY2l0eVwiOnRydWUsXCJmbGV4R3Jvd1wiOnRydWUsXCJmbGV4U2hyaW5rXCI6dHJ1ZSxcImZvbnRXZWlnaHRcIjp0cnVlLFwiZ3JpZEFyZWFcIjp0cnVlLFwiZ3JpZENvbHVtblwiOnRydWUsXCJncmlkQ29sdW1uRW5kXCI6dHJ1ZSxcImdyaWRDb2x1bW5TdGFydFwiOnRydWUsXCJncmlkUm93XCI6dHJ1ZSxcImdyaWRSb3dFbmRcIjp0cnVlLFwiZ3JpZFJvd1N0YXJ0XCI6dHJ1ZSxcImxpbmVIZWlnaHRcIjp0cnVlLFwib3BhY2l0eVwiOnRydWUsXCJvcmRlclwiOnRydWUsXCJvcnBoYW5zXCI6dHJ1ZSxcIndpZG93c1wiOnRydWUsXCJ6SW5kZXhcIjp0cnVlLFwiem9vbVwiOnRydWV9LGNzc1Byb3BzOnt9LHN0eWxlOmZ1bmN0aW9uKGVsZW0sbmFtZSx2YWx1ZSxleHRyYSl7aWYoIWVsZW18fGVsZW0ubm9kZVR5cGU9PT0zfHxlbGVtLm5vZGVUeXBlPT09OHx8IWVsZW0uc3R5bGUpe3JldHVybjt9XG52YXIgcmV0LHR5cGUsaG9va3Msb3JpZ05hbWU9Y2FtZWxDYXNlKG5hbWUpLGlzQ3VzdG9tUHJvcD1yY3VzdG9tUHJvcC50ZXN0KG5hbWUpLHN0eWxlPWVsZW0uc3R5bGU7aWYoIWlzQ3VzdG9tUHJvcCl7bmFtZT1maW5hbFByb3BOYW1lKG9yaWdOYW1lKTt9XG5ob29rcz1qUXVlcnkuY3NzSG9va3NbbmFtZV18fGpRdWVyeS5jc3NIb29rc1tvcmlnTmFtZV07aWYodmFsdWUhPT11bmRlZmluZWQpe3R5cGU9dHlwZW9mIHZhbHVlO2lmKHR5cGU9PT1cInN0cmluZ1wiJiYocmV0PXJjc3NOdW0uZXhlYyh2YWx1ZSkpJiZyZXRbMV0pe3ZhbHVlPWFkanVzdENTUyhlbGVtLG5hbWUscmV0KTt0eXBlPVwibnVtYmVyXCI7fVxuaWYodmFsdWU9PW51bGx8fHZhbHVlIT09dmFsdWUpe3JldHVybjt9XG5pZih0eXBlPT09XCJudW1iZXJcIiYmIWlzQ3VzdG9tUHJvcCl7dmFsdWUrPXJldCYmcmV0WzNdfHwoalF1ZXJ5LmNzc051bWJlcltvcmlnTmFtZV0/XCJcIjpcInB4XCIpO31cbmlmKCFzdXBwb3J0LmNsZWFyQ2xvbmVTdHlsZSYmdmFsdWU9PT1cIlwiJiZuYW1lLmluZGV4T2YoXCJiYWNrZ3JvdW5kXCIpPT09MCl7c3R5bGVbbmFtZV09XCJpbmhlcml0XCI7fVxuaWYoIWhvb2tzfHwhKFwic2V0XCJpbiBob29rcyl8fCh2YWx1ZT1ob29rcy5zZXQoZWxlbSx2YWx1ZSxleHRyYSkpIT09dW5kZWZpbmVkKXtpZihpc0N1c3RvbVByb3Ape3N0eWxlLnNldFByb3BlcnR5KG5hbWUsdmFsdWUpO31lbHNle3N0eWxlW25hbWVdPXZhbHVlO319fWVsc2V7aWYoaG9va3MmJlwiZ2V0XCJpbiBob29rcyYmKHJldD1ob29rcy5nZXQoZWxlbSxmYWxzZSxleHRyYSkpIT09dW5kZWZpbmVkKXtyZXR1cm4gcmV0O31cbnJldHVybiBzdHlsZVtuYW1lXTt9fSxjc3M6ZnVuY3Rpb24oZWxlbSxuYW1lLGV4dHJhLHN0eWxlcyl7dmFyIHZhbCxudW0saG9va3Msb3JpZ05hbWU9Y2FtZWxDYXNlKG5hbWUpLGlzQ3VzdG9tUHJvcD1yY3VzdG9tUHJvcC50ZXN0KG5hbWUpO2lmKCFpc0N1c3RvbVByb3Ape25hbWU9ZmluYWxQcm9wTmFtZShvcmlnTmFtZSk7fVxuaG9va3M9alF1ZXJ5LmNzc0hvb2tzW25hbWVdfHxqUXVlcnkuY3NzSG9va3Nbb3JpZ05hbWVdO2lmKGhvb2tzJiZcImdldFwiaW4gaG9va3Mpe3ZhbD1ob29rcy5nZXQoZWxlbSx0cnVlLGV4dHJhKTt9XG5pZih2YWw9PT11bmRlZmluZWQpe3ZhbD1jdXJDU1MoZWxlbSxuYW1lLHN0eWxlcyk7fVxuaWYodmFsPT09XCJub3JtYWxcIiYmbmFtZSBpbiBjc3NOb3JtYWxUcmFuc2Zvcm0pe3ZhbD1jc3NOb3JtYWxUcmFuc2Zvcm1bbmFtZV07fVxuaWYoZXh0cmE9PT1cIlwifHxleHRyYSl7bnVtPXBhcnNlRmxvYXQodmFsKTtyZXR1cm4gZXh0cmE9PT10cnVlfHxpc0Zpbml0ZShudW0pP251bXx8MDp2YWw7fVxucmV0dXJuIHZhbDt9fSk7alF1ZXJ5LmVhY2goW1wiaGVpZ2h0XCIsXCJ3aWR0aFwiXSxmdW5jdGlvbihfaSxkaW1lbnNpb24pe2pRdWVyeS5jc3NIb29rc1tkaW1lbnNpb25dPXtnZXQ6ZnVuY3Rpb24oZWxlbSxjb21wdXRlZCxleHRyYSl7aWYoY29tcHV0ZWQpe3JldHVybiByZGlzcGxheXN3YXAudGVzdChqUXVlcnkuY3NzKGVsZW0sXCJkaXNwbGF5XCIpKSYmKCFlbGVtLmdldENsaWVudFJlY3RzKCkubGVuZ3RofHwhZWxlbS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS53aWR0aCk/c3dhcChlbGVtLGNzc1Nob3csZnVuY3Rpb24oKXtyZXR1cm4gZ2V0V2lkdGhPckhlaWdodChlbGVtLGRpbWVuc2lvbixleHRyYSk7fSk6Z2V0V2lkdGhPckhlaWdodChlbGVtLGRpbWVuc2lvbixleHRyYSk7fX0sc2V0OmZ1bmN0aW9uKGVsZW0sdmFsdWUsZXh0cmEpe3ZhciBtYXRjaGVzLHN0eWxlcz1nZXRTdHlsZXMoZWxlbSksc2Nyb2xsYm94U2l6ZUJ1Z2d5PSFzdXBwb3J0LnNjcm9sbGJveFNpemUoKSYmc3R5bGVzLnBvc2l0aW9uPT09XCJhYnNvbHV0ZVwiLGJveFNpemluZ05lZWRlZD1zY3JvbGxib3hTaXplQnVnZ3l8fGV4dHJhLGlzQm9yZGVyQm94PWJveFNpemluZ05lZWRlZCYmalF1ZXJ5LmNzcyhlbGVtLFwiYm94U2l6aW5nXCIsZmFsc2Usc3R5bGVzKT09PVwiYm9yZGVyLWJveFwiLHN1YnRyYWN0PWV4dHJhP2JveE1vZGVsQWRqdXN0bWVudChlbGVtLGRpbWVuc2lvbixleHRyYSxpc0JvcmRlckJveCxzdHlsZXMpOjA7aWYoaXNCb3JkZXJCb3gmJnNjcm9sbGJveFNpemVCdWdneSl7c3VidHJhY3QtPU1hdGguY2VpbChlbGVtW1wib2Zmc2V0XCIrZGltZW5zaW9uWzBdLnRvVXBwZXJDYXNlKCkrZGltZW5zaW9uLnNsaWNlKDEpXS1cbnBhcnNlRmxvYXQoc3R5bGVzW2RpbWVuc2lvbl0pLVxuYm94TW9kZWxBZGp1c3RtZW50KGVsZW0sZGltZW5zaW9uLFwiYm9yZGVyXCIsZmFsc2Usc3R5bGVzKS1cbjAuNSk7fVxuaWYoc3VidHJhY3QmJihtYXRjaGVzPXJjc3NOdW0uZXhlYyh2YWx1ZSkpJiYobWF0Y2hlc1szXXx8XCJweFwiKSE9PVwicHhcIil7ZWxlbS5zdHlsZVtkaW1lbnNpb25dPXZhbHVlO3ZhbHVlPWpRdWVyeS5jc3MoZWxlbSxkaW1lbnNpb24pO31cbnJldHVybiBzZXRQb3NpdGl2ZU51bWJlcihlbGVtLHZhbHVlLHN1YnRyYWN0KTt9fTt9KTtqUXVlcnkuY3NzSG9va3MubWFyZ2luTGVmdD1hZGRHZXRIb29rSWYoc3VwcG9ydC5yZWxpYWJsZU1hcmdpbkxlZnQsZnVuY3Rpb24oZWxlbSxjb21wdXRlZCl7aWYoY29tcHV0ZWQpe3JldHVybihwYXJzZUZsb2F0KGN1ckNTUyhlbGVtLFwibWFyZ2luTGVmdFwiKSl8fGVsZW0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdC1cbnN3YXAoZWxlbSx7bWFyZ2luTGVmdDowfSxmdW5jdGlvbigpe3JldHVybiBlbGVtLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmxlZnQ7fSkpK1wicHhcIjt9fSk7alF1ZXJ5LmVhY2goe21hcmdpbjpcIlwiLHBhZGRpbmc6XCJcIixib3JkZXI6XCJXaWR0aFwifSxmdW5jdGlvbihwcmVmaXgsc3VmZml4KXtqUXVlcnkuY3NzSG9va3NbcHJlZml4K3N1ZmZpeF09e2V4cGFuZDpmdW5jdGlvbih2YWx1ZSl7dmFyIGk9MCxleHBhbmRlZD17fSxwYXJ0cz10eXBlb2YgdmFsdWU9PT1cInN0cmluZ1wiP3ZhbHVlLnNwbGl0KFwiIFwiKTpbdmFsdWVdO2Zvcig7aTw0O2krKyl7ZXhwYW5kZWRbcHJlZml4K2Nzc0V4cGFuZFtpXStzdWZmaXhdPXBhcnRzW2ldfHxwYXJ0c1tpLTJdfHxwYXJ0c1swXTt9XG5yZXR1cm4gZXhwYW5kZWQ7fX07aWYocHJlZml4IT09XCJtYXJnaW5cIil7alF1ZXJ5LmNzc0hvb2tzW3ByZWZpeCtzdWZmaXhdLnNldD1zZXRQb3NpdGl2ZU51bWJlcjt9fSk7alF1ZXJ5LmZuLmV4dGVuZCh7Y3NzOmZ1bmN0aW9uKG5hbWUsdmFsdWUpe3JldHVybiBhY2Nlc3ModGhpcyxmdW5jdGlvbihlbGVtLG5hbWUsdmFsdWUpe3ZhciBzdHlsZXMsbGVuLG1hcD17fSxpPTA7aWYoQXJyYXkuaXNBcnJheShuYW1lKSl7c3R5bGVzPWdldFN0eWxlcyhlbGVtKTtsZW49bmFtZS5sZW5ndGg7Zm9yKDtpPGxlbjtpKyspe21hcFtuYW1lW2ldXT1qUXVlcnkuY3NzKGVsZW0sbmFtZVtpXSxmYWxzZSxzdHlsZXMpO31cbnJldHVybiBtYXA7fVxucmV0dXJuIHZhbHVlIT09dW5kZWZpbmVkP2pRdWVyeS5zdHlsZShlbGVtLG5hbWUsdmFsdWUpOmpRdWVyeS5jc3MoZWxlbSxuYW1lKTt9LG5hbWUsdmFsdWUsYXJndW1lbnRzLmxlbmd0aD4xKTt9fSk7ZnVuY3Rpb24gVHdlZW4oZWxlbSxvcHRpb25zLHByb3AsZW5kLGVhc2luZyl7cmV0dXJuIG5ldyBUd2Vlbi5wcm90b3R5cGUuaW5pdChlbGVtLG9wdGlvbnMscHJvcCxlbmQsZWFzaW5nKTt9XG5qUXVlcnkuVHdlZW49VHdlZW47VHdlZW4ucHJvdG90eXBlPXtjb25zdHJ1Y3RvcjpUd2Vlbixpbml0OmZ1bmN0aW9uKGVsZW0sb3B0aW9ucyxwcm9wLGVuZCxlYXNpbmcsdW5pdCl7dGhpcy5lbGVtPWVsZW07dGhpcy5wcm9wPXByb3A7dGhpcy5lYXNpbmc9ZWFzaW5nfHxqUXVlcnkuZWFzaW5nLl9kZWZhdWx0O3RoaXMub3B0aW9ucz1vcHRpb25zO3RoaXMuc3RhcnQ9dGhpcy5ub3c9dGhpcy5jdXIoKTt0aGlzLmVuZD1lbmQ7dGhpcy51bml0PXVuaXR8fChqUXVlcnkuY3NzTnVtYmVyW3Byb3BdP1wiXCI6XCJweFwiKTt9LGN1cjpmdW5jdGlvbigpe3ZhciBob29rcz1Ud2Vlbi5wcm9wSG9va3NbdGhpcy5wcm9wXTtyZXR1cm4gaG9va3MmJmhvb2tzLmdldD9ob29rcy5nZXQodGhpcyk6VHdlZW4ucHJvcEhvb2tzLl9kZWZhdWx0LmdldCh0aGlzKTt9LHJ1bjpmdW5jdGlvbihwZXJjZW50KXt2YXIgZWFzZWQsaG9va3M9VHdlZW4ucHJvcEhvb2tzW3RoaXMucHJvcF07aWYodGhpcy5vcHRpb25zLmR1cmF0aW9uKXt0aGlzLnBvcz1lYXNlZD1qUXVlcnkuZWFzaW5nW3RoaXMuZWFzaW5nXShwZXJjZW50LHRoaXMub3B0aW9ucy5kdXJhdGlvbipwZXJjZW50LDAsMSx0aGlzLm9wdGlvbnMuZHVyYXRpb24pO31lbHNle3RoaXMucG9zPWVhc2VkPXBlcmNlbnQ7fVxudGhpcy5ub3c9KHRoaXMuZW5kLXRoaXMuc3RhcnQpKmVhc2VkK3RoaXMuc3RhcnQ7aWYodGhpcy5vcHRpb25zLnN0ZXApe3RoaXMub3B0aW9ucy5zdGVwLmNhbGwodGhpcy5lbGVtLHRoaXMubm93LHRoaXMpO31cbmlmKGhvb2tzJiZob29rcy5zZXQpe2hvb2tzLnNldCh0aGlzKTt9ZWxzZXtUd2Vlbi5wcm9wSG9va3MuX2RlZmF1bHQuc2V0KHRoaXMpO31cbnJldHVybiB0aGlzO319O1R3ZWVuLnByb3RvdHlwZS5pbml0LnByb3RvdHlwZT1Ud2Vlbi5wcm90b3R5cGU7VHdlZW4ucHJvcEhvb2tzPXtfZGVmYXVsdDp7Z2V0OmZ1bmN0aW9uKHR3ZWVuKXt2YXIgcmVzdWx0O2lmKHR3ZWVuLmVsZW0ubm9kZVR5cGUhPT0xfHx0d2Vlbi5lbGVtW3R3ZWVuLnByb3BdIT1udWxsJiZ0d2Vlbi5lbGVtLnN0eWxlW3R3ZWVuLnByb3BdPT1udWxsKXtyZXR1cm4gdHdlZW4uZWxlbVt0d2Vlbi5wcm9wXTt9XG5yZXN1bHQ9alF1ZXJ5LmNzcyh0d2Vlbi5lbGVtLHR3ZWVuLnByb3AsXCJcIik7cmV0dXJuIXJlc3VsdHx8cmVzdWx0PT09XCJhdXRvXCI/MDpyZXN1bHQ7fSxzZXQ6ZnVuY3Rpb24odHdlZW4pe2lmKGpRdWVyeS5meC5zdGVwW3R3ZWVuLnByb3BdKXtqUXVlcnkuZnguc3RlcFt0d2Vlbi5wcm9wXSh0d2Vlbik7fWVsc2UgaWYodHdlZW4uZWxlbS5ub2RlVHlwZT09PTEmJihqUXVlcnkuY3NzSG9va3NbdHdlZW4ucHJvcF18fHR3ZWVuLmVsZW0uc3R5bGVbZmluYWxQcm9wTmFtZSh0d2Vlbi5wcm9wKV0hPW51bGwpKXtqUXVlcnkuc3R5bGUodHdlZW4uZWxlbSx0d2Vlbi5wcm9wLHR3ZWVuLm5vdyt0d2Vlbi51bml0KTt9ZWxzZXt0d2Vlbi5lbGVtW3R3ZWVuLnByb3BdPXR3ZWVuLm5vdzt9fX19O1R3ZWVuLnByb3BIb29rcy5zY3JvbGxUb3A9VHdlZW4ucHJvcEhvb2tzLnNjcm9sbExlZnQ9e3NldDpmdW5jdGlvbih0d2Vlbil7aWYodHdlZW4uZWxlbS5ub2RlVHlwZSYmdHdlZW4uZWxlbS5wYXJlbnROb2RlKXt0d2Vlbi5lbGVtW3R3ZWVuLnByb3BdPXR3ZWVuLm5vdzt9fX07alF1ZXJ5LmVhc2luZz17bGluZWFyOmZ1bmN0aW9uKHApe3JldHVybiBwO30sc3dpbmc6ZnVuY3Rpb24ocCl7cmV0dXJuIDAuNS1NYXRoLmNvcyhwKk1hdGguUEkpLzI7fSxfZGVmYXVsdDpcInN3aW5nXCJ9O2pRdWVyeS5meD1Ud2Vlbi5wcm90b3R5cGUuaW5pdDtqUXVlcnkuZnguc3RlcD17fTt2YXJcbmZ4Tm93LGluUHJvZ3Jlc3MscmZ4dHlwZXM9L14oPzp0b2dnbGV8c2hvd3xoaWRlKSQvLHJydW49L3F1ZXVlSG9va3MkLztmdW5jdGlvbiBzY2hlZHVsZSgpe2lmKGluUHJvZ3Jlc3Mpe2lmKGRvY3VtZW50LmhpZGRlbj09PWZhbHNlJiZ3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKXt3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKHNjaGVkdWxlKTt9ZWxzZXt3aW5kb3cuc2V0VGltZW91dChzY2hlZHVsZSxqUXVlcnkuZnguaW50ZXJ2YWwpO31cbmpRdWVyeS5meC50aWNrKCk7fX1cbmZ1bmN0aW9uIGNyZWF0ZUZ4Tm93KCl7d2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24oKXtmeE5vdz11bmRlZmluZWQ7fSk7cmV0dXJuKGZ4Tm93PURhdGUubm93KCkpO31cbmZ1bmN0aW9uIGdlbkZ4KHR5cGUsaW5jbHVkZVdpZHRoKXt2YXIgd2hpY2gsaT0wLGF0dHJzPXtoZWlnaHQ6dHlwZX07aW5jbHVkZVdpZHRoPWluY2x1ZGVXaWR0aD8xOjA7Zm9yKDtpPDQ7aSs9Mi1pbmNsdWRlV2lkdGgpe3doaWNoPWNzc0V4cGFuZFtpXTthdHRyc1tcIm1hcmdpblwiK3doaWNoXT1hdHRyc1tcInBhZGRpbmdcIit3aGljaF09dHlwZTt9XG5pZihpbmNsdWRlV2lkdGgpe2F0dHJzLm9wYWNpdHk9YXR0cnMud2lkdGg9dHlwZTt9XG5yZXR1cm4gYXR0cnM7fVxuZnVuY3Rpb24gY3JlYXRlVHdlZW4odmFsdWUscHJvcCxhbmltYXRpb24pe3ZhciB0d2Vlbixjb2xsZWN0aW9uPShBbmltYXRpb24udHdlZW5lcnNbcHJvcF18fFtdKS5jb25jYXQoQW5pbWF0aW9uLnR3ZWVuZXJzW1wiKlwiXSksaW5kZXg9MCxsZW5ndGg9Y29sbGVjdGlvbi5sZW5ndGg7Zm9yKDtpbmRleDxsZW5ndGg7aW5kZXgrKyl7aWYoKHR3ZWVuPWNvbGxlY3Rpb25baW5kZXhdLmNhbGwoYW5pbWF0aW9uLHByb3AsdmFsdWUpKSl7cmV0dXJuIHR3ZWVuO319fVxuZnVuY3Rpb24gZGVmYXVsdFByZWZpbHRlcihlbGVtLHByb3BzLG9wdHMpe3ZhciBwcm9wLHZhbHVlLHRvZ2dsZSxob29rcyxvbGRmaXJlLHByb3BUd2VlbixyZXN0b3JlRGlzcGxheSxkaXNwbGF5LGlzQm94PVwid2lkdGhcImluIHByb3BzfHxcImhlaWdodFwiaW4gcHJvcHMsYW5pbT10aGlzLG9yaWc9e30sc3R5bGU9ZWxlbS5zdHlsZSxoaWRkZW49ZWxlbS5ub2RlVHlwZSYmaXNIaWRkZW5XaXRoaW5UcmVlKGVsZW0pLGRhdGFTaG93PWRhdGFQcml2LmdldChlbGVtLFwiZnhzaG93XCIpO2lmKCFvcHRzLnF1ZXVlKXtob29rcz1qUXVlcnkuX3F1ZXVlSG9va3MoZWxlbSxcImZ4XCIpO2lmKGhvb2tzLnVucXVldWVkPT1udWxsKXtob29rcy51bnF1ZXVlZD0wO29sZGZpcmU9aG9va3MuZW1wdHkuZmlyZTtob29rcy5lbXB0eS5maXJlPWZ1bmN0aW9uKCl7aWYoIWhvb2tzLnVucXVldWVkKXtvbGRmaXJlKCk7fX07fVxuaG9va3MudW5xdWV1ZWQrKzthbmltLmFsd2F5cyhmdW5jdGlvbigpe2FuaW0uYWx3YXlzKGZ1bmN0aW9uKCl7aG9va3MudW5xdWV1ZWQtLTtpZighalF1ZXJ5LnF1ZXVlKGVsZW0sXCJmeFwiKS5sZW5ndGgpe2hvb2tzLmVtcHR5LmZpcmUoKTt9fSk7fSk7fVxuZm9yKHByb3AgaW4gcHJvcHMpe3ZhbHVlPXByb3BzW3Byb3BdO2lmKHJmeHR5cGVzLnRlc3QodmFsdWUpKXtkZWxldGUgcHJvcHNbcHJvcF07dG9nZ2xlPXRvZ2dsZXx8dmFsdWU9PT1cInRvZ2dsZVwiO2lmKHZhbHVlPT09KGhpZGRlbj9cImhpZGVcIjpcInNob3dcIikpe2lmKHZhbHVlPT09XCJzaG93XCImJmRhdGFTaG93JiZkYXRhU2hvd1twcm9wXSE9PXVuZGVmaW5lZCl7aGlkZGVuPXRydWU7fWVsc2V7Y29udGludWU7fX1cbm9yaWdbcHJvcF09ZGF0YVNob3cmJmRhdGFTaG93W3Byb3BdfHxqUXVlcnkuc3R5bGUoZWxlbSxwcm9wKTt9fVxucHJvcFR3ZWVuPSFqUXVlcnkuaXNFbXB0eU9iamVjdChwcm9wcyk7aWYoIXByb3BUd2VlbiYmalF1ZXJ5LmlzRW1wdHlPYmplY3Qob3JpZykpe3JldHVybjt9XG5pZihpc0JveCYmZWxlbS5ub2RlVHlwZT09PTEpe29wdHMub3ZlcmZsb3c9W3N0eWxlLm92ZXJmbG93LHN0eWxlLm92ZXJmbG93WCxzdHlsZS5vdmVyZmxvd1ldO3Jlc3RvcmVEaXNwbGF5PWRhdGFTaG93JiZkYXRhU2hvdy5kaXNwbGF5O2lmKHJlc3RvcmVEaXNwbGF5PT1udWxsKXtyZXN0b3JlRGlzcGxheT1kYXRhUHJpdi5nZXQoZWxlbSxcImRpc3BsYXlcIik7fVxuZGlzcGxheT1qUXVlcnkuY3NzKGVsZW0sXCJkaXNwbGF5XCIpO2lmKGRpc3BsYXk9PT1cIm5vbmVcIil7aWYocmVzdG9yZURpc3BsYXkpe2Rpc3BsYXk9cmVzdG9yZURpc3BsYXk7fWVsc2V7c2hvd0hpZGUoW2VsZW1dLHRydWUpO3Jlc3RvcmVEaXNwbGF5PWVsZW0uc3R5bGUuZGlzcGxheXx8cmVzdG9yZURpc3BsYXk7ZGlzcGxheT1qUXVlcnkuY3NzKGVsZW0sXCJkaXNwbGF5XCIpO3Nob3dIaWRlKFtlbGVtXSk7fX1cbmlmKGRpc3BsYXk9PT1cImlubGluZVwifHxkaXNwbGF5PT09XCJpbmxpbmUtYmxvY2tcIiYmcmVzdG9yZURpc3BsYXkhPW51bGwpe2lmKGpRdWVyeS5jc3MoZWxlbSxcImZsb2F0XCIpPT09XCJub25lXCIpe2lmKCFwcm9wVHdlZW4pe2FuaW0uZG9uZShmdW5jdGlvbigpe3N0eWxlLmRpc3BsYXk9cmVzdG9yZURpc3BsYXk7fSk7aWYocmVzdG9yZURpc3BsYXk9PW51bGwpe2Rpc3BsYXk9c3R5bGUuZGlzcGxheTtyZXN0b3JlRGlzcGxheT1kaXNwbGF5PT09XCJub25lXCI/XCJcIjpkaXNwbGF5O319XG5zdHlsZS5kaXNwbGF5PVwiaW5saW5lLWJsb2NrXCI7fX19XG5pZihvcHRzLm92ZXJmbG93KXtzdHlsZS5vdmVyZmxvdz1cImhpZGRlblwiO2FuaW0uYWx3YXlzKGZ1bmN0aW9uKCl7c3R5bGUub3ZlcmZsb3c9b3B0cy5vdmVyZmxvd1swXTtzdHlsZS5vdmVyZmxvd1g9b3B0cy5vdmVyZmxvd1sxXTtzdHlsZS5vdmVyZmxvd1k9b3B0cy5vdmVyZmxvd1syXTt9KTt9XG5wcm9wVHdlZW49ZmFsc2U7Zm9yKHByb3AgaW4gb3JpZyl7aWYoIXByb3BUd2Vlbil7aWYoZGF0YVNob3cpe2lmKFwiaGlkZGVuXCJpbiBkYXRhU2hvdyl7aGlkZGVuPWRhdGFTaG93LmhpZGRlbjt9fWVsc2V7ZGF0YVNob3c9ZGF0YVByaXYuYWNjZXNzKGVsZW0sXCJmeHNob3dcIix7ZGlzcGxheTpyZXN0b3JlRGlzcGxheX0pO31cbmlmKHRvZ2dsZSl7ZGF0YVNob3cuaGlkZGVuPSFoaWRkZW47fVxuaWYoaGlkZGVuKXtzaG93SGlkZShbZWxlbV0sdHJ1ZSk7fVxuYW5pbS5kb25lKGZ1bmN0aW9uKCl7aWYoIWhpZGRlbil7c2hvd0hpZGUoW2VsZW1dKTt9XG5kYXRhUHJpdi5yZW1vdmUoZWxlbSxcImZ4c2hvd1wiKTtmb3IocHJvcCBpbiBvcmlnKXtqUXVlcnkuc3R5bGUoZWxlbSxwcm9wLG9yaWdbcHJvcF0pO319KTt9XG5wcm9wVHdlZW49Y3JlYXRlVHdlZW4oaGlkZGVuP2RhdGFTaG93W3Byb3BdOjAscHJvcCxhbmltKTtpZighKHByb3AgaW4gZGF0YVNob3cpKXtkYXRhU2hvd1twcm9wXT1wcm9wVHdlZW4uc3RhcnQ7aWYoaGlkZGVuKXtwcm9wVHdlZW4uZW5kPXByb3BUd2Vlbi5zdGFydDtwcm9wVHdlZW4uc3RhcnQ9MDt9fX19XG5mdW5jdGlvbiBwcm9wRmlsdGVyKHByb3BzLHNwZWNpYWxFYXNpbmcpe3ZhciBpbmRleCxuYW1lLGVhc2luZyx2YWx1ZSxob29rcztmb3IoaW5kZXggaW4gcHJvcHMpe25hbWU9Y2FtZWxDYXNlKGluZGV4KTtlYXNpbmc9c3BlY2lhbEVhc2luZ1tuYW1lXTt2YWx1ZT1wcm9wc1tpbmRleF07aWYoQXJyYXkuaXNBcnJheSh2YWx1ZSkpe2Vhc2luZz12YWx1ZVsxXTt2YWx1ZT1wcm9wc1tpbmRleF09dmFsdWVbMF07fVxuaWYoaW5kZXghPT1uYW1lKXtwcm9wc1tuYW1lXT12YWx1ZTtkZWxldGUgcHJvcHNbaW5kZXhdO31cbmhvb2tzPWpRdWVyeS5jc3NIb29rc1tuYW1lXTtpZihob29rcyYmXCJleHBhbmRcImluIGhvb2tzKXt2YWx1ZT1ob29rcy5leHBhbmQodmFsdWUpO2RlbGV0ZSBwcm9wc1tuYW1lXTtmb3IoaW5kZXggaW4gdmFsdWUpe2lmKCEoaW5kZXggaW4gcHJvcHMpKXtwcm9wc1tpbmRleF09dmFsdWVbaW5kZXhdO3NwZWNpYWxFYXNpbmdbaW5kZXhdPWVhc2luZzt9fX1lbHNle3NwZWNpYWxFYXNpbmdbbmFtZV09ZWFzaW5nO319fVxuZnVuY3Rpb24gQW5pbWF0aW9uKGVsZW0scHJvcGVydGllcyxvcHRpb25zKXt2YXIgcmVzdWx0LHN0b3BwZWQsaW5kZXg9MCxsZW5ndGg9QW5pbWF0aW9uLnByZWZpbHRlcnMubGVuZ3RoLGRlZmVycmVkPWpRdWVyeS5EZWZlcnJlZCgpLmFsd2F5cyhmdW5jdGlvbigpe2RlbGV0ZSB0aWNrLmVsZW07fSksdGljaz1mdW5jdGlvbigpe2lmKHN0b3BwZWQpe3JldHVybiBmYWxzZTt9XG52YXIgY3VycmVudFRpbWU9ZnhOb3d8fGNyZWF0ZUZ4Tm93KCkscmVtYWluaW5nPU1hdGgubWF4KDAsYW5pbWF0aW9uLnN0YXJ0VGltZSthbmltYXRpb24uZHVyYXRpb24tY3VycmVudFRpbWUpLHRlbXA9cmVtYWluaW5nL2FuaW1hdGlvbi5kdXJhdGlvbnx8MCxwZXJjZW50PTEtdGVtcCxpbmRleD0wLGxlbmd0aD1hbmltYXRpb24udHdlZW5zLmxlbmd0aDtmb3IoO2luZGV4PGxlbmd0aDtpbmRleCsrKXthbmltYXRpb24udHdlZW5zW2luZGV4XS5ydW4ocGVyY2VudCk7fVxuZGVmZXJyZWQubm90aWZ5V2l0aChlbGVtLFthbmltYXRpb24scGVyY2VudCxyZW1haW5pbmddKTtpZihwZXJjZW50PDEmJmxlbmd0aCl7cmV0dXJuIHJlbWFpbmluZzt9XG5pZighbGVuZ3RoKXtkZWZlcnJlZC5ub3RpZnlXaXRoKGVsZW0sW2FuaW1hdGlvbiwxLDBdKTt9XG5kZWZlcnJlZC5yZXNvbHZlV2l0aChlbGVtLFthbmltYXRpb25dKTtyZXR1cm4gZmFsc2U7fSxhbmltYXRpb249ZGVmZXJyZWQucHJvbWlzZSh7ZWxlbTplbGVtLHByb3BzOmpRdWVyeS5leHRlbmQoe30scHJvcGVydGllcyksb3B0czpqUXVlcnkuZXh0ZW5kKHRydWUse3NwZWNpYWxFYXNpbmc6e30sZWFzaW5nOmpRdWVyeS5lYXNpbmcuX2RlZmF1bHR9LG9wdGlvbnMpLG9yaWdpbmFsUHJvcGVydGllczpwcm9wZXJ0aWVzLG9yaWdpbmFsT3B0aW9uczpvcHRpb25zLHN0YXJ0VGltZTpmeE5vd3x8Y3JlYXRlRnhOb3coKSxkdXJhdGlvbjpvcHRpb25zLmR1cmF0aW9uLHR3ZWVuczpbXSxjcmVhdGVUd2VlbjpmdW5jdGlvbihwcm9wLGVuZCl7dmFyIHR3ZWVuPWpRdWVyeS5Ud2VlbihlbGVtLGFuaW1hdGlvbi5vcHRzLHByb3AsZW5kLGFuaW1hdGlvbi5vcHRzLnNwZWNpYWxFYXNpbmdbcHJvcF18fGFuaW1hdGlvbi5vcHRzLmVhc2luZyk7YW5pbWF0aW9uLnR3ZWVucy5wdXNoKHR3ZWVuKTtyZXR1cm4gdHdlZW47fSxzdG9wOmZ1bmN0aW9uKGdvdG9FbmQpe3ZhciBpbmRleD0wLGxlbmd0aD1nb3RvRW5kP2FuaW1hdGlvbi50d2VlbnMubGVuZ3RoOjA7aWYoc3RvcHBlZCl7cmV0dXJuIHRoaXM7fVxuc3RvcHBlZD10cnVlO2Zvcig7aW5kZXg8bGVuZ3RoO2luZGV4Kyspe2FuaW1hdGlvbi50d2VlbnNbaW5kZXhdLnJ1bigxKTt9XG5pZihnb3RvRW5kKXtkZWZlcnJlZC5ub3RpZnlXaXRoKGVsZW0sW2FuaW1hdGlvbiwxLDBdKTtkZWZlcnJlZC5yZXNvbHZlV2l0aChlbGVtLFthbmltYXRpb24sZ290b0VuZF0pO31lbHNle2RlZmVycmVkLnJlamVjdFdpdGgoZWxlbSxbYW5pbWF0aW9uLGdvdG9FbmRdKTt9XG5yZXR1cm4gdGhpczt9fSkscHJvcHM9YW5pbWF0aW9uLnByb3BzO3Byb3BGaWx0ZXIocHJvcHMsYW5pbWF0aW9uLm9wdHMuc3BlY2lhbEVhc2luZyk7Zm9yKDtpbmRleDxsZW5ndGg7aW5kZXgrKyl7cmVzdWx0PUFuaW1hdGlvbi5wcmVmaWx0ZXJzW2luZGV4XS5jYWxsKGFuaW1hdGlvbixlbGVtLHByb3BzLGFuaW1hdGlvbi5vcHRzKTtpZihyZXN1bHQpe2lmKGlzRnVuY3Rpb24ocmVzdWx0LnN0b3ApKXtqUXVlcnkuX3F1ZXVlSG9va3MoYW5pbWF0aW9uLmVsZW0sYW5pbWF0aW9uLm9wdHMucXVldWUpLnN0b3A9cmVzdWx0LnN0b3AuYmluZChyZXN1bHQpO31cbnJldHVybiByZXN1bHQ7fX1cbmpRdWVyeS5tYXAocHJvcHMsY3JlYXRlVHdlZW4sYW5pbWF0aW9uKTtpZihpc0Z1bmN0aW9uKGFuaW1hdGlvbi5vcHRzLnN0YXJ0KSl7YW5pbWF0aW9uLm9wdHMuc3RhcnQuY2FsbChlbGVtLGFuaW1hdGlvbik7fVxuYW5pbWF0aW9uLnByb2dyZXNzKGFuaW1hdGlvbi5vcHRzLnByb2dyZXNzKS5kb25lKGFuaW1hdGlvbi5vcHRzLmRvbmUsYW5pbWF0aW9uLm9wdHMuY29tcGxldGUpLmZhaWwoYW5pbWF0aW9uLm9wdHMuZmFpbCkuYWx3YXlzKGFuaW1hdGlvbi5vcHRzLmFsd2F5cyk7alF1ZXJ5LmZ4LnRpbWVyKGpRdWVyeS5leHRlbmQodGljayx7ZWxlbTplbGVtLGFuaW06YW5pbWF0aW9uLHF1ZXVlOmFuaW1hdGlvbi5vcHRzLnF1ZXVlfSkpO3JldHVybiBhbmltYXRpb247fVxualF1ZXJ5LkFuaW1hdGlvbj1qUXVlcnkuZXh0ZW5kKEFuaW1hdGlvbix7dHdlZW5lcnM6e1wiKlwiOltmdW5jdGlvbihwcm9wLHZhbHVlKXt2YXIgdHdlZW49dGhpcy5jcmVhdGVUd2Vlbihwcm9wLHZhbHVlKTthZGp1c3RDU1ModHdlZW4uZWxlbSxwcm9wLHJjc3NOdW0uZXhlYyh2YWx1ZSksdHdlZW4pO3JldHVybiB0d2Vlbjt9XX0sdHdlZW5lcjpmdW5jdGlvbihwcm9wcyxjYWxsYmFjayl7aWYoaXNGdW5jdGlvbihwcm9wcykpe2NhbGxiYWNrPXByb3BzO3Byb3BzPVtcIipcIl07fWVsc2V7cHJvcHM9cHJvcHMubWF0Y2gocm5vdGh0bWx3aGl0ZSk7fVxudmFyIHByb3AsaW5kZXg9MCxsZW5ndGg9cHJvcHMubGVuZ3RoO2Zvcig7aW5kZXg8bGVuZ3RoO2luZGV4Kyspe3Byb3A9cHJvcHNbaW5kZXhdO0FuaW1hdGlvbi50d2VlbmVyc1twcm9wXT1BbmltYXRpb24udHdlZW5lcnNbcHJvcF18fFtdO0FuaW1hdGlvbi50d2VlbmVyc1twcm9wXS51bnNoaWZ0KGNhbGxiYWNrKTt9fSxwcmVmaWx0ZXJzOltkZWZhdWx0UHJlZmlsdGVyXSxwcmVmaWx0ZXI6ZnVuY3Rpb24oY2FsbGJhY2sscHJlcGVuZCl7aWYocHJlcGVuZCl7QW5pbWF0aW9uLnByZWZpbHRlcnMudW5zaGlmdChjYWxsYmFjayk7fWVsc2V7QW5pbWF0aW9uLnByZWZpbHRlcnMucHVzaChjYWxsYmFjayk7fX19KTtqUXVlcnkuc3BlZWQ9ZnVuY3Rpb24oc3BlZWQsZWFzaW5nLGZuKXt2YXIgb3B0PXNwZWVkJiZ0eXBlb2Ygc3BlZWQ9PT1cIm9iamVjdFwiP2pRdWVyeS5leHRlbmQoe30sc3BlZWQpOntjb21wbGV0ZTpmbnx8IWZuJiZlYXNpbmd8fGlzRnVuY3Rpb24oc3BlZWQpJiZzcGVlZCxkdXJhdGlvbjpzcGVlZCxlYXNpbmc6Zm4mJmVhc2luZ3x8ZWFzaW5nJiYhaXNGdW5jdGlvbihlYXNpbmcpJiZlYXNpbmd9O2lmKGpRdWVyeS5meC5vZmYpe29wdC5kdXJhdGlvbj0wO31lbHNle2lmKHR5cGVvZiBvcHQuZHVyYXRpb24hPT1cIm51bWJlclwiKXtpZihvcHQuZHVyYXRpb24gaW4galF1ZXJ5LmZ4LnNwZWVkcyl7b3B0LmR1cmF0aW9uPWpRdWVyeS5meC5zcGVlZHNbb3B0LmR1cmF0aW9uXTt9ZWxzZXtvcHQuZHVyYXRpb249alF1ZXJ5LmZ4LnNwZWVkcy5fZGVmYXVsdDt9fX1cbmlmKG9wdC5xdWV1ZT09bnVsbHx8b3B0LnF1ZXVlPT09dHJ1ZSl7b3B0LnF1ZXVlPVwiZnhcIjt9XG5vcHQub2xkPW9wdC5jb21wbGV0ZTtvcHQuY29tcGxldGU9ZnVuY3Rpb24oKXtpZihpc0Z1bmN0aW9uKG9wdC5vbGQpKXtvcHQub2xkLmNhbGwodGhpcyk7fVxuaWYob3B0LnF1ZXVlKXtqUXVlcnkuZGVxdWV1ZSh0aGlzLG9wdC5xdWV1ZSk7fX07cmV0dXJuIG9wdDt9O2pRdWVyeS5mbi5leHRlbmQoe2ZhZGVUbzpmdW5jdGlvbihzcGVlZCx0byxlYXNpbmcsY2FsbGJhY2spe3JldHVybiB0aGlzLmZpbHRlcihpc0hpZGRlbldpdGhpblRyZWUpLmNzcyhcIm9wYWNpdHlcIiwwKS5zaG93KCkuZW5kKCkuYW5pbWF0ZSh7b3BhY2l0eTp0b30sc3BlZWQsZWFzaW5nLGNhbGxiYWNrKTt9LGFuaW1hdGU6ZnVuY3Rpb24ocHJvcCxzcGVlZCxlYXNpbmcsY2FsbGJhY2spe3ZhciBlbXB0eT1qUXVlcnkuaXNFbXB0eU9iamVjdChwcm9wKSxvcHRhbGw9alF1ZXJ5LnNwZWVkKHNwZWVkLGVhc2luZyxjYWxsYmFjayksZG9BbmltYXRpb249ZnVuY3Rpb24oKXt2YXIgYW5pbT1BbmltYXRpb24odGhpcyxqUXVlcnkuZXh0ZW5kKHt9LHByb3ApLG9wdGFsbCk7aWYoZW1wdHl8fGRhdGFQcml2LmdldCh0aGlzLFwiZmluaXNoXCIpKXthbmltLnN0b3AodHJ1ZSk7fX07ZG9BbmltYXRpb24uZmluaXNoPWRvQW5pbWF0aW9uO3JldHVybiBlbXB0eXx8b3B0YWxsLnF1ZXVlPT09ZmFsc2U/dGhpcy5lYWNoKGRvQW5pbWF0aW9uKTp0aGlzLnF1ZXVlKG9wdGFsbC5xdWV1ZSxkb0FuaW1hdGlvbik7fSxzdG9wOmZ1bmN0aW9uKHR5cGUsY2xlYXJRdWV1ZSxnb3RvRW5kKXt2YXIgc3RvcFF1ZXVlPWZ1bmN0aW9uKGhvb2tzKXt2YXIgc3RvcD1ob29rcy5zdG9wO2RlbGV0ZSBob29rcy5zdG9wO3N0b3AoZ290b0VuZCk7fTtpZih0eXBlb2YgdHlwZSE9PVwic3RyaW5nXCIpe2dvdG9FbmQ9Y2xlYXJRdWV1ZTtjbGVhclF1ZXVlPXR5cGU7dHlwZT11bmRlZmluZWQ7fVxuaWYoY2xlYXJRdWV1ZSl7dGhpcy5xdWV1ZSh0eXBlfHxcImZ4XCIsW10pO31cbnJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXt2YXIgZGVxdWV1ZT10cnVlLGluZGV4PXR5cGUhPW51bGwmJnR5cGUrXCJxdWV1ZUhvb2tzXCIsdGltZXJzPWpRdWVyeS50aW1lcnMsZGF0YT1kYXRhUHJpdi5nZXQodGhpcyk7aWYoaW5kZXgpe2lmKGRhdGFbaW5kZXhdJiZkYXRhW2luZGV4XS5zdG9wKXtzdG9wUXVldWUoZGF0YVtpbmRleF0pO319ZWxzZXtmb3IoaW5kZXggaW4gZGF0YSl7aWYoZGF0YVtpbmRleF0mJmRhdGFbaW5kZXhdLnN0b3AmJnJydW4udGVzdChpbmRleCkpe3N0b3BRdWV1ZShkYXRhW2luZGV4XSk7fX19XG5mb3IoaW5kZXg9dGltZXJzLmxlbmd0aDtpbmRleC0tOyl7aWYodGltZXJzW2luZGV4XS5lbGVtPT09dGhpcyYmKHR5cGU9PW51bGx8fHRpbWVyc1tpbmRleF0ucXVldWU9PT10eXBlKSl7dGltZXJzW2luZGV4XS5hbmltLnN0b3AoZ290b0VuZCk7ZGVxdWV1ZT1mYWxzZTt0aW1lcnMuc3BsaWNlKGluZGV4LDEpO319XG5pZihkZXF1ZXVlfHwhZ290b0VuZCl7alF1ZXJ5LmRlcXVldWUodGhpcyx0eXBlKTt9fSk7fSxmaW5pc2g6ZnVuY3Rpb24odHlwZSl7aWYodHlwZSE9PWZhbHNlKXt0eXBlPXR5cGV8fFwiZnhcIjt9XG5yZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKCl7dmFyIGluZGV4LGRhdGE9ZGF0YVByaXYuZ2V0KHRoaXMpLHF1ZXVlPWRhdGFbdHlwZStcInF1ZXVlXCJdLGhvb2tzPWRhdGFbdHlwZStcInF1ZXVlSG9va3NcIl0sdGltZXJzPWpRdWVyeS50aW1lcnMsbGVuZ3RoPXF1ZXVlP3F1ZXVlLmxlbmd0aDowO2RhdGEuZmluaXNoPXRydWU7alF1ZXJ5LnF1ZXVlKHRoaXMsdHlwZSxbXSk7aWYoaG9va3MmJmhvb2tzLnN0b3Ape2hvb2tzLnN0b3AuY2FsbCh0aGlzLHRydWUpO31cbmZvcihpbmRleD10aW1lcnMubGVuZ3RoO2luZGV4LS07KXtpZih0aW1lcnNbaW5kZXhdLmVsZW09PT10aGlzJiZ0aW1lcnNbaW5kZXhdLnF1ZXVlPT09dHlwZSl7dGltZXJzW2luZGV4XS5hbmltLnN0b3AodHJ1ZSk7dGltZXJzLnNwbGljZShpbmRleCwxKTt9fVxuZm9yKGluZGV4PTA7aW5kZXg8bGVuZ3RoO2luZGV4Kyspe2lmKHF1ZXVlW2luZGV4XSYmcXVldWVbaW5kZXhdLmZpbmlzaCl7cXVldWVbaW5kZXhdLmZpbmlzaC5jYWxsKHRoaXMpO319XG5kZWxldGUgZGF0YS5maW5pc2g7fSk7fX0pO2pRdWVyeS5lYWNoKFtcInRvZ2dsZVwiLFwic2hvd1wiLFwiaGlkZVwiXSxmdW5jdGlvbihfaSxuYW1lKXt2YXIgY3NzRm49alF1ZXJ5LmZuW25hbWVdO2pRdWVyeS5mbltuYW1lXT1mdW5jdGlvbihzcGVlZCxlYXNpbmcsY2FsbGJhY2spe3JldHVybiBzcGVlZD09bnVsbHx8dHlwZW9mIHNwZWVkPT09XCJib29sZWFuXCI/Y3NzRm4uYXBwbHkodGhpcyxhcmd1bWVudHMpOnRoaXMuYW5pbWF0ZShnZW5GeChuYW1lLHRydWUpLHNwZWVkLGVhc2luZyxjYWxsYmFjayk7fTt9KTtqUXVlcnkuZWFjaCh7c2xpZGVEb3duOmdlbkZ4KFwic2hvd1wiKSxzbGlkZVVwOmdlbkZ4KFwiaGlkZVwiKSxzbGlkZVRvZ2dsZTpnZW5GeChcInRvZ2dsZVwiKSxmYWRlSW46e29wYWNpdHk6XCJzaG93XCJ9LGZhZGVPdXQ6e29wYWNpdHk6XCJoaWRlXCJ9LGZhZGVUb2dnbGU6e29wYWNpdHk6XCJ0b2dnbGVcIn19LGZ1bmN0aW9uKG5hbWUscHJvcHMpe2pRdWVyeS5mbltuYW1lXT1mdW5jdGlvbihzcGVlZCxlYXNpbmcsY2FsbGJhY2spe3JldHVybiB0aGlzLmFuaW1hdGUocHJvcHMsc3BlZWQsZWFzaW5nLGNhbGxiYWNrKTt9O30pO2pRdWVyeS50aW1lcnM9W107alF1ZXJ5LmZ4LnRpY2s9ZnVuY3Rpb24oKXt2YXIgdGltZXIsaT0wLHRpbWVycz1qUXVlcnkudGltZXJzO2Z4Tm93PURhdGUubm93KCk7Zm9yKDtpPHRpbWVycy5sZW5ndGg7aSsrKXt0aW1lcj10aW1lcnNbaV07aWYoIXRpbWVyKCkmJnRpbWVyc1tpXT09PXRpbWVyKXt0aW1lcnMuc3BsaWNlKGktLSwxKTt9fVxuaWYoIXRpbWVycy5sZW5ndGgpe2pRdWVyeS5meC5zdG9wKCk7fVxuZnhOb3c9dW5kZWZpbmVkO307alF1ZXJ5LmZ4LnRpbWVyPWZ1bmN0aW9uKHRpbWVyKXtqUXVlcnkudGltZXJzLnB1c2godGltZXIpO2pRdWVyeS5meC5zdGFydCgpO307alF1ZXJ5LmZ4LmludGVydmFsPTEzO2pRdWVyeS5meC5zdGFydD1mdW5jdGlvbigpe2lmKGluUHJvZ3Jlc3Mpe3JldHVybjt9XG5pblByb2dyZXNzPXRydWU7c2NoZWR1bGUoKTt9O2pRdWVyeS5meC5zdG9wPWZ1bmN0aW9uKCl7aW5Qcm9ncmVzcz1udWxsO307alF1ZXJ5LmZ4LnNwZWVkcz17c2xvdzo2MDAsZmFzdDoyMDAsX2RlZmF1bHQ6NDAwfTtqUXVlcnkuZm4uZGVsYXk9ZnVuY3Rpb24odGltZSx0eXBlKXt0aW1lPWpRdWVyeS5meD9qUXVlcnkuZnguc3BlZWRzW3RpbWVdfHx0aW1lOnRpbWU7dHlwZT10eXBlfHxcImZ4XCI7cmV0dXJuIHRoaXMucXVldWUodHlwZSxmdW5jdGlvbihuZXh0LGhvb2tzKXt2YXIgdGltZW91dD13aW5kb3cuc2V0VGltZW91dChuZXh0LHRpbWUpO2hvb2tzLnN0b3A9ZnVuY3Rpb24oKXt3aW5kb3cuY2xlYXJUaW1lb3V0KHRpbWVvdXQpO307fSk7fTsoZnVuY3Rpb24oKXt2YXIgaW5wdXQ9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImlucHV0XCIpLHNlbGVjdD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwic2VsZWN0XCIpLG9wdD1zZWxlY3QuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcIm9wdGlvblwiKSk7aW5wdXQudHlwZT1cImNoZWNrYm94XCI7c3VwcG9ydC5jaGVja09uPWlucHV0LnZhbHVlIT09XCJcIjtzdXBwb3J0Lm9wdFNlbGVjdGVkPW9wdC5zZWxlY3RlZDtpbnB1dD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiaW5wdXRcIik7aW5wdXQudmFsdWU9XCJ0XCI7aW5wdXQudHlwZT1cInJhZGlvXCI7c3VwcG9ydC5yYWRpb1ZhbHVlPWlucHV0LnZhbHVlPT09XCJ0XCI7fSkoKTt2YXIgYm9vbEhvb2ssYXR0ckhhbmRsZT1qUXVlcnkuZXhwci5hdHRySGFuZGxlO2pRdWVyeS5mbi5leHRlbmQoe2F0dHI6ZnVuY3Rpb24obmFtZSx2YWx1ZSl7cmV0dXJuIGFjY2Vzcyh0aGlzLGpRdWVyeS5hdHRyLG5hbWUsdmFsdWUsYXJndW1lbnRzLmxlbmd0aD4xKTt9LHJlbW92ZUF0dHI6ZnVuY3Rpb24obmFtZSl7cmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbigpe2pRdWVyeS5yZW1vdmVBdHRyKHRoaXMsbmFtZSk7fSk7fX0pO2pRdWVyeS5leHRlbmQoe2F0dHI6ZnVuY3Rpb24oZWxlbSxuYW1lLHZhbHVlKXt2YXIgcmV0LGhvb2tzLG5UeXBlPWVsZW0ubm9kZVR5cGU7aWYoblR5cGU9PT0zfHxuVHlwZT09PTh8fG5UeXBlPT09Mil7cmV0dXJuO31cbmlmKHR5cGVvZiBlbGVtLmdldEF0dHJpYnV0ZT09PVwidW5kZWZpbmVkXCIpe3JldHVybiBqUXVlcnkucHJvcChlbGVtLG5hbWUsdmFsdWUpO31cbmlmKG5UeXBlIT09MXx8IWpRdWVyeS5pc1hNTERvYyhlbGVtKSl7aG9va3M9alF1ZXJ5LmF0dHJIb29rc1tuYW1lLnRvTG93ZXJDYXNlKCldfHwoalF1ZXJ5LmV4cHIubWF0Y2guYm9vbC50ZXN0KG5hbWUpP2Jvb2xIb29rOnVuZGVmaW5lZCk7fVxuaWYodmFsdWUhPT11bmRlZmluZWQpe2lmKHZhbHVlPT09bnVsbCl7alF1ZXJ5LnJlbW92ZUF0dHIoZWxlbSxuYW1lKTtyZXR1cm47fVxuaWYoaG9va3MmJlwic2V0XCJpbiBob29rcyYmKHJldD1ob29rcy5zZXQoZWxlbSx2YWx1ZSxuYW1lKSkhPT11bmRlZmluZWQpe3JldHVybiByZXQ7fVxuZWxlbS5zZXRBdHRyaWJ1dGUobmFtZSx2YWx1ZStcIlwiKTtyZXR1cm4gdmFsdWU7fVxuaWYoaG9va3MmJlwiZ2V0XCJpbiBob29rcyYmKHJldD1ob29rcy5nZXQoZWxlbSxuYW1lKSkhPT1udWxsKXtyZXR1cm4gcmV0O31cbnJldD1qUXVlcnkuZmluZC5hdHRyKGVsZW0sbmFtZSk7cmV0dXJuIHJldD09bnVsbD91bmRlZmluZWQ6cmV0O30sYXR0ckhvb2tzOnt0eXBlOntzZXQ6ZnVuY3Rpb24oZWxlbSx2YWx1ZSl7aWYoIXN1cHBvcnQucmFkaW9WYWx1ZSYmdmFsdWU9PT1cInJhZGlvXCImJm5vZGVOYW1lKGVsZW0sXCJpbnB1dFwiKSl7dmFyIHZhbD1lbGVtLnZhbHVlO2VsZW0uc2V0QXR0cmlidXRlKFwidHlwZVwiLHZhbHVlKTtpZih2YWwpe2VsZW0udmFsdWU9dmFsO31cbnJldHVybiB2YWx1ZTt9fX19LHJlbW92ZUF0dHI6ZnVuY3Rpb24oZWxlbSx2YWx1ZSl7dmFyIG5hbWUsaT0wLGF0dHJOYW1lcz12YWx1ZSYmdmFsdWUubWF0Y2gocm5vdGh0bWx3aGl0ZSk7aWYoYXR0ck5hbWVzJiZlbGVtLm5vZGVUeXBlPT09MSl7d2hpbGUoKG5hbWU9YXR0ck5hbWVzW2krK10pKXtlbGVtLnJlbW92ZUF0dHJpYnV0ZShuYW1lKTt9fX19KTtib29sSG9vaz17c2V0OmZ1bmN0aW9uKGVsZW0sdmFsdWUsbmFtZSl7aWYodmFsdWU9PT1mYWxzZSl7alF1ZXJ5LnJlbW92ZUF0dHIoZWxlbSxuYW1lKTt9ZWxzZXtlbGVtLnNldEF0dHJpYnV0ZShuYW1lLG5hbWUpO31cbnJldHVybiBuYW1lO319O2pRdWVyeS5lYWNoKGpRdWVyeS5leHByLm1hdGNoLmJvb2wuc291cmNlLm1hdGNoKC9cXHcrL2cpLGZ1bmN0aW9uKF9pLG5hbWUpe3ZhciBnZXR0ZXI9YXR0ckhhbmRsZVtuYW1lXXx8alF1ZXJ5LmZpbmQuYXR0cjthdHRySGFuZGxlW25hbWVdPWZ1bmN0aW9uKGVsZW0sbmFtZSxpc1hNTCl7dmFyIHJldCxoYW5kbGUsbG93ZXJjYXNlTmFtZT1uYW1lLnRvTG93ZXJDYXNlKCk7aWYoIWlzWE1MKXtoYW5kbGU9YXR0ckhhbmRsZVtsb3dlcmNhc2VOYW1lXTthdHRySGFuZGxlW2xvd2VyY2FzZU5hbWVdPXJldDtyZXQ9Z2V0dGVyKGVsZW0sbmFtZSxpc1hNTCkhPW51bGw/bG93ZXJjYXNlTmFtZTpudWxsO2F0dHJIYW5kbGVbbG93ZXJjYXNlTmFtZV09aGFuZGxlO31cbnJldHVybiByZXQ7fTt9KTt2YXIgcmZvY3VzYWJsZT0vXig/OmlucHV0fHNlbGVjdHx0ZXh0YXJlYXxidXR0b24pJC9pLHJjbGlja2FibGU9L14oPzphfGFyZWEpJC9pO2pRdWVyeS5mbi5leHRlbmQoe3Byb3A6ZnVuY3Rpb24obmFtZSx2YWx1ZSl7cmV0dXJuIGFjY2Vzcyh0aGlzLGpRdWVyeS5wcm9wLG5hbWUsdmFsdWUsYXJndW1lbnRzLmxlbmd0aD4xKTt9LHJlbW92ZVByb3A6ZnVuY3Rpb24obmFtZSl7cmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbigpe2RlbGV0ZSB0aGlzW2pRdWVyeS5wcm9wRml4W25hbWVdfHxuYW1lXTt9KTt9fSk7alF1ZXJ5LmV4dGVuZCh7cHJvcDpmdW5jdGlvbihlbGVtLG5hbWUsdmFsdWUpe3ZhciByZXQsaG9va3MsblR5cGU9ZWxlbS5ub2RlVHlwZTtpZihuVHlwZT09PTN8fG5UeXBlPT09OHx8blR5cGU9PT0yKXtyZXR1cm47fVxuaWYoblR5cGUhPT0xfHwhalF1ZXJ5LmlzWE1MRG9jKGVsZW0pKXtuYW1lPWpRdWVyeS5wcm9wRml4W25hbWVdfHxuYW1lO2hvb2tzPWpRdWVyeS5wcm9wSG9va3NbbmFtZV07fVxuaWYodmFsdWUhPT11bmRlZmluZWQpe2lmKGhvb2tzJiZcInNldFwiaW4gaG9va3MmJihyZXQ9aG9va3Muc2V0KGVsZW0sdmFsdWUsbmFtZSkpIT09dW5kZWZpbmVkKXtyZXR1cm4gcmV0O31cbnJldHVybihlbGVtW25hbWVdPXZhbHVlKTt9XG5pZihob29rcyYmXCJnZXRcImluIGhvb2tzJiYocmV0PWhvb2tzLmdldChlbGVtLG5hbWUpKSE9PW51bGwpe3JldHVybiByZXQ7fVxucmV0dXJuIGVsZW1bbmFtZV07fSxwcm9wSG9va3M6e3RhYkluZGV4OntnZXQ6ZnVuY3Rpb24oZWxlbSl7dmFyIHRhYmluZGV4PWpRdWVyeS5maW5kLmF0dHIoZWxlbSxcInRhYmluZGV4XCIpO2lmKHRhYmluZGV4KXtyZXR1cm4gcGFyc2VJbnQodGFiaW5kZXgsMTApO31cbmlmKHJmb2N1c2FibGUudGVzdChlbGVtLm5vZGVOYW1lKXx8cmNsaWNrYWJsZS50ZXN0KGVsZW0ubm9kZU5hbWUpJiZlbGVtLmhyZWYpe3JldHVybiAwO31cbnJldHVybi0xO319fSxwcm9wRml4OntcImZvclwiOlwiaHRtbEZvclwiLFwiY2xhc3NcIjpcImNsYXNzTmFtZVwifX0pO2lmKCFzdXBwb3J0Lm9wdFNlbGVjdGVkKXtqUXVlcnkucHJvcEhvb2tzLnNlbGVjdGVkPXtnZXQ6ZnVuY3Rpb24oZWxlbSl7dmFyIHBhcmVudD1lbGVtLnBhcmVudE5vZGU7aWYocGFyZW50JiZwYXJlbnQucGFyZW50Tm9kZSl7cGFyZW50LnBhcmVudE5vZGUuc2VsZWN0ZWRJbmRleDt9XG5yZXR1cm4gbnVsbDt9LHNldDpmdW5jdGlvbihlbGVtKXt2YXIgcGFyZW50PWVsZW0ucGFyZW50Tm9kZTtpZihwYXJlbnQpe3BhcmVudC5zZWxlY3RlZEluZGV4O2lmKHBhcmVudC5wYXJlbnROb2RlKXtwYXJlbnQucGFyZW50Tm9kZS5zZWxlY3RlZEluZGV4O319fX07fVxualF1ZXJ5LmVhY2goW1widGFiSW5kZXhcIixcInJlYWRPbmx5XCIsXCJtYXhMZW5ndGhcIixcImNlbGxTcGFjaW5nXCIsXCJjZWxsUGFkZGluZ1wiLFwicm93U3BhblwiLFwiY29sU3BhblwiLFwidXNlTWFwXCIsXCJmcmFtZUJvcmRlclwiLFwiY29udGVudEVkaXRhYmxlXCJdLGZ1bmN0aW9uKCl7alF1ZXJ5LnByb3BGaXhbdGhpcy50b0xvd2VyQ2FzZSgpXT10aGlzO30pO2Z1bmN0aW9uIHN0cmlwQW5kQ29sbGFwc2UodmFsdWUpe3ZhciB0b2tlbnM9dmFsdWUubWF0Y2gocm5vdGh0bWx3aGl0ZSl8fFtdO3JldHVybiB0b2tlbnMuam9pbihcIiBcIik7fVxuZnVuY3Rpb24gZ2V0Q2xhc3MoZWxlbSl7cmV0dXJuIGVsZW0uZ2V0QXR0cmlidXRlJiZlbGVtLmdldEF0dHJpYnV0ZShcImNsYXNzXCIpfHxcIlwiO31cbmZ1bmN0aW9uIGNsYXNzZXNUb0FycmF5KHZhbHVlKXtpZihBcnJheS5pc0FycmF5KHZhbHVlKSl7cmV0dXJuIHZhbHVlO31cbmlmKHR5cGVvZiB2YWx1ZT09PVwic3RyaW5nXCIpe3JldHVybiB2YWx1ZS5tYXRjaChybm90aHRtbHdoaXRlKXx8W107fVxucmV0dXJuW107fVxualF1ZXJ5LmZuLmV4dGVuZCh7YWRkQ2xhc3M6ZnVuY3Rpb24odmFsdWUpe3ZhciBjbGFzc2VzLGVsZW0sY3VyLGN1clZhbHVlLGNsYXp6LGosZmluYWxWYWx1ZSxpPTA7aWYoaXNGdW5jdGlvbih2YWx1ZSkpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oail7alF1ZXJ5KHRoaXMpLmFkZENsYXNzKHZhbHVlLmNhbGwodGhpcyxqLGdldENsYXNzKHRoaXMpKSk7fSk7fVxuY2xhc3Nlcz1jbGFzc2VzVG9BcnJheSh2YWx1ZSk7aWYoY2xhc3Nlcy5sZW5ndGgpe3doaWxlKChlbGVtPXRoaXNbaSsrXSkpe2N1clZhbHVlPWdldENsYXNzKGVsZW0pO2N1cj1lbGVtLm5vZGVUeXBlPT09MSYmKFwiIFwiK3N0cmlwQW5kQ29sbGFwc2UoY3VyVmFsdWUpK1wiIFwiKTtpZihjdXIpe2o9MDt3aGlsZSgoY2xheno9Y2xhc3Nlc1tqKytdKSl7aWYoY3VyLmluZGV4T2YoXCIgXCIrY2xhenorXCIgXCIpPDApe2N1cis9Y2xhenorXCIgXCI7fX1cbmZpbmFsVmFsdWU9c3RyaXBBbmRDb2xsYXBzZShjdXIpO2lmKGN1clZhbHVlIT09ZmluYWxWYWx1ZSl7ZWxlbS5zZXRBdHRyaWJ1dGUoXCJjbGFzc1wiLGZpbmFsVmFsdWUpO319fX1cbnJldHVybiB0aGlzO30scmVtb3ZlQ2xhc3M6ZnVuY3Rpb24odmFsdWUpe3ZhciBjbGFzc2VzLGVsZW0sY3VyLGN1clZhbHVlLGNsYXp6LGosZmluYWxWYWx1ZSxpPTA7aWYoaXNGdW5jdGlvbih2YWx1ZSkpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oail7alF1ZXJ5KHRoaXMpLnJlbW92ZUNsYXNzKHZhbHVlLmNhbGwodGhpcyxqLGdldENsYXNzKHRoaXMpKSk7fSk7fVxuaWYoIWFyZ3VtZW50cy5sZW5ndGgpe3JldHVybiB0aGlzLmF0dHIoXCJjbGFzc1wiLFwiXCIpO31cbmNsYXNzZXM9Y2xhc3Nlc1RvQXJyYXkodmFsdWUpO2lmKGNsYXNzZXMubGVuZ3RoKXt3aGlsZSgoZWxlbT10aGlzW2krK10pKXtjdXJWYWx1ZT1nZXRDbGFzcyhlbGVtKTtjdXI9ZWxlbS5ub2RlVHlwZT09PTEmJihcIiBcIitzdHJpcEFuZENvbGxhcHNlKGN1clZhbHVlKStcIiBcIik7aWYoY3VyKXtqPTA7d2hpbGUoKGNsYXp6PWNsYXNzZXNbaisrXSkpe3doaWxlKGN1ci5pbmRleE9mKFwiIFwiK2NsYXp6K1wiIFwiKT4tMSl7Y3VyPWN1ci5yZXBsYWNlKFwiIFwiK2NsYXp6K1wiIFwiLFwiIFwiKTt9fVxuZmluYWxWYWx1ZT1zdHJpcEFuZENvbGxhcHNlKGN1cik7aWYoY3VyVmFsdWUhPT1maW5hbFZhbHVlKXtlbGVtLnNldEF0dHJpYnV0ZShcImNsYXNzXCIsZmluYWxWYWx1ZSk7fX19fVxucmV0dXJuIHRoaXM7fSx0b2dnbGVDbGFzczpmdW5jdGlvbih2YWx1ZSxzdGF0ZVZhbCl7dmFyIHR5cGU9dHlwZW9mIHZhbHVlLGlzVmFsaWRWYWx1ZT10eXBlPT09XCJzdHJpbmdcInx8QXJyYXkuaXNBcnJheSh2YWx1ZSk7aWYodHlwZW9mIHN0YXRlVmFsPT09XCJib29sZWFuXCImJmlzVmFsaWRWYWx1ZSl7cmV0dXJuIHN0YXRlVmFsP3RoaXMuYWRkQ2xhc3ModmFsdWUpOnRoaXMucmVtb3ZlQ2xhc3ModmFsdWUpO31cbmlmKGlzRnVuY3Rpb24odmFsdWUpKXtyZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKGkpe2pRdWVyeSh0aGlzKS50b2dnbGVDbGFzcyh2YWx1ZS5jYWxsKHRoaXMsaSxnZXRDbGFzcyh0aGlzKSxzdGF0ZVZhbCksc3RhdGVWYWwpO30pO31cbnJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXt2YXIgY2xhc3NOYW1lLGksc2VsZixjbGFzc05hbWVzO2lmKGlzVmFsaWRWYWx1ZSl7aT0wO3NlbGY9alF1ZXJ5KHRoaXMpO2NsYXNzTmFtZXM9Y2xhc3Nlc1RvQXJyYXkodmFsdWUpO3doaWxlKChjbGFzc05hbWU9Y2xhc3NOYW1lc1tpKytdKSl7aWYoc2VsZi5oYXNDbGFzcyhjbGFzc05hbWUpKXtzZWxmLnJlbW92ZUNsYXNzKGNsYXNzTmFtZSk7fWVsc2V7c2VsZi5hZGRDbGFzcyhjbGFzc05hbWUpO319fWVsc2UgaWYodmFsdWU9PT11bmRlZmluZWR8fHR5cGU9PT1cImJvb2xlYW5cIil7Y2xhc3NOYW1lPWdldENsYXNzKHRoaXMpO2lmKGNsYXNzTmFtZSl7ZGF0YVByaXYuc2V0KHRoaXMsXCJfX2NsYXNzTmFtZV9fXCIsY2xhc3NOYW1lKTt9XG5pZih0aGlzLnNldEF0dHJpYnV0ZSl7dGhpcy5zZXRBdHRyaWJ1dGUoXCJjbGFzc1wiLGNsYXNzTmFtZXx8dmFsdWU9PT1mYWxzZT9cIlwiOmRhdGFQcml2LmdldCh0aGlzLFwiX19jbGFzc05hbWVfX1wiKXx8XCJcIik7fX19KTt9LGhhc0NsYXNzOmZ1bmN0aW9uKHNlbGVjdG9yKXt2YXIgY2xhc3NOYW1lLGVsZW0saT0wO2NsYXNzTmFtZT1cIiBcIitzZWxlY3RvcitcIiBcIjt3aGlsZSgoZWxlbT10aGlzW2krK10pKXtpZihlbGVtLm5vZGVUeXBlPT09MSYmKFwiIFwiK3N0cmlwQW5kQ29sbGFwc2UoZ2V0Q2xhc3MoZWxlbSkpK1wiIFwiKS5pbmRleE9mKGNsYXNzTmFtZSk+LTEpe3JldHVybiB0cnVlO319XG5yZXR1cm4gZmFsc2U7fX0pO3ZhciBycmV0dXJuPS9cXHIvZztqUXVlcnkuZm4uZXh0ZW5kKHt2YWw6ZnVuY3Rpb24odmFsdWUpe3ZhciBob29rcyxyZXQsdmFsdWVJc0Z1bmN0aW9uLGVsZW09dGhpc1swXTtpZighYXJndW1lbnRzLmxlbmd0aCl7aWYoZWxlbSl7aG9va3M9alF1ZXJ5LnZhbEhvb2tzW2VsZW0udHlwZV18fGpRdWVyeS52YWxIb29rc1tlbGVtLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCldO2lmKGhvb2tzJiZcImdldFwiaW4gaG9va3MmJihyZXQ9aG9va3MuZ2V0KGVsZW0sXCJ2YWx1ZVwiKSkhPT11bmRlZmluZWQpe3JldHVybiByZXQ7fVxucmV0PWVsZW0udmFsdWU7aWYodHlwZW9mIHJldD09PVwic3RyaW5nXCIpe3JldHVybiByZXQucmVwbGFjZShycmV0dXJuLFwiXCIpO31cbnJldHVybiByZXQ9PW51bGw/XCJcIjpyZXQ7fVxucmV0dXJuO31cbnZhbHVlSXNGdW5jdGlvbj1pc0Z1bmN0aW9uKHZhbHVlKTtyZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKGkpe3ZhciB2YWw7aWYodGhpcy5ub2RlVHlwZSE9PTEpe3JldHVybjt9XG5pZih2YWx1ZUlzRnVuY3Rpb24pe3ZhbD12YWx1ZS5jYWxsKHRoaXMsaSxqUXVlcnkodGhpcykudmFsKCkpO31lbHNle3ZhbD12YWx1ZTt9XG5pZih2YWw9PW51bGwpe3ZhbD1cIlwiO31lbHNlIGlmKHR5cGVvZiB2YWw9PT1cIm51bWJlclwiKXt2YWwrPVwiXCI7fWVsc2UgaWYoQXJyYXkuaXNBcnJheSh2YWwpKXt2YWw9alF1ZXJ5Lm1hcCh2YWwsZnVuY3Rpb24odmFsdWUpe3JldHVybiB2YWx1ZT09bnVsbD9cIlwiOnZhbHVlK1wiXCI7fSk7fVxuaG9va3M9alF1ZXJ5LnZhbEhvb2tzW3RoaXMudHlwZV18fGpRdWVyeS52YWxIb29rc1t0aGlzLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCldO2lmKCFob29rc3x8IShcInNldFwiaW4gaG9va3MpfHxob29rcy5zZXQodGhpcyx2YWwsXCJ2YWx1ZVwiKT09PXVuZGVmaW5lZCl7dGhpcy52YWx1ZT12YWw7fX0pO319KTtqUXVlcnkuZXh0ZW5kKHt2YWxIb29rczp7b3B0aW9uOntnZXQ6ZnVuY3Rpb24oZWxlbSl7dmFyIHZhbD1qUXVlcnkuZmluZC5hdHRyKGVsZW0sXCJ2YWx1ZVwiKTtyZXR1cm4gdmFsIT1udWxsP3ZhbDpzdHJpcEFuZENvbGxhcHNlKGpRdWVyeS50ZXh0KGVsZW0pKTt9fSxzZWxlY3Q6e2dldDpmdW5jdGlvbihlbGVtKXt2YXIgdmFsdWUsb3B0aW9uLGksb3B0aW9ucz1lbGVtLm9wdGlvbnMsaW5kZXg9ZWxlbS5zZWxlY3RlZEluZGV4LG9uZT1lbGVtLnR5cGU9PT1cInNlbGVjdC1vbmVcIix2YWx1ZXM9b25lP251bGw6W10sbWF4PW9uZT9pbmRleCsxOm9wdGlvbnMubGVuZ3RoO2lmKGluZGV4PDApe2k9bWF4O31lbHNle2k9b25lP2luZGV4OjA7fVxuZm9yKDtpPG1heDtpKyspe29wdGlvbj1vcHRpb25zW2ldO2lmKChvcHRpb24uc2VsZWN0ZWR8fGk9PT1pbmRleCkmJiFvcHRpb24uZGlzYWJsZWQmJighb3B0aW9uLnBhcmVudE5vZGUuZGlzYWJsZWR8fCFub2RlTmFtZShvcHRpb24ucGFyZW50Tm9kZSxcIm9wdGdyb3VwXCIpKSl7dmFsdWU9alF1ZXJ5KG9wdGlvbikudmFsKCk7aWYob25lKXtyZXR1cm4gdmFsdWU7fVxudmFsdWVzLnB1c2godmFsdWUpO319XG5yZXR1cm4gdmFsdWVzO30sc2V0OmZ1bmN0aW9uKGVsZW0sdmFsdWUpe3ZhciBvcHRpb25TZXQsb3B0aW9uLG9wdGlvbnM9ZWxlbS5vcHRpb25zLHZhbHVlcz1qUXVlcnkubWFrZUFycmF5KHZhbHVlKSxpPW9wdGlvbnMubGVuZ3RoO3doaWxlKGktLSl7b3B0aW9uPW9wdGlvbnNbaV07aWYob3B0aW9uLnNlbGVjdGVkPWpRdWVyeS5pbkFycmF5KGpRdWVyeS52YWxIb29rcy5vcHRpb24uZ2V0KG9wdGlvbiksdmFsdWVzKT4tMSl7b3B0aW9uU2V0PXRydWU7fX1cbmlmKCFvcHRpb25TZXQpe2VsZW0uc2VsZWN0ZWRJbmRleD0tMTt9XG5yZXR1cm4gdmFsdWVzO319fX0pO2pRdWVyeS5lYWNoKFtcInJhZGlvXCIsXCJjaGVja2JveFwiXSxmdW5jdGlvbigpe2pRdWVyeS52YWxIb29rc1t0aGlzXT17c2V0OmZ1bmN0aW9uKGVsZW0sdmFsdWUpe2lmKEFycmF5LmlzQXJyYXkodmFsdWUpKXtyZXR1cm4oZWxlbS5jaGVja2VkPWpRdWVyeS5pbkFycmF5KGpRdWVyeShlbGVtKS52YWwoKSx2YWx1ZSk+LTEpO319fTtpZighc3VwcG9ydC5jaGVja09uKXtqUXVlcnkudmFsSG9va3NbdGhpc10uZ2V0PWZ1bmN0aW9uKGVsZW0pe3JldHVybiBlbGVtLmdldEF0dHJpYnV0ZShcInZhbHVlXCIpPT09bnVsbD9cIm9uXCI6ZWxlbS52YWx1ZTt9O319KTtzdXBwb3J0LmZvY3VzaW49XCJvbmZvY3VzaW5cImluIHdpbmRvdzt2YXIgcmZvY3VzTW9ycGg9L14oPzpmb2N1c2luZm9jdXN8Zm9jdXNvdXRibHVyKSQvLHN0b3BQcm9wYWdhdGlvbkNhbGxiYWNrPWZ1bmN0aW9uKGUpe2Uuc3RvcFByb3BhZ2F0aW9uKCk7fTtqUXVlcnkuZXh0ZW5kKGpRdWVyeS5ldmVudCx7dHJpZ2dlcjpmdW5jdGlvbihldmVudCxkYXRhLGVsZW0sb25seUhhbmRsZXJzKXt2YXIgaSxjdXIsdG1wLGJ1YmJsZVR5cGUsb250eXBlLGhhbmRsZSxzcGVjaWFsLGxhc3RFbGVtZW50LGV2ZW50UGF0aD1bZWxlbXx8ZG9jdW1lbnRdLHR5cGU9aGFzT3duLmNhbGwoZXZlbnQsXCJ0eXBlXCIpP2V2ZW50LnR5cGU6ZXZlbnQsbmFtZXNwYWNlcz1oYXNPd24uY2FsbChldmVudCxcIm5hbWVzcGFjZVwiKT9ldmVudC5uYW1lc3BhY2Uuc3BsaXQoXCIuXCIpOltdO2N1cj1sYXN0RWxlbWVudD10bXA9ZWxlbT1lbGVtfHxkb2N1bWVudDtpZihlbGVtLm5vZGVUeXBlPT09M3x8ZWxlbS5ub2RlVHlwZT09PTgpe3JldHVybjt9XG5pZihyZm9jdXNNb3JwaC50ZXN0KHR5cGUralF1ZXJ5LmV2ZW50LnRyaWdnZXJlZCkpe3JldHVybjt9XG5pZih0eXBlLmluZGV4T2YoXCIuXCIpPi0xKXtuYW1lc3BhY2VzPXR5cGUuc3BsaXQoXCIuXCIpO3R5cGU9bmFtZXNwYWNlcy5zaGlmdCgpO25hbWVzcGFjZXMuc29ydCgpO31cbm9udHlwZT10eXBlLmluZGV4T2YoXCI6XCIpPDAmJlwib25cIit0eXBlO2V2ZW50PWV2ZW50W2pRdWVyeS5leHBhbmRvXT9ldmVudDpuZXcgalF1ZXJ5LkV2ZW50KHR5cGUsdHlwZW9mIGV2ZW50PT09XCJvYmplY3RcIiYmZXZlbnQpO2V2ZW50LmlzVHJpZ2dlcj1vbmx5SGFuZGxlcnM/MjozO2V2ZW50Lm5hbWVzcGFjZT1uYW1lc3BhY2VzLmpvaW4oXCIuXCIpO2V2ZW50LnJuYW1lc3BhY2U9ZXZlbnQubmFtZXNwYWNlP25ldyBSZWdFeHAoXCIoXnxcXFxcLilcIituYW1lc3BhY2VzLmpvaW4oXCJcXFxcLig/Oi4qXFxcXC58KVwiKStcIihcXFxcLnwkKVwiKTpudWxsO2V2ZW50LnJlc3VsdD11bmRlZmluZWQ7aWYoIWV2ZW50LnRhcmdldCl7ZXZlbnQudGFyZ2V0PWVsZW07fVxuZGF0YT1kYXRhPT1udWxsP1tldmVudF06alF1ZXJ5Lm1ha2VBcnJheShkYXRhLFtldmVudF0pO3NwZWNpYWw9alF1ZXJ5LmV2ZW50LnNwZWNpYWxbdHlwZV18fHt9O2lmKCFvbmx5SGFuZGxlcnMmJnNwZWNpYWwudHJpZ2dlciYmc3BlY2lhbC50cmlnZ2VyLmFwcGx5KGVsZW0sZGF0YSk9PT1mYWxzZSl7cmV0dXJuO31cbmlmKCFvbmx5SGFuZGxlcnMmJiFzcGVjaWFsLm5vQnViYmxlJiYhaXNXaW5kb3coZWxlbSkpe2J1YmJsZVR5cGU9c3BlY2lhbC5kZWxlZ2F0ZVR5cGV8fHR5cGU7aWYoIXJmb2N1c01vcnBoLnRlc3QoYnViYmxlVHlwZSt0eXBlKSl7Y3VyPWN1ci5wYXJlbnROb2RlO31cbmZvcig7Y3VyO2N1cj1jdXIucGFyZW50Tm9kZSl7ZXZlbnRQYXRoLnB1c2goY3VyKTt0bXA9Y3VyO31cbmlmKHRtcD09PShlbGVtLm93bmVyRG9jdW1lbnR8fGRvY3VtZW50KSl7ZXZlbnRQYXRoLnB1c2godG1wLmRlZmF1bHRWaWV3fHx0bXAucGFyZW50V2luZG93fHx3aW5kb3cpO319XG5pPTA7d2hpbGUoKGN1cj1ldmVudFBhdGhbaSsrXSkmJiFldmVudC5pc1Byb3BhZ2F0aW9uU3RvcHBlZCgpKXtsYXN0RWxlbWVudD1jdXI7ZXZlbnQudHlwZT1pPjE/YnViYmxlVHlwZTpzcGVjaWFsLmJpbmRUeXBlfHx0eXBlO2hhbmRsZT0oZGF0YVByaXYuZ2V0KGN1cixcImV2ZW50c1wiKXx8T2JqZWN0LmNyZWF0ZShudWxsKSlbZXZlbnQudHlwZV0mJmRhdGFQcml2LmdldChjdXIsXCJoYW5kbGVcIik7aWYoaGFuZGxlKXtoYW5kbGUuYXBwbHkoY3VyLGRhdGEpO31cbmhhbmRsZT1vbnR5cGUmJmN1cltvbnR5cGVdO2lmKGhhbmRsZSYmaGFuZGxlLmFwcGx5JiZhY2NlcHREYXRhKGN1cikpe2V2ZW50LnJlc3VsdD1oYW5kbGUuYXBwbHkoY3VyLGRhdGEpO2lmKGV2ZW50LnJlc3VsdD09PWZhbHNlKXtldmVudC5wcmV2ZW50RGVmYXVsdCgpO319fVxuZXZlbnQudHlwZT10eXBlO2lmKCFvbmx5SGFuZGxlcnMmJiFldmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSl7aWYoKCFzcGVjaWFsLl9kZWZhdWx0fHxzcGVjaWFsLl9kZWZhdWx0LmFwcGx5KGV2ZW50UGF0aC5wb3AoKSxkYXRhKT09PWZhbHNlKSYmYWNjZXB0RGF0YShlbGVtKSl7aWYob250eXBlJiZpc0Z1bmN0aW9uKGVsZW1bdHlwZV0pJiYhaXNXaW5kb3coZWxlbSkpe3RtcD1lbGVtW29udHlwZV07aWYodG1wKXtlbGVtW29udHlwZV09bnVsbDt9XG5qUXVlcnkuZXZlbnQudHJpZ2dlcmVkPXR5cGU7aWYoZXZlbnQuaXNQcm9wYWdhdGlvblN0b3BwZWQoKSl7bGFzdEVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcih0eXBlLHN0b3BQcm9wYWdhdGlvbkNhbGxiYWNrKTt9XG5lbGVtW3R5cGVdKCk7aWYoZXZlbnQuaXNQcm9wYWdhdGlvblN0b3BwZWQoKSl7bGFzdEVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcih0eXBlLHN0b3BQcm9wYWdhdGlvbkNhbGxiYWNrKTt9XG5qUXVlcnkuZXZlbnQudHJpZ2dlcmVkPXVuZGVmaW5lZDtpZih0bXApe2VsZW1bb250eXBlXT10bXA7fX19fVxucmV0dXJuIGV2ZW50LnJlc3VsdDt9LHNpbXVsYXRlOmZ1bmN0aW9uKHR5cGUsZWxlbSxldmVudCl7dmFyIGU9alF1ZXJ5LmV4dGVuZChuZXcgalF1ZXJ5LkV2ZW50KCksZXZlbnQse3R5cGU6dHlwZSxpc1NpbXVsYXRlZDp0cnVlfSk7alF1ZXJ5LmV2ZW50LnRyaWdnZXIoZSxudWxsLGVsZW0pO319KTtqUXVlcnkuZm4uZXh0ZW5kKHt0cmlnZ2VyOmZ1bmN0aW9uKHR5cGUsZGF0YSl7cmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbigpe2pRdWVyeS5ldmVudC50cmlnZ2VyKHR5cGUsZGF0YSx0aGlzKTt9KTt9LHRyaWdnZXJIYW5kbGVyOmZ1bmN0aW9uKHR5cGUsZGF0YSl7dmFyIGVsZW09dGhpc1swXTtpZihlbGVtKXtyZXR1cm4galF1ZXJ5LmV2ZW50LnRyaWdnZXIodHlwZSxkYXRhLGVsZW0sdHJ1ZSk7fX19KTtpZighc3VwcG9ydC5mb2N1c2luKXtqUXVlcnkuZWFjaCh7Zm9jdXM6XCJmb2N1c2luXCIsYmx1cjpcImZvY3Vzb3V0XCJ9LGZ1bmN0aW9uKG9yaWcsZml4KXt2YXIgaGFuZGxlcj1mdW5jdGlvbihldmVudCl7alF1ZXJ5LmV2ZW50LnNpbXVsYXRlKGZpeCxldmVudC50YXJnZXQsalF1ZXJ5LmV2ZW50LmZpeChldmVudCkpO307alF1ZXJ5LmV2ZW50LnNwZWNpYWxbZml4XT17c2V0dXA6ZnVuY3Rpb24oKXt2YXIgZG9jPXRoaXMub3duZXJEb2N1bWVudHx8dGhpcy5kb2N1bWVudHx8dGhpcyxhdHRhY2hlcz1kYXRhUHJpdi5hY2Nlc3MoZG9jLGZpeCk7aWYoIWF0dGFjaGVzKXtkb2MuYWRkRXZlbnRMaXN0ZW5lcihvcmlnLGhhbmRsZXIsdHJ1ZSk7fVxuZGF0YVByaXYuYWNjZXNzKGRvYyxmaXgsKGF0dGFjaGVzfHwwKSsxKTt9LHRlYXJkb3duOmZ1bmN0aW9uKCl7dmFyIGRvYz10aGlzLm93bmVyRG9jdW1lbnR8fHRoaXMuZG9jdW1lbnR8fHRoaXMsYXR0YWNoZXM9ZGF0YVByaXYuYWNjZXNzKGRvYyxmaXgpLTE7aWYoIWF0dGFjaGVzKXtkb2MucmVtb3ZlRXZlbnRMaXN0ZW5lcihvcmlnLGhhbmRsZXIsdHJ1ZSk7ZGF0YVByaXYucmVtb3ZlKGRvYyxmaXgpO31lbHNle2RhdGFQcml2LmFjY2Vzcyhkb2MsZml4LGF0dGFjaGVzKTt9fX07fSk7fVxudmFyIGxvY2F0aW9uPXdpbmRvdy5sb2NhdGlvbjt2YXIgbm9uY2U9e2d1aWQ6RGF0ZS5ub3coKX07dmFyIHJxdWVyeT0oL1xcPy8pO2pRdWVyeS5wYXJzZVhNTD1mdW5jdGlvbihkYXRhKXt2YXIgeG1sO2lmKCFkYXRhfHx0eXBlb2YgZGF0YSE9PVwic3RyaW5nXCIpe3JldHVybiBudWxsO31cbnRyeXt4bWw9KG5ldyB3aW5kb3cuRE9NUGFyc2VyKCkpLnBhcnNlRnJvbVN0cmluZyhkYXRhLFwidGV4dC94bWxcIik7fWNhdGNoKGUpe3htbD11bmRlZmluZWQ7fVxuaWYoIXhtbHx8eG1sLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwicGFyc2VyZXJyb3JcIikubGVuZ3RoKXtqUXVlcnkuZXJyb3IoXCJJbnZhbGlkIFhNTDogXCIrZGF0YSk7fVxucmV0dXJuIHhtbDt9O3ZhclxucmJyYWNrZXQ9L1xcW1xcXSQvLHJDUkxGPS9cXHI/XFxuL2cscnN1Ym1pdHRlclR5cGVzPS9eKD86c3VibWl0fGJ1dHRvbnxpbWFnZXxyZXNldHxmaWxlKSQvaSxyc3VibWl0dGFibGU9L14oPzppbnB1dHxzZWxlY3R8dGV4dGFyZWF8a2V5Z2VuKS9pO2Z1bmN0aW9uIGJ1aWxkUGFyYW1zKHByZWZpeCxvYmosdHJhZGl0aW9uYWwsYWRkKXt2YXIgbmFtZTtpZihBcnJheS5pc0FycmF5KG9iaikpe2pRdWVyeS5lYWNoKG9iaixmdW5jdGlvbihpLHYpe2lmKHRyYWRpdGlvbmFsfHxyYnJhY2tldC50ZXN0KHByZWZpeCkpe2FkZChwcmVmaXgsdik7fWVsc2V7YnVpbGRQYXJhbXMocHJlZml4K1wiW1wiKyh0eXBlb2Ygdj09PVwib2JqZWN0XCImJnYhPW51bGw/aTpcIlwiKStcIl1cIix2LHRyYWRpdGlvbmFsLGFkZCk7fX0pO31lbHNlIGlmKCF0cmFkaXRpb25hbCYmdG9UeXBlKG9iaik9PT1cIm9iamVjdFwiKXtmb3IobmFtZSBpbiBvYmope2J1aWxkUGFyYW1zKHByZWZpeCtcIltcIituYW1lK1wiXVwiLG9ialtuYW1lXSx0cmFkaXRpb25hbCxhZGQpO319ZWxzZXthZGQocHJlZml4LG9iaik7fX1cbmpRdWVyeS5wYXJhbT1mdW5jdGlvbihhLHRyYWRpdGlvbmFsKXt2YXIgcHJlZml4LHM9W10sYWRkPWZ1bmN0aW9uKGtleSx2YWx1ZU9yRnVuY3Rpb24pe3ZhciB2YWx1ZT1pc0Z1bmN0aW9uKHZhbHVlT3JGdW5jdGlvbik/dmFsdWVPckZ1bmN0aW9uKCk6dmFsdWVPckZ1bmN0aW9uO3Nbcy5sZW5ndGhdPWVuY29kZVVSSUNvbXBvbmVudChrZXkpK1wiPVwiK1xuZW5jb2RlVVJJQ29tcG9uZW50KHZhbHVlPT1udWxsP1wiXCI6dmFsdWUpO307aWYoYT09bnVsbCl7cmV0dXJuXCJcIjt9XG5pZihBcnJheS5pc0FycmF5KGEpfHwoYS5qcXVlcnkmJiFqUXVlcnkuaXNQbGFpbk9iamVjdChhKSkpe2pRdWVyeS5lYWNoKGEsZnVuY3Rpb24oKXthZGQodGhpcy5uYW1lLHRoaXMudmFsdWUpO30pO31lbHNle2ZvcihwcmVmaXggaW4gYSl7YnVpbGRQYXJhbXMocHJlZml4LGFbcHJlZml4XSx0cmFkaXRpb25hbCxhZGQpO319XG5yZXR1cm4gcy5qb2luKFwiJlwiKTt9O2pRdWVyeS5mbi5leHRlbmQoe3NlcmlhbGl6ZTpmdW5jdGlvbigpe3JldHVybiBqUXVlcnkucGFyYW0odGhpcy5zZXJpYWxpemVBcnJheSgpKTt9LHNlcmlhbGl6ZUFycmF5OmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMubWFwKGZ1bmN0aW9uKCl7dmFyIGVsZW1lbnRzPWpRdWVyeS5wcm9wKHRoaXMsXCJlbGVtZW50c1wiKTtyZXR1cm4gZWxlbWVudHM/alF1ZXJ5Lm1ha2VBcnJheShlbGVtZW50cyk6dGhpczt9KS5maWx0ZXIoZnVuY3Rpb24oKXt2YXIgdHlwZT10aGlzLnR5cGU7cmV0dXJuIHRoaXMubmFtZSYmIWpRdWVyeSh0aGlzKS5pcyhcIjpkaXNhYmxlZFwiKSYmcnN1Ym1pdHRhYmxlLnRlc3QodGhpcy5ub2RlTmFtZSkmJiFyc3VibWl0dGVyVHlwZXMudGVzdCh0eXBlKSYmKHRoaXMuY2hlY2tlZHx8IXJjaGVja2FibGVUeXBlLnRlc3QodHlwZSkpO30pLm1hcChmdW5jdGlvbihfaSxlbGVtKXt2YXIgdmFsPWpRdWVyeSh0aGlzKS52YWwoKTtpZih2YWw9PW51bGwpe3JldHVybiBudWxsO31cbmlmKEFycmF5LmlzQXJyYXkodmFsKSl7cmV0dXJuIGpRdWVyeS5tYXAodmFsLGZ1bmN0aW9uKHZhbCl7cmV0dXJue25hbWU6ZWxlbS5uYW1lLHZhbHVlOnZhbC5yZXBsYWNlKHJDUkxGLFwiXFxyXFxuXCIpfTt9KTt9XG5yZXR1cm57bmFtZTplbGVtLm5hbWUsdmFsdWU6dmFsLnJlcGxhY2UockNSTEYsXCJcXHJcXG5cIil9O30pLmdldCgpO319KTt2YXJcbnIyMD0vJTIwL2cscmhhc2g9LyMuKiQvLHJhbnRpQ2FjaGU9LyhbPyZdKV89W14mXSovLHJoZWFkZXJzPS9eKC4qPyk6WyBcXHRdKihbXlxcclxcbl0qKSQvbWcscmxvY2FsUHJvdG9jb2w9L14oPzphYm91dHxhcHB8YXBwLXN0b3JhZ2V8ListZXh0ZW5zaW9ufGZpbGV8cmVzfHdpZGdldCk6JC8scm5vQ29udGVudD0vXig/OkdFVHxIRUFEKSQvLHJwcm90b2NvbD0vXlxcL1xcLy8scHJlZmlsdGVycz17fSx0cmFuc3BvcnRzPXt9LGFsbFR5cGVzPVwiKi9cIi5jb25jYXQoXCIqXCIpLG9yaWdpbkFuY2hvcj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiYVwiKTtvcmlnaW5BbmNob3IuaHJlZj1sb2NhdGlvbi5ocmVmO2Z1bmN0aW9uIGFkZFRvUHJlZmlsdGVyc09yVHJhbnNwb3J0cyhzdHJ1Y3R1cmUpe3JldHVybiBmdW5jdGlvbihkYXRhVHlwZUV4cHJlc3Npb24sZnVuYyl7aWYodHlwZW9mIGRhdGFUeXBlRXhwcmVzc2lvbiE9PVwic3RyaW5nXCIpe2Z1bmM9ZGF0YVR5cGVFeHByZXNzaW9uO2RhdGFUeXBlRXhwcmVzc2lvbj1cIipcIjt9XG52YXIgZGF0YVR5cGUsaT0wLGRhdGFUeXBlcz1kYXRhVHlwZUV4cHJlc3Npb24udG9Mb3dlckNhc2UoKS5tYXRjaChybm90aHRtbHdoaXRlKXx8W107aWYoaXNGdW5jdGlvbihmdW5jKSl7d2hpbGUoKGRhdGFUeXBlPWRhdGFUeXBlc1tpKytdKSl7aWYoZGF0YVR5cGVbMF09PT1cIitcIil7ZGF0YVR5cGU9ZGF0YVR5cGUuc2xpY2UoMSl8fFwiKlwiOyhzdHJ1Y3R1cmVbZGF0YVR5cGVdPXN0cnVjdHVyZVtkYXRhVHlwZV18fFtdKS51bnNoaWZ0KGZ1bmMpO31lbHNleyhzdHJ1Y3R1cmVbZGF0YVR5cGVdPXN0cnVjdHVyZVtkYXRhVHlwZV18fFtdKS5wdXNoKGZ1bmMpO319fX07fVxuZnVuY3Rpb24gaW5zcGVjdFByZWZpbHRlcnNPclRyYW5zcG9ydHMoc3RydWN0dXJlLG9wdGlvbnMsb3JpZ2luYWxPcHRpb25zLGpxWEhSKXt2YXIgaW5zcGVjdGVkPXt9LHNlZWtpbmdUcmFuc3BvcnQ9KHN0cnVjdHVyZT09PXRyYW5zcG9ydHMpO2Z1bmN0aW9uIGluc3BlY3QoZGF0YVR5cGUpe3ZhciBzZWxlY3RlZDtpbnNwZWN0ZWRbZGF0YVR5cGVdPXRydWU7alF1ZXJ5LmVhY2goc3RydWN0dXJlW2RhdGFUeXBlXXx8W10sZnVuY3Rpb24oXyxwcmVmaWx0ZXJPckZhY3Rvcnkpe3ZhciBkYXRhVHlwZU9yVHJhbnNwb3J0PXByZWZpbHRlck9yRmFjdG9yeShvcHRpb25zLG9yaWdpbmFsT3B0aW9ucyxqcVhIUik7aWYodHlwZW9mIGRhdGFUeXBlT3JUcmFuc3BvcnQ9PT1cInN0cmluZ1wiJiYhc2Vla2luZ1RyYW5zcG9ydCYmIWluc3BlY3RlZFtkYXRhVHlwZU9yVHJhbnNwb3J0XSl7b3B0aW9ucy5kYXRhVHlwZXMudW5zaGlmdChkYXRhVHlwZU9yVHJhbnNwb3J0KTtpbnNwZWN0KGRhdGFUeXBlT3JUcmFuc3BvcnQpO3JldHVybiBmYWxzZTt9ZWxzZSBpZihzZWVraW5nVHJhbnNwb3J0KXtyZXR1cm4hKHNlbGVjdGVkPWRhdGFUeXBlT3JUcmFuc3BvcnQpO319KTtyZXR1cm4gc2VsZWN0ZWQ7fVxucmV0dXJuIGluc3BlY3Qob3B0aW9ucy5kYXRhVHlwZXNbMF0pfHwhaW5zcGVjdGVkW1wiKlwiXSYmaW5zcGVjdChcIipcIik7fVxuZnVuY3Rpb24gYWpheEV4dGVuZCh0YXJnZXQsc3JjKXt2YXIga2V5LGRlZXAsZmxhdE9wdGlvbnM9alF1ZXJ5LmFqYXhTZXR0aW5ncy5mbGF0T3B0aW9uc3x8e307Zm9yKGtleSBpbiBzcmMpe2lmKHNyY1trZXldIT09dW5kZWZpbmVkKXsoZmxhdE9wdGlvbnNba2V5XT90YXJnZXQ6KGRlZXB8fChkZWVwPXt9KSkpW2tleV09c3JjW2tleV07fX1cbmlmKGRlZXApe2pRdWVyeS5leHRlbmQodHJ1ZSx0YXJnZXQsZGVlcCk7fVxucmV0dXJuIHRhcmdldDt9XG5mdW5jdGlvbiBhamF4SGFuZGxlUmVzcG9uc2VzKHMsanFYSFIscmVzcG9uc2VzKXt2YXIgY3QsdHlwZSxmaW5hbERhdGFUeXBlLGZpcnN0RGF0YVR5cGUsY29udGVudHM9cy5jb250ZW50cyxkYXRhVHlwZXM9cy5kYXRhVHlwZXM7d2hpbGUoZGF0YVR5cGVzWzBdPT09XCIqXCIpe2RhdGFUeXBlcy5zaGlmdCgpO2lmKGN0PT09dW5kZWZpbmVkKXtjdD1zLm1pbWVUeXBlfHxqcVhIUi5nZXRSZXNwb25zZUhlYWRlcihcIkNvbnRlbnQtVHlwZVwiKTt9fVxuaWYoY3Qpe2Zvcih0eXBlIGluIGNvbnRlbnRzKXtpZihjb250ZW50c1t0eXBlXSYmY29udGVudHNbdHlwZV0udGVzdChjdCkpe2RhdGFUeXBlcy51bnNoaWZ0KHR5cGUpO2JyZWFrO319fVxuaWYoZGF0YVR5cGVzWzBdaW4gcmVzcG9uc2VzKXtmaW5hbERhdGFUeXBlPWRhdGFUeXBlc1swXTt9ZWxzZXtmb3IodHlwZSBpbiByZXNwb25zZXMpe2lmKCFkYXRhVHlwZXNbMF18fHMuY29udmVydGVyc1t0eXBlK1wiIFwiK2RhdGFUeXBlc1swXV0pe2ZpbmFsRGF0YVR5cGU9dHlwZTticmVhazt9XG5pZighZmlyc3REYXRhVHlwZSl7Zmlyc3REYXRhVHlwZT10eXBlO319XG5maW5hbERhdGFUeXBlPWZpbmFsRGF0YVR5cGV8fGZpcnN0RGF0YVR5cGU7fVxuaWYoZmluYWxEYXRhVHlwZSl7aWYoZmluYWxEYXRhVHlwZSE9PWRhdGFUeXBlc1swXSl7ZGF0YVR5cGVzLnVuc2hpZnQoZmluYWxEYXRhVHlwZSk7fVxucmV0dXJuIHJlc3BvbnNlc1tmaW5hbERhdGFUeXBlXTt9fVxuZnVuY3Rpb24gYWpheENvbnZlcnQocyxyZXNwb25zZSxqcVhIUixpc1N1Y2Nlc3Mpe3ZhciBjb252MixjdXJyZW50LGNvbnYsdG1wLHByZXYsY29udmVydGVycz17fSxkYXRhVHlwZXM9cy5kYXRhVHlwZXMuc2xpY2UoKTtpZihkYXRhVHlwZXNbMV0pe2Zvcihjb252IGluIHMuY29udmVydGVycyl7Y29udmVydGVyc1tjb252LnRvTG93ZXJDYXNlKCldPXMuY29udmVydGVyc1tjb252XTt9fVxuY3VycmVudD1kYXRhVHlwZXMuc2hpZnQoKTt3aGlsZShjdXJyZW50KXtpZihzLnJlc3BvbnNlRmllbGRzW2N1cnJlbnRdKXtqcVhIUltzLnJlc3BvbnNlRmllbGRzW2N1cnJlbnRdXT1yZXNwb25zZTt9XG5pZighcHJldiYmaXNTdWNjZXNzJiZzLmRhdGFGaWx0ZXIpe3Jlc3BvbnNlPXMuZGF0YUZpbHRlcihyZXNwb25zZSxzLmRhdGFUeXBlKTt9XG5wcmV2PWN1cnJlbnQ7Y3VycmVudD1kYXRhVHlwZXMuc2hpZnQoKTtpZihjdXJyZW50KXtpZihjdXJyZW50PT09XCIqXCIpe2N1cnJlbnQ9cHJldjt9ZWxzZSBpZihwcmV2IT09XCIqXCImJnByZXYhPT1jdXJyZW50KXtjb252PWNvbnZlcnRlcnNbcHJlditcIiBcIitjdXJyZW50XXx8Y29udmVydGVyc1tcIiogXCIrY3VycmVudF07aWYoIWNvbnYpe2Zvcihjb252MiBpbiBjb252ZXJ0ZXJzKXt0bXA9Y29udjIuc3BsaXQoXCIgXCIpO2lmKHRtcFsxXT09PWN1cnJlbnQpe2NvbnY9Y29udmVydGVyc1twcmV2K1wiIFwiK3RtcFswXV18fGNvbnZlcnRlcnNbXCIqIFwiK3RtcFswXV07aWYoY29udil7aWYoY29udj09PXRydWUpe2NvbnY9Y29udmVydGVyc1tjb252Ml07fWVsc2UgaWYoY29udmVydGVyc1tjb252Ml0hPT10cnVlKXtjdXJyZW50PXRtcFswXTtkYXRhVHlwZXMudW5zaGlmdCh0bXBbMV0pO31cbmJyZWFrO319fX1cbmlmKGNvbnYhPT10cnVlKXtpZihjb252JiZzLnRocm93cyl7cmVzcG9uc2U9Y29udihyZXNwb25zZSk7fWVsc2V7dHJ5e3Jlc3BvbnNlPWNvbnYocmVzcG9uc2UpO31jYXRjaChlKXtyZXR1cm57c3RhdGU6XCJwYXJzZXJlcnJvclwiLGVycm9yOmNvbnY/ZTpcIk5vIGNvbnZlcnNpb24gZnJvbSBcIitwcmV2K1wiIHRvIFwiK2N1cnJlbnR9O319fX19fVxucmV0dXJue3N0YXRlOlwic3VjY2Vzc1wiLGRhdGE6cmVzcG9uc2V9O31cbmpRdWVyeS5leHRlbmQoe2FjdGl2ZTowLGxhc3RNb2RpZmllZDp7fSxldGFnOnt9LGFqYXhTZXR0aW5nczp7dXJsOmxvY2F0aW9uLmhyZWYsdHlwZTpcIkdFVFwiLGlzTG9jYWw6cmxvY2FsUHJvdG9jb2wudGVzdChsb2NhdGlvbi5wcm90b2NvbCksZ2xvYmFsOnRydWUscHJvY2Vzc0RhdGE6dHJ1ZSxhc3luYzp0cnVlLGNvbnRlbnRUeXBlOlwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkOyBjaGFyc2V0PVVURi04XCIsYWNjZXB0czp7XCIqXCI6YWxsVHlwZXMsdGV4dDpcInRleHQvcGxhaW5cIixodG1sOlwidGV4dC9odG1sXCIseG1sOlwiYXBwbGljYXRpb24veG1sLCB0ZXh0L3htbFwiLGpzb246XCJhcHBsaWNhdGlvbi9qc29uLCB0ZXh0L2phdmFzY3JpcHRcIn0sY29udGVudHM6e3htbDovXFxieG1sXFxiLyxodG1sOi9cXGJodG1sLyxqc29uOi9cXGJqc29uXFxiL30scmVzcG9uc2VGaWVsZHM6e3htbDpcInJlc3BvbnNlWE1MXCIsdGV4dDpcInJlc3BvbnNlVGV4dFwiLGpzb246XCJyZXNwb25zZUpTT05cIn0sY29udmVydGVyczp7XCIqIHRleHRcIjpTdHJpbmcsXCJ0ZXh0IGh0bWxcIjp0cnVlLFwidGV4dCBqc29uXCI6SlNPTi5wYXJzZSxcInRleHQgeG1sXCI6alF1ZXJ5LnBhcnNlWE1MfSxmbGF0T3B0aW9uczp7dXJsOnRydWUsY29udGV4dDp0cnVlfX0sYWpheFNldHVwOmZ1bmN0aW9uKHRhcmdldCxzZXR0aW5ncyl7cmV0dXJuIHNldHRpbmdzP2FqYXhFeHRlbmQoYWpheEV4dGVuZCh0YXJnZXQsalF1ZXJ5LmFqYXhTZXR0aW5ncyksc2V0dGluZ3MpOmFqYXhFeHRlbmQoalF1ZXJ5LmFqYXhTZXR0aW5ncyx0YXJnZXQpO30sYWpheFByZWZpbHRlcjphZGRUb1ByZWZpbHRlcnNPclRyYW5zcG9ydHMocHJlZmlsdGVycyksYWpheFRyYW5zcG9ydDphZGRUb1ByZWZpbHRlcnNPclRyYW5zcG9ydHModHJhbnNwb3J0cyksYWpheDpmdW5jdGlvbih1cmwsb3B0aW9ucyl7aWYodHlwZW9mIHVybD09PVwib2JqZWN0XCIpe29wdGlvbnM9dXJsO3VybD11bmRlZmluZWQ7fVxub3B0aW9ucz1vcHRpb25zfHx7fTt2YXIgdHJhbnNwb3J0LGNhY2hlVVJMLHJlc3BvbnNlSGVhZGVyc1N0cmluZyxyZXNwb25zZUhlYWRlcnMsdGltZW91dFRpbWVyLHVybEFuY2hvcixjb21wbGV0ZWQsZmlyZUdsb2JhbHMsaSx1bmNhY2hlZCxzPWpRdWVyeS5hamF4U2V0dXAoe30sb3B0aW9ucyksY2FsbGJhY2tDb250ZXh0PXMuY29udGV4dHx8cyxnbG9iYWxFdmVudENvbnRleHQ9cy5jb250ZXh0JiYoY2FsbGJhY2tDb250ZXh0Lm5vZGVUeXBlfHxjYWxsYmFja0NvbnRleHQuanF1ZXJ5KT9qUXVlcnkoY2FsbGJhY2tDb250ZXh0KTpqUXVlcnkuZXZlbnQsZGVmZXJyZWQ9alF1ZXJ5LkRlZmVycmVkKCksY29tcGxldGVEZWZlcnJlZD1qUXVlcnkuQ2FsbGJhY2tzKFwib25jZSBtZW1vcnlcIiksc3RhdHVzQ29kZT1zLnN0YXR1c0NvZGV8fHt9LHJlcXVlc3RIZWFkZXJzPXt9LHJlcXVlc3RIZWFkZXJzTmFtZXM9e30sc3RyQWJvcnQ9XCJjYW5jZWxlZFwiLGpxWEhSPXtyZWFkeVN0YXRlOjAsZ2V0UmVzcG9uc2VIZWFkZXI6ZnVuY3Rpb24oa2V5KXt2YXIgbWF0Y2g7aWYoY29tcGxldGVkKXtpZighcmVzcG9uc2VIZWFkZXJzKXtyZXNwb25zZUhlYWRlcnM9e307d2hpbGUoKG1hdGNoPXJoZWFkZXJzLmV4ZWMocmVzcG9uc2VIZWFkZXJzU3RyaW5nKSkpe3Jlc3BvbnNlSGVhZGVyc1ttYXRjaFsxXS50b0xvd2VyQ2FzZSgpK1wiIFwiXT0ocmVzcG9uc2VIZWFkZXJzW21hdGNoWzFdLnRvTG93ZXJDYXNlKCkrXCIgXCJdfHxbXSkuY29uY2F0KG1hdGNoWzJdKTt9fVxubWF0Y2g9cmVzcG9uc2VIZWFkZXJzW2tleS50b0xvd2VyQ2FzZSgpK1wiIFwiXTt9XG5yZXR1cm4gbWF0Y2g9PW51bGw/bnVsbDptYXRjaC5qb2luKFwiLCBcIik7fSxnZXRBbGxSZXNwb25zZUhlYWRlcnM6ZnVuY3Rpb24oKXtyZXR1cm4gY29tcGxldGVkP3Jlc3BvbnNlSGVhZGVyc1N0cmluZzpudWxsO30sc2V0UmVxdWVzdEhlYWRlcjpmdW5jdGlvbihuYW1lLHZhbHVlKXtpZihjb21wbGV0ZWQ9PW51bGwpe25hbWU9cmVxdWVzdEhlYWRlcnNOYW1lc1tuYW1lLnRvTG93ZXJDYXNlKCldPXJlcXVlc3RIZWFkZXJzTmFtZXNbbmFtZS50b0xvd2VyQ2FzZSgpXXx8bmFtZTtyZXF1ZXN0SGVhZGVyc1tuYW1lXT12YWx1ZTt9XG5yZXR1cm4gdGhpczt9LG92ZXJyaWRlTWltZVR5cGU6ZnVuY3Rpb24odHlwZSl7aWYoY29tcGxldGVkPT1udWxsKXtzLm1pbWVUeXBlPXR5cGU7fVxucmV0dXJuIHRoaXM7fSxzdGF0dXNDb2RlOmZ1bmN0aW9uKG1hcCl7dmFyIGNvZGU7aWYobWFwKXtpZihjb21wbGV0ZWQpe2pxWEhSLmFsd2F5cyhtYXBbanFYSFIuc3RhdHVzXSk7fWVsc2V7Zm9yKGNvZGUgaW4gbWFwKXtzdGF0dXNDb2RlW2NvZGVdPVtzdGF0dXNDb2RlW2NvZGVdLG1hcFtjb2RlXV07fX19XG5yZXR1cm4gdGhpczt9LGFib3J0OmZ1bmN0aW9uKHN0YXR1c1RleHQpe3ZhciBmaW5hbFRleHQ9c3RhdHVzVGV4dHx8c3RyQWJvcnQ7aWYodHJhbnNwb3J0KXt0cmFuc3BvcnQuYWJvcnQoZmluYWxUZXh0KTt9XG5kb25lKDAsZmluYWxUZXh0KTtyZXR1cm4gdGhpczt9fTtkZWZlcnJlZC5wcm9taXNlKGpxWEhSKTtzLnVybD0oKHVybHx8cy51cmx8fGxvY2F0aW9uLmhyZWYpK1wiXCIpLnJlcGxhY2UocnByb3RvY29sLGxvY2F0aW9uLnByb3RvY29sK1wiLy9cIik7cy50eXBlPW9wdGlvbnMubWV0aG9kfHxvcHRpb25zLnR5cGV8fHMubWV0aG9kfHxzLnR5cGU7cy5kYXRhVHlwZXM9KHMuZGF0YVR5cGV8fFwiKlwiKS50b0xvd2VyQ2FzZSgpLm1hdGNoKHJub3RodG1sd2hpdGUpfHxbXCJcIl07aWYocy5jcm9zc0RvbWFpbj09bnVsbCl7dXJsQW5jaG9yPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJhXCIpO3RyeXt1cmxBbmNob3IuaHJlZj1zLnVybDt1cmxBbmNob3IuaHJlZj11cmxBbmNob3IuaHJlZjtzLmNyb3NzRG9tYWluPW9yaWdpbkFuY2hvci5wcm90b2NvbCtcIi8vXCIrb3JpZ2luQW5jaG9yLmhvc3QhPT11cmxBbmNob3IucHJvdG9jb2wrXCIvL1wiK3VybEFuY2hvci5ob3N0O31jYXRjaChlKXtzLmNyb3NzRG9tYWluPXRydWU7fX1cbmlmKHMuZGF0YSYmcy5wcm9jZXNzRGF0YSYmdHlwZW9mIHMuZGF0YSE9PVwic3RyaW5nXCIpe3MuZGF0YT1qUXVlcnkucGFyYW0ocy5kYXRhLHMudHJhZGl0aW9uYWwpO31cbmluc3BlY3RQcmVmaWx0ZXJzT3JUcmFuc3BvcnRzKHByZWZpbHRlcnMscyxvcHRpb25zLGpxWEhSKTtpZihjb21wbGV0ZWQpe3JldHVybiBqcVhIUjt9XG5maXJlR2xvYmFscz1qUXVlcnkuZXZlbnQmJnMuZ2xvYmFsO2lmKGZpcmVHbG9iYWxzJiZqUXVlcnkuYWN0aXZlKys9PT0wKXtqUXVlcnkuZXZlbnQudHJpZ2dlcihcImFqYXhTdGFydFwiKTt9XG5zLnR5cGU9cy50eXBlLnRvVXBwZXJDYXNlKCk7cy5oYXNDb250ZW50PSFybm9Db250ZW50LnRlc3Qocy50eXBlKTtjYWNoZVVSTD1zLnVybC5yZXBsYWNlKHJoYXNoLFwiXCIpO2lmKCFzLmhhc0NvbnRlbnQpe3VuY2FjaGVkPXMudXJsLnNsaWNlKGNhY2hlVVJMLmxlbmd0aCk7aWYocy5kYXRhJiYocy5wcm9jZXNzRGF0YXx8dHlwZW9mIHMuZGF0YT09PVwic3RyaW5nXCIpKXtjYWNoZVVSTCs9KHJxdWVyeS50ZXN0KGNhY2hlVVJMKT9cIiZcIjpcIj9cIikrcy5kYXRhO2RlbGV0ZSBzLmRhdGE7fVxuaWYocy5jYWNoZT09PWZhbHNlKXtjYWNoZVVSTD1jYWNoZVVSTC5yZXBsYWNlKHJhbnRpQ2FjaGUsXCIkMVwiKTt1bmNhY2hlZD0ocnF1ZXJ5LnRlc3QoY2FjaGVVUkwpP1wiJlwiOlwiP1wiKStcIl89XCIrKG5vbmNlLmd1aWQrKykrXG51bmNhY2hlZDt9XG5zLnVybD1jYWNoZVVSTCt1bmNhY2hlZDt9ZWxzZSBpZihzLmRhdGEmJnMucHJvY2Vzc0RhdGEmJihzLmNvbnRlbnRUeXBlfHxcIlwiKS5pbmRleE9mKFwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIpPT09MCl7cy5kYXRhPXMuZGF0YS5yZXBsYWNlKHIyMCxcIitcIik7fVxuaWYocy5pZk1vZGlmaWVkKXtpZihqUXVlcnkubGFzdE1vZGlmaWVkW2NhY2hlVVJMXSl7anFYSFIuc2V0UmVxdWVzdEhlYWRlcihcIklmLU1vZGlmaWVkLVNpbmNlXCIsalF1ZXJ5Lmxhc3RNb2RpZmllZFtjYWNoZVVSTF0pO31cbmlmKGpRdWVyeS5ldGFnW2NhY2hlVVJMXSl7anFYSFIuc2V0UmVxdWVzdEhlYWRlcihcIklmLU5vbmUtTWF0Y2hcIixqUXVlcnkuZXRhZ1tjYWNoZVVSTF0pO319XG5pZihzLmRhdGEmJnMuaGFzQ29udGVudCYmcy5jb250ZW50VHlwZSE9PWZhbHNlfHxvcHRpb25zLmNvbnRlbnRUeXBlKXtqcVhIUi5zZXRSZXF1ZXN0SGVhZGVyKFwiQ29udGVudC1UeXBlXCIscy5jb250ZW50VHlwZSk7fVxuanFYSFIuc2V0UmVxdWVzdEhlYWRlcihcIkFjY2VwdFwiLHMuZGF0YVR5cGVzWzBdJiZzLmFjY2VwdHNbcy5kYXRhVHlwZXNbMF1dP3MuYWNjZXB0c1tzLmRhdGFUeXBlc1swXV0rXG4ocy5kYXRhVHlwZXNbMF0hPT1cIipcIj9cIiwgXCIrYWxsVHlwZXMrXCI7IHE9MC4wMVwiOlwiXCIpOnMuYWNjZXB0c1tcIipcIl0pO2ZvcihpIGluIHMuaGVhZGVycyl7anFYSFIuc2V0UmVxdWVzdEhlYWRlcihpLHMuaGVhZGVyc1tpXSk7fVxuaWYocy5iZWZvcmVTZW5kJiYocy5iZWZvcmVTZW5kLmNhbGwoY2FsbGJhY2tDb250ZXh0LGpxWEhSLHMpPT09ZmFsc2V8fGNvbXBsZXRlZCkpe3JldHVybiBqcVhIUi5hYm9ydCgpO31cbnN0ckFib3J0PVwiYWJvcnRcIjtjb21wbGV0ZURlZmVycmVkLmFkZChzLmNvbXBsZXRlKTtqcVhIUi5kb25lKHMuc3VjY2Vzcyk7anFYSFIuZmFpbChzLmVycm9yKTt0cmFuc3BvcnQ9aW5zcGVjdFByZWZpbHRlcnNPclRyYW5zcG9ydHModHJhbnNwb3J0cyxzLG9wdGlvbnMsanFYSFIpO2lmKCF0cmFuc3BvcnQpe2RvbmUoLTEsXCJObyBUcmFuc3BvcnRcIik7fWVsc2V7anFYSFIucmVhZHlTdGF0ZT0xO2lmKGZpcmVHbG9iYWxzKXtnbG9iYWxFdmVudENvbnRleHQudHJpZ2dlcihcImFqYXhTZW5kXCIsW2pxWEhSLHNdKTt9XG5pZihjb21wbGV0ZWQpe3JldHVybiBqcVhIUjt9XG5pZihzLmFzeW5jJiZzLnRpbWVvdXQ+MCl7dGltZW91dFRpbWVyPXdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uKCl7anFYSFIuYWJvcnQoXCJ0aW1lb3V0XCIpO30scy50aW1lb3V0KTt9XG50cnl7Y29tcGxldGVkPWZhbHNlO3RyYW5zcG9ydC5zZW5kKHJlcXVlc3RIZWFkZXJzLGRvbmUpO31jYXRjaChlKXtpZihjb21wbGV0ZWQpe3Rocm93IGU7fVxuZG9uZSgtMSxlKTt9fVxuZnVuY3Rpb24gZG9uZShzdGF0dXMsbmF0aXZlU3RhdHVzVGV4dCxyZXNwb25zZXMsaGVhZGVycyl7dmFyIGlzU3VjY2VzcyxzdWNjZXNzLGVycm9yLHJlc3BvbnNlLG1vZGlmaWVkLHN0YXR1c1RleHQ9bmF0aXZlU3RhdHVzVGV4dDtpZihjb21wbGV0ZWQpe3JldHVybjt9XG5jb21wbGV0ZWQ9dHJ1ZTtpZih0aW1lb3V0VGltZXIpe3dpbmRvdy5jbGVhclRpbWVvdXQodGltZW91dFRpbWVyKTt9XG50cmFuc3BvcnQ9dW5kZWZpbmVkO3Jlc3BvbnNlSGVhZGVyc1N0cmluZz1oZWFkZXJzfHxcIlwiO2pxWEhSLnJlYWR5U3RhdGU9c3RhdHVzPjA/NDowO2lzU3VjY2Vzcz1zdGF0dXM+PTIwMCYmc3RhdHVzPDMwMHx8c3RhdHVzPT09MzA0O2lmKHJlc3BvbnNlcyl7cmVzcG9uc2U9YWpheEhhbmRsZVJlc3BvbnNlcyhzLGpxWEhSLHJlc3BvbnNlcyk7fVxuaWYoIWlzU3VjY2VzcyYmalF1ZXJ5LmluQXJyYXkoXCJzY3JpcHRcIixzLmRhdGFUeXBlcyk+LTEpe3MuY29udmVydGVyc1tcInRleHQgc2NyaXB0XCJdPWZ1bmN0aW9uKCl7fTt9XG5yZXNwb25zZT1hamF4Q29udmVydChzLHJlc3BvbnNlLGpxWEhSLGlzU3VjY2Vzcyk7aWYoaXNTdWNjZXNzKXtpZihzLmlmTW9kaWZpZWQpe21vZGlmaWVkPWpxWEhSLmdldFJlc3BvbnNlSGVhZGVyKFwiTGFzdC1Nb2RpZmllZFwiKTtpZihtb2RpZmllZCl7alF1ZXJ5Lmxhc3RNb2RpZmllZFtjYWNoZVVSTF09bW9kaWZpZWQ7fVxubW9kaWZpZWQ9anFYSFIuZ2V0UmVzcG9uc2VIZWFkZXIoXCJldGFnXCIpO2lmKG1vZGlmaWVkKXtqUXVlcnkuZXRhZ1tjYWNoZVVSTF09bW9kaWZpZWQ7fX1cbmlmKHN0YXR1cz09PTIwNHx8cy50eXBlPT09XCJIRUFEXCIpe3N0YXR1c1RleHQ9XCJub2NvbnRlbnRcIjt9ZWxzZSBpZihzdGF0dXM9PT0zMDQpe3N0YXR1c1RleHQ9XCJub3Rtb2RpZmllZFwiO31lbHNle3N0YXR1c1RleHQ9cmVzcG9uc2Uuc3RhdGU7c3VjY2Vzcz1yZXNwb25zZS5kYXRhO2Vycm9yPXJlc3BvbnNlLmVycm9yO2lzU3VjY2Vzcz0hZXJyb3I7fX1lbHNle2Vycm9yPXN0YXR1c1RleHQ7aWYoc3RhdHVzfHwhc3RhdHVzVGV4dCl7c3RhdHVzVGV4dD1cImVycm9yXCI7aWYoc3RhdHVzPDApe3N0YXR1cz0wO319fVxuanFYSFIuc3RhdHVzPXN0YXR1cztqcVhIUi5zdGF0dXNUZXh0PShuYXRpdmVTdGF0dXNUZXh0fHxzdGF0dXNUZXh0KStcIlwiO2lmKGlzU3VjY2Vzcyl7ZGVmZXJyZWQucmVzb2x2ZVdpdGgoY2FsbGJhY2tDb250ZXh0LFtzdWNjZXNzLHN0YXR1c1RleHQsanFYSFJdKTt9ZWxzZXtkZWZlcnJlZC5yZWplY3RXaXRoKGNhbGxiYWNrQ29udGV4dCxbanFYSFIsc3RhdHVzVGV4dCxlcnJvcl0pO31cbmpxWEhSLnN0YXR1c0NvZGUoc3RhdHVzQ29kZSk7c3RhdHVzQ29kZT11bmRlZmluZWQ7aWYoZmlyZUdsb2JhbHMpe2dsb2JhbEV2ZW50Q29udGV4dC50cmlnZ2VyKGlzU3VjY2Vzcz9cImFqYXhTdWNjZXNzXCI6XCJhamF4RXJyb3JcIixbanFYSFIscyxpc1N1Y2Nlc3M/c3VjY2VzczplcnJvcl0pO31cbmNvbXBsZXRlRGVmZXJyZWQuZmlyZVdpdGgoY2FsbGJhY2tDb250ZXh0LFtqcVhIUixzdGF0dXNUZXh0XSk7aWYoZmlyZUdsb2JhbHMpe2dsb2JhbEV2ZW50Q29udGV4dC50cmlnZ2VyKFwiYWpheENvbXBsZXRlXCIsW2pxWEhSLHNdKTtpZighKC0talF1ZXJ5LmFjdGl2ZSkpe2pRdWVyeS5ldmVudC50cmlnZ2VyKFwiYWpheFN0b3BcIik7fX19XG5yZXR1cm4ganFYSFI7fSxnZXRKU09OOmZ1bmN0aW9uKHVybCxkYXRhLGNhbGxiYWNrKXtyZXR1cm4galF1ZXJ5LmdldCh1cmwsZGF0YSxjYWxsYmFjayxcImpzb25cIik7fSxnZXRTY3JpcHQ6ZnVuY3Rpb24odXJsLGNhbGxiYWNrKXtyZXR1cm4galF1ZXJ5LmdldCh1cmwsdW5kZWZpbmVkLGNhbGxiYWNrLFwic2NyaXB0XCIpO319KTtqUXVlcnkuZWFjaChbXCJnZXRcIixcInBvc3RcIl0sZnVuY3Rpb24oX2ksbWV0aG9kKXtqUXVlcnlbbWV0aG9kXT1mdW5jdGlvbih1cmwsZGF0YSxjYWxsYmFjayx0eXBlKXtpZihpc0Z1bmN0aW9uKGRhdGEpKXt0eXBlPXR5cGV8fGNhbGxiYWNrO2NhbGxiYWNrPWRhdGE7ZGF0YT11bmRlZmluZWQ7fVxucmV0dXJuIGpRdWVyeS5hamF4KGpRdWVyeS5leHRlbmQoe3VybDp1cmwsdHlwZTptZXRob2QsZGF0YVR5cGU6dHlwZSxkYXRhOmRhdGEsc3VjY2VzczpjYWxsYmFja30salF1ZXJ5LmlzUGxhaW5PYmplY3QodXJsKSYmdXJsKSk7fTt9KTtqUXVlcnkuYWpheFByZWZpbHRlcihmdW5jdGlvbihzKXt2YXIgaTtmb3IoaSBpbiBzLmhlYWRlcnMpe2lmKGkudG9Mb3dlckNhc2UoKT09PVwiY29udGVudC10eXBlXCIpe3MuY29udGVudFR5cGU9cy5oZWFkZXJzW2ldfHxcIlwiO319fSk7alF1ZXJ5Ll9ldmFsVXJsPWZ1bmN0aW9uKHVybCxvcHRpb25zLGRvYyl7cmV0dXJuIGpRdWVyeS5hamF4KHt1cmw6dXJsLHR5cGU6XCJHRVRcIixkYXRhVHlwZTpcInNjcmlwdFwiLGNhY2hlOnRydWUsYXN5bmM6ZmFsc2UsZ2xvYmFsOmZhbHNlLGNvbnZlcnRlcnM6e1widGV4dCBzY3JpcHRcIjpmdW5jdGlvbigpe319LGRhdGFGaWx0ZXI6ZnVuY3Rpb24ocmVzcG9uc2Upe2pRdWVyeS5nbG9iYWxFdmFsKHJlc3BvbnNlLG9wdGlvbnMsZG9jKTt9fSk7fTtqUXVlcnkuZm4uZXh0ZW5kKHt3cmFwQWxsOmZ1bmN0aW9uKGh0bWwpe3ZhciB3cmFwO2lmKHRoaXNbMF0pe2lmKGlzRnVuY3Rpb24oaHRtbCkpe2h0bWw9aHRtbC5jYWxsKHRoaXNbMF0pO31cbndyYXA9alF1ZXJ5KGh0bWwsdGhpc1swXS5vd25lckRvY3VtZW50KS5lcSgwKS5jbG9uZSh0cnVlKTtpZih0aGlzWzBdLnBhcmVudE5vZGUpe3dyYXAuaW5zZXJ0QmVmb3JlKHRoaXNbMF0pO31cbndyYXAubWFwKGZ1bmN0aW9uKCl7dmFyIGVsZW09dGhpczt3aGlsZShlbGVtLmZpcnN0RWxlbWVudENoaWxkKXtlbGVtPWVsZW0uZmlyc3RFbGVtZW50Q2hpbGQ7fVxucmV0dXJuIGVsZW07fSkuYXBwZW5kKHRoaXMpO31cbnJldHVybiB0aGlzO30sd3JhcElubmVyOmZ1bmN0aW9uKGh0bWwpe2lmKGlzRnVuY3Rpb24oaHRtbCkpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oaSl7alF1ZXJ5KHRoaXMpLndyYXBJbm5lcihodG1sLmNhbGwodGhpcyxpKSk7fSk7fVxucmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbigpe3ZhciBzZWxmPWpRdWVyeSh0aGlzKSxjb250ZW50cz1zZWxmLmNvbnRlbnRzKCk7aWYoY29udGVudHMubGVuZ3RoKXtjb250ZW50cy53cmFwQWxsKGh0bWwpO31lbHNle3NlbGYuYXBwZW5kKGh0bWwpO319KTt9LHdyYXA6ZnVuY3Rpb24oaHRtbCl7dmFyIGh0bWxJc0Z1bmN0aW9uPWlzRnVuY3Rpb24oaHRtbCk7cmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbihpKXtqUXVlcnkodGhpcykud3JhcEFsbChodG1sSXNGdW5jdGlvbj9odG1sLmNhbGwodGhpcyxpKTpodG1sKTt9KTt9LHVud3JhcDpmdW5jdGlvbihzZWxlY3Rvcil7dGhpcy5wYXJlbnQoc2VsZWN0b3IpLm5vdChcImJvZHlcIikuZWFjaChmdW5jdGlvbigpe2pRdWVyeSh0aGlzKS5yZXBsYWNlV2l0aCh0aGlzLmNoaWxkTm9kZXMpO30pO3JldHVybiB0aGlzO319KTtqUXVlcnkuZXhwci5wc2V1ZG9zLmhpZGRlbj1mdW5jdGlvbihlbGVtKXtyZXR1cm4halF1ZXJ5LmV4cHIucHNldWRvcy52aXNpYmxlKGVsZW0pO307alF1ZXJ5LmV4cHIucHNldWRvcy52aXNpYmxlPWZ1bmN0aW9uKGVsZW0pe3JldHVybiEhKGVsZW0ub2Zmc2V0V2lkdGh8fGVsZW0ub2Zmc2V0SGVpZ2h0fHxlbGVtLmdldENsaWVudFJlY3RzKCkubGVuZ3RoKTt9O2pRdWVyeS5hamF4U2V0dGluZ3MueGhyPWZ1bmN0aW9uKCl7dHJ5e3JldHVybiBuZXcgd2luZG93LlhNTEh0dHBSZXF1ZXN0KCk7fWNhdGNoKGUpe319O3ZhciB4aHJTdWNjZXNzU3RhdHVzPXswOjIwMCwxMjIzOjIwNH0seGhyU3VwcG9ydGVkPWpRdWVyeS5hamF4U2V0dGluZ3MueGhyKCk7c3VwcG9ydC5jb3JzPSEheGhyU3VwcG9ydGVkJiYoXCJ3aXRoQ3JlZGVudGlhbHNcImluIHhoclN1cHBvcnRlZCk7c3VwcG9ydC5hamF4PXhoclN1cHBvcnRlZD0hIXhoclN1cHBvcnRlZDtqUXVlcnkuYWpheFRyYW5zcG9ydChmdW5jdGlvbihvcHRpb25zKXt2YXIgY2FsbGJhY2ssZXJyb3JDYWxsYmFjaztpZihzdXBwb3J0LmNvcnN8fHhoclN1cHBvcnRlZCYmIW9wdGlvbnMuY3Jvc3NEb21haW4pe3JldHVybntzZW5kOmZ1bmN0aW9uKGhlYWRlcnMsY29tcGxldGUpe3ZhciBpLHhocj1vcHRpb25zLnhocigpO3hoci5vcGVuKG9wdGlvbnMudHlwZSxvcHRpb25zLnVybCxvcHRpb25zLmFzeW5jLG9wdGlvbnMudXNlcm5hbWUsb3B0aW9ucy5wYXNzd29yZCk7aWYob3B0aW9ucy54aHJGaWVsZHMpe2ZvcihpIGluIG9wdGlvbnMueGhyRmllbGRzKXt4aHJbaV09b3B0aW9ucy54aHJGaWVsZHNbaV07fX1cbmlmKG9wdGlvbnMubWltZVR5cGUmJnhoci5vdmVycmlkZU1pbWVUeXBlKXt4aHIub3ZlcnJpZGVNaW1lVHlwZShvcHRpb25zLm1pbWVUeXBlKTt9XG5pZighb3B0aW9ucy5jcm9zc0RvbWFpbiYmIWhlYWRlcnNbXCJYLVJlcXVlc3RlZC1XaXRoXCJdKXtoZWFkZXJzW1wiWC1SZXF1ZXN0ZWQtV2l0aFwiXT1cIlhNTEh0dHBSZXF1ZXN0XCI7fVxuZm9yKGkgaW4gaGVhZGVycyl7eGhyLnNldFJlcXVlc3RIZWFkZXIoaSxoZWFkZXJzW2ldKTt9XG5jYWxsYmFjaz1mdW5jdGlvbih0eXBlKXtyZXR1cm4gZnVuY3Rpb24oKXtpZihjYWxsYmFjayl7Y2FsbGJhY2s9ZXJyb3JDYWxsYmFjaz14aHIub25sb2FkPXhoci5vbmVycm9yPXhoci5vbmFib3J0PXhoci5vbnRpbWVvdXQ9eGhyLm9ucmVhZHlzdGF0ZWNoYW5nZT1udWxsO2lmKHR5cGU9PT1cImFib3J0XCIpe3hoci5hYm9ydCgpO31lbHNlIGlmKHR5cGU9PT1cImVycm9yXCIpe2lmKHR5cGVvZiB4aHIuc3RhdHVzIT09XCJudW1iZXJcIil7Y29tcGxldGUoMCxcImVycm9yXCIpO31lbHNle2NvbXBsZXRlKHhoci5zdGF0dXMseGhyLnN0YXR1c1RleHQpO319ZWxzZXtjb21wbGV0ZSh4aHJTdWNjZXNzU3RhdHVzW3hoci5zdGF0dXNdfHx4aHIuc3RhdHVzLHhoci5zdGF0dXNUZXh0LCh4aHIucmVzcG9uc2VUeXBlfHxcInRleHRcIikhPT1cInRleHRcInx8dHlwZW9mIHhoci5yZXNwb25zZVRleHQhPT1cInN0cmluZ1wiP3tiaW5hcnk6eGhyLnJlc3BvbnNlfTp7dGV4dDp4aHIucmVzcG9uc2VUZXh0fSx4aHIuZ2V0QWxsUmVzcG9uc2VIZWFkZXJzKCkpO319fTt9O3hoci5vbmxvYWQ9Y2FsbGJhY2soKTtlcnJvckNhbGxiYWNrPXhoci5vbmVycm9yPXhoci5vbnRpbWVvdXQ9Y2FsbGJhY2soXCJlcnJvclwiKTtpZih4aHIub25hYm9ydCE9PXVuZGVmaW5lZCl7eGhyLm9uYWJvcnQ9ZXJyb3JDYWxsYmFjazt9ZWxzZXt4aHIub25yZWFkeXN0YXRlY2hhbmdlPWZ1bmN0aW9uKCl7aWYoeGhyLnJlYWR5U3RhdGU9PT00KXt3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbigpe2lmKGNhbGxiYWNrKXtlcnJvckNhbGxiYWNrKCk7fX0pO319O31cbmNhbGxiYWNrPWNhbGxiYWNrKFwiYWJvcnRcIik7dHJ5e3hoci5zZW5kKG9wdGlvbnMuaGFzQ29udGVudCYmb3B0aW9ucy5kYXRhfHxudWxsKTt9Y2F0Y2goZSl7aWYoY2FsbGJhY2spe3Rocm93IGU7fX19LGFib3J0OmZ1bmN0aW9uKCl7aWYoY2FsbGJhY2spe2NhbGxiYWNrKCk7fX19O319KTtqUXVlcnkuYWpheFByZWZpbHRlcihmdW5jdGlvbihzKXtpZihzLmNyb3NzRG9tYWluKXtzLmNvbnRlbnRzLnNjcmlwdD1mYWxzZTt9fSk7alF1ZXJ5LmFqYXhTZXR1cCh7YWNjZXB0czp7c2NyaXB0OlwidGV4dC9qYXZhc2NyaXB0LCBhcHBsaWNhdGlvbi9qYXZhc2NyaXB0LCBcIitcImFwcGxpY2F0aW9uL2VjbWFzY3JpcHQsIGFwcGxpY2F0aW9uL3gtZWNtYXNjcmlwdFwifSxjb250ZW50czp7c2NyaXB0Oi9cXGIoPzpqYXZhfGVjbWEpc2NyaXB0XFxiL30sY29udmVydGVyczp7XCJ0ZXh0IHNjcmlwdFwiOmZ1bmN0aW9uKHRleHQpe2pRdWVyeS5nbG9iYWxFdmFsKHRleHQpO3JldHVybiB0ZXh0O319fSk7alF1ZXJ5LmFqYXhQcmVmaWx0ZXIoXCJzY3JpcHRcIixmdW5jdGlvbihzKXtpZihzLmNhY2hlPT09dW5kZWZpbmVkKXtzLmNhY2hlPWZhbHNlO31cbmlmKHMuY3Jvc3NEb21haW4pe3MudHlwZT1cIkdFVFwiO319KTtqUXVlcnkuYWpheFRyYW5zcG9ydChcInNjcmlwdFwiLGZ1bmN0aW9uKHMpe2lmKHMuY3Jvc3NEb21haW58fHMuc2NyaXB0QXR0cnMpe3ZhciBzY3JpcHQsY2FsbGJhY2s7cmV0dXJue3NlbmQ6ZnVuY3Rpb24oXyxjb21wbGV0ZSl7c2NyaXB0PWpRdWVyeShcIjxzY3JpcHQ+XCIpLmF0dHIocy5zY3JpcHRBdHRyc3x8e30pLnByb3Aoe2NoYXJzZXQ6cy5zY3JpcHRDaGFyc2V0LHNyYzpzLnVybH0pLm9uKFwibG9hZCBlcnJvclwiLGNhbGxiYWNrPWZ1bmN0aW9uKGV2dCl7c2NyaXB0LnJlbW92ZSgpO2NhbGxiYWNrPW51bGw7aWYoZXZ0KXtjb21wbGV0ZShldnQudHlwZT09PVwiZXJyb3JcIj80MDQ6MjAwLGV2dC50eXBlKTt9fSk7ZG9jdW1lbnQuaGVhZC5hcHBlbmRDaGlsZChzY3JpcHRbMF0pO30sYWJvcnQ6ZnVuY3Rpb24oKXtpZihjYWxsYmFjayl7Y2FsbGJhY2soKTt9fX07fX0pO3ZhciBvbGRDYWxsYmFja3M9W10scmpzb25wPS8oPSlcXD8oPz0mfCQpfFxcP1xcPy87alF1ZXJ5LmFqYXhTZXR1cCh7anNvbnA6XCJjYWxsYmFja1wiLGpzb25wQ2FsbGJhY2s6ZnVuY3Rpb24oKXt2YXIgY2FsbGJhY2s9b2xkQ2FsbGJhY2tzLnBvcCgpfHwoalF1ZXJ5LmV4cGFuZG8rXCJfXCIrKG5vbmNlLmd1aWQrKykpO3RoaXNbY2FsbGJhY2tdPXRydWU7cmV0dXJuIGNhbGxiYWNrO319KTtqUXVlcnkuYWpheFByZWZpbHRlcihcImpzb24ganNvbnBcIixmdW5jdGlvbihzLG9yaWdpbmFsU2V0dGluZ3MsanFYSFIpe3ZhciBjYWxsYmFja05hbWUsb3ZlcndyaXR0ZW4scmVzcG9uc2VDb250YWluZXIsanNvblByb3A9cy5qc29ucCE9PWZhbHNlJiYocmpzb25wLnRlc3Qocy51cmwpP1widXJsXCI6dHlwZW9mIHMuZGF0YT09PVwic3RyaW5nXCImJihzLmNvbnRlbnRUeXBlfHxcIlwiKS5pbmRleE9mKFwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIpPT09MCYmcmpzb25wLnRlc3Qocy5kYXRhKSYmXCJkYXRhXCIpO2lmKGpzb25Qcm9wfHxzLmRhdGFUeXBlc1swXT09PVwianNvbnBcIil7Y2FsbGJhY2tOYW1lPXMuanNvbnBDYWxsYmFjaz1pc0Z1bmN0aW9uKHMuanNvbnBDYWxsYmFjayk/cy5qc29ucENhbGxiYWNrKCk6cy5qc29ucENhbGxiYWNrO2lmKGpzb25Qcm9wKXtzW2pzb25Qcm9wXT1zW2pzb25Qcm9wXS5yZXBsYWNlKHJqc29ucCxcIiQxXCIrY2FsbGJhY2tOYW1lKTt9ZWxzZSBpZihzLmpzb25wIT09ZmFsc2Upe3MudXJsKz0ocnF1ZXJ5LnRlc3Qocy51cmwpP1wiJlwiOlwiP1wiKStzLmpzb25wK1wiPVwiK2NhbGxiYWNrTmFtZTt9XG5zLmNvbnZlcnRlcnNbXCJzY3JpcHQganNvblwiXT1mdW5jdGlvbigpe2lmKCFyZXNwb25zZUNvbnRhaW5lcil7alF1ZXJ5LmVycm9yKGNhbGxiYWNrTmFtZStcIiB3YXMgbm90IGNhbGxlZFwiKTt9XG5yZXR1cm4gcmVzcG9uc2VDb250YWluZXJbMF07fTtzLmRhdGFUeXBlc1swXT1cImpzb25cIjtvdmVyd3JpdHRlbj13aW5kb3dbY2FsbGJhY2tOYW1lXTt3aW5kb3dbY2FsbGJhY2tOYW1lXT1mdW5jdGlvbigpe3Jlc3BvbnNlQ29udGFpbmVyPWFyZ3VtZW50czt9O2pxWEhSLmFsd2F5cyhmdW5jdGlvbigpe2lmKG92ZXJ3cml0dGVuPT09dW5kZWZpbmVkKXtqUXVlcnkod2luZG93KS5yZW1vdmVQcm9wKGNhbGxiYWNrTmFtZSk7fWVsc2V7d2luZG93W2NhbGxiYWNrTmFtZV09b3ZlcndyaXR0ZW47fVxuaWYoc1tjYWxsYmFja05hbWVdKXtzLmpzb25wQ2FsbGJhY2s9b3JpZ2luYWxTZXR0aW5ncy5qc29ucENhbGxiYWNrO29sZENhbGxiYWNrcy5wdXNoKGNhbGxiYWNrTmFtZSk7fVxuaWYocmVzcG9uc2VDb250YWluZXImJmlzRnVuY3Rpb24ob3ZlcndyaXR0ZW4pKXtvdmVyd3JpdHRlbihyZXNwb25zZUNvbnRhaW5lclswXSk7fVxucmVzcG9uc2VDb250YWluZXI9b3ZlcndyaXR0ZW49dW5kZWZpbmVkO30pO3JldHVyblwic2NyaXB0XCI7fX0pO3N1cHBvcnQuY3JlYXRlSFRNTERvY3VtZW50PShmdW5jdGlvbigpe3ZhciBib2R5PWRvY3VtZW50LmltcGxlbWVudGF0aW9uLmNyZWF0ZUhUTUxEb2N1bWVudChcIlwiKS5ib2R5O2JvZHkuaW5uZXJIVE1MPVwiPGZvcm0+PC9mb3JtPjxmb3JtPjwvZm9ybT5cIjtyZXR1cm4gYm9keS5jaGlsZE5vZGVzLmxlbmd0aD09PTI7fSkoKTtqUXVlcnkucGFyc2VIVE1MPWZ1bmN0aW9uKGRhdGEsY29udGV4dCxrZWVwU2NyaXB0cyl7aWYodHlwZW9mIGRhdGEhPT1cInN0cmluZ1wiKXtyZXR1cm5bXTt9XG5pZih0eXBlb2YgY29udGV4dD09PVwiYm9vbGVhblwiKXtrZWVwU2NyaXB0cz1jb250ZXh0O2NvbnRleHQ9ZmFsc2U7fVxudmFyIGJhc2UscGFyc2VkLHNjcmlwdHM7aWYoIWNvbnRleHQpe2lmKHN1cHBvcnQuY3JlYXRlSFRNTERvY3VtZW50KXtjb250ZXh0PWRvY3VtZW50LmltcGxlbWVudGF0aW9uLmNyZWF0ZUhUTUxEb2N1bWVudChcIlwiKTtiYXNlPWNvbnRleHQuY3JlYXRlRWxlbWVudChcImJhc2VcIik7YmFzZS5ocmVmPWRvY3VtZW50LmxvY2F0aW9uLmhyZWY7Y29udGV4dC5oZWFkLmFwcGVuZENoaWxkKGJhc2UpO31lbHNle2NvbnRleHQ9ZG9jdW1lbnQ7fX1cbnBhcnNlZD1yc2luZ2xlVGFnLmV4ZWMoZGF0YSk7c2NyaXB0cz0ha2VlcFNjcmlwdHMmJltdO2lmKHBhcnNlZCl7cmV0dXJuW2NvbnRleHQuY3JlYXRlRWxlbWVudChwYXJzZWRbMV0pXTt9XG5wYXJzZWQ9YnVpbGRGcmFnbWVudChbZGF0YV0sY29udGV4dCxzY3JpcHRzKTtpZihzY3JpcHRzJiZzY3JpcHRzLmxlbmd0aCl7alF1ZXJ5KHNjcmlwdHMpLnJlbW92ZSgpO31cbnJldHVybiBqUXVlcnkubWVyZ2UoW10scGFyc2VkLmNoaWxkTm9kZXMpO307alF1ZXJ5LmZuLmxvYWQ9ZnVuY3Rpb24odXJsLHBhcmFtcyxjYWxsYmFjayl7dmFyIHNlbGVjdG9yLHR5cGUscmVzcG9uc2Usc2VsZj10aGlzLG9mZj11cmwuaW5kZXhPZihcIiBcIik7aWYob2ZmPi0xKXtzZWxlY3Rvcj1zdHJpcEFuZENvbGxhcHNlKHVybC5zbGljZShvZmYpKTt1cmw9dXJsLnNsaWNlKDAsb2ZmKTt9XG5pZihpc0Z1bmN0aW9uKHBhcmFtcykpe2NhbGxiYWNrPXBhcmFtcztwYXJhbXM9dW5kZWZpbmVkO31lbHNlIGlmKHBhcmFtcyYmdHlwZW9mIHBhcmFtcz09PVwib2JqZWN0XCIpe3R5cGU9XCJQT1NUXCI7fVxuaWYoc2VsZi5sZW5ndGg+MCl7alF1ZXJ5LmFqYXgoe3VybDp1cmwsdHlwZTp0eXBlfHxcIkdFVFwiLGRhdGFUeXBlOlwiaHRtbFwiLGRhdGE6cGFyYW1zfSkuZG9uZShmdW5jdGlvbihyZXNwb25zZVRleHQpe3Jlc3BvbnNlPWFyZ3VtZW50cztzZWxmLmh0bWwoc2VsZWN0b3I/alF1ZXJ5KFwiPGRpdj5cIikuYXBwZW5kKGpRdWVyeS5wYXJzZUhUTUwocmVzcG9uc2VUZXh0KSkuZmluZChzZWxlY3Rvcik6cmVzcG9uc2VUZXh0KTt9KS5hbHdheXMoY2FsbGJhY2smJmZ1bmN0aW9uKGpxWEhSLHN0YXR1cyl7c2VsZi5lYWNoKGZ1bmN0aW9uKCl7Y2FsbGJhY2suYXBwbHkodGhpcyxyZXNwb25zZXx8W2pxWEhSLnJlc3BvbnNlVGV4dCxzdGF0dXMsanFYSFJdKTt9KTt9KTt9XG5yZXR1cm4gdGhpczt9O2pRdWVyeS5leHByLnBzZXVkb3MuYW5pbWF0ZWQ9ZnVuY3Rpb24oZWxlbSl7cmV0dXJuIGpRdWVyeS5ncmVwKGpRdWVyeS50aW1lcnMsZnVuY3Rpb24oZm4pe3JldHVybiBlbGVtPT09Zm4uZWxlbTt9KS5sZW5ndGg7fTtqUXVlcnkub2Zmc2V0PXtzZXRPZmZzZXQ6ZnVuY3Rpb24oZWxlbSxvcHRpb25zLGkpe3ZhciBjdXJQb3NpdGlvbixjdXJMZWZ0LGN1ckNTU1RvcCxjdXJUb3AsY3VyT2Zmc2V0LGN1ckNTU0xlZnQsY2FsY3VsYXRlUG9zaXRpb24scG9zaXRpb249alF1ZXJ5LmNzcyhlbGVtLFwicG9zaXRpb25cIiksY3VyRWxlbT1qUXVlcnkoZWxlbSkscHJvcHM9e307aWYocG9zaXRpb249PT1cInN0YXRpY1wiKXtlbGVtLnN0eWxlLnBvc2l0aW9uPVwicmVsYXRpdmVcIjt9XG5jdXJPZmZzZXQ9Y3VyRWxlbS5vZmZzZXQoKTtjdXJDU1NUb3A9alF1ZXJ5LmNzcyhlbGVtLFwidG9wXCIpO2N1ckNTU0xlZnQ9alF1ZXJ5LmNzcyhlbGVtLFwibGVmdFwiKTtjYWxjdWxhdGVQb3NpdGlvbj0ocG9zaXRpb249PT1cImFic29sdXRlXCJ8fHBvc2l0aW9uPT09XCJmaXhlZFwiKSYmKGN1ckNTU1RvcCtjdXJDU1NMZWZ0KS5pbmRleE9mKFwiYXV0b1wiKT4tMTtpZihjYWxjdWxhdGVQb3NpdGlvbil7Y3VyUG9zaXRpb249Y3VyRWxlbS5wb3NpdGlvbigpO2N1clRvcD1jdXJQb3NpdGlvbi50b3A7Y3VyTGVmdD1jdXJQb3NpdGlvbi5sZWZ0O31lbHNle2N1clRvcD1wYXJzZUZsb2F0KGN1ckNTU1RvcCl8fDA7Y3VyTGVmdD1wYXJzZUZsb2F0KGN1ckNTU0xlZnQpfHwwO31cbmlmKGlzRnVuY3Rpb24ob3B0aW9ucykpe29wdGlvbnM9b3B0aW9ucy5jYWxsKGVsZW0saSxqUXVlcnkuZXh0ZW5kKHt9LGN1ck9mZnNldCkpO31cbmlmKG9wdGlvbnMudG9wIT1udWxsKXtwcm9wcy50b3A9KG9wdGlvbnMudG9wLWN1ck9mZnNldC50b3ApK2N1clRvcDt9XG5pZihvcHRpb25zLmxlZnQhPW51bGwpe3Byb3BzLmxlZnQ9KG9wdGlvbnMubGVmdC1jdXJPZmZzZXQubGVmdCkrY3VyTGVmdDt9XG5pZihcInVzaW5nXCJpbiBvcHRpb25zKXtvcHRpb25zLnVzaW5nLmNhbGwoZWxlbSxwcm9wcyk7fWVsc2V7aWYodHlwZW9mIHByb3BzLnRvcD09PVwibnVtYmVyXCIpe3Byb3BzLnRvcCs9XCJweFwiO31cbmlmKHR5cGVvZiBwcm9wcy5sZWZ0PT09XCJudW1iZXJcIil7cHJvcHMubGVmdCs9XCJweFwiO31cbmN1ckVsZW0uY3NzKHByb3BzKTt9fX07alF1ZXJ5LmZuLmV4dGVuZCh7b2Zmc2V0OmZ1bmN0aW9uKG9wdGlvbnMpe2lmKGFyZ3VtZW50cy5sZW5ndGgpe3JldHVybiBvcHRpb25zPT09dW5kZWZpbmVkP3RoaXM6dGhpcy5lYWNoKGZ1bmN0aW9uKGkpe2pRdWVyeS5vZmZzZXQuc2V0T2Zmc2V0KHRoaXMsb3B0aW9ucyxpKTt9KTt9XG52YXIgcmVjdCx3aW4sZWxlbT10aGlzWzBdO2lmKCFlbGVtKXtyZXR1cm47fVxuaWYoIWVsZW0uZ2V0Q2xpZW50UmVjdHMoKS5sZW5ndGgpe3JldHVybnt0b3A6MCxsZWZ0OjB9O31cbnJlY3Q9ZWxlbS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTt3aW49ZWxlbS5vd25lckRvY3VtZW50LmRlZmF1bHRWaWV3O3JldHVybnt0b3A6cmVjdC50b3Ard2luLnBhZ2VZT2Zmc2V0LGxlZnQ6cmVjdC5sZWZ0K3dpbi5wYWdlWE9mZnNldH07fSxwb3NpdGlvbjpmdW5jdGlvbigpe2lmKCF0aGlzWzBdKXtyZXR1cm47fVxudmFyIG9mZnNldFBhcmVudCxvZmZzZXQsZG9jLGVsZW09dGhpc1swXSxwYXJlbnRPZmZzZXQ9e3RvcDowLGxlZnQ6MH07aWYoalF1ZXJ5LmNzcyhlbGVtLFwicG9zaXRpb25cIik9PT1cImZpeGVkXCIpe29mZnNldD1lbGVtLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO31lbHNle29mZnNldD10aGlzLm9mZnNldCgpO2RvYz1lbGVtLm93bmVyRG9jdW1lbnQ7b2Zmc2V0UGFyZW50PWVsZW0ub2Zmc2V0UGFyZW50fHxkb2MuZG9jdW1lbnRFbGVtZW50O3doaWxlKG9mZnNldFBhcmVudCYmKG9mZnNldFBhcmVudD09PWRvYy5ib2R5fHxvZmZzZXRQYXJlbnQ9PT1kb2MuZG9jdW1lbnRFbGVtZW50KSYmalF1ZXJ5LmNzcyhvZmZzZXRQYXJlbnQsXCJwb3NpdGlvblwiKT09PVwic3RhdGljXCIpe29mZnNldFBhcmVudD1vZmZzZXRQYXJlbnQucGFyZW50Tm9kZTt9XG5pZihvZmZzZXRQYXJlbnQmJm9mZnNldFBhcmVudCE9PWVsZW0mJm9mZnNldFBhcmVudC5ub2RlVHlwZT09PTEpe3BhcmVudE9mZnNldD1qUXVlcnkob2Zmc2V0UGFyZW50KS5vZmZzZXQoKTtwYXJlbnRPZmZzZXQudG9wKz1qUXVlcnkuY3NzKG9mZnNldFBhcmVudCxcImJvcmRlclRvcFdpZHRoXCIsdHJ1ZSk7cGFyZW50T2Zmc2V0LmxlZnQrPWpRdWVyeS5jc3Mob2Zmc2V0UGFyZW50LFwiYm9yZGVyTGVmdFdpZHRoXCIsdHJ1ZSk7fX1cbnJldHVybnt0b3A6b2Zmc2V0LnRvcC1wYXJlbnRPZmZzZXQudG9wLWpRdWVyeS5jc3MoZWxlbSxcIm1hcmdpblRvcFwiLHRydWUpLGxlZnQ6b2Zmc2V0LmxlZnQtcGFyZW50T2Zmc2V0LmxlZnQtalF1ZXJ5LmNzcyhlbGVtLFwibWFyZ2luTGVmdFwiLHRydWUpfTt9LG9mZnNldFBhcmVudDpmdW5jdGlvbigpe3JldHVybiB0aGlzLm1hcChmdW5jdGlvbigpe3ZhciBvZmZzZXRQYXJlbnQ9dGhpcy5vZmZzZXRQYXJlbnQ7d2hpbGUob2Zmc2V0UGFyZW50JiZqUXVlcnkuY3NzKG9mZnNldFBhcmVudCxcInBvc2l0aW9uXCIpPT09XCJzdGF0aWNcIil7b2Zmc2V0UGFyZW50PW9mZnNldFBhcmVudC5vZmZzZXRQYXJlbnQ7fVxucmV0dXJuIG9mZnNldFBhcmVudHx8ZG9jdW1lbnRFbGVtZW50O30pO319KTtqUXVlcnkuZWFjaCh7c2Nyb2xsTGVmdDpcInBhZ2VYT2Zmc2V0XCIsc2Nyb2xsVG9wOlwicGFnZVlPZmZzZXRcIn0sZnVuY3Rpb24obWV0aG9kLHByb3Ape3ZhciB0b3A9XCJwYWdlWU9mZnNldFwiPT09cHJvcDtqUXVlcnkuZm5bbWV0aG9kXT1mdW5jdGlvbih2YWwpe3JldHVybiBhY2Nlc3ModGhpcyxmdW5jdGlvbihlbGVtLG1ldGhvZCx2YWwpe3ZhciB3aW47aWYoaXNXaW5kb3coZWxlbSkpe3dpbj1lbGVtO31lbHNlIGlmKGVsZW0ubm9kZVR5cGU9PT05KXt3aW49ZWxlbS5kZWZhdWx0Vmlldzt9XG5pZih2YWw9PT11bmRlZmluZWQpe3JldHVybiB3aW4/d2luW3Byb3BdOmVsZW1bbWV0aG9kXTt9XG5pZih3aW4pe3dpbi5zY3JvbGxUbyghdG9wP3ZhbDp3aW4ucGFnZVhPZmZzZXQsdG9wP3ZhbDp3aW4ucGFnZVlPZmZzZXQpO31lbHNle2VsZW1bbWV0aG9kXT12YWw7fX0sbWV0aG9kLHZhbCxhcmd1bWVudHMubGVuZ3RoKTt9O30pO2pRdWVyeS5lYWNoKFtcInRvcFwiLFwibGVmdFwiXSxmdW5jdGlvbihfaSxwcm9wKXtqUXVlcnkuY3NzSG9va3NbcHJvcF09YWRkR2V0SG9va0lmKHN1cHBvcnQucGl4ZWxQb3NpdGlvbixmdW5jdGlvbihlbGVtLGNvbXB1dGVkKXtpZihjb21wdXRlZCl7Y29tcHV0ZWQ9Y3VyQ1NTKGVsZW0scHJvcCk7cmV0dXJuIHJudW1ub25weC50ZXN0KGNvbXB1dGVkKT9qUXVlcnkoZWxlbSkucG9zaXRpb24oKVtwcm9wXStcInB4XCI6Y29tcHV0ZWQ7fX0pO30pO2pRdWVyeS5lYWNoKHtIZWlnaHQ6XCJoZWlnaHRcIixXaWR0aDpcIndpZHRoXCJ9LGZ1bmN0aW9uKG5hbWUsdHlwZSl7alF1ZXJ5LmVhY2goe3BhZGRpbmc6XCJpbm5lclwiK25hbWUsY29udGVudDp0eXBlLFwiXCI6XCJvdXRlclwiK25hbWV9LGZ1bmN0aW9uKGRlZmF1bHRFeHRyYSxmdW5jTmFtZSl7alF1ZXJ5LmZuW2Z1bmNOYW1lXT1mdW5jdGlvbihtYXJnaW4sdmFsdWUpe3ZhciBjaGFpbmFibGU9YXJndW1lbnRzLmxlbmd0aCYmKGRlZmF1bHRFeHRyYXx8dHlwZW9mIG1hcmdpbiE9PVwiYm9vbGVhblwiKSxleHRyYT1kZWZhdWx0RXh0cmF8fChtYXJnaW49PT10cnVlfHx2YWx1ZT09PXRydWU/XCJtYXJnaW5cIjpcImJvcmRlclwiKTtyZXR1cm4gYWNjZXNzKHRoaXMsZnVuY3Rpb24oZWxlbSx0eXBlLHZhbHVlKXt2YXIgZG9jO2lmKGlzV2luZG93KGVsZW0pKXtyZXR1cm4gZnVuY05hbWUuaW5kZXhPZihcIm91dGVyXCIpPT09MD9lbGVtW1wiaW5uZXJcIituYW1lXTplbGVtLmRvY3VtZW50LmRvY3VtZW50RWxlbWVudFtcImNsaWVudFwiK25hbWVdO31cbmlmKGVsZW0ubm9kZVR5cGU9PT05KXtkb2M9ZWxlbS5kb2N1bWVudEVsZW1lbnQ7cmV0dXJuIE1hdGgubWF4KGVsZW0uYm9keVtcInNjcm9sbFwiK25hbWVdLGRvY1tcInNjcm9sbFwiK25hbWVdLGVsZW0uYm9keVtcIm9mZnNldFwiK25hbWVdLGRvY1tcIm9mZnNldFwiK25hbWVdLGRvY1tcImNsaWVudFwiK25hbWVdKTt9XG5yZXR1cm4gdmFsdWU9PT11bmRlZmluZWQ/alF1ZXJ5LmNzcyhlbGVtLHR5cGUsZXh0cmEpOmpRdWVyeS5zdHlsZShlbGVtLHR5cGUsdmFsdWUsZXh0cmEpO30sdHlwZSxjaGFpbmFibGU/bWFyZ2luOnVuZGVmaW5lZCxjaGFpbmFibGUpO307fSk7fSk7alF1ZXJ5LmVhY2goW1wiYWpheFN0YXJ0XCIsXCJhamF4U3RvcFwiLFwiYWpheENvbXBsZXRlXCIsXCJhamF4RXJyb3JcIixcImFqYXhTdWNjZXNzXCIsXCJhamF4U2VuZFwiXSxmdW5jdGlvbihfaSx0eXBlKXtqUXVlcnkuZm5bdHlwZV09ZnVuY3Rpb24oZm4pe3JldHVybiB0aGlzLm9uKHR5cGUsZm4pO307fSk7alF1ZXJ5LmZuLmV4dGVuZCh7YmluZDpmdW5jdGlvbih0eXBlcyxkYXRhLGZuKXtyZXR1cm4gdGhpcy5vbih0eXBlcyxudWxsLGRhdGEsZm4pO30sdW5iaW5kOmZ1bmN0aW9uKHR5cGVzLGZuKXtyZXR1cm4gdGhpcy5vZmYodHlwZXMsbnVsbCxmbik7fSxkZWxlZ2F0ZTpmdW5jdGlvbihzZWxlY3Rvcix0eXBlcyxkYXRhLGZuKXtyZXR1cm4gdGhpcy5vbih0eXBlcyxzZWxlY3RvcixkYXRhLGZuKTt9LHVuZGVsZWdhdGU6ZnVuY3Rpb24oc2VsZWN0b3IsdHlwZXMsZm4pe3JldHVybiBhcmd1bWVudHMubGVuZ3RoPT09MT90aGlzLm9mZihzZWxlY3RvcixcIioqXCIpOnRoaXMub2ZmKHR5cGVzLHNlbGVjdG9yfHxcIioqXCIsZm4pO30saG92ZXI6ZnVuY3Rpb24oZm5PdmVyLGZuT3V0KXtyZXR1cm4gdGhpcy5tb3VzZWVudGVyKGZuT3ZlcikubW91c2VsZWF2ZShmbk91dHx8Zm5PdmVyKTt9fSk7alF1ZXJ5LmVhY2goKFwiYmx1ciBmb2N1cyBmb2N1c2luIGZvY3Vzb3V0IHJlc2l6ZSBzY3JvbGwgY2xpY2sgZGJsY2xpY2sgXCIrXCJtb3VzZWRvd24gbW91c2V1cCBtb3VzZW1vdmUgbW91c2VvdmVyIG1vdXNlb3V0IG1vdXNlZW50ZXIgbW91c2VsZWF2ZSBcIitcImNoYW5nZSBzZWxlY3Qgc3VibWl0IGtleWRvd24ga2V5cHJlc3Mga2V5dXAgY29udGV4dG1lbnVcIikuc3BsaXQoXCIgXCIpLGZ1bmN0aW9uKF9pLG5hbWUpe2pRdWVyeS5mbltuYW1lXT1mdW5jdGlvbihkYXRhLGZuKXtyZXR1cm4gYXJndW1lbnRzLmxlbmd0aD4wP3RoaXMub24obmFtZSxudWxsLGRhdGEsZm4pOnRoaXMudHJpZ2dlcihuYW1lKTt9O30pO3ZhciBydHJpbT0vXltcXHNcXHVGRUZGXFx4QTBdK3xbXFxzXFx1RkVGRlxceEEwXSskL2c7alF1ZXJ5LnByb3h5PWZ1bmN0aW9uKGZuLGNvbnRleHQpe3ZhciB0bXAsYXJncyxwcm94eTtpZih0eXBlb2YgY29udGV4dD09PVwic3RyaW5nXCIpe3RtcD1mbltjb250ZXh0XTtjb250ZXh0PWZuO2ZuPXRtcDt9XG5pZighaXNGdW5jdGlvbihmbikpe3JldHVybiB1bmRlZmluZWQ7fVxuYXJncz1zbGljZS5jYWxsKGFyZ3VtZW50cywyKTtwcm94eT1mdW5jdGlvbigpe3JldHVybiBmbi5hcHBseShjb250ZXh0fHx0aGlzLGFyZ3MuY29uY2F0KHNsaWNlLmNhbGwoYXJndW1lbnRzKSkpO307cHJveHkuZ3VpZD1mbi5ndWlkPWZuLmd1aWR8fGpRdWVyeS5ndWlkKys7cmV0dXJuIHByb3h5O307alF1ZXJ5LmhvbGRSZWFkeT1mdW5jdGlvbihob2xkKXtpZihob2xkKXtqUXVlcnkucmVhZHlXYWl0Kys7fWVsc2V7alF1ZXJ5LnJlYWR5KHRydWUpO319O2pRdWVyeS5pc0FycmF5PUFycmF5LmlzQXJyYXk7alF1ZXJ5LnBhcnNlSlNPTj1KU09OLnBhcnNlO2pRdWVyeS5ub2RlTmFtZT1ub2RlTmFtZTtqUXVlcnkuaXNGdW5jdGlvbj1pc0Z1bmN0aW9uO2pRdWVyeS5pc1dpbmRvdz1pc1dpbmRvdztqUXVlcnkuY2FtZWxDYXNlPWNhbWVsQ2FzZTtqUXVlcnkudHlwZT10b1R5cGU7alF1ZXJ5Lm5vdz1EYXRlLm5vdztqUXVlcnkuaXNOdW1lcmljPWZ1bmN0aW9uKG9iail7dmFyIHR5cGU9alF1ZXJ5LnR5cGUob2JqKTtyZXR1cm4odHlwZT09PVwibnVtYmVyXCJ8fHR5cGU9PT1cInN0cmluZ1wiKSYmIWlzTmFOKG9iai1wYXJzZUZsb2F0KG9iaikpO307alF1ZXJ5LnRyaW09ZnVuY3Rpb24odGV4dCl7cmV0dXJuIHRleHQ9PW51bGw/XCJcIjoodGV4dCtcIlwiKS5yZXBsYWNlKHJ0cmltLFwiXCIpO307aWYodHlwZW9mIGRlZmluZT09PVwiZnVuY3Rpb25cIiYmZGVmaW5lLmFtZCl7ZGVmaW5lKFwianF1ZXJ5XCIsW10sZnVuY3Rpb24oKXtyZXR1cm4galF1ZXJ5O30pO31cbnZhclxuX2pRdWVyeT13aW5kb3cualF1ZXJ5LF8kPXdpbmRvdy4kO2pRdWVyeS5ub0NvbmZsaWN0PWZ1bmN0aW9uKGRlZXApe2lmKHdpbmRvdy4kPT09alF1ZXJ5KXt3aW5kb3cuJD1fJDt9XG5pZihkZWVwJiZ3aW5kb3cualF1ZXJ5PT09alF1ZXJ5KXt3aW5kb3cualF1ZXJ5PV9qUXVlcnk7fVxucmV0dXJuIGpRdWVyeTt9O2lmKHR5cGVvZiBub0dsb2JhbD09PVwidW5kZWZpbmVkXCIpe3dpbmRvdy5qUXVlcnk9d2luZG93LiQ9alF1ZXJ5O31cbnJldHVybiBqUXVlcnk7fSk7alF1ZXJ5Lm5vQ29uZmxpY3QoKTs7XG4vKiFcbiAqIGpRdWVyeSBNaWdyYXRlIC0gdjMuMy4yIC0gMjAyMC0xMS0xOFQwODoyOVpcbiAqIENvcHlyaWdodCBPcGVuSlMgRm91bmRhdGlvbiBhbmQgb3RoZXIgY29udHJpYnV0b3JzXG4gKi9cbihmdW5jdGlvbihmYWN0b3J5KXtcInVzZSBzdHJpY3RcIjtpZih0eXBlb2YgZGVmaW5lPT09XCJmdW5jdGlvblwiJiZkZWZpbmUuYW1kKXtkZWZpbmUoW1wianF1ZXJ5XCJdLGZ1bmN0aW9uKGpRdWVyeSl7cmV0dXJuIGZhY3RvcnkoalF1ZXJ5LHdpbmRvdyk7fSk7fWVsc2UgaWYodHlwZW9mIG1vZHVsZT09PVwib2JqZWN0XCImJm1vZHVsZS5leHBvcnRzKXttb2R1bGUuZXhwb3J0cz1mYWN0b3J5KHJlcXVpcmUoXCJqcXVlcnlcIiksd2luZG93KTt9ZWxzZXtmYWN0b3J5KGpRdWVyeSx3aW5kb3cpO319KShmdW5jdGlvbihqUXVlcnksd2luZG93KXtcInVzZSBzdHJpY3RcIjtqUXVlcnkubWlncmF0ZVZlcnNpb249XCIzLjMuMlwiO2Z1bmN0aW9uIGNvbXBhcmVWZXJzaW9ucyh2MSx2Mil7dmFyIGksclZlcnNpb25QYXJ0cz0vXihcXGQrKVxcLihcXGQrKVxcLihcXGQrKS8sdjFwPXJWZXJzaW9uUGFydHMuZXhlYyh2MSl8fFtdLHYycD1yVmVyc2lvblBhcnRzLmV4ZWModjIpfHxbXTtmb3IoaT0xO2k8PTM7aSsrKXtpZigrdjFwW2ldPit2MnBbaV0pe3JldHVybiAxO31cbmlmKCt2MXBbaV08K3YycFtpXSl7cmV0dXJuLTE7fX1cbnJldHVybiAwO31cbmZ1bmN0aW9uIGpRdWVyeVZlcnNpb25TaW5jZSh2ZXJzaW9uKXtyZXR1cm4gY29tcGFyZVZlcnNpb25zKGpRdWVyeS5mbi5qcXVlcnksdmVyc2lvbik+PTA7fVxuKGZ1bmN0aW9uKCl7aWYoIXdpbmRvdy5jb25zb2xlfHwhd2luZG93LmNvbnNvbGUubG9nKXtyZXR1cm47fVxuaWYoIWpRdWVyeXx8IWpRdWVyeVZlcnNpb25TaW5jZShcIjMuMC4wXCIpKXt3aW5kb3cuY29uc29sZS5sb2coXCJKUU1JR1JBVEU6IGpRdWVyeSAzLjAuMCsgUkVRVUlSRURcIik7fVxuaWYoalF1ZXJ5Lm1pZ3JhdGVXYXJuaW5ncyl7d2luZG93LmNvbnNvbGUubG9nKFwiSlFNSUdSQVRFOiBNaWdyYXRlIHBsdWdpbiBsb2FkZWQgbXVsdGlwbGUgdGltZXNcIik7fVxud2luZG93LmNvbnNvbGUubG9nKFwiSlFNSUdSQVRFOiBNaWdyYXRlIGlzIGluc3RhbGxlZFwiK1xuKGpRdWVyeS5taWdyYXRlTXV0ZT9cIlwiOlwiIHdpdGggbG9nZ2luZyBhY3RpdmVcIikrXCIsIHZlcnNpb24gXCIralF1ZXJ5Lm1pZ3JhdGVWZXJzaW9uKTt9KSgpO3ZhciB3YXJuZWRBYm91dD17fTtqUXVlcnkubWlncmF0ZURlZHVwbGljYXRlV2FybmluZ3M9dHJ1ZTtqUXVlcnkubWlncmF0ZVdhcm5pbmdzPVtdO2lmKGpRdWVyeS5taWdyYXRlVHJhY2U9PT11bmRlZmluZWQpe2pRdWVyeS5taWdyYXRlVHJhY2U9dHJ1ZTt9XG5qUXVlcnkubWlncmF0ZVJlc2V0PWZ1bmN0aW9uKCl7d2FybmVkQWJvdXQ9e307alF1ZXJ5Lm1pZ3JhdGVXYXJuaW5ncy5sZW5ndGg9MDt9O2Z1bmN0aW9uIG1pZ3JhdGVXYXJuKG1zZyl7dmFyIGNvbnNvbGU9d2luZG93LmNvbnNvbGU7aWYoIWpRdWVyeS5taWdyYXRlRGVkdXBsaWNhdGVXYXJuaW5nc3x8IXdhcm5lZEFib3V0W21zZ10pe3dhcm5lZEFib3V0W21zZ109dHJ1ZTtqUXVlcnkubWlncmF0ZVdhcm5pbmdzLnB1c2gobXNnKTtpZihjb25zb2xlJiZjb25zb2xlLndhcm4mJiFqUXVlcnkubWlncmF0ZU11dGUpe2NvbnNvbGUud2FybihcIkpRTUlHUkFURTogXCIrbXNnKTtpZihqUXVlcnkubWlncmF0ZVRyYWNlJiZjb25zb2xlLnRyYWNlKXtjb25zb2xlLnRyYWNlKCk7fX19fVxuZnVuY3Rpb24gbWlncmF0ZVdhcm5Qcm9wKG9iaixwcm9wLHZhbHVlLG1zZyl7T2JqZWN0LmRlZmluZVByb3BlcnR5KG9iaixwcm9wLHtjb25maWd1cmFibGU6dHJ1ZSxlbnVtZXJhYmxlOnRydWUsZ2V0OmZ1bmN0aW9uKCl7bWlncmF0ZVdhcm4obXNnKTtyZXR1cm4gdmFsdWU7fSxzZXQ6ZnVuY3Rpb24obmV3VmFsdWUpe21pZ3JhdGVXYXJuKG1zZyk7dmFsdWU9bmV3VmFsdWU7fX0pO31cbmZ1bmN0aW9uIG1pZ3JhdGVXYXJuRnVuYyhvYmoscHJvcCxuZXdGdW5jLG1zZyl7b2JqW3Byb3BdPWZ1bmN0aW9uKCl7bWlncmF0ZVdhcm4obXNnKTtyZXR1cm4gbmV3RnVuYy5hcHBseSh0aGlzLGFyZ3VtZW50cyk7fTt9XG5pZih3aW5kb3cuZG9jdW1lbnQuY29tcGF0TW9kZT09PVwiQmFja0NvbXBhdFwiKXttaWdyYXRlV2FybihcImpRdWVyeSBpcyBub3QgY29tcGF0aWJsZSB3aXRoIFF1aXJrcyBNb2RlXCIpO31cbnZhciBmaW5kUHJvcCxjbGFzczJ0eXBlPXt9LG9sZEluaXQ9alF1ZXJ5LmZuLmluaXQsb2xkRmluZD1qUXVlcnkuZmluZCxyYXR0ckhhc2hUZXN0PS9cXFsoXFxzKlstXFx3XStcXHMqKShbfnxeJCpdPz0pXFxzKihbLVxcdyNdKj8jWy1cXHcjXSopXFxzKlxcXS8scmF0dHJIYXNoR2xvYj0vXFxbKFxccypbLVxcd10rXFxzKikoW358XiQqXT89KVxccyooWy1cXHcjXSo/I1stXFx3I10qKVxccypcXF0vZyxydHJpbT0vXltcXHNcXHVGRUZGXFx4QTBdK3xbXFxzXFx1RkVGRlxceEEwXSskL2c7alF1ZXJ5LmZuLmluaXQ9ZnVuY3Rpb24oYXJnMSl7dmFyIGFyZ3M9QXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzKTtpZih0eXBlb2YgYXJnMT09PVwic3RyaW5nXCImJmFyZzE9PT1cIiNcIil7bWlncmF0ZVdhcm4oXCJqUXVlcnkoICcjJyApIGlzIG5vdCBhIHZhbGlkIHNlbGVjdG9yXCIpO2FyZ3NbMF09W107fVxucmV0dXJuIG9sZEluaXQuYXBwbHkodGhpcyxhcmdzKTt9O2pRdWVyeS5mbi5pbml0LnByb3RvdHlwZT1qUXVlcnkuZm47alF1ZXJ5LmZpbmQ9ZnVuY3Rpb24oc2VsZWN0b3Ipe3ZhciBhcmdzPUFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cyk7aWYodHlwZW9mIHNlbGVjdG9yPT09XCJzdHJpbmdcIiYmcmF0dHJIYXNoVGVzdC50ZXN0KHNlbGVjdG9yKSl7dHJ5e3dpbmRvdy5kb2N1bWVudC5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKTt9Y2F0Y2goZXJyMSl7c2VsZWN0b3I9c2VsZWN0b3IucmVwbGFjZShyYXR0ckhhc2hHbG9iLGZ1bmN0aW9uKF8sYXR0cixvcCx2YWx1ZSl7cmV0dXJuXCJbXCIrYXR0citvcCtcIlxcXCJcIit2YWx1ZStcIlxcXCJdXCI7fSk7dHJ5e3dpbmRvdy5kb2N1bWVudC5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKTttaWdyYXRlV2FybihcIkF0dHJpYnV0ZSBzZWxlY3RvciB3aXRoICcjJyBtdXN0IGJlIHF1b3RlZDogXCIrYXJnc1swXSk7YXJnc1swXT1zZWxlY3Rvcjt9Y2F0Y2goZXJyMil7bWlncmF0ZVdhcm4oXCJBdHRyaWJ1dGUgc2VsZWN0b3Igd2l0aCAnIycgd2FzIG5vdCBmaXhlZDogXCIrYXJnc1swXSk7fX19XG5yZXR1cm4gb2xkRmluZC5hcHBseSh0aGlzLGFyZ3MpO307Zm9yKGZpbmRQcm9wIGluIG9sZEZpbmQpe2lmKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvbGRGaW5kLGZpbmRQcm9wKSl7alF1ZXJ5LmZpbmRbZmluZFByb3BdPW9sZEZpbmRbZmluZFByb3BdO319XG5taWdyYXRlV2FybkZ1bmMoalF1ZXJ5LmZuLFwic2l6ZVwiLGZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMubGVuZ3RoO30sXCJqUXVlcnkuZm4uc2l6ZSgpIGlzIGRlcHJlY2F0ZWQgYW5kIHJlbW92ZWQ7IHVzZSB0aGUgLmxlbmd0aCBwcm9wZXJ0eVwiKTttaWdyYXRlV2FybkZ1bmMoalF1ZXJ5LFwicGFyc2VKU09OXCIsZnVuY3Rpb24oKXtyZXR1cm4gSlNPTi5wYXJzZS5hcHBseShudWxsLGFyZ3VtZW50cyk7fSxcImpRdWVyeS5wYXJzZUpTT04gaXMgZGVwcmVjYXRlZDsgdXNlIEpTT04ucGFyc2VcIik7bWlncmF0ZVdhcm5GdW5jKGpRdWVyeSxcImhvbGRSZWFkeVwiLGpRdWVyeS5ob2xkUmVhZHksXCJqUXVlcnkuaG9sZFJlYWR5IGlzIGRlcHJlY2F0ZWRcIik7bWlncmF0ZVdhcm5GdW5jKGpRdWVyeSxcInVuaXF1ZVwiLGpRdWVyeS51bmlxdWVTb3J0LFwialF1ZXJ5LnVuaXF1ZSBpcyBkZXByZWNhdGVkOyB1c2UgalF1ZXJ5LnVuaXF1ZVNvcnRcIik7bWlncmF0ZVdhcm5Qcm9wKGpRdWVyeS5leHByLFwiZmlsdGVyc1wiLGpRdWVyeS5leHByLnBzZXVkb3MsXCJqUXVlcnkuZXhwci5maWx0ZXJzIGlzIGRlcHJlY2F0ZWQ7IHVzZSBqUXVlcnkuZXhwci5wc2V1ZG9zXCIpO21pZ3JhdGVXYXJuUHJvcChqUXVlcnkuZXhwcixcIjpcIixqUXVlcnkuZXhwci5wc2V1ZG9zLFwialF1ZXJ5LmV4cHJbJzonXSBpcyBkZXByZWNhdGVkOyB1c2UgalF1ZXJ5LmV4cHIucHNldWRvc1wiKTtpZihqUXVlcnlWZXJzaW9uU2luY2UoXCIzLjEuMVwiKSl7bWlncmF0ZVdhcm5GdW5jKGpRdWVyeSxcInRyaW1cIixmdW5jdGlvbih0ZXh0KXtyZXR1cm4gdGV4dD09bnVsbD9cIlwiOih0ZXh0K1wiXCIpLnJlcGxhY2UocnRyaW0sXCJcIik7fSxcImpRdWVyeS50cmltIGlzIGRlcHJlY2F0ZWQ7IHVzZSBTdHJpbmcucHJvdG90eXBlLnRyaW1cIik7fVxuaWYoalF1ZXJ5VmVyc2lvblNpbmNlKFwiMy4yLjBcIikpe21pZ3JhdGVXYXJuRnVuYyhqUXVlcnksXCJub2RlTmFtZVwiLGZ1bmN0aW9uKGVsZW0sbmFtZSl7cmV0dXJuIGVsZW0ubm9kZU5hbWUmJmVsZW0ubm9kZU5hbWUudG9Mb3dlckNhc2UoKT09PW5hbWUudG9Mb3dlckNhc2UoKTt9LFwialF1ZXJ5Lm5vZGVOYW1lIGlzIGRlcHJlY2F0ZWRcIik7bWlncmF0ZVdhcm5GdW5jKGpRdWVyeSxcImlzQXJyYXlcIixBcnJheS5pc0FycmF5LFwialF1ZXJ5LmlzQXJyYXkgaXMgZGVwcmVjYXRlZDsgdXNlIEFycmF5LmlzQXJyYXlcIik7fVxuaWYoalF1ZXJ5VmVyc2lvblNpbmNlKFwiMy4zLjBcIikpe21pZ3JhdGVXYXJuRnVuYyhqUXVlcnksXCJpc051bWVyaWNcIixmdW5jdGlvbihvYmope3ZhciB0eXBlPXR5cGVvZiBvYmo7cmV0dXJuKHR5cGU9PT1cIm51bWJlclwifHx0eXBlPT09XCJzdHJpbmdcIikmJiFpc05hTihvYmotcGFyc2VGbG9hdChvYmopKTt9LFwialF1ZXJ5LmlzTnVtZXJpYygpIGlzIGRlcHJlY2F0ZWRcIik7alF1ZXJ5LmVhY2goXCJCb29sZWFuIE51bWJlciBTdHJpbmcgRnVuY3Rpb24gQXJyYXkgRGF0ZSBSZWdFeHAgT2JqZWN0IEVycm9yIFN5bWJvbFwiLnNwbGl0KFwiIFwiKSxmdW5jdGlvbihfLG5hbWUpe2NsYXNzMnR5cGVbXCJbb2JqZWN0IFwiK25hbWUrXCJdXCJdPW5hbWUudG9Mb3dlckNhc2UoKTt9KTttaWdyYXRlV2FybkZ1bmMoalF1ZXJ5LFwidHlwZVwiLGZ1bmN0aW9uKG9iail7aWYob2JqPT1udWxsKXtyZXR1cm4gb2JqK1wiXCI7fVxucmV0dXJuIHR5cGVvZiBvYmo9PT1cIm9iamVjdFwifHx0eXBlb2Ygb2JqPT09XCJmdW5jdGlvblwiP2NsYXNzMnR5cGVbT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG9iaildfHxcIm9iamVjdFwiOnR5cGVvZiBvYmo7fSxcImpRdWVyeS50eXBlIGlzIGRlcHJlY2F0ZWRcIik7bWlncmF0ZVdhcm5GdW5jKGpRdWVyeSxcImlzRnVuY3Rpb25cIixmdW5jdGlvbihvYmope3JldHVybiB0eXBlb2Ygb2JqPT09XCJmdW5jdGlvblwiO30sXCJqUXVlcnkuaXNGdW5jdGlvbigpIGlzIGRlcHJlY2F0ZWRcIik7bWlncmF0ZVdhcm5GdW5jKGpRdWVyeSxcImlzV2luZG93XCIsZnVuY3Rpb24ob2JqKXtyZXR1cm4gb2JqIT1udWxsJiZvYmo9PT1vYmoud2luZG93O30sXCJqUXVlcnkuaXNXaW5kb3coKSBpcyBkZXByZWNhdGVkXCIpO31cbmlmKGpRdWVyeS5hamF4KXt2YXIgb2xkQWpheD1qUXVlcnkuYWpheCxyanNvbnA9Lyg9KVxcPyg/PSZ8JCl8XFw/XFw/LztqUXVlcnkuYWpheD1mdW5jdGlvbigpe3ZhciBqUVhIUj1vbGRBamF4LmFwcGx5KHRoaXMsYXJndW1lbnRzKTtpZihqUVhIUi5wcm9taXNlKXttaWdyYXRlV2FybkZ1bmMoalFYSFIsXCJzdWNjZXNzXCIsalFYSFIuZG9uZSxcImpRWEhSLnN1Y2Nlc3MgaXMgZGVwcmVjYXRlZCBhbmQgcmVtb3ZlZFwiKTttaWdyYXRlV2FybkZ1bmMoalFYSFIsXCJlcnJvclwiLGpRWEhSLmZhaWwsXCJqUVhIUi5lcnJvciBpcyBkZXByZWNhdGVkIGFuZCByZW1vdmVkXCIpO21pZ3JhdGVXYXJuRnVuYyhqUVhIUixcImNvbXBsZXRlXCIsalFYSFIuYWx3YXlzLFwialFYSFIuY29tcGxldGUgaXMgZGVwcmVjYXRlZCBhbmQgcmVtb3ZlZFwiKTt9XG5yZXR1cm4galFYSFI7fTtpZighalF1ZXJ5VmVyc2lvblNpbmNlKFwiNC4wLjBcIikpe2pRdWVyeS5hamF4UHJlZmlsdGVyKFwiK2pzb25cIixmdW5jdGlvbihzKXtpZihzLmpzb25wIT09ZmFsc2UmJihyanNvbnAudGVzdChzLnVybCl8fHR5cGVvZiBzLmRhdGE9PT1cInN0cmluZ1wiJiYocy5jb250ZW50VHlwZXx8XCJcIikuaW5kZXhPZihcImFwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZFwiKT09PTAmJnJqc29ucC50ZXN0KHMuZGF0YSkpKXttaWdyYXRlV2FybihcIkpTT04tdG8tSlNPTlAgYXV0by1wcm9tb3Rpb24gaXMgZGVwcmVjYXRlZFwiKTt9fSk7fX1cbnZhciBvbGRSZW1vdmVBdHRyPWpRdWVyeS5mbi5yZW1vdmVBdHRyLG9sZFRvZ2dsZUNsYXNzPWpRdWVyeS5mbi50b2dnbGVDbGFzcyxybWF0Y2hOb25TcGFjZT0vXFxTKy9nO2pRdWVyeS5mbi5yZW1vdmVBdHRyPWZ1bmN0aW9uKG5hbWUpe3ZhciBzZWxmPXRoaXM7alF1ZXJ5LmVhY2gobmFtZS5tYXRjaChybWF0Y2hOb25TcGFjZSksZnVuY3Rpb24oX2ksYXR0cil7aWYoalF1ZXJ5LmV4cHIubWF0Y2guYm9vbC50ZXN0KGF0dHIpKXttaWdyYXRlV2FybihcImpRdWVyeS5mbi5yZW1vdmVBdHRyIG5vIGxvbmdlciBzZXRzIGJvb2xlYW4gcHJvcGVydGllczogXCIrYXR0cik7c2VsZi5wcm9wKGF0dHIsZmFsc2UpO319KTtyZXR1cm4gb2xkUmVtb3ZlQXR0ci5hcHBseSh0aGlzLGFyZ3VtZW50cyk7fTtqUXVlcnkuZm4udG9nZ2xlQ2xhc3M9ZnVuY3Rpb24oc3RhdGUpe2lmKHN0YXRlIT09dW5kZWZpbmVkJiZ0eXBlb2Ygc3RhdGUhPT1cImJvb2xlYW5cIil7cmV0dXJuIG9sZFRvZ2dsZUNsYXNzLmFwcGx5KHRoaXMsYXJndW1lbnRzKTt9XG5taWdyYXRlV2FybihcImpRdWVyeS5mbi50b2dnbGVDbGFzcyggYm9vbGVhbiApIGlzIGRlcHJlY2F0ZWRcIik7cmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbigpe3ZhciBjbGFzc05hbWU9dGhpcy5nZXRBdHRyaWJ1dGUmJnRoaXMuZ2V0QXR0cmlidXRlKFwiY2xhc3NcIil8fFwiXCI7aWYoY2xhc3NOYW1lKXtqUXVlcnkuZGF0YSh0aGlzLFwiX19jbGFzc05hbWVfX1wiLGNsYXNzTmFtZSk7fVxuaWYodGhpcy5zZXRBdHRyaWJ1dGUpe3RoaXMuc2V0QXR0cmlidXRlKFwiY2xhc3NcIixjbGFzc05hbWV8fHN0YXRlPT09ZmFsc2U/XCJcIjpqUXVlcnkuZGF0YSh0aGlzLFwiX19jbGFzc05hbWVfX1wiKXx8XCJcIik7fX0pO307ZnVuY3Rpb24gY2FtZWxDYXNlKHN0cmluZyl7cmV0dXJuIHN0cmluZy5yZXBsYWNlKC8tKFthLXpdKS9nLGZ1bmN0aW9uKF8sbGV0dGVyKXtyZXR1cm4gbGV0dGVyLnRvVXBwZXJDYXNlKCk7fSk7fVxudmFyIG9sZEZuQ3NzLGludGVybmFsU3dhcENhbGw9ZmFsc2UscmFscGhhU3RhcnQ9L15bYS16XS8scmF1dG9QeD0vXig/OkJvcmRlcig/OlRvcHxSaWdodHxCb3R0b218TGVmdCk/KD86V2lkdGh8KXwoPzpNYXJnaW58UGFkZGluZyk/KD86VG9wfFJpZ2h0fEJvdHRvbXxMZWZ0KT98KD86TWlufE1heCk/KD86V2lkdGh8SGVpZ2h0KSkkLztpZihqUXVlcnkuc3dhcCl7alF1ZXJ5LmVhY2goW1wiaGVpZ2h0XCIsXCJ3aWR0aFwiLFwicmVsaWFibGVNYXJnaW5SaWdodFwiXSxmdW5jdGlvbihfLG5hbWUpe3ZhciBvbGRIb29rPWpRdWVyeS5jc3NIb29rc1tuYW1lXSYmalF1ZXJ5LmNzc0hvb2tzW25hbWVdLmdldDtpZihvbGRIb29rKXtqUXVlcnkuY3NzSG9va3NbbmFtZV0uZ2V0PWZ1bmN0aW9uKCl7dmFyIHJldDtpbnRlcm5hbFN3YXBDYWxsPXRydWU7cmV0PW9sZEhvb2suYXBwbHkodGhpcyxhcmd1bWVudHMpO2ludGVybmFsU3dhcENhbGw9ZmFsc2U7cmV0dXJuIHJldDt9O319KTt9XG5qUXVlcnkuc3dhcD1mdW5jdGlvbihlbGVtLG9wdGlvbnMsY2FsbGJhY2ssYXJncyl7dmFyIHJldCxuYW1lLG9sZD17fTtpZighaW50ZXJuYWxTd2FwQ2FsbCl7bWlncmF0ZVdhcm4oXCJqUXVlcnkuc3dhcCgpIGlzIHVuZG9jdW1lbnRlZCBhbmQgZGVwcmVjYXRlZFwiKTt9XG5mb3IobmFtZSBpbiBvcHRpb25zKXtvbGRbbmFtZV09ZWxlbS5zdHlsZVtuYW1lXTtlbGVtLnN0eWxlW25hbWVdPW9wdGlvbnNbbmFtZV07fVxucmV0PWNhbGxiYWNrLmFwcGx5KGVsZW0sYXJnc3x8W10pO2ZvcihuYW1lIGluIG9wdGlvbnMpe2VsZW0uc3R5bGVbbmFtZV09b2xkW25hbWVdO31cbnJldHVybiByZXQ7fTtpZihqUXVlcnlWZXJzaW9uU2luY2UoXCIzLjQuMFwiKSYmdHlwZW9mIFByb3h5IT09XCJ1bmRlZmluZWRcIil7alF1ZXJ5LmNzc1Byb3BzPW5ldyBQcm94eShqUXVlcnkuY3NzUHJvcHN8fHt9LHtzZXQ6ZnVuY3Rpb24oKXttaWdyYXRlV2FybihcIkpRTUlHUkFURTogalF1ZXJ5LmNzc1Byb3BzIGlzIGRlcHJlY2F0ZWRcIik7cmV0dXJuIFJlZmxlY3Quc2V0LmFwcGx5KHRoaXMsYXJndW1lbnRzKTt9fSk7fVxuaWYoIWpRdWVyeS5jc3NOdW1iZXIpe2pRdWVyeS5jc3NOdW1iZXI9e307fVxuZnVuY3Rpb24gaXNBdXRvUHgocHJvcCl7cmV0dXJuIHJhbHBoYVN0YXJ0LnRlc3QocHJvcCkmJnJhdXRvUHgudGVzdChwcm9wWzBdLnRvVXBwZXJDYXNlKCkrcHJvcC5zbGljZSgxKSk7fVxub2xkRm5Dc3M9alF1ZXJ5LmZuLmNzcztqUXVlcnkuZm4uY3NzPWZ1bmN0aW9uKG5hbWUsdmFsdWUpe3ZhciBjYW1lbE5hbWUsb3JpZ1RoaXM9dGhpcztpZihuYW1lJiZ0eXBlb2YgbmFtZT09PVwib2JqZWN0XCImJiFBcnJheS5pc0FycmF5KG5hbWUpKXtqUXVlcnkuZWFjaChuYW1lLGZ1bmN0aW9uKG4sdil7alF1ZXJ5LmZuLmNzcy5jYWxsKG9yaWdUaGlzLG4sdik7fSk7cmV0dXJuIHRoaXM7fVxuaWYodHlwZW9mIHZhbHVlPT09XCJudW1iZXJcIil7Y2FtZWxOYW1lPWNhbWVsQ2FzZShuYW1lKTtpZighaXNBdXRvUHgoY2FtZWxOYW1lKSYmIWpRdWVyeS5jc3NOdW1iZXJbY2FtZWxOYW1lXSl7bWlncmF0ZVdhcm4oXCJOdW1iZXItdHlwZWQgdmFsdWVzIGFyZSBkZXByZWNhdGVkIGZvciBqUXVlcnkuZm4uY3NzKCBcXFwiXCIrXG5uYW1lK1wiXFxcIiwgdmFsdWUgKVwiKTt9fVxucmV0dXJuIG9sZEZuQ3NzLmFwcGx5KHRoaXMsYXJndW1lbnRzKTt9O3ZhciBvbGREYXRhPWpRdWVyeS5kYXRhO2pRdWVyeS5kYXRhPWZ1bmN0aW9uKGVsZW0sbmFtZSx2YWx1ZSl7dmFyIGN1ckRhdGEsc2FtZUtleXMsa2V5O2lmKG5hbWUmJnR5cGVvZiBuYW1lPT09XCJvYmplY3RcIiYmYXJndW1lbnRzLmxlbmd0aD09PTIpe2N1ckRhdGE9alF1ZXJ5Lmhhc0RhdGEoZWxlbSkmJm9sZERhdGEuY2FsbCh0aGlzLGVsZW0pO3NhbWVLZXlzPXt9O2ZvcihrZXkgaW4gbmFtZSl7aWYoa2V5IT09Y2FtZWxDYXNlKGtleSkpe21pZ3JhdGVXYXJuKFwialF1ZXJ5LmRhdGEoKSBhbHdheXMgc2V0cy9nZXRzIGNhbWVsQ2FzZWQgbmFtZXM6IFwiK2tleSk7Y3VyRGF0YVtrZXldPW5hbWVba2V5XTt9ZWxzZXtzYW1lS2V5c1trZXldPW5hbWVba2V5XTt9fVxub2xkRGF0YS5jYWxsKHRoaXMsZWxlbSxzYW1lS2V5cyk7cmV0dXJuIG5hbWU7fVxuaWYobmFtZSYmdHlwZW9mIG5hbWU9PT1cInN0cmluZ1wiJiZuYW1lIT09Y2FtZWxDYXNlKG5hbWUpKXtjdXJEYXRhPWpRdWVyeS5oYXNEYXRhKGVsZW0pJiZvbGREYXRhLmNhbGwodGhpcyxlbGVtKTtpZihjdXJEYXRhJiZuYW1lIGluIGN1ckRhdGEpe21pZ3JhdGVXYXJuKFwialF1ZXJ5LmRhdGEoKSBhbHdheXMgc2V0cy9nZXRzIGNhbWVsQ2FzZWQgbmFtZXM6IFwiK25hbWUpO2lmKGFyZ3VtZW50cy5sZW5ndGg+Mil7Y3VyRGF0YVtuYW1lXT12YWx1ZTt9XG5yZXR1cm4gY3VyRGF0YVtuYW1lXTt9fVxucmV0dXJuIG9sZERhdGEuYXBwbHkodGhpcyxhcmd1bWVudHMpO307aWYoalF1ZXJ5LmZ4KXt2YXIgaW50ZXJ2YWxWYWx1ZSxpbnRlcnZhbE1zZyxvbGRUd2VlblJ1bj1qUXVlcnkuVHdlZW4ucHJvdG90eXBlLnJ1bixsaW5lYXJFYXNpbmc9ZnVuY3Rpb24ocGN0KXtyZXR1cm4gcGN0O307alF1ZXJ5LlR3ZWVuLnByb3RvdHlwZS5ydW49ZnVuY3Rpb24oKXtpZihqUXVlcnkuZWFzaW5nW3RoaXMuZWFzaW5nXS5sZW5ndGg+MSl7bWlncmF0ZVdhcm4oXCInalF1ZXJ5LmVhc2luZy5cIit0aGlzLmVhc2luZy50b1N0cmluZygpK1wiJyBzaG91bGQgdXNlIG9ubHkgb25lIGFyZ3VtZW50XCIpO2pRdWVyeS5lYXNpbmdbdGhpcy5lYXNpbmddPWxpbmVhckVhc2luZzt9XG5vbGRUd2VlblJ1bi5hcHBseSh0aGlzLGFyZ3VtZW50cyk7fTtpbnRlcnZhbFZhbHVlPWpRdWVyeS5meC5pbnRlcnZhbHx8MTM7aW50ZXJ2YWxNc2c9XCJqUXVlcnkuZnguaW50ZXJ2YWwgaXMgZGVwcmVjYXRlZFwiO2lmKHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUpe09iamVjdC5kZWZpbmVQcm9wZXJ0eShqUXVlcnkuZngsXCJpbnRlcnZhbFwiLHtjb25maWd1cmFibGU6dHJ1ZSxlbnVtZXJhYmxlOnRydWUsZ2V0OmZ1bmN0aW9uKCl7aWYoIXdpbmRvdy5kb2N1bWVudC5oaWRkZW4pe21pZ3JhdGVXYXJuKGludGVydmFsTXNnKTt9XG5yZXR1cm4gaW50ZXJ2YWxWYWx1ZTt9LHNldDpmdW5jdGlvbihuZXdWYWx1ZSl7bWlncmF0ZVdhcm4oaW50ZXJ2YWxNc2cpO2ludGVydmFsVmFsdWU9bmV3VmFsdWU7fX0pO319XG52YXIgb2xkTG9hZD1qUXVlcnkuZm4ubG9hZCxvbGRFdmVudEFkZD1qUXVlcnkuZXZlbnQuYWRkLG9yaWdpbmFsRml4PWpRdWVyeS5ldmVudC5maXg7alF1ZXJ5LmV2ZW50LnByb3BzPVtdO2pRdWVyeS5ldmVudC5maXhIb29rcz17fTttaWdyYXRlV2FyblByb3AoalF1ZXJ5LmV2ZW50LnByb3BzLFwiY29uY2F0XCIsalF1ZXJ5LmV2ZW50LnByb3BzLmNvbmNhdCxcImpRdWVyeS5ldmVudC5wcm9wcy5jb25jYXQoKSBpcyBkZXByZWNhdGVkIGFuZCByZW1vdmVkXCIpO2pRdWVyeS5ldmVudC5maXg9ZnVuY3Rpb24ob3JpZ2luYWxFdmVudCl7dmFyIGV2ZW50LHR5cGU9b3JpZ2luYWxFdmVudC50eXBlLGZpeEhvb2s9dGhpcy5maXhIb29rc1t0eXBlXSxwcm9wcz1qUXVlcnkuZXZlbnQucHJvcHM7aWYocHJvcHMubGVuZ3RoKXttaWdyYXRlV2FybihcImpRdWVyeS5ldmVudC5wcm9wcyBhcmUgZGVwcmVjYXRlZCBhbmQgcmVtb3ZlZDogXCIrcHJvcHMuam9pbigpKTt3aGlsZShwcm9wcy5sZW5ndGgpe2pRdWVyeS5ldmVudC5hZGRQcm9wKHByb3BzLnBvcCgpKTt9fVxuaWYoZml4SG9vayYmIWZpeEhvb2suX21pZ3JhdGVkXyl7Zml4SG9vay5fbWlncmF0ZWRfPXRydWU7bWlncmF0ZVdhcm4oXCJqUXVlcnkuZXZlbnQuZml4SG9va3MgYXJlIGRlcHJlY2F0ZWQgYW5kIHJlbW92ZWQ6IFwiK3R5cGUpO2lmKChwcm9wcz1maXhIb29rLnByb3BzKSYmcHJvcHMubGVuZ3RoKXt3aGlsZShwcm9wcy5sZW5ndGgpe2pRdWVyeS5ldmVudC5hZGRQcm9wKHByb3BzLnBvcCgpKTt9fX1cbmV2ZW50PW9yaWdpbmFsRml4LmNhbGwodGhpcyxvcmlnaW5hbEV2ZW50KTtyZXR1cm4gZml4SG9vayYmZml4SG9vay5maWx0ZXI/Zml4SG9vay5maWx0ZXIoZXZlbnQsb3JpZ2luYWxFdmVudCk6ZXZlbnQ7fTtqUXVlcnkuZXZlbnQuYWRkPWZ1bmN0aW9uKGVsZW0sdHlwZXMpe2lmKGVsZW09PT13aW5kb3cmJnR5cGVzPT09XCJsb2FkXCImJndpbmRvdy5kb2N1bWVudC5yZWFkeVN0YXRlPT09XCJjb21wbGV0ZVwiKXttaWdyYXRlV2FybihcImpRdWVyeSh3aW5kb3cpLm9uKCdsb2FkJy4uLikgY2FsbGVkIGFmdGVyIGxvYWQgZXZlbnQgb2NjdXJyZWRcIik7fVxucmV0dXJuIG9sZEV2ZW50QWRkLmFwcGx5KHRoaXMsYXJndW1lbnRzKTt9O2pRdWVyeS5lYWNoKFtcImxvYWRcIixcInVubG9hZFwiLFwiZXJyb3JcIl0sZnVuY3Rpb24oXyxuYW1lKXtqUXVlcnkuZm5bbmFtZV09ZnVuY3Rpb24oKXt2YXIgYXJncz1BcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsMCk7aWYobmFtZT09PVwibG9hZFwiJiZ0eXBlb2YgYXJnc1swXT09PVwic3RyaW5nXCIpe3JldHVybiBvbGRMb2FkLmFwcGx5KHRoaXMsYXJncyk7fVxubWlncmF0ZVdhcm4oXCJqUXVlcnkuZm4uXCIrbmFtZStcIigpIGlzIGRlcHJlY2F0ZWRcIik7YXJncy5zcGxpY2UoMCwwLG5hbWUpO2lmKGFyZ3VtZW50cy5sZW5ndGgpe3JldHVybiB0aGlzLm9uLmFwcGx5KHRoaXMsYXJncyk7fVxudGhpcy50cmlnZ2VySGFuZGxlci5hcHBseSh0aGlzLGFyZ3MpO3JldHVybiB0aGlzO307fSk7alF1ZXJ5LmVhY2goKFwiYmx1ciBmb2N1cyBmb2N1c2luIGZvY3Vzb3V0IHJlc2l6ZSBzY3JvbGwgY2xpY2sgZGJsY2xpY2sgXCIrXCJtb3VzZWRvd24gbW91c2V1cCBtb3VzZW1vdmUgbW91c2VvdmVyIG1vdXNlb3V0IG1vdXNlZW50ZXIgbW91c2VsZWF2ZSBcIitcImNoYW5nZSBzZWxlY3Qgc3VibWl0IGtleWRvd24ga2V5cHJlc3Mga2V5dXAgY29udGV4dG1lbnVcIikuc3BsaXQoXCIgXCIpLGZ1bmN0aW9uKF9pLG5hbWUpe2pRdWVyeS5mbltuYW1lXT1mdW5jdGlvbihkYXRhLGZuKXttaWdyYXRlV2FybihcImpRdWVyeS5mbi5cIituYW1lK1wiKCkgZXZlbnQgc2hvcnRoYW5kIGlzIGRlcHJlY2F0ZWRcIik7cmV0dXJuIGFyZ3VtZW50cy5sZW5ndGg+MD90aGlzLm9uKG5hbWUsbnVsbCxkYXRhLGZuKTp0aGlzLnRyaWdnZXIobmFtZSk7fTt9KTtqUXVlcnkoZnVuY3Rpb24oKXtqUXVlcnkod2luZG93LmRvY3VtZW50KS50cmlnZ2VySGFuZGxlcihcInJlYWR5XCIpO30pO2pRdWVyeS5ldmVudC5zcGVjaWFsLnJlYWR5PXtzZXR1cDpmdW5jdGlvbigpe2lmKHRoaXM9PT13aW5kb3cuZG9jdW1lbnQpe21pZ3JhdGVXYXJuKFwiJ3JlYWR5JyBldmVudCBpcyBkZXByZWNhdGVkXCIpO319fTtqUXVlcnkuZm4uZXh0ZW5kKHtiaW5kOmZ1bmN0aW9uKHR5cGVzLGRhdGEsZm4pe21pZ3JhdGVXYXJuKFwialF1ZXJ5LmZuLmJpbmQoKSBpcyBkZXByZWNhdGVkXCIpO3JldHVybiB0aGlzLm9uKHR5cGVzLG51bGwsZGF0YSxmbik7fSx1bmJpbmQ6ZnVuY3Rpb24odHlwZXMsZm4pe21pZ3JhdGVXYXJuKFwialF1ZXJ5LmZuLnVuYmluZCgpIGlzIGRlcHJlY2F0ZWRcIik7cmV0dXJuIHRoaXMub2ZmKHR5cGVzLG51bGwsZm4pO30sZGVsZWdhdGU6ZnVuY3Rpb24oc2VsZWN0b3IsdHlwZXMsZGF0YSxmbil7bWlncmF0ZVdhcm4oXCJqUXVlcnkuZm4uZGVsZWdhdGUoKSBpcyBkZXByZWNhdGVkXCIpO3JldHVybiB0aGlzLm9uKHR5cGVzLHNlbGVjdG9yLGRhdGEsZm4pO30sdW5kZWxlZ2F0ZTpmdW5jdGlvbihzZWxlY3Rvcix0eXBlcyxmbil7bWlncmF0ZVdhcm4oXCJqUXVlcnkuZm4udW5kZWxlZ2F0ZSgpIGlzIGRlcHJlY2F0ZWRcIik7cmV0dXJuIGFyZ3VtZW50cy5sZW5ndGg9PT0xP3RoaXMub2ZmKHNlbGVjdG9yLFwiKipcIik6dGhpcy5vZmYodHlwZXMsc2VsZWN0b3J8fFwiKipcIixmbik7fSxob3ZlcjpmdW5jdGlvbihmbk92ZXIsZm5PdXQpe21pZ3JhdGVXYXJuKFwialF1ZXJ5LmZuLmhvdmVyKCkgaXMgZGVwcmVjYXRlZFwiKTtyZXR1cm4gdGhpcy5vbihcIm1vdXNlZW50ZXJcIixmbk92ZXIpLm9uKFwibW91c2VsZWF2ZVwiLGZuT3V0fHxmbk92ZXIpO319KTt2YXIgcnhodG1sVGFnPS88KD8hYXJlYXxicnxjb2x8ZW1iZWR8aHJ8aW1nfGlucHV0fGxpbmt8bWV0YXxwYXJhbSkoKFthLXpdW15cXC9cXDA+XFx4MjBcXHRcXHJcXG5cXGZdKilbXj5dKilcXC8+L2dpLG9yaWdIdG1sUHJlZmlsdGVyPWpRdWVyeS5odG1sUHJlZmlsdGVyLG1ha2VNYXJrdXA9ZnVuY3Rpb24oaHRtbCl7dmFyIGRvYz13aW5kb3cuZG9jdW1lbnQuaW1wbGVtZW50YXRpb24uY3JlYXRlSFRNTERvY3VtZW50KFwiXCIpO2RvYy5ib2R5LmlubmVySFRNTD1odG1sO3JldHVybiBkb2MuYm9keSYmZG9jLmJvZHkuaW5uZXJIVE1MO30sd2FybklmQ2hhbmdlZD1mdW5jdGlvbihodG1sKXt2YXIgY2hhbmdlZD1odG1sLnJlcGxhY2UocnhodG1sVGFnLFwiPCQxPjwvJDI+XCIpO2lmKGNoYW5nZWQhPT1odG1sJiZtYWtlTWFya3VwKGh0bWwpIT09bWFrZU1hcmt1cChjaGFuZ2VkKSl7bWlncmF0ZVdhcm4oXCJIVE1MIHRhZ3MgbXVzdCBiZSBwcm9wZXJseSBuZXN0ZWQgYW5kIGNsb3NlZDogXCIraHRtbCk7fX07alF1ZXJ5LlVOU0FGRV9yZXN0b3JlTGVnYWN5SHRtbFByZWZpbHRlcj1mdW5jdGlvbigpe2pRdWVyeS5odG1sUHJlZmlsdGVyPWZ1bmN0aW9uKGh0bWwpe3dhcm5JZkNoYW5nZWQoaHRtbCk7cmV0dXJuIGh0bWwucmVwbGFjZShyeGh0bWxUYWcsXCI8JDE+PC8kMj5cIik7fTt9O2pRdWVyeS5odG1sUHJlZmlsdGVyPWZ1bmN0aW9uKGh0bWwpe3dhcm5JZkNoYW5nZWQoaHRtbCk7cmV0dXJuIG9yaWdIdG1sUHJlZmlsdGVyKGh0bWwpO307dmFyIG9sZE9mZnNldD1qUXVlcnkuZm4ub2Zmc2V0O2pRdWVyeS5mbi5vZmZzZXQ9ZnVuY3Rpb24oKXt2YXIgZWxlbT10aGlzWzBdO2lmKGVsZW0mJighZWxlbS5ub2RlVHlwZXx8IWVsZW0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KSl7bWlncmF0ZVdhcm4oXCJqUXVlcnkuZm4ub2Zmc2V0KCkgcmVxdWlyZXMgYSB2YWxpZCBET00gZWxlbWVudFwiKTtyZXR1cm4gYXJndW1lbnRzLmxlbmd0aD90aGlzOnVuZGVmaW5lZDt9XG5yZXR1cm4gb2xkT2Zmc2V0LmFwcGx5KHRoaXMsYXJndW1lbnRzKTt9O2lmKGpRdWVyeS5hamF4KXt2YXIgb2xkUGFyYW09alF1ZXJ5LnBhcmFtO2pRdWVyeS5wYXJhbT1mdW5jdGlvbihkYXRhLHRyYWRpdGlvbmFsKXt2YXIgYWpheFRyYWRpdGlvbmFsPWpRdWVyeS5hamF4U2V0dGluZ3MmJmpRdWVyeS5hamF4U2V0dGluZ3MudHJhZGl0aW9uYWw7aWYodHJhZGl0aW9uYWw9PT11bmRlZmluZWQmJmFqYXhUcmFkaXRpb25hbCl7bWlncmF0ZVdhcm4oXCJqUXVlcnkucGFyYW0oKSBubyBsb25nZXIgdXNlcyBqUXVlcnkuYWpheFNldHRpbmdzLnRyYWRpdGlvbmFsXCIpO3RyYWRpdGlvbmFsPWFqYXhUcmFkaXRpb25hbDt9XG5yZXR1cm4gb2xkUGFyYW0uY2FsbCh0aGlzLGRhdGEsdHJhZGl0aW9uYWwpO307fVxudmFyIG9sZFNlbGY9alF1ZXJ5LmZuLmFuZFNlbGZ8fGpRdWVyeS5mbi5hZGRCYWNrO2pRdWVyeS5mbi5hbmRTZWxmPWZ1bmN0aW9uKCl7bWlncmF0ZVdhcm4oXCJqUXVlcnkuZm4uYW5kU2VsZigpIGlzIGRlcHJlY2F0ZWQgYW5kIHJlbW92ZWQsIHVzZSBqUXVlcnkuZm4uYWRkQmFjaygpXCIpO3JldHVybiBvbGRTZWxmLmFwcGx5KHRoaXMsYXJndW1lbnRzKTt9O2lmKGpRdWVyeS5EZWZlcnJlZCl7dmFyIG9sZERlZmVycmVkPWpRdWVyeS5EZWZlcnJlZCx0dXBsZXM9W1tcInJlc29sdmVcIixcImRvbmVcIixqUXVlcnkuQ2FsbGJhY2tzKFwib25jZSBtZW1vcnlcIiksalF1ZXJ5LkNhbGxiYWNrcyhcIm9uY2UgbWVtb3J5XCIpLFwicmVzb2x2ZWRcIl0sW1wicmVqZWN0XCIsXCJmYWlsXCIsalF1ZXJ5LkNhbGxiYWNrcyhcIm9uY2UgbWVtb3J5XCIpLGpRdWVyeS5DYWxsYmFja3MoXCJvbmNlIG1lbW9yeVwiKSxcInJlamVjdGVkXCJdLFtcIm5vdGlmeVwiLFwicHJvZ3Jlc3NcIixqUXVlcnkuQ2FsbGJhY2tzKFwibWVtb3J5XCIpLGpRdWVyeS5DYWxsYmFja3MoXCJtZW1vcnlcIildXTtqUXVlcnkuRGVmZXJyZWQ9ZnVuY3Rpb24oZnVuYyl7dmFyIGRlZmVycmVkPW9sZERlZmVycmVkKCkscHJvbWlzZT1kZWZlcnJlZC5wcm9taXNlKCk7ZGVmZXJyZWQucGlwZT1wcm9taXNlLnBpcGU9ZnVuY3Rpb24oKXt2YXIgZm5zPWFyZ3VtZW50czttaWdyYXRlV2FybihcImRlZmVycmVkLnBpcGUoKSBpcyBkZXByZWNhdGVkXCIpO3JldHVybiBqUXVlcnkuRGVmZXJyZWQoZnVuY3Rpb24obmV3RGVmZXIpe2pRdWVyeS5lYWNoKHR1cGxlcyxmdW5jdGlvbihpLHR1cGxlKXt2YXIgZm49dHlwZW9mIGZuc1tpXT09PVwiZnVuY3Rpb25cIiYmZm5zW2ldO2RlZmVycmVkW3R1cGxlWzFdXShmdW5jdGlvbigpe3ZhciByZXR1cm5lZD1mbiYmZm4uYXBwbHkodGhpcyxhcmd1bWVudHMpO2lmKHJldHVybmVkJiZ0eXBlb2YgcmV0dXJuZWQucHJvbWlzZT09PVwiZnVuY3Rpb25cIil7cmV0dXJuZWQucHJvbWlzZSgpLmRvbmUobmV3RGVmZXIucmVzb2x2ZSkuZmFpbChuZXdEZWZlci5yZWplY3QpLnByb2dyZXNzKG5ld0RlZmVyLm5vdGlmeSk7fWVsc2V7bmV3RGVmZXJbdHVwbGVbMF0rXCJXaXRoXCJdKHRoaXM9PT1wcm9taXNlP25ld0RlZmVyLnByb21pc2UoKTp0aGlzLGZuP1tyZXR1cm5lZF06YXJndW1lbnRzKTt9fSk7fSk7Zm5zPW51bGw7fSkucHJvbWlzZSgpO307aWYoZnVuYyl7ZnVuYy5jYWxsKGRlZmVycmVkLGRlZmVycmVkKTt9XG5yZXR1cm4gZGVmZXJyZWQ7fTtqUXVlcnkuRGVmZXJyZWQuZXhjZXB0aW9uSG9vaz1vbGREZWZlcnJlZC5leGNlcHRpb25Ib29rO31cbnJldHVybiBqUXVlcnk7fSk7Il0sInNvdXJjZVJvb3QiOiIifQ==