(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./resources/js/frontend/31cd1.js?ver=5.3.6":
/*!**************************************************!*\
  !*** ./resources/js/frontend/31cd1.js?ver=5.3.6 ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_LOCAL_MODULE_0__, __WEBPACK_LOCAL_MODULE_0__factory, __WEBPACK_LOCAL_MODULE_0__module;var __WEBPACK_LOCAL_MODULE_1__, __WEBPACK_LOCAL_MODULE_1__factory, __WEBPACK_LOCAL_MODULE_1__module;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_LOCAL_MODULE_2__;var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var require;var require;function _typeof2(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

;
/*!
 * jQuery & Zepto Lazy - v1.7.7
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2017, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * $("img.lazy").lazy();
 */

(function (window, undefined) {
  "use strict";

  var $ = window.jQuery || window.Zepto,
      lazyInstanceId = 0,
      windowLoaded = false;

  $.fn.Lazy = $.fn.lazy = function (settings) {
    return new LazyPlugin(this, settings);
  };

  $.Lazy = $.lazy = function (names, elements, loader) {
    if ($.isFunction(elements)) {
      loader = elements;
      elements = [];
    }

    if (!$.isFunction(loader)) {
      return;
    }

    names = $.isArray(names) ? names : [names];
    elements = $.isArray(elements) ? elements : [elements];
    var config = LazyPlugin.prototype.config,
        forced = config._f || (config._f = {});

    for (var i = 0, l = names.length; i < l; i++) {
      if (config[names[i]] === undefined || $.isFunction(config[names[i]])) {
        config[names[i]] = loader;
      }
    }

    for (var c = 0, a = elements.length; c < a; c++) {
      forced[elements[c]] = names[0];
    }
  };

  function _executeLazy(instance, config, items, events, namespace) {
    var _awaitingAfterLoad = 0,
        _actualWidth = -1,
        _actualHeight = -1,
        _isRetinaDisplay = false,
        _afterLoad = "afterLoad",
        _load = "load",
        _error = "error",
        _img = "img",
        _src = "src",
        _srcset = "srcset",
        _sizes = "sizes",
        _backgroundImage = "background-image";

    function _initialize() {
      _isRetinaDisplay = window.devicePixelRatio > 1;
      items = _prepareItems(items);

      if (config.delay >= 0) {
        setTimeout(function () {
          _lazyLoadItems(true);
        }, config.delay);
      }

      if (config.delay < 0 || config.combined) {
        events.e = _throttle(config.throttle, function (event) {
          if (event.type === "resize") {
            _actualWidth = _actualHeight = -1;
          }

          _lazyLoadItems(event.all);
        });

        events.a = function (additionalItems) {
          additionalItems = _prepareItems(additionalItems);
          items.push.apply(items, additionalItems);
        };

        events.g = function () {
          return items = $(items).filter(function () {
            return !$(this).data(config.loadedName);
          });
        };

        events.f = function (forcedItems) {
          for (var i = 0; i < forcedItems.length; i++) {
            var item = items.filter(function () {
              return this === forcedItems[i];
            });

            if (item.length) {
              _lazyLoadItems(false, item);
            }
          }
        };

        _lazyLoadItems();

        $(config.appendScroll).on("scroll." + namespace + " resize." + namespace, events.e);
      }
    }

    function _prepareItems(items) {
      var defaultImage = config.defaultImage,
          placeholder = config.placeholder,
          imageBase = config.imageBase,
          srcsetAttribute = config.srcsetAttribute,
          loaderAttribute = config.loaderAttribute,
          forcedTags = config._f || {};
      items = $(items).filter(function () {
        var element = $(this),
            tag = _getElementTagName(this);

        return !element.data(config.handledName) && (element.attr(config.attribute) || element.attr(srcsetAttribute) || element.attr(loaderAttribute) || forcedTags[tag] !== undefined);
      }).data("plugin_" + config.name, instance);

      for (var i = 0, l = items.length; i < l; i++) {
        var element = $(items[i]),
            tag = _getElementTagName(items[i]),
            elementImageBase = element.attr(config.imageBaseAttribute) || imageBase;

        if (tag === _img && elementImageBase && element.attr(srcsetAttribute)) {
          element.attr(srcsetAttribute, _getCorrectedSrcSet(element.attr(srcsetAttribute), elementImageBase));
        }

        if (forcedTags[tag] !== undefined && !element.attr(loaderAttribute)) {
          element.attr(loaderAttribute, forcedTags[tag]);
        }

        if (tag === _img && defaultImage && !element.attr(_src)) {
          element.attr(_src, defaultImage);
        } else if (tag !== _img && placeholder && (!element.css(_backgroundImage) || element.css(_backgroundImage) === "none")) {
          element.css(_backgroundImage, "url('" + placeholder + "')");
        }
      }

      return items;
    }

    function _lazyLoadItems(allItems, forced) {
      if (!items.length) {
        if (config.autoDestroy) {
          instance.destroy();
        }

        return;
      }

      var elements = forced || items,
          loadTriggered = false,
          imageBase = config.imageBase || "",
          srcsetAttribute = config.srcsetAttribute,
          handledName = config.handledName;

      for (var i = 0; i < elements.length; i++) {
        if (allItems || forced || _isInLoadableArea(elements[i])) {
          var element = $(elements[i]),
              tag = _getElementTagName(elements[i]),
              attribute = element.attr(config.attribute),
              elementImageBase = element.attr(config.imageBaseAttribute) || imageBase,
              customLoader = element.attr(config.loaderAttribute);

          if (!element.data(handledName) && (!config.visibleOnly || element.is(":visible")) && ((attribute || element.attr(srcsetAttribute)) && (tag === _img && (elementImageBase + attribute !== element.attr(_src) || element.attr(srcsetAttribute) !== element.attr(_srcset)) || tag !== _img && elementImageBase + attribute !== element.css(_backgroundImage)) || customLoader)) {
            loadTriggered = true;
            element.data(handledName, true);

            _handleItem(element, tag, elementImageBase, customLoader);
          }
        }
      }

      if (loadTriggered) {
        items = $(items).filter(function () {
          return !$(this).data(handledName);
        });
      }
    }

    function _handleItem(element, tag, imageBase, customLoader) {
      ++_awaitingAfterLoad;

      var _errorCallback = function errorCallback() {
        _triggerCallback("onError", element);

        _reduceAwaiting();

        _errorCallback = $.noop;
      };

      _triggerCallback("beforeLoad", element);

      var srcAttribute = config.attribute,
          srcsetAttribute = config.srcsetAttribute,
          sizesAttribute = config.sizesAttribute,
          retinaAttribute = config.retinaAttribute,
          removeAttribute = config.removeAttribute,
          loadedName = config.loadedName,
          elementRetina = element.attr(retinaAttribute);

      if (customLoader) {
        var _loadCallback = function loadCallback() {
          if (removeAttribute) {
            element.removeAttr(config.loaderAttribute);
          }

          element.data(loadedName, true);

          _triggerCallback(_afterLoad, element);

          setTimeout(_reduceAwaiting, 1);
          _loadCallback = $.noop;
        };

        element.off(_error).one(_error, _errorCallback).one(_load, _loadCallback);
        if (!_triggerCallback(customLoader, element, function (response) {
          if (response) {
            element.off(_load);

            _loadCallback();
          } else {
            element.off(_error);

            _errorCallback();
          }
        })) element.trigger(_error);
      } else {
        var imageObj = $(new Image());
        imageObj.one(_error, _errorCallback).one(_load, function () {
          element.hide();

          if (tag === _img) {
            element.attr(_sizes, imageObj.attr(_sizes)).attr(_srcset, imageObj.attr(_srcset)).attr(_src, imageObj.attr(_src));
          } else {
            element.css(_backgroundImage, "url('" + imageObj.attr(_src) + "')");
          }

          element[config.effect](config.effectTime);

          if (removeAttribute) {
            element.removeAttr(srcAttribute + " " + srcsetAttribute + " " + retinaAttribute + " " + config.imageBaseAttribute);

            if (sizesAttribute !== _sizes) {
              element.removeAttr(sizesAttribute);
            }
          }

          element.data(loadedName, true);

          _triggerCallback(_afterLoad, element);

          imageObj.remove();

          _reduceAwaiting();
        });
        var imageSrc = (_isRetinaDisplay && elementRetina ? elementRetina : element.attr(srcAttribute)) || "";
        imageObj.attr(_sizes, element.attr(sizesAttribute)).attr(_srcset, element.attr(srcsetAttribute)).attr(_src, imageSrc ? imageBase + imageSrc : null);
        imageObj.complete && imageObj.trigger(_load);
      }
    }

    function _isInLoadableArea(element) {
      var elementBound = element.getBoundingClientRect(),
          direction = config.scrollDirection,
          threshold = config.threshold,
          vertical = _getActualHeight() + threshold > elementBound.top && -threshold < elementBound.bottom,
          horizontal = _getActualWidth() + threshold > elementBound.left && -threshold < elementBound.right;

      if (direction === "vertical") {
        return vertical;
      } else if (direction === "horizontal") {
        return horizontal;
      }

      return vertical && horizontal;
    }

    function _getActualWidth() {
      return _actualWidth >= 0 ? _actualWidth : _actualWidth = $(window).width();
    }

    function _getActualHeight() {
      return _actualHeight >= 0 ? _actualHeight : _actualHeight = $(window).height();
    }

    function _getElementTagName(element) {
      return element.tagName.toLowerCase();
    }

    function _getCorrectedSrcSet(srcset, imageBase) {
      if (imageBase) {
        var entries = srcset.split(",");
        srcset = "";

        for (var i = 0, l = entries.length; i < l; i++) {
          srcset += imageBase + entries[i].trim() + (i !== l - 1 ? "," : "");
        }
      }

      return srcset;
    }

    function _throttle(delay, callback) {
      var timeout,
          lastExecute = 0;
      return function (event, ignoreThrottle) {
        var elapsed = +new Date() - lastExecute;

        function run() {
          lastExecute = +new Date();
          callback.call(instance, event);
        }

        timeout && clearTimeout(timeout);

        if (elapsed > delay || !config.enableThrottle || ignoreThrottle) {
          run();
        } else {
          timeout = setTimeout(run, delay - elapsed);
        }
      };
    }

    function _reduceAwaiting() {
      --_awaitingAfterLoad;

      if (!items.length && !_awaitingAfterLoad) {
        _triggerCallback("onFinishedAll");
      }
    }

    function _triggerCallback(callback, element, args) {
      if (callback = config[callback]) {
        callback.apply(instance, [].slice.call(arguments, 1));
        return true;
      }

      return false;
    }

    if (config.bind === "event" || windowLoaded) {
      _initialize();
    } else {
      $(window).on(_load + "." + namespace, _initialize);
    }
  }

  function LazyPlugin(elements, settings) {
    var _instance = this,
        _config = $.extend({}, _instance.config, settings),
        _events = {},
        _namespace = _config.name + "-" + ++lazyInstanceId;

    _instance.config = function (entryName, value) {
      if (value === undefined) {
        return _config[entryName];
      }

      _config[entryName] = value;
      return _instance;
    };

    _instance.addItems = function (items) {
      _events.a && _events.a($.type(items) === "string" ? $(items) : items);
      return _instance;
    };

    _instance.getItems = function () {
      return _events.g ? _events.g() : {};
    };

    _instance.update = function (useThrottle) {
      _events.e && _events.e({}, !useThrottle);
      return _instance;
    };

    _instance.force = function (items) {
      _events.f && _events.f($.type(items) === "string" ? $(items) : items);
      return _instance;
    };

    _instance.loadAll = function () {
      _events.e && _events.e({
        all: true
      }, true);
      return _instance;
    };

    _instance.destroy = function () {
      $(_config.appendScroll).off("." + _namespace, _events.e);
      $(window).off("." + _namespace);
      _events = {};
      return undefined;
    };

    _executeLazy(_instance, _config, elements, _events, _namespace);

    return _config.chainable ? elements : _instance;
  }

  LazyPlugin.prototype.config = {
    name: "lazy",
    chainable: true,
    autoDestroy: true,
    bind: "load",
    threshold: 500,
    visibleOnly: false,
    appendScroll: window,
    scrollDirection: "both",
    imageBase: null,
    defaultImage: "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",
    placeholder: null,
    delay: -1,
    combined: false,
    attribute: "data-src",
    srcsetAttribute: "data-srcset",
    sizesAttribute: "data-sizes",
    retinaAttribute: "data-retina",
    loaderAttribute: "data-loader",
    imageBaseAttribute: "data-imagebase",
    removeAttribute: true,
    handledName: "handled",
    loadedName: "loaded",
    effect: "show",
    effectTime: 0,
    enableThrottle: true,
    throttle: 250,
    beforeLoad: undefined,
    afterLoad: undefined,
    onError: undefined,
    onFinishedAll: undefined
  };
  $(window).on("load", function () {
    windowLoaded = true;
  });
})(window);

;

(function () {
  var win = window,
      lastTime = 0;
  win.requestAnimationFrame = win.requestAnimationFrame || win.webkitRequestAnimationFrame;

  if (!win.requestAnimationFrame) {
    win.requestAnimationFrame = function (callback) {
      var currTime = new Date().getTime(),
          timeToCall = Math.max(0, 16 - (currTime - lastTime)),
          id = setTimeout(callback, timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }

  if (!win.cancelAnimationFrame) {
    win.cancelAnimationFrame = function (id) {
      clearTimeout(id);
    };
  }
})();

(function (root, factory) {
  if (true) {
    !(__WEBPACK_LOCAL_MODULE_0__factory = (factory), (__WEBPACK_LOCAL_MODULE_0__module = { id: "themeone-utils/utils", exports: {}, loaded: false }), __WEBPACK_LOCAL_MODULE_0__ = (typeof __WEBPACK_LOCAL_MODULE_0__factory === 'function' ? (__WEBPACK_LOCAL_MODULE_0__factory.call(__WEBPACK_LOCAL_MODULE_0__module.exports, __webpack_require__, __WEBPACK_LOCAL_MODULE_0__module.exports, __WEBPACK_LOCAL_MODULE_0__module)) : __WEBPACK_LOCAL_MODULE_0__factory), (__WEBPACK_LOCAL_MODULE_0__module.loaded = true), __WEBPACK_LOCAL_MODULE_0__ === undefined && (__WEBPACK_LOCAL_MODULE_0__ = __WEBPACK_LOCAL_MODULE_0__module.exports));
  } else {}
})(this, function () {
  "use strict";

  var utils = {};
  var Console = window.console;

  utils.error = function (message) {
    if (typeof Console !== 'undefined') {
      Console.error(message);
    }
  };

  utils.extend = function (options, defaults) {
    if (options) {
      if (_typeof2(options) !== 'object') {
        this.error('Custom options must be an object');
      } else {
        for (var prop in defaults) {
          if (defaults.hasOwnProperty(prop) && options.hasOwnProperty(prop)) {
            defaults[prop] = options[prop];
          }
        }
      }
    }

    return defaults;
  };

  utils.prop = function (prop) {
    var el = this.createEl(),
        prefixes = ['', 'Webkit', 'Moz', 'ms', 'O'];

    for (var p = 0, pl = prefixes.length; p < pl; p++) {
      var prefixedProp = prefixes[p] ? prefixes[p] + prop.charAt(0).toUpperCase() + prop.slice(1) : prop;

      if (el.style[prefixedProp] !== undefined) {
        return prefixedProp;
      }
    }

    return '';
  };

  utils.cloneObject = function (obj) {
    var copy = {};

    for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) {
        copy[attr] = obj[attr];
      }
    }

    return copy;
  };

  utils.createEl = function (tag, classes) {
    var el = document.createElement(tag || 'div');

    if (classes) {
      el.className = classes;
    }

    return el;
  };

  utils.camelize = function (string) {
    return string.replace(/-([a-z])/g, function (g) {
      return g[1].toUpperCase();
    });
  };

  utils.handleEvents = function (_this, el, event, fn, isBind) {
    if (_typeof2(this.event_handlers) !== 'object') {
      this.event_handlers = {};
    }

    if (!this.event_handlers[fn]) {
      this.event_handlers[fn] = _this[fn].bind(_this);
    }

    isBind = isBind === undefined ? true : !!isBind;
    var bindMethod = isBind ? 'addEventListener' : 'removeEventListener';
    event.forEach(function (ev) {
      el[bindMethod](ev, this.event_handlers[fn], false);
    }.bind(this));
  };

  utils.dispatchEvent = function (_this, namespace, type, event, args) {
    type += namespace ? '.' + namespace : '';
    var emitArgs = event ? [event].concat(args) : [args];

    _this.emitEvent(type, emitArgs);
  };

  utils.throttle = function (func, delay) {
    var timestamp = null,
        limit = delay;
    return function () {
      var self = this,
          args = arguments,
          now = Date.now();

      if (!timestamp || now - timestamp >= limit) {
        timestamp = now;
        func.apply(self, args);
      }
    };
  };

  utils.modulo = function (length, index) {
    return (length + index % length) % length;
  };

  utils.classReg = function (className) {
    return new RegExp('(^|\\s+)' + className + '(\\s+|$)');
  };

  utils.hasClass = function (el, className) {
    return !!el.className.match(this.classReg(className));
  };

  utils.addClass = function (el, className) {
    if (!this.hasClass(el, className)) {
      el.className += (el.className ? ' ' : '') + className;
    }
  };

  utils.removeClass = function (el, className) {
    if (this.hasClass(el, className)) {
      el.className = el.className.replace(this.classReg(className), ' ').replace(/\s+$/, '');
    }
  };

  utils.translate = function (el, x, y, s) {
    var scale = s ? ' scale(' + s + ',' + s + ')' : '';
    el.style[this.browser.trans] = this.browser.gpu ? 'translate3d(' + (x || 0) + 'px, ' + (y || 0) + 'px, 0)' + scale : 'translate(' + (x || 0) + 'px, ' + (y || 0) + 'px)' + scale;
  };

  utils.browser = {
    trans: utils.prop('transform'),
    gpu: utils.prop('perspective') ? true : false
  };
  return utils;
});

(function (root, factory) {
  if (true) {
    !(__WEBPACK_LOCAL_MODULE_1__factory = (factory), (__WEBPACK_LOCAL_MODULE_1__module = { id: "themeone-event/event", exports: {}, loaded: false }), __WEBPACK_LOCAL_MODULE_1__ = (typeof __WEBPACK_LOCAL_MODULE_1__factory === 'function' ? (__WEBPACK_LOCAL_MODULE_1__factory.call(__WEBPACK_LOCAL_MODULE_1__module.exports, __webpack_require__, __WEBPACK_LOCAL_MODULE_1__module.exports, __WEBPACK_LOCAL_MODULE_1__module)) : __WEBPACK_LOCAL_MODULE_1__factory), (__WEBPACK_LOCAL_MODULE_1__module.loaded = true), __WEBPACK_LOCAL_MODULE_1__ === undefined && (__WEBPACK_LOCAL_MODULE_1__ = __WEBPACK_LOCAL_MODULE_1__module.exports));
  } else {}
})(typeof window !== 'undefined' ? window : this, function () {
  "use strict";

  var EvEmitter = function EvEmitter() {},
      proto = EvEmitter.prototype;

  proto.on = function (eventName, listener) {
    if (!eventName || !listener) {
      return null;
    }

    var events = this._events = this._events || {};
    var listeners = events[eventName] = events[eventName] || [];

    if (listeners.indexOf(listener) === -1) {
      listeners.push(listener);
    }

    return this;
  };

  proto.off = function (eventName, listener) {
    var listeners = this._events && this._events[eventName];

    if (!listeners || !listeners.length) {
      return null;
    }

    var index = listeners.indexOf(listener);

    if (index !== -1) {
      listeners.splice(index, 1);
    }

    return this;
  };

  proto.emitEvent = function (eventName, args) {
    var listeners = this._events && this._events[eventName];

    if (!listeners || !listeners.length) {
      return null;
    }

    var i = 0,
        listener = listeners[i];
    args = args || [];
    var onceListeners = this._onceEvents && this._onceEvents[eventName];

    while (listener) {
      var isOnce = onceListeners && onceListeners[listener];

      if (isOnce) {
        this.off(eventName, listener);
        delete onceListeners[listener];
      }

      listener.apply(this, args);
      i += isOnce ? 0 : 1;
      listener = listeners[i];
    }

    return this;
  };

  return EvEmitter;
});

(function (root, factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_0__, __WEBPACK_LOCAL_MODULE_1__], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_LOCAL_MODULE_2__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__));
  } else {}
})(this, function (utils, EvEmitter) {
  'use strict';

  var Animate = function Animate(element, positions, friction, attraction) {
    this.element = element;
    this.defaults = positions;
    this.forces = {
      friction: friction || 0.28,
      attraction: attraction || 0.028
    };
    this.resetAnimate();
  };

  var proto = Animate.prototype = Object.create(EvEmitter.prototype);

  proto.updateDrag = function (obj) {
    this.move = true;
    this.drag = obj;
  };

  proto.releaseDrag = function () {
    this.move = false;
  };

  proto.animateTo = function (obj) {
    this.attraction = obj;
  };

  proto.startAnimate = function () {
    this.move = true;
    this.settle = false;
    this.restingFrames = 0;

    if (!this.RAF) {
      this.animate();
    }
  };

  proto.stopAnimate = function () {
    this.move = false;
    this.restingFrames = 0;

    if (this.RAF) {
      cancelAnimationFrame(this.RAF);
      this.RAF = false;
    }

    this.start = utils.cloneObject(this.position);
    this.velocity = {
      x: 0,
      y: 0,
      s: 0
    };
  };

  proto.resetAnimate = function () {
    this.stopAnimate();
    this.settle = true;
    this.drag = utils.cloneObject(this.defaults);
    this.start = utils.cloneObject(this.defaults);
    this.resting = utils.cloneObject(this.defaults);
    this.position = utils.cloneObject(this.defaults);
    this.attraction = utils.cloneObject(this.defaults);
  };

  proto.animate = function () {
    var loop = function () {
      if (typeof this.position !== 'undefined') {
        var previous = utils.cloneObject(this.position);
        this.applyDragForce();
        this.applyAttractionForce();
        utils.dispatchEvent(this, 'toanimate', 'render', this);
        this.integratePhysics();
        this.getRestingPosition();
        this.render(100);
        this.RAF = requestAnimationFrame(loop);
        this.checkSettle(previous);
      }
    }.bind(this);

    this.RAF = requestAnimationFrame(loop);
  };

  proto.integratePhysics = function () {
    for (var k in this.position) {
      if (typeof this.position[k] !== 'undefined') {
        this.position[k] += this.velocity[k];
        this.position[k] = k === 's' ? Math.max(0.1, this.position[k]) : this.position[k];
        this.velocity[k] *= this.getFrictionFactor();
      }
    }
  };

  proto.applyDragForce = function () {
    if (this.move) {
      for (var k in this.drag) {
        if (typeof this.drag[k] !== 'undefined') {
          var dragVelocity = this.drag[k] - this.position[k];
          var dragForce = dragVelocity - this.velocity[k];
          this.applyForce(k, dragForce);
        }
      }
    }
  };

  proto.applyAttractionForce = function () {
    if (!this.move) {
      for (var k in this.attraction) {
        if (typeof this.attraction[k] !== 'undefined') {
          var distance = this.attraction[k] - this.position[k];
          var force = distance * this.forces.attraction;
          this.applyForce(k, force);
        }
      }
    }
  };

  proto.getRestingPosition = function () {
    for (var k in this.position) {
      if (typeof this.position[k] !== 'undefined') {
        this.resting[k] = this.position[k] + this.velocity[k] / (1 - this.getFrictionFactor());
      }
    }
  };

  proto.applyForce = function (direction, force) {
    this.velocity[direction] += force;
  };

  proto.getFrictionFactor = function () {
    return 1 - this.forces.friction;
  };

  proto.roundValues = function (values, round) {
    for (var k in values) {
      if (typeof values[k] !== 'undefined') {
        round = k === 's' ? round * 100 : round;
        values[k] = Math.round(values[k] * round) / round;
      }
    }
  };

  proto.checkSettle = function (previous) {
    if (!this.move) {
      var count = 0;

      for (var k in this.position) {
        if (typeof this.position[k] !== 'undefined') {
          var round = k === 's' ? 10000 : 100;

          if (Math.round(this.position[k] * round) === Math.round(previous[k] * round)) {
            count++;

            if (count === Object.keys(this.position).length) {
              this.restingFrames++;
            }
          }
        }
      }
    }

    if (this.restingFrames > 2) {
      this.stopAnimate();
      this.render(this.position.s > 1 ? 10 : 1);
      this.settle = true;

      if (JSON.stringify(this.start) !== JSON.stringify(this.position)) {
        utils.dispatchEvent(this, 'toanimate', 'settle', this);
      }
    }
  };

  proto.render = function (round) {
    this.roundValues(this.position, round);
    utils.translate(this.element, this.position.x, this.position.y, this.position.s);
  };

  return Animate;
});

(function (root, factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_0__, __WEBPACK_LOCAL_MODULE_1__, __WEBPACK_LOCAL_MODULE_2__], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
})(this, function (utils, EvEmitter, Animate) {
  'use strict';

  var version = '1.5.0';
  var GUID = 0;
  var instances = {};
  var expando = 'mobx' + (version + Math.random()).replace(/\D/g, '');
  var cache = {
    uid: 0
  };
  var defaults = {
    mediaSelector: '.mobx',
    threshold: 5,
    attraction: {
      slider: 0.055,
      slide: 0.018,
      thumbs: 0.016
    },
    friction: {
      slider: 0.62,
      slide: 0.18,
      thumbs: 0.22
    },
    rightToLeft: false,
    loop: 3,
    preload: 1,
    unload: false,
    timeToIdle: 4000,
    history: false,
    mouseWheel: true,
    contextMenu: true,
    scrollBar: true,
    fadeIfSettle: false,
    controls: ['close'],
    prevNext: true,
    prevNextTouch: false,
    counterMessage: '[index] / [total]',
    caption: true,
    autoCaption: false,
    captionSmallDevice: true,
    thumbnails: true,
    thumbnailsNav: 'basic',
    thumbnailSizes: {
      1920: {
        width: 110,
        height: 80,
        gutter: 10
      },
      1280: {
        width: 90,
        height: 65,
        gutter: 10
      },
      680: {
        width: 70,
        height: 50,
        gutter: 8
      },
      480: {
        width: 60,
        height: 44,
        gutter: 5
      }
    },
    spacing: 0.1,
    smartResize: true,
    overflow: false,
    loadError: 'Sorry, an error occured while loading the content...',
    noContent: 'Sorry, no content was found!',
    prevNextKey: true,
    scrollToNav: false,
    scrollSensitivity: 15,
    zoomTo: 'auto',
    minZoom: 1.2,
    maxZoom: 4,
    doubleTapToZoom: true,
    scrollToZoom: false,
    pinchToZoom: true,
    escapeToClose: true,
    scrollToClose: false,
    pinchToClose: true,
    dragToClose: true,
    tapToClose: true,
    shareButtons: ['facebook', 'googleplus', 'twitter', 'pinterest', 'linkedin', 'reddit'],
    shareText: 'Share on',
    sharedUrl: 'deeplink',
    slideShowInterval: 4000,
    slideShowAutoPlay: false,
    slideShowAutoStop: false,
    countTimer: true,
    countTimerBg: 'rgba(255,255,255,0.25)',
    countTimerColor: 'rgba(255,255,255,0.75)',
    mediaelement: false,
    videoRatio: 16 / 9,
    videoMaxWidth: 1180,
    videoAutoPlay: false,
    videoThumbnail: false
  };

  var ModuloBox = function ModuloBox(options) {
    var element = document.querySelector('.mobx-holder');

    if (element && element.GUID) {
      return instances[element.GUID];
    }

    this.options = utils.extend(options, defaults);
    this.setVar();
  };

  var proto = ModuloBox.prototype = Object.create(EvEmitter.prototype);

  proto.init = function () {
    if (this.GUID) {
      return;
    }

    this.createDOM();
    this.GUID = ++GUID;
    this.DOM.holder.GUID = GUID;
    instances[this.GUID] = this;
    this.setAnimation();
    this.getGalleries();
    this.openFromQuery();
  };

  proto.setVar = function () {
    var win = window,
        doc = document,
        nav = navigator;
    this.pre = 'mobx';
    this.gesture = {};
    this.buttons = {};
    this.slider = {};
    this.slides = {};
    this.cells = {};
    this.states = {};
    this.pointers = [];
    this.expando = expando;
    this.cache = cache;
    this.dragEvents = this.detectPointerEvents();
    this.browser = {
      touchDevice: 'ontouchstart' in win || nav.maxTouchPoints > 0 || nav.msMaxTouchPoints > 0,
      pushState: 'history' in win && 'pushState' in history,
      fullScreen: this.detectFullScreen(),
      mouseWheel: 'onwheel' in doc.createElement('div') ? 'wheel' : doc.onmousewheel !== undefined ? 'mousewheel' : 'DOMMouseScroll'
    };
    this.iframeVideo = this.iframeVideo();
    this.socialMedia = this.socialMedia();
  };

  proto.detectPointerEvents = function () {
    var nav = navigator;

    if (nav.pointerEnabled) {
      return {
        start: ['pointerdown'],
        move: ['pointermove'],
        end: ['pointerup', 'pointercancel']
      };
    }

    if (nav.msPointerEnabled) {
      return {
        start: ['MSPointerDown'],
        move: ['MSPointerMove'],
        end: ['MSPointerUp', 'MSPointerCancel']
      };
    }

    return {
      start: ['mousedown', 'touchstart'],
      move: ['mousemove', 'touchmove'],
      end: ['mouseup', 'mouseleave', 'touchend', 'touchcancel']
    };
  };

  proto.detectFullScreen = function () {
    var fullScreen = ['fullscreenEnabled', 'webkitFullscreenEnabled', 'mozFullScreenEnabled', 'msFullscreenEnabled'];

    for (var i = 0, l = fullScreen.length; i < l; i++) {
      if (document[fullScreen[i]]) {
        return {
          element: ['fullscreenElement', 'webkitFullscreenElement', 'mozFullScreenElement', 'msFullscreenElement'][i],
          request: ['requestFullscreen', 'webkitRequestFullscreen', 'mozRequestFullScreen', 'msRequestFullscreen'][i],
          change: ['fullscreenchange', 'webkitfullscreenchange', 'mozfullscreenchange', 'MSFullscreenChange'][i],
          exit: ['exitFullscreen', 'webkitExitFullscreen', 'mozCancelFullScreen', 'msExitFullscreen'][i]
        };
      }
    }

    var controls = this.options.controls,
        index = controls.indexOf('fullScreen');

    if (index > -1) {
      controls.splice(index, 1);
    }

    return null;
  };

  proto.iframeVideo = function () {
    return {
      youtube: {
        reg: /(?:www\.)?youtu\.?be(?:\.com)?\/?.*?(?:watch|embed)?(?:.*v=|v\/|watch%3Fv%3D|\/)([\w\-_]+)&?/i,
        url: 'https://www.youtube.com/embed/[ID]?enablejsapi=1&rel=0&autoplay=1',
        share: 'https://www.youtube.com/watch?v=[ID]',
        poster: 'https://img.youtube.com/vi/[ID]/maxresdefault.jpg',
        thumb: 'https://img.youtube.com/vi/[ID]/default.jpg',
        play: {
          event: 'command',
          func: 'playVideo'
        },
        pause: {
          event: 'command',
          func: 'pauseVideo'
        }
      },
      vimeo: {
        reg: /(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/)?(\d+)(?:[a-zA-Z0-9_\-]+)?/i,
        url: 'https://player.vimeo.com/video/[ID]?autoplay=1&api=1',
        share: 'https://vimeo.com/[ID]',
        poster: 'https://vimeo.com/api/v2/video/[ID].json',
        play: {
          event: 'command',
          method: 'play'
        },
        pause: {
          event: 'command',
          method: 'pause'
        }
      },
      dailymotion: {
        reg: /(?:www\.)?(?:dailymotion\.com(?:\/embed)(?:\/video|\/hub)|dai\.ly)\/([0-9a-z]+)(?:[\-_0-9a-zA-Z]+#video=(?:[a-zA-Z0-9_\-]+))?/i,
        url: 'https://dailymotion.com/embed/video/[ID]?autoplay=1&api=postMessage',
        share: 'https://www.dailymotion.com/video/[ID]',
        poster: 'https://www.dailymotion.com/thumbnail/video/[ID]',
        thumb: 'https://www.dailymotion.com/thumbnail/video/[ID]',
        play: 'play',
        pause: 'pause'
      },
      wistia: {
        reg: /(?:www\.)?(?:wistia\.(?:com|net)|wi\.st)\/(?:(?:m|medias|projects)|embed\/(?:iframe|playlists))\/([a-zA-Z0-9_\-]+)/i,
        url: 'https://fast.wistia.net/embed/iframe/[ID]?version=3&enablejsapi=1&html5=1&autoplay=1',
        share: 'https://fast.wistia.net/embed/iframe/[ID]',
        poster: 'https://fast.wistia.com/oembed?url=https://home.wistia.com/medias/[ID].json',
        play: {
          event: 'cmd',
          method: 'play'
        },
        pause: {
          event: 'cmd',
          method: 'pause'
        }
      }
    };
  };

  proto.socialMedia = function () {
    return {
      facebook: 'https://www.facebook.com/sharer/sharer.php?u=[url]',
      googleplus: 'https://plus.google.com/share?url=[url]',
      twitter: 'https://twitter.com/intent/tweet?text=[text]&url=[url]',
      pinterest: 'https://www.pinterest.com/pin/create/button/?url=[url]&media=[image]&description=[text]',
      linkedin: 'https://www.linkedin.com/shareArticle?url=[url]&mini=true&title=[text]',
      reddit: 'https://www.reddit.com/submit?url=[url]&title=[text]',
      stumbleupon: 'https://www.stumbleupon.com/badge?url=[url]&title=[text]',
      tumblr: 'https://www.tumblr.com/share?v=3&u=[url]&t=[text]',
      blogger: 'https://www.blogger.com/blog_this.pyra?t&u=[url]&n=[text]',
      buffer: 'https://bufferapp.com/add?url=[url]title=[text]',
      digg: 'https://digg.com/submit?url=[url]&title=[text]',
      evernote: 'https://www.evernote.com/clip.action?url=[url]&title=[text]'
    };
  };

  proto.createDOM = function () {
    this.DOM = {};
    var elements = ['holder', 'overlay', 'slider', 'item', 'item-inner', 'ui', 'top-bar', 'bottom-bar', 'share-tooltip', 'counter', 'caption', 'caption-inner', 'thumbs-holder', 'thumbs-inner'];

    for (var i = 0; i < elements.length; i++) {
      this.DOM[utils.camelize(elements[i])] = utils.createEl('div', this.pre + '-' + elements[i]);
    }

    this.appendDOM(this.DOM);
  };

  proto.appendDOM = function (dom) {
    var opt = this.options;
    dom.holder.appendChild(dom.overlay);
    dom.holder.appendChild(dom.slider);
    dom.holder.appendChild(dom.ui);

    for (var i = 0; i < 5; i++) {
      var slide = dom.item.cloneNode(true);
      slide.appendChild(dom.itemInner.cloneNode(true));
      dom.slider.appendChild(slide);
      this.slides[i] = slide;
    }

    this.slides.length = dom.slider.children.length;
    this.createUI(dom, opt);
    dom.holder.setAttribute('tabindex', -1);
    dom.holder.setAttribute('aria-hidden', true);
    this.DOM.comment = document.createComment(' ModuloBox (v' + version + ') by Themeone ');
    document.body.appendChild(this.DOM.comment);
    utils.dispatchEvent(this, 'modulobox', 'beforeAppendDOM', dom);
    document.body.appendChild(dom.holder);
    dom.topBar.height = dom.topBar.clientHeight;
  };

  proto.createUI = function (dom, opt) {
    var shareIndex = opt.controls.indexOf('share');

    if (shareIndex > -1) {
      var buttons = opt.shareButtons,
          i = buttons.length;

      while (i--) {
        if (!this.socialMedia.hasOwnProperty(buttons[i])) {
          buttons.splice(i, 1);
        }
      }

      if (buttons.length) {
        dom.ui.appendChild(dom.shareTooltip);

        if (opt.shareText) {
          dom.shareTooltip.appendChild(utils.createEl('span')).textContent = opt.shareText;
        }

        this.createButtons(buttons, dom.shareTooltip, 'shareOn');
      } else {
        opt.controls.splice(shareIndex, 1);
      }
    }

    if (opt.controls.length || opt.counterMessage) {
      var slideShow = opt.controls.indexOf('play');
      dom.ui.appendChild(dom.topBar);

      if (opt.counterMessage) {
        dom.topBar.appendChild(dom.counter);
      }

      if (opt.slideShowInterval < 1 && slideShow > -1) {
        opt.controls.splice(slideShow, 1);
      }

      if (opt.countTimer && slideShow > -1) {
        var timer = this.DOM.timer = utils.createEl('canvas', this.pre + '-timer');
        timer.setAttribute('width', 48);
        timer.setAttribute('height', 48);
        dom.topBar.appendChild(timer);
      }

      if (opt.controls.length) {
        var controls = opt.controls.slice();
        this.createButtons(controls.reverse(), dom.topBar);
      }
    }

    if (opt.caption || opt.thumbnails) {
      dom.ui.appendChild(dom.bottomBar);

      if (opt.caption) {
        dom.bottomBar.appendChild(dom.caption).appendChild(dom.captionInner);
      }

      if (opt.thumbnails) {
        dom.bottomBar.appendChild(dom.thumbsHolder).appendChild(dom.thumbsInner);
      }
    }

    if (opt.prevNext) {
      this.createButtons(['prev', 'next'], dom.ui);
    }
  };

  proto.createButtons = function (buttons, dom, event) {
    var length = buttons.length;

    for (var i = 0; i < length; i++) {
      var type = buttons[i];
      this.buttons[type] = utils.createEl('BUTTON', this.pre + '-' + type.toLowerCase());
      dom.appendChild(this.buttons[type]);

      if (type && typeof this[type] === 'function' || event) {
        this.buttons[type].event = event ? event : type;
        this.buttons[type].action = type;

        if (event === 'shareOn') {
          this.buttons[type].setAttribute('title', type.charAt(0).toUpperCase() + type.slice(1));
        }
      }
    }
  };

  proto.getGalleries = function () {
    this.galleries = {};
    var selectors = this.options.mediaSelector,
        sources = '';

    if (!selectors) {
      return false;
    }

    try {
      sources = document.querySelectorAll(selectors);
    } catch (error) {
      utils.error('Your current mediaSelector is not a valid selector: "' + selectors + '"');
    }

    for (var i = 0, l = sources.length; i < l; i++) {
      var source = sources[i],
          media = {};
      media.src = source.tagName === 'A' ? source.getAttribute('href') : null;
      media.src = source.tagName === 'IMG' ? source.currentSrc || source.src : media.src;
      media.src = source.getAttribute('data-src') || media.src;

      if (media.src) {
        this.getMediaAtts(source, media);
        this.setMediaType(media);

        if (media.type) {
          this.getMediaThumb(source, media);
          this.getVideoThumb(media);
          this.getMediaCaption(source, media);
          this.setMediaCaption(media);
          var gallery = this.setGalleryName(source);
          this.setGalleryFeatures(gallery, media);
          media.index = gallery.length;
          gallery.push(media);
          this.setMediaEvent(source, gallery.name, media.index);
        }
      }
    }

    utils.dispatchEvent(this, 'modulobox', 'updateGalleries', this.galleries);
  };

  proto.addMedia = function (name, media) {
    if (!media || _typeof2(media) !== 'object') {
      utils.error('No media was found to addMedia() in a gallery');
      return false;
    }

    name = name === '' ? 1 : name;
    var gallery = this.galleries[name];
    gallery = !gallery ? this.galleries[name] = [] : gallery;
    gallery.name = name;
    var length = media.length;

    for (var i = 0; i < length; i++) {
      var item = utils.cloneObject(media[i]);

      if (item.src) {
        this.setMediaType(item);
        this.getVideoThumb(item);
        this.setMediaCaption(item);
        this.setGalleryFeatures(gallery, item);
        item.index = gallery.length;
        gallery.push(item);
      }
    }
  };

  proto.setMediaType = function (media) {
    if (['image', 'video', 'iframe', 'HTML'].indexOf(media.type) > -1) {
      return;
    }

    media.type = null;
    var source = media.src ? media.src : null;
    var extension = (source.split(/[?#]/)[0] || source).substr((~-source.lastIndexOf('.') >>> 0) + 2);

    if (/(jpg|jpeg|png|bmp|gif|tif|tiff|jfi|jfif|exif|svg)/i.test(extension) || ['external.xx.fbcdn', 'drscdn.500px.org'].indexOf(source) > -1) {
      media.type = 'image';
      media.src = this.getSrc(source);
    } else if (/(mp4|webm|ogv)/i.test(extension)) {
      media.type = 'video';
      media.format = 'html5';
    } else {
      var stream = this.iframeVideo;

      for (var type in stream) {
        if (stream.hasOwnProperty(type)) {
          var regs = source.match(stream[type].reg);

          if (regs && regs[1]) {
            var object = stream[type];
            media.type = 'video';
            media.format = type;
            media.share = object.share.replace('[ID]', regs[1]);
            media.src = object.url.replace('[ID]', regs[1]);
            media.pause = object.pause;
            media.play = object.play;

            if (this.options.videoThumbnail) {
              media.poster = !media.poster && object.poster ? object.poster.replace('[ID]', regs[1]) : media.poster;
              media.thumb = !media.thumb && object.poster ? object.poster.replace('[ID]', regs[1]) : media.thumb;
            }

            break;
          }
        }
      }
    }
  };

  proto.getSrc = function (source) {
    var srcset = (source || '').split(/,/),
        length = srcset.length,
        width = 0;

    if (length <= 1) {
      return source;
    }

    for (var i = 0; i < length; i++) {
      var parts = srcset[i].replace(/\s+/g, ' ').trim().split(/ /),
          value = parseFloat(parts[1]) || 0,
          unit = parts[1] ? parts[1].slice(-1) : null;

      if (unit === 'w' && screen.width >= value && value > width || !value || i === 0) {
        width = value;
        source = parts[0];
      }
    }

    return source;
  };

  proto.getMediaAtts = function (source, media) {
    var auto = this.options.autoCaption,
        data = this.getAttr(source),
        img = source.firstElementChild;
    img = source.tagName !== 'IMG' && img && img.tagName === 'IMG' ? img : source;
    media.type = !media.type ? data.type || source.getAttribute('data-type') : media.type;
    media.title = data.title || source.getAttribute('data-title') || (auto ? img.title : null);
    media.desc = data.desc || source.getAttribute('data-desc') || (auto ? img.alt : null);
    media.thumb = data.thumb || source.getAttribute('data-thumb');
    media.poster = this.getSrc(data.poster || source.getAttribute('data-poster'));
    media.width = data.width || source.getAttribute('data-width');
    media.height = data.height || source.getAttribute('data-height');

    if (media.title === media.desc) {
      media.desc = null;
    }
  };

  proto.getMediaThumb = function (source, media) {
    var thumbnail = source.getElementsByTagName('img');

    if (!media.thumb && thumbnail[0]) {
      media.thumb = thumbnail[0].src;
    }
  };

  proto.getVideoThumb = function (media) {
    if (!this.options.videoThumbnail || media.type !== 'video' || media.format === 'html5') {
      return;
    }

    var hasPoster = media.poster && media.poster.indexOf('.json') > -1,
        hasThumb = media.thumb && media.thumb.indexOf('.json') > -1;

    if (hasPoster || hasThumb) {
      var uri = hasPoster ? media.poster : media.thumb,
          xhr = new XMLHttpRequest();

      xhr.onload = function (event) {
        var response = event.target.responseText;
        response = JSON.parse(response);
        response = response.hasOwnProperty(0) ? response[0] : response;

        if (response) {
          media.poster = response.thumbnail_large || response.thumbnail_url;

          if (media.dom) {
            media.dom.style.backgroundImage = 'url("' + media.poster + '")';
          }

          if (hasThumb) {
            var thumb = response.thumbnail_small || response.thumbnail_url;

            if (_typeof2(media.thumb) === 'object') {
              media.thumb.style.backgroundImage = 'url("' + thumb + '")';
            } else {
              media.thumb = thumb;
            }
          }
        }
      }.bind(this);

      xhr.open('GET', encodeURI(uri), true);
      setTimeout(function () {
        xhr.send();
      }, 0);
    }
  };

  proto.getMediaCaption = function (source, media) {
    var next = source.nextElementSibling;

    if (next && next.tagName === 'FIGCAPTION') {
      var caption = next.innerHTML;

      if (!media.title) {
        media.title = caption;
      } else if (!media.desc) {
        media.desc = caption;
      }
    }
  };

  proto.setMediaCaption = function (media) {
    media.title = media.title ? '<div class="' + this.pre + '-title">' + media.title.trim() + '</div>' : '';
    media.desc = media.desc ? '<div class="' + this.pre + '-desc">' + media.desc.trim() + '</div>' : '';
    media.caption = media.title + media.desc;
  };

  proto.getGalleryName = function (source) {
    var parent = source,
        node = 0;

    while (parent && node < 2) {
      parent = parent.parentNode;

      if (parent && parent.tagName === 'FIGURE' && parent.parentNode) {
        return parent.parentNode.getAttribute('id');
      }

      node++;
    }
  };

  proto.setGalleryName = function (source) {
    var data = this.getAttr(source);
    var name = data.rel || source.getAttribute('data-rel');
    name = !name ? this.getGalleryName(source) : name;
    name = !name ? Object.keys(this.galleries).length + 1 : name;
    var gallery = this.galleries[name];
    gallery = !gallery ? this.galleries[name] = [] : gallery;
    gallery.name = name;
    return gallery;
  };

  proto.setGalleryFeatures = function (gallery, media) {
    if (!gallery.zoom && media.type === 'image') {
      gallery.zoom = true;
    }

    if (!gallery.download && (media.type === 'image' || media.format === 'html5')) {
      gallery.download = true;
    }
  };

  proto.setMediaEvent = function (source, name, index) {
    if (source.mobxListener) {
      source.removeEventListener('click', source.mobxListener, false);
    }

    source.mobxListener = this.open.bind(this, name, index);
    source.addEventListener('click', source.mobxListener, false);
  };

  proto.open = function (name, index, event) {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    if (!this.GUID) {
      return false;
    }

    if (!this.galleries.hasOwnProperty(name)) {
      utils.error('This gallery name : "' + name + '", does not exist!');
      return false;
    }

    if (!this.galleries[name].length) {
      utils.error('Sorry, no media was found for the current gallery.');
      return false;
    }

    if (!this.galleries[name][index]) {
      utils.error('Sorry, no media was found for the current media index: ' + index);
      return false;
    }

    utils.dispatchEvent(this, 'modulobox', 'beforeOpen', name, index);
    this.slides.index = index;
    this.gallery = this.galleries[name];
    this.gallery.name = name;
    this.gallery.index = index;
    this.gallery.loaded = false;
    this.removeContent();
    this.wrapAround();
    this.hideScrollBar();
    this.setSlider();
    this.setThumbs();
    this.setCaption();
    this.setMedia(this.options.preload);
    this.updateMediaInfo();
    this.replaceState();
    this.setControls();
    this.bindEvents(true);
    this.show();

    if (this.options.videoAutoPlay) {
      this.appendVideo();
    }

    if (this.options.slideShowAutoPlay && this.options.controls.indexOf('play') > -1 && (!this.options.videoAutoPlay || this.galleries[name][index].type !== 'video')) {
      this.startSlideShow();
    }

    this.states.zoom = false;
    this.states.open = true;
  };

  proto.openFromQuery = function () {
    var query = this.getQueryString(window.location.search);

    if (query.hasOwnProperty('guid') && query.hasOwnProperty('mid')) {
      var open = this.open(decodeURIComponent(query.guid), decodeURIComponent(query.mid) - 1);

      if (open === false) {
        this.replaceState(true);
      }
    }
  };

  proto.show = function () {
    var holder = this.DOM.holder,
        method = this.options.rightToLeft ? 'add' : 'remove';
    holder.setAttribute('aria-hidden', false);
    utils.removeClass(holder, this.pre + '-idle');
    utils.removeClass(holder, this.pre + '-panzoom');
    utils.removeClass(holder, this.pre + '-will-close');
    utils[method + 'Class'](holder, this.pre + '-rtl');
    utils.addClass(holder, this.pre + '-open');
  };

  proto.close = function (event) {
    if (event) {
      event.preventDefault();
    }

    var holder = this.DOM.holder,
        gallery = this.gallery,
        index = gallery ? gallery.index : 'undefined',
        name = gallery ? gallery.name : 'undefined';
    utils.dispatchEvent(this, 'modulobox', 'beforeClose', name, index);

    if (this.states.fullScreen) {
      this.exitFullScreen();
      utils.removeClass(holder, this.pre + '-fullscreen');
    }

    this.share();
    this.stopSlideShow();
    this.pauseVideo();
    this.bindEvents(false);
    this.replaceState(true);
    this.hideScrollBar();
    holder.setAttribute('aria-hidden', true);
    utils.removeClass(holder, this.pre + '-open');
    this.states.open = false;
  };

  proto.setControls = function () {
    var gallery = this.gallery,
        options = this.options,
        buttons = this.buttons;

    if (this.DOM.counter) {
      this.DOM.counter.style.display = gallery.initialLength > 1 ? '' : 'none';
    }

    if (options.controls.indexOf('play') > -1) {
      buttons.play.style.display = gallery.initialLength > 1 ? '' : 'none';
    }

    if (options.controls.indexOf('zoom') > -1) {
      buttons.zoom.style.display = !gallery.zoom ? 'none' : '';
    }

    if (options.controls.indexOf('download') > -1) {
      buttons.download.style.display = !gallery.download ? 'none' : '';
    }

    this.setPrevNextButtons();
  };

  proto.setPrevNextButtons = function () {
    if (this.options.prevNext) {
      var hide = this.slider.width < 680 && this.browser.touchDevice && !this.options.prevNextTouch;
      this.buttons.prev.style.display = this.buttons.next.style.display = this.gallery.length > 1 && !hide ? '' : 'none';
    }
  };

  proto.setCaption = function () {
    this.states.caption = !(!this.options.captionSmallDevice && (this.slider.width <= 480 || this.slider.height <= 480));
    this.DOM.caption.style.display = this.states.caption ? '' : 'none';
  };

  proto.hideScrollBar = function () {
    if (!this.options.scrollBar) {
      var open = this.states.open,
          scrollBar = open === 'undefined' || !open ? true : false;
      document.body.style.overflow = document.documentElement.style.overflow = scrollBar ? 'hidden' : '';
    }
  };

  proto.bindEvents = function (bind) {
    var win = window,
        doc = document,
        opt = this.options,
        holder = this.DOM.holder,
        buttons = this.buttons,
        scrollMethod;

    for (var type in buttons) {
      if (buttons.hasOwnProperty(type)) {
        var DOM = type !== 'share' ? buttons[type] : win;
        utils.handleEvents(this, DOM, ['click', 'touchend'], buttons[type].event, bind);
      }
    }

    utils.handleEvents(this, holder, this.dragEvents.start, 'touchStart', bind);
    utils.handleEvents(this, win, ['keydown'], 'keyDown', bind);
    utils.handleEvents(this, win, ['resize', 'orientationchange'], 'resize', bind);
    utils.handleEvents(this, holder, ['transitionend', 'webkitTransitionEnd', 'oTransitionEnd', 'otransitionend', 'MSTransitionEnd'], 'opened');
    utils.handleEvents(this, holder, ['touchend'], 'disableZoom', bind);

    if (this.browser.fullScreen) {
      utils.handleEvents(this, doc, [this.browser.fullScreen.change], 'toggleFullScreen', bind);
    }

    if (opt.history) {
      utils.handleEvents(this, win, ['mouseout'], 'mouseOut', bind);
    }

    if (opt.timeToIdle > 0) {
      utils.handleEvents(this, holder, ['mousemove'], 'mouseMove', bind);
    }

    if (!opt.contextMenu) {
      utils.handleEvents(this, holder, ['contextmenu'], 'contextMenu', bind);
    }

    if (!opt.mouseWheel) {
      this.disableScroll(bind);
    }

    if (opt.scrollToZoom) {
      scrollMethod = 'scrollToZoom';
    } else if (opt.scrollToNav) {
      scrollMethod = 'scrollToNav';
    } else if (opt.scrollToClose) {
      scrollMethod = 'scrollToClose';
    }

    if (scrollMethod) {
      utils.handleEvents(this, holder, [this.browser.mouseWheel], scrollMethod, bind);
    }
  };

  proto.opened = function (event) {
    if (event.propertyName === 'visibility' && event.target === this.DOM.holder) {
      var name = this.gallery.name,
          index = this.gallery.index;

      if (!this.states.open) {
        this.removeContent();
        utils.dispatchEvent(this, 'modulobox', 'afterClose', name, index);
      } else {
        utils.dispatchEvent(this, 'modulobox', 'afterOpen', name, index);
      }
    }
  };

  proto.mouseOut = function (event) {
    var e = event ? event : window.event,
        from = e.relatedTarget || e.toElement;

    if (!from || from.nodeName === 'HTML') {
      this.replaceState();
    }
  };

  proto.mouseMove = function () {
    var holder = this.DOM.holder,
        idleClass = this.pre + '-idle';
    clearTimeout(this.states.idle);
    this.states.idle = setTimeout(function () {
      if (!utils.hasClass(holder, this.pre + '-open-tooltip')) {
        utils.addClass(holder, idleClass);
      }
    }.bind(this), this.options.timeToIdle);
    utils.removeClass(holder, idleClass);
  };

  proto.contextMenu = function (event) {
    var target = event.target,
        tagName = target.tagName,
        className = target.className;

    if (tagName === 'IMG' || tagName === 'VIDEO' || className.indexOf(this.pre + '-video') > -1 || className.indexOf(this.pre + '-thumb-bg') > -1 || className === this.pre + '-thumb') {
      event.preventDefault();
    }
  };

  proto.disableScroll = function (bind) {
    var doc = document,
        win = window;

    var prevent = function prevent(e) {
      if (!this.isEl(e)) {
        return;
      }

      e = e || win.event;

      if (e.preventDefault) {
        e.preventDefault();
        e.returnValue = false;
        return false;
      }
    };

    win.onwheel = win.ontouchmove = win.onmousewheel = doc.onmousewheel = doc.onmousewheel = bind ? prevent.bind(this) : null;
  };

  proto.scrollToZoom = function (event) {
    if (!this.isEl(event)) {
      return;
    }

    var data = this.normalizeWheel(event);

    if (data && data.deltaY) {
      var cell = this.getCell(),
          scale = cell.attraction.s || cell.position.s;
      scale = Math.min(this.options.maxZoom, Math.max(1, scale - Math.abs(data.deltaY) / data.deltaY));
      this.stopSlideShow();
      this.zoomTo(event.clientX, event.clientY, Math.round(scale * 10) / 10);
    }
  };

  proto.scrollToNav = function (event) {
    if (!this.isEl(event)) {
      return;
    }

    var data = this.normalizeWheel(event);

    if (data && data.delta) {
      this[data.delta * this.isRTL() < 0 ? 'prev' : 'next']();
    }
  };

  proto.scrollToClose = function (event) {
    if (!this.isEl(event)) {
      return;
    }

    event.preventDefault();
    this.close();
  };

  proto.disableZoom = function (event) {
    var node = event.target;

    while (node) {
      if (['VIDEO', 'INPUT', 'A'].indexOf(node.tagName) > -1) {
        return;
      }

      node = node.parentElement;
    }

    event.preventDefault();
  };

  proto.resize = function (event) {
    this.DOM.topBar.height = this.DOM.topBar.clientHeight;
    this.share();
    this.setSlider();
    this.setThumbsPosition();
    this.setCaption();
    this.resizeMedia();
    this.updateMediaInfo();
    this.setPrevNextButtons();
    this.states.zoom = false;
    utils.removeClass(this.DOM.holder, this.pre + '-panzoom');
    utils.dispatchEvent(this, 'modulobox', 'resize', event);
  };

  proto.resizeMedia = function () {
    var slides = this.slides;

    for (var i = 0; i < slides.length; i++) {
      if (!this.gallery) {
        break;
      }

      var media = this.gallery[slides[i].media];

      if (media && (media.dom && media.dom.loaded || media.dom && ['video', 'iframe', 'HTML'].indexOf(media.type) > -1)) {
        this.setMediaSize(media, slides[i]);
      }
    }
  };

  proto.isEl = function (event) {
    var name = event.target.className;
    name = typeof name === 'string' ? name : name.baseVal;
    return name.indexOf(this.pre) > -1;
  };

  proto.isZoomable = function () {
    var media = this.getMedia(),
        zoom = false;

    if (media.type === 'image' && media.dom && media.dom.size && media.dom.size.scale > 1) {
      zoom = true;
    }

    this.DOM.holder.setAttribute('data-zoom', zoom);
    return zoom;
  };

  proto.isDownloadable = function () {
    var media = this.getMedia(),
        download = true;

    if (media.type !== 'image' && media.format !== 'html5') {
      download = false;
    }

    this.DOM.holder.setAttribute('data-download', download);
    return download;
  };

  proto.isRTL = function () {
    return this.options.rightToLeft ? -1 : 1;
  };

  proto.addAttr = function (el, attrs) {
    var cacheID;

    if (typeof el[this.expando] === 'undefined') {
      cacheID = this.cache.uid++;
      el[this.expando] = cacheID;
      this.cache[cacheID] = {};
    } else {
      cacheID = el[this.expando];
    }

    for (var attr in attrs) {
      if (attrs.hasOwnProperty(attr)) {
        this.cache[cacheID][attr] = attrs[attr];
      }
    }
  };

  proto.getAttr = function (el) {
    return this.cache[el[this.expando]] || {};
  };

  proto.getThumbHeight = function () {
    var thumb = this.thumbs;
    return thumb.height > 0 && thumb.width > 0 ? thumb.height + Math.min(10, thumb.gutter) * 2 : 0;
  };

  proto.getMedia = function () {
    var gallery = this.gallery;
    return gallery ? gallery[gallery.index] : null;
  };

  proto.getCell = function () {
    var slides = this.slides,
        index = utils.modulo(slides.length, slides.index);
    return this.cells[index];
  };

  proto.removeContent = function () {
    for (var i = 0; i < this.slides.length; i++) {
      var slide = this.slides[i];
      this.unloadMedia(slide);
      this.removeMedia(slide);
      slide.index = slide.media = null;
    }

    this.removeMedia(this.DOM.thumbsHolder);
  };

  proto.getQueryString = function (search) {
    var params = {};
    search.substr(1).split('&').forEach(function (param) {
      param = param.split('=');
      params[decodeURIComponent(param[0])] = param.length > 1 ? decodeURIComponent(param[1]) : '';
    });
    return params;
  };

  proto.setQueryString = function (key) {
    var search = window.location.search,
        query = this.getQueryString(search);
    search = decodeURI(search);

    for (var prop in key) {
      if (key.hasOwnProperty(prop)) {
        var replace = encodeURIComponent(key[prop]);

        if (query.hasOwnProperty(prop)) {
          var value = query[prop];

          if (!replace) {
            search = search.replace('&' + prop + '=' + value, '');
            search = search.replace(prop + '=' + value, '');
          } else {
            search = search.replace(prop + '=' + value, prop + '=' + replace);
          }
        } else {
          if (replace) {
            search = search + (!search ? '?' : '&') + prop + '=' + replace;
          } else {
            search = search.replace(prop + '=', '');
          }
        }
      }
    }

    var base = [location.protocol, '//', location.host, location.pathname].join('');
    search = !search.substr(1) ? search.substr(1) : search;
    return encodeURI(base + search);
  };

  proto.replaceState = function (remove) {
    if ((this.options.history || remove) && this.browser.pushState && !this.states.push) {
      var prevData = window.history.state,
          data = {
        guid: !remove ? this.gallery.name : '',
        mid: !remove ? utils.modulo(this.gallery.initialLength, this.gallery.index) + 1 : ''
      };

      if (!prevData || prevData.mid !== data.mid) {
        var search = this.setQueryString(data);

        try {
          window.history.replaceState(data, '', search);
        } catch (error) {
          this.options.history = false;
          utils.error('SecurityError: A history state object with origin \'null\' cannot be created. Please run the script on a server.');
        }
      }
    }

    this.states.push = false;
  };

  proto.normalizeWheel = function (event) {
    var ev = event || window.event,
        data = null,
        deltaX,
        deltaY,
        delta;
    event.preventDefault();

    if ('detail' in ev) {
      deltaY = ev.detail * -1;
    }

    if ('wheelDelta' in ev) {
      deltaY = ev.wheelDelta * -1;
    }

    if ('wheelDeltaY' in ev) {
      deltaY = ev.wheelDeltaY * -1;
    }

    if ('wheelDeltaX' in ev) {
      deltaX = ev.wheelDeltaX * -1;
    }

    if ('deltaY' in ev) {
      deltaY = ev.deltaY;
    }

    if ('deltaX' in ev) {
      deltaX = ev.deltaX * -1;
    }

    if (ev.deltaMode === 1) {
      deltaX *= 40;
      deltaY *= 40;
    } else if (ev.deltaMode === 2) {
      deltaX *= 100;
      deltaY *= 100;
    }

    delta = Math.abs(deltaX) > Math.abs(deltaY) ? deltaX : deltaY;
    delta = Math.min(100, Math.max(-100, delta));

    if (Math.abs(delta) < this.options.scrollSensitivity) {
      this.states.prevDelta = delta;
      return data;
    }

    var time = +new Date();

    if (Math.abs(delta) > Math.abs(this.states.prevDelta) || time - this.states.prevScroll > 60) {
      data = {
        'deltaX': deltaX,
        'deltaY': deltaY,
        'delta': delta
      };
    }

    this.states.prevDelta = delta;
    this.states.prevScroll = time;
    return data;
  };

  proto.share = function (event) {
    if (event && event.target.tagName === 'VIDEO') {
      return;
    }

    var holder = this.DOM.holder,
        className = this.pre + '-open-tooltip',
        element = event ? event.target.className : null,
        method = utils.hasClass(holder, className) ? 'remove' : 'add';

    if (method === 'remove' && (element !== this.pre + '-share' || !event) || element === this.pre + '-share') {
      if (method === 'add') {
        this.setShareTooltip();
      }

      utils[method + 'Class'](holder, className);
    }
  };

  proto.shareOn = function (event) {
    var type = event.target.action,
        gallery = this.gallery,
        media = this.getMedia(),
        image = media.type === 'image' ? media.src : media.poster,
        url = this.socialMedia[type],
        uri;

    if (url) {
      if (this.options.sharedUrl === 'page') {
        uri = [location.protocol, '//', location.host, location.pathname].join('');
      } else if (this.options.sharedUrl === 'deeplink' || ['iframe', 'HTML'].indexOf(media.type) > -1) {
        uri = this.setQueryString({
          guid: gallery.name,
          mid: gallery.index + 1
        });
      } else {
        uri = media.src.replace(/\s/g, '').split(',')[0];

        if (media.type === 'video' && media.format !== 'html5') {
          uri = media.share;
        }
      }

      var link = utils.createEl('a');
      link.href = image;
      image = link.href;
      link.href = uri;
      uri = link.href;
      var tmp = utils.createEl('div');
      tmp.innerHTML = media.caption;
      var text = (tmp.textContent || tmp.innerText).replace(/\s+/g, ' ').trim() || '';
      url = url.replace('[url]', encodeURIComponent(uri)).replace('[image]', encodeURIComponent(image)).replace('[text]', encodeURIComponent(text || document.title));

      if (url) {
        var left = Math.round(window.screenX + (window.outerWidth - 626) / 2),
            top = Math.round(window.screenY + (window.outerHeight - 436) / 2);
        window.open(url, this.pre + '_share', 'status=0,resizable=1,location=1,toolbar=0,width=626,height=436,top=' + top + ',left=' + left);
      }
    } else {
      utils.error('This social share media does not exist');
    }

    return false;
  };

  proto.setShareTooltip = function () {
    if (this.options.controls.indexOf('share') > -1) {
      var attribute = 'right',
          tooltip = this.DOM.shareTooltip,
          width = tooltip.clientWidth,
          button = this.buttons.share.getBoundingClientRect(),
          position = button.left - width + button.width / 2 + 20;

      if (position < 0) {
        attribute = 'left';
        position = button.left + button.width / 2 - 20;
      }

      tooltip.setAttribute('data-position', attribute);
      tooltip.style.top = this.DOM.topBar.height + 6 + 'px';
      tooltip.style.left = position + 'px';
    }
  };

  proto.download = function () {
    if (!this.isDownloadable()) {
      return false;
    }

    var media = this.getMedia(),
        url = media.src.replace(/\s/g, '').split(',')[0],
        link = document.createElement('a'),
        group = url.split('/');
    link.href = url;
    link.download = group.pop().split('?')[0];
    link.setAttribute('target', '_blank');
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  proto.fullScreen = function () {
    var fullScreenElement = this.browser.fullScreen.element;

    if (!document[fullScreenElement]) {
      this.requestFullScreen();
    } else {
      this.exitFullScreen();
    }
  };

  proto.toggleFullScreen = function () {
    var holder = this.DOM.holder,
        fullScreenElement = document[this.browser.fullScreen.element];

    if (!fullScreenElement) {
      this.share();
      this.states.fullScreen = false;
      utils.removeClass(holder, this.pre + '-fullscreen');
    } else if (fullScreenElement === holder) {
      this.setShareTooltip();
      this.states.fullScreen = true;
      utils.addClass(holder, this.pre + '-fullscreen');
    }

    this.videoFullScreen();
  };

  proto.requestFullScreen = function () {
    var request = this.browser.fullScreen.request;

    if (document.documentElement[request]) {
      this.DOM.holder[request]();
    }
  };

  proto.exitFullScreen = function () {
    var exit = this.browser.fullScreen.exit;

    if (document[exit]) {
      document[exit]();
    }
  };

  proto.play = function () {
    if (this.states.play) {
      this.stopSlideShow();
    } else {
      this.startSlideShow();
    }
  };

  proto.startSlideShow = function () {
    var start = 0,
        gallery = this.gallery,
        options = this.options,
        loop = this.states.loop,
        autoStop = options.slideShowAutoStop,
        cycle = Math.max(120, options.slideShowInterval),
        count = options.countTimer,
        canvas = count && this.DOM.timer ? this.DOM.timer.getContext('2d') : null;

    var countDown = function (now) {
      now = !now ? +new Date() : now;
      start = !start ? now : start;

      if (!loop || autoStop) {
        if (gallery.index === gallery.initialLength - 1) {
          this.stopSlideShow();
          return;
        }
      }

      if (count && canvas) {
        var percent = Math.min(1, (now - start + cycle) / cycle - 1),
            radians = percent * 360 * (Math.PI / 180);
        canvas.clearRect(0, 0, 48, 48);
        this.timerProgress(canvas, options.countTimerBg, 100);
        this.timerProgress(canvas, options.countTimerColor, radians);
      }

      if (now >= start + cycle) {
        start = now;
        this.slideTo(this.slides.index + 1, true);
      }

      this.timer = requestAnimationFrame(countDown);
    }.bind(this);

    utils.addClass(this.DOM.holder, this.pre + '-autoplay');
    this.states.play = true;
    this.timer = requestAnimationFrame(countDown);
  };

  proto.stopSlideShow = function () {
    cancelAnimationFrame(this.timer);
    utils.removeClass(this.DOM.holder, this.pre + '-autoplay');
    this.states.play = false;
  };

  proto.timerProgress = function (canvas, color, end) {
    var start = 1.5 * Math.PI;
    canvas.strokeStyle = color;
    canvas.lineWidth = 5;
    canvas.beginPath();
    canvas.arc(24, 24, 18, start, start + end, false);
    canvas.stroke();
  };

  proto.appendVideo = function () {
    var media = this.getMedia();

    if (media.type !== 'video') {
      return;
    }

    utils.addClass(media.dom, this.pre + '-loading');
    utils.removeClass(media.dom, this.pre + '-playing');

    if (!media.video) {
      if (media.format === 'html5') {
        media.video = utils.createEl('video');
        media.video.setAttribute('controls', '');
        media.video.setAttribute('autoplay', '');
        var urls = media.src.replace(/\s/g, '').split(',');

        for (var i = 0; i < urls.length; i++) {
          var frag = document.createDocumentFragment(),
              source = utils.createEl('source'),
              type = /^.+\.([^.]+)$/.exec(urls[i]);

          if (type && ['mp4', 'webm', 'ogv'].indexOf(type[1]) > -1) {
            source.src = urls[i];
            source.setAttribute('type', 'video/' + (type[1] === 'ogv' ? 'ogg' : type[1]));
            frag.appendChild(source);
          }

          media.video.appendChild(frag);
        }
      } else if (media.format) {
        media.video = utils.createEl('iframe');
        media.video.src = media.src;
        media.video.setAttribute('frameborder', 0);
        media.video.setAttribute('allowfullscreen', '');
      }

      media.video.setAttribute('width', '100%');
      media.video.setAttribute('height', '100%');
    }

    if (!media.dom.firstChild) {
      media.dom.appendChild(media.video);

      if (media.format !== 'html5') {
        media.video.loaded = false;
      }
    }

    this.playVideo(media);
  };

  proto.onVideoLoaded = function (media) {
    media.video.loaded = true;
    utils.removeClass(media.dom, this.pre + '-loading');
    utils.addClass(media.dom, this.pre + '-playing');
    this.cloneVideo(media);
  };

  proto.cloneVideo = function (media) {
    if (this.states.loop && media.format === 'html5') {
      var gallery = this.gallery,
          length = gallery.length,
          initial = gallery.initialLength,
          current = utils.modulo(initial, media.index);

      for (var i = 0; i < length; i++) {
        var index = utils.modulo(initial, gallery[i].index);

        if (index === current && gallery[i].index !== media.index) {
          gallery[i].video = media.video;
        }
      }
    }
  };

  proto.videoFullScreen = function () {
    var media = this.getMedia(),
        fullScreen = this.states.fullScreen;

    if (media.type === 'video' && media.format !== 'html5' && media.video) {
      media.video[fullScreen ? 'removeAttribute' : 'setAttribute']('allowfullscreen', '');
    }
  };

  proto.playVideo = function (media) {
    if (media.video.loaded) {
      media.video.getClientRects();
      utils.removeClass(media.dom, this.pre + '-loading');
      utils.addClass(media.dom, this.pre + '-playing');

      if (media.format !== 'html5') {
        if (media.play) {
          var message = _typeof2(media.play) === 'object' ? JSON.stringify(media.play) : String(media.play);
          media.video.contentWindow.postMessage(message, '*');
        }
      } else if (!media.video.error) {
        if (typeof MediaElementPlayer === 'function' && this.options.mediaelement) {
          var mejs = media.video.tagName === 'VIDEO' ? media.video : media.video.getElementsByTagName('video')[0];

          if (mejs.player) {
            mejs.player.setControlsSize();
          }

          mejs.play();
        } else {
          media.video.play();
        }
      }
    } else {
      var _this = this;

      if (typeof MediaElementPlayer === 'function' && !media.play && this.options.mediaelement && !media.video.player) {
        new MediaElementPlayer(media.video, {
          features: ['playpause', 'stop', 'current', 'progress', 'duration', 'volume', 'fullscreen'],
          videoVolume: 'horizontal',
          startVolume: 0.8,
          classPrefix: 'mejs-',
          keyActions: [],
          enableKeyboard: false,
          iPadUseNativeControls: true,
          iPhoneUseNativeControls: true,
          AndroidUseNativeControls: true,
          success: function success(mejs) {
            mejs.addEventListener('loadeddata', function () {
              media.video = media.dom.lastChild;

              if (media.video) {
                var offScreen = media.video.previousSibling;

                if (offScreen && offScreen.parentNode) {
                  offScreen.parentNode.removeChild(offScreen);
                }

                _this.onVideoLoaded(media);
              }
            }, media, false);
          },
          error: function error() {
            _this.onVideoLoaded(media);
          }
        });
      } else if (!media.video.onload) {
        media.video.onload = media.video.onerror = media.video.onloadedmetadata = function () {
          if (media.dom.firstChild) {
            _this.onVideoLoaded(media);

            _this.videoFullScreen();
          }
        };

        media.video.src = media.src.replace(/\s/g, '').split(',')[0];
      }
    }
  };

  proto.pauseVideo = function () {
    var media = this.getMedia();

    if (media && media.type === 'video' && media.video) {
      utils.removeClass(media.dom, this.pre + '-playing');

      if (!media.video.loaded) {
        media.dom.innerHTML = '';
        utils.removeClass(media.dom, this.pre + '-loading');
        return;
      }

      if (media.format === 'html5') {
        if (typeof MediaElementPlayer === 'function' && this.options.mediaelement) {
          var mejs = media.video.tagName === 'VIDEO' ? media.video : media.video.getElementsByTagName('video')[0];
          mejs.pause();
        } else {
          media.video.pause();
        }
      } else {
        if (media.pause && media.format !== 'dailymotion') {
          var message = _typeof2(media.pause) === 'object' ? JSON.stringify(media.pause) : String(media.pause);
          media.video.contentWindow.postMessage(message, '*');
        } else {
          media.dom.innerHTML = '';
          media.video = null;
        }
      }
    }
  };

  proto.insertMedia = function (media_index, slide_index) {
    var media = this.gallery[media_index];

    if (!media) {
      return;
    }

    if (typeof media.index === 'undefined') {
      media.index = this.gallery.indexOf(media);
    }

    this.buildMedia(media);
    this.appendMedia(media, slide_index);
    this.loadMedia(media, slide_index);
  };

  proto.buildMedia = function (media) {
    if (typeof media.dom === 'undefined') {
      switch (media.type) {
        case 'image':
          media.dom = utils.createEl('img', this.pre + '-img');
          media.dom.src = media.src;
          break;

        case 'video':
          media.dom = utils.createEl('div', this.pre + '-video');

          if (media.poster) {
            media.dom.style.backgroundImage = 'url("' + media.poster + '")';
          } else {
            media.dom.loaded = true;
          }

          break;

        case 'iframe':
          media.dom = utils.createEl('iframe', this.pre + '-iframe');
          media.dom.setAttribute('allowfullscreen', '');
          media.dom.setAttribute('frameborder', 0);
          media.dom.src = media.src;
          break;

        case 'HTML':
          var element = media.src;
          var content = document.querySelector(element);
          media.dom = utils.createEl('div', this.pre + '-html');
          media.dom.appendChild(utils.createEl('div', this.pre + '-html-inner'));
          media.dom.firstChild.innerHTML = content ? content.innerHTML : null;
          media.src = content ? content : '';
          media.dom.loaded = true;
          break;
      }

      if (!media.type || !media.src) {
        media.dom = utils.createEl('div', this.pre + '-error');
        media.dom.textContent = this.options.noContent;
        media.dom.loaded = true;
        media.dom.error = true;
        utils.dispatchEvent(this, 'modulobox', 'noContent', this.gallery.name, parseInt(media.index, 10));
      }
    }
  };

  proto.appendMedia = function (media, slide_index) {
    var slide = this.slides[slide_index],
        holder = slide.firstChild,
        loader;

    if (!holder.childElementCount) {
      var fragment = document.createDocumentFragment();
      loader = utils.createEl('div', this.pre + '-loader');
      fragment.appendChild(loader);
      fragment.appendChild(media.dom);
      holder.appendChild(fragment);
    } else {
      var oldMedia = holder.lastChild;
      loader = holder.firstChild;
      loader.style.visibility = '';

      if (media.dom !== oldMedia) {
        var method = holder.childElementCount === 1 ? 'appendChild' : 'replaceChild';
        holder[method](media.dom, oldMedia);
      }
    }

    slide.media = media.index;
  };

  proto.loadMedia = function (media, slide_index) {
    if (media.dom.loaded) {
      this.showMedia(media, slide_index);
      return;
    }

    var _this = this,
        dom = media.type === 'iframe' ? media.dom : media.dom.img = new Image();

    var onComplete = function onComplete() {
      if (!media.dom.error) {
        utils.dispatchEvent(_this, 'modulobox', 'loadComplete', _this.gallery.name, parseInt(media.index, 10));
      }

      media.dom.loaded = media.type !== 'iframe' ? true : false;

      _this.showMedia(media, slide_index);
    };

    dom.onload = onComplete;

    dom.onerror = function (e) {
      if (media.type !== 'video') {
        media.dom = utils.createEl('p', _this.pre + '-error');
        media.dom.textContent = _this.options.loadError;
        media.dom.error = true;

        _this.appendMedia(media, slide_index);
      }

      utils.dispatchEvent(_this, 'modulobox', 'loadError', _this.gallery.name, parseInt(media.index, 10));
      onComplete();
    };

    dom.src = media.type === 'video' ? media.poster : media.src;
  };

  proto.unloadMedia = function (slide) {
    if (!this.gallery) {
      return;
    }

    var index = slide.media,
        media = this.gallery[index];

    if (!media || !media.dom) {
      return;
    }

    if (this.options.unload && media.type === 'image' && !media.dom.loaded && !media.dom.complete && !media.dom.naturalWidth) {
      media.dom.onload = null;
      media.dom.onerror = null;
      media.dom.src = '';

      if (media.dom.img) {
        media.dom.img.onload = null;
        media.dom.img.onerror = null;
        media.dom.img.src = '';
        delete media.dom.img;
      }

      delete media.dom;
    } else if (media.type === 'video' && media.format !== 'html5' && media.dom.firstChild) {
      media.video = null;
      media.dom.removeChild(media.dom.firstChild);
    }
  };

  proto.removeMedia = function (holder) {
    var content = holder.firstChild;

    if (!content) {
      return;
    }

    while (content.firstChild) {
      content.removeChild(content.firstChild);
    }
  };

  proto.showMedia = function (media, slide_index) {
    var slider = this.slider;

    if (this.options.fadeIfSettle && !slider.settle && !media.dom.revealed) {
      return;
    }

    var slide = this.slides[slide_index],
        gallery = this.gallery,
        holder = slide.firstChild,
        loader = holder.firstChild,
        preload = this.options.preload;
    this.setMediaSize(media, slide);

    if (media.index === gallery.index) {
      this.isZoomable();
    }

    utils.addClass(media.dom, this.pre + '-media-loaded');
    media.dom.revealed = true;

    if (slide.media === media.index) {
      loader.style.visibility = 'hidden';
      gallery.loaded += 1;

      if (gallery.loaded === preload && preload < 4) {
        this.setMedia(preload + 2);
      }
    }

    if (media.type === 'iframe') {
      media.dom.loaded = false;
    }
  };

  proto.setMediaSize = function (media, slide) {
    var object = media.dom,
        slider = this.slider,
        viewport = object.viewport,
        thumbs = this.getThumbHeight();

    if (object.error) {
      return;
    }

    if (!viewport || viewport.width !== slider.width || viewport.height !== slider.height - thumbs) {
      this.getCaptionHeight(media, slide);
      this.getMediaSize(media, slide);
      this.fitMediaSize(media, slide);
      this.setMediaOffset(media, slide);
    }

    var style = object.style;
    style.width = object.size.width + 'px';
    style.height = object.size.height + 'px';
    style.left = object.offset.left + 'px';
    style.top = object.offset.top + 'px';
  };

  proto.getCaptionHeight = function (media, slide) {
    var caption = this.DOM.captionInner,
        topBar = this.DOM.topBar.height,
        content = caption.innerHTML,
        thumbs = this.getThumbHeight();

    if (this.options.caption && this.states.caption && media.caption) {
      caption.innerHTML = media.caption;
      caption.height = Math.max(topBar, parseInt(caption.clientHeight, 10)) || topBar;
      caption.innerHTML = content;
    } else {
      caption.height = thumbs ? 0 : topBar;
    }

    slide.width = this.slider.width;
    slide.height = this.slider.height - topBar - caption.height - thumbs;
  };

  proto.getMediaSize = function (media, slide) {
    var size = media.dom.size = {};

    switch (media.type) {
      case 'image':
        size.width = media.dom.naturalWidth;
        size.height = media.dom.naturalHeight;
        break;

      case 'video':
        size.width = this.options.videoMaxWidth;
        size.height = size.width / this.options.videoRatio;
        break;

      case 'iframe':
        size.width = media.width ? media.width : slide.width > 680 ? slide.width * 0.8 : slide.width;
        size.height = media.height ? media.height : slide.height;
        break;

      case 'HTML':
        size.width = media.width ? media.width : slide.width;
        size.height = media.height ? media.height : slide.height;
        break;
    }
  };

  proto.fitMediaSize = function (media, slide) {
    var slider = this.slider,
        options = this.options,
        zoom = options.zoomTo,
        size = media.dom.size,
        ratio = size.width / size.height,
        thumbs = this.getThumbHeight(),
        smallDevice = slider.width <= 480 || slider.height <= 680,
        canOverflow = ['video', 'iframe', 'HTML'].indexOf(media.type) < 0,
        smartResize = options.smartResize && smallDevice,
        width,
        height;
    var viewports = [slide.height];

    if ((smartResize || options.overflow) && canOverflow) {
      viewports.unshift(slider.height - thumbs);
    }

    viewports.forEach(function (viewport) {
      if (!height || height < slider.height - thumbs) {
        width = Math.min(size.width, ratio * viewport);
        width = width > slide.width ? slide.width : Math.round(width);
        height = Math.ceil(1 / ratio * width);
        height = height % viewport < 2 ? viewport : height;
      }
    });
    var scale = Number((size.width / width).toFixed(3));
    zoom = zoom === 'auto' ? scale : zoom;
    media.dom.size = {
      width: width,
      height: height,
      scale: scale >= options.minZoom ? Math.min(options.maxZoom, zoom) : 1
    };
  };

  proto.setMediaOffset = function (media, slide) {
    var size = media.dom.size,
        slider = this.slider,
        topBar = this.DOM.topBar.height,
        thumbs = this.getThumbHeight(),
        fromTop = 0;

    if (size.height <= slide.height) {
      fromTop = topBar + (slide.height - size.height) * 0.5;
    }

    media.dom.offset = {
      top: fromTop < 0 ? 0 : Math.round(fromTop),
      left: Math.round((slide.width - size.width) * 0.5)
    };
    media.dom.viewport = {
      width: slider.width,
      height: slider.height - thumbs
    };
  };

  proto.mediaViewport = function (scale) {
    var media = this.getMedia();

    if (!media.dom || !media.dom.size) {
      return {
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
      };
    }

    var size = media.dom.size,
        offset = media.dom.offset,
        height = this.slider.height,
        width = this.slider.width,
        hGap = (height - size.height) * 0.5,
        vGap = offset.top * 2 - hGap,
        cGap = (hGap - vGap) * 0.5,
        sGap = cGap * scale - cGap * 2 - vGap,
        left = size.width / 2 * (scale - 1) - offset.left,
        top = size.height * scale <= height ? cGap * scale : -size.height / 2 * (scale - 1) + height - size.height + sGap,
        bottom = size.height * scale <= height ? cGap * scale : size.height / 2 * (scale - 1) + sGap;
    return {
      top: scale <= 1 ? 0 : Math.round(top),
      bottom: scale <= 1 ? 0 : Math.round(bottom),
      left: size.width * scale < width ? 0 : Math.round(left),
      right: size.width * scale < width ? 0 : Math.round(-left)
    };
  };

  proto.setMedia = function (number) {
    var gallery = this.gallery,
        slides = this.slides,
        loop = this.states.loop,
        RTL = this.isRTL(),
        index = Math.round(-RTL * this.slider.position.x / slides.width),
        length = gallery.initialLength - 1,
        adjust = 0,
        toLoad = [],
        i;

    if (!number && !gallery.loaded) {
      number = 0;

      for (i = 0; i < slides.length; i++) {
        if (slides[i].firstChild.childElementCount) {
          number++;
        }
      }

      number += 2;
      gallery.loaded = this.options.preload;
    }

    switch (number) {
      case 0:
      case 1:
        toLoad = [0];
        break;

      case 2:
      case 3:
        toLoad = [-1, 0, 1];
        break;

      default:
        number = 5;
        toLoad = [-2, -1, 0, 1, 2];
    }

    if (!loop) {
      var maxMedia = index + toLoad[number - 1],
          minMedia = index + toLoad[0];
      adjust = minMedia < 0 ? -minMedia : 0;
      adjust = maxMedia > length ? length - maxMedia : adjust;
    }

    toLoad = toLoad.map(function (i) {
      return utils.modulo(gallery.length, i + adjust + index);
    });

    for (i = 0; i < slides.length; i++) {
      var slide = slides[i],
          media_index = utils.modulo(gallery.length, slide.index);

      if (!loop && slide.index > media_index) {
        continue;
      }

      if (toLoad.indexOf(media_index) > -1 && slide.media !== media_index) {
        this.unloadMedia(slide);
        this.insertMedia(media_index, i);
      }
    }
  };

  proto.updateMediaInfo = function () {
    var slides = this.slides,
        gallery = this.gallery;
    gallery.index = utils.modulo(gallery.length, slides.index);
    this.isZoomable();
    this.isDownloadable();
    this.updateCounter();
    this.updateCaption();
    this.updateThumbs();
    utils.dispatchEvent(this, 'modulobox', 'updateMedia', this.getMedia());
  };

  proto.setThumbs = function () {
    var gallery = this.gallery,
        thumbs = this.thumbs,
        length = gallery.initialLength,
        thumbnails = this.options.thumbnails,
        thumbsHolder = this.DOM.thumbsHolder;

    if (!thumbnails || length < 2) {
      this.DOM.caption.style.bottom = 0;
      thumbsHolder.style.visibility = 'hidden';
      thumbsHolder.style.height = 0;
      thumbs.height = thumbs.gutter = 0;
      return;
    }

    var sizes = this.options.thumbnailSizes,
        screenW = Math.max(window.innerWidth, Math.max(screen.width, screen.height)),
        thumbNb = 0,
        i;
    var widths = Object.keys(sizes).sort(function (a, b) {
      return a - b;
    });

    for (i = 0; i < widths.length; i++) {
      var size = widths[i],
          width = i === widths.length - 1 ? screenW : Math.min(screenW, size),
          number = Math.ceil(width / (sizes[size].width + sizes[size].gutter) * 2);

      if (isFinite(number) && number > thumbNb) {
        thumbNb = number;
      }

      if (size >= screenW) {
        break;
      }
    }

    var fragment = document.createDocumentFragment();
    length = length > 50 ? Math.min(thumbNb, length) : length;

    for (i = 0; i < length; i++) {
      var thumb = utils.createEl('div', this.pre + '-thumb');
      fragment.appendChild(thumb);
    }

    this.DOM.thumbsInner.appendChild(fragment);
    this.setThumbsPosition();
  };

  proto.thumbClick = function (event) {
    var target = event.target;

    if (!utils.hasClass(target, this.pre + '-thumb')) {
      target = target.parentNode;
    }

    if (parseInt(target.index, 10) >= 0) {
      this.slideTo(target.index);
    }
  };

  proto.loadThumb = function (thumb, index) {
    var media = this.gallery[index],
        src;

    if (!media.thumb || _typeof2(media.thumb) !== 'object') {
      src = media.thumb;
      media.thumb = utils.createEl('div', this.pre + '-thumb-bg');
      media.thumb.style.backgroundImage = src && src.indexOf('.json') < 0 ? 'url(' + src + ')' : null;

      if (media.type === 'video') {
        utils.addClass(media.thumb, this.pre + '-thumb-video');
        utils.addClass(media.thumb, this.pre + '-thumb-loaded');
      }
    }

    var method = thumb.firstChild ? 'replaceChild' : 'appendChild';
    thumb[method](media.thumb, thumb.firstChild);
    thumb.media = index;

    if (src) {
      var dom = new Image();

      dom.onload = function () {
        utils.addClass(media.thumb, this.pre + '-thumb-loaded');
      }.bind(this);

      dom.src = src;
    }
  };

  proto.updateThumbs = function () {
    var gallery = this.gallery;

    if (!this.options.thumbnails || gallery.initialLength < 2) {
      return;
    }

    var thumbs = this.thumbs,
        position = this.getThumbPosition(thumbs);
    thumbs.stopAnimate();

    if (position === thumbs.position.x) {
      this.shiftThumbs(thumbs);
      return;
    }

    if (Math.abs(position - thumbs.position.x) > 50 * thumbs.size) {
      this.DOM.thumbsHolder.style.visibility = 'hidden';
      thumbs.position.x = position;
      utils.translate(this.DOM.thumbsInner, position, 0);
      this.renderThumbs(thumbs);
      this.DOM.thumbsHolder.style.visibility = '';
    } else {
      thumbs.startAnimate();
      thumbs.releaseDrag();
      thumbs.animateTo({
        x: position
      });
    }
  };

  proto.updateCaption = function () {
    if (this.options.caption) {
      var media = this.getMedia(),
          content = media.caption ? media.caption : '',
          caption = this.DOM.captionInner;

      if (caption.innerHTML !== content) {
        caption.innerHTML = content;
      }
    }
  };

  proto.updateCounter = function () {
    if (this.options.counterMessage) {
      var gallery = this.gallery,
          length = gallery.initialLength,
          index = utils.modulo(length, gallery.index),
          message = this.options.counterMessage,
          content = message.replace('[index]', index + 1).replace('[total]', length),
          counter = this.DOM.counter;

      if (counter.textContent !== content) {
        counter.textContent = content;
      }
    }
  };

  proto.wrapAround = function () {
    var loop = this.options.loop,
        gallery = this.gallery,
        length = gallery.length;

    if (!gallery.initialLength) {
      gallery.initialLength = length;
    }

    this.states.loop = loop && loop <= length ? true : false;

    if (this.states.loop && length < this.slides.length) {
      var add = Math.ceil(this.slides.length / length) * length - length;

      for (var i = 0; i < add; i++) {
        var index = length + i;
        gallery[index] = utils.cloneObject(gallery[utils.modulo(length, i)]);
        gallery[index].index = index;
      }
    }
  };

  proto.setSlider = function () {
    var slider = this.slider,
        slides = this.slides;
    this.setSizes(slider, slides);
    this.setSliderPosition(slider, slides);
    this.setSlidesPositions(slides);
    this.DOM.overlay.style.opacity = 1;
  };

  proto.setSizes = function (slider, slides) {
    slider.width = document.body.clientWidth;
    slider.height = window.innerHeight;
    slides.width = slider.width + Math.round(slider.width * this.options.spacing);
  };

  proto.setSlidesPositions = function (slides) {
    for (var i = 0; i < slides.length; i++) {
      slides[i].position = null;
      this.setCellPosition(i);
    }

    this.shiftSlides();
  };

  proto.setThumbsPosition = function () {
    if (!this.options.thumbnails || this.gallery.initialLength < 2) {
      return;
    }

    var thumbs = this.thumbs,
        slider = this.slider,
        holder = this.DOM.thumbsHolder,
        inner = this.DOM.thumbsInner,
        sizes = this.options.thumbnailSizes,
        RTL = this.options.rightToLeft,
        widths = Object.keys(sizes).sort(function (a, b) {
      return b - a;
    }),
        width = Math.max.apply(null, widths),
        browser = window.innerWidth,
        size;

    for (var i = 0; i < widths.length; i++) {
      size = Number(widths[i]);

      if (browser <= size) {
        width = size;
      }
    }

    thumbs.width = Number(sizes[width].width);
    thumbs.gutter = Number(sizes[width].gutter);
    thumbs.height = Number(sizes[width].height);
    thumbs.size = thumbs.width + thumbs.gutter;
    thumbs.length = this.gallery.initialLength;
    var totalWidth = thumbs.length * thumbs.size;
    thumbs.bound = {
      left: 0,
      right: totalWidth > slider.width ? slider.width - totalWidth : 0
    };

    if (RTL) {
      thumbs.bound.right = totalWidth > slider.width ? slider.width - thumbs.size : totalWidth - thumbs.size;
      thumbs.bound.left = totalWidth - thumbs.size;
    }

    if (this.options.thumbnailsNav === 'centered') {
      thumbs.bound = {
        left: totalWidth > slider.width ? Math.floor(slider.width * 0.5 - thumbs.size * 0.5) : Math.floor(totalWidth * 0.5 - thumbs.size * 0.5),
        right: totalWidth > slider.width ? Math.ceil(slider.width * 0.5 - totalWidth + thumbs.size * 0.5) : -Math.ceil(totalWidth * 0.5 - thumbs.size * 0.5)
      };

      if (RTL) {
        thumbs.bound.right = thumbs.bound.left;
        thumbs.bound.left = thumbs.bound.left + totalWidth - thumbs.size;
      }
    }

    thumbs.resetAnimate();
    var position = this.getThumbPosition(thumbs);
    thumbs.position.x = position;
    utils.translate(inner, position, 0);
    var hasHeight = this.getThumbHeight();
    holder.style.visibility = hasHeight ? '' : 'hidden';
    holder.style.height = hasHeight ? hasHeight + 'px' : '';
    inner.style.height = hasHeight ? thumbs.height + Math.min(10, thumbs.gutter) + 'px' : '';
    inner.style.width = thumbs.length * thumbs.size + 'px';
    inner.style.right = totalWidth > slider.width && RTL ? 'auto' : '';
  };

  proto.getThumbPosition = function (thumbs) {
    var slider = this.slider,
        gallery = this.gallery,
        thumbNav = this.options.thumbnailsNav,
        RTL = this.isRTL(),
        left = RTL < 0 ? 'right' : 'left',
        index = utils.modulo(gallery.initialLength, gallery.index),
        centered = slider.width * 0.5 - thumbs.size * 0.5,
        position = thumbs.bound[left] - index * thumbs.size * RTL;
    position = !thumbs.bound[left] ? position + centered : position + (RTL < 0 && thumbNav !== 'centered' ? -centered : 0);
    return Math.max(thumbs.bound.right, Math.min(thumbs.bound.left, position));
  };

  proto.setCellPosition = function (index) {
    var cell = this.cells[index];
    cell.resetAnimate();
    utils.translate(this.slides[index].children[0], 0, 0, 1);
  };

  proto.setSliderPosition = function (slider, slides) {
    var RTL = this.options.rightToLeft,
        posX = -slides.index * slides.width;
    posX = RTL ? -posX : posX;
    slider.resetAnimate();
    slider.position.x = slider.attraction.x = posX;
    slider.bound = {
      left: 0,
      right: -(this.gallery.length - 1) * slides.width
    };

    if (RTL) {
      slider.bound.left = -slider.bound.right;
      slider.bound.right = 0;
    }

    utils.translate(this.DOM.slider, posX, 0);
  };

  proto.setAnimation = function () {
    var slider = this.DOM.slider,
        friction = this.options.friction,
        attraction = this.options.attraction;
    this.slider = new Animate(slider, {
      x: 0,
      y: 0
    }, Math.min(Math.max(friction.slider, 0), 1), Math.min(Math.max(attraction.slider, 0), 1));
    this.slider.on('settle.toanimate', this.settleSider.bind(this));
    this.slider.on('render.toanimate', this.renderSlider.bind(this));
    var slides = slider.children,
        length = slides.length;

    for (var i = 0; i < length; i++) {
      this.cells[i] = new Animate(slides[i].children[0], {
        x: 0,
        y: 0,
        s: 1
      }, Math.min(Math.max(friction.slide, 0), 1), Math.min(Math.max(attraction.slide, 0), 1));
      this.cells[i].on('settle.toanimate', this.settleCell.bind(this));
      this.cells[i].on('render.toanimate', this.renderCell.bind(this));
    }

    this.thumbs = new Animate(this.DOM.thumbsInner, {
      x: 0
    }, Math.min(Math.max(friction.thumbs, 0), 1), Math.min(Math.max(attraction.thumbs, 0), 1));
    this.thumbs.on('settle.toanimate', this.settleThumbs.bind(this));
    this.thumbs.on('render.toanimate', this.renderThumbs.bind(this));
  };

  proto.settleSider = function (slider) {
    var media;
    utils.dispatchEvent(this, 'modulobox', 'sliderSettled', slider.position);

    if (this.states.open) {
      this.setMedia();
      this.replaceState();
    }

    if (this.options.fadeIfSettle) {
      var slides = this.slides;

      for (var i = 0; i < slides.length; i++) {
        var index = slides[i].media;
        media = this.gallery[index];

        if (media.dom.loaded) {
          this.showMedia(media, i);
        }
      }
    }
  };

  proto.settleCell = function (cell) {
    var gesture = this.gesture;

    if (gesture.closeBy) {
      utils.dispatchEvent(this, 'modulobox', 'panYSettled', null, cell.position);
    }

    if (gesture.closeBy && gesture.canClose === false || !gesture.closeBy) {
      utils.dispatchEvent(this, 'modulobox', 'panZoomSettled', null, cell.position);
    }
  };

  proto.settleThumbs = function (thumbs) {
    utils.dispatchEvent(this, 'modulobox', 'thumbsSettled', null, thumbs.position);
  };

  proto.renderSlider = function (slider) {
    this.shiftSlides();
    var RTL = this.isRTL(),
        length = this.gallery.initialLength,
        indexPos = -RTL * slider.position.x / this.slides.width,
        moduloPos = utils.modulo(length, indexPos),
        progress = (moduloPos > length - 0.5 ? 0 : moduloPos) / (length - 1);
    utils.dispatchEvent(this, 'modulobox', 'sliderProgress', null, Math.min(1, Math.max(0, progress)));
  };

  proto.renderCell = function (cell) {
    this.willClose(cell);
    var progress;

    if (this.gesture.type === 'panY' || this.gesture.closeBy || this.gesture.type === 'dragSlider' && cell.position.y !== 0) {
      progress = 1 - Math.abs(cell.position.y) / (this.slider.height * 0.5);
      utils.dispatchEvent(this, 'modulobox', 'panYProgress', null, progress);
    }

    if (this.gesture.type !== 'panY' && cell.position.s !== 1) {
      progress = cell.position.s;
      utils.dispatchEvent(this, 'modulobox', 'panZoomProgress', null, progress);
    }
  };

  proto.renderThumbs = function (thumbs) {
    this.shiftThumbs(thumbs);
    var progress = thumbs.bound.left !== thumbs.bound.right ? (thumbs.bound.left - thumbs.position.x) / (thumbs.bound.left - thumbs.bound.right) : 0;
    utils.dispatchEvent(this, 'modulobox', 'thumbsProgress', null, progress);
  };

  proto.touchStart = function (event) {
    var element = event.target,
        tagName = element.tagName,
        className = element.className;

    if (event.which !== 3 && element !== this.buttons.play) {
      this.stopSlideShow();
    }

    if (event.which === 3 || !this.isEl(event) || ['BUTTON', 'VIDEO', 'INPUT', 'A'].indexOf(tagName) > -1) {
      return;
    }

    if (tagName === 'IMG' && this.gallery.length > 1) {
      utils.addClass(this.DOM.holder, this.pre + '-dragging');
    }

    event.preventDefault();

    if (utils.hasClass(this.DOM.holder, this.pre + '-open-tooltip')) {
      return;
    }

    if (!this.pointers.length) {
      this.gesture.canClose = undefined;
      utils.handleEvents(this, window, this.dragEvents.move, 'touchMove');
      utils.handleEvents(this, window, this.dragEvents.end, 'touchEnd');
    }

    this.addPointer(event);

    if (className.indexOf('-thumb') < 0) {
      this.slider.stopAnimate();
      var cell = this.getCell();

      if (Math.round(cell.position.s * 100) / 100 !== 1 || this.pointers.length === 2 || this.gesture.closeBy) {
        cell.stopAnimate();
      }
    } else {
      this.thumbs.stopAnimate();
    }

    this.gestures('start');
  };

  proto.touchMove = function (event) {
    this.updatePointer(event);
    var gesture = this.gesture;
    var pointerNb = this.pointers.length;
    var isSettle = this.isSliderSettle();
    this.switchPointers();
    this.gestures('move');

    if (gesture.type) {
      this[gesture.type](event);
      utils.dispatchEvent(this, 'modulobox', gesture.type + 'Move', event, gesture);
      gesture.move = true;
    } else if (pointerNb === 2 && isSettle || Math.abs(gesture.dx) > this.options.threshold || Math.abs(gesture.dy) > this.options.threshold) {
      gesture.sx += gesture.dx;
      gesture.sy += gesture.dy;
      gesture.canZoom = this.isZoomable();
      gesture.closeBy = false;
      gesture.type = Math.abs(gesture.dx) < Math.abs(gesture.dy) / 2 ? false : 'dragSlider';
      gesture.type = this.options.dragToClose && !gesture.type && isSettle ? 'panY' : gesture.type;
      gesture.type = (this.options.pinchToZoom || this.states.zoom) && gesture.canZoom && isSettle && (pointerNb === 2 || this.states.zoom) ? 'panZoom' : gesture.type;
      gesture.type = this.options.pinchToClose && gesture.scale < 1 && isSettle && pointerNb === 2 ? 'panZoom' : gesture.type;
      gesture.type = event.target.className.indexOf('-thumb') > -1 ? 'dragThumbs' : gesture.type;

      if (gesture.type === 'dragSlider') {
        this.setMedia();
      }

      if (['dragSlider', 'dragThumbs'].indexOf(gesture.type) > -1) {
        var cell = this.getCell();
        cell.startAnimate();
        cell.releaseDrag();
        cell.animateTo({
          x: 0,
          y: 0,
          s: 1
        });
      }

      if (gesture.type !== 'dragSlider') {
        var slider = this.slider,
            slides = this.slides;
        var RTL = this.isRTL();

        if (-RTL * slider.position.x !== slides.index * slides.width) {
          slider.startAnimate();
          slider.releaseDrag();
        }
      }

      if (gesture.type) {
        this.pauseVideo();
        utils.dispatchEvent(this, 'modulobox', gesture.type + 'Start', event, gesture);

        if (this.gallery.length > 1 || gesture.type !== 'dragSlider') {
          utils.addClass(this.DOM.holder, this.pre + '-dragging');
        }
      }
    }
  };

  proto.touchEnd = function (event) {
    this.deletePointer(event);

    if (!this.pointers.length) {
      utils.removeClass(this.DOM.holder, this.pre + '-dragging');
      utils.handleEvents(this, window, this.dragEvents.move, 'touchMove', false);
      utils.handleEvents(this, window, this.dragEvents.end, 'touchEnd', false);

      if (this.isSliderSettle()) {
        var className = event.target.className;

        if (utils.hasClass(event.target, this.pre + '-video')) {
          this.appendVideo();
        } else if (this.options.tapToClose && !this.states.zoom && (className === this.pre + '-item-inner' || className === this.pre + '-top-bar') && Math.abs(this.gesture.dx) < this.options.threshold) {
          this.close();
          return;
        }

        if (event.target.tagName === 'IMG') {
          this.doubleTap(event);
        }
      }

      if (this.options.thumbnails && !this.gesture.move) {
        this.thumbClick(event);
      }

      var gestureEnd = this.gesture.type + 'End';

      if (this.gesture.type && typeof this[gestureEnd] === 'function') {
        this[gestureEnd](event);
        utils.dispatchEvent(this, 'modulobox', gestureEnd, event, this.gesture);
      }

      this.gesture.type = this.gesture.move = false;

      if (!this.states.open) {
        return;
      }

      var cell = this.getCell();

      if (!cell.settle) {
        cell.startAnimate();
        cell.releaseDrag();
      }

      var slider = this.slider;

      if (!slider.settle) {
        slider.startAnimate();
        slider.releaseDrag();
      }
    }
  };

  proto.switchPointers = function () {
    if (this.gesture.type === 'panZoom' && this.pointers.length === 1 && this.gesture.distance !== 0) {
      var moving = this.getCell();
      moving.stopAnimate();
      moving.startAnimate();
      this.gesture.move = false;
      this.gestures('start');
      this.gestures('move');
    }
  };

  proto.doubleTap = function (event) {
    event.preventDefault();
    var touches = this.mapPointer(event),
        clientX = touches[0].clientX,
        clientY = touches[0].clientY;

    if (typeof this.tap !== 'undefined' && +new Date() - this.tap.delay < 350 && Math.abs(this.tap.deltaX - clientX) < 30 && Math.abs(this.tap.deltaY - clientY) < 30) {
      if (this.states.tapIdle) {
        clearTimeout(this.states.tapIdle);
      }

      if (this.options.doubleTapToZoom) {
        this.zoomTo(clientX, clientY);
      }

      this.tap = undefined;
    } else {
      if (this.browser.touchDevice && this.options.timeToIdle && !this.states.idle) {
        this.states.tapIdle = setTimeout(function () {
          var method = !utils.hasClass(this.DOM.holder, this.pre + '-idle') ? 'add' : 'remove';
          utils[method + 'Class'](this.DOM.holder, this.pre + '-idle');
        }.bind(this), 350);
      }

      this.tap = {
        delay: +new Date(),
        deltaX: touches[0].clientX,
        deltaY: touches[0].clientY
      };
    }
  };

  proto.isSliderSettle = function () {
    if (this.gesture.type) {
      return false;
    }

    var RTL = this.isRTL(),
        slides = this.slides,
        width = slides.width,
        toSettle = Math.abs(RTL * this.slider.position.x + slides.index * width) / width * 100;
    return toSettle <= 3 ? true : false;
  };

  proto.mapPointer = function (event) {
    return event.touches ? event.changedTouches : [event];
  };

  proto.addPointer = function (event) {
    var pointers = this.mapPointer(event);

    for (var i = 0; i < pointers.length; i++) {
      if (this.pointers.length < 2 && ['dragSlider', 'panY', 'dragThumbs'].indexOf(this.gesture.type) === -1) {
        var ev = pointers[i],
            id = ev.pointerId !== undefined ? ev.pointerId : ev.identifier;

        if (!this.getPointer(id)) {
          this.pointers[this.pointers.length] = {
            id: id,
            x: Math.round(ev.clientX),
            y: Math.round(ev.clientY)
          };
        }
      }
    }
  };

  proto.updatePointer = function (event) {
    var pointers = this.mapPointer(event);

    for (var i = 0; i < pointers.length; i++) {
      var ev = pointers[i],
          id = ev.pointerId !== undefined ? ev.pointerId : ev.identifier,
          pt = this.getPointer(id);

      if (pt) {
        pt.x = Math.round(ev.clientX);
        pt.y = Math.round(ev.clientY);
      }
    }
  };

  proto.deletePointer = function (event) {
    var pointers = this.mapPointer(event);

    for (var i = 0; i < pointers.length; i++) {
      var ev = pointers[i],
          id = ev.pointerId !== undefined ? ev.pointerId : ev.identifier;

      for (var p = 0; p < this.pointers.length; p++) {
        if (this.pointers[p].id === id) {
          this.pointers.splice(p, 1);
        }
      }
    }
  };

  proto.getPointer = function (id) {
    for (var k in this.pointers) {
      if (this.pointers[k].id === id) {
        return this.pointers[k];
      }
    }

    return null;
  };

  proto.gestures = function (type) {
    var g = this.gesture,
        pointers = this.pointers,
        distance;

    if (!pointers.length) {
      return;
    }

    g.direction = g.x ? pointers[0].x > g.x ? 1 : -1 : 0;
    g.x = pointers[0].x;
    g.y = pointers[0].y;

    if (pointers.length === 2) {
      var x2 = pointers[1].x,
          y2 = pointers[1].y;
      distance = this.getDistance([g.x, g.y], [x2, y2]);
      g.x = g.x - (g.x - x2) / 2;
      g.y = g.y - (g.y - y2) / 2;
    }

    if (type === 'start') {
      g.dx = 0;
      g.dy = 0;
      g.sx = g.x;
      g.sy = g.y;
      g.distance = distance ? distance : 0;
    } else {
      g.dx = g.x - g.sx;
      g.dy = g.y - g.sy;
      g.scale = distance && g.distance ? distance / g.distance : 1;
    }
  };

  proto.getDistance = function (p1, p2) {
    var x = p2[0] - p1[0],
        y = p2[1] - p1[1];
    return Math.sqrt(x * x + y * y);
  };

  proto.panY = function () {
    var moving = this.getCell();
    moving.startAnimate();
    moving.updateDrag({
      x: moving.position.x,
      y: moving.start.y + this.gesture.dy,
      s: moving.position.s
    });
  };

  proto.panYEnd = function () {
    var posY = 0,
        moving = this.getCell(),
        height = this.slider.height,
        resting = moving.resting.y;

    if (1 - Math.abs(resting) / (height * 0.5) < 0.8) {
      posY = Math.abs(resting) < height * 0.5 ? Math.abs(resting) / resting * height * 0.5 : resting;
      this.close();
      moving.animateTo({
        x: 0,
        y: posY,
        s: posY ? moving.resting.s : 1
      });
      moving.startAnimate();
      moving.releaseDrag();
    }
  };

  proto.panZoom = function () {
    var moving = this.getCell(),
        gesture = this.gesture,
        bound = this.mediaViewport(moving.position.s),
        minZoom = this.options.pinchToClose && gesture.canClose ? 0.1 : 0.6,
        scale = Math.min(this.options.maxZoom * 1.5, Math.max(minZoom, moving.start.s * gesture.scale)),
        posX = moving.start.x + gesture.dx,
        posY = moving.start.y + gesture.dy,
        centerX = gesture.sx - this.slider.width * 0.5,
        centerY = gesture.sy - this.slider.height * 0.5;

    if (!gesture.canZoom || !this.options.pinchToZoom && !this.states.zoom) {
      scale = Math.min(1, scale);
    }

    if (!this.options.pinchToZoom && this.states.zoom) {
      scale = moving.position.s;
    }

    if (!gesture.move && this.pointers.length === 1) {
      moving.start.x += posX > bound.left ? posX - bound.left : posX < bound.right ? posX - bound.right : 0;
      moving.start.y += posY > bound.bottom ? posY - bound.bottom : posY < bound.top ? posY - bound.top : 0;
    }

    posX = gesture.dx + centerX + (moving.start.x - centerX) * (scale / moving.start.s);
    posY = gesture.dy + centerY + (moving.start.y - centerY) * (scale / moving.start.s);

    if (this.pointers.length === 1) {
      posX = posX > bound.left ? (posX + bound.left) * 0.5 : posX < bound.right ? (posX + bound.right) * 0.5 : posX;
      posY = posY > bound.bottom ? (posY + bound.bottom) * 0.5 : posY < bound.top ? (posY + bound.top) * 0.5 : posY;
    }

    moving.startAnimate();
    moving.updateDrag({
      x: posX,
      y: posY,
      s: scale
    });
    this.updateZoom(scale);
  };

  proto.panZoomEnd = function () {
    var moving = this.getCell(),
        gesture = this.gesture,
        scale = moving.resting.s > this.options.maxZoom ? this.options.maxZoom : moving.resting.s < 1 ? 1 : moving.resting.s,
        bound = this.mediaViewport(scale),
        posX,
        posY;

    if (Math.round(moving.resting.s * 10) / 10 > this.options.maxZoom) {
      var centerX = gesture.distance ? gesture.sx - this.slider.width * 0.5 : 0;
      var centerY = gesture.distance ? gesture.sy - this.slider.height * 0.5 : 0;
      posX = gesture.dx + centerX + (moving.start.x - centerX) * (scale / moving.start.s);
      posY = gesture.dy + centerY + (moving.start.y - centerY) * (scale / moving.start.s);
      posX = posX > bound.left ? bound.left : posX < bound.right ? bound.right : posX;
      posY = posY > bound.bottom ? bound.bottom : posY < bound.top ? bound.top : posY;
    } else {
      posX = moving.resting.x > bound.left ? bound.left : moving.resting.x < bound.right ? bound.right : undefined;
      posY = moving.resting.y > bound.bottom ? bound.bottom : moving.resting.y < bound.top ? bound.top : undefined;
    }

    if (this.options.pinchToClose && moving.resting.s < 0.8 && gesture.canClose) {
      scale = moving.resting.s < 0.3 ? moving.resting.s : 0.15;
      posX = moving.resting.x;
      posY = moving.resting.y;
      this.close();
    }

    moving.animateTo({
      x: posX,
      y: posY,
      s: scale !== moving.resting.s ? scale : undefined
    });
    moving.startAnimate();
    moving.releaseDrag();
    this.updateZoom(moving.resting.s);
  };

  proto.dragThumbs = function () {
    var moving = this.thumbs,
        bound = moving.bound,
        posX = moving.start.x + this.gesture.dx;

    if (!this.gesture.move) {
      moving.start.x += posX > bound.left ? posX - bound.left : posX < bound.right ? posX - bound.right : 0;
      posX = moving.start.x + this.gesture.dx;
    }

    posX = posX > bound.left ? (posX + bound.left) * 0.5 : posX < bound.right ? (posX + bound.right) * 0.5 : posX;
    moving.startAnimate();
    moving.attraction.x = undefined;
    moving.updateDrag({
      x: posX
    });
  };

  proto.dragThumbsEnd = function () {
    var moving = this.thumbs,
        bound = moving.bound,
        posX = moving.resting.x;
    posX = posX > bound.left ? bound.left : posX < bound.right ? bound.right : posX;

    if (posX !== moving.resting.x) {
      moving.animateTo({
        x: posX
      });
    }

    moving.startAnimate();
    moving.releaseDrag();
  };

  proto.dragSlider = function () {
    if (this.gallery.length === 1) {
      return;
    }

    var moving = this.slider,
        posX = moving.start.x + this.gesture.dx;

    if (!this.states.loop) {
      var bound = moving.bound;

      if (!this.gesture.move) {
        moving.start.x += posX > bound.left ? posX - bound.left : posX < bound.right ? posX - bound.right : 0;
        posX = moving.start.x + this.gesture.dx;
      }

      posX = posX > bound.left ? (posX + bound.left) * 0.5 : posX < bound.right ? (posX + bound.right) * 0.5 : posX;
    }

    moving.startAnimate();
    moving.updateDrag({
      x: posX
    });
  };

  proto.dragSliderEnd = function () {
    if (this.gallery.length === 1) {
      return;
    }

    var moving = this.slider,
        slides = this.slides,
        oldIndex = slides.index,
        RTL = this.isRTL(),
        restingX = moving.resting.x,
        positionX = moving.position.x;
    this.getRestingIndex(positionX, restingX);

    if (oldIndex !== slides.index) {
      this.updateMediaInfo();
    }

    this.slider.animateTo({
      x: -RTL * slides.index * slides.width,
      y: undefined,
      s: undefined
    });
    moving.startAnimate();
    moving.releaseDrag();
  };

  proto.getRestingIndex = function (positionX, restingX) {
    var direction = this.gesture.direction,
        gallery = this.gallery,
        slides = this.slides,
        deltaX = this.gesture.dx,
        RTL = this.isRTL(),
        index = Math.round(-RTL * positionX / slides.width),
        moved = Math.abs(restingX - positionX);

    if (Math.abs(deltaX) < slides.width * 0.5 && moved) {
      if (deltaX > 0 && direction > 0) {
        index -= 1 * RTL;
      } else if (deltaX < 0 && direction < 0) {
        index += 1 * RTL;
      }
    }

    var gap = Math.max(-1, Math.min(1, index - slides.index));

    if (!this.states.loop && (gallery.index + gap < 0 || gallery.index + gap > gallery.length - 1)) {
      return;
    }

    slides.index += gap;
  };

  proto.shiftSlides = function () {
    var slides = this.slides,
        gallery = this.gallery,
        loop = this.states.loop,
        RTL = this.isRTL(),
        from = RTL * Math.round(-this.slider.position.x / slides.width) - 2,
        to = from + 5;

    if (!loop && to > gallery.initialLength - 1) {
      from = gallery.initialLength - 5;
      to = from + 5;
    }

    if (!loop && from < 0) {
      from = 0;
      to = 5;
    }

    for (var i = from; i < to; i++) {
      var position = RTL * i * slides.width,
          slide_index = utils.modulo(slides.length, i),
          slide = slides[slide_index];

      if (slide.index !== i || slide.position !== position) {
        slide.index = i;
        slide.position = position;
        slide.style.left = position + 'px';
      }
    }

    if (this.states.open) {
      this.setMedia(3);
    }
  };

  proto.shiftThumbs = function (thumbs) {
    var child = this.DOM.thumbsInner.children,
        slider = this.slider,
        gallery = this.gallery,
        RTL = this.isRTL(),
        length = child.length,
        index = utils.modulo(gallery.initialLength, gallery.index),
        width = thumbs.size * length * 0.5,
        center = Math.round((-RTL * thumbs.position.x + RTL * width * 0.5) / thumbs.size),
        from = Math.max(0, center - Math.floor(length / 2)),
        to = from + length;
    var tolerance = slider.width * 0.5,
        boundLeft = thumbs.position.x + tolerance,
        boundRight = thumbs.position.x - slider.width - tolerance;

    if (to > gallery.initialLength) {
      to = gallery.initialLength;
      from = to - length;
    }

    if (to === gallery.initialLength - 1 && from - to < length) {
      from = gallery.initialLength - length;
    }

    for (var i = from; i < to; i++) {
      var thumb = child[utils.modulo(length, i)],
          position = RTL * i * thumbs.size + thumbs.gutter * 0.5,
          className = this.pre + '-active-thumb',
          isActive = utils.hasClass(thumb, className);

      if (thumb.index !== i || thumb.position !== position) {
        thumb.index = i;
        thumb.position = position;
        thumb.style.left = position + 'px';
      }

      this.setThumbSize(thumb, thumbs);

      if (-thumb.position <= boundLeft && -thumb.position >= boundRight && thumb.media !== i) {
        this.loadThumb(thumb, i);
      }

      if (isActive && index !== i) {
        utils.removeClass(thumb, className);
      } else if (!isActive && index === i) {
        utils.addClass(thumb, className);
      }
    }
  };

  proto.setThumbSize = function (thumb, thumbs) {
    if (thumb.width !== thumbs.width || thumb.height !== thumbs.height || thumb.gutter !== thumbs.gutter) {
      thumb.width = thumbs.width;
      thumb.height = thumbs.height;
      thumb.gutter = thumbs.gutter;
      thumb.style.width = thumbs.width + 'px';
      thumb.style.height = thumbs.height + 'px';
    }
  };

  proto.willClose = function (cell) {
    var opacity = this.DOM.overlay.style.opacity,
        canClose = this.gesture.canClose,
        gestureType = this.gesture.type,
        gestureClose = this.gesture.closeBy,
        pinchToClose = gestureType === 'panZoom' || gestureClose === 'panZoom',
        dragYToClose = gestureType === 'panY' || gestureClose === 'panY';

    if (cell.position.s > 1.1 && typeof canClose === 'undefined') {
      this.gesture.canClose = false;
    } else if (cell.position.s < 1 && typeof canClose === 'undefined') {
      this.gesture.canClose = true;
    }

    if (this.options.pinchToClose && pinchToClose && this.gesture.canClose) {
      opacity = cell.position.s;
      this.gesture.closeBy = 'panZoom';
    } else if (dragYToClose) {
      opacity = 1 - Math.abs(cell.position.y) / (this.slider.height * 0.5);
      this.gesture.closeBy = 'panY';
    } else if (opacity && opacity < 1) {
      opacity = 1;
      this.gesture.closeBy = false;
    }

    opacity = !opacity ? 1 : Math.max(0, Math.min(1, opacity));
    var method = opacity <= 0.8 || !opacity ? 'add' : 'remove';
    utils[method + 'Class'](this.DOM.holder, this.pre + '-will-close');
    this.DOM.overlay.style.opacity = opacity;
  };

  proto.prev = utils.throttle(function () {
    if (!this.gesture.move) {
      this.slideTo(this.slides.index - 1 * this.isRTL());
    }
  }, 120);
  proto.next = utils.throttle(function () {
    if (!this.gesture.move) {
      this.slideTo(this.slides.index + 1 * this.isRTL());
    }
  }, 120);

  proto.slideTo = function (to, slideShow) {
    var slides = this.slides,
        gallery = this.gallery,
        holder = this.DOM.slider,
        RTL = this.isRTL(),
        length = gallery.initialLength,
        moduloTo = utils.modulo(length, to),
        moduloFrom = utils.modulo(length, gallery.index),
        slideBy = moduloTo - moduloFrom,
        fromEnds = length - Math.abs(slideBy);

    if (!this.states.loop && (to < 0 || to > this.gallery.initialLength - 1)) {
      return;
    }

    if (this.states.loop && fromEnds < 3 && fromEnds * 2 < length) {
      slideBy = slideBy < 0 ? fromEnds : -fromEnds;
    }

    if (moduloTo === to) {
      to = slides.index + slideBy;
    }

    slideBy = to - slides.index;

    if (!slideBy) {
      return;
    }

    if (this.states.zoom) {
      this.zoom();
    }

    this.pauseVideo();
    this.share();

    if (!slideShow) {
      this.stopSlideShow();
    }

    slides.index = to;
    var slider = this.slider;

    if (Math.abs(slideBy) > 2) {
      utils.addClass(holder, this.pre + '-hide');
      this.setSliderPosition(slider, slides);
      this.setSlidesPositions(slides);
      var moveBy = RTL * slides.width * Math.min(2, Math.abs(slideBy)) * Math.abs(slideBy) / slideBy;
      slider.position.x = slider.attraction.x = slider.position.x + moveBy;
      utils.translate(holder, slider.position.x, 0);
      holder.getClientRects();
    }

    this.updateMediaInfo();
    utils.removeClass(holder, this.pre + '-hide');
    slider.startAnimate();
    slider.releaseDrag();
    slider.animateTo({
      x: -RTL * to * slides.width,
      y: 0,
      s: undefined
    });
  };

  proto.keyDown = function (event) {
    var key = event.keyCode,
        opt = this.options;

    if (opt.prevNextKey) {
      if (key === 37) {
        this.prev(event);
      } else if (key === 39) {
        this.next(event);
      }
    }

    if (key === 27 && opt.escapeToClose) {
      this.close();
    }

    if (!opt.mouseWheel && [32, 33, 34, 35, 36, 38, 40].indexOf(key) > -1) {
      event.preventDefault();
      return false;
    }
  };

  proto.zoom = function () {
    this.zoomTo();
  };

  proto.zoomTo = function (x, y, scale) {
    if (!this.isSliderSettle() || !this.isZoomable() && scale > 1) {
      return;
    }

    this.gesture.closeBy = false;
    var media = this.getMedia();
    scale = scale ? scale : this.states.zoom ? 1 : media.dom.size.scale;
    var cell = this.getCell(),
        bound = this.mediaViewport(scale),
        centX = x ? x - this.slider.width * 0.5 : 0,
        centY = y ? y - this.slider.height * 0.5 : 0,
        PosX = scale > 1 ? Math.ceil(centX + (cell.position.x - centX) * (scale / cell.position.s)) : 0,
        PosY = scale > 1 ? Math.ceil(centY + (cell.position.y - centY) * (scale / cell.position.s)) : 0;
    cell.startAnimate();
    cell.releaseDrag();
    cell.animateTo({
      x: PosX > bound.left ? bound.left : PosX < bound.right ? bound.right : PosX,
      y: PosY > bound.bottom ? bound.bottom : PosY < bound.top ? bound.top : PosY,
      s: scale
    });
    this.updateZoom(scale);
  };

  proto.updateZoom = function (scale) {
    this.states.zoom = scale > 1 ? true : false;
    utils[this.states.zoom ? 'addClass' : 'removeClass'](this.DOM.holder, this.pre + '-panzoom');
  };

  proto.destroy = function () {
    if (!this.GUID) {
      return;
    }

    if (this.states.open) {
      this.close();
    }

    var selectors = this.options.mediaSelector,
        sources = '';

    try {
      sources = document.querySelectorAll(selectors);
    } catch (error) {}

    for (var i = 0, l = sources.length; i < l; i++) {
      var source = sources[i];

      if (source.mobxListener) {
        source.removeEventListener('click', source.mobxListener, false);
      }
    }

    this.bindEvents(false);
    this.slider.resetAnimate();

    for (i = 0; i < this.slides.length; i++) {
      this.cells[i].resetAnimate();
    }

    if (this.thumbs) {
      this.thumbs.resetAnimate();
    }

    this.DOM.holder.parentNode.removeChild(this.DOM.holder);
    this.DOM.comment.parentNode.removeChild(this.DOM.comment);
    delete instances[this.GUID];
    delete this.DOM.holder.GUID;
    delete this.GUID;
  };

  if (typeof jQuery !== 'undefined') {
    (function ($) {
      $.ModuloBox = function (options) {
        return new ModuloBox(options);
      };
    })(jQuery);
  }

  return ModuloBox;
});

;
jQuery(function () {
  ParallaxScroll.init();
});
var ParallaxScroll = {
  showLogs: false,
  round: 1000,
  init: function init() {
    this._log("init");

    if (this._inited) {
      this._log("Already Inited");

      this._inited = true;
      return;
    }

    this._requestAnimationFrame = function () {
      return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (callback, element) {
        window.setTimeout(callback, 1000 / 60);
      };
    }();

    this._onScroll(true);
  },
  _inited: false,
  _properties: ['x', 'y', 'z', 'rotateX', 'rotateY', 'rotateZ', 'scaleX', 'scaleY', 'scaleZ', 'scale'],
  _requestAnimationFrame: null,
  _log: function _log(message) {
    if (this.showLogs) console.log("Parallax Scroll / " + message);
  },
  _onScroll: function _onScroll(noSmooth) {
    var scroll = jQuery(document).scrollTop();
    var windowHeight = jQuery(window).height();

    this._log("onScroll " + scroll);

    jQuery("[data-parallax]").each(jQuery.proxy(function (index, el) {
      var $el = jQuery(el);
      var properties = [];
      var applyProperties = false;
      var style = $el.data("style");

      if (style == undefined) {
        style = $el.attr("style") || "";
        $el.data("style", style);
      }

      var datas = [$el.data("parallax")];
      var iData;

      for (iData = 2;; iData++) {
        if ($el.data("parallax" + iData)) {
          datas.push($el.data("parallax-" + iData));
        } else {
          break;
        }
      }

      var datasLength = datas.length;

      for (iData = 0; iData < datasLength; iData++) {
        var data = datas[iData];
        var scrollFrom = data["from-scroll"];
        if (scrollFrom == undefined) scrollFrom = Math.max(0, jQuery(el).offset().top - windowHeight);
        scrollFrom = scrollFrom | 0;
        var scrollDistance = data["distance"];
        var scrollTo = data["to-scroll"];
        if (scrollDistance == undefined && scrollTo == undefined) scrollDistance = windowHeight;
        scrollDistance = Math.max(scrollDistance | 0, 1);
        var easing = data["easing"];
        var easingReturn = data["easing-return"];
        if (easing == undefined || !jQuery.easing || !jQuery.easing[easing]) easing = null;
        if (easingReturn == undefined || !jQuery.easing || !jQuery.easing[easingReturn]) easingReturn = easing;

        if (easing) {
          var totalTime = data["duration"];
          if (totalTime == undefined) totalTime = scrollDistance;
          totalTime = Math.max(totalTime | 0, 1);
          var totalTimeReturn = data["duration-return"];
          if (totalTimeReturn == undefined) totalTimeReturn = totalTime;
          scrollDistance = 1;
          var currentTime = $el.data("current-time");
          if (currentTime == undefined) currentTime = 0;
        }

        if (scrollTo == undefined) scrollTo = scrollFrom + scrollDistance;
        scrollTo = scrollTo | 0;
        var smoothness = data["smoothness"];
        if (smoothness == undefined) smoothness = 30;
        smoothness = smoothness | 0;
        if (noSmooth || smoothness == 0) smoothness = 1;
        smoothness = smoothness | 0;
        var scrollCurrent = scroll;
        scrollCurrent = Math.max(scrollCurrent, scrollFrom);
        scrollCurrent = Math.min(scrollCurrent, scrollTo);

        if (easing) {
          if ($el.data("sens") == undefined) $el.data("sens", "back");

          if (scrollCurrent > scrollFrom) {
            if ($el.data("sens") == "back") {
              currentTime = 1;
              $el.data("sens", "go");
            } else {
              currentTime++;
            }
          }

          if (scrollCurrent < scrollTo) {
            if ($el.data("sens") == "go") {
              currentTime = 1;
              $el.data("sens", "back");
            } else {
              currentTime++;
            }
          }

          if (noSmooth) currentTime = totalTime;
          $el.data("current-time", currentTime);
        }

        this._properties.map(jQuery.proxy(function (prop) {
          var defaultProp = 0;
          var to = data[prop];
          if (to == undefined) return;

          if (prop == "scale" || prop == "scaleX" || prop == "scaleY" || prop == "scaleZ") {
            defaultProp = 1;
          } else {
            to = to | 0;
          }

          var prev = $el.data("_" + prop);
          if (prev == undefined) prev = defaultProp;
          var next = (to - defaultProp) * ((scrollCurrent - scrollFrom) / (scrollTo - scrollFrom)) + defaultProp;
          var val = prev + (next - prev) / smoothness;

          if (easing && currentTime > 0 && currentTime <= totalTime) {
            var from = defaultProp;

            if ($el.data("sens") == "back") {
              from = to;
              to = -to;
              easing = easingReturn;
              totalTime = totalTimeReturn;
            }

            val = jQuery.easing[easing](null, currentTime, from, to, totalTime);
          }

          val = Math.ceil(val * this.round) / this.round;
          if (val == prev && next == to) val = to;
          if (!properties[prop]) properties[prop] = 0;
          properties[prop] += val;

          if (prev != properties[prop]) {
            $el.data("_" + prop, properties[prop]);
            applyProperties = true;
          }
        }, this));
      }

      if (applyProperties) {
        if (properties["z"] != undefined) {
          var perspective = data["perspective"];
          if (perspective == undefined) perspective = 800;
          var $parent = $el.parent();
          if (!$parent.data("style")) $parent.data("style", $parent.attr("style") || "");
          $parent.attr("style", "perspective:" + perspective + "px; -webkit-perspective:" + perspective + "px; " + $parent.data("style"));
        }

        if (properties["scaleX"] == undefined) properties["scaleX"] = 1;
        if (properties["scaleY"] == undefined) properties["scaleY"] = 1;
        if (properties["scaleZ"] == undefined) properties["scaleZ"] = 1;

        if (properties["scale"] != undefined) {
          properties["scaleX"] *= properties["scale"];
          properties["scaleY"] *= properties["scale"];
          properties["scaleZ"] *= properties["scale"];
        }

        var translate3d = "translate3d(" + (properties["x"] ? properties["x"] : 0) + "px, " + (properties["y"] ? properties["y"] : 0) + "px, " + (properties["z"] ? properties["z"] : 0) + "px)";
        var rotate3d = "rotateX(" + (properties["rotateX"] ? properties["rotateX"] : 0) + "deg) rotateY(" + (properties["rotateY"] ? properties["rotateY"] : 0) + "deg) rotateZ(" + (properties["rotateZ"] ? properties["rotateZ"] : 0) + "deg)";
        var scale3d = "scaleX(" + properties["scaleX"] + ") scaleY(" + properties["scaleY"] + ") scaleZ(" + properties["scaleZ"] + ")";
        var cssTransform = translate3d + " " + rotate3d + " " + scale3d + ";";

        this._log(cssTransform);

        $el.attr("style", "transform:" + cssTransform + " -webkit-transform:" + cssTransform + " " + style);
      }
    }, this));

    if (window.requestAnimationFrame) {
      window.requestAnimationFrame(jQuery.proxy(this._onScroll, this, false));
    } else {
      this._requestAnimationFrame(jQuery.proxy(this._onScroll, this, false));
    }
  }
};
;
/*!
* jQuery Smoove v0.2.11 (http://smoove.js.org/)
* Copyright (c) 2017 Adam Bouqdib
* Licensed under GPL-2.0 (http://abemedia.co.uk/license) 
*/

(function ($, window, document) {
  function crossBrowser(property, value, prefix) {
    function ucase(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }

    var vendor = ['webkit', 'moz', 'ms', 'o'],
        properties = {};

    for (var i = 0; i < vendor.length; i++) {
      if (prefix) {
        value = value.replace(prefix, '-' + vendor[i] + '-' + prefix);
      }

      properties[ucase(vendor[i]) + ucase(property)] = value;
    }

    properties[property] = value;
    return properties;
  }

  function smooveIt(direction) {
    var height = $(window).height(),
        width = $(window).width();

    for (var i = 0; i < $.fn.smoove.items.length; i++) {
      var $item = $.fn.smoove.items[i],
          params = $item.params;

      if (!$item.hasClass('smooved')) {
        var offset = !direction || direction === 'down' && $item.css('opacity') === '1' ? 0 : params.offset,
            itemtop = $(window).scrollTop() + height - $item.offset().top;

        if (typeof offset === 'string' && offset.indexOf('%')) {
          offset = parseInt(offset) / 100 * height;
        }

        if (itemtop < offset || direction == 'first') {
          if (!isNaN(params.opacity)) {
            $item.css({
              opacity: params.opacity
            });
          }

          var transforms = [],
              properties = ['move', 'move3D', 'moveX', 'moveY', 'moveZ', 'rotate', 'rotate3d', 'rotateX', 'rotateY', 'rotateZ', 'scale', 'scale3d', 'scaleX', 'scaleY', 'skew', 'skewX', 'skewY'];

          for (var p = 0; p < properties.length; p++) {
            if (typeof params[properties[p]] !== "undefined") {
              transforms[properties[p]] = params[properties[p]];
            }
          }

          var transform = '';

          for (var t in transforms) {
            transform += t.replace('move', 'translate') + '(' + transforms[t] + ') ';
          }

          if (transform) {
            $item.css(crossBrowser('transform', transform));
            $item.parent().css(crossBrowser('perspective', params.perspective));

            if (params.transformOrigin) {
              $item.css(crossBrowser('transformOrigin', params.transformOrigin));
            }
          }

          if (typeof params.delay !== "undefined" && params.delay > 0) {
            $item.css('transition-delay', parseInt(params.delay) + 'ms');
          }

          $item.addClass('first_smooved');
        } else {
          if (!$item.hasClass('first_smooved') === true) {
            $item.stop().css('opacity', 1).css(crossBrowser('transform', 'translate(0px, 0px)')).css('transform', '');
            $item.addClass('smooved');
            $item.parent().addClass('smooved');
          } else {
            jQuery('body').addClass('has-smoove');

            if (itemtop < offset || jQuery(window).height() >= jQuery(document).height()) {
              $item.stop().queue(function (next) {
                jQuery(this).stop().css('opacity', 1).css(crossBrowser('transform', 'translate(0px, 0px)')).css('transform', '');
                $item.addClass('smooved');
                $item.parent().addClass('smooved');
                $item.removeClass('first_smooved');
                next();
              });
            } else {
              $item.stop().delay(1000).queue(function (next) {
                window.scrollTo(window.scrollX, window.scrollY + 1);
              });
              $item.removeClass('first_smooved');
            }
          }
        }
      }
    }
  }

  function throttle(fn, threshhold, scope) {
    threshhold = threshhold || 250;
    var last, deferTimer;
    return function () {
      var context = scope || this,
          now = +new Date(),
          args = arguments;

      if (last && now < last + threshhold) {
        clearTimeout(deferTimer);
        deferTimer = setTimeout(function () {
          last = now;
          fn.apply(context, args);
        }, threshhold);
      } else {
        last = now;
        fn.apply(context, args);
      }
    };
  }

  $.fn.smoove = function (options) {
    $.fn.smoove.init(this, $.extend({}, $.fn.smoove.defaults, options));
    return this;
  };

  $.fn.smoove.items = [];
  $.fn.smoove.loaded = false;
  $.fn.smoove.defaults = {
    offset: '50%',
    opacity: 0,
    delay: '0ms',
    duration: '500ms',
    transition: "",
    transformStyle: 'preserve-3d',
    transformOrigin: false,
    perspective: 1000,
    min_width: 768,
    min_height: false
  };

  $.fn.smoove.init = function (items, settings) {
    items.each(function () {
      var $item = $(this),
          params = $item.params = $.extend({}, settings, $item.data());
      $item.data('top', $item.offset().top);
      params.transition = crossBrowser('transition', params.transition, 'transform');
      $item.css(params.transition);
      $.fn.smoove.items.push($item);
    });

    if (!$.fn.smoove.loaded) {
      $.fn.smoove.loaded = true;
      var oldScroll = 0,
          oldHeight = $(window).height(),
          oldWidth = $(window).width(),
          oldDocHeight = $(document).height(),
          resizing;

      if ($('body').width() === $(window).width()) {
        $('body').addClass('smoove-overflow');
      }

      if ($('body').hasClass('elementor-editor-active')) {
        $('iframe#elementor-preview-iframe').ready(function () {
          smooveIt('first');
          $(window).on('scroll', throttle(function () {
            var scrolltop = $(window).scrollTop(),
                direction = scrolltop < oldScroll ? direction = 'up' : 'down';
            oldScroll = scrolltop;
            smooveIt(direction);
          }, 250));
          window.scrollTo(window.scrollX, window.scrollY + 1);
        });
      } else {
        jQuery(document).ready(function () {
          smooveIt('down');
          $(window).on('scroll', throttle(function () {
            var scrolltop = $(window).scrollTop(),
                direction = scrolltop < oldScroll ? direction = 'up' : 'down';
            oldScroll = scrolltop;
            smooveIt(direction);
          }, 250));

          if (jQuery(window).height() >= jQuery(document).height()) {
            smooveIt('down');
          }

          var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

          if (isFirefox) {
            window.scrollTo(window.scrollX, window.scrollY + 1);
          }

          var isTouch = ('ontouchstart' in document.documentElement);

          if (isTouch) {
            window.scrollTo(window.scrollX, window.scrollY + 1);
          }
        });
      }
    }
  };
})(jQuery, window, document);

;

(function (f) {
  if (( false ? undefined : _typeof2(exports)) === "object" && typeof module !== "undefined") {
    module.exports = f();
  } else if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (f),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else { var g; }
})(function () {
  var define, module, exports;
  return function e(t, n, r) {
    function s(o, u) {
      if (!n[o]) {
        if (!t[o]) {
          var a = typeof require == "function" && require;
          if (!u && a) return require(o, !0);
          if (i) return i(o, !0);
          var f = new Error("Cannot find module '" + o + "'");
          throw f.code = "MODULE_NOT_FOUND", f;
        }

        var l = n[o] = {
          exports: {}
        };
        t[o][0].call(l.exports, function (e) {
          var n = t[o][1][e];
          return s(n ? n : e);
        }, l, l.exports, e, t, n, r);
      }

      return n[o].exports;
    }

    var i = typeof require == "function" && require;

    for (var o = 0; o < r.length; o++) {
      s(r[o]);
    }

    return s;
  }({
    1: [function (require, module, exports) {
      'use strict';

      var getOwnPropertySymbols = Object.getOwnPropertySymbols;
      var hasOwnProperty = Object.prototype.hasOwnProperty;
      var propIsEnumerable = Object.prototype.propertyIsEnumerable;

      function toObject(val) {
        if (val === null || val === undefined) {
          throw new TypeError('Object.assign cannot be called with null or undefined');
        }

        return Object(val);
      }

      function shouldUseNative() {
        try {
          if (!Object.assign) {
            return false;
          }

          var test1 = new String('abc');
          test1[5] = 'de';

          if (Object.getOwnPropertyNames(test1)[0] === '5') {
            return false;
          }

          var test2 = {};

          for (var i = 0; i < 10; i++) {
            test2['_' + String.fromCharCode(i)] = i;
          }

          var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
            return test2[n];
          });

          if (order2.join('') !== '0123456789') {
            return false;
          }

          var test3 = {};
          'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
            test3[letter] = letter;
          });

          if (Object.keys(Object.assign({}, test3)).join('') !== 'abcdefghijklmnopqrst') {
            return false;
          }

          return true;
        } catch (err) {
          return false;
        }
      }

      module.exports = shouldUseNative() ? Object.assign : function (target, source) {
        var from;
        var to = toObject(target);
        var symbols;

        for (var s = 1; s < arguments.length; s++) {
          from = Object(arguments[s]);

          for (var key in from) {
            if (hasOwnProperty.call(from, key)) {
              to[key] = from[key];
            }
          }

          if (getOwnPropertySymbols) {
            symbols = getOwnPropertySymbols(from);

            for (var i = 0; i < symbols.length; i++) {
              if (propIsEnumerable.call(from, symbols[i])) {
                to[symbols[i]] = from[symbols[i]];
              }
            }
          }
        }

        return to;
      };
    }, {}],
    2: [function (require, module, exports) {
      (function (process) {
        (function () {
          var getNanoSeconds, hrtime, loadTime, moduleLoadTime, nodeLoadTime, upTime;

          if (typeof performance !== "undefined" && performance !== null && performance.now) {
            module.exports = function () {
              return performance.now();
            };
          } else if (typeof process !== "undefined" && process !== null && process.hrtime) {
            module.exports = function () {
              return (getNanoSeconds() - nodeLoadTime) / 1e6;
            };

            hrtime = process.hrtime;

            getNanoSeconds = function getNanoSeconds() {
              var hr;
              hr = hrtime();
              return hr[0] * 1e9 + hr[1];
            };

            moduleLoadTime = getNanoSeconds();
            upTime = process.uptime() * 1e9;
            nodeLoadTime = moduleLoadTime - upTime;
          } else if (Date.now) {
            module.exports = function () {
              return Date.now() - loadTime;
            };

            loadTime = Date.now();
          } else {
            module.exports = function () {
              return new Date().getTime() - loadTime;
            };

            loadTime = new Date().getTime();
          }
        }).call(this);
      }).call(this, require('_process'));
    }, {
      "_process": 3
    }],
    3: [function (require, module, exports) {
      var process = module.exports = {};
      var cachedSetTimeout;
      var cachedClearTimeout;

      function defaultSetTimout() {
        throw new Error('setTimeout has not been defined');
      }

      function defaultClearTimeout() {
        throw new Error('clearTimeout has not been defined');
      }

      (function () {
        try {
          if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
          } else {
            cachedSetTimeout = defaultSetTimout;
          }
        } catch (e) {
          cachedSetTimeout = defaultSetTimout;
        }

        try {
          if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
          } else {
            cachedClearTimeout = defaultClearTimeout;
          }
        } catch (e) {
          cachedClearTimeout = defaultClearTimeout;
        }
      })();

      function runTimeout(fun) {
        if (cachedSetTimeout === setTimeout) {
          return setTimeout(fun, 0);
        }

        if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
          cachedSetTimeout = setTimeout;
          return setTimeout(fun, 0);
        }

        try {
          return cachedSetTimeout(fun, 0);
        } catch (e) {
          try {
            return cachedSetTimeout.call(null, fun, 0);
          } catch (e) {
            return cachedSetTimeout.call(this, fun, 0);
          }
        }
      }

      function runClearTimeout(marker) {
        if (cachedClearTimeout === clearTimeout) {
          return clearTimeout(marker);
        }

        if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
          cachedClearTimeout = clearTimeout;
          return clearTimeout(marker);
        }

        try {
          return cachedClearTimeout(marker);
        } catch (e) {
          try {
            return cachedClearTimeout.call(null, marker);
          } catch (e) {
            return cachedClearTimeout.call(this, marker);
          }
        }
      }

      var queue = [];
      var draining = false;
      var currentQueue;
      var queueIndex = -1;

      function cleanUpNextTick() {
        if (!draining || !currentQueue) {
          return;
        }

        draining = false;

        if (currentQueue.length) {
          queue = currentQueue.concat(queue);
        } else {
          queueIndex = -1;
        }

        if (queue.length) {
          drainQueue();
        }
      }

      function drainQueue() {
        if (draining) {
          return;
        }

        var timeout = runTimeout(cleanUpNextTick);
        draining = true;
        var len = queue.length;

        while (len) {
          currentQueue = queue;
          queue = [];

          while (++queueIndex < len) {
            if (currentQueue) {
              currentQueue[queueIndex].run();
            }
          }

          queueIndex = -1;
          len = queue.length;
        }

        currentQueue = null;
        draining = false;
        runClearTimeout(timeout);
      }

      process.nextTick = function (fun) {
        var args = new Array(arguments.length - 1);

        if (arguments.length > 1) {
          for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
          }
        }

        queue.push(new Item(fun, args));

        if (queue.length === 1 && !draining) {
          runTimeout(drainQueue);
        }
      };

      function Item(fun, array) {
        this.fun = fun;
        this.array = array;
      }

      Item.prototype.run = function () {
        this.fun.apply(null, this.array);
      };

      process.title = 'browser';
      process.browser = true;
      process.env = {};
      process.argv = [];
      process.version = '';
      process.versions = {};

      function noop() {}

      process.on = noop;
      process.addListener = noop;
      process.once = noop;
      process.off = noop;
      process.removeListener = noop;
      process.removeAllListeners = noop;
      process.emit = noop;
      process.prependListener = noop;
      process.prependOnceListener = noop;

      process.listeners = function (name) {
        return [];
      };

      process.binding = function (name) {
        throw new Error('process.binding is not supported');
      };

      process.cwd = function () {
        return '/';
      };

      process.chdir = function (dir) {
        throw new Error('process.chdir is not supported');
      };

      process.umask = function () {
        return 0;
      };
    }, {}],
    4: [function (require, module, exports) {
      (function (global) {
        var now = require('performance-now'),
            root = typeof window === 'undefined' ? global : window,
            vendors = ['moz', 'webkit'],
            suffix = 'AnimationFrame',
            raf = root['request' + suffix],
            caf = root['cancel' + suffix] || root['cancelRequest' + suffix];

        for (var i = 0; !raf && i < vendors.length; i++) {
          raf = root[vendors[i] + 'Request' + suffix];
          caf = root[vendors[i] + 'Cancel' + suffix] || root[vendors[i] + 'CancelRequest' + suffix];
        }

        if (!raf || !caf) {
          var last = 0,
              id = 0,
              queue = [],
              frameDuration = 1000 / 60;

          raf = function raf(callback) {
            if (queue.length === 0) {
              var _now = now(),
                  next = Math.max(0, frameDuration - (_now - last));

              last = next + _now;
              setTimeout(function () {
                var cp = queue.slice(0);
                queue.length = 0;

                for (var i = 0; i < cp.length; i++) {
                  if (!cp[i].cancelled) {
                    try {
                      cp[i].callback(last);
                    } catch (e) {
                      setTimeout(function () {
                        throw e;
                      }, 0);
                    }
                  }
                }
              }, Math.round(next));
            }

            queue.push({
              handle: ++id,
              callback: callback,
              cancelled: false
            });
            return id;
          };

          caf = function caf(handle) {
            for (var i = 0; i < queue.length; i++) {
              if (queue[i].handle === handle) {
                queue[i].cancelled = true;
              }
            }
          };
        }

        module.exports = function (fn) {
          return raf.call(root, fn);
        };

        module.exports.cancel = function () {
          caf.apply(root, arguments);
        };

        module.exports.polyfill = function () {
          root.requestAnimationFrame = raf;
          root.cancelAnimationFrame = caf;
        };
      }).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
    }, {
      "performance-now": 2
    }],
    5: [function (require, module, exports) {
      'use strict';

      var _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      var rqAnFr = require('raf');

      var objectAssign = require('object-assign');

      var helpers = {
        propertyCache: {},
        vendors: [null, ['-webkit-', 'webkit'], ['-moz-', 'Moz'], ['-o-', 'O'], ['-ms-', 'ms']],
        clamp: function clamp(value, min, max) {
          return min < max ? value < min ? min : value > max ? max : value : value < max ? max : value > min ? min : value;
        },
        data: function data(element, name) {
          return helpers.deserialize(element.getAttribute('data-' + name));
        },
        deserialize: function deserialize(value) {
          if (value === 'true') {
            return true;
          } else if (value === 'false') {
            return false;
          } else if (value === 'null') {
            return null;
          } else if (!isNaN(parseFloat(value)) && isFinite(value)) {
            return parseFloat(value);
          } else {
            return value;
          }
        },
        camelCase: function camelCase(value) {
          return value.replace(/-+(.)?/g, function (match, character) {
            return character ? character.toUpperCase() : '';
          });
        },
        accelerate: function accelerate(element) {
          helpers.css(element, 'transform', 'translate3d(0,0,0) rotate(0.0001deg)');
          helpers.css(element, 'transform-style', 'preserve-3d');
          helpers.css(element, 'backface-visibility', 'hidden');
        },
        transformSupport: function transformSupport(value) {
          var element = document.createElement('div'),
              propertySupport = false,
              propertyValue = null,
              featureSupport = false,
              cssProperty = null,
              jsProperty = null;

          for (var i = 0, l = helpers.vendors.length; i < l; i++) {
            if (helpers.vendors[i] !== null) {
              cssProperty = helpers.vendors[i][0] + 'transform';
              jsProperty = helpers.vendors[i][1] + 'Transform';
            } else {
              cssProperty = 'transform';
              jsProperty = 'transform';
            }

            if (element.style[jsProperty] !== undefined) {
              propertySupport = true;
              break;
            }
          }

          switch (value) {
            case '2D':
              featureSupport = propertySupport;
              break;

            case '3D':
              if (propertySupport) {
                var body = document.body || document.createElement('body'),
                    documentElement = document.documentElement,
                    documentOverflow = documentElement.style.overflow,
                    isCreatedBody = false;

                if (!document.body) {
                  isCreatedBody = true;
                  documentElement.style.overflow = 'hidden';
                  documentElement.appendChild(body);
                  body.style.overflow = 'hidden';
                  body.style.background = '';
                }

                body.appendChild(element);
                element.style[jsProperty] = 'translate3d(1px,1px,1px)';
                propertyValue = window.getComputedStyle(element).getPropertyValue(cssProperty);
                featureSupport = propertyValue !== undefined && propertyValue.length > 0 && propertyValue !== 'none';
                documentElement.style.overflow = documentOverflow;
                body.removeChild(element);

                if (isCreatedBody) {
                  body.removeAttribute('style');
                  body.parentNode.removeChild(body);
                }
              }

              break;
          }

          return featureSupport;
        },
        css: function css(element, property, value) {
          var jsProperty = helpers.propertyCache[property];

          if (!jsProperty) {
            for (var i = 0, l = helpers.vendors.length; i < l; i++) {
              if (helpers.vendors[i] !== null) {
                jsProperty = helpers.camelCase(helpers.vendors[i][1] + '-' + property);
              } else {
                jsProperty = property;
              }

              if (element.style[jsProperty] !== undefined) {
                helpers.propertyCache[property] = jsProperty;
                break;
              }
            }
          }

          element.style[jsProperty] = value;
        }
      };
      var MAGIC_NUMBER = 30,
          DEFAULTS = {
        relativeInput: false,
        clipRelativeInput: false,
        inputElement: null,
        hoverOnly: false,
        calibrationThreshold: 100,
        calibrationDelay: 500,
        supportDelay: 500,
        calibrateX: false,
        calibrateY: true,
        invertX: true,
        invertY: true,
        limitX: false,
        limitY: false,
        scalarX: 10.0,
        scalarY: 10.0,
        frictionX: 0.1,
        frictionY: 0.1,
        originX: 0.5,
        originY: 0.5,
        pointerEvents: false,
        precision: 1,
        onReady: null,
        selector: null
      };

      var Parallax = function () {
        function Parallax(element, options) {
          _classCallCheck(this, Parallax);

          this.element = element;
          var data = {
            calibrateX: helpers.data(this.element, 'calibrate-x'),
            calibrateY: helpers.data(this.element, 'calibrate-y'),
            invertX: helpers.data(this.element, 'invert-x'),
            invertY: helpers.data(this.element, 'invert-y'),
            limitX: helpers.data(this.element, 'limit-x'),
            limitY: helpers.data(this.element, 'limit-y'),
            scalarX: helpers.data(this.element, 'scalar-x'),
            scalarY: helpers.data(this.element, 'scalar-y'),
            frictionX: helpers.data(this.element, 'friction-x'),
            frictionY: helpers.data(this.element, 'friction-y'),
            originX: helpers.data(this.element, 'origin-x'),
            originY: helpers.data(this.element, 'origin-y'),
            pointerEvents: helpers.data(this.element, 'pointer-events'),
            precision: helpers.data(this.element, 'precision'),
            relativeInput: helpers.data(this.element, 'relative-input'),
            clipRelativeInput: helpers.data(this.element, 'clip-relative-input'),
            hoverOnly: helpers.data(this.element, 'hover-only'),
            inputElement: document.querySelector(helpers.data(this.element, 'input-element')),
            selector: helpers.data(this.element, 'selector')
          };

          for (var key in data) {
            if (data[key] === null) {
              delete data[key];
            }
          }

          objectAssign(this, DEFAULTS, data, options);

          if (!this.inputElement) {
            this.inputElement = this.element;
          }

          this.calibrationTimer = null;
          this.calibrationFlag = true;
          this.enabled = false;
          this.depthsX = [];
          this.depthsY = [];
          this.raf = null;
          this.bounds = null;
          this.elementPositionX = 0;
          this.elementPositionY = 0;
          this.elementWidth = 0;
          this.elementHeight = 0;
          this.elementCenterX = 0;
          this.elementCenterY = 0;
          this.elementRangeX = 0;
          this.elementRangeY = 0;
          this.calibrationX = 0;
          this.calibrationY = 0;
          this.inputX = 0;
          this.inputY = 0;
          this.motionX = 0;
          this.motionY = 0;
          this.velocityX = 0;
          this.velocityY = 0;
          this.onMouseMove = this.onMouseMove.bind(this);
          this.onDeviceOrientation = this.onDeviceOrientation.bind(this);
          this.onDeviceMotion = this.onDeviceMotion.bind(this);
          this.onOrientationTimer = this.onOrientationTimer.bind(this);
          this.onMotionTimer = this.onMotionTimer.bind(this);
          this.onCalibrationTimer = this.onCalibrationTimer.bind(this);
          this.onAnimationFrame = this.onAnimationFrame.bind(this);
          this.onWindowResize = this.onWindowResize.bind(this);
          this.windowWidth = null;
          this.windowHeight = null;
          this.windowCenterX = null;
          this.windowCenterY = null;
          this.windowRadiusX = null;
          this.windowRadiusY = null;
          this.portrait = false;
          this.desktop = !navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|BB10|mobi|tablet|opera mini|nexus 7)/i);
          this.motionSupport = !!window.DeviceMotionEvent && !this.desktop;
          this.orientationSupport = !!window.DeviceOrientationEvent && !this.desktop;
          this.orientationStatus = 0;
          this.motionStatus = 0;
          this.initialise();
        }

        _createClass(Parallax, [{
          key: 'initialise',
          value: function initialise() {
            if (this.transform2DSupport === undefined) {
              this.transform2DSupport = helpers.transformSupport('2D');
              this.transform3DSupport = helpers.transformSupport('3D');
            }

            if (this.transform3DSupport) {
              helpers.accelerate(this.element);
            }

            var style = window.getComputedStyle(this.element);

            if (style.getPropertyValue('position') === 'static') {
              this.element.style.position = 'relative';
            }

            if (!this.pointerEvents) {
              this.element.style.pointerEvents = 'none';
            }

            this.updateLayers();
            this.updateDimensions();
            this.enable();
            this.queueCalibration(this.calibrationDelay);
          }
        }, {
          key: 'doReadyCallback',
          value: function doReadyCallback() {
            if (this.onReady) {
              this.onReady();
            }
          }
        }, {
          key: 'updateLayers',
          value: function updateLayers() {
            if (this.selector) {
              this.layers = this.element.querySelectorAll(this.selector);
            } else {
              this.layers = this.element.children;
            }

            if (!this.layers.length) {
              console.warn('ParallaxJS: Your scene does not have any layers.');
            }

            this.depthsX = [];
            this.depthsY = [];

            for (var index = 0; index < this.layers.length; index++) {
              var layer = this.layers[index];

              if (this.transform3DSupport) {
                helpers.accelerate(layer);
              }

              layer.style.position = index ? 'absolute' : 'relative';
              layer.style.display = 'block';
              layer.style.left = 0;
              layer.style.top = 0;
              var depth = helpers.data(layer, 'depth') || 0;
              this.depthsX.push(helpers.data(layer, 'depth-x') || depth);
              this.depthsY.push(helpers.data(layer, 'depth-y') || depth);
            }
          }
        }, {
          key: 'updateDimensions',
          value: function updateDimensions() {
            this.windowWidth = window.innerWidth;
            this.windowHeight = window.innerHeight;
            this.windowCenterX = this.windowWidth * this.originX;
            this.windowCenterY = this.windowHeight * this.originY;
            this.windowRadiusX = Math.max(this.windowCenterX, this.windowWidth - this.windowCenterX);
            this.windowRadiusY = Math.max(this.windowCenterY, this.windowHeight - this.windowCenterY);
          }
        }, {
          key: 'updateBounds',
          value: function updateBounds() {
            this.bounds = this.inputElement.getBoundingClientRect();
            this.elementPositionX = this.bounds.left;
            this.elementPositionY = this.bounds.top;
            this.elementWidth = this.bounds.width;
            this.elementHeight = this.bounds.height;
            this.elementCenterX = this.elementWidth * this.originX;
            this.elementCenterY = this.elementHeight * this.originY;
            this.elementRangeX = Math.max(this.elementCenterX, this.elementWidth - this.elementCenterX);
            this.elementRangeY = Math.max(this.elementCenterY, this.elementHeight - this.elementCenterY);
          }
        }, {
          key: 'queueCalibration',
          value: function queueCalibration(delay) {
            clearTimeout(this.calibrationTimer);
            this.calibrationTimer = setTimeout(this.onCalibrationTimer, delay);
          }
        }, {
          key: 'enable',
          value: function enable() {
            if (this.enabled) {
              return;
            }

            this.enabled = true;

            if (this.orientationSupport) {
              this.portrait = false;
              window.addEventListener('deviceorientation', this.onDeviceOrientation);
              this.detectionTimer = setTimeout(this.onOrientationTimer, this.supportDelay);
            } else if (this.motionSupport) {
              this.portrait = false;
              window.addEventListener('devicemotion', this.onDeviceMotion);
              this.detectionTimer = setTimeout(this.onMotionTimer, this.supportDelay);
            } else {
              this.calibrationX = 0;
              this.calibrationY = 0;
              this.portrait = false;
              window.addEventListener('mousemove', this.onMouseMove);
              this.doReadyCallback();
            }

            window.addEventListener('resize', this.onWindowResize);
            this.raf = rqAnFr(this.onAnimationFrame);
          }
        }, {
          key: 'disable',
          value: function disable() {
            if (!this.enabled) {
              return;
            }

            this.enabled = false;

            if (this.orientationSupport) {
              window.removeEventListener('deviceorientation', this.onDeviceOrientation);
            } else if (this.motionSupport) {
              window.removeEventListener('devicemotion', this.onDeviceMotion);
            } else {
              window.removeEventListener('mousemove', this.onMouseMove);
            }

            window.removeEventListener('resize', this.onWindowResize);
            rqAnFr.cancel(this.raf);
          }
        }, {
          key: 'calibrate',
          value: function calibrate(x, y) {
            this.calibrateX = x === undefined ? this.calibrateX : x;
            this.calibrateY = y === undefined ? this.calibrateY : y;
          }
        }, {
          key: 'invert',
          value: function invert(x, y) {
            this.invertX = x === undefined ? this.invertX : x;
            this.invertY = y === undefined ? this.invertY : y;
          }
        }, {
          key: 'friction',
          value: function friction(x, y) {
            this.frictionX = x === undefined ? this.frictionX : x;
            this.frictionY = y === undefined ? this.frictionY : y;
          }
        }, {
          key: 'scalar',
          value: function scalar(x, y) {
            this.scalarX = x === undefined ? this.scalarX : x;
            this.scalarY = y === undefined ? this.scalarY : y;
          }
        }, {
          key: 'limit',
          value: function limit(x, y) {
            this.limitX = x === undefined ? this.limitX : x;
            this.limitY = y === undefined ? this.limitY : y;
          }
        }, {
          key: 'origin',
          value: function origin(x, y) {
            this.originX = x === undefined ? this.originX : x;
            this.originY = y === undefined ? this.originY : y;
          }
        }, {
          key: 'setInputElement',
          value: function setInputElement(element) {
            this.inputElement = element;
            this.updateDimensions();
          }
        }, {
          key: 'setPosition',
          value: function setPosition(element, x, y) {
            x = x.toFixed(this.precision) + 'px';
            y = y.toFixed(this.precision) + 'px';

            if (this.transform3DSupport) {
              helpers.css(element, 'transform', 'translate3d(' + x + ',' + y + ',0)');
            } else if (this.transform2DSupport) {
              helpers.css(element, 'transform', 'translate(' + x + ',' + y + ')');
            } else {
              element.style.left = x;
              element.style.top = y;
            }
          }
        }, {
          key: 'onOrientationTimer',
          value: function onOrientationTimer() {
            if (this.orientationSupport && this.orientationStatus === 0) {
              this.disable();
              this.orientationSupport = false;
              this.enable();
            } else {
              this.doReadyCallback();
            }
          }
        }, {
          key: 'onMotionTimer',
          value: function onMotionTimer() {
            if (this.motionSupport && this.motionStatus === 0) {
              this.disable();
              this.motionSupport = false;
              this.enable();
            } else {
              this.doReadyCallback();
            }
          }
        }, {
          key: 'onCalibrationTimer',
          value: function onCalibrationTimer() {
            this.calibrationFlag = true;
          }
        }, {
          key: 'onWindowResize',
          value: function onWindowResize() {
            this.updateDimensions();
          }
        }, {
          key: 'onAnimationFrame',
          value: function onAnimationFrame() {
            this.updateBounds();
            var calibratedInputX = this.inputX - this.calibrationX,
                calibratedInputY = this.inputY - this.calibrationY;

            if (Math.abs(calibratedInputX) > this.calibrationThreshold || Math.abs(calibratedInputY) > this.calibrationThreshold) {
              this.queueCalibration(0);
            }

            if (this.portrait) {
              this.motionX = this.calibrateX ? calibratedInputY : this.inputY;
              this.motionY = this.calibrateY ? calibratedInputX : this.inputX;
            } else {
              this.motionX = this.calibrateX ? calibratedInputX : this.inputX;
              this.motionY = this.calibrateY ? calibratedInputY : this.inputY;
            }

            this.motionX *= this.elementWidth * (this.scalarX / 100);
            this.motionY *= this.elementHeight * (this.scalarY / 100);

            if (!isNaN(parseFloat(this.limitX))) {
              this.motionX = helpers.clamp(this.motionX, -this.limitX, this.limitX);
            }

            if (!isNaN(parseFloat(this.limitY))) {
              this.motionY = helpers.clamp(this.motionY, -this.limitY, this.limitY);
            }

            this.velocityX += (this.motionX - this.velocityX) * this.frictionX;
            this.velocityY += (this.motionY - this.velocityY) * this.frictionY;

            for (var index = 0; index < this.layers.length; index++) {
              var layer = this.layers[index],
                  depthX = this.depthsX[index],
                  depthY = this.depthsY[index],
                  xOffset = this.velocityX * (depthX * (this.invertX ? -1 : 1)),
                  yOffset = this.velocityY * (depthY * (this.invertY ? -1 : 1));
              this.setPosition(layer, xOffset, yOffset);
            }

            this.raf = rqAnFr(this.onAnimationFrame);
          }
        }, {
          key: 'rotate',
          value: function rotate(beta, gamma) {
            var x = (beta || 0) / MAGIC_NUMBER,
                y = (gamma || 0) / MAGIC_NUMBER;
            var portrait = this.windowHeight > this.windowWidth;

            if (this.portrait !== portrait) {
              this.portrait = portrait;
              this.calibrationFlag = true;
            }

            if (this.calibrationFlag) {
              this.calibrationFlag = false;
              this.calibrationX = x;
              this.calibrationY = y;
            }

            this.inputX = x;
            this.inputY = y;
          }
        }, {
          key: 'onDeviceOrientation',
          value: function onDeviceOrientation(event) {
            var beta = event.beta;
            var gamma = event.gamma;

            if (beta !== null && gamma !== null) {
              this.orientationStatus = 1;
              this.rotate(beta, gamma);
            }
          }
        }, {
          key: 'onDeviceMotion',
          value: function onDeviceMotion(event) {
            var beta = event.rotationRate.beta;
            var gamma = event.rotationRate.gamma;

            if (beta !== null && gamma !== null) {
              this.motionStatus = 1;
              this.rotate(beta, gamma);
            }
          }
        }, {
          key: 'onMouseMove',
          value: function onMouseMove(event) {
            var clientX = event.clientX,
                clientY = event.clientY;

            if (this.hoverOnly && (clientX < this.elementPositionX || clientX > this.elementPositionX + this.elementWidth || clientY < this.elementPositionY || clientY > this.elementPositionY + this.elementHeight)) {
              this.inputX = 0;
              this.inputY = 0;
              return;
            }

            if (this.relativeInput) {
              if (this.clipRelativeInput) {
                clientX = Math.max(clientX, this.elementPositionX);
                clientX = Math.min(clientX, this.elementPositionX + this.elementWidth);
                clientY = Math.max(clientY, this.elementPositionY);
                clientY = Math.min(clientY, this.elementPositionY + this.elementHeight);
              }

              if (this.elementRangeX && this.elementRangeY) {
                this.inputX = (clientX - this.elementPositionX - this.elementCenterX) / this.elementRangeX;
                this.inputY = (clientY - this.elementPositionY - this.elementCenterY) / this.elementRangeY;
              }
            } else {
              if (this.windowRadiusX && this.windowRadiusY) {
                this.inputX = (clientX - this.windowCenterX) / this.windowRadiusX;
                this.inputY = (clientY - this.windowCenterY) / this.windowRadiusY;
              }
            }
          }
        }, {
          key: 'destroy',
          value: function destroy() {
            this.disable();
            clearTimeout(this.calibrationTimer);
            clearTimeout(this.detectionTimer);
            this.element.removeAttribute('style');

            for (var index = 0; index < this.layers.length; index++) {
              this.layers[index].removeAttribute('style');
            }

            delete this.element;
            delete this.layers;
          }
        }, {
          key: 'version',
          value: function version() {
            return '3.1.0';
          }
        }]);

        return Parallax;
      }();

      module.exports = Parallax;
    }, {
      "object-assign": 1,
      "raf": 4
    }]
  }, {}, [5])(5);
});

;
/*! Blast.js (2.0.0): julian.com/research/blast (C) 2015 Julian Shapiro. MIT @license: en.wikipedia.org/wiki/MIT_License */

;

(function ($, window, document, undefined) {
  var IE = function () {
    if (document.documentMode) {
      return document.documentMode;
    } else {
      for (var i = 7; i > 0; i--) {
        var div = document.createElement("div");
        div.innerHTML = "<!--[if IE " + i + "]><span></span><![endif]-->";

        if (div.getElementsByTagName("span").length) {
          div = null;
          return i;
        }

        div = null;
      }
    }

    return undefined;
  }();

  var console = window.console || {
    log: function log() {},
    time: function time() {}
  };
  var NAME = "blast",
      characterRanges = {
    latinPunctuation: "–—′’'“″„\"(«.…¡¿′’'”″“\")».…!?",
    latinLetters: "\\u0041-\\u005A\\u0061-\\u007A\\u00C0-\\u017F\\u0100-\\u01FF\\u0180-\\u027F"
  },
      Reg = {
    abbreviations: new RegExp("[^" + characterRanges.latinLetters + "](e\\.g\\.)|(i\\.e\\.)|(mr\\.)|(mrs\\.)|(ms\\.)|(dr\\.)|(prof\\.)|(esq\\.)|(sr\\.)|(jr\\.)[^" + characterRanges.latinLetters + "]", "ig"),
    innerWordPeriod: new RegExp("[" + characterRanges.latinLetters + "]\.[" + characterRanges.latinLetters + "]", "ig"),
    onlyContainsPunctuation: new RegExp("[^" + characterRanges.latinPunctuation + "]"),
    adjoinedPunctuation: new RegExp("^[" + characterRanges.latinPunctuation + "]+|[" + characterRanges.latinPunctuation + "]+$", "g"),
    skippedElements: /(script|style|select|textarea)/i,
    hasPluginClass: new RegExp("(^| )" + NAME + "( |$)", "gi")
  };

  $.fn[NAME] = function (options) {
    function encodePunctuation(text) {
      return text.replace(Reg.abbreviations, function (match) {
        return match.replace(/\./g, "{{46}}");
      }).replace(Reg.innerWordPeriod, function (match) {
        return match.replace(/\./g, "{{46}}");
      });
    }

    function decodePunctuation(text) {
      return text.replace(/{{(\d{1,3})}}/g, function (fullMatch, subMatch) {
        return String.fromCharCode(subMatch);
      });
    }

    function wrapNode(node, opts) {
      var wrapper = document.createElement(opts.tag);
      wrapper.className = NAME;

      if (opts.customClass) {
        wrapper.className += " " + opts.customClass;

        if (opts.generateIndexID) {
          wrapper.id = opts.customClass + "-" + Element.blastedIndex;
        }
      }

      if (opts.delimiter === "all" && /\s/.test(node.data)) {
        wrapper.style.whiteSpace = "pre-line";
      }

      if (opts.generateValueClass === true && !opts.search && (opts.delimiter === "character" || opts.delimiter === "word")) {
        var valueClass,
            text = node.data;

        if (opts.delimiter === "word" && Reg.onlyContainsPunctuation.test(text)) {
          text = text.replace(Reg.adjoinedPunctuation, "");
        }

        valueClass = NAME + "-" + opts.delimiter.toLowerCase() + "-" + text.toLowerCase();
        wrapper.className += " " + valueClass;
      }

      if (opts.aria) {
        wrapper.setAttribute("aria-hidden", "true");
      }

      wrapper.appendChild(node.cloneNode(false));
      return wrapper;
    }

    function traverseDOM(node, opts) {
      var matchPosition = -1,
          skipNodeBit = 0;

      if (node.nodeType === 3 && node.data.length) {
        if (Element.nodeBeginning) {
          node.data = !opts.search && opts.delimiter === "sentence" ? encodePunctuation(node.data) : decodePunctuation(node.data);
          Element.nodeBeginning = false;
        }

        matchPosition = node.data.search(delimiterRegex);

        if (matchPosition !== -1) {
          var match = node.data.match(delimiterRegex),
              matchText = match[0],
              subMatchText = match[1] || false;

          if (matchText === "") {
            matchPosition++;
          } else if (subMatchText && subMatchText !== matchText) {
            matchPosition += matchText.indexOf(subMatchText);
            matchText = subMatchText;
          }

          var middleBit = node.splitText(matchPosition);
          middleBit.splitText(matchText.length);
          skipNodeBit = 1;

          if (!opts.search && opts.delimiter === "sentence") {
            middleBit.data = decodePunctuation(middleBit.data);
          }

          var wrappedNode = wrapNode(middleBit, opts, Element.blastedIndex);
          middleBit.parentNode.replaceChild(wrappedNode, middleBit);
          Element.wrappers.push(wrappedNode);
          Element.blastedIndex++;
        }
      } else if (node.nodeType === 1 && node.hasChildNodes() && !Reg.skippedElements.test(node.tagName) && !Reg.hasPluginClass.test(node.className)) {
        for (var i = 0; i < node.childNodes.length; i++) {
          Element.nodeBeginning = true;
          i += traverseDOM(node.childNodes[i], opts);
        }
      }

      return skipNodeBit;
    }

    var opts = $.extend({}, $.fn[NAME].defaults, options),
        delimiterRegex,
        Element = {};

    if (opts.search.length && (typeof opts.search === "string" || /^\d/.test(parseFloat(opts.search)))) {
      opts.delimiter = opts.search.toString().replace(/[-[\]{,}(.)*+?|^$\\\/]/g, "\\$&");
      delimiterRegex = new RegExp("(?:^|[^-" + characterRanges.latinLetters + "])(" + opts.delimiter + "('s)?)(?![-" + characterRanges.latinLetters + "])", "i");
    } else {
      if (typeof opts.delimiter === "string") {
        opts.delimiter = opts.delimiter.toLowerCase();
      }

      switch (opts.delimiter) {
        case "all":
          delimiterRegex = /(.)/;
          break;

        case "letter":
        case "char":
        case "character":
          delimiterRegex = /(\S)/;
          break;

        case "word":
          delimiterRegex = /\s*(\S+)\s*/;
          break;

        case "sentence":
          delimiterRegex = /(?=\S)(([.]{2,})?[^!?]+?([.…!?]+|(?=\s+$)|$)(\s*[′’'”″“")»]+)*)/;
          break;

        case "element":
          delimiterRegex = /(?=\S)([\S\s]*\S)/;
          break;

        default:
          if (opts.delimiter instanceof RegExp) {
            delimiterRegex = opts.delimiter;
          } else {
            console.log(NAME + ": Unrecognized delimiter, empty search string, or invalid custom Regex. Aborting.");
            return true;
          }

      }
    }

    this.each(function () {
      var $this = $(this),
          text = $this.text();

      if (options !== false) {
        Element = {
          blastedIndex: 0,
          nodeBeginning: false,
          wrappers: Element.wrappers || []
        };

        if ($this.data(NAME) !== undefined && ($this.data(NAME) !== "search" || opts.search === false)) {
          reverse($this, opts);
          if (opts.debug) console.log(NAME + ": Removed element's existing Blast call.");
        }

        $this.data(NAME, opts.search !== false ? "search" : opts.delimiter);

        if (opts.aria) {
          $this.attr("aria-label", text);
        }

        if (opts.stripHTMLTags) {
          $this.html(text);
        }

        try {
          document.createElement(opts.tag);
        } catch (error) {
          opts.tag = "span";
          if (opts.debug) console.log(NAME + ": Invalid tag supplied. Defaulting to span.");
        }

        $this.addClass(NAME + "-root");
        if (opts.debug) console.time(NAME);
        traverseDOM(this, opts);
        if (opts.debug) console.timeEnd(NAME);
      } else if (options === false && $this.data(NAME) !== undefined) {
        reverse($this, opts);
      }

      if (opts.debug) {
        $.each(Element.wrappers, function (index, element) {
          console.log(NAME + " [" + opts.delimiter + "] " + this.outerHTML);
          this.style.backgroundColor = index % 2 ? "#f12185" : "#075d9a";
        });
      }
    });

    function reverse($this, opts) {
      if (opts.debug) console.time("blast reversal");
      var skippedDescendantRoot = false;
      $this.removeClass(NAME + "-root").removeAttr("aria-label").find("." + NAME).each(function () {
        var $this = $(this);

        if (!$this.closest("." + NAME + "-root").length) {
          var thisParentNode = this.parentNode;
          if (IE <= 7) thisParentNode.firstChild.nodeName;
          thisParentNode.replaceChild(this.firstChild, this);
          thisParentNode.normalize();
        } else {
          skippedDescendantRoot = true;
        }
      });

      if (window.Zepto) {
        $this.data(NAME, undefined);
      } else {
        $this.removeData(NAME);
      }

      if (opts.debug) {
        console.log(NAME + ": Reversed Blast" + ($this.attr("id") ? " on #" + $this.attr("id") + "." : ".") + (skippedDescendantRoot ? " Skipped reversal on the children of one or more descendant root elements." : ""));
        console.timeEnd("blast reversal");
      }
    }

    if (options !== false && opts.returnGenerated === true) {
      var newStack = $().add(Element.wrappers);
      newStack.prevObject = this;
      newStack.context = this.context;
      return newStack;
    } else {
      return this;
    }
  };

  $.fn.blast.defaults = {
    returnGenerated: true,
    delimiter: "word",
    tag: "span",
    search: false,
    customClass: "",
    generateIndexID: false,
    generateValueClass: false,
    stripHTMLTags: false,
    aria: true,
    debug: false
  };
})(window.jQuery || window.Zepto, window, document);

;
/*!
 * Name    : Just Another Parallax [Jarallax]
 * Version : 1.12.5
 * Author  : nK <https://nkdev.info>
 * GitHub  : https://github.com/nk-o/jarallax
 */

(function (modules) {
  var installedModules = {};

  function __webpack_require__(moduleId) {
    if (installedModules[moduleId]) {
      return installedModules[moduleId].exports;
    }

    var module = installedModules[moduleId] = {
      i: moduleId,
      l: false,
      exports: {}
    };
    modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
    module.l = true;
    return module.exports;
  }

  __webpack_require__.m = modules;
  __webpack_require__.c = installedModules;

  __webpack_require__.d = function (exports, name, getter) {
    if (!__webpack_require__.o(exports, name)) {
      Object.defineProperty(exports, name, {
        enumerable: true,
        get: getter
      });
    }
  };

  __webpack_require__.r = function (exports) {
    if (typeof Symbol !== 'undefined' && Symbol.toStringTag) {
      Object.defineProperty(exports, Symbol.toStringTag, {
        value: 'Module'
      });
    }

    Object.defineProperty(exports, '__esModule', {
      value: true
    });
  };

  __webpack_require__.t = function (value, mode) {
    if (mode & 1) value = __webpack_require__(value);
    if (mode & 8) return value;
    if (mode & 4 && _typeof2(value) === 'object' && value && value.__esModule) return value;
    var ns = Object.create(null);

    __webpack_require__.r(ns);

    Object.defineProperty(ns, 'default', {
      enumerable: true,
      value: value
    });
    if (mode & 2 && typeof value != 'string') for (var key in value) {
      __webpack_require__.d(ns, key, function (key) {
        return value[key];
      }.bind(null, key));
    }
    return ns;
  };

  __webpack_require__.n = function (module) {
    var getter = module && module.__esModule ? function getDefault() {
      return module['default'];
    } : function getModuleExports() {
      return module;
    };

    __webpack_require__.d(getter, 'a', getter);

    return getter;
  };

  __webpack_require__.o = function (object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  };

  __webpack_require__.p = "";
  return __webpack_require__(__webpack_require__.s = 10);
})([,, function (module, exports) {
  module.exports = function (callback) {
    if (document.readyState === 'complete' || document.readyState === 'interactive') {
      callback.call();
    } else if (document.attachEvent) {
      document.attachEvent('onreadystatechange', function () {
        if (document.readyState === 'interactive') callback.call();
      });
    } else if (document.addEventListener) {
      document.addEventListener('DOMContentLoaded', callback);
    }
  };
}, function (module, exports, __webpack_require__) {
  (function (global) {
    var win;

    if (typeof window !== "undefined") {
      win = window;
    } else if (typeof global !== "undefined") {
      win = global;
    } else if (typeof self !== "undefined") {
      win = self;
    } else {
      win = {};
    }

    module.exports = win;
  }).call(this, __webpack_require__(4));
}, function (module, exports) {
  function _typeof(obj) {
    "@babel/helpers - typeof";

    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function _typeof(obj) {
        return typeof obj;
      };
    } else {
      _typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  var g;

  g = function () {
    return this;
  }();

  try {
    g = g || new Function("return this")();
  } catch (e) {
    if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
  }

  module.exports = g;
},,,,,, function (module, exports, __webpack_require__) {
  module.exports = __webpack_require__(11);
}, function (module, __webpack_exports__, __webpack_require__) {
  "use strict";

  __webpack_require__.r(__webpack_exports__);

  var lite_ready__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);

  var lite_ready__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(lite_ready__WEBPACK_IMPORTED_MODULE_0__);

  var global__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);

  var global__WEBPACK_IMPORTED_MODULE_1___default = __webpack_require__.n(global__WEBPACK_IMPORTED_MODULE_1__);

  var _jarallax_esm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(12);

  function _typeof(obj) {
    "@babel/helpers - typeof";

    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function _typeof(obj) {
        return typeof obj;
      };
    } else {
      _typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  var oldPlugin = global__WEBPACK_IMPORTED_MODULE_1__["window"].jarallax;
  global__WEBPACK_IMPORTED_MODULE_1__["window"].jarallax = _jarallax_esm__WEBPACK_IMPORTED_MODULE_2__["default"];

  global__WEBPACK_IMPORTED_MODULE_1__["window"].jarallax.noConflict = function () {
    global__WEBPACK_IMPORTED_MODULE_1__["window"].jarallax = oldPlugin;
    return this;
  };

  if ('undefined' !== typeof global__WEBPACK_IMPORTED_MODULE_1__["jQuery"]) {
    var jQueryPlugin = function jQueryPlugin() {
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      Array.prototype.unshift.call(args, this);

      var res = _jarallax_esm__WEBPACK_IMPORTED_MODULE_2__["default"].apply(global__WEBPACK_IMPORTED_MODULE_1__["window"], args);

      return 'object' !== _typeof(res) ? res : this;
    };

    jQueryPlugin.constructor = _jarallax_esm__WEBPACK_IMPORTED_MODULE_2__["default"].constructor;
    var oldJqPlugin = global__WEBPACK_IMPORTED_MODULE_1__["jQuery"].fn.jarallax;
    global__WEBPACK_IMPORTED_MODULE_1__["jQuery"].fn.jarallax = jQueryPlugin;

    global__WEBPACK_IMPORTED_MODULE_1__["jQuery"].fn.jarallax.noConflict = function () {
      global__WEBPACK_IMPORTED_MODULE_1__["jQuery"].fn.jarallax = oldJqPlugin;
      return this;
    };
  }

  lite_ready__WEBPACK_IMPORTED_MODULE_0___default()(function () {
    Object(_jarallax_esm__WEBPACK_IMPORTED_MODULE_2__["default"])(document.querySelectorAll('[data-jarallax]'));
  });
}, function (module, __webpack_exports__, __webpack_require__) {
  "use strict";

  __webpack_require__.r(__webpack_exports__);

  var lite_ready__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);

  var lite_ready__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(lite_ready__WEBPACK_IMPORTED_MODULE_0__);

  var global__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);

  var global__WEBPACK_IMPORTED_MODULE_1___default = __webpack_require__.n(global__WEBPACK_IMPORTED_MODULE_1__);

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
  }

  function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;

    for (var i = 0, arr2 = new Array(len); i < len; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }

  function _iterableToArrayLimit(arr, i) {
    if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _typeof(obj) {
    "@babel/helpers - typeof";

    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function _typeof(obj) {
        return typeof obj;
      };
    } else {
      _typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  var navigator = global__WEBPACK_IMPORTED_MODULE_1__["window"].navigator;
  var isIE = -1 < navigator.userAgent.indexOf('MSIE ') || -1 < navigator.userAgent.indexOf('Trident/') || -1 < navigator.userAgent.indexOf('Edge/');
  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

  var supportTransform = function () {
    var prefixes = 'transform WebkitTransform MozTransform'.split(' ');
    var div = document.createElement('div');

    for (var i = 0; i < prefixes.length; i += 1) {
      if (div && div.style[prefixes[i]] !== undefined) {
        return prefixes[i];
      }
    }

    return false;
  }();

  var $deviceHelper;

  function getDeviceHeight() {
    if (!$deviceHelper && document.body) {
      $deviceHelper = document.createElement('div');
      $deviceHelper.style.cssText = 'position: fixed; top: -9999px; left: 0; height: 100vh; width: 0;';
      document.body.appendChild($deviceHelper);
    }

    return ($deviceHelper ? $deviceHelper.clientHeight : 0) || global__WEBPACK_IMPORTED_MODULE_1__["window"].innerHeight || document.documentElement.clientHeight;
  }

  var wndH;

  function updateWndVars() {
    if (isMobile) {
      wndH = getDeviceHeight();
    } else {
      wndH = global__WEBPACK_IMPORTED_MODULE_1__["window"].innerHeight || document.documentElement.clientHeight;
    }
  }

  updateWndVars();
  global__WEBPACK_IMPORTED_MODULE_1__["window"].addEventListener('resize', updateWndVars);
  global__WEBPACK_IMPORTED_MODULE_1__["window"].addEventListener('orientationchange', updateWndVars);
  global__WEBPACK_IMPORTED_MODULE_1__["window"].addEventListener('load', updateWndVars);
  lite_ready__WEBPACK_IMPORTED_MODULE_0___default()(function () {
    updateWndVars({
      type: 'dom-loaded'
    });
  });
  var jarallaxList = [];

  function getParents(elem) {
    var parents = [];

    while (null !== elem.parentElement) {
      elem = elem.parentElement;

      if (1 === elem.nodeType) {
        parents.push(elem);
      }
    }

    return parents;
  }

  function updateParallax() {
    if (!jarallaxList.length) {
      return;
    }

    jarallaxList.forEach(function (data, k) {
      var instance = data.instance,
          oldData = data.oldData;
      var clientRect = instance.$item.getBoundingClientRect();
      var newData = {
        width: clientRect.width,
        height: clientRect.height,
        top: clientRect.top,
        bottom: clientRect.bottom,
        wndW: global__WEBPACK_IMPORTED_MODULE_1__["window"].innerWidth,
        wndH: wndH
      };
      var isResized = !oldData || oldData.wndW !== newData.wndW || oldData.wndH !== newData.wndH || oldData.width !== newData.width || oldData.height !== newData.height;
      var isScrolled = isResized || !oldData || oldData.top !== newData.top || oldData.bottom !== newData.bottom;
      jarallaxList[k].oldData = newData;

      if (isResized) {
        instance.onResize();
      }

      if (isScrolled) {
        instance.onScroll();
      }
    });
    global__WEBPACK_IMPORTED_MODULE_1__["window"].requestAnimationFrame(updateParallax);
  }

  var instanceID = 0;

  var Jarallax = function () {
    function Jarallax(item, userOptions) {
      _classCallCheck(this, Jarallax);

      var self = this;
      self.instanceID = instanceID;
      instanceID += 1;
      self.$item = item;
      self.defaults = {
        type: 'scroll',
        speed: 0.5,
        imgSrc: null,
        imgElement: '.jarallax-img',
        imgSize: 'cover',
        imgPosition: '50% 50%',
        imgRepeat: 'no-repeat',
        keepImg: false,
        elementInViewport: null,
        zIndex: -100,
        disableParallax: false,
        disableVideo: false,
        videoSrc: null,
        videoStartTime: 0,
        videoEndTime: 0,
        videoVolume: 0,
        videoLoop: true,
        videoPlayOnlyVisible: true,
        videoLazyLoading: true,
        onScroll: null,
        onInit: null,
        onDestroy: null,
        onCoverImage: null
      };
      var dataOptions = self.$item.dataset || {};
      var pureDataOptions = {};
      Object.keys(dataOptions).forEach(function (key) {
        var loweCaseOption = key.substr(0, 1).toLowerCase() + key.substr(1);

        if (loweCaseOption && 'undefined' !== typeof self.defaults[loweCaseOption]) {
          pureDataOptions[loweCaseOption] = dataOptions[key];
        }
      });
      self.options = self.extend({}, self.defaults, pureDataOptions, userOptions);
      self.pureOptions = self.extend({}, self.options);
      Object.keys(self.options).forEach(function (key) {
        if ('true' === self.options[key]) {
          self.options[key] = true;
        } else if ('false' === self.options[key]) {
          self.options[key] = false;
        }
      });
      self.options.speed = Math.min(2, Math.max(-1, parseFloat(self.options.speed)));

      if ('string' === typeof self.options.disableParallax) {
        self.options.disableParallax = new RegExp(self.options.disableParallax);
      }

      if (self.options.disableParallax instanceof RegExp) {
        var disableParallaxRegexp = self.options.disableParallax;

        self.options.disableParallax = function () {
          return disableParallaxRegexp.test(navigator.userAgent);
        };
      }

      if ('function' !== typeof self.options.disableParallax) {
        self.options.disableParallax = function () {
          return false;
        };
      }

      if ('string' === typeof self.options.disableVideo) {
        self.options.disableVideo = new RegExp(self.options.disableVideo);
      }

      if (self.options.disableVideo instanceof RegExp) {
        var disableVideoRegexp = self.options.disableVideo;

        self.options.disableVideo = function () {
          return disableVideoRegexp.test(navigator.userAgent);
        };
      }

      if ('function' !== typeof self.options.disableVideo) {
        self.options.disableVideo = function () {
          return false;
        };
      }

      var elementInVP = self.options.elementInViewport;

      if (elementInVP && 'object' === _typeof(elementInVP) && 'undefined' !== typeof elementInVP.length) {
        var _elementInVP = elementInVP;

        var _elementInVP2 = _slicedToArray(_elementInVP, 1);

        elementInVP = _elementInVP2[0];
      }

      if (!(elementInVP instanceof Element)) {
        elementInVP = null;
      }

      self.options.elementInViewport = elementInVP;
      self.image = {
        src: self.options.imgSrc || null,
        $container: null,
        useImgTag: false,
        position: /iPad|iPhone|iPod|Android/.test(navigator.userAgent) ? 'absolute' : 'fixed'
      };

      if (self.initImg() && self.canInitParallax()) {
        self.init();
      }
    }

    _createClass(Jarallax, [{
      key: "css",
      value: function css(el, styles) {
        if ('string' === typeof styles) {
          return global__WEBPACK_IMPORTED_MODULE_1__["window"].getComputedStyle(el).getPropertyValue(styles);
        }

        if (styles.transform && supportTransform) {
          styles[supportTransform] = styles.transform;
        }

        Object.keys(styles).forEach(function (key) {
          el.style[key] = styles[key];
        });
        return el;
      }
    }, {
      key: "extend",
      value: function extend(out) {
        for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          args[_key - 1] = arguments[_key];
        }

        out = out || {};
        Object.keys(args).forEach(function (i) {
          if (!args[i]) {
            return;
          }

          Object.keys(args[i]).forEach(function (key) {
            out[key] = args[i][key];
          });
        });
        return out;
      }
    }, {
      key: "getWindowData",
      value: function getWindowData() {
        return {
          width: global__WEBPACK_IMPORTED_MODULE_1__["window"].innerWidth || document.documentElement.clientWidth,
          height: wndH,
          y: document.documentElement.scrollTop
        };
      }
    }, {
      key: "initImg",
      value: function initImg() {
        var self = this;
        var $imgElement = self.options.imgElement;

        if ($imgElement && 'string' === typeof $imgElement) {
          $imgElement = self.$item.querySelector($imgElement);
        }

        if (!($imgElement instanceof Element)) {
          if (self.options.imgSrc) {
            $imgElement = new Image();
            $imgElement.src = self.options.imgSrc;
          } else {
            $imgElement = null;
          }
        }

        if ($imgElement) {
          if (self.options.keepImg) {
            self.image.$item = $imgElement.cloneNode(true);
          } else {
            self.image.$item = $imgElement;
            self.image.$itemParent = $imgElement.parentNode;
          }

          self.image.useImgTag = true;
        }

        if (self.image.$item) {
          return true;
        }

        if (null === self.image.src) {
          self.image.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
          self.image.bgImage = self.css(self.$item, 'background-image');
        }

        return !(!self.image.bgImage || 'none' === self.image.bgImage);
      }
    }, {
      key: "canInitParallax",
      value: function canInitParallax() {
        return supportTransform && !this.options.disableParallax();
      }
    }, {
      key: "init",
      value: function init() {
        var self = this;
        var containerStyles = {
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          overflow: 'hidden'
        };
        var imageStyles = {
          pointerEvents: 'none',
          transformStyle: 'preserve-3d',
          backfaceVisibility: 'hidden',
          willChange: 'transform,opacity'
        };

        if (!self.options.keepImg) {
          var curStyle = self.$item.getAttribute('style');

          if (curStyle) {
            self.$item.setAttribute('data-jarallax-original-styles', curStyle);
          }

          if (self.image.useImgTag) {
            var curImgStyle = self.image.$item.getAttribute('style');

            if (curImgStyle) {
              self.image.$item.setAttribute('data-jarallax-original-styles', curImgStyle);
            }
          }
        }

        if ('static' === self.css(self.$item, 'position')) {
          self.css(self.$item, {
            position: 'relative'
          });
        }

        if ('auto' === self.css(self.$item, 'z-index')) {
          self.css(self.$item, {
            zIndex: 0
          });
        }

        self.image.$container = document.createElement('div');
        self.css(self.image.$container, containerStyles);
        self.css(self.image.$container, {
          'z-index': self.options.zIndex
        });

        if (isIE) {
          self.css(self.image.$container, {
            opacity: 0.9999
          });
        }

        self.image.$container.setAttribute('id', "jarallax-container-".concat(self.instanceID));
        self.$item.appendChild(self.image.$container);

        if (self.image.useImgTag) {
          imageStyles = self.extend({
            'object-fit': self.options.imgSize,
            'object-position': self.options.imgPosition,
            'font-family': "object-fit: ".concat(self.options.imgSize, "; object-position: ").concat(self.options.imgPosition, ";"),
            'max-width': 'none'
          }, containerStyles, imageStyles);
        } else {
          self.image.$item = document.createElement('div');

          if (self.image.src) {
            imageStyles = self.extend({
              'background-position': self.options.imgPosition,
              'background-size': self.options.imgSize,
              'background-repeat': self.options.imgRepeat,
              'background-image': self.image.bgImage || "url(\"".concat(self.image.src, "\")")
            }, containerStyles, imageStyles);
          }
        }

        if ('opacity' === self.options.type || 'scale' === self.options.type || 'scale-opacity' === self.options.type || 1 === self.options.speed) {
          self.image.position = 'absolute';
        }

        if ('fixed' === self.image.position) {
          var $parents = getParents(self.$item).filter(function (el) {
            var styles = global__WEBPACK_IMPORTED_MODULE_1__["window"].getComputedStyle(el);
            var parentTransform = styles['-webkit-transform'] || styles['-moz-transform'] || styles.transform;
            var overflowRegex = /(auto|scroll)/;
            return parentTransform && 'none' !== parentTransform || overflowRegex.test(styles.overflow + styles['overflow-y'] + styles['overflow-x']);
          });
          self.image.position = $parents.length ? 'absolute' : 'fixed';
        }

        imageStyles.position = self.image.position;
        self.css(self.image.$item, imageStyles);
        self.image.$container.appendChild(self.image.$item);
        self.onResize();
        self.onScroll(true);

        if (self.options.onInit) {
          self.options.onInit.call(self);
        }

        if ('none' !== self.css(self.$item, 'background-image')) {
          self.css(self.$item, {
            'background-image': 'none'
          });
        }

        self.addToParallaxList();
      }
    }, {
      key: "addToParallaxList",
      value: function addToParallaxList() {
        jarallaxList.push({
          instance: this
        });

        if (1 === jarallaxList.length) {
          global__WEBPACK_IMPORTED_MODULE_1__["window"].requestAnimationFrame(updateParallax);
        }
      }
    }, {
      key: "removeFromParallaxList",
      value: function removeFromParallaxList() {
        var self = this;
        jarallaxList.forEach(function (data, key) {
          if (data.instance.instanceID === self.instanceID) {
            jarallaxList.splice(key, 1);
          }
        });
      }
    }, {
      key: "destroy",
      value: function destroy() {
        var self = this;
        self.removeFromParallaxList();
        var originalStylesTag = self.$item.getAttribute('data-jarallax-original-styles');
        self.$item.removeAttribute('data-jarallax-original-styles');

        if (!originalStylesTag) {
          self.$item.removeAttribute('style');
        } else {
          self.$item.setAttribute('style', originalStylesTag);
        }

        if (self.image.useImgTag) {
          var originalStylesImgTag = self.image.$item.getAttribute('data-jarallax-original-styles');
          self.image.$item.removeAttribute('data-jarallax-original-styles');

          if (!originalStylesImgTag) {
            self.image.$item.removeAttribute('style');
          } else {
            self.image.$item.setAttribute('style', originalStylesTag);
          }

          if (self.image.$itemParent) {
            self.image.$itemParent.appendChild(self.image.$item);
          }
        }

        if (self.$clipStyles) {
          self.$clipStyles.parentNode.removeChild(self.$clipStyles);
        }

        if (self.image.$container) {
          self.image.$container.parentNode.removeChild(self.image.$container);
        }

        if (self.options.onDestroy) {
          self.options.onDestroy.call(self);
        }

        delete self.$item.jarallax;
      }
    }, {
      key: "clipContainer",
      value: function clipContainer() {
        if ('fixed' !== this.image.position) {
          return;
        }

        var self = this;
        var rect = self.image.$container.getBoundingClientRect();
        var width = rect.width,
            height = rect.height;

        if (!self.$clipStyles) {
          self.$clipStyles = document.createElement('style');
          self.$clipStyles.setAttribute('type', 'text/css');
          self.$clipStyles.setAttribute('id', "jarallax-clip-".concat(self.instanceID));
          var head = document.head || document.getElementsByTagName('head')[0];
          head.appendChild(self.$clipStyles);
        }

        var styles = "#jarallax-container-".concat(self.instanceID, " {\n            clip: rect(0 ").concat(width, "px ").concat(height, "px 0);\n            clip: rect(0, ").concat(width, "px, ").concat(height, "px, 0);\n            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);\n            clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);\n        }");

        if (self.$clipStyles.styleSheet) {
          self.$clipStyles.styleSheet.cssText = styles;
        } else {
          self.$clipStyles.innerHTML = styles;
        }
      }
    }, {
      key: "coverImage",
      value: function coverImage() {
        var self = this;
        var rect = self.image.$container.getBoundingClientRect();
        var contH = rect.height;
        var speed = self.options.speed;
        var isScroll = 'scroll' === self.options.type || 'scroll-opacity' === self.options.type;
        var scrollDist = 0;
        var resultH = contH;
        var resultMT = 0;

        if (isScroll) {
          if (0 > speed) {
            scrollDist = speed * Math.max(contH, wndH);

            if (wndH < contH) {
              scrollDist -= speed * (contH - wndH);
            }
          } else {
            scrollDist = speed * (contH + wndH);
          }

          if (1 < speed) {
            resultH = Math.abs(scrollDist - wndH);
          } else if (0 > speed) {
            resultH = scrollDist / speed + Math.abs(scrollDist);
          } else {
            resultH += (wndH - contH) * (1 - speed);
          }

          scrollDist /= 2;
        }

        self.parallaxScrollDistance = scrollDist;

        if (isScroll) {
          resultMT = (wndH - resultH) / 2;
        } else {
          resultMT = (contH - resultH) / 2;
        }

        self.css(self.image.$item, {
          height: "".concat(resultH, "px"),
          marginTop: "".concat(resultMT, "px"),
          left: 'fixed' === self.image.position ? "".concat(rect.left, "px") : '0',
          width: "".concat(rect.width, "px")
        });

        if (self.options.onCoverImage) {
          self.options.onCoverImage.call(self);
        }

        return {
          image: {
            height: resultH,
            marginTop: resultMT
          },
          container: rect
        };
      }
    }, {
      key: "isVisible",
      value: function isVisible() {
        return this.isElementInViewport || false;
      }
    }, {
      key: "onScroll",
      value: function onScroll(force) {
        var self = this;
        var rect = self.$item.getBoundingClientRect();
        var contT = rect.top;
        var contH = rect.height;
        var styles = {};
        var viewportRect = rect;

        if (self.options.elementInViewport) {
          viewportRect = self.options.elementInViewport.getBoundingClientRect();
        }

        self.isElementInViewport = 0 <= viewportRect.bottom && 0 <= viewportRect.right && viewportRect.top <= wndH && viewportRect.left <= global__WEBPACK_IMPORTED_MODULE_1__["window"].innerWidth;

        if (force ? false : !self.isElementInViewport) {
          return;
        }

        var beforeTop = Math.max(0, contT);
        var beforeTopEnd = Math.max(0, contH + contT);
        var afterTop = Math.max(0, -contT);
        var beforeBottom = Math.max(0, contT + contH - wndH);
        var beforeBottomEnd = Math.max(0, contH - (contT + contH - wndH));
        var afterBottom = Math.max(0, -contT + wndH - contH);
        var fromViewportCenter = 1 - 2 * ((wndH - contT) / (wndH + contH));
        var visiblePercent = 1;

        if (contH < wndH) {
          visiblePercent = 1 - (afterTop || beforeBottom) / contH;
        } else if (beforeTopEnd <= wndH) {
          visiblePercent = beforeTopEnd / wndH;
        } else if (beforeBottomEnd <= wndH) {
          visiblePercent = beforeBottomEnd / wndH;
        }

        if ('opacity' === self.options.type || 'scale-opacity' === self.options.type || 'scroll-opacity' === self.options.type) {
          styles.transform = 'translate3d(0,0,0)';
          styles.opacity = visiblePercent;
        }

        if ('scale' === self.options.type || 'scale-opacity' === self.options.type) {
          var scale = 1;

          if (0 > self.options.speed) {
            scale -= self.options.speed * visiblePercent;
          } else {
            scale += self.options.speed * (1 - visiblePercent);
          }

          styles.transform = "scale(".concat(scale, ") translate3d(0,0,0)");
        }

        if ('scroll' === self.options.type || 'scroll-opacity' === self.options.type) {
          var positionY = self.parallaxScrollDistance * fromViewportCenter;

          if ('absolute' === self.image.position) {
            positionY -= contT;
          }

          styles.transform = "translate3d(0,".concat(positionY, "px,0)");
        }

        self.css(self.image.$item, styles);

        if (self.options.onScroll) {
          self.options.onScroll.call(self, {
            section: rect,
            beforeTop: beforeTop,
            beforeTopEnd: beforeTopEnd,
            afterTop: afterTop,
            beforeBottom: beforeBottom,
            beforeBottomEnd: beforeBottomEnd,
            afterBottom: afterBottom,
            visiblePercent: visiblePercent,
            fromViewportCenter: fromViewportCenter
          });
        }
      }
    }, {
      key: "onResize",
      value: function onResize() {
        this.coverImage();
        this.clipContainer();
      }
    }]);

    return Jarallax;
  }();

  var plugin = function plugin(items, options) {
    if ('object' === (typeof HTMLElement === "undefined" ? "undefined" : _typeof(HTMLElement)) ? items instanceof HTMLElement : items && 'object' === _typeof(items) && null !== items && 1 === items.nodeType && 'string' === typeof items.nodeName) {
      items = [items];
    }

    var len = items.length;
    var k = 0;
    var ret;

    for (var _len2 = arguments.length, args = new Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
      args[_key2 - 2] = arguments[_key2];
    }

    for (k; k < len; k += 1) {
      if ('object' === _typeof(options) || 'undefined' === typeof options) {
        if (!items[k].jarallax) {
          items[k].jarallax = new Jarallax(items[k], options);
        }
      } else if (items[k].jarallax) {
        ret = items[k].jarallax[options].apply(items[k].jarallax, args);
      }

      if ('undefined' !== typeof ret) {
        return ret;
      }
    }

    return items;
  };

  plugin.constructor = Jarallax;
  __webpack_exports__["default"] = plugin;
}]);

;
/*
Sticky-kit v1.1.2 | WTFPL | Leaf Corcoran 2015 | http://leafo.net
*/

(function () {
  var b, f;
  b = this.jQuery || window.jQuery;
  f = b(window);

  b.fn.stick_in_parent = function (d) {
    var A, w, J, n, B, K, p, q, k, E, t;
    null == d && (d = {});
    t = d.sticky_class;
    B = d.inner_scrolling;
    E = d.recalc_every;
    k = d.parent;
    q = d.offset_top;
    p = d.spacer;
    w = d.bottoming;
    null == q && (q = 0);
    null == k && (k = void 0);
    null == B && (B = !0);
    null == t && (t = "is_stuck");
    A = b(document);
    null == w && (w = !0);

    J = function J(a, d, n, C, F, u, r, G) {
      var v, _H, m, D, I, c, g, x, y, z, h, l;

      if (!a.data("sticky_kit")) {
        a.data("sticky_kit", !0);
        I = A.height();
        g = a.parent();
        null != k && (g = g.closest(k));
        if (!g.length) throw "failed to find stick parent";
        v = m = !1;
        (h = null != p ? p && a.closest(p) : b("<div />")) && h.css("position", a.css("position"));

        x = function x() {
          var c, f, e;
          if (!G && (I = A.height(), c = parseInt(g.css("border-top-width"), 10), f = parseInt(g.css("padding-top"), 10), d = parseInt(g.css("padding-bottom"), 10), n = g.offset().top + c + f, C = g.height(), m && (v = m = !1, null == p && (a.insertAfter(h), h.detach()), a.css({
            position: "",
            top: "",
            width: "",
            bottom: ""
          }).removeClass(t), e = !0), F = a.offset().top - (parseInt(a.css("margin-top"), 10) || 0) - q, u = a.outerHeight(!0), r = a.css("float"), h && h.css({
            width: a.outerWidth(!0),
            height: u,
            display: a.css("display"),
            "vertical-align": a.css("vertical-align"),
            "float": r
          }), e)) return l();
        };

        x();
        if (u !== C) return D = void 0, c = q, z = E, l = function l() {
          var b, l, e, k;
          if (!G && (e = !1, null != z && (--z, 0 >= z && (z = E, x(), e = !0)), e || A.height() === I || x(), e = f.scrollTop(), null != D && (l = e - D), D = e, m ? (w && (k = e + u + c > C + n, v && !k && (v = !1, a.css({
            position: "fixed",
            bottom: "",
            top: c
          }).trigger("sticky_kit:unbottom"))), e < F && (m = !1, c = q, null == p && ("left" !== r && "right" !== r || a.insertAfter(h), h.detach()), b = {
            position: "",
            width: "",
            top: ""
          }, a.css(b).removeClass(t).trigger("sticky_kit:unstick")), B && (b = f.height(), u + q > b && !v && (c -= l, c = Math.max(b - u, c), c = Math.min(q, c), m && a.css({
            top: c + "px"
          })))) : e > F && (m = !0, b = {
            position: "fixed",
            top: c
          }, b.width = "border-box" === a.css("box-sizing") ? a.outerWidth() + "px" : a.width() + "px", a.css(b).addClass(t), null == p && (a.after(h), "left" !== r && "right" !== r || h.append(a)), a.trigger("sticky_kit:stick")), m && w && (null == k && (k = e + u + c > C + n), !v && k))) return v = !0, "static" === g.css("position") && g.css({
            position: "relative"
          }), a.css({
            position: "absolute",
            bottom: d,
            top: "auto"
          }).trigger("sticky_kit:bottom");
        }, y = function y() {
          x();
          return l();
        }, _H = function H() {
          G = !0;
          f.off("touchmove", l);
          f.off("scroll", l);
          f.off("resize", y);
          b(document.body).off("sticky_kit:recalc", y);
          a.off("sticky_kit:detach", _H);
          a.removeData("sticky_kit");
          a.css({
            position: "",
            bottom: "",
            top: "",
            width: ""
          });
          g.position("position", "");
          if (m) return null == p && ("left" !== r && "right" !== r || a.insertAfter(h), h.remove()), a.removeClass(t);
        }, f.on("touchmove", l), f.on("scroll", l), f.on("resize", y), b(document.body).on("sticky_kit:recalc", y), a.on("sticky_kit:detach", _H), setTimeout(l, 0);
      }
    };

    n = 0;

    for (K = this.length; n < K; n++) {
      d = this[n], J(b(d));
    }

    return this;
  };
}).call(this);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvZnJvbnRlbmQvMzFjZDEuanMiXSwibmFtZXMiOlsid2luZG93IiwidW5kZWZpbmVkIiwiJCIsImpRdWVyeSIsIlplcHRvIiwibGF6eUluc3RhbmNlSWQiLCJ3aW5kb3dMb2FkZWQiLCJmbiIsIkxhenkiLCJsYXp5Iiwic2V0dGluZ3MiLCJMYXp5UGx1Z2luIiwibmFtZXMiLCJlbGVtZW50cyIsImxvYWRlciIsImlzRnVuY3Rpb24iLCJpc0FycmF5IiwiY29uZmlnIiwicHJvdG90eXBlIiwiZm9yY2VkIiwiX2YiLCJpIiwibCIsImxlbmd0aCIsImMiLCJhIiwiX2V4ZWN1dGVMYXp5IiwiaW5zdGFuY2UiLCJpdGVtcyIsImV2ZW50cyIsIm5hbWVzcGFjZSIsIl9hd2FpdGluZ0FmdGVyTG9hZCIsIl9hY3R1YWxXaWR0aCIsIl9hY3R1YWxIZWlnaHQiLCJfaXNSZXRpbmFEaXNwbGF5IiwiX2FmdGVyTG9hZCIsIl9sb2FkIiwiX2Vycm9yIiwiX2ltZyIsIl9zcmMiLCJfc3Jjc2V0IiwiX3NpemVzIiwiX2JhY2tncm91bmRJbWFnZSIsIl9pbml0aWFsaXplIiwiZGV2aWNlUGl4ZWxSYXRpbyIsIl9wcmVwYXJlSXRlbXMiLCJkZWxheSIsInNldFRpbWVvdXQiLCJfbGF6eUxvYWRJdGVtcyIsImNvbWJpbmVkIiwiZSIsIl90aHJvdHRsZSIsInRocm90dGxlIiwiZXZlbnQiLCJ0eXBlIiwiYWxsIiwiYWRkaXRpb25hbEl0ZW1zIiwicHVzaCIsImFwcGx5IiwiZyIsImZpbHRlciIsImRhdGEiLCJsb2FkZWROYW1lIiwiZiIsImZvcmNlZEl0ZW1zIiwiaXRlbSIsImFwcGVuZFNjcm9sbCIsIm9uIiwiZGVmYXVsdEltYWdlIiwicGxhY2Vob2xkZXIiLCJpbWFnZUJhc2UiLCJzcmNzZXRBdHRyaWJ1dGUiLCJsb2FkZXJBdHRyaWJ1dGUiLCJmb3JjZWRUYWdzIiwiZWxlbWVudCIsInRhZyIsIl9nZXRFbGVtZW50VGFnTmFtZSIsImhhbmRsZWROYW1lIiwiYXR0ciIsImF0dHJpYnV0ZSIsIm5hbWUiLCJlbGVtZW50SW1hZ2VCYXNlIiwiaW1hZ2VCYXNlQXR0cmlidXRlIiwiX2dldENvcnJlY3RlZFNyY1NldCIsImNzcyIsImFsbEl0ZW1zIiwiYXV0b0Rlc3Ryb3kiLCJkZXN0cm95IiwibG9hZFRyaWdnZXJlZCIsIl9pc0luTG9hZGFibGVBcmVhIiwiY3VzdG9tTG9hZGVyIiwidmlzaWJsZU9ubHkiLCJpcyIsIl9oYW5kbGVJdGVtIiwiZXJyb3JDYWxsYmFjayIsIl90cmlnZ2VyQ2FsbGJhY2siLCJfcmVkdWNlQXdhaXRpbmciLCJub29wIiwic3JjQXR0cmlidXRlIiwic2l6ZXNBdHRyaWJ1dGUiLCJyZXRpbmFBdHRyaWJ1dGUiLCJyZW1vdmVBdHRyaWJ1dGUiLCJlbGVtZW50UmV0aW5hIiwibG9hZENhbGxiYWNrIiwicmVtb3ZlQXR0ciIsIm9mZiIsIm9uZSIsInJlc3BvbnNlIiwidHJpZ2dlciIsImltYWdlT2JqIiwiSW1hZ2UiLCJoaWRlIiwiZWZmZWN0IiwiZWZmZWN0VGltZSIsInJlbW92ZSIsImltYWdlU3JjIiwiY29tcGxldGUiLCJlbGVtZW50Qm91bmQiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCJkaXJlY3Rpb24iLCJzY3JvbGxEaXJlY3Rpb24iLCJ0aHJlc2hvbGQiLCJ2ZXJ0aWNhbCIsIl9nZXRBY3R1YWxIZWlnaHQiLCJ0b3AiLCJib3R0b20iLCJob3Jpem9udGFsIiwiX2dldEFjdHVhbFdpZHRoIiwibGVmdCIsInJpZ2h0Iiwid2lkdGgiLCJoZWlnaHQiLCJ0YWdOYW1lIiwidG9Mb3dlckNhc2UiLCJzcmNzZXQiLCJlbnRyaWVzIiwic3BsaXQiLCJ0cmltIiwiY2FsbGJhY2siLCJ0aW1lb3V0IiwibGFzdEV4ZWN1dGUiLCJpZ25vcmVUaHJvdHRsZSIsImVsYXBzZWQiLCJEYXRlIiwicnVuIiwiY2FsbCIsImNsZWFyVGltZW91dCIsImVuYWJsZVRocm90dGxlIiwiYXJncyIsInNsaWNlIiwiYXJndW1lbnRzIiwiYmluZCIsIl9pbnN0YW5jZSIsIl9jb25maWciLCJleHRlbmQiLCJfZXZlbnRzIiwiX25hbWVzcGFjZSIsImVudHJ5TmFtZSIsInZhbHVlIiwiYWRkSXRlbXMiLCJnZXRJdGVtcyIsInVwZGF0ZSIsInVzZVRocm90dGxlIiwiZm9yY2UiLCJsb2FkQWxsIiwiY2hhaW5hYmxlIiwiYmVmb3JlTG9hZCIsImFmdGVyTG9hZCIsIm9uRXJyb3IiLCJvbkZpbmlzaGVkQWxsIiwid2luIiwibGFzdFRpbWUiLCJyZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJ3ZWJraXRSZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJjdXJyVGltZSIsImdldFRpbWUiLCJ0aW1lVG9DYWxsIiwiTWF0aCIsIm1heCIsImlkIiwiY2FuY2VsQW5pbWF0aW9uRnJhbWUiLCJyb290IiwiZmFjdG9yeSIsImRlZmluZSIsInV0aWxzIiwiQ29uc29sZSIsImNvbnNvbGUiLCJlcnJvciIsIm1lc3NhZ2UiLCJvcHRpb25zIiwiZGVmYXVsdHMiLCJwcm9wIiwiaGFzT3duUHJvcGVydHkiLCJlbCIsImNyZWF0ZUVsIiwicHJlZml4ZXMiLCJwIiwicGwiLCJwcmVmaXhlZFByb3AiLCJjaGFyQXQiLCJ0b1VwcGVyQ2FzZSIsInN0eWxlIiwiY2xvbmVPYmplY3QiLCJvYmoiLCJjb3B5IiwiY2xhc3NlcyIsImRvY3VtZW50IiwiY3JlYXRlRWxlbWVudCIsImNsYXNzTmFtZSIsImNhbWVsaXplIiwic3RyaW5nIiwicmVwbGFjZSIsImhhbmRsZUV2ZW50cyIsIl90aGlzIiwiaXNCaW5kIiwiZXZlbnRfaGFuZGxlcnMiLCJiaW5kTWV0aG9kIiwiZm9yRWFjaCIsImV2IiwiZGlzcGF0Y2hFdmVudCIsImVtaXRBcmdzIiwiY29uY2F0IiwiZW1pdEV2ZW50IiwiZnVuYyIsInRpbWVzdGFtcCIsImxpbWl0Iiwic2VsZiIsIm5vdyIsIm1vZHVsbyIsImluZGV4IiwiY2xhc3NSZWciLCJSZWdFeHAiLCJoYXNDbGFzcyIsIm1hdGNoIiwiYWRkQ2xhc3MiLCJyZW1vdmVDbGFzcyIsInRyYW5zbGF0ZSIsIngiLCJ5IiwicyIsInNjYWxlIiwiYnJvd3NlciIsInRyYW5zIiwiZ3B1IiwiRXZFbWl0dGVyIiwicHJvdG8iLCJldmVudE5hbWUiLCJsaXN0ZW5lciIsImxpc3RlbmVycyIsImluZGV4T2YiLCJzcGxpY2UiLCJvbmNlTGlzdGVuZXJzIiwiX29uY2VFdmVudHMiLCJpc09uY2UiLCJBbmltYXRlIiwicG9zaXRpb25zIiwiZnJpY3Rpb24iLCJhdHRyYWN0aW9uIiwiZm9yY2VzIiwicmVzZXRBbmltYXRlIiwiT2JqZWN0IiwiY3JlYXRlIiwidXBkYXRlRHJhZyIsIm1vdmUiLCJkcmFnIiwicmVsZWFzZURyYWciLCJhbmltYXRlVG8iLCJzdGFydEFuaW1hdGUiLCJzZXR0bGUiLCJyZXN0aW5nRnJhbWVzIiwiUkFGIiwiYW5pbWF0ZSIsInN0b3BBbmltYXRlIiwic3RhcnQiLCJwb3NpdGlvbiIsInZlbG9jaXR5IiwicmVzdGluZyIsImxvb3AiLCJwcmV2aW91cyIsImFwcGx5RHJhZ0ZvcmNlIiwiYXBwbHlBdHRyYWN0aW9uRm9yY2UiLCJpbnRlZ3JhdGVQaHlzaWNzIiwiZ2V0UmVzdGluZ1Bvc2l0aW9uIiwicmVuZGVyIiwiY2hlY2tTZXR0bGUiLCJrIiwiZ2V0RnJpY3Rpb25GYWN0b3IiLCJkcmFnVmVsb2NpdHkiLCJkcmFnRm9yY2UiLCJhcHBseUZvcmNlIiwiZGlzdGFuY2UiLCJyb3VuZFZhbHVlcyIsInZhbHVlcyIsInJvdW5kIiwiY291bnQiLCJrZXlzIiwiSlNPTiIsInN0cmluZ2lmeSIsInZlcnNpb24iLCJHVUlEIiwiaW5zdGFuY2VzIiwiZXhwYW5kbyIsInJhbmRvbSIsImNhY2hlIiwidWlkIiwibWVkaWFTZWxlY3RvciIsInNsaWRlciIsInNsaWRlIiwidGh1bWJzIiwicmlnaHRUb0xlZnQiLCJwcmVsb2FkIiwidW5sb2FkIiwidGltZVRvSWRsZSIsImhpc3RvcnkiLCJtb3VzZVdoZWVsIiwiY29udGV4dE1lbnUiLCJzY3JvbGxCYXIiLCJmYWRlSWZTZXR0bGUiLCJjb250cm9scyIsInByZXZOZXh0IiwicHJldk5leHRUb3VjaCIsImNvdW50ZXJNZXNzYWdlIiwiY2FwdGlvbiIsImF1dG9DYXB0aW9uIiwiY2FwdGlvblNtYWxsRGV2aWNlIiwidGh1bWJuYWlscyIsInRodW1ibmFpbHNOYXYiLCJ0aHVtYm5haWxTaXplcyIsImd1dHRlciIsInNwYWNpbmciLCJzbWFydFJlc2l6ZSIsIm92ZXJmbG93IiwibG9hZEVycm9yIiwibm9Db250ZW50IiwicHJldk5leHRLZXkiLCJzY3JvbGxUb05hdiIsInNjcm9sbFNlbnNpdGl2aXR5Iiwiem9vbVRvIiwibWluWm9vbSIsIm1heFpvb20iLCJkb3VibGVUYXBUb1pvb20iLCJzY3JvbGxUb1pvb20iLCJwaW5jaFRvWm9vbSIsImVzY2FwZVRvQ2xvc2UiLCJzY3JvbGxUb0Nsb3NlIiwicGluY2hUb0Nsb3NlIiwiZHJhZ1RvQ2xvc2UiLCJ0YXBUb0Nsb3NlIiwic2hhcmVCdXR0b25zIiwic2hhcmVUZXh0Iiwic2hhcmVkVXJsIiwic2xpZGVTaG93SW50ZXJ2YWwiLCJzbGlkZVNob3dBdXRvUGxheSIsInNsaWRlU2hvd0F1dG9TdG9wIiwiY291bnRUaW1lciIsImNvdW50VGltZXJCZyIsImNvdW50VGltZXJDb2xvciIsIm1lZGlhZWxlbWVudCIsInZpZGVvUmF0aW8iLCJ2aWRlb01heFdpZHRoIiwidmlkZW9BdXRvUGxheSIsInZpZGVvVGh1bWJuYWlsIiwiTW9kdWxvQm94IiwicXVlcnlTZWxlY3RvciIsInNldFZhciIsImluaXQiLCJjcmVhdGVET00iLCJET00iLCJob2xkZXIiLCJzZXRBbmltYXRpb24iLCJnZXRHYWxsZXJpZXMiLCJvcGVuRnJvbVF1ZXJ5IiwiZG9jIiwibmF2IiwibmF2aWdhdG9yIiwicHJlIiwiZ2VzdHVyZSIsImJ1dHRvbnMiLCJzbGlkZXMiLCJjZWxscyIsInN0YXRlcyIsInBvaW50ZXJzIiwiZHJhZ0V2ZW50cyIsImRldGVjdFBvaW50ZXJFdmVudHMiLCJ0b3VjaERldmljZSIsIm1heFRvdWNoUG9pbnRzIiwibXNNYXhUb3VjaFBvaW50cyIsInB1c2hTdGF0ZSIsImZ1bGxTY3JlZW4iLCJkZXRlY3RGdWxsU2NyZWVuIiwib25tb3VzZXdoZWVsIiwiaWZyYW1lVmlkZW8iLCJzb2NpYWxNZWRpYSIsInBvaW50ZXJFbmFibGVkIiwiZW5kIiwibXNQb2ludGVyRW5hYmxlZCIsInJlcXVlc3QiLCJjaGFuZ2UiLCJleGl0IiwieW91dHViZSIsInJlZyIsInVybCIsInNoYXJlIiwicG9zdGVyIiwidGh1bWIiLCJwbGF5IiwicGF1c2UiLCJ2aW1lbyIsIm1ldGhvZCIsImRhaWx5bW90aW9uIiwid2lzdGlhIiwiZmFjZWJvb2siLCJnb29nbGVwbHVzIiwidHdpdHRlciIsInBpbnRlcmVzdCIsImxpbmtlZGluIiwicmVkZGl0Iiwic3R1bWJsZXVwb24iLCJ0dW1ibHIiLCJibG9nZ2VyIiwiYnVmZmVyIiwiZGlnZyIsImV2ZXJub3RlIiwiYXBwZW5kRE9NIiwiZG9tIiwib3B0IiwiYXBwZW5kQ2hpbGQiLCJvdmVybGF5IiwidWkiLCJjbG9uZU5vZGUiLCJpdGVtSW5uZXIiLCJjaGlsZHJlbiIsImNyZWF0ZVVJIiwic2V0QXR0cmlidXRlIiwiY29tbWVudCIsImNyZWF0ZUNvbW1lbnQiLCJib2R5IiwidG9wQmFyIiwiY2xpZW50SGVpZ2h0Iiwic2hhcmVJbmRleCIsInNoYXJlVG9vbHRpcCIsInRleHRDb250ZW50IiwiY3JlYXRlQnV0dG9ucyIsInNsaWRlU2hvdyIsImNvdW50ZXIiLCJ0aW1lciIsInJldmVyc2UiLCJib3R0b21CYXIiLCJjYXB0aW9uSW5uZXIiLCJ0aHVtYnNIb2xkZXIiLCJ0aHVtYnNJbm5lciIsImFjdGlvbiIsImdhbGxlcmllcyIsInNlbGVjdG9ycyIsInNvdXJjZXMiLCJxdWVyeVNlbGVjdG9yQWxsIiwic291cmNlIiwibWVkaWEiLCJzcmMiLCJnZXRBdHRyaWJ1dGUiLCJjdXJyZW50U3JjIiwiZ2V0TWVkaWFBdHRzIiwic2V0TWVkaWFUeXBlIiwiZ2V0TWVkaWFUaHVtYiIsImdldFZpZGVvVGh1bWIiLCJnZXRNZWRpYUNhcHRpb24iLCJzZXRNZWRpYUNhcHRpb24iLCJnYWxsZXJ5Iiwic2V0R2FsbGVyeU5hbWUiLCJzZXRHYWxsZXJ5RmVhdHVyZXMiLCJzZXRNZWRpYUV2ZW50IiwiYWRkTWVkaWEiLCJleHRlbnNpb24iLCJzdWJzdHIiLCJsYXN0SW5kZXhPZiIsInRlc3QiLCJnZXRTcmMiLCJmb3JtYXQiLCJzdHJlYW0iLCJyZWdzIiwib2JqZWN0IiwicGFydHMiLCJwYXJzZUZsb2F0IiwidW5pdCIsInNjcmVlbiIsImF1dG8iLCJnZXRBdHRyIiwiaW1nIiwiZmlyc3RFbGVtZW50Q2hpbGQiLCJ0aXRsZSIsImRlc2MiLCJhbHQiLCJ0aHVtYm5haWwiLCJnZXRFbGVtZW50c0J5VGFnTmFtZSIsImhhc1Bvc3RlciIsImhhc1RodW1iIiwidXJpIiwieGhyIiwiWE1MSHR0cFJlcXVlc3QiLCJvbmxvYWQiLCJ0YXJnZXQiLCJyZXNwb25zZVRleHQiLCJwYXJzZSIsInRodW1ibmFpbF9sYXJnZSIsInRodW1ibmFpbF91cmwiLCJiYWNrZ3JvdW5kSW1hZ2UiLCJ0aHVtYm5haWxfc21hbGwiLCJvcGVuIiwiZW5jb2RlVVJJIiwic2VuZCIsIm5leHQiLCJuZXh0RWxlbWVudFNpYmxpbmciLCJpbm5lckhUTUwiLCJnZXRHYWxsZXJ5TmFtZSIsInBhcmVudCIsIm5vZGUiLCJwYXJlbnROb2RlIiwicmVsIiwiem9vbSIsImRvd25sb2FkIiwibW9ieExpc3RlbmVyIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsImFkZEV2ZW50TGlzdGVuZXIiLCJwcmV2ZW50RGVmYXVsdCIsInN0b3BQcm9wYWdhdGlvbiIsImxvYWRlZCIsInJlbW92ZUNvbnRlbnQiLCJ3cmFwQXJvdW5kIiwiaGlkZVNjcm9sbEJhciIsInNldFNsaWRlciIsInNldFRodW1icyIsInNldENhcHRpb24iLCJzZXRNZWRpYSIsInVwZGF0ZU1lZGlhSW5mbyIsInJlcGxhY2VTdGF0ZSIsInNldENvbnRyb2xzIiwiYmluZEV2ZW50cyIsInNob3ciLCJhcHBlbmRWaWRlbyIsInN0YXJ0U2xpZGVTaG93IiwicXVlcnkiLCJnZXRRdWVyeVN0cmluZyIsImxvY2F0aW9uIiwic2VhcmNoIiwiZGVjb2RlVVJJQ29tcG9uZW50IiwiZ3VpZCIsIm1pZCIsImNsb3NlIiwiZXhpdEZ1bGxTY3JlZW4iLCJzdG9wU2xpZGVTaG93IiwicGF1c2VWaWRlbyIsImRpc3BsYXkiLCJpbml0aWFsTGVuZ3RoIiwic2V0UHJldk5leHRCdXR0b25zIiwicHJldiIsImRvY3VtZW50RWxlbWVudCIsInNjcm9sbE1ldGhvZCIsImRpc2FibGVTY3JvbGwiLCJvcGVuZWQiLCJwcm9wZXJ0eU5hbWUiLCJtb3VzZU91dCIsImZyb20iLCJyZWxhdGVkVGFyZ2V0IiwidG9FbGVtZW50Iiwibm9kZU5hbWUiLCJtb3VzZU1vdmUiLCJpZGxlQ2xhc3MiLCJpZGxlIiwicHJldmVudCIsImlzRWwiLCJyZXR1cm5WYWx1ZSIsIm9ud2hlZWwiLCJvbnRvdWNobW92ZSIsIm5vcm1hbGl6ZVdoZWVsIiwiZGVsdGFZIiwiY2VsbCIsImdldENlbGwiLCJtaW4iLCJhYnMiLCJjbGllbnRYIiwiY2xpZW50WSIsImRlbHRhIiwiaXNSVEwiLCJkaXNhYmxlWm9vbSIsInBhcmVudEVsZW1lbnQiLCJyZXNpemUiLCJzZXRUaHVtYnNQb3NpdGlvbiIsInJlc2l6ZU1lZGlhIiwic2V0TWVkaWFTaXplIiwiYmFzZVZhbCIsImlzWm9vbWFibGUiLCJnZXRNZWRpYSIsInNpemUiLCJpc0Rvd25sb2FkYWJsZSIsImFkZEF0dHIiLCJhdHRycyIsImNhY2hlSUQiLCJnZXRUaHVtYkhlaWdodCIsInVubG9hZE1lZGlhIiwicmVtb3ZlTWVkaWEiLCJwYXJhbXMiLCJwYXJhbSIsInNldFF1ZXJ5U3RyaW5nIiwia2V5IiwiZGVjb2RlVVJJIiwiZW5jb2RlVVJJQ29tcG9uZW50IiwiYmFzZSIsInByb3RvY29sIiwiaG9zdCIsInBhdGhuYW1lIiwiam9pbiIsInByZXZEYXRhIiwic3RhdGUiLCJkZWx0YVgiLCJkZXRhaWwiLCJ3aGVlbERlbHRhIiwid2hlZWxEZWx0YVkiLCJ3aGVlbERlbHRhWCIsImRlbHRhTW9kZSIsInByZXZEZWx0YSIsInRpbWUiLCJwcmV2U2Nyb2xsIiwic2V0U2hhcmVUb29sdGlwIiwic2hhcmVPbiIsImltYWdlIiwibGluayIsImhyZWYiLCJ0bXAiLCJ0ZXh0IiwiaW5uZXJUZXh0Iiwic2NyZWVuWCIsIm91dGVyV2lkdGgiLCJzY3JlZW5ZIiwib3V0ZXJIZWlnaHQiLCJ0b29sdGlwIiwiY2xpZW50V2lkdGgiLCJidXR0b24iLCJncm91cCIsInBvcCIsImNsaWNrIiwicmVtb3ZlQ2hpbGQiLCJmdWxsU2NyZWVuRWxlbWVudCIsInJlcXVlc3RGdWxsU2NyZWVuIiwidG9nZ2xlRnVsbFNjcmVlbiIsInZpZGVvRnVsbFNjcmVlbiIsImF1dG9TdG9wIiwiY3ljbGUiLCJjYW52YXMiLCJnZXRDb250ZXh0IiwiY291bnREb3duIiwicGVyY2VudCIsInJhZGlhbnMiLCJQSSIsImNsZWFyUmVjdCIsInRpbWVyUHJvZ3Jlc3MiLCJzbGlkZVRvIiwiY29sb3IiLCJzdHJva2VTdHlsZSIsImxpbmVXaWR0aCIsImJlZ2luUGF0aCIsImFyYyIsInN0cm9rZSIsInZpZGVvIiwidXJscyIsImZyYWciLCJjcmVhdGVEb2N1bWVudEZyYWdtZW50IiwiZXhlYyIsImZpcnN0Q2hpbGQiLCJwbGF5VmlkZW8iLCJvblZpZGVvTG9hZGVkIiwiY2xvbmVWaWRlbyIsImluaXRpYWwiLCJjdXJyZW50IiwiZ2V0Q2xpZW50UmVjdHMiLCJTdHJpbmciLCJjb250ZW50V2luZG93IiwicG9zdE1lc3NhZ2UiLCJNZWRpYUVsZW1lbnRQbGF5ZXIiLCJtZWpzIiwicGxheWVyIiwic2V0Q29udHJvbHNTaXplIiwiZmVhdHVyZXMiLCJ2aWRlb1ZvbHVtZSIsInN0YXJ0Vm9sdW1lIiwiY2xhc3NQcmVmaXgiLCJrZXlBY3Rpb25zIiwiZW5hYmxlS2V5Ym9hcmQiLCJpUGFkVXNlTmF0aXZlQ29udHJvbHMiLCJpUGhvbmVVc2VOYXRpdmVDb250cm9scyIsIkFuZHJvaWRVc2VOYXRpdmVDb250cm9scyIsInN1Y2Nlc3MiLCJsYXN0Q2hpbGQiLCJvZmZTY3JlZW4iLCJwcmV2aW91c1NpYmxpbmciLCJvbmVycm9yIiwib25sb2FkZWRtZXRhZGF0YSIsImluc2VydE1lZGlhIiwibWVkaWFfaW5kZXgiLCJzbGlkZV9pbmRleCIsImJ1aWxkTWVkaWEiLCJhcHBlbmRNZWRpYSIsImxvYWRNZWRpYSIsImNvbnRlbnQiLCJwYXJzZUludCIsImNoaWxkRWxlbWVudENvdW50IiwiZnJhZ21lbnQiLCJvbGRNZWRpYSIsInZpc2liaWxpdHkiLCJzaG93TWVkaWEiLCJvbkNvbXBsZXRlIiwibmF0dXJhbFdpZHRoIiwicmV2ZWFsZWQiLCJ2aWV3cG9ydCIsImdldENhcHRpb25IZWlnaHQiLCJnZXRNZWRpYVNpemUiLCJmaXRNZWRpYVNpemUiLCJzZXRNZWRpYU9mZnNldCIsIm9mZnNldCIsIm5hdHVyYWxIZWlnaHQiLCJyYXRpbyIsInNtYWxsRGV2aWNlIiwiY2FuT3ZlcmZsb3ciLCJ2aWV3cG9ydHMiLCJ1bnNoaWZ0IiwiY2VpbCIsIk51bWJlciIsInRvRml4ZWQiLCJmcm9tVG9wIiwibWVkaWFWaWV3cG9ydCIsImhHYXAiLCJ2R2FwIiwiY0dhcCIsInNHYXAiLCJudW1iZXIiLCJSVEwiLCJhZGp1c3QiLCJ0b0xvYWQiLCJtYXhNZWRpYSIsIm1pbk1lZGlhIiwibWFwIiwidXBkYXRlQ291bnRlciIsInVwZGF0ZUNhcHRpb24iLCJ1cGRhdGVUaHVtYnMiLCJzaXplcyIsInNjcmVlblciLCJpbm5lcldpZHRoIiwidGh1bWJOYiIsIndpZHRocyIsInNvcnQiLCJiIiwiaXNGaW5pdGUiLCJ0aHVtYkNsaWNrIiwibG9hZFRodW1iIiwiZ2V0VGh1bWJQb3NpdGlvbiIsInNoaWZ0VGh1bWJzIiwicmVuZGVyVGh1bWJzIiwiYWRkIiwic2V0U2l6ZXMiLCJzZXRTbGlkZXJQb3NpdGlvbiIsInNldFNsaWRlc1Bvc2l0aW9ucyIsIm9wYWNpdHkiLCJpbm5lckhlaWdodCIsInNldENlbGxQb3NpdGlvbiIsInNoaWZ0U2xpZGVzIiwiaW5uZXIiLCJ0b3RhbFdpZHRoIiwiYm91bmQiLCJmbG9vciIsImhhc0hlaWdodCIsInRodW1iTmF2IiwiY2VudGVyZWQiLCJwb3NYIiwic2V0dGxlU2lkZXIiLCJyZW5kZXJTbGlkZXIiLCJzZXR0bGVDZWxsIiwicmVuZGVyQ2VsbCIsInNldHRsZVRodW1icyIsImNsb3NlQnkiLCJjYW5DbG9zZSIsImluZGV4UG9zIiwibW9kdWxvUG9zIiwicHJvZ3Jlc3MiLCJ3aWxsQ2xvc2UiLCJ0b3VjaFN0YXJ0Iiwid2hpY2giLCJhZGRQb2ludGVyIiwiZ2VzdHVyZXMiLCJ0b3VjaE1vdmUiLCJ1cGRhdGVQb2ludGVyIiwicG9pbnRlck5iIiwiaXNTZXR0bGUiLCJpc1NsaWRlclNldHRsZSIsInN3aXRjaFBvaW50ZXJzIiwiZHgiLCJkeSIsInN4Iiwic3kiLCJjYW5ab29tIiwidG91Y2hFbmQiLCJkZWxldGVQb2ludGVyIiwiZG91YmxlVGFwIiwiZ2VzdHVyZUVuZCIsIm1vdmluZyIsInRvdWNoZXMiLCJtYXBQb2ludGVyIiwidGFwIiwidGFwSWRsZSIsInRvU2V0dGxlIiwiY2hhbmdlZFRvdWNoZXMiLCJwb2ludGVySWQiLCJpZGVudGlmaWVyIiwiZ2V0UG9pbnRlciIsInB0IiwieDIiLCJ5MiIsImdldERpc3RhbmNlIiwicDEiLCJwMiIsInNxcnQiLCJwYW5ZIiwicGFuWUVuZCIsInBvc1kiLCJwYW5ab29tIiwiY2VudGVyWCIsImNlbnRlclkiLCJ1cGRhdGVab29tIiwicGFuWm9vbUVuZCIsImRyYWdUaHVtYnMiLCJkcmFnVGh1bWJzRW5kIiwiZHJhZ1NsaWRlciIsImRyYWdTbGlkZXJFbmQiLCJvbGRJbmRleCIsInJlc3RpbmdYIiwicG9zaXRpb25YIiwiZ2V0UmVzdGluZ0luZGV4IiwibW92ZWQiLCJnYXAiLCJ0byIsImNoaWxkIiwiY2VudGVyIiwidG9sZXJhbmNlIiwiYm91bmRMZWZ0IiwiYm91bmRSaWdodCIsImlzQWN0aXZlIiwic2V0VGh1bWJTaXplIiwiZ2VzdHVyZVR5cGUiLCJnZXN0dXJlQ2xvc2UiLCJkcmFnWVRvQ2xvc2UiLCJtb2R1bG9UbyIsIm1vZHVsb0Zyb20iLCJzbGlkZUJ5IiwiZnJvbUVuZHMiLCJtb3ZlQnkiLCJrZXlEb3duIiwia2V5Q29kZSIsImNlbnRYIiwiY2VudFkiLCJQb3NYIiwiUG9zWSIsIlBhcmFsbGF4U2Nyb2xsIiwic2hvd0xvZ3MiLCJfbG9nIiwiX2luaXRlZCIsIl9yZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJtb3pSZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJvUmVxdWVzdEFuaW1hdGlvbkZyYW1lIiwibXNSZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJfb25TY3JvbGwiLCJfcHJvcGVydGllcyIsImxvZyIsIm5vU21vb3RoIiwic2Nyb2xsIiwic2Nyb2xsVG9wIiwid2luZG93SGVpZ2h0IiwiZWFjaCIsInByb3h5IiwiJGVsIiwicHJvcGVydGllcyIsImFwcGx5UHJvcGVydGllcyIsImRhdGFzIiwiaURhdGEiLCJkYXRhc0xlbmd0aCIsInNjcm9sbEZyb20iLCJzY3JvbGxEaXN0YW5jZSIsInNjcm9sbFRvIiwiZWFzaW5nIiwiZWFzaW5nUmV0dXJuIiwidG90YWxUaW1lIiwidG90YWxUaW1lUmV0dXJuIiwiY3VycmVudFRpbWUiLCJzbW9vdGhuZXNzIiwic2Nyb2xsQ3VycmVudCIsImRlZmF1bHRQcm9wIiwidmFsIiwicGVyc3BlY3RpdmUiLCIkcGFyZW50IiwidHJhbnNsYXRlM2QiLCJyb3RhdGUzZCIsInNjYWxlM2QiLCJjc3NUcmFuc2Zvcm0iLCJjcm9zc0Jyb3dzZXIiLCJwcm9wZXJ0eSIsInByZWZpeCIsInVjYXNlIiwidmVuZG9yIiwic21vb3ZlSXQiLCJzbW9vdmUiLCIkaXRlbSIsIml0ZW10b3AiLCJpc05hTiIsInRyYW5zZm9ybXMiLCJ0cmFuc2Zvcm0iLCJ0IiwidHJhbnNmb3JtT3JpZ2luIiwic3RvcCIsInF1ZXVlIiwic2Nyb2xsWCIsInNjcm9sbFkiLCJ0aHJlc2hob2xkIiwic2NvcGUiLCJsYXN0IiwiZGVmZXJUaW1lciIsImNvbnRleHQiLCJkdXJhdGlvbiIsInRyYW5zaXRpb24iLCJ0cmFuc2Zvcm1TdHlsZSIsIm1pbl93aWR0aCIsIm1pbl9oZWlnaHQiLCJvbGRTY3JvbGwiLCJvbGRIZWlnaHQiLCJvbGRXaWR0aCIsIm9sZERvY0hlaWdodCIsInJlc2l6aW5nIiwicmVhZHkiLCJzY3JvbGx0b3AiLCJpc0ZpcmVmb3giLCJ1c2VyQWdlbnQiLCJpc1RvdWNoIiwiZXhwb3J0cyIsIm1vZHVsZSIsIm4iLCJyIiwibyIsInUiLCJyZXF1aXJlIiwiRXJyb3IiLCJjb2RlIiwiZ2V0T3duUHJvcGVydHlTeW1ib2xzIiwicHJvcElzRW51bWVyYWJsZSIsInByb3BlcnR5SXNFbnVtZXJhYmxlIiwidG9PYmplY3QiLCJUeXBlRXJyb3IiLCJzaG91bGRVc2VOYXRpdmUiLCJhc3NpZ24iLCJ0ZXN0MSIsImdldE93blByb3BlcnR5TmFtZXMiLCJ0ZXN0MiIsImZyb21DaGFyQ29kZSIsIm9yZGVyMiIsInRlc3QzIiwibGV0dGVyIiwiZXJyIiwic3ltYm9scyIsInByb2Nlc3MiLCJnZXROYW5vU2Vjb25kcyIsImhydGltZSIsImxvYWRUaW1lIiwibW9kdWxlTG9hZFRpbWUiLCJub2RlTG9hZFRpbWUiLCJ1cFRpbWUiLCJwZXJmb3JtYW5jZSIsImhyIiwidXB0aW1lIiwiY2FjaGVkU2V0VGltZW91dCIsImNhY2hlZENsZWFyVGltZW91dCIsImRlZmF1bHRTZXRUaW1vdXQiLCJkZWZhdWx0Q2xlYXJUaW1lb3V0IiwicnVuVGltZW91dCIsImZ1biIsInJ1bkNsZWFyVGltZW91dCIsIm1hcmtlciIsImRyYWluaW5nIiwiY3VycmVudFF1ZXVlIiwicXVldWVJbmRleCIsImNsZWFuVXBOZXh0VGljayIsImRyYWluUXVldWUiLCJsZW4iLCJuZXh0VGljayIsIkFycmF5IiwiSXRlbSIsImFycmF5IiwiZW52IiwiYXJndiIsInZlcnNpb25zIiwiYWRkTGlzdGVuZXIiLCJvbmNlIiwicmVtb3ZlTGlzdGVuZXIiLCJyZW1vdmVBbGxMaXN0ZW5lcnMiLCJlbWl0IiwicHJlcGVuZExpc3RlbmVyIiwicHJlcGVuZE9uY2VMaXN0ZW5lciIsImJpbmRpbmciLCJjd2QiLCJjaGRpciIsImRpciIsInVtYXNrIiwiZ2xvYmFsIiwidmVuZG9ycyIsInN1ZmZpeCIsInJhZiIsImNhZiIsImZyYW1lRHVyYXRpb24iLCJfbm93IiwiY3AiLCJjYW5jZWxsZWQiLCJoYW5kbGUiLCJjYW5jZWwiLCJwb2x5ZmlsbCIsIl9jcmVhdGVDbGFzcyIsImRlZmluZVByb3BlcnRpZXMiLCJwcm9wcyIsImRlc2NyaXB0b3IiLCJlbnVtZXJhYmxlIiwiY29uZmlndXJhYmxlIiwid3JpdGFibGUiLCJkZWZpbmVQcm9wZXJ0eSIsIkNvbnN0cnVjdG9yIiwicHJvdG9Qcm9wcyIsInN0YXRpY1Byb3BzIiwiX2NsYXNzQ2FsbENoZWNrIiwicnFBbkZyIiwib2JqZWN0QXNzaWduIiwiaGVscGVycyIsInByb3BlcnR5Q2FjaGUiLCJjbGFtcCIsImRlc2VyaWFsaXplIiwiY2FtZWxDYXNlIiwiY2hhcmFjdGVyIiwiYWNjZWxlcmF0ZSIsInRyYW5zZm9ybVN1cHBvcnQiLCJwcm9wZXJ0eVN1cHBvcnQiLCJwcm9wZXJ0eVZhbHVlIiwiZmVhdHVyZVN1cHBvcnQiLCJjc3NQcm9wZXJ0eSIsImpzUHJvcGVydHkiLCJkb2N1bWVudE92ZXJmbG93IiwiaXNDcmVhdGVkQm9keSIsImJhY2tncm91bmQiLCJnZXRDb21wdXRlZFN0eWxlIiwiZ2V0UHJvcGVydHlWYWx1ZSIsIk1BR0lDX05VTUJFUiIsIkRFRkFVTFRTIiwicmVsYXRpdmVJbnB1dCIsImNsaXBSZWxhdGl2ZUlucHV0IiwiaW5wdXRFbGVtZW50IiwiaG92ZXJPbmx5IiwiY2FsaWJyYXRpb25UaHJlc2hvbGQiLCJjYWxpYnJhdGlvbkRlbGF5Iiwic3VwcG9ydERlbGF5IiwiY2FsaWJyYXRlWCIsImNhbGlicmF0ZVkiLCJpbnZlcnRYIiwiaW52ZXJ0WSIsImxpbWl0WCIsImxpbWl0WSIsInNjYWxhclgiLCJzY2FsYXJZIiwiZnJpY3Rpb25YIiwiZnJpY3Rpb25ZIiwib3JpZ2luWCIsIm9yaWdpblkiLCJwb2ludGVyRXZlbnRzIiwicHJlY2lzaW9uIiwib25SZWFkeSIsInNlbGVjdG9yIiwiUGFyYWxsYXgiLCJjYWxpYnJhdGlvblRpbWVyIiwiY2FsaWJyYXRpb25GbGFnIiwiZW5hYmxlZCIsImRlcHRoc1giLCJkZXB0aHNZIiwiYm91bmRzIiwiZWxlbWVudFBvc2l0aW9uWCIsImVsZW1lbnRQb3NpdGlvblkiLCJlbGVtZW50V2lkdGgiLCJlbGVtZW50SGVpZ2h0IiwiZWxlbWVudENlbnRlclgiLCJlbGVtZW50Q2VudGVyWSIsImVsZW1lbnRSYW5nZVgiLCJlbGVtZW50UmFuZ2VZIiwiY2FsaWJyYXRpb25YIiwiY2FsaWJyYXRpb25ZIiwiaW5wdXRYIiwiaW5wdXRZIiwibW90aW9uWCIsIm1vdGlvblkiLCJ2ZWxvY2l0eVgiLCJ2ZWxvY2l0eVkiLCJvbk1vdXNlTW92ZSIsIm9uRGV2aWNlT3JpZW50YXRpb24iLCJvbkRldmljZU1vdGlvbiIsIm9uT3JpZW50YXRpb25UaW1lciIsIm9uTW90aW9uVGltZXIiLCJvbkNhbGlicmF0aW9uVGltZXIiLCJvbkFuaW1hdGlvbkZyYW1lIiwib25XaW5kb3dSZXNpemUiLCJ3aW5kb3dXaWR0aCIsIndpbmRvd0NlbnRlclgiLCJ3aW5kb3dDZW50ZXJZIiwid2luZG93UmFkaXVzWCIsIndpbmRvd1JhZGl1c1kiLCJwb3J0cmFpdCIsImRlc2t0b3AiLCJtb3Rpb25TdXBwb3J0IiwiRGV2aWNlTW90aW9uRXZlbnQiLCJvcmllbnRhdGlvblN1cHBvcnQiLCJEZXZpY2VPcmllbnRhdGlvbkV2ZW50Iiwib3JpZW50YXRpb25TdGF0dXMiLCJtb3Rpb25TdGF0dXMiLCJpbml0aWFsaXNlIiwidHJhbnNmb3JtMkRTdXBwb3J0IiwidHJhbnNmb3JtM0RTdXBwb3J0IiwidXBkYXRlTGF5ZXJzIiwidXBkYXRlRGltZW5zaW9ucyIsImVuYWJsZSIsInF1ZXVlQ2FsaWJyYXRpb24iLCJkb1JlYWR5Q2FsbGJhY2siLCJsYXllcnMiLCJ3YXJuIiwibGF5ZXIiLCJkZXB0aCIsInVwZGF0ZUJvdW5kcyIsImRldGVjdGlvblRpbWVyIiwiZGlzYWJsZSIsImNhbGlicmF0ZSIsImludmVydCIsInNjYWxhciIsIm9yaWdpbiIsInNldElucHV0RWxlbWVudCIsInNldFBvc2l0aW9uIiwiY2FsaWJyYXRlZElucHV0WCIsImNhbGlicmF0ZWRJbnB1dFkiLCJkZXB0aFgiLCJkZXB0aFkiLCJ4T2Zmc2V0IiwieU9mZnNldCIsInJvdGF0ZSIsImJldGEiLCJnYW1tYSIsInJvdGF0aW9uUmF0ZSIsIklFIiwiZG9jdW1lbnRNb2RlIiwiZGl2IiwiTkFNRSIsImNoYXJhY3RlclJhbmdlcyIsImxhdGluUHVuY3R1YXRpb24iLCJsYXRpbkxldHRlcnMiLCJSZWciLCJhYmJyZXZpYXRpb25zIiwiaW5uZXJXb3JkUGVyaW9kIiwib25seUNvbnRhaW5zUHVuY3R1YXRpb24iLCJhZGpvaW5lZFB1bmN0dWF0aW9uIiwic2tpcHBlZEVsZW1lbnRzIiwiaGFzUGx1Z2luQ2xhc3MiLCJlbmNvZGVQdW5jdHVhdGlvbiIsImRlY29kZVB1bmN0dWF0aW9uIiwiZnVsbE1hdGNoIiwic3ViTWF0Y2giLCJ3cmFwTm9kZSIsIm9wdHMiLCJ3cmFwcGVyIiwiY3VzdG9tQ2xhc3MiLCJnZW5lcmF0ZUluZGV4SUQiLCJFbGVtZW50IiwiYmxhc3RlZEluZGV4IiwiZGVsaW1pdGVyIiwid2hpdGVTcGFjZSIsImdlbmVyYXRlVmFsdWVDbGFzcyIsInZhbHVlQ2xhc3MiLCJhcmlhIiwidHJhdmVyc2VET00iLCJtYXRjaFBvc2l0aW9uIiwic2tpcE5vZGVCaXQiLCJub2RlVHlwZSIsIm5vZGVCZWdpbm5pbmciLCJkZWxpbWl0ZXJSZWdleCIsIm1hdGNoVGV4dCIsInN1Yk1hdGNoVGV4dCIsIm1pZGRsZUJpdCIsInNwbGl0VGV4dCIsIndyYXBwZWROb2RlIiwicmVwbGFjZUNoaWxkIiwid3JhcHBlcnMiLCJoYXNDaGlsZE5vZGVzIiwiY2hpbGROb2RlcyIsInRvU3RyaW5nIiwiJHRoaXMiLCJkZWJ1ZyIsInN0cmlwSFRNTFRhZ3MiLCJodG1sIiwidGltZUVuZCIsIm91dGVySFRNTCIsImJhY2tncm91bmRDb2xvciIsInNraXBwZWREZXNjZW5kYW50Um9vdCIsImZpbmQiLCJjbG9zZXN0IiwidGhpc1BhcmVudE5vZGUiLCJub3JtYWxpemUiLCJyZW1vdmVEYXRhIiwicmV0dXJuR2VuZXJhdGVkIiwibmV3U3RhY2siLCJwcmV2T2JqZWN0IiwiYmxhc3QiLCJtb2R1bGVzIiwiaW5zdGFsbGVkTW9kdWxlcyIsIl9fd2VicGFja19yZXF1aXJlX18iLCJtb2R1bGVJZCIsIm0iLCJkIiwiZ2V0dGVyIiwiZ2V0IiwiU3ltYm9sIiwidG9TdHJpbmdUYWciLCJtb2RlIiwiX19lc01vZHVsZSIsIm5zIiwiZ2V0RGVmYXVsdCIsImdldE1vZHVsZUV4cG9ydHMiLCJyZWFkeVN0YXRlIiwiYXR0YWNoRXZlbnQiLCJfdHlwZW9mIiwiaXRlcmF0b3IiLCJjb25zdHJ1Y3RvciIsIkZ1bmN0aW9uIiwiX193ZWJwYWNrX2V4cG9ydHNfXyIsImxpdGVfcmVhZHlfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzBfXyIsImxpdGVfcmVhZHlfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzBfX19kZWZhdWx0IiwiZ2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX18iLCJnbG9iYWxfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzFfX19kZWZhdWx0IiwiX2phcmFsbGF4X2VzbV9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMl9fIiwib2xkUGx1Z2luIiwiamFyYWxsYXgiLCJub0NvbmZsaWN0IiwialF1ZXJ5UGx1Z2luIiwiX2xlbiIsIl9rZXkiLCJyZXMiLCJvbGRKcVBsdWdpbiIsIl9zbGljZWRUb0FycmF5IiwiYXJyIiwiX2FycmF5V2l0aEhvbGVzIiwiX2l0ZXJhYmxlVG9BcnJheUxpbWl0IiwiX3Vuc3VwcG9ydGVkSXRlcmFibGVUb0FycmF5IiwiX25vbkl0ZXJhYmxlUmVzdCIsIm1pbkxlbiIsIl9hcnJheUxpa2VUb0FycmF5IiwiYXJyMiIsIl9hcnIiLCJfbiIsIl9kIiwiX2UiLCJfaSIsIl9zIiwiZG9uZSIsIl9kZWZpbmVQcm9wZXJ0aWVzIiwiaXNJRSIsImlzTW9iaWxlIiwic3VwcG9ydFRyYW5zZm9ybSIsIiRkZXZpY2VIZWxwZXIiLCJnZXREZXZpY2VIZWlnaHQiLCJjc3NUZXh0Iiwid25kSCIsInVwZGF0ZVduZFZhcnMiLCJqYXJhbGxheExpc3QiLCJnZXRQYXJlbnRzIiwiZWxlbSIsInBhcmVudHMiLCJ1cGRhdGVQYXJhbGxheCIsIm9sZERhdGEiLCJjbGllbnRSZWN0IiwibmV3RGF0YSIsInduZFciLCJpc1Jlc2l6ZWQiLCJpc1Njcm9sbGVkIiwib25SZXNpemUiLCJvblNjcm9sbCIsImluc3RhbmNlSUQiLCJKYXJhbGxheCIsInVzZXJPcHRpb25zIiwic3BlZWQiLCJpbWdTcmMiLCJpbWdFbGVtZW50IiwiaW1nU2l6ZSIsImltZ1Bvc2l0aW9uIiwiaW1nUmVwZWF0Iiwia2VlcEltZyIsImVsZW1lbnRJblZpZXdwb3J0IiwiekluZGV4IiwiZGlzYWJsZVBhcmFsbGF4IiwiZGlzYWJsZVZpZGVvIiwidmlkZW9TcmMiLCJ2aWRlb1N0YXJ0VGltZSIsInZpZGVvRW5kVGltZSIsInZpZGVvTG9vcCIsInZpZGVvUGxheU9ubHlWaXNpYmxlIiwidmlkZW9MYXp5TG9hZGluZyIsIm9uSW5pdCIsIm9uRGVzdHJveSIsIm9uQ292ZXJJbWFnZSIsImRhdGFPcHRpb25zIiwiZGF0YXNldCIsInB1cmVEYXRhT3B0aW9ucyIsImxvd2VDYXNlT3B0aW9uIiwicHVyZU9wdGlvbnMiLCJkaXNhYmxlUGFyYWxsYXhSZWdleHAiLCJkaXNhYmxlVmlkZW9SZWdleHAiLCJlbGVtZW50SW5WUCIsIl9lbGVtZW50SW5WUCIsIl9lbGVtZW50SW5WUDIiLCIkY29udGFpbmVyIiwidXNlSW1nVGFnIiwiaW5pdEltZyIsImNhbkluaXRQYXJhbGxheCIsInN0eWxlcyIsIm91dCIsImdldFdpbmRvd0RhdGEiLCIkaW1nRWxlbWVudCIsIiRpdGVtUGFyZW50IiwiYmdJbWFnZSIsImNvbnRhaW5lclN0eWxlcyIsImltYWdlU3R5bGVzIiwiYmFja2ZhY2VWaXNpYmlsaXR5Iiwid2lsbENoYW5nZSIsImN1clN0eWxlIiwiY3VySW1nU3R5bGUiLCIkcGFyZW50cyIsInBhcmVudFRyYW5zZm9ybSIsIm92ZXJmbG93UmVnZXgiLCJhZGRUb1BhcmFsbGF4TGlzdCIsInJlbW92ZUZyb21QYXJhbGxheExpc3QiLCJvcmlnaW5hbFN0eWxlc1RhZyIsIm9yaWdpbmFsU3R5bGVzSW1nVGFnIiwiJGNsaXBTdHlsZXMiLCJjbGlwQ29udGFpbmVyIiwicmVjdCIsImhlYWQiLCJzdHlsZVNoZWV0IiwiY292ZXJJbWFnZSIsImNvbnRIIiwiaXNTY3JvbGwiLCJzY3JvbGxEaXN0IiwicmVzdWx0SCIsInJlc3VsdE1UIiwicGFyYWxsYXhTY3JvbGxEaXN0YW5jZSIsIm1hcmdpblRvcCIsImNvbnRhaW5lciIsImlzVmlzaWJsZSIsImlzRWxlbWVudEluVmlld3BvcnQiLCJjb250VCIsInZpZXdwb3J0UmVjdCIsImJlZm9yZVRvcCIsImJlZm9yZVRvcEVuZCIsImFmdGVyVG9wIiwiYmVmb3JlQm90dG9tIiwiYmVmb3JlQm90dG9tRW5kIiwiYWZ0ZXJCb3R0b20iLCJmcm9tVmlld3BvcnRDZW50ZXIiLCJ2aXNpYmxlUGVyY2VudCIsInBvc2l0aW9uWSIsInNlY3Rpb24iLCJwbHVnaW4iLCJIVE1MRWxlbWVudCIsInJldCIsIl9sZW4yIiwiX2tleTIiLCJzdGlja19pbl9wYXJlbnQiLCJBIiwidyIsIkoiLCJCIiwiSyIsInEiLCJFIiwic3RpY2t5X2NsYXNzIiwiaW5uZXJfc2Nyb2xsaW5nIiwicmVjYWxjX2V2ZXJ5Iiwib2Zmc2V0X3RvcCIsInNwYWNlciIsImJvdHRvbWluZyIsIkMiLCJGIiwiRyIsInYiLCJIIiwiRCIsIkkiLCJ6IiwiaCIsImluc2VydEFmdGVyIiwiZGV0YWNoIiwiYWZ0ZXIiLCJhcHBlbmQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsQ0FBQyxVQUFTQSxNQUFULEVBQWdCQyxTQUFoQixFQUEwQjtBQUFDOztBQUFhLE1BQUlDLENBQUMsR0FBQ0YsTUFBTSxDQUFDRyxNQUFQLElBQWVILE1BQU0sQ0FBQ0ksS0FBNUI7QUFBQSxNQUFrQ0MsY0FBYyxHQUFDLENBQWpEO0FBQUEsTUFBbURDLFlBQVksR0FBQyxLQUFoRTs7QUFBc0VKLEdBQUMsQ0FBQ0ssRUFBRixDQUFLQyxJQUFMLEdBQVVOLENBQUMsQ0FBQ0ssRUFBRixDQUFLRSxJQUFMLEdBQVUsVUFBU0MsUUFBVCxFQUFrQjtBQUFDLFdBQU8sSUFBSUMsVUFBSixDQUFlLElBQWYsRUFBb0JELFFBQXBCLENBQVA7QUFBc0MsR0FBN0U7O0FBQThFUixHQUFDLENBQUNNLElBQUYsR0FBT04sQ0FBQyxDQUFDTyxJQUFGLEdBQU8sVUFBU0csS0FBVCxFQUFlQyxRQUFmLEVBQXdCQyxNQUF4QixFQUErQjtBQUFDLFFBQUdaLENBQUMsQ0FBQ2EsVUFBRixDQUFhRixRQUFiLENBQUgsRUFBMEI7QUFBQ0MsWUFBTSxHQUFDRCxRQUFQO0FBQWdCQSxjQUFRLEdBQUMsRUFBVDtBQUFhOztBQUNuUyxRQUFHLENBQUNYLENBQUMsQ0FBQ2EsVUFBRixDQUFhRCxNQUFiLENBQUosRUFBeUI7QUFBQztBQUFROztBQUNsQ0YsU0FBSyxHQUFDVixDQUFDLENBQUNjLE9BQUYsQ0FBVUosS0FBVixJQUFpQkEsS0FBakIsR0FBdUIsQ0FBQ0EsS0FBRCxDQUE3QjtBQUFxQ0MsWUFBUSxHQUFDWCxDQUFDLENBQUNjLE9BQUYsQ0FBVUgsUUFBVixJQUFvQkEsUUFBcEIsR0FBNkIsQ0FBQ0EsUUFBRCxDQUF0QztBQUFpRCxRQUFJSSxNQUFNLEdBQUNOLFVBQVUsQ0FBQ08sU0FBWCxDQUFxQkQsTUFBaEM7QUFBQSxRQUF1Q0UsTUFBTSxHQUFDRixNQUFNLENBQUNHLEVBQVAsS0FBWUgsTUFBTSxDQUFDRyxFQUFQLEdBQVUsRUFBdEIsQ0FBOUM7O0FBQXdFLFNBQUksSUFBSUMsQ0FBQyxHQUFDLENBQU4sRUFBUUMsQ0FBQyxHQUFDVixLQUFLLENBQUNXLE1BQXBCLEVBQTJCRixDQUFDLEdBQUNDLENBQTdCLEVBQStCRCxDQUFDLEVBQWhDLEVBQW1DO0FBQUMsVUFBR0osTUFBTSxDQUFDTCxLQUFLLENBQUNTLENBQUQsQ0FBTixDQUFOLEtBQW1CcEIsU0FBbkIsSUFBOEJDLENBQUMsQ0FBQ2EsVUFBRixDQUFhRSxNQUFNLENBQUNMLEtBQUssQ0FBQ1MsQ0FBRCxDQUFOLENBQW5CLENBQWpDLEVBQWdFO0FBQUNKLGNBQU0sQ0FBQ0wsS0FBSyxDQUFDUyxDQUFELENBQU4sQ0FBTixHQUFpQlAsTUFBakI7QUFBeUI7QUFBQzs7QUFDN1IsU0FBSSxJQUFJVSxDQUFDLEdBQUMsQ0FBTixFQUFRQyxDQUFDLEdBQUNaLFFBQVEsQ0FBQ1UsTUFBdkIsRUFBOEJDLENBQUMsR0FBQ0MsQ0FBaEMsRUFBa0NELENBQUMsRUFBbkMsRUFBc0M7QUFBQ0wsWUFBTSxDQUFDTixRQUFRLENBQUNXLENBQUQsQ0FBVCxDQUFOLEdBQW9CWixLQUFLLENBQUMsQ0FBRCxDQUF6QjtBQUE4QjtBQUFDLEdBSHVIOztBQUd0SCxXQUFTYyxZQUFULENBQXNCQyxRQUF0QixFQUErQlYsTUFBL0IsRUFBc0NXLEtBQXRDLEVBQTRDQyxNQUE1QyxFQUFtREMsU0FBbkQsRUFBNkQ7QUFBQyxRQUFJQyxrQkFBa0IsR0FBQyxDQUF2QjtBQUFBLFFBQXlCQyxZQUFZLEdBQUMsQ0FBQyxDQUF2QztBQUFBLFFBQXlDQyxhQUFhLEdBQUMsQ0FBQyxDQUF4RDtBQUFBLFFBQTBEQyxnQkFBZ0IsR0FBQyxLQUEzRTtBQUFBLFFBQWlGQyxVQUFVLEdBQUMsV0FBNUY7QUFBQSxRQUF3R0MsS0FBSyxHQUFDLE1BQTlHO0FBQUEsUUFBcUhDLE1BQU0sR0FBQyxPQUE1SDtBQUFBLFFBQW9JQyxJQUFJLEdBQUMsS0FBekk7QUFBQSxRQUErSUMsSUFBSSxHQUFDLEtBQXBKO0FBQUEsUUFBMEpDLE9BQU8sR0FBQyxRQUFsSztBQUFBLFFBQTJLQyxNQUFNLEdBQUMsT0FBbEw7QUFBQSxRQUEwTEMsZ0JBQWdCLEdBQUMsa0JBQTNNOztBQUE4TixhQUFTQyxXQUFULEdBQXNCO0FBQUNULHNCQUFnQixHQUFDbEMsTUFBTSxDQUFDNEMsZ0JBQVAsR0FBd0IsQ0FBekM7QUFBMkNoQixXQUFLLEdBQUNpQixhQUFhLENBQUNqQixLQUFELENBQW5COztBQUEyQixVQUFHWCxNQUFNLENBQUM2QixLQUFQLElBQWMsQ0FBakIsRUFBbUI7QUFBQ0Msa0JBQVUsQ0FBQyxZQUFVO0FBQUNDLHdCQUFjLENBQUMsSUFBRCxDQUFkO0FBQXNCLFNBQWxDLEVBQW1DL0IsTUFBTSxDQUFDNkIsS0FBMUMsQ0FBVjtBQUE0RDs7QUFDaGhCLFVBQUc3QixNQUFNLENBQUM2QixLQUFQLEdBQWEsQ0FBYixJQUFnQjdCLE1BQU0sQ0FBQ2dDLFFBQTFCLEVBQW1DO0FBQUNwQixjQUFNLENBQUNxQixDQUFQLEdBQVNDLFNBQVMsQ0FBQ2xDLE1BQU0sQ0FBQ21DLFFBQVIsRUFBaUIsVUFBU0MsS0FBVCxFQUFlO0FBQUMsY0FBR0EsS0FBSyxDQUFDQyxJQUFOLEtBQWEsUUFBaEIsRUFBeUI7QUFBQ3RCLHdCQUFZLEdBQUNDLGFBQWEsR0FBQyxDQUFDLENBQTVCO0FBQStCOztBQUNoSmUsd0JBQWMsQ0FBQ0ssS0FBSyxDQUFDRSxHQUFQLENBQWQ7QUFBMkIsU0FEMkIsQ0FBbEI7O0FBQ1AxQixjQUFNLENBQUNKLENBQVAsR0FBUyxVQUFTK0IsZUFBVCxFQUF5QjtBQUFDQSx5QkFBZSxHQUFDWCxhQUFhLENBQUNXLGVBQUQsQ0FBN0I7QUFBK0M1QixlQUFLLENBQUM2QixJQUFOLENBQVdDLEtBQVgsQ0FBaUI5QixLQUFqQixFQUF1QjRCLGVBQXZCO0FBQXlDLFNBQTNIOztBQUE0SDNCLGNBQU0sQ0FBQzhCLENBQVAsR0FBUyxZQUFVO0FBQUMsaUJBQU8vQixLQUFLLEdBQUMxQixDQUFDLENBQUMwQixLQUFELENBQUQsQ0FBU2dDLE1BQVQsQ0FBZ0IsWUFBVTtBQUFDLG1CQUFNLENBQUMxRCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVEyRCxJQUFSLENBQWE1QyxNQUFNLENBQUM2QyxVQUFwQixDQUFQO0FBQXdDLFdBQW5FLENBQWI7QUFBb0YsU0FBeEc7O0FBQXlHakMsY0FBTSxDQUFDa0MsQ0FBUCxHQUFTLFVBQVNDLFdBQVQsRUFBcUI7QUFBQyxlQUFJLElBQUkzQyxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUMyQyxXQUFXLENBQUN6QyxNQUExQixFQUFpQ0YsQ0FBQyxFQUFsQyxFQUFxQztBQUFDLGdCQUFJNEMsSUFBSSxHQUFDckMsS0FBSyxDQUFDZ0MsTUFBTixDQUFhLFlBQVU7QUFBQyxxQkFBTyxTQUFPSSxXQUFXLENBQUMzQyxDQUFELENBQXpCO0FBQThCLGFBQXRELENBQVQ7O0FBQWlFLGdCQUFHNEMsSUFBSSxDQUFDMUMsTUFBUixFQUFlO0FBQUN5Qiw0QkFBYyxDQUFDLEtBQUQsRUFBT2lCLElBQVAsQ0FBZDtBQUE0QjtBQUFDO0FBQUMsU0FBcEw7O0FBQXFMakIsc0JBQWM7O0FBQUc5QyxTQUFDLENBQUNlLE1BQU0sQ0FBQ2lELFlBQVIsQ0FBRCxDQUF1QkMsRUFBdkIsQ0FBMEIsWUFBVXJDLFNBQVYsR0FBb0IsVUFBcEIsR0FBK0JBLFNBQXpELEVBQW1FRCxNQUFNLENBQUNxQixDQUExRTtBQUE4RTtBQUFDOztBQUN2aEIsYUFBU0wsYUFBVCxDQUF1QmpCLEtBQXZCLEVBQTZCO0FBQUMsVUFBSXdDLFlBQVksR0FBQ25ELE1BQU0sQ0FBQ21ELFlBQXhCO0FBQUEsVUFBcUNDLFdBQVcsR0FBQ3BELE1BQU0sQ0FBQ29ELFdBQXhEO0FBQUEsVUFBb0VDLFNBQVMsR0FBQ3JELE1BQU0sQ0FBQ3FELFNBQXJGO0FBQUEsVUFBK0ZDLGVBQWUsR0FBQ3RELE1BQU0sQ0FBQ3NELGVBQXRIO0FBQUEsVUFBc0lDLGVBQWUsR0FBQ3ZELE1BQU0sQ0FBQ3VELGVBQTdKO0FBQUEsVUFBNktDLFVBQVUsR0FBQ3hELE1BQU0sQ0FBQ0csRUFBUCxJQUFXLEVBQW5NO0FBQXNNUSxXQUFLLEdBQUMxQixDQUFDLENBQUMwQixLQUFELENBQUQsQ0FBU2dDLE1BQVQsQ0FBZ0IsWUFBVTtBQUFDLFlBQUljLE9BQU8sR0FBQ3hFLENBQUMsQ0FBQyxJQUFELENBQWI7QUFBQSxZQUFvQnlFLEdBQUcsR0FBQ0Msa0JBQWtCLENBQUMsSUFBRCxDQUExQzs7QUFBaUQsZUFBTSxDQUFDRixPQUFPLENBQUNiLElBQVIsQ0FBYTVDLE1BQU0sQ0FBQzRELFdBQXBCLENBQUQsS0FBb0NILE9BQU8sQ0FBQ0ksSUFBUixDQUFhN0QsTUFBTSxDQUFDOEQsU0FBcEIsS0FBZ0NMLE9BQU8sQ0FBQ0ksSUFBUixDQUFhUCxlQUFiLENBQWhDLElBQStERyxPQUFPLENBQUNJLElBQVIsQ0FBYU4sZUFBYixDQUEvRCxJQUE4RkMsVUFBVSxDQUFDRSxHQUFELENBQVYsS0FBa0IxRSxTQUFwSixDQUFOO0FBQXNLLE9BQWxQLEVBQW9QNEQsSUFBcFAsQ0FBeVAsWUFBVTVDLE1BQU0sQ0FBQytELElBQTFRLEVBQStRckQsUUFBL1EsQ0FBTjs7QUFBK1IsV0FBSSxJQUFJTixDQUFDLEdBQUMsQ0FBTixFQUFRQyxDQUFDLEdBQUNNLEtBQUssQ0FBQ0wsTUFBcEIsRUFBMkJGLENBQUMsR0FBQ0MsQ0FBN0IsRUFBK0JELENBQUMsRUFBaEMsRUFBbUM7QUFBQyxZQUFJcUQsT0FBTyxHQUFDeEUsQ0FBQyxDQUFDMEIsS0FBSyxDQUFDUCxDQUFELENBQU4sQ0FBYjtBQUFBLFlBQXdCc0QsR0FBRyxHQUFDQyxrQkFBa0IsQ0FBQ2hELEtBQUssQ0FBQ1AsQ0FBRCxDQUFOLENBQTlDO0FBQUEsWUFBeUQ0RCxnQkFBZ0IsR0FBQ1AsT0FBTyxDQUFDSSxJQUFSLENBQWE3RCxNQUFNLENBQUNpRSxrQkFBcEIsS0FBeUNaLFNBQW5IOztBQUE2SCxZQUFHSyxHQUFHLEtBQUdyQyxJQUFOLElBQVkyQyxnQkFBWixJQUE4QlAsT0FBTyxDQUFDSSxJQUFSLENBQWFQLGVBQWIsQ0FBakMsRUFBK0Q7QUFBQ0csaUJBQU8sQ0FBQ0ksSUFBUixDQUFhUCxlQUFiLEVBQTZCWSxtQkFBbUIsQ0FBQ1QsT0FBTyxDQUFDSSxJQUFSLENBQWFQLGVBQWIsQ0FBRCxFQUErQlUsZ0JBQS9CLENBQWhEO0FBQW1HOztBQUN2MEIsWUFBR1IsVUFBVSxDQUFDRSxHQUFELENBQVYsS0FBa0IxRSxTQUFsQixJQUE2QixDQUFDeUUsT0FBTyxDQUFDSSxJQUFSLENBQWFOLGVBQWIsQ0FBakMsRUFBK0Q7QUFBQ0UsaUJBQU8sQ0FBQ0ksSUFBUixDQUFhTixlQUFiLEVBQTZCQyxVQUFVLENBQUNFLEdBQUQsQ0FBdkM7QUFBK0M7O0FBQy9HLFlBQUdBLEdBQUcsS0FBR3JDLElBQU4sSUFBWThCLFlBQVosSUFBMEIsQ0FBQ00sT0FBTyxDQUFDSSxJQUFSLENBQWF2QyxJQUFiLENBQTlCLEVBQWlEO0FBQUNtQyxpQkFBTyxDQUFDSSxJQUFSLENBQWF2QyxJQUFiLEVBQWtCNkIsWUFBbEI7QUFBaUMsU0FBbkYsTUFDSyxJQUFHTyxHQUFHLEtBQUdyQyxJQUFOLElBQVkrQixXQUFaLEtBQTBCLENBQUNLLE9BQU8sQ0FBQ1UsR0FBUixDQUFZMUMsZ0JBQVosQ0FBRCxJQUFnQ2dDLE9BQU8sQ0FBQ1UsR0FBUixDQUFZMUMsZ0JBQVosTUFBZ0MsTUFBMUYsQ0FBSCxFQUFxRztBQUFDZ0MsaUJBQU8sQ0FBQ1UsR0FBUixDQUFZMUMsZ0JBQVosRUFBNkIsVUFBUTJCLFdBQVIsR0FBb0IsSUFBakQ7QUFBd0Q7QUFBQzs7QUFDcEssYUFBT3pDLEtBQVA7QUFBYzs7QUFDZCxhQUFTb0IsY0FBVCxDQUF3QnFDLFFBQXhCLEVBQWlDbEUsTUFBakMsRUFBd0M7QUFBQyxVQUFHLENBQUNTLEtBQUssQ0FBQ0wsTUFBVixFQUFpQjtBQUFDLFlBQUdOLE1BQU0sQ0FBQ3FFLFdBQVYsRUFBc0I7QUFBQzNELGtCQUFRLENBQUM0RCxPQUFUO0FBQW9COztBQUN0RztBQUFROztBQUNSLFVBQUkxRSxRQUFRLEdBQUNNLE1BQU0sSUFBRVMsS0FBckI7QUFBQSxVQUEyQjRELGFBQWEsR0FBQyxLQUF6QztBQUFBLFVBQStDbEIsU0FBUyxHQUFDckQsTUFBTSxDQUFDcUQsU0FBUCxJQUFrQixFQUEzRTtBQUFBLFVBQThFQyxlQUFlLEdBQUN0RCxNQUFNLENBQUNzRCxlQUFyRztBQUFBLFVBQXFITSxXQUFXLEdBQUM1RCxNQUFNLENBQUM0RCxXQUF4STs7QUFBb0osV0FBSSxJQUFJeEQsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDUixRQUFRLENBQUNVLE1BQXZCLEVBQThCRixDQUFDLEVBQS9CLEVBQWtDO0FBQUMsWUFBR2dFLFFBQVEsSUFBRWxFLE1BQVYsSUFBa0JzRSxpQkFBaUIsQ0FBQzVFLFFBQVEsQ0FBQ1EsQ0FBRCxDQUFULENBQXRDLEVBQW9EO0FBQUMsY0FBSXFELE9BQU8sR0FBQ3hFLENBQUMsQ0FBQ1csUUFBUSxDQUFDUSxDQUFELENBQVQsQ0FBYjtBQUFBLGNBQTJCc0QsR0FBRyxHQUFDQyxrQkFBa0IsQ0FBQy9ELFFBQVEsQ0FBQ1EsQ0FBRCxDQUFULENBQWpEO0FBQUEsY0FBK0QwRCxTQUFTLEdBQUNMLE9BQU8sQ0FBQ0ksSUFBUixDQUFhN0QsTUFBTSxDQUFDOEQsU0FBcEIsQ0FBekU7QUFBQSxjQUF3R0UsZ0JBQWdCLEdBQUNQLE9BQU8sQ0FBQ0ksSUFBUixDQUFhN0QsTUFBTSxDQUFDaUUsa0JBQXBCLEtBQXlDWixTQUFsSztBQUFBLGNBQTRLb0IsWUFBWSxHQUFDaEIsT0FBTyxDQUFDSSxJQUFSLENBQWE3RCxNQUFNLENBQUN1RCxlQUFwQixDQUF6TDs7QUFBOE4sY0FBRyxDQUFDRSxPQUFPLENBQUNiLElBQVIsQ0FBYWdCLFdBQWIsQ0FBRCxLQUE2QixDQUFDNUQsTUFBTSxDQUFDMEUsV0FBUixJQUFxQmpCLE9BQU8sQ0FBQ2tCLEVBQVIsQ0FBVyxVQUFYLENBQWxELE1BQTRFLENBQUNiLFNBQVMsSUFBRUwsT0FBTyxDQUFDSSxJQUFSLENBQWFQLGVBQWIsQ0FBWixNQUE4Q0ksR0FBRyxLQUFHckMsSUFBTixLQUFhMkMsZ0JBQWdCLEdBQUNGLFNBQWpCLEtBQTZCTCxPQUFPLENBQUNJLElBQVIsQ0FBYXZDLElBQWIsQ0FBN0IsSUFBaURtQyxPQUFPLENBQUNJLElBQVIsQ0FBYVAsZUFBYixNQUFnQ0csT0FBTyxDQUFDSSxJQUFSLENBQWF0QyxPQUFiLENBQTlGLENBQUQsSUFBeUhtQyxHQUFHLEtBQUdyQyxJQUFOLElBQVkyQyxnQkFBZ0IsR0FBQ0YsU0FBakIsS0FBNkJMLE9BQU8sQ0FBQ1UsR0FBUixDQUFZMUMsZ0JBQVosQ0FBL00sS0FBZ1BnRCxZQUE1VCxDQUFILEVBQzFjO0FBQUNGLHlCQUFhLEdBQUMsSUFBZDtBQUFtQmQsbUJBQU8sQ0FBQ2IsSUFBUixDQUFhZ0IsV0FBYixFQUF5QixJQUF6Qjs7QUFBK0JnQix1QkFBVyxDQUFDbkIsT0FBRCxFQUFTQyxHQUFULEVBQWFNLGdCQUFiLEVBQThCUyxZQUE5QixDQUFYO0FBQXdEO0FBQUM7QUFBQzs7QUFDN0csVUFBR0YsYUFBSCxFQUFpQjtBQUFDNUQsYUFBSyxHQUFDMUIsQ0FBQyxDQUFDMEIsS0FBRCxDQUFELENBQVNnQyxNQUFULENBQWdCLFlBQVU7QUFBQyxpQkFBTSxDQUFDMUQsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRMkQsSUFBUixDQUFhZ0IsV0FBYixDQUFQO0FBQWtDLFNBQTdELENBQU47QUFBc0U7QUFBQzs7QUFDekYsYUFBU2dCLFdBQVQsQ0FBcUJuQixPQUFyQixFQUE2QkMsR0FBN0IsRUFBaUNMLFNBQWpDLEVBQTJDb0IsWUFBM0MsRUFBd0Q7QUFBQyxRQUFFM0Qsa0JBQUY7O0FBQXFCLFVBQUkrRCxjQUFhLEdBQUMseUJBQVU7QUFBQ0Msd0JBQWdCLENBQUMsU0FBRCxFQUFXckIsT0FBWCxDQUFoQjs7QUFBb0NzQix1QkFBZTs7QUFBR0Ysc0JBQWEsR0FBQzVGLENBQUMsQ0FBQytGLElBQWhCO0FBQXNCLE9BQXpHOztBQUEwR0Ysc0JBQWdCLENBQUMsWUFBRCxFQUFjckIsT0FBZCxDQUFoQjs7QUFBdUMsVUFBSXdCLFlBQVksR0FBQ2pGLE1BQU0sQ0FBQzhELFNBQXhCO0FBQUEsVUFBa0NSLGVBQWUsR0FBQ3RELE1BQU0sQ0FBQ3NELGVBQXpEO0FBQUEsVUFBeUU0QixjQUFjLEdBQUNsRixNQUFNLENBQUNrRixjQUEvRjtBQUFBLFVBQThHQyxlQUFlLEdBQUNuRixNQUFNLENBQUNtRixlQUFySTtBQUFBLFVBQXFKQyxlQUFlLEdBQUNwRixNQUFNLENBQUNvRixlQUE1SztBQUFBLFVBQTRMdkMsVUFBVSxHQUFDN0MsTUFBTSxDQUFDNkMsVUFBOU07QUFBQSxVQUF5TndDLGFBQWEsR0FBQzVCLE9BQU8sQ0FBQ0ksSUFBUixDQUFhc0IsZUFBYixDQUF2Tzs7QUFBcVEsVUFBR1YsWUFBSCxFQUFnQjtBQUFDLFlBQUlhLGFBQVksR0FBQyx3QkFBVTtBQUFDLGNBQUdGLGVBQUgsRUFBbUI7QUFBQzNCLG1CQUFPLENBQUM4QixVQUFSLENBQW1CdkYsTUFBTSxDQUFDdUQsZUFBMUI7QUFBNEM7O0FBQ2psQkUsaUJBQU8sQ0FBQ2IsSUFBUixDQUFhQyxVQUFiLEVBQXdCLElBQXhCOztBQUE4QmlDLDBCQUFnQixDQUFDNUQsVUFBRCxFQUFZdUMsT0FBWixDQUFoQjs7QUFBcUMzQixvQkFBVSxDQUFDaUQsZUFBRCxFQUFpQixDQUFqQixDQUFWO0FBQThCTyx1QkFBWSxHQUFDckcsQ0FBQyxDQUFDK0YsSUFBZjtBQUFxQixTQUQrWDs7QUFDOVh2QixlQUFPLENBQUMrQixHQUFSLENBQVlwRSxNQUFaLEVBQW9CcUUsR0FBcEIsQ0FBd0JyRSxNQUF4QixFQUErQnlELGNBQS9CLEVBQThDWSxHQUE5QyxDQUFrRHRFLEtBQWxELEVBQXdEbUUsYUFBeEQ7QUFBc0UsWUFBRyxDQUFDUixnQkFBZ0IsQ0FBQ0wsWUFBRCxFQUFjaEIsT0FBZCxFQUFzQixVQUFTaUMsUUFBVCxFQUFrQjtBQUFDLGNBQUdBLFFBQUgsRUFBWTtBQUFDakMsbUJBQU8sQ0FBQytCLEdBQVIsQ0FBWXJFLEtBQVo7O0FBQW1CbUUseUJBQVk7QUFBSSxXQUFoRCxNQUN0UDtBQUFDN0IsbUJBQU8sQ0FBQytCLEdBQVIsQ0FBWXBFLE1BQVo7O0FBQW9CeUQsMEJBQWE7QUFBSTtBQUFDLFNBRHNLLENBQXBCLEVBQ2hKcEIsT0FBTyxDQUFDa0MsT0FBUixDQUFnQnZFLE1BQWhCO0FBQXlCLE9BRjhaLE1BR2hlO0FBQUMsWUFBSXdFLFFBQVEsR0FBQzNHLENBQUMsQ0FBQyxJQUFJNEcsS0FBSixFQUFELENBQWQ7QUFBNEJELGdCQUFRLENBQUNILEdBQVQsQ0FBYXJFLE1BQWIsRUFBb0J5RCxjQUFwQixFQUFtQ1ksR0FBbkMsQ0FBdUN0RSxLQUF2QyxFQUE2QyxZQUFVO0FBQUNzQyxpQkFBTyxDQUFDcUMsSUFBUjs7QUFBZSxjQUFHcEMsR0FBRyxLQUFHckMsSUFBVCxFQUFjO0FBQUNvQyxtQkFBTyxDQUFDSSxJQUFSLENBQWFyQyxNQUFiLEVBQW9Cb0UsUUFBUSxDQUFDL0IsSUFBVCxDQUFjckMsTUFBZCxDQUFwQixFQUEyQ3FDLElBQTNDLENBQWdEdEMsT0FBaEQsRUFBd0RxRSxRQUFRLENBQUMvQixJQUFULENBQWN0QyxPQUFkLENBQXhELEVBQWdGc0MsSUFBaEYsQ0FBcUZ2QyxJQUFyRixFQUEwRnNFLFFBQVEsQ0FBQy9CLElBQVQsQ0FBY3ZDLElBQWQsQ0FBMUY7QUFBZ0gsV0FBL0gsTUFDcEc7QUFBQ21DLG1CQUFPLENBQUNVLEdBQVIsQ0FBWTFDLGdCQUFaLEVBQTZCLFVBQVFtRSxRQUFRLENBQUMvQixJQUFULENBQWN2QyxJQUFkLENBQVIsR0FBNEIsSUFBekQ7QUFBZ0U7O0FBQ3JFbUMsaUJBQU8sQ0FBQ3pELE1BQU0sQ0FBQytGLE1BQVIsQ0FBUCxDQUF1Qi9GLE1BQU0sQ0FBQ2dHLFVBQTlCOztBQUEwQyxjQUFHWixlQUFILEVBQW1CO0FBQUMzQixtQkFBTyxDQUFDOEIsVUFBUixDQUFtQk4sWUFBWSxHQUFDLEdBQWIsR0FBaUIzQixlQUFqQixHQUFpQyxHQUFqQyxHQUFxQzZCLGVBQXJDLEdBQXFELEdBQXJELEdBQXlEbkYsTUFBTSxDQUFDaUUsa0JBQW5GOztBQUF1RyxnQkFBR2lCLGNBQWMsS0FBRzFELE1BQXBCLEVBQTJCO0FBQUNpQyxxQkFBTyxDQUFDOEIsVUFBUixDQUFtQkwsY0FBbkI7QUFBb0M7QUFBQzs7QUFDdE96QixpQkFBTyxDQUFDYixJQUFSLENBQWFDLFVBQWIsRUFBd0IsSUFBeEI7O0FBQThCaUMsMEJBQWdCLENBQUM1RCxVQUFELEVBQVl1QyxPQUFaLENBQWhCOztBQUFxQ21DLGtCQUFRLENBQUNLLE1BQVQ7O0FBQWtCbEIseUJBQWU7QUFBSSxTQUh2RTtBQUd5RSxZQUFJbUIsUUFBUSxHQUFDLENBQUNqRixnQkFBZ0IsSUFBRW9FLGFBQWxCLEdBQWdDQSxhQUFoQyxHQUE4QzVCLE9BQU8sQ0FBQ0ksSUFBUixDQUFhb0IsWUFBYixDQUEvQyxLQUE0RSxFQUF6RjtBQUE0RlcsZ0JBQVEsQ0FBQy9CLElBQVQsQ0FBY3JDLE1BQWQsRUFBcUJpQyxPQUFPLENBQUNJLElBQVIsQ0FBYXFCLGNBQWIsQ0FBckIsRUFBbURyQixJQUFuRCxDQUF3RHRDLE9BQXhELEVBQWdFa0MsT0FBTyxDQUFDSSxJQUFSLENBQWFQLGVBQWIsQ0FBaEUsRUFBK0ZPLElBQS9GLENBQW9HdkMsSUFBcEcsRUFBeUc0RSxRQUFRLEdBQUM3QyxTQUFTLEdBQUM2QyxRQUFYLEdBQW9CLElBQXJJO0FBQTJJTixnQkFBUSxDQUFDTyxRQUFULElBQW1CUCxRQUFRLENBQUNELE9BQVQsQ0FBaUJ4RSxLQUFqQixDQUFuQjtBQUE0QztBQUFDOztBQUM5WCxhQUFTcUQsaUJBQVQsQ0FBMkJmLE9BQTNCLEVBQW1DO0FBQUMsVUFBSTJDLFlBQVksR0FBQzNDLE9BQU8sQ0FBQzRDLHFCQUFSLEVBQWpCO0FBQUEsVUFBaURDLFNBQVMsR0FBQ3RHLE1BQU0sQ0FBQ3VHLGVBQWxFO0FBQUEsVUFBa0ZDLFNBQVMsR0FBQ3hHLE1BQU0sQ0FBQ3dHLFNBQW5HO0FBQUEsVUFBNkdDLFFBQVEsR0FBR0MsZ0JBQWdCLEtBQUdGLFNBQXBCLEdBQStCSixZQUFZLENBQUNPLEdBQTdDLElBQW9ELENBQUNILFNBQUQsR0FBV0osWUFBWSxDQUFDUSxNQUFsTTtBQUFBLFVBQTBNQyxVQUFVLEdBQUdDLGVBQWUsS0FBR04sU0FBbkIsR0FBOEJKLFlBQVksQ0FBQ1csSUFBNUMsSUFBb0QsQ0FBQ1AsU0FBRCxHQUFXSixZQUFZLENBQUNZLEtBQWpTOztBQUF3UyxVQUFHVixTQUFTLEtBQUcsVUFBZixFQUEwQjtBQUFDLGVBQU9HLFFBQVA7QUFBaUIsT0FBNUMsTUFDdlUsSUFBR0gsU0FBUyxLQUFHLFlBQWYsRUFBNEI7QUFBQyxlQUFPTyxVQUFQO0FBQW1COztBQUNyRCxhQUFPSixRQUFRLElBQUVJLFVBQWpCO0FBQTZCOztBQUM3QixhQUFTQyxlQUFULEdBQTBCO0FBQUMsYUFBTy9GLFlBQVksSUFBRSxDQUFkLEdBQWdCQSxZQUFoQixHQUE4QkEsWUFBWSxHQUFDOUIsQ0FBQyxDQUFDRixNQUFELENBQUQsQ0FBVWtJLEtBQVYsRUFBbEQ7QUFBc0U7O0FBQ2pHLGFBQVNQLGdCQUFULEdBQTJCO0FBQUMsYUFBTzFGLGFBQWEsSUFBRSxDQUFmLEdBQWlCQSxhQUFqQixHQUFnQ0EsYUFBYSxHQUFDL0IsQ0FBQyxDQUFDRixNQUFELENBQUQsQ0FBVW1JLE1BQVYsRUFBckQ7QUFBMEU7O0FBQ3RHLGFBQVN2RCxrQkFBVCxDQUE0QkYsT0FBNUIsRUFBb0M7QUFBQyxhQUFPQSxPQUFPLENBQUMwRCxPQUFSLENBQWdCQyxXQUFoQixFQUFQO0FBQXNDOztBQUMzRSxhQUFTbEQsbUJBQVQsQ0FBNkJtRCxNQUE3QixFQUFvQ2hFLFNBQXBDLEVBQThDO0FBQUMsVUFBR0EsU0FBSCxFQUFhO0FBQUMsWUFBSWlFLE9BQU8sR0FBQ0QsTUFBTSxDQUFDRSxLQUFQLENBQWEsR0FBYixDQUFaO0FBQThCRixjQUFNLEdBQUMsRUFBUDs7QUFBVSxhQUFJLElBQUlqSCxDQUFDLEdBQUMsQ0FBTixFQUFRQyxDQUFDLEdBQUNpSCxPQUFPLENBQUNoSCxNQUF0QixFQUE2QkYsQ0FBQyxHQUFDQyxDQUEvQixFQUFpQ0QsQ0FBQyxFQUFsQyxFQUFxQztBQUFDaUgsZ0JBQU0sSUFBRWhFLFNBQVMsR0FBQ2lFLE9BQU8sQ0FBQ2xILENBQUQsQ0FBUCxDQUFXb0gsSUFBWCxFQUFWLElBQTZCcEgsQ0FBQyxLQUFHQyxDQUFDLEdBQUMsQ0FBTixHQUFRLEdBQVIsR0FBWSxFQUF6QyxDQUFSO0FBQXNEO0FBQUM7O0FBQ2xNLGFBQU9nSCxNQUFQO0FBQWU7O0FBQ2YsYUFBU25GLFNBQVQsQ0FBbUJMLEtBQW5CLEVBQXlCNEYsUUFBekIsRUFBa0M7QUFBQyxVQUFJQyxPQUFKO0FBQUEsVUFBWUMsV0FBVyxHQUFDLENBQXhCO0FBQTBCLGFBQU8sVUFBU3ZGLEtBQVQsRUFBZXdGLGNBQWYsRUFBOEI7QUFBQyxZQUFJQyxPQUFPLEdBQUMsQ0FBQyxJQUFJQyxJQUFKLEVBQUQsR0FBWUgsV0FBeEI7O0FBQW9DLGlCQUFTSSxHQUFULEdBQWM7QUFBQ0oscUJBQVcsR0FBQyxDQUFDLElBQUlHLElBQUosRUFBYjtBQUF3Qkwsa0JBQVEsQ0FBQ08sSUFBVCxDQUFjdEgsUUFBZCxFQUF1QjBCLEtBQXZCO0FBQStCOztBQUM3TXNGLGVBQU8sSUFBRU8sWUFBWSxDQUFDUCxPQUFELENBQXJCOztBQUErQixZQUFHRyxPQUFPLEdBQUNoRyxLQUFSLElBQWUsQ0FBQzdCLE1BQU0sQ0FBQ2tJLGNBQXZCLElBQXVDTixjQUExQyxFQUF5RDtBQUFDRyxhQUFHO0FBQUksU0FBakUsTUFDM0I7QUFBQ0wsaUJBQU8sR0FBQzVGLFVBQVUsQ0FBQ2lHLEdBQUQsRUFBS2xHLEtBQUssR0FBQ2dHLE9BQVgsQ0FBbEI7QUFBdUM7QUFBQyxPQUZnQjtBQUVkOztBQUMvQyxhQUFTOUMsZUFBVCxHQUEwQjtBQUFDLFFBQUVqRSxrQkFBRjs7QUFBcUIsVUFBRyxDQUFDSCxLQUFLLENBQUNMLE1BQVAsSUFBZSxDQUFDUSxrQkFBbkIsRUFBc0M7QUFBQ2dFLHdCQUFnQixDQUFDLGVBQUQsQ0FBaEI7QUFBbUM7QUFBQzs7QUFDM0gsYUFBU0EsZ0JBQVQsQ0FBMEIyQyxRQUExQixFQUFtQ2hFLE9BQW5DLEVBQTJDMEUsSUFBM0MsRUFBZ0Q7QUFBQyxVQUFJVixRQUFRLEdBQUN6SCxNQUFNLENBQUN5SCxRQUFELENBQW5CLEVBQStCO0FBQUNBLGdCQUFRLENBQUNoRixLQUFULENBQWUvQixRQUFmLEVBQXdCLEdBQUcwSCxLQUFILENBQVNKLElBQVQsQ0FBY0ssU0FBZCxFQUF3QixDQUF4QixDQUF4QjtBQUFvRCxlQUFPLElBQVA7QUFBYTs7QUFDbEosYUFBTyxLQUFQO0FBQWM7O0FBQ2QsUUFBR3JJLE1BQU0sQ0FBQ3NJLElBQVAsS0FBYyxPQUFkLElBQXVCakosWUFBMUIsRUFBdUM7QUFBQ3FDLGlCQUFXO0FBQUksS0FBdkQsTUFDSTtBQUFDekMsT0FBQyxDQUFDRixNQUFELENBQUQsQ0FBVW1FLEVBQVYsQ0FBYS9CLEtBQUssR0FBQyxHQUFOLEdBQVVOLFNBQXZCLEVBQWlDYSxXQUFqQztBQUErQztBQUFDOztBQUNyRCxXQUFTaEMsVUFBVCxDQUFvQkUsUUFBcEIsRUFBNkJILFFBQTdCLEVBQXNDO0FBQUMsUUFBSThJLFNBQVMsR0FBQyxJQUFkO0FBQUEsUUFBbUJDLE9BQU8sR0FBQ3ZKLENBQUMsQ0FBQ3dKLE1BQUYsQ0FBUyxFQUFULEVBQVlGLFNBQVMsQ0FBQ3ZJLE1BQXRCLEVBQTZCUCxRQUE3QixDQUEzQjtBQUFBLFFBQWtFaUosT0FBTyxHQUFDLEVBQTFFO0FBQUEsUUFBNkVDLFVBQVUsR0FBQ0gsT0FBTyxDQUFDekUsSUFBUixHQUFhLEdBQWIsR0FBa0IsRUFBRTNFLGNBQTVHOztBQUE0SG1KLGFBQVMsQ0FBQ3ZJLE1BQVYsR0FBaUIsVUFBUzRJLFNBQVQsRUFBbUJDLEtBQW5CLEVBQXlCO0FBQUMsVUFBR0EsS0FBSyxLQUFHN0osU0FBWCxFQUFxQjtBQUFDLGVBQU93SixPQUFPLENBQUNJLFNBQUQsQ0FBZDtBQUEyQjs7QUFDL1BKLGFBQU8sQ0FBQ0ksU0FBRCxDQUFQLEdBQW1CQyxLQUFuQjtBQUF5QixhQUFPTixTQUFQO0FBQWtCLEtBRHdIOztBQUN2SEEsYUFBUyxDQUFDTyxRQUFWLEdBQW1CLFVBQVNuSSxLQUFULEVBQWU7QUFBQytILGFBQU8sQ0FBQ2xJLENBQVIsSUFBV2tJLE9BQU8sQ0FBQ2xJLENBQVIsQ0FBVXZCLENBQUMsQ0FBQ29ELElBQUYsQ0FBTzFCLEtBQVAsTUFBZ0IsUUFBaEIsR0FBeUIxQixDQUFDLENBQUMwQixLQUFELENBQTFCLEdBQWtDQSxLQUE1QyxDQUFYO0FBQThELGFBQU80SCxTQUFQO0FBQWtCLEtBQW5IOztBQUFvSEEsYUFBUyxDQUFDUSxRQUFWLEdBQW1CLFlBQVU7QUFBQyxhQUFPTCxPQUFPLENBQUNoRyxDQUFSLEdBQVVnRyxPQUFPLENBQUNoRyxDQUFSLEVBQVYsR0FBc0IsRUFBN0I7QUFBaUMsS0FBL0Q7O0FBQWdFNkYsYUFBUyxDQUFDUyxNQUFWLEdBQWlCLFVBQVNDLFdBQVQsRUFBcUI7QUFBQ1AsYUFBTyxDQUFDekcsQ0FBUixJQUFXeUcsT0FBTyxDQUFDekcsQ0FBUixDQUFVLEVBQVYsRUFBYSxDQUFDZ0gsV0FBZCxDQUFYO0FBQXNDLGFBQU9WLFNBQVA7QUFBa0IsS0FBL0Y7O0FBQWdHQSxhQUFTLENBQUNXLEtBQVYsR0FBZ0IsVUFBU3ZJLEtBQVQsRUFBZTtBQUFDK0gsYUFBTyxDQUFDNUYsQ0FBUixJQUFXNEYsT0FBTyxDQUFDNUYsQ0FBUixDQUFVN0QsQ0FBQyxDQUFDb0QsSUFBRixDQUFPMUIsS0FBUCxNQUFnQixRQUFoQixHQUF5QjFCLENBQUMsQ0FBQzBCLEtBQUQsQ0FBMUIsR0FBa0NBLEtBQTVDLENBQVg7QUFBOEQsYUFBTzRILFNBQVA7QUFBa0IsS0FBaEg7O0FBQWlIQSxhQUFTLENBQUNZLE9BQVYsR0FBa0IsWUFBVTtBQUFDVCxhQUFPLENBQUN6RyxDQUFSLElBQVd5RyxPQUFPLENBQUN6RyxDQUFSLENBQVU7QUFBQ0ssV0FBRyxFQUFDO0FBQUwsT0FBVixFQUFxQixJQUFyQixDQUFYO0FBQXNDLGFBQU9pRyxTQUFQO0FBQWtCLEtBQXJGOztBQUFzRkEsYUFBUyxDQUFDakUsT0FBVixHQUFrQixZQUFVO0FBQUNyRixPQUFDLENBQUN1SixPQUFPLENBQUN2RixZQUFULENBQUQsQ0FBd0J1QyxHQUF4QixDQUE0QixNQUFJbUQsVUFBaEMsRUFBMkNELE9BQU8sQ0FBQ3pHLENBQW5EO0FBQXNEaEQsT0FBQyxDQUFDRixNQUFELENBQUQsQ0FBVXlHLEdBQVYsQ0FBYyxNQUFJbUQsVUFBbEI7QUFBOEJELGFBQU8sR0FBQyxFQUFSO0FBQVcsYUFBTzFKLFNBQVA7QUFBa0IsS0FBOUk7O0FBQStJeUIsZ0JBQVksQ0FBQzhILFNBQUQsRUFBV0MsT0FBWCxFQUFtQjVJLFFBQW5CLEVBQTRCOEksT0FBNUIsRUFBb0NDLFVBQXBDLENBQVo7O0FBQTRELFdBQU9ILE9BQU8sQ0FBQ1ksU0FBUixHQUFrQnhKLFFBQWxCLEdBQTJCMkksU0FBbEM7QUFBNkM7O0FBQy92QjdJLFlBQVUsQ0FBQ08sU0FBWCxDQUFxQkQsTUFBckIsR0FBNEI7QUFBQytELFFBQUksRUFBQyxNQUFOO0FBQWFxRixhQUFTLEVBQUMsSUFBdkI7QUFBNEIvRSxlQUFXLEVBQUMsSUFBeEM7QUFBNkNpRSxRQUFJLEVBQUMsTUFBbEQ7QUFBeUQ5QixhQUFTLEVBQUMsR0FBbkU7QUFBdUU5QixlQUFXLEVBQUMsS0FBbkY7QUFBeUZ6QixnQkFBWSxFQUFDbEUsTUFBdEc7QUFBNkd3SCxtQkFBZSxFQUFDLE1BQTdIO0FBQW9JbEQsYUFBUyxFQUFDLElBQTlJO0FBQW1KRixnQkFBWSxFQUFDLG9GQUFoSztBQUFxUEMsZUFBVyxFQUFDLElBQWpRO0FBQXNRdkIsU0FBSyxFQUFDLENBQUMsQ0FBN1E7QUFBK1FHLFlBQVEsRUFBQyxLQUF4UjtBQUE4UjhCLGFBQVMsRUFBQyxVQUF4UztBQUFtVFIsbUJBQWUsRUFBQyxhQUFuVTtBQUFpVjRCLGtCQUFjLEVBQUMsWUFBaFc7QUFBNldDLG1CQUFlLEVBQUMsYUFBN1g7QUFBMlk1QixtQkFBZSxFQUFDLGFBQTNaO0FBQXlhVSxzQkFBa0IsRUFBQyxnQkFBNWI7QUFBNmNtQixtQkFBZSxFQUFDLElBQTdkO0FBQWtleEIsZUFBVyxFQUFDLFNBQTllO0FBQXdmZixjQUFVLEVBQUMsUUFBbmdCO0FBQTRnQmtELFVBQU0sRUFBQyxNQUFuaEI7QUFBMGhCQyxjQUFVLEVBQUMsQ0FBcmlCO0FBQXVpQmtDLGtCQUFjLEVBQUMsSUFBdGpCO0FBQTJqQi9GLFlBQVEsRUFBQyxHQUFwa0I7QUFBd2tCa0gsY0FBVSxFQUFDckssU0FBbmxCO0FBQTZsQnNLLGFBQVMsRUFBQ3RLLFNBQXZtQjtBQUFpbkJ1SyxXQUFPLEVBQUN2SyxTQUF6bkI7QUFBbW9Cd0ssaUJBQWEsRUFBQ3hLO0FBQWpwQixHQUE1QjtBQUF3ckJDLEdBQUMsQ0FBQ0YsTUFBRCxDQUFELENBQVVtRSxFQUFWLENBQWEsTUFBYixFQUFvQixZQUFVO0FBQUM3RCxnQkFBWSxHQUFDLElBQWI7QUFBbUIsR0FBbEQ7QUFBcUQsQ0F6Qzd1QixFQXlDK3VCTixNQXpDL3VCOztBQXlDdXZCOztBQUFFLGFBQVU7QUFBQyxNQUFJMEssR0FBRyxHQUFDMUssTUFBUjtBQUFBLE1BQWUySyxRQUFRLEdBQUMsQ0FBeEI7QUFBMEJELEtBQUcsQ0FBQ0UscUJBQUosR0FBMEJGLEdBQUcsQ0FBQ0UscUJBQUosSUFBMkJGLEdBQUcsQ0FBQ0csMkJBQXpEOztBQUFxRixNQUFHLENBQUNILEdBQUcsQ0FBQ0UscUJBQVIsRUFBOEI7QUFBQ0YsT0FBRyxDQUFDRSxxQkFBSixHQUEwQixVQUFTbEMsUUFBVCxFQUFrQjtBQUFDLFVBQUlvQyxRQUFRLEdBQUMsSUFBSS9CLElBQUosR0FBV2dDLE9BQVgsRUFBYjtBQUFBLFVBQWtDQyxVQUFVLEdBQUNDLElBQUksQ0FBQ0MsR0FBTCxDQUFTLENBQVQsRUFBVyxNQUFJSixRQUFRLEdBQUNILFFBQWIsQ0FBWCxDQUE3QztBQUFBLFVBQWdGUSxFQUFFLEdBQUNwSSxVQUFVLENBQUMyRixRQUFELEVBQVVzQyxVQUFWLENBQTdGO0FBQW1ITCxjQUFRLEdBQUNHLFFBQVEsR0FBQ0UsVUFBbEI7QUFBNkIsYUFBT0csRUFBUDtBQUFXLEtBQXhNO0FBQTBNOztBQUM1bEMsTUFBRyxDQUFDVCxHQUFHLENBQUNVLG9CQUFSLEVBQTZCO0FBQUNWLE9BQUcsQ0FBQ1Usb0JBQUosR0FBeUIsVUFBU0QsRUFBVCxFQUFZO0FBQUNqQyxrQkFBWSxDQUFDaUMsRUFBRCxDQUFaO0FBQWtCLEtBQXhEO0FBQTBEO0FBQUMsQ0FEZ3FCLEdBQUQ7O0FBQzFwQixXQUFTRSxJQUFULEVBQWNDLE9BQWQsRUFBc0I7QUFBQyxNQUFHLElBQUgsRUFBMEM7QUFBQ0MsMkNBQThCRCxPQUF4Qiw0akJBQU47QUFBd0MsR0FBbkYsTUFBd0YsRUFBMEc7QUFBQyxDQUExTixFQUEyTixJQUEzTixFQUFnTyxZQUFVO0FBQUM7O0FBQWEsTUFBSUUsS0FBSyxHQUFDLEVBQVY7QUFBYSxNQUFJQyxPQUFPLEdBQUN6TCxNQUFNLENBQUMwTCxPQUFuQjs7QUFBMkJGLE9BQUssQ0FBQ0csS0FBTixHQUFZLFVBQVNDLE9BQVQsRUFBaUI7QUFBQyxRQUFHLE9BQU9ILE9BQVAsS0FBaUIsV0FBcEIsRUFBZ0M7QUFBQ0EsYUFBTyxDQUFDRSxLQUFSLENBQWNDLE9BQWQ7QUFBd0I7QUFBQyxHQUF4Rjs7QUFBeUZKLE9BQUssQ0FBQzlCLE1BQU4sR0FBYSxVQUFTbUMsT0FBVCxFQUFpQkMsUUFBakIsRUFBMEI7QUFBQyxRQUFHRCxPQUFILEVBQVc7QUFBQyxVQUFHLFNBQU9BLE9BQVAsTUFBaUIsUUFBcEIsRUFBNkI7QUFBQyxhQUFLRixLQUFMLENBQVcsa0NBQVg7QUFBZ0QsT0FBOUUsTUFBa0Y7QUFBQyxhQUFJLElBQUlJLElBQVIsSUFBZ0JELFFBQWhCLEVBQXlCO0FBQUMsY0FBR0EsUUFBUSxDQUFDRSxjQUFULENBQXdCRCxJQUF4QixLQUErQkYsT0FBTyxDQUFDRyxjQUFSLENBQXVCRCxJQUF2QixDQUFsQyxFQUErRDtBQUFDRCxvQkFBUSxDQUFDQyxJQUFELENBQVIsR0FBZUYsT0FBTyxDQUFDRSxJQUFELENBQXRCO0FBQThCO0FBQUM7QUFBQztBQUFDOztBQUN6dEIsV0FBT0QsUUFBUDtBQUFpQixHQURzYzs7QUFDcmNOLE9BQUssQ0FBQ08sSUFBTixHQUFXLFVBQVNBLElBQVQsRUFBYztBQUFDLFFBQUlFLEVBQUUsR0FBQyxLQUFLQyxRQUFMLEVBQVA7QUFBQSxRQUF1QkMsUUFBUSxHQUFDLENBQUMsRUFBRCxFQUFJLFFBQUosRUFBYSxLQUFiLEVBQW1CLElBQW5CLEVBQXdCLEdBQXhCLENBQWhDOztBQUE2RCxTQUFJLElBQUlDLENBQUMsR0FBQyxDQUFOLEVBQVFDLEVBQUUsR0FBQ0YsUUFBUSxDQUFDNUssTUFBeEIsRUFBK0I2SyxDQUFDLEdBQUNDLEVBQWpDLEVBQW9DRCxDQUFDLEVBQXJDLEVBQXdDO0FBQUMsVUFBSUUsWUFBWSxHQUFDSCxRQUFRLENBQUNDLENBQUQsQ0FBUixHQUFZRCxRQUFRLENBQUNDLENBQUQsQ0FBUixHQUFZTCxJQUFJLENBQUNRLE1BQUwsQ0FBWSxDQUFaLEVBQWVDLFdBQWYsRUFBWixHQUF5Q1QsSUFBSSxDQUFDMUMsS0FBTCxDQUFXLENBQVgsQ0FBckQsR0FBbUUwQyxJQUFwRjs7QUFBeUYsVUFBR0UsRUFBRSxDQUFDUSxLQUFILENBQVNILFlBQVQsTUFBeUJyTSxTQUE1QixFQUFzQztBQUFDLGVBQU9xTSxZQUFQO0FBQXFCO0FBQUM7O0FBQ3hTLFdBQU0sRUFBTjtBQUFVLEdBRFE7O0FBQ1BkLE9BQUssQ0FBQ2tCLFdBQU4sR0FBa0IsVUFBU0MsR0FBVCxFQUFhO0FBQUMsUUFBSUMsSUFBSSxHQUFDLEVBQVQ7O0FBQVksU0FBSSxJQUFJOUgsSUFBUixJQUFnQjZILEdBQWhCLEVBQW9CO0FBQUMsVUFBR0EsR0FBRyxDQUFDWCxjQUFKLENBQW1CbEgsSUFBbkIsQ0FBSCxFQUE0QjtBQUFDOEgsWUFBSSxDQUFDOUgsSUFBRCxDQUFKLEdBQVc2SCxHQUFHLENBQUM3SCxJQUFELENBQWQ7QUFBc0I7QUFBQzs7QUFDaEksV0FBTzhILElBQVA7QUFBYSxHQURGOztBQUNHcEIsT0FBSyxDQUFDVSxRQUFOLEdBQWUsVUFBU3ZILEdBQVQsRUFBYWtJLE9BQWIsRUFBcUI7QUFBQyxRQUFJWixFQUFFLEdBQUNhLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QnBJLEdBQUcsSUFBRSxLQUE1QixDQUFQOztBQUEwQyxRQUFHa0ksT0FBSCxFQUFXO0FBQUNaLFFBQUUsQ0FBQ2UsU0FBSCxHQUFhSCxPQUFiO0FBQXNCOztBQUMvSCxXQUFPWixFQUFQO0FBQVcsR0FERzs7QUFDRlQsT0FBSyxDQUFDeUIsUUFBTixHQUFlLFVBQVNDLE1BQVQsRUFBZ0I7QUFBQyxXQUFPQSxNQUFNLENBQUNDLE9BQVAsQ0FBZSxXQUFmLEVBQTJCLFVBQVN4SixDQUFULEVBQVc7QUFBQyxhQUFPQSxDQUFDLENBQUMsQ0FBRCxDQUFELENBQUs2SSxXQUFMLEVBQVA7QUFBMkIsS0FBbEUsQ0FBUDtBQUE0RSxHQUE1Rzs7QUFBNkdoQixPQUFLLENBQUM0QixZQUFOLEdBQW1CLFVBQVNDLEtBQVQsRUFBZXBCLEVBQWYsRUFBa0I1SSxLQUFsQixFQUF3QjlDLEVBQXhCLEVBQTJCK00sTUFBM0IsRUFBa0M7QUFBQyxRQUFHLFNBQU8sS0FBS0MsY0FBWixNQUE2QixRQUFoQyxFQUF5QztBQUFDLFdBQUtBLGNBQUwsR0FBb0IsRUFBcEI7QUFBd0I7O0FBQ2pQLFFBQUcsQ0FBQyxLQUFLQSxjQUFMLENBQW9CaE4sRUFBcEIsQ0FBSixFQUE0QjtBQUFDLFdBQUtnTixjQUFMLENBQW9CaE4sRUFBcEIsSUFBd0I4TSxLQUFLLENBQUM5TSxFQUFELENBQUwsQ0FBVWdKLElBQVYsQ0FBZThELEtBQWYsQ0FBeEI7QUFBK0M7O0FBQzVFQyxVQUFNLEdBQUNBLE1BQU0sS0FBR3JOLFNBQVQsR0FBbUIsSUFBbkIsR0FBd0IsQ0FBQyxDQUFDcU4sTUFBakM7QUFBd0MsUUFBSUUsVUFBVSxHQUFDRixNQUFNLEdBQUMsa0JBQUQsR0FBb0IscUJBQXpDO0FBQStEakssU0FBSyxDQUFDb0ssT0FBTixDQUFjLFVBQVNDLEVBQVQsRUFBWTtBQUFDekIsUUFBRSxDQUFDdUIsVUFBRCxDQUFGLENBQWVFLEVBQWYsRUFBa0IsS0FBS0gsY0FBTCxDQUFvQmhOLEVBQXBCLENBQWxCLEVBQTBDLEtBQTFDO0FBQWtELEtBQS9ELENBQWdFZ0osSUFBaEUsQ0FBcUUsSUFBckUsQ0FBZDtBQUEyRixHQUZ6RTs7QUFFMEVpQyxPQUFLLENBQUNtQyxhQUFOLEdBQW9CLFVBQVNOLEtBQVQsRUFBZXZMLFNBQWYsRUFBeUJ3QixJQUF6QixFQUE4QkQsS0FBOUIsRUFBb0MrRixJQUFwQyxFQUF5QztBQUFDOUYsUUFBSSxJQUFFeEIsU0FBUyxHQUFDLE1BQUlBLFNBQUwsR0FBZSxFQUE5QjtBQUFpQyxRQUFJOEwsUUFBUSxHQUFDdkssS0FBSyxHQUFDLENBQUNBLEtBQUQsRUFBUXdLLE1BQVIsQ0FBZXpFLElBQWYsQ0FBRCxHQUFzQixDQUFDQSxJQUFELENBQXhDOztBQUErQ2lFLFNBQUssQ0FBQ1MsU0FBTixDQUFnQnhLLElBQWhCLEVBQXFCc0ssUUFBckI7QUFBZ0MsR0FBOUs7O0FBQStLcEMsT0FBSyxDQUFDcEksUUFBTixHQUFlLFVBQVMySyxJQUFULEVBQWNqTCxLQUFkLEVBQW9CO0FBQUMsUUFBSWtMLFNBQVMsR0FBQyxJQUFkO0FBQUEsUUFBbUJDLEtBQUssR0FBQ25MLEtBQXpCO0FBQStCLFdBQU8sWUFBVTtBQUFDLFVBQUlvTCxJQUFJLEdBQUMsSUFBVDtBQUFBLFVBQWM5RSxJQUFJLEdBQUNFLFNBQW5CO0FBQUEsVUFBNkI2RSxHQUFHLEdBQUNwRixJQUFJLENBQUNvRixHQUFMLEVBQWpDOztBQUE0QyxVQUFHLENBQUNILFNBQUQsSUFBWUcsR0FBRyxHQUFDSCxTQUFKLElBQWVDLEtBQTlCLEVBQW9DO0FBQUNELGlCQUFTLEdBQUNHLEdBQVY7QUFBY0osWUFBSSxDQUFDckssS0FBTCxDQUFXd0ssSUFBWCxFQUFnQjlFLElBQWhCO0FBQXVCO0FBQUMsS0FBekk7QUFBMkksR0FBOU07O0FBQStNb0MsT0FBSyxDQUFDNEMsTUFBTixHQUFhLFVBQVM3TSxNQUFULEVBQWdCOE0sS0FBaEIsRUFBc0I7QUFBQyxXQUFNLENBQUM5TSxNQUFNLEdBQUU4TSxLQUFLLEdBQUM5TSxNQUFmLElBQXdCQSxNQUE5QjtBQUFzQyxHQUExRTs7QUFBMkVpSyxPQUFLLENBQUM4QyxRQUFOLEdBQWUsVUFBU3RCLFNBQVQsRUFBbUI7QUFBQyxXQUFPLElBQUl1QixNQUFKLENBQVcsYUFBV3ZCLFNBQVgsR0FBcUIsVUFBaEMsQ0FBUDtBQUFvRCxHQUF2Rjs7QUFBd0Z4QixPQUFLLENBQUNnRCxRQUFOLEdBQWUsVUFBU3ZDLEVBQVQsRUFBWWUsU0FBWixFQUFzQjtBQUFDLFdBQU0sQ0FBQyxDQUFDZixFQUFFLENBQUNlLFNBQUgsQ0FBYXlCLEtBQWIsQ0FBbUIsS0FBS0gsUUFBTCxDQUFjdEIsU0FBZCxDQUFuQixDQUFSO0FBQXNELEdBQTVGOztBQUE2RnhCLE9BQUssQ0FBQ2tELFFBQU4sR0FBZSxVQUFTekMsRUFBVCxFQUFZZSxTQUFaLEVBQXNCO0FBQUMsUUFBRyxDQUFDLEtBQUt3QixRQUFMLENBQWN2QyxFQUFkLEVBQWlCZSxTQUFqQixDQUFKLEVBQWdDO0FBQUNmLFFBQUUsQ0FBQ2UsU0FBSCxJQUFjLENBQUNmLEVBQUUsQ0FBQ2UsU0FBSCxHQUFhLEdBQWIsR0FBaUIsRUFBbEIsSUFBc0JBLFNBQXBDO0FBQStDO0FBQUMsR0FBdkg7O0FBQXdIeEIsT0FBSyxDQUFDbUQsV0FBTixHQUFrQixVQUFTMUMsRUFBVCxFQUFZZSxTQUFaLEVBQXNCO0FBQUMsUUFBRyxLQUFLd0IsUUFBTCxDQUFjdkMsRUFBZCxFQUFpQmUsU0FBakIsQ0FBSCxFQUErQjtBQUFDZixRQUFFLENBQUNlLFNBQUgsR0FBYWYsRUFBRSxDQUFDZSxTQUFILENBQWFHLE9BQWIsQ0FBcUIsS0FBS21CLFFBQUwsQ0FBY3RCLFNBQWQsQ0FBckIsRUFBOEMsR0FBOUMsRUFBbURHLE9BQW5ELENBQTJELE1BQTNELEVBQWtFLEVBQWxFLENBQWI7QUFBb0Y7QUFBQyxHQUE5Sjs7QUFBK0ozQixPQUFLLENBQUNvRCxTQUFOLEdBQWdCLFVBQVMzQyxFQUFULEVBQVk0QyxDQUFaLEVBQWNDLENBQWQsRUFBZ0JDLENBQWhCLEVBQWtCO0FBQUMsUUFBSUMsS0FBSyxHQUFDRCxDQUFDLEdBQUMsWUFBVUEsQ0FBVixHQUFZLEdBQVosR0FBZ0JBLENBQWhCLEdBQWtCLEdBQW5CLEdBQXVCLEVBQWxDO0FBQXFDOUMsTUFBRSxDQUFDUSxLQUFILENBQVMsS0FBS3dDLE9BQUwsQ0FBYUMsS0FBdEIsSUFBOEIsS0FBS0QsT0FBTCxDQUFhRSxHQUFkLEdBQW1CLGtCQUFnQk4sQ0FBQyxJQUFFLENBQW5CLElBQXNCLE1BQXRCLElBQThCQyxDQUFDLElBQUUsQ0FBakMsSUFBb0MsUUFBcEMsR0FBNkNFLEtBQWhFLEdBQXNFLGdCQUFjSCxDQUFDLElBQUUsQ0FBakIsSUFBb0IsTUFBcEIsSUFBNEJDLENBQUMsSUFBRSxDQUEvQixJQUFrQyxLQUFsQyxHQUF3Q0UsS0FBM0k7QUFBa0osR0FBMU47O0FBQTJOeEQsT0FBSyxDQUFDeUQsT0FBTixHQUFjO0FBQUNDLFNBQUssRUFBQzFELEtBQUssQ0FBQ08sSUFBTixDQUFXLFdBQVgsQ0FBUDtBQUErQm9ELE9BQUcsRUFBQzNELEtBQUssQ0FBQ08sSUFBTixDQUFXLGFBQVgsSUFBMEIsSUFBMUIsR0FBK0I7QUFBbEUsR0FBZDtBQUF1RixTQUFPUCxLQUFQO0FBQWMsQ0FOMXpDLENBQUQ7O0FBTSt6QyxXQUFTSCxJQUFULEVBQWNDLE9BQWQsRUFBc0I7QUFBQyxNQUFHLElBQUgsRUFBMEM7QUFBQ0MsMkNBQThCRCxPQUF4Qiw0akJBQU47QUFBd0MsR0FBbkYsTUFBd0YsRUFBMEc7QUFBQyxDQUExTixFQUEyTixPQUFPdEwsTUFBUCxLQUFnQixXQUFoQixHQUE0QkEsTUFBNUIsR0FBbUMsSUFBOVAsRUFBbVEsWUFBVTtBQUFDOztBQUFhLE1BQUlvUCxTQUFTLEdBQUMsU0FBVkEsU0FBVSxHQUFVLENBQUUsQ0FBMUI7QUFBQSxNQUEyQkMsS0FBSyxHQUFDRCxTQUFTLENBQUNsTyxTQUEzQzs7QUFBcURtTyxPQUFLLENBQUNsTCxFQUFOLEdBQVMsVUFBU21MLFNBQVQsRUFBbUJDLFFBQW5CLEVBQTRCO0FBQUMsUUFBRyxDQUFDRCxTQUFELElBQVksQ0FBQ0MsUUFBaEIsRUFBeUI7QUFBQyxhQUFPLElBQVA7QUFBYTs7QUFDenpELFFBQUkxTixNQUFNLEdBQUMsS0FBSzhILE9BQUwsR0FBYSxLQUFLQSxPQUFMLElBQWMsRUFBdEM7QUFBeUMsUUFBSTZGLFNBQVMsR0FBQzNOLE1BQU0sQ0FBQ3lOLFNBQUQsQ0FBTixHQUFrQnpOLE1BQU0sQ0FBQ3lOLFNBQUQsQ0FBTixJQUFtQixFQUFuRDs7QUFBc0QsUUFBR0UsU0FBUyxDQUFDQyxPQUFWLENBQWtCRixRQUFsQixNQUE4QixDQUFDLENBQWxDLEVBQW9DO0FBQUNDLGVBQVMsQ0FBQy9MLElBQVYsQ0FBZThMLFFBQWY7QUFBMEI7O0FBQzlKLFdBQU8sSUFBUDtBQUFhLEdBRit0RDs7QUFFOXRERixPQUFLLENBQUM1SSxHQUFOLEdBQVUsVUFBUzZJLFNBQVQsRUFBbUJDLFFBQW5CLEVBQTRCO0FBQUMsUUFBSUMsU0FBUyxHQUFDLEtBQUs3RixPQUFMLElBQWMsS0FBS0EsT0FBTCxDQUFhMkYsU0FBYixDQUE1Qjs7QUFBb0QsUUFBRyxDQUFDRSxTQUFELElBQVksQ0FBQ0EsU0FBUyxDQUFDak8sTUFBMUIsRUFBaUM7QUFBQyxhQUFPLElBQVA7QUFBYTs7QUFDeEosUUFBSThNLEtBQUssR0FBQ21CLFNBQVMsQ0FBQ0MsT0FBVixDQUFrQkYsUUFBbEIsQ0FBVjs7QUFBc0MsUUFBR2xCLEtBQUssS0FBRyxDQUFDLENBQVosRUFBYztBQUFDbUIsZUFBUyxDQUFDRSxNQUFWLENBQWlCckIsS0FBakIsRUFBdUIsQ0FBdkI7QUFBMkI7O0FBQ2hGLFdBQU8sSUFBUDtBQUFhLEdBRkM7O0FBRUFnQixPQUFLLENBQUN2QixTQUFOLEdBQWdCLFVBQVN3QixTQUFULEVBQW1CbEcsSUFBbkIsRUFBd0I7QUFBQyxRQUFJb0csU0FBUyxHQUFDLEtBQUs3RixPQUFMLElBQWMsS0FBS0EsT0FBTCxDQUFhMkYsU0FBYixDQUE1Qjs7QUFBb0QsUUFBRyxDQUFDRSxTQUFELElBQVksQ0FBQ0EsU0FBUyxDQUFDak8sTUFBMUIsRUFBaUM7QUFBQyxhQUFPLElBQVA7QUFBYTs7QUFDMUosUUFBSUYsQ0FBQyxHQUFDLENBQU47QUFBQSxRQUFRa08sUUFBUSxHQUFDQyxTQUFTLENBQUNuTyxDQUFELENBQTFCO0FBQThCK0gsUUFBSSxHQUFDQSxJQUFJLElBQUUsRUFBWDtBQUFjLFFBQUl1RyxhQUFhLEdBQUMsS0FBS0MsV0FBTCxJQUFrQixLQUFLQSxXQUFMLENBQWlCTixTQUFqQixDQUFwQzs7QUFBZ0UsV0FBTUMsUUFBTixFQUFlO0FBQUMsVUFBSU0sTUFBTSxHQUFDRixhQUFhLElBQUVBLGFBQWEsQ0FBQ0osUUFBRCxDQUF2Qzs7QUFBa0QsVUFBR00sTUFBSCxFQUFVO0FBQUMsYUFBS3BKLEdBQUwsQ0FBUzZJLFNBQVQsRUFBbUJDLFFBQW5CO0FBQTZCLGVBQU9JLGFBQWEsQ0FBQ0osUUFBRCxDQUFwQjtBQUFnQzs7QUFDdFBBLGNBQVEsQ0FBQzdMLEtBQVQsQ0FBZSxJQUFmLEVBQW9CMEYsSUFBcEI7QUFBMEIvSCxPQUFDLElBQUV3TyxNQUFNLEdBQUMsQ0FBRCxHQUFHLENBQVo7QUFBY04sY0FBUSxHQUFDQyxTQUFTLENBQUNuTyxDQUFELENBQWxCO0FBQXVCOztBQUMvRCxXQUFPLElBQVA7QUFBYSxHQUhDOztBQUdBLFNBQU8rTixTQUFQO0FBQWtCLENBUDQzQyxDQUFEOztBQU92M0MsV0FBUy9ELElBQVQsRUFBY0MsT0FBZCxFQUFzQjtBQUFDLE1BQUcsSUFBSCxFQUEwQztBQUFDQyxxQ0FBa0MsQ0FBQywwQkFBRCxFQUF3QiwwQkFBeEIsQ0FBNUIsb0NBQTRFRCxPQUE1RTtBQUFBO0FBQUEsb0hBQU47QUFBNEYsR0FBdkksTUFBNEksRUFBb007QUFBQyxDQUF4VyxFQUF5VyxJQUF6VyxFQUE4VyxVQUFTRSxLQUFULEVBQWU0RCxTQUFmLEVBQXlCO0FBQUM7O0FBQWEsTUFBSVUsT0FBTyxHQUFDLFNBQVJBLE9BQVEsQ0FBU3BMLE9BQVQsRUFBaUJxTCxTQUFqQixFQUEyQkMsUUFBM0IsRUFBb0NDLFVBQXBDLEVBQStDO0FBQUMsU0FBS3ZMLE9BQUwsR0FBYUEsT0FBYjtBQUFxQixTQUFLb0gsUUFBTCxHQUFjaUUsU0FBZDtBQUF3QixTQUFLRyxNQUFMLEdBQVk7QUFBQ0YsY0FBUSxFQUFDQSxRQUFRLElBQUUsSUFBcEI7QUFBeUJDLGdCQUFVLEVBQUNBLFVBQVUsSUFBRTtBQUFoRCxLQUFaO0FBQW1FLFNBQUtFLFlBQUw7QUFBcUIsR0FBak07O0FBQWtNLE1BQUlkLEtBQUssR0FBQ1MsT0FBTyxDQUFDNU8sU0FBUixHQUFrQmtQLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjakIsU0FBUyxDQUFDbE8sU0FBeEIsQ0FBNUI7O0FBQStEbU8sT0FBSyxDQUFDaUIsVUFBTixHQUFpQixVQUFTM0QsR0FBVCxFQUFhO0FBQUMsU0FBSzRELElBQUwsR0FBVSxJQUFWO0FBQWUsU0FBS0MsSUFBTCxHQUFVN0QsR0FBVjtBQUFlLEdBQTdEOztBQUE4RDBDLE9BQUssQ0FBQ29CLFdBQU4sR0FBa0IsWUFBVTtBQUFDLFNBQUtGLElBQUwsR0FBVSxLQUFWO0FBQWlCLEdBQTlDOztBQUErQ2xCLE9BQUssQ0FBQ3FCLFNBQU4sR0FBZ0IsVUFBUy9ELEdBQVQsRUFBYTtBQUFDLFNBQUtzRCxVQUFMLEdBQWdCdEQsR0FBaEI7QUFBcUIsR0FBbkQ7O0FBQW9EMEMsT0FBSyxDQUFDc0IsWUFBTixHQUFtQixZQUFVO0FBQUMsU0FBS0osSUFBTCxHQUFVLElBQVY7QUFBZSxTQUFLSyxNQUFMLEdBQVksS0FBWjtBQUFrQixTQUFLQyxhQUFMLEdBQW1CLENBQW5COztBQUFxQixRQUFHLENBQUMsS0FBS0MsR0FBVCxFQUFhO0FBQUMsV0FBS0MsT0FBTDtBQUFnQjtBQUFDLEdBQW5IOztBQUFvSDFCLE9BQUssQ0FBQzJCLFdBQU4sR0FBa0IsWUFBVTtBQUFDLFNBQUtULElBQUwsR0FBVSxLQUFWO0FBQWdCLFNBQUtNLGFBQUwsR0FBbUIsQ0FBbkI7O0FBQXFCLFFBQUcsS0FBS0MsR0FBUixFQUFZO0FBQUMxRiwwQkFBb0IsQ0FBQyxLQUFLMEYsR0FBTixDQUFwQjtBQUErQixXQUFLQSxHQUFMLEdBQVMsS0FBVDtBQUFnQjs7QUFDN2tDLFNBQUtHLEtBQUwsR0FBV3pGLEtBQUssQ0FBQ2tCLFdBQU4sQ0FBa0IsS0FBS3dFLFFBQXZCLENBQVg7QUFBNEMsU0FBS0MsUUFBTCxHQUFjO0FBQUN0QyxPQUFDLEVBQUMsQ0FBSDtBQUFLQyxPQUFDLEVBQUMsQ0FBUDtBQUFTQyxPQUFDLEVBQUM7QUFBWCxLQUFkO0FBQTZCLEdBRHM0Qjs7QUFDcjRCTSxPQUFLLENBQUNjLFlBQU4sR0FBbUIsWUFBVTtBQUFDLFNBQUthLFdBQUw7QUFBbUIsU0FBS0osTUFBTCxHQUFZLElBQVo7QUFBaUIsU0FBS0osSUFBTCxHQUFVaEYsS0FBSyxDQUFDa0IsV0FBTixDQUFrQixLQUFLWixRQUF2QixDQUFWO0FBQTJDLFNBQUttRixLQUFMLEdBQVd6RixLQUFLLENBQUNrQixXQUFOLENBQWtCLEtBQUtaLFFBQXZCLENBQVg7QUFBNEMsU0FBS3NGLE9BQUwsR0FBYTVGLEtBQUssQ0FBQ2tCLFdBQU4sQ0FBa0IsS0FBS1osUUFBdkIsQ0FBYjtBQUE4QyxTQUFLb0YsUUFBTCxHQUFjMUYsS0FBSyxDQUFDa0IsV0FBTixDQUFrQixLQUFLWixRQUF2QixDQUFkO0FBQStDLFNBQUttRSxVQUFMLEdBQWdCekUsS0FBSyxDQUFDa0IsV0FBTixDQUFrQixLQUFLWixRQUF2QixDQUFoQjtBQUFrRCxHQUF4Uzs7QUFBeVN1RCxPQUFLLENBQUMwQixPQUFOLEdBQWMsWUFBVTtBQUFDLFFBQUlNLElBQUksR0FBRSxZQUFVO0FBQUMsVUFBRyxPQUFPLEtBQUtILFFBQVosS0FBdUIsV0FBMUIsRUFBc0M7QUFBQyxZQUFJSSxRQUFRLEdBQUM5RixLQUFLLENBQUNrQixXQUFOLENBQWtCLEtBQUt3RSxRQUF2QixDQUFiO0FBQThDLGFBQUtLLGNBQUw7QUFBc0IsYUFBS0Msb0JBQUw7QUFBNEJoRyxhQUFLLENBQUNtQyxhQUFOLENBQW9CLElBQXBCLEVBQXlCLFdBQXpCLEVBQXFDLFFBQXJDLEVBQThDLElBQTlDO0FBQW9ELGFBQUs4RCxnQkFBTDtBQUF3QixhQUFLQyxrQkFBTDtBQUEwQixhQUFLQyxNQUFMLENBQVksR0FBWjtBQUFpQixhQUFLYixHQUFMLEdBQVNsRyxxQkFBcUIsQ0FBQ3lHLElBQUQsQ0FBOUI7QUFBcUMsYUFBS08sV0FBTCxDQUFpQk4sUUFBakI7QUFBNEI7QUFBQyxLQUE1VSxDQUE4VS9ILElBQTlVLENBQW1WLElBQW5WLENBQVQ7O0FBQWtXLFNBQUt1SCxHQUFMLEdBQVNsRyxxQkFBcUIsQ0FBQ3lHLElBQUQsQ0FBOUI7QUFBc0MsR0FBamE7O0FBQWthaEMsT0FBSyxDQUFDb0MsZ0JBQU4sR0FBdUIsWUFBVTtBQUFDLFNBQUksSUFBSUksQ0FBUixJQUFhLEtBQUtYLFFBQWxCLEVBQTJCO0FBQUMsVUFBRyxPQUFPLEtBQUtBLFFBQUwsQ0FBY1csQ0FBZCxDQUFQLEtBQTBCLFdBQTdCLEVBQXlDO0FBQUMsYUFBS1gsUUFBTCxDQUFjVyxDQUFkLEtBQWtCLEtBQUtWLFFBQUwsQ0FBY1UsQ0FBZCxDQUFsQjtBQUFtQyxhQUFLWCxRQUFMLENBQWNXLENBQWQsSUFBa0JBLENBQUMsS0FBRyxHQUFMLEdBQVU1RyxJQUFJLENBQUNDLEdBQUwsQ0FBUyxHQUFULEVBQWEsS0FBS2dHLFFBQUwsQ0FBY1csQ0FBZCxDQUFiLENBQVYsR0FBeUMsS0FBS1gsUUFBTCxDQUFjVyxDQUFkLENBQTFEO0FBQTJFLGFBQUtWLFFBQUwsQ0FBY1UsQ0FBZCxLQUFrQixLQUFLQyxpQkFBTCxFQUFsQjtBQUE0QztBQUFDO0FBQUMsR0FBcFE7O0FBQXFRekMsT0FBSyxDQUFDa0MsY0FBTixHQUFxQixZQUFVO0FBQUMsUUFBRyxLQUFLaEIsSUFBUixFQUFhO0FBQUMsV0FBSSxJQUFJc0IsQ0FBUixJQUFhLEtBQUtyQixJQUFsQixFQUF1QjtBQUFDLFlBQUcsT0FBTyxLQUFLQSxJQUFMLENBQVVxQixDQUFWLENBQVAsS0FBc0IsV0FBekIsRUFBcUM7QUFBQyxjQUFJRSxZQUFZLEdBQUMsS0FBS3ZCLElBQUwsQ0FBVXFCLENBQVYsSUFBYSxLQUFLWCxRQUFMLENBQWNXLENBQWQsQ0FBOUI7QUFBK0MsY0FBSUcsU0FBUyxHQUFDRCxZQUFZLEdBQUMsS0FBS1osUUFBTCxDQUFjVSxDQUFkLENBQTNCO0FBQTRDLGVBQUtJLFVBQUwsQ0FBZ0JKLENBQWhCLEVBQWtCRyxTQUFsQjtBQUE4QjtBQUFDO0FBQUM7QUFBQyxHQUF4Tzs7QUFBeU8zQyxPQUFLLENBQUNtQyxvQkFBTixHQUEyQixZQUFVO0FBQUMsUUFBRyxDQUFDLEtBQUtqQixJQUFULEVBQWM7QUFBQyxXQUFJLElBQUlzQixDQUFSLElBQWEsS0FBSzVCLFVBQWxCLEVBQTZCO0FBQUMsWUFBRyxPQUFPLEtBQUtBLFVBQUwsQ0FBZ0I0QixDQUFoQixDQUFQLEtBQTRCLFdBQS9CLEVBQTJDO0FBQUMsY0FBSUssUUFBUSxHQUFDLEtBQUtqQyxVQUFMLENBQWdCNEIsQ0FBaEIsSUFBbUIsS0FBS1gsUUFBTCxDQUFjVyxDQUFkLENBQWhDO0FBQWlELGNBQUkxSCxLQUFLLEdBQUMrSCxRQUFRLEdBQUMsS0FBS2hDLE1BQUwsQ0FBWUQsVUFBL0I7QUFBMEMsZUFBS2dDLFVBQUwsQ0FBZ0JKLENBQWhCLEVBQWtCMUgsS0FBbEI7QUFBMEI7QUFBQztBQUFDO0FBQUMsR0FBdlA7O0FBQXdQa0YsT0FBSyxDQUFDcUMsa0JBQU4sR0FBeUIsWUFBVTtBQUFDLFNBQUksSUFBSUcsQ0FBUixJQUFhLEtBQUtYLFFBQWxCLEVBQTJCO0FBQUMsVUFBRyxPQUFPLEtBQUtBLFFBQUwsQ0FBY1csQ0FBZCxDQUFQLEtBQTBCLFdBQTdCLEVBQXlDO0FBQUMsYUFBS1QsT0FBTCxDQUFhUyxDQUFiLElBQWdCLEtBQUtYLFFBQUwsQ0FBY1csQ0FBZCxJQUFpQixLQUFLVixRQUFMLENBQWNVLENBQWQsS0FBa0IsSUFBRSxLQUFLQyxpQkFBTCxFQUFwQixDQUFqQztBQUFnRjtBQUFDO0FBQUMsR0FBNUw7O0FBQTZMekMsT0FBSyxDQUFDNEMsVUFBTixHQUFpQixVQUFTMUssU0FBVCxFQUFtQjRDLEtBQW5CLEVBQXlCO0FBQUMsU0FBS2dILFFBQUwsQ0FBYzVKLFNBQWQsS0FBMEI0QyxLQUExQjtBQUFpQyxHQUE1RTs7QUFBNkVrRixPQUFLLENBQUN5QyxpQkFBTixHQUF3QixZQUFVO0FBQUMsV0FBTyxJQUFFLEtBQUs1QixNQUFMLENBQVlGLFFBQXJCO0FBQStCLEdBQWxFOztBQUFtRVgsT0FBSyxDQUFDOEMsV0FBTixHQUFrQixVQUFTQyxNQUFULEVBQWdCQyxLQUFoQixFQUFzQjtBQUFDLFNBQUksSUFBSVIsQ0FBUixJQUFhTyxNQUFiLEVBQW9CO0FBQUMsVUFBRyxPQUFPQSxNQUFNLENBQUNQLENBQUQsQ0FBYixLQUFtQixXQUF0QixFQUFrQztBQUFDUSxhQUFLLEdBQUNSLENBQUMsS0FBRyxHQUFKLEdBQVFRLEtBQUssR0FBQyxHQUFkLEdBQWtCQSxLQUF4QjtBQUE4QkQsY0FBTSxDQUFDUCxDQUFELENBQU4sR0FBVTVHLElBQUksQ0FBQ29ILEtBQUwsQ0FBV0QsTUFBTSxDQUFDUCxDQUFELENBQU4sR0FBVVEsS0FBckIsSUFBNEJBLEtBQXRDO0FBQTZDO0FBQUM7QUFBQyxHQUE5Szs7QUFBK0toRCxPQUFLLENBQUN1QyxXQUFOLEdBQWtCLFVBQVNOLFFBQVQsRUFBa0I7QUFBQyxRQUFHLENBQUMsS0FBS2YsSUFBVCxFQUFjO0FBQUMsVUFBSStCLEtBQUssR0FBQyxDQUFWOztBQUFZLFdBQUksSUFBSVQsQ0FBUixJQUFhLEtBQUtYLFFBQWxCLEVBQTJCO0FBQUMsWUFBRyxPQUFPLEtBQUtBLFFBQUwsQ0FBY1csQ0FBZCxDQUFQLEtBQTBCLFdBQTdCLEVBQXlDO0FBQUMsY0FBSVEsS0FBSyxHQUFDUixDQUFDLEtBQUcsR0FBSixHQUFRLEtBQVIsR0FBYyxHQUF4Qjs7QUFBNEIsY0FBRzVHLElBQUksQ0FBQ29ILEtBQUwsQ0FBVyxLQUFLbkIsUUFBTCxDQUFjVyxDQUFkLElBQWlCUSxLQUE1QixNQUFxQ3BILElBQUksQ0FBQ29ILEtBQUwsQ0FBV2YsUUFBUSxDQUFDTyxDQUFELENBQVIsR0FBWVEsS0FBdkIsQ0FBeEMsRUFBc0U7QUFBQ0MsaUJBQUs7O0FBQUcsZ0JBQUdBLEtBQUssS0FBR2xDLE1BQU0sQ0FBQ21DLElBQVAsQ0FBWSxLQUFLckIsUUFBakIsRUFBMkIzUCxNQUF0QyxFQUE2QztBQUFDLG1CQUFLc1AsYUFBTDtBQUFzQjtBQUFDO0FBQUM7QUFBQztBQUFDOztBQUNoekUsUUFBRyxLQUFLQSxhQUFMLEdBQW1CLENBQXRCLEVBQXdCO0FBQUMsV0FBS0csV0FBTDtBQUFtQixXQUFLVyxNQUFMLENBQVksS0FBS1QsUUFBTCxDQUFjbkMsQ0FBZCxHQUFnQixDQUFoQixHQUFrQixFQUFsQixHQUFxQixDQUFqQztBQUFvQyxXQUFLNkIsTUFBTCxHQUFZLElBQVo7O0FBQWlCLFVBQUc0QixJQUFJLENBQUNDLFNBQUwsQ0FBZSxLQUFLeEIsS0FBcEIsTUFBNkJ1QixJQUFJLENBQUNDLFNBQUwsQ0FBZSxLQUFLdkIsUUFBcEIsQ0FBaEMsRUFBOEQ7QUFBQzFGLGFBQUssQ0FBQ21DLGFBQU4sQ0FBb0IsSUFBcEIsRUFBeUIsV0FBekIsRUFBcUMsUUFBckMsRUFBOEMsSUFBOUM7QUFBcUQ7QUFBQztBQUFDLEdBRGd5RDs7QUFDL3hEMEIsT0FBSyxDQUFDc0MsTUFBTixHQUFhLFVBQVNVLEtBQVQsRUFBZTtBQUFDLFNBQUtGLFdBQUwsQ0FBaUIsS0FBS2pCLFFBQXRCLEVBQStCbUIsS0FBL0I7QUFBc0M3RyxTQUFLLENBQUNvRCxTQUFOLENBQWdCLEtBQUtsSyxPQUFyQixFQUE2QixLQUFLd00sUUFBTCxDQUFjckMsQ0FBM0MsRUFBNkMsS0FBS3FDLFFBQUwsQ0FBY3BDLENBQTNELEVBQTZELEtBQUtvQyxRQUFMLENBQWNuQyxDQUEzRTtBQUErRSxHQUFsSjs7QUFBbUosU0FBT2UsT0FBUDtBQUFnQixDQUZ2VixDQUFEOztBQUU0VixXQUFTekUsSUFBVCxFQUFjQyxPQUFkLEVBQXNCO0FBQUMsTUFBRyxJQUFILEVBQTBDO0FBQUNDLHFDQUFPLENBQUMsMEJBQUQsRUFBd0IsMEJBQXhCLEVBQStDLDBCQUEvQyxDQUFELG9DQUE0RUQsT0FBNUU7QUFBQTtBQUFBO0FBQUEsb0dBQU47QUFBNEYsR0FBdkksTUFBNEksRUFBZ1A7QUFBQyxDQUFwWixFQUFxWixJQUFyWixFQUEwWixVQUFTRSxLQUFULEVBQWU0RCxTQUFmLEVBQXlCVSxPQUF6QixFQUFpQztBQUFDOztBQUFhLE1BQUk0QyxPQUFPLEdBQUMsT0FBWjtBQUFvQixNQUFJQyxJQUFJLEdBQUMsQ0FBVDtBQUFXLE1BQUlDLFNBQVMsR0FBQyxFQUFkO0FBQWlCLE1BQUlDLE9BQU8sR0FBQyxTQUFPLENBQUNILE9BQU8sR0FBQ3pILElBQUksQ0FBQzZILE1BQUwsRUFBVCxFQUF3QjNGLE9BQXhCLENBQWdDLEtBQWhDLEVBQXNDLEVBQXRDLENBQW5CO0FBQTZELE1BQUk0RixLQUFLLEdBQUM7QUFBQ0MsT0FBRyxFQUFDO0FBQUwsR0FBVjtBQUFrQixNQUFJbEgsUUFBUSxHQUFDO0FBQUNtSCxpQkFBYSxFQUFDLE9BQWY7QUFBdUJ4TCxhQUFTLEVBQUMsQ0FBakM7QUFBbUN3SSxjQUFVLEVBQUM7QUFBQ2lELFlBQU0sRUFBQyxLQUFSO0FBQWNDLFdBQUssRUFBQyxLQUFwQjtBQUEwQkMsWUFBTSxFQUFDO0FBQWpDLEtBQTlDO0FBQXNGcEQsWUFBUSxFQUFDO0FBQUNrRCxZQUFNLEVBQUMsSUFBUjtBQUFhQyxXQUFLLEVBQUMsSUFBbkI7QUFBd0JDLFlBQU0sRUFBQztBQUEvQixLQUEvRjtBQUFvSUMsZUFBVyxFQUFDLEtBQWhKO0FBQXNKaEMsUUFBSSxFQUFDLENBQTNKO0FBQTZKaUMsV0FBTyxFQUFDLENBQXJLO0FBQXVLQyxVQUFNLEVBQUMsS0FBOUs7QUFBb0xDLGNBQVUsRUFBQyxJQUEvTDtBQUFvTUMsV0FBTyxFQUFDLEtBQTVNO0FBQWtOQyxjQUFVLEVBQUMsSUFBN047QUFBa09DLGVBQVcsRUFBQyxJQUE5TztBQUFtUEMsYUFBUyxFQUFDLElBQTdQO0FBQWtRQyxnQkFBWSxFQUFDLEtBQS9RO0FBQXFSQyxZQUFRLEVBQUMsQ0FBQyxPQUFELENBQTlSO0FBQXdTQyxZQUFRLEVBQUMsSUFBalQ7QUFBc1RDLGlCQUFhLEVBQUMsS0FBcFU7QUFBMFVDLGtCQUFjLEVBQUMsbUJBQXpWO0FBQTZXQyxXQUFPLEVBQUMsSUFBclg7QUFBMFhDLGVBQVcsRUFBQyxLQUF0WTtBQUE0WUMsc0JBQWtCLEVBQUMsSUFBL1o7QUFBb2FDLGNBQVUsRUFBQyxJQUEvYTtBQUFvYkMsaUJBQWEsRUFBQyxPQUFsYztBQUEwY0Msa0JBQWMsRUFBQztBQUFDLFlBQUs7QUFBQ3JNLGFBQUssRUFBQyxHQUFQO0FBQVdDLGNBQU0sRUFBQyxFQUFsQjtBQUFxQnFNLGNBQU0sRUFBQztBQUE1QixPQUFOO0FBQXNDLFlBQUs7QUFBQ3RNLGFBQUssRUFBQyxFQUFQO0FBQVVDLGNBQU0sRUFBQyxFQUFqQjtBQUFvQnFNLGNBQU0sRUFBQztBQUEzQixPQUEzQztBQUEwRSxXQUFJO0FBQUN0TSxhQUFLLEVBQUMsRUFBUDtBQUFVQyxjQUFNLEVBQUMsRUFBakI7QUFBb0JxTSxjQUFNLEVBQUM7QUFBM0IsT0FBOUU7QUFBNEcsV0FBSTtBQUFDdE0sYUFBSyxFQUFDLEVBQVA7QUFBVUMsY0FBTSxFQUFDLEVBQWpCO0FBQW9CcU0sY0FBTSxFQUFDO0FBQTNCO0FBQWhILEtBQXpkO0FBQXdtQkMsV0FBTyxFQUFDLEdBQWhuQjtBQUFvbkJDLGVBQVcsRUFBQyxJQUFob0I7QUFBcW9CQyxZQUFRLEVBQUMsS0FBOW9CO0FBQW9wQkMsYUFBUyxFQUFDLHNEQUE5cEI7QUFBcXRCQyxhQUFTLEVBQUMsOEJBQS90QjtBQUE4dkJDLGVBQVcsRUFBQyxJQUExd0I7QUFBK3dCQyxlQUFXLEVBQUMsS0FBM3hCO0FBQWl5QkMscUJBQWlCLEVBQUMsRUFBbnpCO0FBQXN6QkMsVUFBTSxFQUFDLE1BQTd6QjtBQUFvMEJDLFdBQU8sRUFBQyxHQUE1MEI7QUFBZzFCQyxXQUFPLEVBQUMsQ0FBeDFCO0FBQTAxQkMsbUJBQWUsRUFBQyxJQUExMkI7QUFBKzJCQyxnQkFBWSxFQUFDLEtBQTUzQjtBQUFrNEJDLGVBQVcsRUFBQyxJQUE5NEI7QUFBbTVCQyxpQkFBYSxFQUFDLElBQWo2QjtBQUFzNkJDLGlCQUFhLEVBQUMsS0FBcDdCO0FBQTA3QkMsZ0JBQVksRUFBQyxJQUF2OEI7QUFBNDhCQyxlQUFXLEVBQUMsSUFBeDlCO0FBQTY5QkMsY0FBVSxFQUFDLElBQXgrQjtBQUE2K0JDLGdCQUFZLEVBQUMsQ0FBQyxVQUFELEVBQVksWUFBWixFQUF5QixTQUF6QixFQUFtQyxXQUFuQyxFQUErQyxVQUEvQyxFQUEwRCxRQUExRCxDQUExL0I7QUFBOGpDQyxhQUFTLEVBQUMsVUFBeGtDO0FBQW1sQ0MsYUFBUyxFQUFDLFVBQTdsQztBQUF3bUNDLHFCQUFpQixFQUFDLElBQTFuQztBQUErbkNDLHFCQUFpQixFQUFDLEtBQWpwQztBQUF1cENDLHFCQUFpQixFQUFDLEtBQXpxQztBQUErcUNDLGNBQVUsRUFBQyxJQUExckM7QUFBK3JDQyxnQkFBWSxFQUFDLHdCQUE1c0M7QUFBcXVDQyxtQkFBZSxFQUFDLHdCQUFydkM7QUFBOHdDQyxnQkFBWSxFQUFDLEtBQTN4QztBQUFpeUNDLGNBQVUsRUFBQyxLQUFHLENBQS95QztBQUFpekNDLGlCQUFhLEVBQUMsSUFBL3pDO0FBQW8wQ0MsaUJBQWEsRUFBQyxLQUFsMUM7QUFBdzFDQyxrQkFBYyxFQUFDO0FBQXYyQyxHQUFiOztBQUEyM0MsTUFBSUMsU0FBUyxHQUFDLFNBQVZBLFNBQVUsQ0FBUzdLLE9BQVQsRUFBaUI7QUFBQyxRQUFJbkgsT0FBTyxHQUFDb0ksUUFBUSxDQUFDNkosYUFBVCxDQUF1QixjQUF2QixDQUFaOztBQUFtRCxRQUFHalMsT0FBTyxJQUFFQSxPQUFPLENBQUNpTyxJQUFwQixFQUF5QjtBQUFDLGFBQU9DLFNBQVMsQ0FBQ2xPLE9BQU8sQ0FBQ2lPLElBQVQsQ0FBaEI7QUFBZ0M7O0FBQy84RSxTQUFLOUcsT0FBTCxHQUFhTCxLQUFLLENBQUM5QixNQUFOLENBQWFtQyxPQUFiLEVBQXFCQyxRQUFyQixDQUFiO0FBQTRDLFNBQUs4SyxNQUFMO0FBQWUsR0FEdXdFOztBQUN0d0UsTUFBSXZILEtBQUssR0FBQ3FILFNBQVMsQ0FBQ3hWLFNBQVYsR0FBb0JrUCxNQUFNLENBQUNDLE1BQVAsQ0FBY2pCLFNBQVMsQ0FBQ2xPLFNBQXhCLENBQTlCOztBQUFpRW1PLE9BQUssQ0FBQ3dILElBQU4sR0FBVyxZQUFVO0FBQUMsUUFBRyxLQUFLbEUsSUFBUixFQUFhO0FBQUM7QUFBUTs7QUFDekssU0FBS21FLFNBQUw7QUFBaUIsU0FBS25FLElBQUwsR0FBVSxFQUFFQSxJQUFaO0FBQWlCLFNBQUtvRSxHQUFMLENBQVNDLE1BQVQsQ0FBZ0JyRSxJQUFoQixHQUFxQkEsSUFBckI7QUFBMEJDLGFBQVMsQ0FBQyxLQUFLRCxJQUFOLENBQVQsR0FBcUIsSUFBckI7QUFBMEIsU0FBS3NFLFlBQUw7QUFBb0IsU0FBS0MsWUFBTDtBQUFvQixTQUFLQyxhQUFMO0FBQXNCLEdBRHZCOztBQUN3QjlILE9BQUssQ0FBQ3VILE1BQU4sR0FBYSxZQUFVO0FBQUMsUUFBSWxNLEdBQUcsR0FBQzFLLE1BQVI7QUFBQSxRQUFlb1gsR0FBRyxHQUFDdEssUUFBbkI7QUFBQSxRQUE0QnVLLEdBQUcsR0FBQ0MsU0FBaEM7QUFBMEMsU0FBS0MsR0FBTCxHQUFTLE1BQVQ7QUFBZ0IsU0FBS0MsT0FBTCxHQUFhLEVBQWI7QUFBZ0IsU0FBS0MsT0FBTCxHQUFhLEVBQWI7QUFBZ0IsU0FBS3ZFLE1BQUwsR0FBWSxFQUFaO0FBQWUsU0FBS3dFLE1BQUwsR0FBWSxFQUFaO0FBQWUsU0FBS0MsS0FBTCxHQUFXLEVBQVg7QUFBYyxTQUFLQyxNQUFMLEdBQVksRUFBWjtBQUFlLFNBQUtDLFFBQUwsR0FBYyxFQUFkO0FBQWlCLFNBQUtoRixPQUFMLEdBQWFBLE9BQWI7QUFBcUIsU0FBS0UsS0FBTCxHQUFXQSxLQUFYO0FBQWlCLFNBQUsrRSxVQUFMLEdBQWdCLEtBQUtDLG1CQUFMLEVBQWhCO0FBQTJDLFNBQUs5SSxPQUFMLEdBQWE7QUFBQytJLGlCQUFXLEVBQUUsa0JBQWlCdE4sR0FBbEIsSUFBeUIyTSxHQUFHLENBQUNZLGNBQUosR0FBbUIsQ0FBNUMsSUFBaURaLEdBQUcsQ0FBQ2EsZ0JBQUosR0FBcUIsQ0FBbkY7QUFBc0ZDLGVBQVMsRUFBQyxhQUFZek4sR0FBWixJQUFpQixlQUFjK0ksT0FBL0g7QUFBdUkyRSxnQkFBVSxFQUFDLEtBQUtDLGdCQUFMLEVBQWxKO0FBQTBLM0UsZ0JBQVUsRUFBQyxhQUFZMEQsR0FBRyxDQUFDckssYUFBSixDQUFrQixLQUFsQixDQUFaLEdBQXFDLE9BQXJDLEdBQTZDcUssR0FBRyxDQUFDa0IsWUFBSixLQUFtQnJZLFNBQW5CLEdBQTZCLFlBQTdCLEdBQTBDO0FBQTVRLEtBQWI7QUFBMlMsU0FBS3NZLFdBQUwsR0FBaUIsS0FBS0EsV0FBTCxFQUFqQjtBQUFvQyxTQUFLQyxXQUFMLEdBQWlCLEtBQUtBLFdBQUwsRUFBakI7QUFBcUMsR0FBbm9COztBQUFvb0JuSixPQUFLLENBQUMwSSxtQkFBTixHQUEwQixZQUFVO0FBQUMsUUFBSVYsR0FBRyxHQUFDQyxTQUFSOztBQUFrQixRQUFHRCxHQUFHLENBQUNvQixjQUFQLEVBQXNCO0FBQUMsYUFBTTtBQUFDeEgsYUFBSyxFQUFDLENBQUMsYUFBRCxDQUFQO0FBQXVCVixZQUFJLEVBQUMsQ0FBQyxhQUFELENBQTVCO0FBQTRDbUksV0FBRyxFQUFDLENBQUMsV0FBRCxFQUFhLGVBQWI7QUFBaEQsT0FBTjtBQUFzRjs7QUFDNzdCLFFBQUdyQixHQUFHLENBQUNzQixnQkFBUCxFQUF3QjtBQUFDLGFBQU07QUFBQzFILGFBQUssRUFBQyxDQUFDLGVBQUQsQ0FBUDtBQUF5QlYsWUFBSSxFQUFDLENBQUMsZUFBRCxDQUE5QjtBQUFnRG1JLFdBQUcsRUFBQyxDQUFDLGFBQUQsRUFBZSxpQkFBZjtBQUFwRCxPQUFOO0FBQThGOztBQUN2SCxXQUFNO0FBQUN6SCxXQUFLLEVBQUMsQ0FBQyxXQUFELEVBQWEsWUFBYixDQUFQO0FBQWtDVixVQUFJLEVBQUMsQ0FBQyxXQUFELEVBQWEsV0FBYixDQUF2QztBQUFpRW1JLFNBQUcsRUFBQyxDQUFDLFNBQUQsRUFBVyxZQUFYLEVBQXdCLFVBQXhCLEVBQW1DLGFBQW5DO0FBQXJFLEtBQU47QUFBK0gsR0FGMHBCOztBQUV6cEJySixPQUFLLENBQUNnSixnQkFBTixHQUF1QixZQUFVO0FBQUMsUUFBSUQsVUFBVSxHQUFDLENBQUMsbUJBQUQsRUFBcUIseUJBQXJCLEVBQStDLHNCQUEvQyxFQUFzRSxxQkFBdEUsQ0FBZjs7QUFBNEcsU0FBSSxJQUFJL1csQ0FBQyxHQUFDLENBQU4sRUFBUUMsQ0FBQyxHQUFDOFcsVUFBVSxDQUFDN1csTUFBekIsRUFBZ0NGLENBQUMsR0FBQ0MsQ0FBbEMsRUFBb0NELENBQUMsRUFBckMsRUFBd0M7QUFBQyxVQUFHeUwsUUFBUSxDQUFDc0wsVUFBVSxDQUFDL1csQ0FBRCxDQUFYLENBQVgsRUFBMkI7QUFBQyxlQUFNO0FBQUNxRCxpQkFBTyxFQUFDLENBQUMsbUJBQUQsRUFBcUIseUJBQXJCLEVBQStDLHNCQUEvQyxFQUFzRSxxQkFBdEUsRUFBNkZyRCxDQUE3RixDQUFUO0FBQXlHdVgsaUJBQU8sRUFBQyxDQUFDLG1CQUFELEVBQXFCLHlCQUFyQixFQUErQyxzQkFBL0MsRUFBc0UscUJBQXRFLEVBQTZGdlgsQ0FBN0YsQ0FBakg7QUFBaU53WCxnQkFBTSxFQUFDLENBQUMsa0JBQUQsRUFBb0Isd0JBQXBCLEVBQTZDLHFCQUE3QyxFQUFtRSxvQkFBbkUsRUFBeUZ4WCxDQUF6RixDQUF4TjtBQUFvVHlYLGNBQUksRUFBQyxDQUFDLGdCQUFELEVBQWtCLHNCQUFsQixFQUF5QyxxQkFBekMsRUFBK0Qsa0JBQS9ELEVBQW1GelgsQ0FBbkY7QUFBelQsU0FBTjtBQUF1WjtBQUFDOztBQUMzdUIsUUFBSXlTLFFBQVEsR0FBQyxLQUFLakksT0FBTCxDQUFhaUksUUFBMUI7QUFBQSxRQUFtQ3pGLEtBQUssR0FBQ3lGLFFBQVEsQ0FBQ3JFLE9BQVQsQ0FBaUIsWUFBakIsQ0FBekM7O0FBQXdFLFFBQUdwQixLQUFLLEdBQUMsQ0FBQyxDQUFWLEVBQVk7QUFBQ3lGLGNBQVEsQ0FBQ3BFLE1BQVQsQ0FBZ0JyQixLQUFoQixFQUFzQixDQUF0QjtBQUEwQjs7QUFDL0csV0FBTyxJQUFQO0FBQWEsR0FGbUg7O0FBRWxIZ0IsT0FBSyxDQUFDa0osV0FBTixHQUFrQixZQUFVO0FBQUMsV0FBTTtBQUFDUSxhQUFPLEVBQUM7QUFBQ0MsV0FBRyxFQUFDLCtGQUFMO0FBQXFHQyxXQUFHLEVBQUMsbUVBQXpHO0FBQTZLQyxhQUFLLEVBQUMsc0NBQW5MO0FBQTBOQyxjQUFNLEVBQUMsbURBQWpPO0FBQXFSQyxhQUFLLEVBQUMsNkNBQTNSO0FBQXlVQyxZQUFJLEVBQUM7QUFBQ2hXLGVBQUssRUFBQyxTQUFQO0FBQWlCMEssY0FBSSxFQUFDO0FBQXRCLFNBQTlVO0FBQWlYdUwsYUFBSyxFQUFDO0FBQUNqVyxlQUFLLEVBQUMsU0FBUDtBQUFpQjBLLGNBQUksRUFBQztBQUF0QjtBQUF2WCxPQUFUO0FBQXFhd0wsV0FBSyxFQUFDO0FBQUNQLFdBQUcsRUFBQyxnSkFBTDtBQUFzSkMsV0FBRyxFQUFDLHNEQUExSjtBQUFpTkMsYUFBSyxFQUFDLHdCQUF2TjtBQUFnUEMsY0FBTSxFQUFDLDBDQUF2UDtBQUFrU0UsWUFBSSxFQUFDO0FBQUNoVyxlQUFLLEVBQUMsU0FBUDtBQUFpQm1XLGdCQUFNLEVBQUM7QUFBeEIsU0FBdlM7QUFBdVVGLGFBQUssRUFBQztBQUFDalcsZUFBSyxFQUFDLFNBQVA7QUFBaUJtVyxnQkFBTSxFQUFDO0FBQXhCO0FBQTdVLE9BQTNhO0FBQTB4QkMsaUJBQVcsRUFBQztBQUFDVCxXQUFHLEVBQUMsZ0lBQUw7QUFBc0lDLFdBQUcsRUFBQyxxRUFBMUk7QUFBZ05DLGFBQUssRUFBQyx3Q0FBdE47QUFBK1BDLGNBQU0sRUFBQyxrREFBdFE7QUFBeVRDLGFBQUssRUFBQyxrREFBL1Q7QUFBa1hDLFlBQUksRUFBQyxNQUF2WDtBQUE4WEMsYUFBSyxFQUFDO0FBQXBZLE9BQXR5QjtBQUFtckNJLFlBQU0sRUFBQztBQUFDVixXQUFHLEVBQUMscUhBQUw7QUFBMkhDLFdBQUcsRUFBQyxzRkFBL0g7QUFBc05DLGFBQUssRUFBQywyQ0FBNU47QUFBd1FDLGNBQU0sRUFBQyw2RUFBL1E7QUFBNlZFLFlBQUksRUFBQztBQUFDaFcsZUFBSyxFQUFDLEtBQVA7QUFBYW1XLGdCQUFNLEVBQUM7QUFBcEIsU0FBbFc7QUFBOFhGLGFBQUssRUFBQztBQUFDalcsZUFBSyxFQUFDLEtBQVA7QUFBYW1XLGdCQUFNLEVBQUM7QUFBcEI7QUFBcFk7QUFBMXJDLEtBQU47QUFBb21ELEdBQWpvRDs7QUFBa29EbkssT0FBSyxDQUFDbUosV0FBTixHQUFrQixZQUFVO0FBQUMsV0FBTTtBQUFDbUIsY0FBUSxFQUFDLG9EQUFWO0FBQStEQyxnQkFBVSxFQUFDLHlDQUExRTtBQUFvSEMsYUFBTyxFQUFDLHdEQUE1SDtBQUFxTEMsZUFBUyxFQUFDLHlGQUEvTDtBQUF5UkMsY0FBUSxFQUFDLHdFQUFsUztBQUEyV0MsWUFBTSxFQUFDLHNEQUFsWDtBQUF5YUMsaUJBQVcsRUFBQywwREFBcmI7QUFBZ2ZDLFlBQU0sRUFBQyxtREFBdmY7QUFBMmlCQyxhQUFPLEVBQUMsMkRBQW5qQjtBQUErbUJDLFlBQU0sRUFBQyxpREFBdG5CO0FBQXdxQkMsVUFBSSxFQUFDLGdEQUE3cUI7QUFBOHRCQyxjQUFRLEVBQUM7QUFBdnVCLEtBQU47QUFBNnlCLEdBQTEwQjs7QUFBMjBCakwsT0FBSyxDQUFDeUgsU0FBTixHQUFnQixZQUFVO0FBQUMsU0FBS0MsR0FBTCxHQUFTLEVBQVQ7QUFBWSxRQUFJbFcsUUFBUSxHQUFDLENBQUMsUUFBRCxFQUFVLFNBQVYsRUFBb0IsUUFBcEIsRUFBNkIsTUFBN0IsRUFBb0MsWUFBcEMsRUFBaUQsSUFBakQsRUFBc0QsU0FBdEQsRUFBZ0UsWUFBaEUsRUFBNkUsZUFBN0UsRUFBNkYsU0FBN0YsRUFBdUcsU0FBdkcsRUFBaUgsZUFBakgsRUFBaUksZUFBakksRUFBaUosY0FBakosQ0FBYjs7QUFBOEssU0FBSSxJQUFJUSxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNSLFFBQVEsQ0FBQ1UsTUFBdkIsRUFBOEJGLENBQUMsRUFBL0IsRUFBa0M7QUFBQyxXQUFLMFYsR0FBTCxDQUFTdkwsS0FBSyxDQUFDeUIsUUFBTixDQUFlcE0sUUFBUSxDQUFDUSxDQUFELENBQXZCLENBQVQsSUFBc0NtSyxLQUFLLENBQUNVLFFBQU4sQ0FBZSxLQUFmLEVBQXFCLEtBQUtxTCxHQUFMLEdBQVMsR0FBVCxHQUFhMVcsUUFBUSxDQUFDUSxDQUFELENBQTFDLENBQXRDO0FBQXNGOztBQUN6eUYsU0FBS2taLFNBQUwsQ0FBZSxLQUFLeEQsR0FBcEI7QUFBMEIsR0FEaThFOztBQUNoOEUxSCxPQUFLLENBQUNrTCxTQUFOLEdBQWdCLFVBQVNDLEdBQVQsRUFBYTtBQUFDLFFBQUlDLEdBQUcsR0FBQyxLQUFLNU8sT0FBYjtBQUFxQjJPLE9BQUcsQ0FBQ3hELE1BQUosQ0FBVzBELFdBQVgsQ0FBdUJGLEdBQUcsQ0FBQ0csT0FBM0I7QUFBb0NILE9BQUcsQ0FBQ3hELE1BQUosQ0FBVzBELFdBQVgsQ0FBdUJGLEdBQUcsQ0FBQ3RILE1BQTNCO0FBQW1Dc0gsT0FBRyxDQUFDeEQsTUFBSixDQUFXMEQsV0FBWCxDQUF1QkYsR0FBRyxDQUFDSSxFQUEzQjs7QUFBK0IsU0FBSSxJQUFJdlosQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDLENBQWQsRUFBZ0JBLENBQUMsRUFBakIsRUFBb0I7QUFBQyxVQUFJOFIsS0FBSyxHQUFDcUgsR0FBRyxDQUFDdlcsSUFBSixDQUFTNFcsU0FBVCxDQUFtQixJQUFuQixDQUFWO0FBQW1DMUgsV0FBSyxDQUFDdUgsV0FBTixDQUFrQkYsR0FBRyxDQUFDTSxTQUFKLENBQWNELFNBQWQsQ0FBd0IsSUFBeEIsQ0FBbEI7QUFBaURMLFNBQUcsQ0FBQ3RILE1BQUosQ0FBV3dILFdBQVgsQ0FBdUJ2SCxLQUF2QjtBQUE4QixXQUFLdUUsTUFBTCxDQUFZclcsQ0FBWixJQUFlOFIsS0FBZjtBQUFzQjs7QUFDalYsU0FBS3VFLE1BQUwsQ0FBWW5XLE1BQVosR0FBbUJpWixHQUFHLENBQUN0SCxNQUFKLENBQVc2SCxRQUFYLENBQW9CeFosTUFBdkM7QUFBOEMsU0FBS3laLFFBQUwsQ0FBY1IsR0FBZCxFQUFrQkMsR0FBbEI7QUFBdUJELE9BQUcsQ0FBQ3hELE1BQUosQ0FBV2lFLFlBQVgsQ0FBd0IsVUFBeEIsRUFBbUMsQ0FBQyxDQUFwQztBQUF1Q1QsT0FBRyxDQUFDeEQsTUFBSixDQUFXaUUsWUFBWCxDQUF3QixhQUF4QixFQUFzQyxJQUF0QztBQUE0QyxTQUFLbEUsR0FBTCxDQUFTbUUsT0FBVCxHQUFpQnBPLFFBQVEsQ0FBQ3FPLGFBQVQsQ0FBdUIsa0JBQWdCekksT0FBaEIsR0FBd0IsZ0JBQS9DLENBQWpCO0FBQWtGNUYsWUFBUSxDQUFDc08sSUFBVCxDQUFjVixXQUFkLENBQTBCLEtBQUszRCxHQUFMLENBQVNtRSxPQUFuQztBQUE0QzFQLFNBQUssQ0FBQ21DLGFBQU4sQ0FBb0IsSUFBcEIsRUFBeUIsV0FBekIsRUFBcUMsaUJBQXJDLEVBQXVENk0sR0FBdkQ7QUFBNEQxTixZQUFRLENBQUNzTyxJQUFULENBQWNWLFdBQWQsQ0FBMEJGLEdBQUcsQ0FBQ3hELE1BQTlCO0FBQXNDd0QsT0FBRyxDQUFDYSxNQUFKLENBQVdsVCxNQUFYLEdBQWtCcVMsR0FBRyxDQUFDYSxNQUFKLENBQVdDLFlBQTdCO0FBQTJDLEdBRHhZOztBQUN5WWpNLE9BQUssQ0FBQzJMLFFBQU4sR0FBZSxVQUFTUixHQUFULEVBQWFDLEdBQWIsRUFBaUI7QUFBQyxRQUFJYyxVQUFVLEdBQUNkLEdBQUcsQ0FBQzNHLFFBQUosQ0FBYXJFLE9BQWIsQ0FBcUIsT0FBckIsQ0FBZjs7QUFBNkMsUUFBRzhMLFVBQVUsR0FBQyxDQUFDLENBQWYsRUFBaUI7QUFBQyxVQUFJOUQsT0FBTyxHQUFDZ0QsR0FBRyxDQUFDN0UsWUFBaEI7QUFBQSxVQUE2QnZVLENBQUMsR0FBQ29XLE9BQU8sQ0FBQ2xXLE1BQXZDOztBQUE4QyxhQUFNRixDQUFDLEVBQVAsRUFBVTtBQUFDLFlBQUcsQ0FBQyxLQUFLbVgsV0FBTCxDQUFpQnhNLGNBQWpCLENBQWdDeUwsT0FBTyxDQUFDcFcsQ0FBRCxDQUF2QyxDQUFKLEVBQWdEO0FBQUNvVyxpQkFBTyxDQUFDL0gsTUFBUixDQUFlck8sQ0FBZixFQUFpQixDQUFqQjtBQUFxQjtBQUFDOztBQUNwb0IsVUFBR29XLE9BQU8sQ0FBQ2xXLE1BQVgsRUFBa0I7QUFBQ2laLFdBQUcsQ0FBQ0ksRUFBSixDQUFPRixXQUFQLENBQW1CRixHQUFHLENBQUNnQixZQUF2Qjs7QUFBcUMsWUFBR2YsR0FBRyxDQUFDNUUsU0FBUCxFQUFpQjtBQUFDMkUsYUFBRyxDQUFDZ0IsWUFBSixDQUFpQmQsV0FBakIsQ0FBNkJsUCxLQUFLLENBQUNVLFFBQU4sQ0FBZSxNQUFmLENBQTdCLEVBQXFEdVAsV0FBckQsR0FBaUVoQixHQUFHLENBQUM1RSxTQUFyRTtBQUFnRjs7QUFDMUosYUFBSzZGLGFBQUwsQ0FBbUJqRSxPQUFuQixFQUEyQitDLEdBQUcsQ0FBQ2dCLFlBQS9CLEVBQTRDLFNBQTVDO0FBQXdELE9BRHhELE1BQzREO0FBQUNmLFdBQUcsQ0FBQzNHLFFBQUosQ0FBYXBFLE1BQWIsQ0FBb0I2TCxVQUFwQixFQUErQixDQUEvQjtBQUFtQztBQUFDOztBQUNqRyxRQUFHZCxHQUFHLENBQUMzRyxRQUFKLENBQWF2UyxNQUFiLElBQXFCa1osR0FBRyxDQUFDeEcsY0FBNUIsRUFBMkM7QUFBQyxVQUFJMEgsU0FBUyxHQUFDbEIsR0FBRyxDQUFDM0csUUFBSixDQUFhckUsT0FBYixDQUFxQixNQUFyQixDQUFkO0FBQTJDK0ssU0FBRyxDQUFDSSxFQUFKLENBQU9GLFdBQVAsQ0FBbUJGLEdBQUcsQ0FBQ2EsTUFBdkI7O0FBQStCLFVBQUdaLEdBQUcsQ0FBQ3hHLGNBQVAsRUFBc0I7QUFBQ3VHLFdBQUcsQ0FBQ2EsTUFBSixDQUFXWCxXQUFYLENBQXVCRixHQUFHLENBQUNvQixPQUEzQjtBQUFxQzs7QUFDbEwsVUFBR25CLEdBQUcsQ0FBQzFFLGlCQUFKLEdBQXNCLENBQXRCLElBQXlCNEYsU0FBUyxHQUFDLENBQUMsQ0FBdkMsRUFBeUM7QUFBQ2xCLFdBQUcsQ0FBQzNHLFFBQUosQ0FBYXBFLE1BQWIsQ0FBb0JpTSxTQUFwQixFQUE4QixDQUE5QjtBQUFrQzs7QUFDNUUsVUFBR2xCLEdBQUcsQ0FBQ3ZFLFVBQUosSUFBZ0J5RixTQUFTLEdBQUMsQ0FBQyxDQUE5QixFQUFnQztBQUFDLFlBQUlFLEtBQUssR0FBQyxLQUFLOUUsR0FBTCxDQUFTOEUsS0FBVCxHQUFlclEsS0FBSyxDQUFDVSxRQUFOLENBQWUsUUFBZixFQUF3QixLQUFLcUwsR0FBTCxHQUFTLFFBQWpDLENBQXpCO0FBQW9Fc0UsYUFBSyxDQUFDWixZQUFOLENBQW1CLE9BQW5CLEVBQTJCLEVBQTNCO0FBQStCWSxhQUFLLENBQUNaLFlBQU4sQ0FBbUIsUUFBbkIsRUFBNEIsRUFBNUI7QUFBZ0NULFdBQUcsQ0FBQ2EsTUFBSixDQUFXWCxXQUFYLENBQXVCbUIsS0FBdkI7QUFBK0I7O0FBQ25NLFVBQUdwQixHQUFHLENBQUMzRyxRQUFKLENBQWF2UyxNQUFoQixFQUF1QjtBQUFDLFlBQUl1UyxRQUFRLEdBQUMyRyxHQUFHLENBQUMzRyxRQUFKLENBQWF6SyxLQUFiLEVBQWI7QUFBa0MsYUFBS3FTLGFBQUwsQ0FBbUI1SCxRQUFRLENBQUNnSSxPQUFULEVBQW5CLEVBQXNDdEIsR0FBRyxDQUFDYSxNQUExQztBQUFtRDtBQUFDOztBQUM5RyxRQUFHWixHQUFHLENBQUN2RyxPQUFKLElBQWF1RyxHQUFHLENBQUNwRyxVQUFwQixFQUErQjtBQUFDbUcsU0FBRyxDQUFDSSxFQUFKLENBQU9GLFdBQVAsQ0FBbUJGLEdBQUcsQ0FBQ3VCLFNBQXZCOztBQUFrQyxVQUFHdEIsR0FBRyxDQUFDdkcsT0FBUCxFQUFlO0FBQUNzRyxXQUFHLENBQUN1QixTQUFKLENBQWNyQixXQUFkLENBQTBCRixHQUFHLENBQUN0RyxPQUE5QixFQUF1Q3dHLFdBQXZDLENBQW1ERixHQUFHLENBQUN3QixZQUF2RDtBQUFzRTs7QUFDeEosVUFBR3ZCLEdBQUcsQ0FBQ3BHLFVBQVAsRUFBa0I7QUFBQ21HLFdBQUcsQ0FBQ3VCLFNBQUosQ0FBY3JCLFdBQWQsQ0FBMEJGLEdBQUcsQ0FBQ3lCLFlBQTlCLEVBQTRDdkIsV0FBNUMsQ0FBd0RGLEdBQUcsQ0FBQzBCLFdBQTVEO0FBQTBFO0FBQUM7O0FBQzlGLFFBQUd6QixHQUFHLENBQUMxRyxRQUFQLEVBQWdCO0FBQUMsV0FBSzJILGFBQUwsQ0FBbUIsQ0FBQyxNQUFELEVBQVEsTUFBUixDQUFuQixFQUFtQ2xCLEdBQUcsQ0FBQ0ksRUFBdkM7QUFBNEM7QUFBQyxHQVRzVzs7QUFTcld2TCxPQUFLLENBQUNxTSxhQUFOLEdBQW9CLFVBQVNqRSxPQUFULEVBQWlCK0MsR0FBakIsRUFBcUJuWCxLQUFyQixFQUEyQjtBQUFDLFFBQUk5QixNQUFNLEdBQUNrVyxPQUFPLENBQUNsVyxNQUFuQjs7QUFBMEIsU0FBSSxJQUFJRixDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNFLE1BQWQsRUFBcUJGLENBQUMsRUFBdEIsRUFBeUI7QUFBQyxVQUFJaUMsSUFBSSxHQUFDbVUsT0FBTyxDQUFDcFcsQ0FBRCxDQUFoQjtBQUFvQixXQUFLb1csT0FBTCxDQUFhblUsSUFBYixJQUFtQmtJLEtBQUssQ0FBQ1UsUUFBTixDQUFlLFFBQWYsRUFBd0IsS0FBS3FMLEdBQUwsR0FBUyxHQUFULEdBQWFqVSxJQUFJLENBQUMrRSxXQUFMLEVBQXJDLENBQW5CO0FBQTRFbVMsU0FBRyxDQUFDRSxXQUFKLENBQWdCLEtBQUtqRCxPQUFMLENBQWFuVSxJQUFiLENBQWhCOztBQUFvQyxVQUFJQSxJQUFJLElBQUUsT0FBTyxLQUFLQSxJQUFMLENBQVAsS0FBb0IsVUFBM0IsSUFBd0NELEtBQTNDLEVBQWlEO0FBQUMsYUFBS29VLE9BQUwsQ0FBYW5VLElBQWIsRUFBbUJELEtBQW5CLEdBQXlCQSxLQUFLLEdBQUNBLEtBQUQsR0FBT0MsSUFBckM7QUFBMEMsYUFBS21VLE9BQUwsQ0FBYW5VLElBQWIsRUFBbUI2WSxNQUFuQixHQUEwQjdZLElBQTFCOztBQUErQixZQUFHRCxLQUFLLEtBQUcsU0FBWCxFQUFxQjtBQUFDLGVBQUtvVSxPQUFMLENBQWFuVSxJQUFiLEVBQW1CMlgsWUFBbkIsQ0FBZ0MsT0FBaEMsRUFBd0MzWCxJQUFJLENBQUNpSixNQUFMLENBQVksQ0FBWixFQUFlQyxXQUFmLEtBQTZCbEosSUFBSSxDQUFDK0YsS0FBTCxDQUFXLENBQVgsQ0FBckU7QUFBcUY7QUFBQztBQUFDO0FBQUMsR0FBamQ7O0FBQWtkZ0csT0FBSyxDQUFDNkgsWUFBTixHQUFtQixZQUFVO0FBQUMsU0FBS2tGLFNBQUwsR0FBZSxFQUFmO0FBQWtCLFFBQUlDLFNBQVMsR0FBQyxLQUFLeFEsT0FBTCxDQUFhb0gsYUFBM0I7QUFBQSxRQUF5Q3FKLE9BQU8sR0FBQyxFQUFqRDs7QUFBb0QsUUFBRyxDQUFDRCxTQUFKLEVBQWM7QUFBQyxhQUFPLEtBQVA7QUFBYzs7QUFDbHBCLFFBQUc7QUFBQ0MsYUFBTyxHQUFDeFAsUUFBUSxDQUFDeVAsZ0JBQVQsQ0FBMEJGLFNBQTFCLENBQVI7QUFBOEMsS0FBbEQsQ0FBa0QsT0FBTTFRLEtBQU4sRUFBWTtBQUFDSCxXQUFLLENBQUNHLEtBQU4sQ0FBWSwwREFBd0QwUSxTQUF4RCxHQUFrRSxHQUE5RTtBQUFvRjs7QUFDbkosU0FBSSxJQUFJaGIsQ0FBQyxHQUFDLENBQU4sRUFBUUMsQ0FBQyxHQUFDZ2IsT0FBTyxDQUFDL2EsTUFBdEIsRUFBNkJGLENBQUMsR0FBQ0MsQ0FBL0IsRUFBaUNELENBQUMsRUFBbEMsRUFBcUM7QUFBQyxVQUFJbWIsTUFBTSxHQUFDRixPQUFPLENBQUNqYixDQUFELENBQWxCO0FBQUEsVUFBc0JvYixLQUFLLEdBQUMsRUFBNUI7QUFBK0JBLFdBQUssQ0FBQ0MsR0FBTixHQUFVRixNQUFNLENBQUNwVSxPQUFQLEtBQWlCLEdBQWpCLEdBQXFCb1UsTUFBTSxDQUFDRyxZQUFQLENBQW9CLE1BQXBCLENBQXJCLEdBQWlELElBQTNEO0FBQWdFRixXQUFLLENBQUNDLEdBQU4sR0FBVUYsTUFBTSxDQUFDcFUsT0FBUCxLQUFpQixLQUFqQixHQUF1Qm9VLE1BQU0sQ0FBQ0ksVUFBUCxJQUFtQkosTUFBTSxDQUFDRSxHQUFqRCxHQUFxREQsS0FBSyxDQUFDQyxHQUFyRTtBQUF5RUQsV0FBSyxDQUFDQyxHQUFOLEdBQVVGLE1BQU0sQ0FBQ0csWUFBUCxDQUFvQixVQUFwQixLQUFpQ0YsS0FBSyxDQUFDQyxHQUFqRDs7QUFBcUQsVUFBR0QsS0FBSyxDQUFDQyxHQUFULEVBQWE7QUFBQyxhQUFLRyxZQUFMLENBQWtCTCxNQUFsQixFQUF5QkMsS0FBekI7QUFBZ0MsYUFBS0ssWUFBTCxDQUFrQkwsS0FBbEI7O0FBQXlCLFlBQUdBLEtBQUssQ0FBQ25aLElBQVQsRUFBYztBQUFDLGVBQUt5WixhQUFMLENBQW1CUCxNQUFuQixFQUEwQkMsS0FBMUI7QUFBaUMsZUFBS08sYUFBTCxDQUFtQlAsS0FBbkI7QUFBMEIsZUFBS1EsZUFBTCxDQUFxQlQsTUFBckIsRUFBNEJDLEtBQTVCO0FBQW1DLGVBQUtTLGVBQUwsQ0FBcUJULEtBQXJCO0FBQTRCLGNBQUlVLE9BQU8sR0FBQyxLQUFLQyxjQUFMLENBQW9CWixNQUFwQixDQUFaO0FBQXdDLGVBQUthLGtCQUFMLENBQXdCRixPQUF4QixFQUFnQ1YsS0FBaEM7QUFBdUNBLGVBQUssQ0FBQ3BPLEtBQU4sR0FBWThPLE9BQU8sQ0FBQzViLE1BQXBCO0FBQTJCNGIsaUJBQU8sQ0FBQzFaLElBQVIsQ0FBYWdaLEtBQWI7QUFBb0IsZUFBS2EsYUFBTCxDQUFtQmQsTUFBbkIsRUFBMEJXLE9BQU8sQ0FBQ25ZLElBQWxDLEVBQXVDeVgsS0FBSyxDQUFDcE8sS0FBN0M7QUFBcUQ7QUFBQztBQUFDOztBQUN4b0I3QyxTQUFLLENBQUNtQyxhQUFOLENBQW9CLElBQXBCLEVBQXlCLFdBQXpCLEVBQXFDLGlCQUFyQyxFQUF1RCxLQUFLeU8sU0FBNUQ7QUFBd0UsR0FIeWM7O0FBR3hjL00sT0FBSyxDQUFDa08sUUFBTixHQUFlLFVBQVN2WSxJQUFULEVBQWN5WCxLQUFkLEVBQW9CO0FBQUMsUUFBRyxDQUFDQSxLQUFELElBQVEsU0FBT0EsS0FBUCxNQUFlLFFBQTFCLEVBQW1DO0FBQUNqUixXQUFLLENBQUNHLEtBQU4sQ0FBWSwrQ0FBWjtBQUE2RCxhQUFPLEtBQVA7QUFBYzs7QUFDNU4zRyxRQUFJLEdBQUNBLElBQUksS0FBRyxFQUFQLEdBQVUsQ0FBVixHQUFZQSxJQUFqQjtBQUFzQixRQUFJbVksT0FBTyxHQUFDLEtBQUtmLFNBQUwsQ0FBZXBYLElBQWYsQ0FBWjtBQUFpQ21ZLFdBQU8sR0FBQyxDQUFDQSxPQUFELEdBQVUsS0FBS2YsU0FBTCxDQUFlcFgsSUFBZixJQUFxQixFQUEvQixHQUFtQ21ZLE9BQTNDO0FBQW1EQSxXQUFPLENBQUNuWSxJQUFSLEdBQWFBLElBQWI7QUFBa0IsUUFBSXpELE1BQU0sR0FBQ2tiLEtBQUssQ0FBQ2xiLE1BQWpCOztBQUF3QixTQUFJLElBQUlGLENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBQ0UsTUFBZCxFQUFxQkYsQ0FBQyxFQUF0QixFQUF5QjtBQUFDLFVBQUk0QyxJQUFJLEdBQUN1SCxLQUFLLENBQUNrQixXQUFOLENBQWtCK1AsS0FBSyxDQUFDcGIsQ0FBRCxDQUF2QixDQUFUOztBQUFxQyxVQUFHNEMsSUFBSSxDQUFDeVksR0FBUixFQUFZO0FBQUMsYUFBS0ksWUFBTCxDQUFrQjdZLElBQWxCO0FBQXdCLGFBQUsrWSxhQUFMLENBQW1CL1ksSUFBbkI7QUFBeUIsYUFBS2laLGVBQUwsQ0FBcUJqWixJQUFyQjtBQUEyQixhQUFLb1osa0JBQUwsQ0FBd0JGLE9BQXhCLEVBQWdDbFosSUFBaEM7QUFBc0NBLFlBQUksQ0FBQ29LLEtBQUwsR0FBVzhPLE9BQU8sQ0FBQzViLE1BQW5CO0FBQTBCNGIsZUFBTyxDQUFDMVosSUFBUixDQUFhUSxJQUFiO0FBQW9CO0FBQUM7QUFBQyxHQUR6VDs7QUFDMFRvTCxPQUFLLENBQUN5TixZQUFOLEdBQW1CLFVBQVNMLEtBQVQsRUFBZTtBQUFDLFFBQUcsQ0FBQyxPQUFELEVBQVMsT0FBVCxFQUFpQixRQUFqQixFQUEwQixNQUExQixFQUFrQ2hOLE9BQWxDLENBQTBDZ04sS0FBSyxDQUFDblosSUFBaEQsSUFBc0QsQ0FBQyxDQUExRCxFQUE0RDtBQUFDO0FBQVE7O0FBQzNlbVosU0FBSyxDQUFDblosSUFBTixHQUFXLElBQVg7QUFBZ0IsUUFBSWtaLE1BQU0sR0FBQ0MsS0FBSyxDQUFDQyxHQUFOLEdBQVVELEtBQUssQ0FBQ0MsR0FBaEIsR0FBb0IsSUFBL0I7QUFBb0MsUUFBSWMsU0FBUyxHQUFDLENBQUNoQixNQUFNLENBQUNoVSxLQUFQLENBQWEsTUFBYixFQUFxQixDQUFyQixLQUF5QmdVLE1BQTFCLEVBQWtDaUIsTUFBbEMsQ0FBeUMsQ0FBQyxDQUFDLENBQUNqQixNQUFNLENBQUNrQixXQUFQLENBQW1CLEdBQW5CLENBQUYsS0FBNEIsQ0FBN0IsSUFBZ0MsQ0FBekUsQ0FBZDs7QUFBMEYsUUFBRyxxREFBcURDLElBQXJELENBQTBESCxTQUExRCxLQUFzRSxDQUFDLG1CQUFELEVBQXFCLGtCQUFyQixFQUF5Qy9OLE9BQXpDLENBQWlEK00sTUFBakQsSUFBeUQsQ0FBQyxDQUFuSSxFQUFxSTtBQUFDQyxXQUFLLENBQUNuWixJQUFOLEdBQVcsT0FBWDtBQUFtQm1aLFdBQUssQ0FBQ0MsR0FBTixHQUFVLEtBQUtrQixNQUFMLENBQVlwQixNQUFaLENBQVY7QUFBK0IsS0FBeEwsTUFBNkwsSUFBRyxrQkFBa0JtQixJQUFsQixDQUF1QkgsU0FBdkIsQ0FBSCxFQUFxQztBQUFDZixXQUFLLENBQUNuWixJQUFOLEdBQVcsT0FBWDtBQUFtQm1aLFdBQUssQ0FBQ29CLE1BQU4sR0FBYSxPQUFiO0FBQXNCLEtBQS9FLE1BQW1GO0FBQUMsVUFBSUMsTUFBTSxHQUFDLEtBQUt2RixXQUFoQjs7QUFBNEIsV0FBSSxJQUFJalYsSUFBUixJQUFnQndhLE1BQWhCLEVBQXVCO0FBQUMsWUFBR0EsTUFBTSxDQUFDOVIsY0FBUCxDQUFzQjFJLElBQXRCLENBQUgsRUFBK0I7QUFBQyxjQUFJeWEsSUFBSSxHQUFDdkIsTUFBTSxDQUFDL04sS0FBUCxDQUFhcVAsTUFBTSxDQUFDeGEsSUFBRCxDQUFOLENBQWEwVixHQUExQixDQUFUOztBQUF3QyxjQUFHK0UsSUFBSSxJQUFFQSxJQUFJLENBQUMsQ0FBRCxDQUFiLEVBQWlCO0FBQUMsZ0JBQUlDLE1BQU0sR0FBQ0YsTUFBTSxDQUFDeGEsSUFBRCxDQUFqQjtBQUF3Qm1aLGlCQUFLLENBQUNuWixJQUFOLEdBQVcsT0FBWDtBQUFtQm1aLGlCQUFLLENBQUNvQixNQUFOLEdBQWF2YSxJQUFiO0FBQWtCbVosaUJBQUssQ0FBQ3ZELEtBQU4sR0FBWThFLE1BQU0sQ0FBQzlFLEtBQVAsQ0FBYS9MLE9BQWIsQ0FBcUIsTUFBckIsRUFBNEI0USxJQUFJLENBQUMsQ0FBRCxDQUFoQyxDQUFaO0FBQWlEdEIsaUJBQUssQ0FBQ0MsR0FBTixHQUFVc0IsTUFBTSxDQUFDL0UsR0FBUCxDQUFXOUwsT0FBWCxDQUFtQixNQUFuQixFQUEwQjRRLElBQUksQ0FBQyxDQUFELENBQTlCLENBQVY7QUFBNkN0QixpQkFBSyxDQUFDbkQsS0FBTixHQUFZMEUsTUFBTSxDQUFDMUUsS0FBbkI7QUFBeUJtRCxpQkFBSyxDQUFDcEQsSUFBTixHQUFXMkUsTUFBTSxDQUFDM0UsSUFBbEI7O0FBQXVCLGdCQUFHLEtBQUt4TixPQUFMLENBQWE0SyxjQUFoQixFQUErQjtBQUFDZ0csbUJBQUssQ0FBQ3RELE1BQU4sR0FBYSxDQUFDc0QsS0FBSyxDQUFDdEQsTUFBUCxJQUFlNkUsTUFBTSxDQUFDN0UsTUFBdEIsR0FBNkI2RSxNQUFNLENBQUM3RSxNQUFQLENBQWNoTSxPQUFkLENBQXNCLE1BQXRCLEVBQTZCNFEsSUFBSSxDQUFDLENBQUQsQ0FBakMsQ0FBN0IsR0FBbUV0QixLQUFLLENBQUN0RCxNQUF0RjtBQUE2RnNELG1CQUFLLENBQUNyRCxLQUFOLEdBQVksQ0FBQ3FELEtBQUssQ0FBQ3JELEtBQVAsSUFBYzRFLE1BQU0sQ0FBQzdFLE1BQXJCLEdBQTRCNkUsTUFBTSxDQUFDN0UsTUFBUCxDQUFjaE0sT0FBZCxDQUFzQixNQUF0QixFQUE2QjRRLElBQUksQ0FBQyxDQUFELENBQWpDLENBQTVCLEdBQWtFdEIsS0FBSyxDQUFDckQsS0FBcEY7QUFBMkY7O0FBQ2g5QjtBQUFPO0FBQUM7QUFBQztBQUFDO0FBQUMsR0FGd1g7O0FBRXZYL0osT0FBSyxDQUFDdU8sTUFBTixHQUFhLFVBQVNwQixNQUFULEVBQWdCO0FBQUMsUUFBSWxVLE1BQU0sR0FBQyxDQUFDa1UsTUFBTSxJQUFFLEVBQVQsRUFBYWhVLEtBQWIsQ0FBbUIsR0FBbkIsQ0FBWDtBQUFBLFFBQW1DakgsTUFBTSxHQUFDK0csTUFBTSxDQUFDL0csTUFBakQ7QUFBQSxRQUF3RDJHLEtBQUssR0FBQyxDQUE5RDs7QUFBZ0UsUUFBRzNHLE1BQU0sSUFBRSxDQUFYLEVBQWE7QUFBQyxhQUFPaWIsTUFBUDtBQUFlOztBQUN2SSxTQUFJLElBQUluYixDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNFLE1BQWQsRUFBcUJGLENBQUMsRUFBdEIsRUFBeUI7QUFBQyxVQUFJNGMsS0FBSyxHQUFDM1YsTUFBTSxDQUFDakgsQ0FBRCxDQUFOLENBQVU4TCxPQUFWLENBQWtCLE1BQWxCLEVBQXlCLEdBQXpCLEVBQThCMUUsSUFBOUIsR0FBcUNELEtBQXJDLENBQTJDLEdBQTNDLENBQVY7QUFBQSxVQUEwRHNCLEtBQUssR0FBQ29VLFVBQVUsQ0FBQ0QsS0FBSyxDQUFDLENBQUQsQ0FBTixDQUFWLElBQXNCLENBQXRGO0FBQUEsVUFBd0ZFLElBQUksR0FBQ0YsS0FBSyxDQUFDLENBQUQsQ0FBTCxHQUFTQSxLQUFLLENBQUMsQ0FBRCxDQUFMLENBQVM1VSxLQUFULENBQWUsQ0FBQyxDQUFoQixDQUFULEdBQTRCLElBQXpIOztBQUE4SCxVQUFJOFUsSUFBSSxLQUFHLEdBQVAsSUFBWUMsTUFBTSxDQUFDbFcsS0FBUCxJQUFjNEIsS0FBMUIsSUFBaUNBLEtBQUssR0FBQzVCLEtBQXhDLElBQWdELENBQUM0QixLQUFqRCxJQUF3RHpJLENBQUMsS0FBRyxDQUEvRCxFQUFpRTtBQUFDNkcsYUFBSyxHQUFDNEIsS0FBTjtBQUFZMFMsY0FBTSxHQUFDeUIsS0FBSyxDQUFDLENBQUQsQ0FBWjtBQUFpQjtBQUFDOztBQUN4UCxXQUFPekIsTUFBUDtBQUFlLEdBRkg7O0FBRUluTixPQUFLLENBQUN3TixZQUFOLEdBQW1CLFVBQVNMLE1BQVQsRUFBZ0JDLEtBQWhCLEVBQXNCO0FBQUMsUUFBSTRCLElBQUksR0FBQyxLQUFLeFMsT0FBTCxDQUFhc0ksV0FBdEI7QUFBQSxRQUFrQ3RRLElBQUksR0FBQyxLQUFLeWEsT0FBTCxDQUFhOUIsTUFBYixDQUF2QztBQUFBLFFBQTREK0IsR0FBRyxHQUFDL0IsTUFBTSxDQUFDZ0MsaUJBQXZFO0FBQXlGRCxPQUFHLEdBQUMvQixNQUFNLENBQUNwVSxPQUFQLEtBQWlCLEtBQWpCLElBQXdCbVcsR0FBeEIsSUFBNkJBLEdBQUcsQ0FBQ25XLE9BQUosS0FBYyxLQUEzQyxHQUFpRG1XLEdBQWpELEdBQXFEL0IsTUFBekQ7QUFBZ0VDLFNBQUssQ0FBQ25aLElBQU4sR0FBVyxDQUFDbVosS0FBSyxDQUFDblosSUFBUCxHQUFZTyxJQUFJLENBQUNQLElBQUwsSUFBV2taLE1BQU0sQ0FBQ0csWUFBUCxDQUFvQixXQUFwQixDQUF2QixHQUF3REYsS0FBSyxDQUFDblosSUFBekU7QUFBOEVtWixTQUFLLENBQUNnQyxLQUFOLEdBQVk1YSxJQUFJLENBQUM0YSxLQUFMLElBQVlqQyxNQUFNLENBQUNHLFlBQVAsQ0FBb0IsWUFBcEIsQ0FBWixLQUFnRDBCLElBQUksR0FBQ0UsR0FBRyxDQUFDRSxLQUFMLEdBQVcsSUFBL0QsQ0FBWjtBQUFpRmhDLFNBQUssQ0FBQ2lDLElBQU4sR0FBVzdhLElBQUksQ0FBQzZhLElBQUwsSUFBV2xDLE1BQU0sQ0FBQ0csWUFBUCxDQUFvQixXQUFwQixDQUFYLEtBQThDMEIsSUFBSSxHQUFDRSxHQUFHLENBQUNJLEdBQUwsR0FBUyxJQUEzRCxDQUFYO0FBQTRFbEMsU0FBSyxDQUFDckQsS0FBTixHQUFZdlYsSUFBSSxDQUFDdVYsS0FBTCxJQUFZb0QsTUFBTSxDQUFDRyxZQUFQLENBQW9CLFlBQXBCLENBQXhCO0FBQTBERixTQUFLLENBQUN0RCxNQUFOLEdBQWEsS0FBS3lFLE1BQUwsQ0FBWS9aLElBQUksQ0FBQ3NWLE1BQUwsSUFBYXFELE1BQU0sQ0FBQ0csWUFBUCxDQUFvQixhQUFwQixDQUF6QixDQUFiO0FBQTBFRixTQUFLLENBQUN2VSxLQUFOLEdBQVlyRSxJQUFJLENBQUNxRSxLQUFMLElBQVlzVSxNQUFNLENBQUNHLFlBQVAsQ0FBb0IsWUFBcEIsQ0FBeEI7QUFBMERGLFNBQUssQ0FBQ3RVLE1BQU4sR0FBYXRFLElBQUksQ0FBQ3NFLE1BQUwsSUFBYXFVLE1BQU0sQ0FBQ0csWUFBUCxDQUFvQixhQUFwQixDQUExQjs7QUFBNkQsUUFBR0YsS0FBSyxDQUFDZ0MsS0FBTixLQUFjaEMsS0FBSyxDQUFDaUMsSUFBdkIsRUFBNEI7QUFBQ2pDLFdBQUssQ0FBQ2lDLElBQU4sR0FBVyxJQUFYO0FBQWlCO0FBQUMsR0FBeHRCOztBQUF5dEJyUCxPQUFLLENBQUMwTixhQUFOLEdBQW9CLFVBQVNQLE1BQVQsRUFBZ0JDLEtBQWhCLEVBQXNCO0FBQUMsUUFBSW1DLFNBQVMsR0FBQ3BDLE1BQU0sQ0FBQ3FDLG9CQUFQLENBQTRCLEtBQTVCLENBQWQ7O0FBQWlELFFBQUcsQ0FBQ3BDLEtBQUssQ0FBQ3JELEtBQVAsSUFBY3dGLFNBQVMsQ0FBQyxDQUFELENBQTFCLEVBQThCO0FBQUNuQyxXQUFLLENBQUNyRCxLQUFOLEdBQVl3RixTQUFTLENBQUMsQ0FBRCxDQUFULENBQWFsQyxHQUF6QjtBQUE4QjtBQUFDLEdBQTFKOztBQUEySnJOLE9BQUssQ0FBQzJOLGFBQU4sR0FBb0IsVUFBU1AsS0FBVCxFQUFlO0FBQUMsUUFBRyxDQUFDLEtBQUs1USxPQUFMLENBQWE0SyxjQUFkLElBQThCZ0csS0FBSyxDQUFDblosSUFBTixLQUFhLE9BQTNDLElBQW9EbVosS0FBSyxDQUFDb0IsTUFBTixLQUFlLE9BQXRFLEVBQThFO0FBQUM7QUFBUTs7QUFDLy9CLFFBQUlpQixTQUFTLEdBQUNyQyxLQUFLLENBQUN0RCxNQUFOLElBQWNzRCxLQUFLLENBQUN0RCxNQUFOLENBQWExSixPQUFiLENBQXFCLE9BQXJCLElBQThCLENBQUMsQ0FBM0Q7QUFBQSxRQUE2RHNQLFFBQVEsR0FBQ3RDLEtBQUssQ0FBQ3JELEtBQU4sSUFBYXFELEtBQUssQ0FBQ3JELEtBQU4sQ0FBWTNKLE9BQVosQ0FBb0IsT0FBcEIsSUFBNkIsQ0FBQyxDQUFqSDs7QUFBbUgsUUFBR3FQLFNBQVMsSUFBRUMsUUFBZCxFQUF1QjtBQUFDLFVBQUlDLEdBQUcsR0FBQ0YsU0FBUyxHQUFDckMsS0FBSyxDQUFDdEQsTUFBUCxHQUFjc0QsS0FBSyxDQUFDckQsS0FBckM7QUFBQSxVQUEyQzZGLEdBQUcsR0FBQyxJQUFJQyxjQUFKLEVBQS9DOztBQUFvRUQsU0FBRyxDQUFDRSxNQUFKLEdBQVcsVUFBUzliLEtBQVQsRUFBZTtBQUFDLFlBQUlzRCxRQUFRLEdBQUN0RCxLQUFLLENBQUMrYixNQUFOLENBQWFDLFlBQTFCO0FBQXVDMVksZ0JBQVEsR0FBQzZMLElBQUksQ0FBQzhNLEtBQUwsQ0FBVzNZLFFBQVgsQ0FBVDtBQUE4QkEsZ0JBQVEsR0FBQ0EsUUFBUSxDQUFDcUYsY0FBVCxDQUF3QixDQUF4QixJQUEyQnJGLFFBQVEsQ0FBQyxDQUFELENBQW5DLEdBQXVDQSxRQUFoRDs7QUFBeUQsWUFBR0EsUUFBSCxFQUFZO0FBQUM4VixlQUFLLENBQUN0RCxNQUFOLEdBQWF4UyxRQUFRLENBQUM0WSxlQUFULElBQTBCNVksUUFBUSxDQUFDNlksYUFBaEQ7O0FBQThELGNBQUcvQyxLQUFLLENBQUNqQyxHQUFULEVBQWE7QUFBQ2lDLGlCQUFLLENBQUNqQyxHQUFOLENBQVUvTixLQUFWLENBQWdCZ1QsZUFBaEIsR0FBZ0MsVUFBUWhELEtBQUssQ0FBQ3RELE1BQWQsR0FBcUIsSUFBckQ7QUFBMkQ7O0FBQzVmLGNBQUc0RixRQUFILEVBQVk7QUFBQyxnQkFBSTNGLEtBQUssR0FBQ3pTLFFBQVEsQ0FBQytZLGVBQVQsSUFBMEIvWSxRQUFRLENBQUM2WSxhQUE3Qzs7QUFBMkQsZ0JBQUcsU0FBTy9DLEtBQUssQ0FBQ3JELEtBQWIsTUFBcUIsUUFBeEIsRUFBaUM7QUFBQ3FELG1CQUFLLENBQUNyRCxLQUFOLENBQVkzTSxLQUFaLENBQWtCZ1QsZUFBbEIsR0FBa0MsVUFBUXJHLEtBQVIsR0FBYyxJQUFoRDtBQUFzRCxhQUF4RixNQUE0RjtBQUFDcUQsbUJBQUssQ0FBQ3JELEtBQU4sR0FBWUEsS0FBWjtBQUFtQjtBQUFDO0FBQUM7QUFBQyxPQUQrQixDQUM5QjdQLElBRDhCLENBQ3pCLElBRHlCLENBQVg7O0FBQ1IwVixTQUFHLENBQUNVLElBQUosQ0FBUyxLQUFULEVBQWVDLFNBQVMsQ0FBQ1osR0FBRCxDQUF4QixFQUE4QixJQUE5QjtBQUFvQ2pjLGdCQUFVLENBQUMsWUFBVTtBQUFDa2MsV0FBRyxDQUFDWSxJQUFKO0FBQVksT0FBeEIsRUFBeUIsQ0FBekIsQ0FBVjtBQUF1QztBQUFDLEdBRmluQjs7QUFFaG5CeFEsT0FBSyxDQUFDNE4sZUFBTixHQUFzQixVQUFTVCxNQUFULEVBQWdCQyxLQUFoQixFQUFzQjtBQUFDLFFBQUlxRCxJQUFJLEdBQUN0RCxNQUFNLENBQUN1RCxrQkFBaEI7O0FBQW1DLFFBQUdELElBQUksSUFBRUEsSUFBSSxDQUFDMVgsT0FBTCxLQUFlLFlBQXhCLEVBQXFDO0FBQUMsVUFBSThMLE9BQU8sR0FBQzRMLElBQUksQ0FBQ0UsU0FBakI7O0FBQTJCLFVBQUcsQ0FBQ3ZELEtBQUssQ0FBQ2dDLEtBQVYsRUFBZ0I7QUFBQ2hDLGFBQUssQ0FBQ2dDLEtBQU4sR0FBWXZLLE9BQVo7QUFBcUIsT0FBdEMsTUFBMkMsSUFBRyxDQUFDdUksS0FBSyxDQUFDaUMsSUFBVixFQUFlO0FBQUNqQyxhQUFLLENBQUNpQyxJQUFOLEdBQVd4SyxPQUFYO0FBQW9CO0FBQUM7QUFBQyxHQUFsTzs7QUFBbU83RSxPQUFLLENBQUM2TixlQUFOLEdBQXNCLFVBQVNULEtBQVQsRUFBZTtBQUFDQSxTQUFLLENBQUNnQyxLQUFOLEdBQVloQyxLQUFLLENBQUNnQyxLQUFOLEdBQVksaUJBQWUsS0FBS2xILEdBQXBCLEdBQXdCLFVBQXhCLEdBQW1Da0YsS0FBSyxDQUFDZ0MsS0FBTixDQUFZaFcsSUFBWixFQUFuQyxHQUFzRCxRQUFsRSxHQUEyRSxFQUF2RjtBQUEwRmdVLFNBQUssQ0FBQ2lDLElBQU4sR0FBV2pDLEtBQUssQ0FBQ2lDLElBQU4sR0FBVyxpQkFBZSxLQUFLbkgsR0FBcEIsR0FBd0IsU0FBeEIsR0FBa0NrRixLQUFLLENBQUNpQyxJQUFOLENBQVdqVyxJQUFYLEVBQWxDLEdBQW9ELFFBQS9ELEdBQXdFLEVBQW5GO0FBQXNGZ1UsU0FBSyxDQUFDdkksT0FBTixHQUFjdUksS0FBSyxDQUFDZ0MsS0FBTixHQUFZaEMsS0FBSyxDQUFDaUMsSUFBaEM7QUFBc0MsR0FBNVA7O0FBQTZQclAsT0FBSyxDQUFDNFEsY0FBTixHQUFxQixVQUFTekQsTUFBVCxFQUFnQjtBQUFDLFFBQUkwRCxNQUFNLEdBQUMxRCxNQUFYO0FBQUEsUUFBa0IyRCxJQUFJLEdBQUMsQ0FBdkI7O0FBQXlCLFdBQU1ELE1BQU0sSUFBRUMsSUFBSSxHQUFDLENBQW5CLEVBQXFCO0FBQUNELFlBQU0sR0FBQ0EsTUFBTSxDQUFDRSxVQUFkOztBQUF5QixVQUFHRixNQUFNLElBQUVBLE1BQU0sQ0FBQzlYLE9BQVAsS0FBaUIsUUFBekIsSUFBbUM4WCxNQUFNLENBQUNFLFVBQTdDLEVBQXdEO0FBQUMsZUFBT0YsTUFBTSxDQUFDRSxVQUFQLENBQWtCekQsWUFBbEIsQ0FBK0IsSUFBL0IsQ0FBUDtBQUE2Qzs7QUFDeDhCd0QsVUFBSTtBQUFJO0FBQUMsR0FEMnVCOztBQUMxdUI5USxPQUFLLENBQUMrTixjQUFOLEdBQXFCLFVBQVNaLE1BQVQsRUFBZ0I7QUFBQyxRQUFJM1ksSUFBSSxHQUFDLEtBQUt5YSxPQUFMLENBQWE5QixNQUFiLENBQVQ7QUFBOEIsUUFBSXhYLElBQUksR0FBQ25CLElBQUksQ0FBQ3djLEdBQUwsSUFBVTdELE1BQU0sQ0FBQ0csWUFBUCxDQUFvQixVQUFwQixDQUFuQjtBQUFtRDNYLFFBQUksR0FBQyxDQUFDQSxJQUFELEdBQU0sS0FBS2liLGNBQUwsQ0FBb0J6RCxNQUFwQixDQUFOLEdBQWtDeFgsSUFBdkM7QUFBNENBLFFBQUksR0FBQyxDQUFDQSxJQUFELEdBQU1vTCxNQUFNLENBQUNtQyxJQUFQLENBQVksS0FBSzZKLFNBQWpCLEVBQTRCN2EsTUFBNUIsR0FBbUMsQ0FBekMsR0FBMkN5RCxJQUFoRDtBQUFxRCxRQUFJbVksT0FBTyxHQUFDLEtBQUtmLFNBQUwsQ0FBZXBYLElBQWYsQ0FBWjtBQUFpQ21ZLFdBQU8sR0FBQyxDQUFDQSxPQUFELEdBQVUsS0FBS2YsU0FBTCxDQUFlcFgsSUFBZixJQUFxQixFQUEvQixHQUFtQ21ZLE9BQTNDO0FBQW1EQSxXQUFPLENBQUNuWSxJQUFSLEdBQWFBLElBQWI7QUFBa0IsV0FBT21ZLE9BQVA7QUFBZ0IsR0FBOVU7O0FBQStVOU4sT0FBSyxDQUFDZ08sa0JBQU4sR0FBeUIsVUFBU0YsT0FBVCxFQUFpQlYsS0FBakIsRUFBdUI7QUFBQyxRQUFHLENBQUNVLE9BQU8sQ0FBQ21ELElBQVQsSUFBZTdELEtBQUssQ0FBQ25aLElBQU4sS0FBYSxPQUEvQixFQUF1QztBQUFDNlosYUFBTyxDQUFDbUQsSUFBUixHQUFhLElBQWI7QUFBbUI7O0FBQ3JjLFFBQUcsQ0FBQ25ELE9BQU8sQ0FBQ29ELFFBQVQsS0FBb0I5RCxLQUFLLENBQUNuWixJQUFOLEtBQWEsT0FBYixJQUFzQm1aLEtBQUssQ0FBQ29CLE1BQU4sS0FBZSxPQUF6RCxDQUFILEVBQXFFO0FBQUNWLGFBQU8sQ0FBQ29ELFFBQVIsR0FBaUIsSUFBakI7QUFBdUI7QUFBQyxHQUQyUDs7QUFDMVBsUixPQUFLLENBQUNpTyxhQUFOLEdBQW9CLFVBQVNkLE1BQVQsRUFBZ0J4WCxJQUFoQixFQUFxQnFKLEtBQXJCLEVBQTJCO0FBQUMsUUFBR21PLE1BQU0sQ0FBQ2dFLFlBQVYsRUFBdUI7QUFBQ2hFLFlBQU0sQ0FBQ2lFLG1CQUFQLENBQTJCLE9BQTNCLEVBQW1DakUsTUFBTSxDQUFDZ0UsWUFBMUMsRUFBdUQsS0FBdkQ7QUFBK0Q7O0FBQ3RPaEUsVUFBTSxDQUFDZ0UsWUFBUCxHQUFvQixLQUFLYixJQUFMLENBQVVwVyxJQUFWLENBQWUsSUFBZixFQUFvQnZFLElBQXBCLEVBQXlCcUosS0FBekIsQ0FBcEI7QUFBb0RtTyxVQUFNLENBQUNrRSxnQkFBUCxDQUF3QixPQUF4QixFQUFnQ2xFLE1BQU0sQ0FBQ2dFLFlBQXZDLEVBQW9ELEtBQXBEO0FBQTRELEdBRGpCOztBQUNrQm5SLE9BQUssQ0FBQ3NRLElBQU4sR0FBVyxVQUFTM2EsSUFBVCxFQUFjcUosS0FBZCxFQUFvQmhMLEtBQXBCLEVBQTBCO0FBQUMsUUFBR0EsS0FBSCxFQUFTO0FBQUNBLFdBQUssQ0FBQ3NkLGNBQU47QUFBdUJ0ZCxXQUFLLENBQUN1ZCxlQUFOO0FBQXlCOztBQUNqTixRQUFHLENBQUMsS0FBS2pPLElBQVQsRUFBYztBQUFDLGFBQU8sS0FBUDtBQUFjOztBQUM3QixRQUFHLENBQUMsS0FBS3lKLFNBQUwsQ0FBZXBRLGNBQWYsQ0FBOEJoSCxJQUE5QixDQUFKLEVBQXdDO0FBQUN3RyxXQUFLLENBQUNHLEtBQU4sQ0FBWSwwQkFBd0IzRyxJQUF4QixHQUE2QixvQkFBekM7QUFBK0QsYUFBTyxLQUFQO0FBQWM7O0FBQ3RILFFBQUcsQ0FBQyxLQUFLb1gsU0FBTCxDQUFlcFgsSUFBZixFQUFxQnpELE1BQXpCLEVBQWdDO0FBQUNpSyxXQUFLLENBQUNHLEtBQU4sQ0FBWSxvREFBWjtBQUFrRSxhQUFPLEtBQVA7QUFBYzs7QUFDakgsUUFBRyxDQUFDLEtBQUt5USxTQUFMLENBQWVwWCxJQUFmLEVBQXFCcUosS0FBckIsQ0FBSixFQUFnQztBQUFDN0MsV0FBSyxDQUFDRyxLQUFOLENBQVksNERBQTBEMEMsS0FBdEU7QUFBNkUsYUFBTyxLQUFQO0FBQWM7O0FBQzVIN0MsU0FBSyxDQUFDbUMsYUFBTixDQUFvQixJQUFwQixFQUF5QixXQUF6QixFQUFxQyxZQUFyQyxFQUFrRDNJLElBQWxELEVBQXVEcUosS0FBdkQ7QUFBOEQsU0FBS3FKLE1BQUwsQ0FBWXJKLEtBQVosR0FBa0JBLEtBQWxCO0FBQXdCLFNBQUs4TyxPQUFMLEdBQWEsS0FBS2YsU0FBTCxDQUFlcFgsSUFBZixDQUFiO0FBQWtDLFNBQUttWSxPQUFMLENBQWFuWSxJQUFiLEdBQWtCQSxJQUFsQjtBQUF1QixTQUFLbVksT0FBTCxDQUFhOU8sS0FBYixHQUFtQkEsS0FBbkI7QUFBeUIsU0FBSzhPLE9BQUwsQ0FBYTBELE1BQWIsR0FBb0IsS0FBcEI7QUFBMEIsU0FBS0MsYUFBTDtBQUFxQixTQUFLQyxVQUFMO0FBQWtCLFNBQUtDLGFBQUw7QUFBcUIsU0FBS0MsU0FBTDtBQUFpQixTQUFLQyxTQUFMO0FBQWlCLFNBQUtDLFVBQUw7QUFBa0IsU0FBS0MsUUFBTCxDQUFjLEtBQUt2VixPQUFMLENBQWF5SCxPQUEzQjtBQUFvQyxTQUFLK04sZUFBTDtBQUF1QixTQUFLQyxZQUFMO0FBQW9CLFNBQUtDLFdBQUw7QUFBbUIsU0FBS0MsVUFBTCxDQUFnQixJQUFoQjtBQUFzQixTQUFLQyxJQUFMOztBQUFZLFFBQUcsS0FBSzVWLE9BQUwsQ0FBYTJLLGFBQWhCLEVBQThCO0FBQUMsV0FBS2tMLFdBQUw7QUFBb0I7O0FBQ3plLFFBQUcsS0FBSzdWLE9BQUwsQ0FBYW1LLGlCQUFiLElBQWdDLEtBQUtuSyxPQUFMLENBQWFpSSxRQUFiLENBQXNCckUsT0FBdEIsQ0FBOEIsTUFBOUIsSUFBc0MsQ0FBQyxDQUF2RSxLQUEyRSxDQUFDLEtBQUs1RCxPQUFMLENBQWEySyxhQUFkLElBQTZCLEtBQUs0RixTQUFMLENBQWVwWCxJQUFmLEVBQXFCcUosS0FBckIsRUFBNEIvSyxJQUE1QixLQUFtQyxPQUEzSSxDQUFILEVBQXVKO0FBQUMsV0FBS3FlLGNBQUw7QUFBdUI7O0FBQy9LLFNBQUsvSixNQUFMLENBQVkwSSxJQUFaLEdBQWlCLEtBQWpCO0FBQXVCLFNBQUsxSSxNQUFMLENBQVkrSCxJQUFaLEdBQWlCLElBQWpCO0FBQXVCLEdBUG1FOztBQU9sRXRRLE9BQUssQ0FBQzhILGFBQU4sR0FBb0IsWUFBVTtBQUFDLFFBQUl5SyxLQUFLLEdBQUMsS0FBS0MsY0FBTCxDQUFvQjdoQixNQUFNLENBQUM4aEIsUUFBUCxDQUFnQkMsTUFBcEMsQ0FBVjs7QUFBc0QsUUFBR0gsS0FBSyxDQUFDNVYsY0FBTixDQUFxQixNQUFyQixLQUE4QjRWLEtBQUssQ0FBQzVWLGNBQU4sQ0FBcUIsS0FBckIsQ0FBakMsRUFBNkQ7QUFBQyxVQUFJMlQsSUFBSSxHQUFDLEtBQUtBLElBQUwsQ0FBVXFDLGtCQUFrQixDQUFDSixLQUFLLENBQUNLLElBQVAsQ0FBNUIsRUFBeUNELGtCQUFrQixDQUFDSixLQUFLLENBQUNNLEdBQVAsQ0FBbEIsR0FBOEIsQ0FBdkUsQ0FBVDs7QUFBbUYsVUFBR3ZDLElBQUksS0FBRyxLQUFWLEVBQWdCO0FBQUMsYUFBSzJCLFlBQUwsQ0FBa0IsSUFBbEI7QUFBeUI7QUFBQztBQUFDLEdBQWxSOztBQUFtUmpTLE9BQUssQ0FBQ29TLElBQU4sR0FBVyxZQUFVO0FBQUMsUUFBSXpLLE1BQU0sR0FBQyxLQUFLRCxHQUFMLENBQVNDLE1BQXBCO0FBQUEsUUFBMkJ3QyxNQUFNLEdBQUMsS0FBSzNOLE9BQUwsQ0FBYXdILFdBQWIsR0FBeUIsS0FBekIsR0FBK0IsUUFBakU7QUFBMEUyRCxVQUFNLENBQUNpRSxZQUFQLENBQW9CLGFBQXBCLEVBQWtDLEtBQWxDO0FBQXlDelAsU0FBSyxDQUFDbUQsV0FBTixDQUFrQnFJLE1BQWxCLEVBQXlCLEtBQUtPLEdBQUwsR0FBUyxPQUFsQztBQUEyQy9MLFNBQUssQ0FBQ21ELFdBQU4sQ0FBa0JxSSxNQUFsQixFQUF5QixLQUFLTyxHQUFMLEdBQVMsVUFBbEM7QUFBOEMvTCxTQUFLLENBQUNtRCxXQUFOLENBQWtCcUksTUFBbEIsRUFBeUIsS0FBS08sR0FBTCxHQUFTLGFBQWxDO0FBQWlEL0wsU0FBSyxDQUFDZ08sTUFBTSxHQUFDLE9BQVIsQ0FBTCxDQUFzQnhDLE1BQXRCLEVBQTZCLEtBQUtPLEdBQUwsR0FBUyxNQUF0QztBQUE4Qy9MLFNBQUssQ0FBQ2tELFFBQU4sQ0FBZXNJLE1BQWYsRUFBc0IsS0FBS08sR0FBTCxHQUFTLE9BQS9CO0FBQXlDLEdBQTFXOztBQUEyV2xJLE9BQUssQ0FBQzhTLEtBQU4sR0FBWSxVQUFTOWUsS0FBVCxFQUFlO0FBQUMsUUFBR0EsS0FBSCxFQUFTO0FBQUNBLFdBQUssQ0FBQ3NkLGNBQU47QUFBd0I7O0FBQzN1QixRQUFJM0osTUFBTSxHQUFDLEtBQUtELEdBQUwsQ0FBU0MsTUFBcEI7QUFBQSxRQUEyQm1HLE9BQU8sR0FBQyxLQUFLQSxPQUF4QztBQUFBLFFBQWdEOU8sS0FBSyxHQUFDOE8sT0FBTyxHQUFDQSxPQUFPLENBQUM5TyxLQUFULEdBQWUsV0FBNUU7QUFBQSxRQUF3RnJKLElBQUksR0FBQ21ZLE9BQU8sR0FBQ0EsT0FBTyxDQUFDblksSUFBVCxHQUFjLFdBQWxIO0FBQThId0csU0FBSyxDQUFDbUMsYUFBTixDQUFvQixJQUFwQixFQUF5QixXQUF6QixFQUFxQyxhQUFyQyxFQUFtRDNJLElBQW5ELEVBQXdEcUosS0FBeEQ7O0FBQStELFFBQUcsS0FBS3VKLE1BQUwsQ0FBWVEsVUFBZixFQUEwQjtBQUFDLFdBQUtnSyxjQUFMO0FBQXNCNVcsV0FBSyxDQUFDbUQsV0FBTixDQUFrQnFJLE1BQWxCLEVBQXlCLEtBQUtPLEdBQUwsR0FBUyxhQUFsQztBQUFrRDs7QUFDaFMsU0FBSzJCLEtBQUw7QUFBYSxTQUFLbUosYUFBTDtBQUFxQixTQUFLQyxVQUFMO0FBQWtCLFNBQUtkLFVBQUwsQ0FBZ0IsS0FBaEI7QUFBdUIsU0FBS0YsWUFBTCxDQUFrQixJQUFsQjtBQUF3QixTQUFLTixhQUFMO0FBQXFCaEssVUFBTSxDQUFDaUUsWUFBUCxDQUFvQixhQUFwQixFQUFrQyxJQUFsQztBQUF3Q3pQLFNBQUssQ0FBQ21ELFdBQU4sQ0FBa0JxSSxNQUFsQixFQUF5QixLQUFLTyxHQUFMLEdBQVMsT0FBbEM7QUFBMkMsU0FBS0ssTUFBTCxDQUFZK0gsSUFBWixHQUFpQixLQUFqQjtBQUF3QixHQUYwYzs7QUFFemN0USxPQUFLLENBQUNrUyxXQUFOLEdBQWtCLFlBQVU7QUFBQyxRQUFJcEUsT0FBTyxHQUFDLEtBQUtBLE9BQWpCO0FBQUEsUUFBeUJ0UixPQUFPLEdBQUMsS0FBS0EsT0FBdEM7QUFBQSxRQUE4QzRMLE9BQU8sR0FBQyxLQUFLQSxPQUEzRDs7QUFBbUUsUUFBRyxLQUFLVixHQUFMLENBQVM2RSxPQUFaLEVBQW9CO0FBQUMsV0FBSzdFLEdBQUwsQ0FBUzZFLE9BQVQsQ0FBaUJuUCxLQUFqQixDQUF1QjhWLE9BQXZCLEdBQWdDcEYsT0FBTyxDQUFDcUYsYUFBUixHQUFzQixDQUF2QixHQUEwQixFQUExQixHQUE2QixNQUE1RDtBQUFvRTs7QUFDN1osUUFBRzNXLE9BQU8sQ0FBQ2lJLFFBQVIsQ0FBaUJyRSxPQUFqQixDQUF5QixNQUF6QixJQUFpQyxDQUFDLENBQXJDLEVBQXVDO0FBQUNnSSxhQUFPLENBQUM0QixJQUFSLENBQWE1TSxLQUFiLENBQW1COFYsT0FBbkIsR0FBNEJwRixPQUFPLENBQUNxRixhQUFSLEdBQXNCLENBQXZCLEdBQTBCLEVBQTFCLEdBQTZCLE1BQXhEO0FBQWdFOztBQUN4RyxRQUFHM1csT0FBTyxDQUFDaUksUUFBUixDQUFpQnJFLE9BQWpCLENBQXlCLE1BQXpCLElBQWlDLENBQUMsQ0FBckMsRUFBdUM7QUFBQ2dJLGFBQU8sQ0FBQzZJLElBQVIsQ0FBYTdULEtBQWIsQ0FBbUI4VixPQUFuQixHQUEyQixDQUFDcEYsT0FBTyxDQUFDbUQsSUFBVCxHQUFjLE1BQWQsR0FBcUIsRUFBaEQ7QUFBb0Q7O0FBQzVGLFFBQUd6VSxPQUFPLENBQUNpSSxRQUFSLENBQWlCckUsT0FBakIsQ0FBeUIsVUFBekIsSUFBcUMsQ0FBQyxDQUF6QyxFQUEyQztBQUFDZ0ksYUFBTyxDQUFDOEksUUFBUixDQUFpQjlULEtBQWpCLENBQXVCOFYsT0FBdkIsR0FBK0IsQ0FBQ3BGLE9BQU8sQ0FBQ29ELFFBQVQsR0FBa0IsTUFBbEIsR0FBeUIsRUFBeEQ7QUFBNEQ7O0FBQ3hHLFNBQUtrQyxrQkFBTDtBQUEyQixHQUp5TTs7QUFJeE1wVCxPQUFLLENBQUNvVCxrQkFBTixHQUF5QixZQUFVO0FBQUMsUUFBRyxLQUFLNVcsT0FBTCxDQUFha0ksUUFBaEIsRUFBeUI7QUFBQyxVQUFJaE4sSUFBSSxHQUFDLEtBQUttTSxNQUFMLENBQVloTCxLQUFaLEdBQWtCLEdBQWxCLElBQXVCLEtBQUsrRyxPQUFMLENBQWErSSxXQUFwQyxJQUFpRCxDQUFDLEtBQUtuTSxPQUFMLENBQWFtSSxhQUF4RTtBQUFzRixXQUFLeUQsT0FBTCxDQUFhaUwsSUFBYixDQUFrQmpXLEtBQWxCLENBQXdCOFYsT0FBeEIsR0FBZ0MsS0FBSzlLLE9BQUwsQ0FBYXFJLElBQWIsQ0FBa0JyVCxLQUFsQixDQUF3QjhWLE9BQXhCLEdBQWlDLEtBQUtwRixPQUFMLENBQWE1YixNQUFiLEdBQW9CLENBQXBCLElBQXVCLENBQUN3RixJQUF6QixHQUErQixFQUEvQixHQUFrQyxNQUFsRztBQUEwRztBQUFDLEdBQS9QOztBQUFnUXNJLE9BQUssQ0FBQzhSLFVBQU4sR0FBaUIsWUFBVTtBQUFDLFNBQUt2SixNQUFMLENBQVkxRCxPQUFaLEdBQW9CLEVBQUUsQ0FBQyxLQUFLckksT0FBTCxDQUFhdUksa0JBQWQsS0FBbUMsS0FBS2xCLE1BQUwsQ0FBWWhMLEtBQVosSUFBbUIsR0FBbkIsSUFBd0IsS0FBS2dMLE1BQUwsQ0FBWS9LLE1BQVosSUFBb0IsR0FBL0UsQ0FBRixDQUFwQjtBQUEyRyxTQUFLNE8sR0FBTCxDQUFTN0MsT0FBVCxDQUFpQnpILEtBQWpCLENBQXVCOFYsT0FBdkIsR0FBK0IsS0FBSzNLLE1BQUwsQ0FBWTFELE9BQVosR0FBb0IsRUFBcEIsR0FBdUIsTUFBdEQ7QUFBOEQsR0FBck07O0FBQXNNN0UsT0FBSyxDQUFDMlIsYUFBTixHQUFvQixZQUFVO0FBQUMsUUFBRyxDQUFDLEtBQUtuVixPQUFMLENBQWErSCxTQUFqQixFQUEyQjtBQUFDLFVBQUkrTCxJQUFJLEdBQUMsS0FBSy9ILE1BQUwsQ0FBWStILElBQXJCO0FBQUEsVUFBMEIvTCxTQUFTLEdBQUMrTCxJQUFJLEtBQUcsV0FBUCxJQUFvQixDQUFDQSxJQUFyQixHQUEwQixJQUExQixHQUErQixLQUFuRTtBQUF5RTdTLGNBQVEsQ0FBQ3NPLElBQVQsQ0FBYzNPLEtBQWQsQ0FBb0JrSSxRQUFwQixHQUE2QjdILFFBQVEsQ0FBQzZWLGVBQVQsQ0FBeUJsVyxLQUF6QixDQUErQmtJLFFBQS9CLEdBQXdDZixTQUFTLEdBQUMsUUFBRCxHQUFVLEVBQXhGO0FBQTRGO0FBQUMsR0FBak87O0FBQWtPdkUsT0FBSyxDQUFDbVMsVUFBTixHQUFpQixVQUFTalksSUFBVCxFQUFjO0FBQUMsUUFBSW1CLEdBQUcsR0FBQzFLLE1BQVI7QUFBQSxRQUFlb1gsR0FBRyxHQUFDdEssUUFBbkI7QUFBQSxRQUE0QjJOLEdBQUcsR0FBQyxLQUFLNU8sT0FBckM7QUFBQSxRQUE2Q21MLE1BQU0sR0FBQyxLQUFLRCxHQUFMLENBQVNDLE1BQTdEO0FBQUEsUUFBb0VTLE9BQU8sR0FBQyxLQUFLQSxPQUFqRjtBQUFBLFFBQXlGbUwsWUFBekY7O0FBQXNHLFNBQUksSUFBSXRmLElBQVIsSUFBZ0JtVSxPQUFoQixFQUF3QjtBQUFDLFVBQUdBLE9BQU8sQ0FBQ3pMLGNBQVIsQ0FBdUIxSSxJQUF2QixDQUFILEVBQWdDO0FBQUMsWUFBSXlULEdBQUcsR0FBRXpULElBQUksS0FBRyxPQUFSLEdBQWlCbVUsT0FBTyxDQUFDblUsSUFBRCxDQUF4QixHQUErQm9ILEdBQXZDO0FBQTJDYyxhQUFLLENBQUM0QixZQUFOLENBQW1CLElBQW5CLEVBQXdCMkosR0FBeEIsRUFBNEIsQ0FBQyxPQUFELEVBQVMsVUFBVCxDQUE1QixFQUFpRFUsT0FBTyxDQUFDblUsSUFBRCxDQUFQLENBQWNELEtBQS9ELEVBQXFFa0csSUFBckU7QUFBNEU7QUFBQzs7QUFDNS9CaUMsU0FBSyxDQUFDNEIsWUFBTixDQUFtQixJQUFuQixFQUF3QjRKLE1BQXhCLEVBQStCLEtBQUtjLFVBQUwsQ0FBZ0I3RyxLQUEvQyxFQUFxRCxZQUFyRCxFQUFrRTFILElBQWxFO0FBQXdFaUMsU0FBSyxDQUFDNEIsWUFBTixDQUFtQixJQUFuQixFQUF3QjFDLEdBQXhCLEVBQTRCLENBQUMsU0FBRCxDQUE1QixFQUF3QyxTQUF4QyxFQUFrRG5CLElBQWxEO0FBQXdEaUMsU0FBSyxDQUFDNEIsWUFBTixDQUFtQixJQUFuQixFQUF3QjFDLEdBQXhCLEVBQTRCLENBQUMsUUFBRCxFQUFVLG1CQUFWLENBQTVCLEVBQTJELFFBQTNELEVBQW9FbkIsSUFBcEU7QUFBMEVpQyxTQUFLLENBQUM0QixZQUFOLENBQW1CLElBQW5CLEVBQXdCNEosTUFBeEIsRUFBK0IsQ0FBQyxlQUFELEVBQWlCLHFCQUFqQixFQUF1QyxnQkFBdkMsRUFBd0QsZ0JBQXhELEVBQXlFLGlCQUF6RSxDQUEvQixFQUEySCxRQUEzSDtBQUFxSXhMLFNBQUssQ0FBQzRCLFlBQU4sQ0FBbUIsSUFBbkIsRUFBd0I0SixNQUF4QixFQUErQixDQUFDLFVBQUQsQ0FBL0IsRUFBNEMsYUFBNUMsRUFBMER6TixJQUExRDs7QUFBZ0UsUUFBRyxLQUFLMEYsT0FBTCxDQUFhbUosVUFBaEIsRUFBMkI7QUFBQzVNLFdBQUssQ0FBQzRCLFlBQU4sQ0FBbUIsSUFBbkIsRUFBd0JnSyxHQUF4QixFQUE0QixDQUFDLEtBQUtuSSxPQUFMLENBQWFtSixVQUFiLENBQXdCUyxNQUF6QixDQUE1QixFQUE2RCxrQkFBN0QsRUFBZ0Z0UCxJQUFoRjtBQUF1Rjs7QUFDbGdCLFFBQUdrUixHQUFHLENBQUNoSCxPQUFQLEVBQWU7QUFBQ2pJLFdBQUssQ0FBQzRCLFlBQU4sQ0FBbUIsSUFBbkIsRUFBd0IxQyxHQUF4QixFQUE0QixDQUFDLFVBQUQsQ0FBNUIsRUFBeUMsVUFBekMsRUFBb0RuQixJQUFwRDtBQUEyRDs7QUFDM0UsUUFBR2tSLEdBQUcsQ0FBQ2pILFVBQUosR0FBZSxDQUFsQixFQUFvQjtBQUFDaEksV0FBSyxDQUFDNEIsWUFBTixDQUFtQixJQUFuQixFQUF3QjRKLE1BQXhCLEVBQStCLENBQUMsV0FBRCxDQUEvQixFQUE2QyxXQUE3QyxFQUF5RHpOLElBQXpEO0FBQWdFOztBQUNyRixRQUFHLENBQUNrUixHQUFHLENBQUM5RyxXQUFSLEVBQW9CO0FBQUNuSSxXQUFLLENBQUM0QixZQUFOLENBQW1CLElBQW5CLEVBQXdCNEosTUFBeEIsRUFBK0IsQ0FBQyxhQUFELENBQS9CLEVBQStDLGFBQS9DLEVBQTZEek4sSUFBN0Q7QUFBb0U7O0FBQ3pGLFFBQUcsQ0FBQ2tSLEdBQUcsQ0FBQy9HLFVBQVIsRUFBbUI7QUFBQyxXQUFLbVAsYUFBTCxDQUFtQnRaLElBQW5CO0FBQTBCOztBQUM5QyxRQUFHa1IsR0FBRyxDQUFDcEYsWUFBUCxFQUFvQjtBQUFDdU4sa0JBQVksR0FBQyxjQUFiO0FBQTZCLEtBQWxELE1BQ0ssSUFBR25JLEdBQUcsQ0FBQzFGLFdBQVAsRUFBbUI7QUFBQzZOLGtCQUFZLEdBQUMsYUFBYjtBQUE0QixLQUFoRCxNQUNBLElBQUduSSxHQUFHLENBQUNqRixhQUFQLEVBQXFCO0FBQUNvTixrQkFBWSxHQUFDLGVBQWI7QUFBOEI7O0FBQ3pELFFBQUdBLFlBQUgsRUFBZ0I7QUFBQ3BYLFdBQUssQ0FBQzRCLFlBQU4sQ0FBbUIsSUFBbkIsRUFBd0I0SixNQUF4QixFQUErQixDQUFDLEtBQUsvSCxPQUFMLENBQWF5RSxVQUFkLENBQS9CLEVBQXlEa1AsWUFBekQsRUFBc0VyWixJQUF0RTtBQUE2RTtBQUFDLEdBVHFtQjs7QUFTcG1COEYsT0FBSyxDQUFDeVQsTUFBTixHQUFhLFVBQVN6ZixLQUFULEVBQWU7QUFBQyxRQUFHQSxLQUFLLENBQUMwZixZQUFOLEtBQXFCLFlBQXJCLElBQW1DMWYsS0FBSyxDQUFDK2IsTUFBTixLQUFlLEtBQUtySSxHQUFMLENBQVNDLE1BQTlELEVBQXFFO0FBQUMsVUFBSWhTLElBQUksR0FBQyxLQUFLbVksT0FBTCxDQUFhblksSUFBdEI7QUFBQSxVQUEyQnFKLEtBQUssR0FBQyxLQUFLOE8sT0FBTCxDQUFhOU8sS0FBOUM7O0FBQW9ELFVBQUcsQ0FBQyxLQUFLdUosTUFBTCxDQUFZK0gsSUFBaEIsRUFBcUI7QUFBQyxhQUFLbUIsYUFBTDtBQUFxQnRWLGFBQUssQ0FBQ21DLGFBQU4sQ0FBb0IsSUFBcEIsRUFBeUIsV0FBekIsRUFBcUMsWUFBckMsRUFBa0QzSSxJQUFsRCxFQUF1RHFKLEtBQXZEO0FBQStELE9BQTFHLE1BQThHO0FBQUM3QyxhQUFLLENBQUNtQyxhQUFOLENBQW9CLElBQXBCLEVBQXlCLFdBQXpCLEVBQXFDLFdBQXJDLEVBQWlEM0ksSUFBakQsRUFBc0RxSixLQUF0RDtBQUE4RDtBQUFDO0FBQUMsR0FBdFU7O0FBQXVVZ0IsT0FBSyxDQUFDMlQsUUFBTixHQUFlLFVBQVMzZixLQUFULEVBQWU7QUFBQyxRQUFJSCxDQUFDLEdBQUNHLEtBQUssR0FBQ0EsS0FBRCxHQUFPckQsTUFBTSxDQUFDcUQsS0FBekI7QUFBQSxRQUErQjRmLElBQUksR0FBQy9mLENBQUMsQ0FBQ2dnQixhQUFGLElBQWlCaGdCLENBQUMsQ0FBQ2lnQixTQUF2RDs7QUFBaUUsUUFBRyxDQUFDRixJQUFELElBQU9BLElBQUksQ0FBQ0csUUFBTCxLQUFnQixNQUExQixFQUFpQztBQUFDLFdBQUs5QixZQUFMO0FBQXFCO0FBQUMsR0FBeEo7O0FBQXlKalMsT0FBSyxDQUFDZ1UsU0FBTixHQUFnQixZQUFVO0FBQUMsUUFBSXJNLE1BQU0sR0FBQyxLQUFLRCxHQUFMLENBQVNDLE1BQXBCO0FBQUEsUUFBMkJzTSxTQUFTLEdBQUMsS0FBSy9MLEdBQUwsR0FBUyxPQUE5QztBQUFzRHJPLGdCQUFZLENBQUMsS0FBSzBPLE1BQUwsQ0FBWTJMLElBQWIsQ0FBWjtBQUErQixTQUFLM0wsTUFBTCxDQUFZMkwsSUFBWixHQUFpQnhnQixVQUFVLENBQUMsWUFBVTtBQUFDLFVBQUcsQ0FBQ3lJLEtBQUssQ0FBQ2dELFFBQU4sQ0FBZXdJLE1BQWYsRUFBc0IsS0FBS08sR0FBTCxHQUFTLGVBQS9CLENBQUosRUFBb0Q7QUFBQy9MLGFBQUssQ0FBQ2tELFFBQU4sQ0FBZXNJLE1BQWYsRUFBc0JzTSxTQUF0QjtBQUFrQztBQUFDLEtBQW5HLENBQW9HL1osSUFBcEcsQ0FBeUcsSUFBekcsQ0FBRCxFQUFnSCxLQUFLc0MsT0FBTCxDQUFhMkgsVUFBN0gsQ0FBM0I7QUFBb0toSSxTQUFLLENBQUNtRCxXQUFOLENBQWtCcUksTUFBbEIsRUFBeUJzTSxTQUF6QjtBQUFxQyxHQUF6VDs7QUFBMFRqVSxPQUFLLENBQUNzRSxXQUFOLEdBQWtCLFVBQVN0USxLQUFULEVBQWU7QUFBQyxRQUFJK2IsTUFBTSxHQUFDL2IsS0FBSyxDQUFDK2IsTUFBakI7QUFBQSxRQUF3QmhYLE9BQU8sR0FBQ2dYLE1BQU0sQ0FBQ2hYLE9BQXZDO0FBQUEsUUFBK0M0RSxTQUFTLEdBQUNvUyxNQUFNLENBQUNwUyxTQUFoRTs7QUFBMEUsUUFBRzVFLE9BQU8sS0FBRyxLQUFWLElBQWlCQSxPQUFPLEtBQUcsT0FBM0IsSUFBb0M0RSxTQUFTLENBQUN5QyxPQUFWLENBQWtCLEtBQUs4SCxHQUFMLEdBQVMsUUFBM0IsSUFBcUMsQ0FBQyxDQUExRSxJQUE2RXZLLFNBQVMsQ0FBQ3lDLE9BQVYsQ0FBa0IsS0FBSzhILEdBQUwsR0FBUyxXQUEzQixJQUF3QyxDQUFDLENBQXRILElBQXlIdkssU0FBUyxLQUFHLEtBQUt1SyxHQUFMLEdBQVMsUUFBakosRUFBMEo7QUFBQ2xVLFdBQUssQ0FBQ3NkLGNBQU47QUFBd0I7QUFBQyxHQUFoUzs7QUFBaVN0UixPQUFLLENBQUN3VCxhQUFOLEdBQW9CLFVBQVN0WixJQUFULEVBQWM7QUFBQyxRQUFJNk4sR0FBRyxHQUFDdEssUUFBUjtBQUFBLFFBQWlCcEMsR0FBRyxHQUFDMUssTUFBckI7O0FBQTRCLFFBQUl3akIsT0FBTyxHQUFDLFNBQVJBLE9BQVEsQ0FBU3RnQixDQUFULEVBQVc7QUFBQyxVQUFHLENBQUMsS0FBS3VnQixJQUFMLENBQVV2Z0IsQ0FBVixDQUFKLEVBQWlCO0FBQUM7QUFBUTs7QUFDNXdDQSxPQUFDLEdBQUNBLENBQUMsSUFBRXdILEdBQUcsQ0FBQ3JILEtBQVQ7O0FBQWUsVUFBR0gsQ0FBQyxDQUFDeWQsY0FBTCxFQUFvQjtBQUFDemQsU0FBQyxDQUFDeWQsY0FBRjtBQUFtQnpkLFNBQUMsQ0FBQ3dnQixXQUFGLEdBQWMsS0FBZDtBQUFvQixlQUFPLEtBQVA7QUFBYztBQUFDLEtBRGdvQzs7QUFDL25DaFosT0FBRyxDQUFDaVosT0FBSixHQUFZalosR0FBRyxDQUFDa1osV0FBSixHQUFnQmxaLEdBQUcsQ0FBQzROLFlBQUosR0FBaUJsQixHQUFHLENBQUNrQixZQUFKLEdBQWlCbEIsR0FBRyxDQUFDa0IsWUFBSixHQUFpQi9PLElBQUksR0FBQ2lhLE9BQU8sQ0FBQ2phLElBQVIsQ0FBYSxJQUFiLENBQUQsR0FBb0IsSUFBdkc7QUFBNkcsR0FEbTlCOztBQUNsOUI4RixPQUFLLENBQUNnRyxZQUFOLEdBQW1CLFVBQVNoUyxLQUFULEVBQWU7QUFBQyxRQUFHLENBQUMsS0FBS29nQixJQUFMLENBQVVwZ0IsS0FBVixDQUFKLEVBQXFCO0FBQUM7QUFBUTs7QUFDMVEsUUFBSVEsSUFBSSxHQUFDLEtBQUtnZ0IsY0FBTCxDQUFvQnhnQixLQUFwQixDQUFUOztBQUFvQyxRQUFHUSxJQUFJLElBQUVBLElBQUksQ0FBQ2lnQixNQUFkLEVBQXFCO0FBQUMsVUFBSUMsSUFBSSxHQUFDLEtBQUtDLE9BQUwsRUFBVDtBQUFBLFVBQXdCaFYsS0FBSyxHQUFDK1UsSUFBSSxDQUFDOVQsVUFBTCxDQUFnQmxCLENBQWhCLElBQW1CZ1YsSUFBSSxDQUFDN1MsUUFBTCxDQUFjbkMsQ0FBL0Q7QUFBaUVDLFdBQUssR0FBQy9ELElBQUksQ0FBQ2daLEdBQUwsQ0FBUyxLQUFLcFksT0FBTCxDQUFhc0osT0FBdEIsRUFBOEJsSyxJQUFJLENBQUNDLEdBQUwsQ0FBUyxDQUFULEVBQVc4RCxLQUFLLEdBQUMvRCxJQUFJLENBQUNpWixHQUFMLENBQVNyZ0IsSUFBSSxDQUFDaWdCLE1BQWQsSUFBc0JqZ0IsSUFBSSxDQUFDaWdCLE1BQTVDLENBQTlCLENBQU47QUFBeUYsV0FBS3pCLGFBQUw7QUFBcUIsV0FBS3BOLE1BQUwsQ0FBWTVSLEtBQUssQ0FBQzhnQixPQUFsQixFQUEwQjlnQixLQUFLLENBQUMrZ0IsT0FBaEMsRUFBd0NuWixJQUFJLENBQUNvSCxLQUFMLENBQVdyRCxLQUFLLEdBQUMsRUFBakIsSUFBcUIsRUFBN0Q7QUFBa0U7QUFBQyxHQURuRzs7QUFDb0dLLE9BQUssQ0FBQzBGLFdBQU4sR0FBa0IsVUFBUzFSLEtBQVQsRUFBZTtBQUFDLFFBQUcsQ0FBQyxLQUFLb2dCLElBQUwsQ0FBVXBnQixLQUFWLENBQUosRUFBcUI7QUFBQztBQUFROztBQUM3VyxRQUFJUSxJQUFJLEdBQUMsS0FBS2dnQixjQUFMLENBQW9CeGdCLEtBQXBCLENBQVQ7O0FBQW9DLFFBQUdRLElBQUksSUFBRUEsSUFBSSxDQUFDd2dCLEtBQWQsRUFBb0I7QUFBQyxXQUFLeGdCLElBQUksQ0FBQ3dnQixLQUFMLEdBQVcsS0FBS0MsS0FBTCxFQUFYLEdBQXdCLENBQXhCLEdBQTBCLE1BQTFCLEdBQWlDLE1BQXRDO0FBQWlEO0FBQUMsR0FEa007O0FBQ2pNalYsT0FBSyxDQUFDbUcsYUFBTixHQUFvQixVQUFTblMsS0FBVCxFQUFlO0FBQUMsUUFBRyxDQUFDLEtBQUtvZ0IsSUFBTCxDQUFVcGdCLEtBQVYsQ0FBSixFQUFxQjtBQUFDO0FBQVE7O0FBQzlLQSxTQUFLLENBQUNzZCxjQUFOO0FBQXVCLFNBQUt3QixLQUFMO0FBQWMsR0FEdUU7O0FBQ3RFOVMsT0FBSyxDQUFDa1YsV0FBTixHQUFrQixVQUFTbGhCLEtBQVQsRUFBZTtBQUFDLFFBQUk4YyxJQUFJLEdBQUM5YyxLQUFLLENBQUMrYixNQUFmOztBQUFzQixXQUFNZSxJQUFOLEVBQVc7QUFBQyxVQUFHLENBQUMsT0FBRCxFQUFTLE9BQVQsRUFBaUIsR0FBakIsRUFBc0IxUSxPQUF0QixDQUE4QjBRLElBQUksQ0FBQy9YLE9BQW5DLElBQTRDLENBQUMsQ0FBaEQsRUFBa0Q7QUFBQztBQUFROztBQUNySytYLFVBQUksR0FBQ0EsSUFBSSxDQUFDcUUsYUFBVjtBQUF5Qjs7QUFDekJuaEIsU0FBSyxDQUFDc2QsY0FBTjtBQUF3QixHQUZjOztBQUVidFIsT0FBSyxDQUFDb1YsTUFBTixHQUFhLFVBQVNwaEIsS0FBVCxFQUFlO0FBQUMsU0FBSzBULEdBQUwsQ0FBU3NFLE1BQVQsQ0FBZ0JsVCxNQUFoQixHQUF1QixLQUFLNE8sR0FBTCxDQUFTc0UsTUFBVCxDQUFnQkMsWUFBdkM7QUFBb0QsU0FBS3BDLEtBQUw7QUFBYSxTQUFLK0gsU0FBTDtBQUFpQixTQUFLeUQsaUJBQUw7QUFBeUIsU0FBS3ZELFVBQUw7QUFBa0IsU0FBS3dELFdBQUw7QUFBbUIsU0FBS3RELGVBQUw7QUFBdUIsU0FBS29CLGtCQUFMO0FBQTBCLFNBQUs3SyxNQUFMLENBQVkwSSxJQUFaLEdBQWlCLEtBQWpCO0FBQXVCOVUsU0FBSyxDQUFDbUQsV0FBTixDQUFrQixLQUFLb0ksR0FBTCxDQUFTQyxNQUEzQixFQUFrQyxLQUFLTyxHQUFMLEdBQVMsVUFBM0M7QUFBdUQvTCxTQUFLLENBQUNtQyxhQUFOLENBQW9CLElBQXBCLEVBQXlCLFdBQXpCLEVBQXFDLFFBQXJDLEVBQThDdEssS0FBOUM7QUFBc0QsR0FBbFc7O0FBQW1XZ00sT0FBSyxDQUFDc1YsV0FBTixHQUFrQixZQUFVO0FBQUMsUUFBSWpOLE1BQU0sR0FBQyxLQUFLQSxNQUFoQjs7QUFBdUIsU0FBSSxJQUFJclcsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDcVcsTUFBTSxDQUFDblcsTUFBckIsRUFBNEJGLENBQUMsRUFBN0IsRUFBZ0M7QUFBQyxVQUFHLENBQUMsS0FBSzhiLE9BQVQsRUFBaUI7QUFBQztBQUFPOztBQUMxZSxVQUFJVixLQUFLLEdBQUMsS0FBS1UsT0FBTCxDQUFhekYsTUFBTSxDQUFDclcsQ0FBRCxDQUFOLENBQVVvYixLQUF2QixDQUFWOztBQUF3QyxVQUFHQSxLQUFLLEtBQUlBLEtBQUssQ0FBQ2pDLEdBQU4sSUFBV2lDLEtBQUssQ0FBQ2pDLEdBQU4sQ0FBVXFHLE1BQXRCLElBQWdDcEUsS0FBSyxDQUFDakMsR0FBTixJQUFXLENBQUMsT0FBRCxFQUFTLFFBQVQsRUFBa0IsTUFBbEIsRUFBMEIvSyxPQUExQixDQUFrQ2dOLEtBQUssQ0FBQ25aLElBQXhDLElBQThDLENBQUMsQ0FBN0YsQ0FBUixFQUF5RztBQUFDLGFBQUtzaEIsWUFBTCxDQUFrQm5JLEtBQWxCLEVBQXdCL0UsTUFBTSxDQUFDclcsQ0FBRCxDQUE5QjtBQUFvQztBQUFDO0FBQUMsR0FEb007O0FBQ25NZ08sT0FBSyxDQUFDb1UsSUFBTixHQUFXLFVBQVNwZ0IsS0FBVCxFQUFlO0FBQUMsUUFBSTJCLElBQUksR0FBQzNCLEtBQUssQ0FBQytiLE1BQU4sQ0FBYXBTLFNBQXRCO0FBQWdDaEksUUFBSSxHQUFDLE9BQU9BLElBQVAsS0FBYyxRQUFkLEdBQXVCQSxJQUF2QixHQUE0QkEsSUFBSSxDQUFDNmYsT0FBdEM7QUFBOEMsV0FBTzdmLElBQUksQ0FBQ3lLLE9BQUwsQ0FBYSxLQUFLOEgsR0FBbEIsSUFBdUIsQ0FBQyxDQUEvQjtBQUFrQyxHQUEzSTs7QUFBNElsSSxPQUFLLENBQUN5VixVQUFOLEdBQWlCLFlBQVU7QUFBQyxRQUFJckksS0FBSyxHQUFDLEtBQUtzSSxRQUFMLEVBQVY7QUFBQSxRQUEwQnpFLElBQUksR0FBQyxLQUEvQjs7QUFBcUMsUUFBRzdELEtBQUssQ0FBQ25aLElBQU4sS0FBYSxPQUFiLElBQXNCbVosS0FBSyxDQUFDakMsR0FBNUIsSUFBaUNpQyxLQUFLLENBQUNqQyxHQUFOLENBQVV3SyxJQUEzQyxJQUFpRHZJLEtBQUssQ0FBQ2pDLEdBQU4sQ0FBVXdLLElBQVYsQ0FBZWhXLEtBQWYsR0FBcUIsQ0FBekUsRUFBMkU7QUFBQ3NSLFVBQUksR0FBQyxJQUFMO0FBQVc7O0FBQzdkLFNBQUt2SixHQUFMLENBQVNDLE1BQVQsQ0FBZ0JpRSxZQUFoQixDQUE2QixXQUE3QixFQUF5Q3FGLElBQXpDO0FBQStDLFdBQU9BLElBQVA7QUFBYSxHQUR5UTs7QUFDeFFqUixPQUFLLENBQUM0VixjQUFOLEdBQXFCLFlBQVU7QUFBQyxRQUFJeEksS0FBSyxHQUFDLEtBQUtzSSxRQUFMLEVBQVY7QUFBQSxRQUEwQnhFLFFBQVEsR0FBQyxJQUFuQzs7QUFBd0MsUUFBRzlELEtBQUssQ0FBQ25aLElBQU4sS0FBYSxPQUFiLElBQXNCbVosS0FBSyxDQUFDb0IsTUFBTixLQUFlLE9BQXhDLEVBQWdEO0FBQUMwQyxjQUFRLEdBQUMsS0FBVDtBQUFnQjs7QUFDdE0sU0FBS3hKLEdBQUwsQ0FBU0MsTUFBVCxDQUFnQmlFLFlBQWhCLENBQTZCLGVBQTdCLEVBQTZDc0YsUUFBN0M7QUFBdUQsV0FBT0EsUUFBUDtBQUFpQixHQURYOztBQUNZbFIsT0FBSyxDQUFDaVYsS0FBTixHQUFZLFlBQVU7QUFBQyxXQUFPLEtBQUt6WSxPQUFMLENBQWF3SCxXQUFiLEdBQXlCLENBQUMsQ0FBMUIsR0FBNEIsQ0FBbkM7QUFBc0MsR0FBN0Q7O0FBQThEaEUsT0FBSyxDQUFDNlYsT0FBTixHQUFjLFVBQVNqWixFQUFULEVBQVlrWixLQUFaLEVBQWtCO0FBQUMsUUFBSUMsT0FBSjs7QUFBWSxRQUFHLE9BQU9uWixFQUFFLENBQUMsS0FBSzRHLE9BQU4sQ0FBVCxLQUEwQixXQUE3QixFQUF5QztBQUFDdVMsYUFBTyxHQUFDLEtBQUtyUyxLQUFMLENBQVdDLEdBQVgsRUFBUjtBQUF5Qi9HLFFBQUUsQ0FBQyxLQUFLNEcsT0FBTixDQUFGLEdBQWlCdVMsT0FBakI7QUFBeUIsV0FBS3JTLEtBQUwsQ0FBV3FTLE9BQVgsSUFBb0IsRUFBcEI7QUFBd0IsS0FBcEgsTUFBd0g7QUFBQ0EsYUFBTyxHQUFDblosRUFBRSxDQUFDLEtBQUs0RyxPQUFOLENBQVY7QUFBMEI7O0FBQ3ZVLFNBQUksSUFBSS9OLElBQVIsSUFBZ0JxZ0IsS0FBaEIsRUFBc0I7QUFBQyxVQUFHQSxLQUFLLENBQUNuWixjQUFOLENBQXFCbEgsSUFBckIsQ0FBSCxFQUE4QjtBQUFDLGFBQUtpTyxLQUFMLENBQVdxUyxPQUFYLEVBQW9CdGdCLElBQXBCLElBQTBCcWdCLEtBQUssQ0FBQ3JnQixJQUFELENBQS9CO0FBQXVDO0FBQUM7QUFBQyxHQUR3Qzs7QUFDdkN1SyxPQUFLLENBQUNpUCxPQUFOLEdBQWMsVUFBU3JTLEVBQVQsRUFBWTtBQUFDLFdBQU8sS0FBSzhHLEtBQUwsQ0FBVzlHLEVBQUUsQ0FBQyxLQUFLNEcsT0FBTixDQUFiLEtBQThCLEVBQXJDO0FBQXlDLEdBQXBFOztBQUFxRXhELE9BQUssQ0FBQ2dXLGNBQU4sR0FBcUIsWUFBVTtBQUFDLFFBQUlqTSxLQUFLLEdBQUMsS0FBS2hHLE1BQWY7QUFBc0IsV0FBT2dHLEtBQUssQ0FBQ2pSLE1BQU4sR0FBYSxDQUFiLElBQWdCaVIsS0FBSyxDQUFDbFIsS0FBTixHQUFZLENBQTVCLEdBQThCa1IsS0FBSyxDQUFDalIsTUFBTixHQUFhOEMsSUFBSSxDQUFDZ1osR0FBTCxDQUFTLEVBQVQsRUFBWTdLLEtBQUssQ0FBQzVFLE1BQWxCLElBQTBCLENBQXJFLEdBQXVFLENBQTlFO0FBQWlGLEdBQXZJOztBQUF3SW5GLE9BQUssQ0FBQzBWLFFBQU4sR0FBZSxZQUFVO0FBQUMsUUFBSTVILE9BQU8sR0FBQyxLQUFLQSxPQUFqQjtBQUF5QixXQUFPQSxPQUFPLEdBQUNBLE9BQU8sQ0FBQ0EsT0FBTyxDQUFDOU8sS0FBVCxDQUFSLEdBQXdCLElBQXRDO0FBQTRDLEdBQS9GOztBQUFnR2dCLE9BQUssQ0FBQzJVLE9BQU4sR0FBYyxZQUFVO0FBQUMsUUFBSXRNLE1BQU0sR0FBQyxLQUFLQSxNQUFoQjtBQUFBLFFBQXVCckosS0FBSyxHQUFDN0MsS0FBSyxDQUFDNEMsTUFBTixDQUFhc0osTUFBTSxDQUFDblcsTUFBcEIsRUFBMkJtVyxNQUFNLENBQUNySixLQUFsQyxDQUE3QjtBQUFzRSxXQUFPLEtBQUtzSixLQUFMLENBQVd0SixLQUFYLENBQVA7QUFBMEIsR0FBekg7O0FBQTBIZ0IsT0FBSyxDQUFDeVIsYUFBTixHQUFvQixZQUFVO0FBQUMsU0FBSSxJQUFJemYsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDLEtBQUtxVyxNQUFMLENBQVluVyxNQUExQixFQUFpQ0YsQ0FBQyxFQUFsQyxFQUFxQztBQUFDLFVBQUk4UixLQUFLLEdBQUMsS0FBS3VFLE1BQUwsQ0FBWXJXLENBQVosQ0FBVjtBQUF5QixXQUFLaWtCLFdBQUwsQ0FBaUJuUyxLQUFqQjtBQUF3QixXQUFLb1MsV0FBTCxDQUFpQnBTLEtBQWpCO0FBQXdCQSxXQUFLLENBQUM5RSxLQUFOLEdBQVk4RSxLQUFLLENBQUNzSixLQUFOLEdBQVksSUFBeEI7QUFBOEI7O0FBQ25yQixTQUFLOEksV0FBTCxDQUFpQixLQUFLeE8sR0FBTCxDQUFTa0YsWUFBMUI7QUFBeUMsR0FEOGQ7O0FBQzdkNU0sT0FBSyxDQUFDd1MsY0FBTixHQUFxQixVQUFTRSxNQUFULEVBQWdCO0FBQUMsUUFBSXlELE1BQU0sR0FBQyxFQUFYO0FBQWN6RCxVQUFNLENBQUN0RSxNQUFQLENBQWMsQ0FBZCxFQUFpQmpWLEtBQWpCLENBQXVCLEdBQXZCLEVBQTRCaUYsT0FBNUIsQ0FBb0MsVUFBU2dZLEtBQVQsRUFBZTtBQUFDQSxXQUFLLEdBQUNBLEtBQUssQ0FBQ2pkLEtBQU4sQ0FBWSxHQUFaLENBQU47QUFBdUJnZCxZQUFNLENBQUN4RCxrQkFBa0IsQ0FBQ3lELEtBQUssQ0FBQyxDQUFELENBQU4sQ0FBbkIsQ0FBTixHQUFxQ0EsS0FBSyxDQUFDbGtCLE1BQU4sR0FBYSxDQUFiLEdBQWV5Z0Isa0JBQWtCLENBQUN5RCxLQUFLLENBQUMsQ0FBRCxDQUFOLENBQWpDLEdBQTRDLEVBQWpGO0FBQXFGLEtBQWhLO0FBQWtLLFdBQU9ELE1BQVA7QUFBZSxHQUFyTzs7QUFBc09uVyxPQUFLLENBQUNxVyxjQUFOLEdBQXFCLFVBQVNDLEdBQVQsRUFBYTtBQUFDLFFBQUk1RCxNQUFNLEdBQUMvaEIsTUFBTSxDQUFDOGhCLFFBQVAsQ0FBZ0JDLE1BQTNCO0FBQUEsUUFBa0NILEtBQUssR0FBQyxLQUFLQyxjQUFMLENBQW9CRSxNQUFwQixDQUF4QztBQUFvRUEsVUFBTSxHQUFDNkQsU0FBUyxDQUFDN0QsTUFBRCxDQUFoQjs7QUFBeUIsU0FBSSxJQUFJaFcsSUFBUixJQUFnQjRaLEdBQWhCLEVBQW9CO0FBQUMsVUFBR0EsR0FBRyxDQUFDM1osY0FBSixDQUFtQkQsSUFBbkIsQ0FBSCxFQUE0QjtBQUFDLFlBQUlvQixPQUFPLEdBQUMwWSxrQkFBa0IsQ0FBQ0YsR0FBRyxDQUFDNVosSUFBRCxDQUFKLENBQTlCOztBQUEwQyxZQUFHNlYsS0FBSyxDQUFDNVYsY0FBTixDQUFxQkQsSUFBckIsQ0FBSCxFQUE4QjtBQUFDLGNBQUlqQyxLQUFLLEdBQUM4WCxLQUFLLENBQUM3VixJQUFELENBQWY7O0FBQXNCLGNBQUcsQ0FBQ29CLE9BQUosRUFBWTtBQUFDNFUsa0JBQU0sR0FBQ0EsTUFBTSxDQUFDNVUsT0FBUCxDQUFlLE1BQUlwQixJQUFKLEdBQVMsR0FBVCxHQUFhakMsS0FBNUIsRUFBa0MsRUFBbEMsQ0FBUDtBQUE2Q2lZLGtCQUFNLEdBQUNBLE1BQU0sQ0FBQzVVLE9BQVAsQ0FBZXBCLElBQUksR0FBQyxHQUFMLEdBQVNqQyxLQUF4QixFQUE4QixFQUE5QixDQUFQO0FBQTBDLFdBQXBHLE1BQXdHO0FBQUNpWSxrQkFBTSxHQUFDQSxNQUFNLENBQUM1VSxPQUFQLENBQWVwQixJQUFJLEdBQUMsR0FBTCxHQUFTakMsS0FBeEIsRUFBOEJpQyxJQUFJLEdBQUMsR0FBTCxHQUFTb0IsT0FBdkMsQ0FBUDtBQUF3RDtBQUFDLFNBQXZOLE1BQTJOO0FBQUMsY0FBR0EsT0FBSCxFQUFXO0FBQUM0VSxrQkFBTSxHQUFDQSxNQUFNLElBQUUsQ0FBQ0EsTUFBRCxHQUFRLEdBQVIsR0FBWSxHQUFkLENBQU4sR0FBeUJoVyxJQUF6QixHQUE4QixHQUE5QixHQUFrQ29CLE9BQXpDO0FBQWtELFdBQTlELE1BQWtFO0FBQUM0VSxrQkFBTSxHQUFDQSxNQUFNLENBQUM1VSxPQUFQLENBQWVwQixJQUFJLEdBQUMsR0FBcEIsRUFBd0IsRUFBeEIsQ0FBUDtBQUFvQztBQUFDO0FBQUM7QUFBQzs7QUFDbHpCLFFBQUkrWixJQUFJLEdBQUMsQ0FBQ2hFLFFBQVEsQ0FBQ2lFLFFBQVYsRUFBbUIsSUFBbkIsRUFBd0JqRSxRQUFRLENBQUNrRSxJQUFqQyxFQUFzQ2xFLFFBQVEsQ0FBQ21FLFFBQS9DLEVBQXlEQyxJQUF6RCxDQUE4RCxFQUE5RCxDQUFUO0FBQTJFbkUsVUFBTSxHQUFDLENBQUNBLE1BQU0sQ0FBQ3RFLE1BQVAsQ0FBYyxDQUFkLENBQUQsR0FBa0JzRSxNQUFNLENBQUN0RSxNQUFQLENBQWMsQ0FBZCxDQUFsQixHQUFtQ3NFLE1BQTFDO0FBQWlELFdBQU9uQyxTQUFTLENBQUNrRyxJQUFJLEdBQUMvRCxNQUFOLENBQWhCO0FBQStCLEdBRHFIOztBQUNwSDFTLE9BQUssQ0FBQ2lTLFlBQU4sR0FBbUIsVUFBU3BhLE1BQVQsRUFBZ0I7QUFBQyxRQUFHLENBQUMsS0FBSzJFLE9BQUwsQ0FBYTRILE9BQWIsSUFBc0J2TSxNQUF2QixLQUFnQyxLQUFLK0gsT0FBTCxDQUFha0osU0FBN0MsSUFBd0QsQ0FBQyxLQUFLUCxNQUFMLENBQVluVSxJQUF4RSxFQUE2RTtBQUFDLFVBQUkwaUIsUUFBUSxHQUFDbm1CLE1BQU0sQ0FBQ3lULE9BQVAsQ0FBZTJTLEtBQTVCO0FBQUEsVUFBa0N2aUIsSUFBSSxHQUFDO0FBQUNvZSxZQUFJLEVBQUMsQ0FBQy9hLE1BQUQsR0FBUSxLQUFLaVcsT0FBTCxDQUFhblksSUFBckIsR0FBMEIsRUFBaEM7QUFBbUNrZCxXQUFHLEVBQUMsQ0FBQ2hiLE1BQUQsR0FBUXNFLEtBQUssQ0FBQzRDLE1BQU4sQ0FBYSxLQUFLK08sT0FBTCxDQUFhcUYsYUFBMUIsRUFBd0MsS0FBS3JGLE9BQUwsQ0FBYTlPLEtBQXJELElBQTRELENBQXBFLEdBQXNFO0FBQTdHLE9BQXZDOztBQUF3SixVQUFHLENBQUM4WCxRQUFELElBQVdBLFFBQVEsQ0FBQ2pFLEdBQVQsS0FBZXJlLElBQUksQ0FBQ3FlLEdBQWxDLEVBQXNDO0FBQUMsWUFBSUgsTUFBTSxHQUFDLEtBQUsyRCxjQUFMLENBQW9CN2hCLElBQXBCLENBQVg7O0FBQXFDLFlBQUc7QUFBQzdELGdCQUFNLENBQUN5VCxPQUFQLENBQWU2TixZQUFmLENBQTRCemQsSUFBNUIsRUFBaUMsRUFBakMsRUFBb0NrZSxNQUFwQztBQUE2QyxTQUFqRCxDQUFpRCxPQUFNcFcsS0FBTixFQUFZO0FBQUMsZUFBS0UsT0FBTCxDQUFhNEgsT0FBYixHQUFxQixLQUFyQjtBQUEyQmpJLGVBQUssQ0FBQ0csS0FBTixDQUFZLGtIQUFaO0FBQWlJO0FBQUM7QUFBQzs7QUFDOXNCLFNBQUtpTSxNQUFMLENBQVluVSxJQUFaLEdBQWlCLEtBQWpCO0FBQXdCLEdBRG9JOztBQUNuSTRMLE9BQUssQ0FBQ3dVLGNBQU4sR0FBcUIsVUFBU3hnQixLQUFULEVBQWU7QUFBQyxRQUFJcUssRUFBRSxHQUFDckssS0FBSyxJQUFFckQsTUFBTSxDQUFDcUQsS0FBckI7QUFBQSxRQUEyQlEsSUFBSSxHQUFDLElBQWhDO0FBQUEsUUFBcUN3aUIsTUFBckM7QUFBQSxRQUE0Q3ZDLE1BQTVDO0FBQUEsUUFBbURPLEtBQW5EO0FBQXlEaGhCLFNBQUssQ0FBQ3NkLGNBQU47O0FBQXVCLFFBQUcsWUFBV2pULEVBQWQsRUFBaUI7QUFBQ29XLFlBQU0sR0FBQ3BXLEVBQUUsQ0FBQzRZLE1BQUgsR0FBVSxDQUFDLENBQWxCO0FBQXFCOztBQUNyTCxRQUFHLGdCQUFlNVksRUFBbEIsRUFBcUI7QUFBQ29XLFlBQU0sR0FBQ3BXLEVBQUUsQ0FBQzZZLFVBQUgsR0FBYyxDQUFDLENBQXRCO0FBQXlCOztBQUMvQyxRQUFHLGlCQUFnQjdZLEVBQW5CLEVBQXNCO0FBQUNvVyxZQUFNLEdBQUNwVyxFQUFFLENBQUM4WSxXQUFILEdBQWUsQ0FBQyxDQUF2QjtBQUEwQjs7QUFDakQsUUFBRyxpQkFBZ0I5WSxFQUFuQixFQUFzQjtBQUFDMlksWUFBTSxHQUFDM1ksRUFBRSxDQUFDK1ksV0FBSCxHQUFlLENBQUMsQ0FBdkI7QUFBMEI7O0FBQ2pELFFBQUcsWUFBVy9ZLEVBQWQsRUFBaUI7QUFBQ29XLFlBQU0sR0FBQ3BXLEVBQUUsQ0FBQ29XLE1BQVY7QUFBa0I7O0FBQ3BDLFFBQUcsWUFBV3BXLEVBQWQsRUFBaUI7QUFBQzJZLFlBQU0sR0FBQzNZLEVBQUUsQ0FBQzJZLE1BQUgsR0FBVSxDQUFDLENBQWxCO0FBQXFCOztBQUN2QyxRQUFHM1ksRUFBRSxDQUFDZ1osU0FBSCxLQUFlLENBQWxCLEVBQW9CO0FBQUNMLFlBQU0sSUFBRSxFQUFSO0FBQVd2QyxZQUFNLElBQUUsRUFBUjtBQUFZLEtBQTVDLE1BQWlELElBQUdwVyxFQUFFLENBQUNnWixTQUFILEtBQWUsQ0FBbEIsRUFBb0I7QUFBQ0wsWUFBTSxJQUFFLEdBQVI7QUFBWXZDLFlBQU0sSUFBRSxHQUFSO0FBQWE7O0FBQy9GTyxTQUFLLEdBQUNwWixJQUFJLENBQUNpWixHQUFMLENBQVNtQyxNQUFULElBQWlCcGIsSUFBSSxDQUFDaVosR0FBTCxDQUFTSixNQUFULENBQWpCLEdBQWtDdUMsTUFBbEMsR0FBeUN2QyxNQUEvQztBQUFzRE8sU0FBSyxHQUFDcFosSUFBSSxDQUFDZ1osR0FBTCxDQUFTLEdBQVQsRUFBYWhaLElBQUksQ0FBQ0MsR0FBTCxDQUFTLENBQUMsR0FBVixFQUFjbVosS0FBZCxDQUFiLENBQU47O0FBQXlDLFFBQUdwWixJQUFJLENBQUNpWixHQUFMLENBQVNHLEtBQVQsSUFBZ0IsS0FBS3hZLE9BQUwsQ0FBYW1KLGlCQUFoQyxFQUFrRDtBQUFDLFdBQUs0QyxNQUFMLENBQVkrTyxTQUFaLEdBQXNCdEMsS0FBdEI7QUFBNEIsYUFBT3hnQixJQUFQO0FBQWE7O0FBQzNMLFFBQUkraUIsSUFBSSxHQUFDLENBQUMsSUFBSTdkLElBQUosRUFBVjs7QUFBcUIsUUFBR2tDLElBQUksQ0FBQ2laLEdBQUwsQ0FBU0csS0FBVCxJQUFnQnBaLElBQUksQ0FBQ2laLEdBQUwsQ0FBUyxLQUFLdE0sTUFBTCxDQUFZK08sU0FBckIsQ0FBaEIsSUFBaURDLElBQUksR0FBQyxLQUFLaFAsTUFBTCxDQUFZaVAsVUFBakIsR0FBNEIsRUFBaEYsRUFBbUY7QUFBQ2hqQixVQUFJLEdBQUM7QUFBQyxrQkFBU3dpQixNQUFWO0FBQWlCLGtCQUFTdkMsTUFBMUI7QUFBaUMsaUJBQVFPO0FBQXpDLE9BQUw7QUFBc0Q7O0FBQy9KLFNBQUt6TSxNQUFMLENBQVkrTyxTQUFaLEdBQXNCdEMsS0FBdEI7QUFBNEIsU0FBS3pNLE1BQUwsQ0FBWWlQLFVBQVosR0FBdUJELElBQXZCO0FBQTRCLFdBQU8vaUIsSUFBUDtBQUFhLEdBVDVDOztBQVM2Q3dMLE9BQUssQ0FBQzZKLEtBQU4sR0FBWSxVQUFTN1YsS0FBVCxFQUFlO0FBQUMsUUFBR0EsS0FBSyxJQUFFQSxLQUFLLENBQUMrYixNQUFOLENBQWFoWCxPQUFiLEtBQXVCLE9BQWpDLEVBQXlDO0FBQUM7QUFBUTs7QUFDcEosUUFBSTRPLE1BQU0sR0FBQyxLQUFLRCxHQUFMLENBQVNDLE1BQXBCO0FBQUEsUUFBMkJoSyxTQUFTLEdBQUMsS0FBS3VLLEdBQUwsR0FBUyxlQUE5QztBQUFBLFFBQThEN1MsT0FBTyxHQUFDckIsS0FBSyxHQUFDQSxLQUFLLENBQUMrYixNQUFOLENBQWFwUyxTQUFkLEdBQXdCLElBQW5HO0FBQUEsUUFBd0d3TSxNQUFNLEdBQUNoTyxLQUFLLENBQUNnRCxRQUFOLENBQWV3SSxNQUFmLEVBQXNCaEssU0FBdEIsSUFBaUMsUUFBakMsR0FBMEMsS0FBeko7O0FBQStKLFFBQUl3TSxNQUFNLEtBQUcsUUFBVCxLQUFvQjlVLE9BQU8sS0FBRyxLQUFLNlMsR0FBTCxHQUFTLFFBQW5CLElBQTZCLENBQUNsVSxLQUFsRCxDQUFELElBQTREcUIsT0FBTyxLQUFHLEtBQUs2UyxHQUFMLEdBQVMsUUFBbEYsRUFBMkY7QUFBQyxVQUFHaUMsTUFBTSxLQUFHLEtBQVosRUFBa0I7QUFBQyxhQUFLc04sZUFBTDtBQUF3Qjs7QUFDdFN0YixXQUFLLENBQUNnTyxNQUFNLEdBQUMsT0FBUixDQUFMLENBQXNCeEMsTUFBdEIsRUFBNkJoSyxTQUE3QjtBQUF5QztBQUFDLEdBRjRCOztBQUUzQnFDLE9BQUssQ0FBQzBYLE9BQU4sR0FBYyxVQUFTMWpCLEtBQVQsRUFBZTtBQUFDLFFBQUlDLElBQUksR0FBQ0QsS0FBSyxDQUFDK2IsTUFBTixDQUFhakQsTUFBdEI7QUFBQSxRQUE2QmdCLE9BQU8sR0FBQyxLQUFLQSxPQUExQztBQUFBLFFBQWtEVixLQUFLLEdBQUMsS0FBS3NJLFFBQUwsRUFBeEQ7QUFBQSxRQUF3RWlDLEtBQUssR0FBQ3ZLLEtBQUssQ0FBQ25aLElBQU4sS0FBYSxPQUFiLEdBQXFCbVosS0FBSyxDQUFDQyxHQUEzQixHQUErQkQsS0FBSyxDQUFDdEQsTUFBbkg7QUFBQSxRQUEwSEYsR0FBRyxHQUFDLEtBQUtULFdBQUwsQ0FBaUJsVixJQUFqQixDQUE5SDtBQUFBLFFBQXFKMGIsR0FBcko7O0FBQXlKLFFBQUcvRixHQUFILEVBQU87QUFBQyxVQUFHLEtBQUtwTixPQUFMLENBQWFpSyxTQUFiLEtBQXlCLE1BQTVCLEVBQW1DO0FBQUNrSixXQUFHLEdBQUMsQ0FBQzhDLFFBQVEsQ0FBQ2lFLFFBQVYsRUFBbUIsSUFBbkIsRUFBd0JqRSxRQUFRLENBQUNrRSxJQUFqQyxFQUFzQ2xFLFFBQVEsQ0FBQ21FLFFBQS9DLEVBQXlEQyxJQUF6RCxDQUE4RCxFQUE5RCxDQUFKO0FBQXVFLE9BQTNHLE1BQWdILElBQUcsS0FBS3JhLE9BQUwsQ0FBYWlLLFNBQWIsS0FBeUIsVUFBekIsSUFBcUMsQ0FBQyxRQUFELEVBQVUsTUFBVixFQUFrQnJHLE9BQWxCLENBQTBCZ04sS0FBSyxDQUFDblosSUFBaEMsSUFBc0MsQ0FBQyxDQUEvRSxFQUFpRjtBQUFDMGIsV0FBRyxHQUFDLEtBQUswRyxjQUFMLENBQW9CO0FBQUN6RCxjQUFJLEVBQUM5RSxPQUFPLENBQUNuWSxJQUFkO0FBQW1Ca2QsYUFBRyxFQUFDL0UsT0FBTyxDQUFDOU8sS0FBUixHQUFjO0FBQXJDLFNBQXBCLENBQUo7QUFBa0UsT0FBcEosTUFBd0o7QUFBQzJRLFdBQUcsR0FBQ3ZDLEtBQUssQ0FBQ0MsR0FBTixDQUFVdlAsT0FBVixDQUFrQixLQUFsQixFQUF3QixFQUF4QixFQUE0QjNFLEtBQTVCLENBQWtDLEdBQWxDLEVBQXVDLENBQXZDLENBQUo7O0FBQThDLFlBQUdpVSxLQUFLLENBQUNuWixJQUFOLEtBQWEsT0FBYixJQUFzQm1aLEtBQUssQ0FBQ29CLE1BQU4sS0FBZSxPQUF4QyxFQUFnRDtBQUFDbUIsYUFBRyxHQUFDdkMsS0FBSyxDQUFDdkQsS0FBVjtBQUFpQjtBQUFDOztBQUNwbUIsVUFBSStOLElBQUksR0FBQ3piLEtBQUssQ0FBQ1UsUUFBTixDQUFlLEdBQWYsQ0FBVDtBQUE2QithLFVBQUksQ0FBQ0MsSUFBTCxHQUFVRixLQUFWO0FBQWdCQSxXQUFLLEdBQUNDLElBQUksQ0FBQ0MsSUFBWDtBQUFnQkQsVUFBSSxDQUFDQyxJQUFMLEdBQVVsSSxHQUFWO0FBQWNBLFNBQUcsR0FBQ2lJLElBQUksQ0FBQ0MsSUFBVDtBQUFjLFVBQUlDLEdBQUcsR0FBQzNiLEtBQUssQ0FBQ1UsUUFBTixDQUFlLEtBQWYsQ0FBUjtBQUE4QmliLFNBQUcsQ0FBQ25ILFNBQUosR0FBY3ZELEtBQUssQ0FBQ3ZJLE9BQXBCO0FBQTRCLFVBQUlrVCxJQUFJLEdBQUMsQ0FBQ0QsR0FBRyxDQUFDMUwsV0FBSixJQUFpQjBMLEdBQUcsQ0FBQ0UsU0FBdEIsRUFBaUNsYSxPQUFqQyxDQUF5QyxNQUF6QyxFQUFnRCxHQUFoRCxFQUFxRDFFLElBQXJELE1BQTZELEVBQXRFO0FBQXlFd1EsU0FBRyxHQUFDQSxHQUFHLENBQUM5TCxPQUFKLENBQVksT0FBWixFQUFvQjBZLGtCQUFrQixDQUFDN0csR0FBRCxDQUF0QyxFQUE2QzdSLE9BQTdDLENBQXFELFNBQXJELEVBQStEMFksa0JBQWtCLENBQUNtQixLQUFELENBQWpGLEVBQTBGN1osT0FBMUYsQ0FBa0csUUFBbEcsRUFBMkcwWSxrQkFBa0IsQ0FBQ3VCLElBQUksSUFBRXRhLFFBQVEsQ0FBQzJSLEtBQWhCLENBQTdILENBQUo7O0FBQXlKLFVBQUd4RixHQUFILEVBQU87QUFBQyxZQUFJalIsSUFBSSxHQUFDaUQsSUFBSSxDQUFDb0gsS0FBTCxDQUFXclMsTUFBTSxDQUFDc25CLE9BQVAsR0FBZSxDQUFDdG5CLE1BQU0sQ0FBQ3VuQixVQUFQLEdBQWtCLEdBQW5CLElBQXdCLENBQWxELENBQVQ7QUFBQSxZQUE4RDNmLEdBQUcsR0FBQ3FELElBQUksQ0FBQ29ILEtBQUwsQ0FBV3JTLE1BQU0sQ0FBQ3duQixPQUFQLEdBQWUsQ0FBQ3huQixNQUFNLENBQUN5bkIsV0FBUCxHQUFtQixHQUFwQixJQUF5QixDQUFuRCxDQUFsRTtBQUF3SHpuQixjQUFNLENBQUMyZixJQUFQLENBQVkxRyxHQUFaLEVBQWdCLEtBQUsxQixHQUFMLEdBQVMsUUFBekIsRUFBa0Msd0VBQXNFM1AsR0FBdEUsR0FBMEUsUUFBMUUsR0FBbUZJLElBQXJIO0FBQTRIO0FBQUMsS0FEaFosTUFDb1o7QUFBQ3dELFdBQUssQ0FBQ0csS0FBTixDQUFZLHdDQUFaO0FBQXVEOztBQUM5cUIsV0FBTyxLQUFQO0FBQWMsR0FGNkI7O0FBRTVCMEQsT0FBSyxDQUFDeVgsZUFBTixHQUFzQixZQUFVO0FBQUMsUUFBRyxLQUFLamIsT0FBTCxDQUFhaUksUUFBYixDQUFzQnJFLE9BQXRCLENBQThCLE9BQTlCLElBQXVDLENBQUMsQ0FBM0MsRUFBNkM7QUFBQyxVQUFJMUssU0FBUyxHQUFDLE9BQWQ7QUFBQSxVQUFzQjJpQixPQUFPLEdBQUMsS0FBSzNRLEdBQUwsQ0FBU3lFLFlBQXZDO0FBQUEsVUFBb0R0VCxLQUFLLEdBQUN3ZixPQUFPLENBQUNDLFdBQWxFO0FBQUEsVUFBOEVDLE1BQU0sR0FBQyxLQUFLblEsT0FBTCxDQUFheUIsS0FBYixDQUFtQjVSLHFCQUFuQixFQUFyRjtBQUFBLFVBQWdJNEosUUFBUSxHQUFDMFcsTUFBTSxDQUFDNWYsSUFBUCxHQUFZRSxLQUFaLEdBQWtCMGYsTUFBTSxDQUFDMWYsS0FBUCxHQUFhLENBQS9CLEdBQWlDLEVBQTFLOztBQUE2SyxVQUFHZ0osUUFBUSxHQUFDLENBQVosRUFBYztBQUFDbk0saUJBQVMsR0FBQyxNQUFWO0FBQWlCbU0sZ0JBQVEsR0FBQzBXLE1BQU0sQ0FBQzVmLElBQVAsR0FBWTRmLE1BQU0sQ0FBQzFmLEtBQVAsR0FBYSxDQUF6QixHQUEyQixFQUFwQztBQUF3Qzs7QUFDblZ3ZixhQUFPLENBQUN6TSxZQUFSLENBQXFCLGVBQXJCLEVBQXFDbFcsU0FBckM7QUFBZ0QyaUIsYUFBTyxDQUFDamIsS0FBUixDQUFjN0UsR0FBZCxHQUFrQixLQUFLbVAsR0FBTCxDQUFTc0UsTUFBVCxDQUFnQmxULE1BQWhCLEdBQXVCLENBQXZCLEdBQXlCLElBQTNDO0FBQWdEdWYsYUFBTyxDQUFDamIsS0FBUixDQUFjekUsSUFBZCxHQUFtQmtKLFFBQVEsR0FBQyxJQUE1QjtBQUFrQztBQUFDLEdBRHBIOztBQUNxSDdCLE9BQUssQ0FBQ2tSLFFBQU4sR0FBZSxZQUFVO0FBQUMsUUFBRyxDQUFDLEtBQUswRSxjQUFMLEVBQUosRUFBMEI7QUFBQyxhQUFPLEtBQVA7QUFBYzs7QUFDdk0sUUFBSXhJLEtBQUssR0FBQyxLQUFLc0ksUUFBTCxFQUFWO0FBQUEsUUFBMEI5TCxHQUFHLEdBQUN3RCxLQUFLLENBQUNDLEdBQU4sQ0FBVXZQLE9BQVYsQ0FBa0IsS0FBbEIsRUFBd0IsRUFBeEIsRUFBNEIzRSxLQUE1QixDQUFrQyxHQUFsQyxFQUF1QyxDQUF2QyxDQUE5QjtBQUFBLFFBQXdFeWUsSUFBSSxHQUFDbmEsUUFBUSxDQUFDQyxhQUFULENBQXVCLEdBQXZCLENBQTdFO0FBQUEsUUFBeUc4YSxLQUFLLEdBQUM1TyxHQUFHLENBQUN6USxLQUFKLENBQVUsR0FBVixDQUEvRztBQUE4SHllLFFBQUksQ0FBQ0MsSUFBTCxHQUFVak8sR0FBVjtBQUFjZ08sUUFBSSxDQUFDMUcsUUFBTCxHQUFjc0gsS0FBSyxDQUFDQyxHQUFOLEdBQVl0ZixLQUFaLENBQWtCLEdBQWxCLEVBQXVCLENBQXZCLENBQWQ7QUFBd0N5ZSxRQUFJLENBQUNoTSxZQUFMLENBQWtCLFFBQWxCLEVBQTJCLFFBQTNCO0FBQXFDbk8sWUFBUSxDQUFDc08sSUFBVCxDQUFjVixXQUFkLENBQTBCdU0sSUFBMUI7QUFBZ0NBLFFBQUksQ0FBQ2MsS0FBTDtBQUFhamIsWUFBUSxDQUFDc08sSUFBVCxDQUFjNE0sV0FBZCxDQUEwQmYsSUFBMUI7QUFBaUMsR0FEbks7O0FBQ29LNVgsT0FBSyxDQUFDK0ksVUFBTixHQUFpQixZQUFVO0FBQUMsUUFBSTZQLGlCQUFpQixHQUFDLEtBQUtoWixPQUFMLENBQWFtSixVQUFiLENBQXdCMVQsT0FBOUM7O0FBQXNELFFBQUcsQ0FBQ29JLFFBQVEsQ0FBQ21iLGlCQUFELENBQVosRUFBZ0M7QUFBQyxXQUFLQyxpQkFBTDtBQUEwQixLQUEzRCxNQUErRDtBQUFDLFdBQUs5RixjQUFMO0FBQXVCO0FBQUMsR0FBMUs7O0FBQTJLL1MsT0FBSyxDQUFDOFksZ0JBQU4sR0FBdUIsWUFBVTtBQUFDLFFBQUluUixNQUFNLEdBQUMsS0FBS0QsR0FBTCxDQUFTQyxNQUFwQjtBQUFBLFFBQTJCaVIsaUJBQWlCLEdBQUNuYixRQUFRLENBQUMsS0FBS21DLE9BQUwsQ0FBYW1KLFVBQWIsQ0FBd0IxVCxPQUF6QixDQUFyRDs7QUFBdUYsUUFBRyxDQUFDdWpCLGlCQUFKLEVBQXNCO0FBQUMsV0FBSy9PLEtBQUw7QUFBYSxXQUFLdEIsTUFBTCxDQUFZUSxVQUFaLEdBQXVCLEtBQXZCO0FBQTZCNU0sV0FBSyxDQUFDbUQsV0FBTixDQUFrQnFJLE1BQWxCLEVBQXlCLEtBQUtPLEdBQUwsR0FBUyxhQUFsQztBQUFrRCxLQUFuSCxNQUF3SCxJQUFHMFEsaUJBQWlCLEtBQUdqUixNQUF2QixFQUE4QjtBQUFDLFdBQUs4UCxlQUFMO0FBQXVCLFdBQUtsUCxNQUFMLENBQVlRLFVBQVosR0FBdUIsSUFBdkI7QUFBNEI1TSxXQUFLLENBQUNrRCxRQUFOLENBQWVzSSxNQUFmLEVBQXNCLEtBQUtPLEdBQUwsR0FBUyxhQUEvQjtBQUErQzs7QUFDcjBCLFNBQUs2USxlQUFMO0FBQXdCLEdBRDJiOztBQUMxYi9ZLE9BQUssQ0FBQzZZLGlCQUFOLEdBQXdCLFlBQVU7QUFBQyxRQUFJdFAsT0FBTyxHQUFDLEtBQUszSixPQUFMLENBQWFtSixVQUFiLENBQXdCUSxPQUFwQzs7QUFBNEMsUUFBRzlMLFFBQVEsQ0FBQzZWLGVBQVQsQ0FBeUIvSixPQUF6QixDQUFILEVBQXFDO0FBQUMsV0FBSzdCLEdBQUwsQ0FBU0MsTUFBVCxDQUFnQjRCLE9BQWhCO0FBQTRCO0FBQUMsR0FBbEo7O0FBQW1KdkosT0FBSyxDQUFDK1MsY0FBTixHQUFxQixZQUFVO0FBQUMsUUFBSXRKLElBQUksR0FBQyxLQUFLN0osT0FBTCxDQUFhbUosVUFBYixDQUF3QlUsSUFBakM7O0FBQXNDLFFBQUdoTSxRQUFRLENBQUNnTSxJQUFELENBQVgsRUFBa0I7QUFBQ2hNLGNBQVEsQ0FBQ2dNLElBQUQsQ0FBUjtBQUFrQjtBQUFDLEdBQTVHOztBQUE2R3pKLE9BQUssQ0FBQ2dLLElBQU4sR0FBVyxZQUFVO0FBQUMsUUFBRyxLQUFLekIsTUFBTCxDQUFZeUIsSUFBZixFQUFvQjtBQUFDLFdBQUtnSixhQUFMO0FBQXNCLEtBQTNDLE1BQStDO0FBQUMsV0FBS1YsY0FBTDtBQUF1QjtBQUFDLEdBQTlGOztBQUErRnRTLE9BQUssQ0FBQ3NTLGNBQU4sR0FBcUIsWUFBVTtBQUFDLFFBQUkxUSxLQUFLLEdBQUMsQ0FBVjtBQUFBLFFBQVlrTSxPQUFPLEdBQUMsS0FBS0EsT0FBekI7QUFBQSxRQUFpQ3RSLE9BQU8sR0FBQyxLQUFLQSxPQUE5QztBQUFBLFFBQXNEd0YsSUFBSSxHQUFDLEtBQUt1RyxNQUFMLENBQVl2RyxJQUF2RTtBQUFBLFFBQTRFZ1gsUUFBUSxHQUFDeGMsT0FBTyxDQUFDb0ssaUJBQTdGO0FBQUEsUUFBK0dxUyxLQUFLLEdBQUNyZCxJQUFJLENBQUNDLEdBQUwsQ0FBUyxHQUFULEVBQWFXLE9BQU8sQ0FBQ2tLLGlCQUFyQixDQUFySDtBQUFBLFFBQTZKekQsS0FBSyxHQUFDekcsT0FBTyxDQUFDcUssVUFBM0s7QUFBQSxRQUFzTHFTLE1BQU0sR0FBQ2pXLEtBQUssSUFBRSxLQUFLeUUsR0FBTCxDQUFTOEUsS0FBaEIsR0FBc0IsS0FBSzlFLEdBQUwsQ0FBUzhFLEtBQVQsQ0FBZTJNLFVBQWYsQ0FBMEIsSUFBMUIsQ0FBdEIsR0FBc0QsSUFBblA7O0FBQXdQLFFBQUlDLFNBQVMsR0FBRSxVQUFTdGEsR0FBVCxFQUFhO0FBQUNBLFNBQUcsR0FBQyxDQUFDQSxHQUFELEdBQUssQ0FBQyxJQUFJcEYsSUFBSixFQUFOLEdBQWlCb0YsR0FBckI7QUFBeUI4QyxXQUFLLEdBQUMsQ0FBQ0EsS0FBRCxHQUFPOUMsR0FBUCxHQUFXOEMsS0FBakI7O0FBQXVCLFVBQUcsQ0FBQ0ksSUFBRCxJQUFPZ1gsUUFBVixFQUFtQjtBQUFDLFlBQUdsTCxPQUFPLENBQUM5TyxLQUFSLEtBQWdCOE8sT0FBTyxDQUFDcUYsYUFBUixHQUFzQixDQUF6QyxFQUEyQztBQUFDLGVBQUtILGFBQUw7QUFBcUI7QUFBUTtBQUFDOztBQUMzekIsVUFBRy9QLEtBQUssSUFBRWlXLE1BQVYsRUFBaUI7QUFBQyxZQUFJRyxPQUFPLEdBQUN6ZCxJQUFJLENBQUNnWixHQUFMLENBQVMsQ0FBVCxFQUFXLENBQUM5VixHQUFHLEdBQUM4QyxLQUFKLEdBQVVxWCxLQUFYLElBQWtCQSxLQUFsQixHQUF3QixDQUFuQyxDQUFaO0FBQUEsWUFBa0RLLE9BQU8sR0FBQ0QsT0FBTyxHQUFDLEdBQVIsSUFBYXpkLElBQUksQ0FBQzJkLEVBQUwsR0FBUSxHQUFyQixDQUExRDtBQUFvRkwsY0FBTSxDQUFDTSxTQUFQLENBQWlCLENBQWpCLEVBQW1CLENBQW5CLEVBQXFCLEVBQXJCLEVBQXdCLEVBQXhCO0FBQTRCLGFBQUtDLGFBQUwsQ0FBbUJQLE1BQW5CLEVBQTBCMWMsT0FBTyxDQUFDc0ssWUFBbEMsRUFBK0MsR0FBL0M7QUFBb0QsYUFBSzJTLGFBQUwsQ0FBbUJQLE1BQW5CLEVBQTBCMWMsT0FBTyxDQUFDdUssZUFBbEMsRUFBa0R1UyxPQUFsRDtBQUE0RDs7QUFDbFAsVUFBR3hhLEdBQUcsSUFBRThDLEtBQUssR0FBQ3FYLEtBQWQsRUFBb0I7QUFBQ3JYLGFBQUssR0FBQzlDLEdBQU47QUFBVSxhQUFLNGEsT0FBTCxDQUFhLEtBQUtyUixNQUFMLENBQVlySixLQUFaLEdBQWtCLENBQS9CLEVBQWlDLElBQWpDO0FBQXdDOztBQUN2RSxXQUFLd04sS0FBTCxHQUFXalIscUJBQXFCLENBQUM2ZCxTQUFELENBQWhDO0FBQTZDLEtBSGluQixDQUcvbUJsZixJQUgrbUIsQ0FHMW1CLElBSDBtQixDQUFkOztBQUd0bEJpQyxTQUFLLENBQUNrRCxRQUFOLENBQWUsS0FBS3FJLEdBQUwsQ0FBU0MsTUFBeEIsRUFBK0IsS0FBS08sR0FBTCxHQUFTLFdBQXhDO0FBQXFELFNBQUtLLE1BQUwsQ0FBWXlCLElBQVosR0FBaUIsSUFBakI7QUFBc0IsU0FBS3dDLEtBQUwsR0FBV2pSLHFCQUFxQixDQUFDNmQsU0FBRCxDQUFoQztBQUE2QyxHQUhzTTs7QUFHck1wWixPQUFLLENBQUNnVCxhQUFOLEdBQW9CLFlBQVU7QUFBQ2pYLHdCQUFvQixDQUFDLEtBQUt5USxLQUFOLENBQXBCO0FBQWlDclEsU0FBSyxDQUFDbUQsV0FBTixDQUFrQixLQUFLb0ksR0FBTCxDQUFTQyxNQUEzQixFQUFrQyxLQUFLTyxHQUFMLEdBQVMsV0FBM0M7QUFBd0QsU0FBS0ssTUFBTCxDQUFZeUIsSUFBWixHQUFpQixLQUFqQjtBQUF3QixHQUFoSjs7QUFBaUpoSyxPQUFLLENBQUN5WixhQUFOLEdBQW9CLFVBQVNQLE1BQVQsRUFBZ0JTLEtBQWhCLEVBQXNCdFEsR0FBdEIsRUFBMEI7QUFBQyxRQUFJekgsS0FBSyxHQUFDLE1BQUloRyxJQUFJLENBQUMyZCxFQUFuQjtBQUFzQkwsVUFBTSxDQUFDVSxXQUFQLEdBQW1CRCxLQUFuQjtBQUF5QlQsVUFBTSxDQUFDVyxTQUFQLEdBQWlCLENBQWpCO0FBQW1CWCxVQUFNLENBQUNZLFNBQVA7QUFBbUJaLFVBQU0sQ0FBQ2EsR0FBUCxDQUFXLEVBQVgsRUFBYyxFQUFkLEVBQWlCLEVBQWpCLEVBQW9CblksS0FBcEIsRUFBMEJBLEtBQUssR0FBQ3lILEdBQWhDLEVBQW9DLEtBQXBDO0FBQTJDNlAsVUFBTSxDQUFDYyxNQUFQO0FBQWlCLEdBQWhNOztBQUFpTWhhLE9BQUssQ0FBQ3FTLFdBQU4sR0FBa0IsWUFBVTtBQUFDLFFBQUlqRixLQUFLLEdBQUMsS0FBS3NJLFFBQUwsRUFBVjs7QUFBMEIsUUFBR3RJLEtBQUssQ0FBQ25aLElBQU4sS0FBYSxPQUFoQixFQUF3QjtBQUFDO0FBQVE7O0FBQzdsQmtJLFNBQUssQ0FBQ2tELFFBQU4sQ0FBZStOLEtBQUssQ0FBQ2pDLEdBQXJCLEVBQXlCLEtBQUtqRCxHQUFMLEdBQVMsVUFBbEM7QUFBOEMvTCxTQUFLLENBQUNtRCxXQUFOLENBQWtCOE4sS0FBSyxDQUFDakMsR0FBeEIsRUFBNEIsS0FBS2pELEdBQUwsR0FBUyxVQUFyQzs7QUFBaUQsUUFBRyxDQUFDa0YsS0FBSyxDQUFDNk0sS0FBVixFQUFnQjtBQUFDLFVBQUc3TSxLQUFLLENBQUNvQixNQUFOLEtBQWUsT0FBbEIsRUFBMEI7QUFBQ3BCLGFBQUssQ0FBQzZNLEtBQU4sR0FBWTlkLEtBQUssQ0FBQ1UsUUFBTixDQUFlLE9BQWYsQ0FBWjtBQUFvQ3VRLGFBQUssQ0FBQzZNLEtBQU4sQ0FBWXJPLFlBQVosQ0FBeUIsVUFBekIsRUFBb0MsRUFBcEM7QUFBd0N3QixhQUFLLENBQUM2TSxLQUFOLENBQVlyTyxZQUFaLENBQXlCLFVBQXpCLEVBQW9DLEVBQXBDO0FBQXdDLFlBQUlzTyxJQUFJLEdBQUM5TSxLQUFLLENBQUNDLEdBQU4sQ0FBVXZQLE9BQVYsQ0FBa0IsS0FBbEIsRUFBd0IsRUFBeEIsRUFBNEIzRSxLQUE1QixDQUFrQyxHQUFsQyxDQUFUOztBQUFnRCxhQUFJLElBQUluSCxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNrb0IsSUFBSSxDQUFDaG9CLE1BQW5CLEVBQTBCRixDQUFDLEVBQTNCLEVBQThCO0FBQUMsY0FBSW1vQixJQUFJLEdBQUMxYyxRQUFRLENBQUMyYyxzQkFBVCxFQUFUO0FBQUEsY0FBMkNqTixNQUFNLEdBQUNoUixLQUFLLENBQUNVLFFBQU4sQ0FBZSxRQUFmLENBQWxEO0FBQUEsY0FBMkU1SSxJQUFJLEdBQUUsZUFBRCxDQUFrQm9tQixJQUFsQixDQUF1QkgsSUFBSSxDQUFDbG9CLENBQUQsQ0FBM0IsQ0FBaEY7O0FBQWdILGNBQUdpQyxJQUFJLElBQUUsQ0FBQyxLQUFELEVBQU8sTUFBUCxFQUFjLEtBQWQsRUFBcUJtTSxPQUFyQixDQUE2Qm5NLElBQUksQ0FBQyxDQUFELENBQWpDLElBQXNDLENBQUMsQ0FBaEQsRUFBa0Q7QUFBQ2taLGtCQUFNLENBQUNFLEdBQVAsR0FBVzZNLElBQUksQ0FBQ2xvQixDQUFELENBQWY7QUFBbUJtYixrQkFBTSxDQUFDdkIsWUFBUCxDQUFvQixNQUFwQixFQUEyQixZQUFVM1gsSUFBSSxDQUFDLENBQUQsQ0FBSixLQUFVLEtBQVYsR0FBZ0IsS0FBaEIsR0FBc0JBLElBQUksQ0FBQyxDQUFELENBQXBDLENBQTNCO0FBQXFFa21CLGdCQUFJLENBQUM5TyxXQUFMLENBQWlCOEIsTUFBakI7QUFBMEI7O0FBQ25tQkMsZUFBSyxDQUFDNk0sS0FBTixDQUFZNU8sV0FBWixDQUF3QjhPLElBQXhCO0FBQStCO0FBQUMsT0FEZ0YsTUFFM0csSUFBRy9NLEtBQUssQ0FBQ29CLE1BQVQsRUFBZ0I7QUFBQ3BCLGFBQUssQ0FBQzZNLEtBQU4sR0FBWTlkLEtBQUssQ0FBQ1UsUUFBTixDQUFlLFFBQWYsQ0FBWjtBQUFxQ3VRLGFBQUssQ0FBQzZNLEtBQU4sQ0FBWTVNLEdBQVosR0FBZ0JELEtBQUssQ0FBQ0MsR0FBdEI7QUFBMEJELGFBQUssQ0FBQzZNLEtBQU4sQ0FBWXJPLFlBQVosQ0FBeUIsYUFBekIsRUFBdUMsQ0FBdkM7QUFBMEN3QixhQUFLLENBQUM2TSxLQUFOLENBQVlyTyxZQUFaLENBQXlCLGlCQUF6QixFQUEyQyxFQUEzQztBQUFnRDs7QUFDL0t3QixXQUFLLENBQUM2TSxLQUFOLENBQVlyTyxZQUFaLENBQXlCLE9BQXpCLEVBQWlDLE1BQWpDO0FBQXlDd0IsV0FBSyxDQUFDNk0sS0FBTixDQUFZck8sWUFBWixDQUF5QixRQUF6QixFQUFrQyxNQUFsQztBQUEyQzs7QUFDcEYsUUFBRyxDQUFDd0IsS0FBSyxDQUFDakMsR0FBTixDQUFVbVAsVUFBZCxFQUF5QjtBQUFDbE4sV0FBSyxDQUFDakMsR0FBTixDQUFVRSxXQUFWLENBQXNCK0IsS0FBSyxDQUFDNk0sS0FBNUI7O0FBQW1DLFVBQUc3TSxLQUFLLENBQUNvQixNQUFOLEtBQWUsT0FBbEIsRUFBMEI7QUFBQ3BCLGFBQUssQ0FBQzZNLEtBQU4sQ0FBWXpJLE1BQVosR0FBbUIsS0FBbkI7QUFBMEI7QUFBQzs7QUFDbkgsU0FBSytJLFNBQUwsQ0FBZW5OLEtBQWY7QUFBdUIsR0FOOGU7O0FBTTdlcE4sT0FBSyxDQUFDd2EsYUFBTixHQUFvQixVQUFTcE4sS0FBVCxFQUFlO0FBQUNBLFNBQUssQ0FBQzZNLEtBQU4sQ0FBWXpJLE1BQVosR0FBbUIsSUFBbkI7QUFBd0JyVixTQUFLLENBQUNtRCxXQUFOLENBQWtCOE4sS0FBSyxDQUFDakMsR0FBeEIsRUFBNEIsS0FBS2pELEdBQUwsR0FBUyxVQUFyQztBQUFpRC9MLFNBQUssQ0FBQ2tELFFBQU4sQ0FBZStOLEtBQUssQ0FBQ2pDLEdBQXJCLEVBQXlCLEtBQUtqRCxHQUFMLEdBQVMsVUFBbEM7QUFBOEMsU0FBS3VTLFVBQUwsQ0FBZ0JyTixLQUFoQjtBQUF3QixHQUFuTDs7QUFBb0xwTixPQUFLLENBQUN5YSxVQUFOLEdBQWlCLFVBQVNyTixLQUFULEVBQWU7QUFBQyxRQUFHLEtBQUs3RSxNQUFMLENBQVl2RyxJQUFaLElBQWtCb0wsS0FBSyxDQUFDb0IsTUFBTixLQUFlLE9BQXBDLEVBQTRDO0FBQUMsVUFBSVYsT0FBTyxHQUFDLEtBQUtBLE9BQWpCO0FBQUEsVUFBeUI1YixNQUFNLEdBQUM0YixPQUFPLENBQUM1YixNQUF4QztBQUFBLFVBQStDd29CLE9BQU8sR0FBQzVNLE9BQU8sQ0FBQ3FGLGFBQS9EO0FBQUEsVUFBNkV3SCxPQUFPLEdBQUN4ZSxLQUFLLENBQUM0QyxNQUFOLENBQWEyYixPQUFiLEVBQXFCdE4sS0FBSyxDQUFDcE8sS0FBM0IsQ0FBckY7O0FBQXVILFdBQUksSUFBSWhOLENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBQ0UsTUFBZCxFQUFxQkYsQ0FBQyxFQUF0QixFQUF5QjtBQUFDLFlBQUlnTixLQUFLLEdBQUM3QyxLQUFLLENBQUM0QyxNQUFOLENBQWEyYixPQUFiLEVBQXFCNU0sT0FBTyxDQUFDOWIsQ0FBRCxDQUFQLENBQVdnTixLQUFoQyxDQUFWOztBQUFpRCxZQUFHQSxLQUFLLEtBQUcyYixPQUFSLElBQWlCN00sT0FBTyxDQUFDOWIsQ0FBRCxDQUFQLENBQVdnTixLQUFYLEtBQW1Cb08sS0FBSyxDQUFDcE8sS0FBN0MsRUFBbUQ7QUFBQzhPLGlCQUFPLENBQUM5YixDQUFELENBQVAsQ0FBV2lvQixLQUFYLEdBQWlCN00sS0FBSyxDQUFDNk0sS0FBdkI7QUFBOEI7QUFBQztBQUFDO0FBQUMsR0FBclc7O0FBQXNXamEsT0FBSyxDQUFDK1ksZUFBTixHQUFzQixZQUFVO0FBQUMsUUFBSTNMLEtBQUssR0FBQyxLQUFLc0ksUUFBTCxFQUFWO0FBQUEsUUFBMEIzTSxVQUFVLEdBQUMsS0FBS1IsTUFBTCxDQUFZUSxVQUFqRDs7QUFBNEQsUUFBR3FFLEtBQUssQ0FBQ25aLElBQU4sS0FBYSxPQUFiLElBQXNCbVosS0FBSyxDQUFDb0IsTUFBTixLQUFlLE9BQXJDLElBQThDcEIsS0FBSyxDQUFDNk0sS0FBdkQsRUFBNkQ7QUFBQzdNLFdBQUssQ0FBQzZNLEtBQU4sQ0FBWWxSLFVBQVUsR0FBQyxpQkFBRCxHQUFtQixjQUF6QyxFQUF5RCxpQkFBekQsRUFBMkUsRUFBM0U7QUFBZ0Y7QUFBQyxHQUE1Tzs7QUFBNk8vSSxPQUFLLENBQUN1YSxTQUFOLEdBQWdCLFVBQVNuTixLQUFULEVBQWU7QUFBQyxRQUFHQSxLQUFLLENBQUM2TSxLQUFOLENBQVl6SSxNQUFmLEVBQXNCO0FBQUNwRSxXQUFLLENBQUM2TSxLQUFOLENBQVlXLGNBQVo7QUFBNkJ6ZSxXQUFLLENBQUNtRCxXQUFOLENBQWtCOE4sS0FBSyxDQUFDakMsR0FBeEIsRUFBNEIsS0FBS2pELEdBQUwsR0FBUyxVQUFyQztBQUFpRC9MLFdBQUssQ0FBQ2tELFFBQU4sQ0FBZStOLEtBQUssQ0FBQ2pDLEdBQXJCLEVBQXlCLEtBQUtqRCxHQUFMLEdBQVMsVUFBbEM7O0FBQThDLFVBQUdrRixLQUFLLENBQUNvQixNQUFOLEtBQWUsT0FBbEIsRUFBMEI7QUFBQyxZQUFHcEIsS0FBSyxDQUFDcEQsSUFBVCxFQUFjO0FBQUMsY0FBSXpOLE9BQU8sR0FBQyxTQUFPNlEsS0FBSyxDQUFDcEQsSUFBYixNQUFvQixRQUFwQixHQUE2QjdHLElBQUksQ0FBQ0MsU0FBTCxDQUFlZ0ssS0FBSyxDQUFDcEQsSUFBckIsQ0FBN0IsR0FBd0Q2USxNQUFNLENBQUN6TixLQUFLLENBQUNwRCxJQUFQLENBQTFFO0FBQXVGb0QsZUFBSyxDQUFDNk0sS0FBTixDQUFZYSxhQUFaLENBQTBCQyxXQUExQixDQUFzQ3hlLE9BQXRDLEVBQThDLEdBQTlDO0FBQW9EO0FBQUMsT0FBdEwsTUFBMkwsSUFBRyxDQUFDNlEsS0FBSyxDQUFDNk0sS0FBTixDQUFZM2QsS0FBaEIsRUFBc0I7QUFBQyxZQUFHLE9BQU8wZSxrQkFBUCxLQUE0QixVQUE1QixJQUF3QyxLQUFLeGUsT0FBTCxDQUFhd0ssWUFBeEQsRUFBcUU7QUFBQyxjQUFJaVUsSUFBSSxHQUFFN04sS0FBSyxDQUFDNk0sS0FBTixDQUFZbGhCLE9BQVosS0FBc0IsT0FBdkIsR0FBZ0NxVSxLQUFLLENBQUM2TSxLQUF0QyxHQUE0QzdNLEtBQUssQ0FBQzZNLEtBQU4sQ0FBWXpLLG9CQUFaLENBQWlDLE9BQWpDLEVBQTBDLENBQTFDLENBQXJEOztBQUFrRyxjQUFHeUwsSUFBSSxDQUFDQyxNQUFSLEVBQWU7QUFBQ0QsZ0JBQUksQ0FBQ0MsTUFBTCxDQUFZQyxlQUFaO0FBQStCOztBQUMzM0NGLGNBQUksQ0FBQ2pSLElBQUw7QUFBYSxTQUR1cEMsTUFDbnBDO0FBQUNvRCxlQUFLLENBQUM2TSxLQUFOLENBQVlqUSxJQUFaO0FBQW9CO0FBQUM7QUFBQyxLQUR1eEIsTUFDbnhCO0FBQUMsVUFBSWhNLEtBQUssR0FBQyxJQUFWOztBQUFlLFVBQUcsT0FBT2dkLGtCQUFQLEtBQTRCLFVBQTVCLElBQXdDLENBQUM1TixLQUFLLENBQUNwRCxJQUEvQyxJQUFxRCxLQUFLeE4sT0FBTCxDQUFhd0ssWUFBbEUsSUFBZ0YsQ0FBQ29HLEtBQUssQ0FBQzZNLEtBQU4sQ0FBWWlCLE1BQWhHLEVBQXVHO0FBQUMsWUFBSUYsa0JBQUosQ0FBdUI1TixLQUFLLENBQUM2TSxLQUE3QixFQUFtQztBQUFDbUIsa0JBQVEsRUFBQyxDQUFDLFdBQUQsRUFBYSxNQUFiLEVBQW9CLFNBQXBCLEVBQThCLFVBQTlCLEVBQXlDLFVBQXpDLEVBQW9ELFFBQXBELEVBQTZELFlBQTdELENBQVY7QUFBcUZDLHFCQUFXLEVBQUMsWUFBakc7QUFBOEdDLHFCQUFXLEVBQUMsR0FBMUg7QUFBOEhDLHFCQUFXLEVBQUMsT0FBMUk7QUFBa0pDLG9CQUFVLEVBQUMsRUFBN0o7QUFBZ0tDLHdCQUFjLEVBQUMsS0FBL0s7QUFBcUxDLCtCQUFxQixFQUFDLElBQTNNO0FBQWdOQyxpQ0FBdUIsRUFBQyxJQUF4TztBQUE2T0Msa0NBQXdCLEVBQUMsSUFBdFE7QUFBMlFDLGlCQUFPLEVBQUMsaUJBQVNaLElBQVQsRUFBYztBQUFDQSxnQkFBSSxDQUFDNUosZ0JBQUwsQ0FBc0IsWUFBdEIsRUFBbUMsWUFBVTtBQUFDakUsbUJBQUssQ0FBQzZNLEtBQU4sR0FBWTdNLEtBQUssQ0FBQ2pDLEdBQU4sQ0FBVTJRLFNBQXRCOztBQUFnQyxrQkFBRzFPLEtBQUssQ0FBQzZNLEtBQVQsRUFBZTtBQUFDLG9CQUFJOEIsU0FBUyxHQUFDM08sS0FBSyxDQUFDNk0sS0FBTixDQUFZK0IsZUFBMUI7O0FBQTBDLG9CQUFHRCxTQUFTLElBQUVBLFNBQVMsQ0FBQ2hMLFVBQXhCLEVBQW1DO0FBQUNnTCwyQkFBUyxDQUFDaEwsVUFBVixDQUFxQjRILFdBQXJCLENBQWlDb0QsU0FBakM7QUFBNkM7O0FBQ2xzQi9kLHFCQUFLLENBQUN3YyxhQUFOLENBQW9CcE4sS0FBcEI7QUFBNEI7QUFBQyxhQUQ0YyxFQUMzY0EsS0FEMmMsRUFDcmMsS0FEcWM7QUFDN2IsV0FEMko7QUFDMUo5USxlQUFLLEVBQUMsaUJBQVU7QUFBQzBCLGlCQUFLLENBQUN3YyxhQUFOLENBQW9CcE4sS0FBcEI7QUFBNEI7QUFENkcsU0FBbkM7QUFDdEUsT0FEbEMsTUFDdUMsSUFBRyxDQUFDQSxLQUFLLENBQUM2TSxLQUFOLENBQVluSyxNQUFoQixFQUF1QjtBQUFDMUMsYUFBSyxDQUFDNk0sS0FBTixDQUFZbkssTUFBWixHQUFtQjFDLEtBQUssQ0FBQzZNLEtBQU4sQ0FBWWdDLE9BQVosR0FBb0I3TyxLQUFLLENBQUM2TSxLQUFOLENBQVlpQyxnQkFBWixHQUE2QixZQUFVO0FBQUMsY0FBRzlPLEtBQUssQ0FBQ2pDLEdBQU4sQ0FBVW1QLFVBQWIsRUFBd0I7QUFBQ3RjLGlCQUFLLENBQUN3YyxhQUFOLENBQW9CcE4sS0FBcEI7O0FBQTJCcFAsaUJBQUssQ0FBQythLGVBQU47QUFBeUI7QUFBQyxTQUE3Sjs7QUFBOEozTCxhQUFLLENBQUM2TSxLQUFOLENBQVk1TSxHQUFaLEdBQWdCRCxLQUFLLENBQUNDLEdBQU4sQ0FBVXZQLE9BQVYsQ0FBa0IsS0FBbEIsRUFBd0IsRUFBeEIsRUFBNEIzRSxLQUE1QixDQUFrQyxHQUFsQyxFQUF1QyxDQUF2QyxDQUFoQjtBQUEyRDtBQUFDO0FBQUMsR0FGeWM7O0FBRXhjNkcsT0FBSyxDQUFDaVQsVUFBTixHQUFpQixZQUFVO0FBQUMsUUFBSTdGLEtBQUssR0FBQyxLQUFLc0ksUUFBTCxFQUFWOztBQUEwQixRQUFHdEksS0FBSyxJQUFFQSxLQUFLLENBQUNuWixJQUFOLEtBQWEsT0FBcEIsSUFBNkJtWixLQUFLLENBQUM2TSxLQUF0QyxFQUE0QztBQUFDOWQsV0FBSyxDQUFDbUQsV0FBTixDQUFrQjhOLEtBQUssQ0FBQ2pDLEdBQXhCLEVBQTRCLEtBQUtqRCxHQUFMLEdBQVMsVUFBckM7O0FBQWlELFVBQUcsQ0FBQ2tGLEtBQUssQ0FBQzZNLEtBQU4sQ0FBWXpJLE1BQWhCLEVBQXVCO0FBQUNwRSxhQUFLLENBQUNqQyxHQUFOLENBQVV3RixTQUFWLEdBQW9CLEVBQXBCO0FBQXVCeFUsYUFBSyxDQUFDbUQsV0FBTixDQUFrQjhOLEtBQUssQ0FBQ2pDLEdBQXhCLEVBQTRCLEtBQUtqRCxHQUFMLEdBQVMsVUFBckM7QUFBaUQ7QUFBUTs7QUFDbmxCLFVBQUdrRixLQUFLLENBQUNvQixNQUFOLEtBQWUsT0FBbEIsRUFBMEI7QUFBQyxZQUFHLE9BQU93TSxrQkFBUCxLQUE0QixVQUE1QixJQUF3QyxLQUFLeGUsT0FBTCxDQUFhd0ssWUFBeEQsRUFBcUU7QUFBQyxjQUFJaVUsSUFBSSxHQUFFN04sS0FBSyxDQUFDNk0sS0FBTixDQUFZbGhCLE9BQVosS0FBc0IsT0FBdkIsR0FBZ0NxVSxLQUFLLENBQUM2TSxLQUF0QyxHQUE0QzdNLEtBQUssQ0FBQzZNLEtBQU4sQ0FBWXpLLG9CQUFaLENBQWlDLE9BQWpDLEVBQTBDLENBQTFDLENBQXJEO0FBQWtHeUwsY0FBSSxDQUFDaFIsS0FBTDtBQUFjLFNBQXRMLE1BQTBMO0FBQUNtRCxlQUFLLENBQUM2TSxLQUFOLENBQVloUSxLQUFaO0FBQXFCO0FBQUMsT0FBNU8sTUFBZ1A7QUFBQyxZQUFHbUQsS0FBSyxDQUFDbkQsS0FBTixJQUFhbUQsS0FBSyxDQUFDb0IsTUFBTixLQUFlLGFBQS9CLEVBQTZDO0FBQUMsY0FBSWpTLE9BQU8sR0FBQyxTQUFPNlEsS0FBSyxDQUFDbkQsS0FBYixNQUFxQixRQUFyQixHQUE4QjlHLElBQUksQ0FBQ0MsU0FBTCxDQUFlZ0ssS0FBSyxDQUFDbkQsS0FBckIsQ0FBOUIsR0FBMEQ0USxNQUFNLENBQUN6TixLQUFLLENBQUNuRCxLQUFQLENBQTVFO0FBQTBGbUQsZUFBSyxDQUFDNk0sS0FBTixDQUFZYSxhQUFaLENBQTBCQyxXQUExQixDQUFzQ3hlLE9BQXRDLEVBQThDLEdBQTlDO0FBQW9ELFNBQTVMLE1BQWdNO0FBQUM2USxlQUFLLENBQUNqQyxHQUFOLENBQVV3RixTQUFWLEdBQW9CLEVBQXBCO0FBQXVCdkQsZUFBSyxDQUFDNk0sS0FBTixHQUFZLElBQVo7QUFBa0I7QUFBQztBQUFDO0FBQUMsR0FEdkk7O0FBQ3dJamEsT0FBSyxDQUFDbWMsV0FBTixHQUFrQixVQUFTQyxXQUFULEVBQXFCQyxXQUFyQixFQUFpQztBQUFDLFFBQUlqUCxLQUFLLEdBQUMsS0FBS1UsT0FBTCxDQUFhc08sV0FBYixDQUFWOztBQUFvQyxRQUFHLENBQUNoUCxLQUFKLEVBQVU7QUFBQztBQUFROztBQUMxa0IsUUFBRyxPQUFPQSxLQUFLLENBQUNwTyxLQUFiLEtBQXFCLFdBQXhCLEVBQW9DO0FBQUNvTyxXQUFLLENBQUNwTyxLQUFOLEdBQVksS0FBSzhPLE9BQUwsQ0FBYTFOLE9BQWIsQ0FBcUJnTixLQUFyQixDQUFaO0FBQXlDOztBQUM5RSxTQUFLa1AsVUFBTCxDQUFnQmxQLEtBQWhCO0FBQXVCLFNBQUttUCxXQUFMLENBQWlCblAsS0FBakIsRUFBdUJpUCxXQUF2QjtBQUFvQyxTQUFLRyxTQUFMLENBQWVwUCxLQUFmLEVBQXFCaVAsV0FBckI7QUFBbUMsR0FGaVk7O0FBRWhZcmMsT0FBSyxDQUFDc2MsVUFBTixHQUFpQixVQUFTbFAsS0FBVCxFQUFlO0FBQUMsUUFBRyxPQUFPQSxLQUFLLENBQUNqQyxHQUFiLEtBQW1CLFdBQXRCLEVBQWtDO0FBQUMsY0FBT2lDLEtBQUssQ0FBQ25aLElBQWI7QUFBbUIsYUFBSSxPQUFKO0FBQVltWixlQUFLLENBQUNqQyxHQUFOLEdBQVVoUCxLQUFLLENBQUNVLFFBQU4sQ0FBZSxLQUFmLEVBQXFCLEtBQUtxTCxHQUFMLEdBQVMsTUFBOUIsQ0FBVjtBQUFnRGtGLGVBQUssQ0FBQ2pDLEdBQU4sQ0FBVWtDLEdBQVYsR0FBY0QsS0FBSyxDQUFDQyxHQUFwQjtBQUF3Qjs7QUFBTSxhQUFJLE9BQUo7QUFBWUQsZUFBSyxDQUFDakMsR0FBTixHQUFVaFAsS0FBSyxDQUFDVSxRQUFOLENBQWUsS0FBZixFQUFxQixLQUFLcUwsR0FBTCxHQUFTLFFBQTlCLENBQVY7O0FBQWtELGNBQUdrRixLQUFLLENBQUN0RCxNQUFULEVBQWdCO0FBQUNzRCxpQkFBSyxDQUFDakMsR0FBTixDQUFVL04sS0FBVixDQUFnQmdULGVBQWhCLEdBQWdDLFVBQVFoRCxLQUFLLENBQUN0RCxNQUFkLEdBQXFCLElBQXJEO0FBQTJELFdBQTVFLE1BQWdGO0FBQUNzRCxpQkFBSyxDQUFDakMsR0FBTixDQUFVcUcsTUFBVixHQUFpQixJQUFqQjtBQUF1Qjs7QUFDdGI7O0FBQU0sYUFBSSxRQUFKO0FBQWFwRSxlQUFLLENBQUNqQyxHQUFOLEdBQVVoUCxLQUFLLENBQUNVLFFBQU4sQ0FBZSxRQUFmLEVBQXdCLEtBQUtxTCxHQUFMLEdBQVMsU0FBakMsQ0FBVjtBQUFzRGtGLGVBQUssQ0FBQ2pDLEdBQU4sQ0FBVVMsWUFBVixDQUF1QixpQkFBdkIsRUFBeUMsRUFBekM7QUFBNkN3QixlQUFLLENBQUNqQyxHQUFOLENBQVVTLFlBQVYsQ0FBdUIsYUFBdkIsRUFBcUMsQ0FBckM7QUFBd0N3QixlQUFLLENBQUNqQyxHQUFOLENBQVVrQyxHQUFWLEdBQWNELEtBQUssQ0FBQ0MsR0FBcEI7QUFBd0I7O0FBQU0sYUFBSSxNQUFKO0FBQVcsY0FBSWhZLE9BQU8sR0FBQytYLEtBQUssQ0FBQ0MsR0FBbEI7QUFBc0IsY0FBSW9QLE9BQU8sR0FBQ2hmLFFBQVEsQ0FBQzZKLGFBQVQsQ0FBdUJqUyxPQUF2QixDQUFaO0FBQTRDK1gsZUFBSyxDQUFDakMsR0FBTixHQUFVaFAsS0FBSyxDQUFDVSxRQUFOLENBQWUsS0FBZixFQUFxQixLQUFLcUwsR0FBTCxHQUFTLE9BQTlCLENBQVY7QUFBaURrRixlQUFLLENBQUNqQyxHQUFOLENBQVVFLFdBQVYsQ0FBc0JsUCxLQUFLLENBQUNVLFFBQU4sQ0FBZSxLQUFmLEVBQXFCLEtBQUtxTCxHQUFMLEdBQVMsYUFBOUIsQ0FBdEI7QUFBb0VrRixlQUFLLENBQUNqQyxHQUFOLENBQVVtUCxVQUFWLENBQXFCM0osU0FBckIsR0FBK0I4TCxPQUFPLEdBQUNBLE9BQU8sQ0FBQzlMLFNBQVQsR0FBbUIsSUFBekQ7QUFBOER2RCxlQUFLLENBQUNDLEdBQU4sR0FBVW9QLE9BQU8sR0FBQ0EsT0FBRCxHQUFTLEVBQTFCO0FBQTZCclAsZUFBSyxDQUFDakMsR0FBTixDQUFVcUcsTUFBVixHQUFpQixJQUFqQjtBQUFzQjtBQUQ1VTs7QUFFbkssVUFBRyxDQUFDcEUsS0FBSyxDQUFDblosSUFBUCxJQUFhLENBQUNtWixLQUFLLENBQUNDLEdBQXZCLEVBQTJCO0FBQUNELGFBQUssQ0FBQ2pDLEdBQU4sR0FBVWhQLEtBQUssQ0FBQ1UsUUFBTixDQUFlLEtBQWYsRUFBcUIsS0FBS3FMLEdBQUwsR0FBUyxRQUE5QixDQUFWO0FBQWtEa0YsYUFBSyxDQUFDakMsR0FBTixDQUFVaUIsV0FBVixHQUFzQixLQUFLNVAsT0FBTCxDQUFhZ0osU0FBbkM7QUFBNkM0SCxhQUFLLENBQUNqQyxHQUFOLENBQVVxRyxNQUFWLEdBQWlCLElBQWpCO0FBQXNCcEUsYUFBSyxDQUFDakMsR0FBTixDQUFVN08sS0FBVixHQUFnQixJQUFoQjtBQUFxQkgsYUFBSyxDQUFDbUMsYUFBTixDQUFvQixJQUFwQixFQUF5QixXQUF6QixFQUFxQyxXQUFyQyxFQUFpRCxLQUFLd1AsT0FBTCxDQUFhblksSUFBOUQsRUFBbUUrbUIsUUFBUSxDQUFDdFAsS0FBSyxDQUFDcE8sS0FBUCxFQUFhLEVBQWIsQ0FBM0U7QUFBOEY7QUFBQztBQUFDLEdBRnZLOztBQUV3S2dCLE9BQUssQ0FBQ3VjLFdBQU4sR0FBa0IsVUFBU25QLEtBQVQsRUFBZWlQLFdBQWYsRUFBMkI7QUFBQyxRQUFJdlksS0FBSyxHQUFDLEtBQUt1RSxNQUFMLENBQVlnVSxXQUFaLENBQVY7QUFBQSxRQUFtQzFVLE1BQU0sR0FBQzdELEtBQUssQ0FBQ3dXLFVBQWhEO0FBQUEsUUFBMkQ3b0IsTUFBM0Q7O0FBQWtFLFFBQUcsQ0FBQ2tXLE1BQU0sQ0FBQ2dWLGlCQUFYLEVBQTZCO0FBQUMsVUFBSUMsUUFBUSxHQUFDbmYsUUFBUSxDQUFDMmMsc0JBQVQsRUFBYjtBQUErQzNvQixZQUFNLEdBQUMwSyxLQUFLLENBQUNVLFFBQU4sQ0FBZSxLQUFmLEVBQXFCLEtBQUtxTCxHQUFMLEdBQVMsU0FBOUIsQ0FBUDtBQUFnRDBVLGNBQVEsQ0FBQ3ZSLFdBQVQsQ0FBcUI1WixNQUFyQjtBQUE2Qm1yQixjQUFRLENBQUN2UixXQUFULENBQXFCK0IsS0FBSyxDQUFDakMsR0FBM0I7QUFBZ0N4RCxZQUFNLENBQUMwRCxXQUFQLENBQW1CdVIsUUFBbkI7QUFBOEIsS0FBeE4sTUFBNE47QUFBQyxVQUFJQyxRQUFRLEdBQUNsVixNQUFNLENBQUNtVSxTQUFwQjtBQUE4QnJxQixZQUFNLEdBQUNrVyxNQUFNLENBQUMyUyxVQUFkO0FBQXlCN29CLFlBQU0sQ0FBQzJMLEtBQVAsQ0FBYTBmLFVBQWIsR0FBd0IsRUFBeEI7O0FBQTJCLFVBQUcxUCxLQUFLLENBQUNqQyxHQUFOLEtBQVkwUixRQUFmLEVBQXdCO0FBQUMsWUFBSTFTLE1BQU0sR0FBQ3hDLE1BQU0sQ0FBQ2dWLGlCQUFQLEtBQTJCLENBQTNCLEdBQTZCLGFBQTdCLEdBQTJDLGNBQXREO0FBQXFFaFYsY0FBTSxDQUFDd0MsTUFBRCxDQUFOLENBQWVpRCxLQUFLLENBQUNqQyxHQUFyQixFQUF5QjBSLFFBQXpCO0FBQW9DO0FBQUM7O0FBQ3p5Qi9ZLFNBQUssQ0FBQ3NKLEtBQU4sR0FBWUEsS0FBSyxDQUFDcE8sS0FBbEI7QUFBeUIsR0FEOE87O0FBQzdPZ0IsT0FBSyxDQUFDd2MsU0FBTixHQUFnQixVQUFTcFAsS0FBVCxFQUFlaVAsV0FBZixFQUEyQjtBQUFDLFFBQUdqUCxLQUFLLENBQUNqQyxHQUFOLENBQVVxRyxNQUFiLEVBQW9CO0FBQUMsV0FBS3VMLFNBQUwsQ0FBZTNQLEtBQWYsRUFBcUJpUCxXQUFyQjtBQUFrQztBQUFROztBQUNySSxRQUFJcmUsS0FBSyxHQUFDLElBQVY7QUFBQSxRQUFlbU4sR0FBRyxHQUFDaUMsS0FBSyxDQUFDblosSUFBTixLQUFhLFFBQWIsR0FBc0JtWixLQUFLLENBQUNqQyxHQUE1QixHQUFnQ2lDLEtBQUssQ0FBQ2pDLEdBQU4sQ0FBVStELEdBQVYsR0FBYyxJQUFJelgsS0FBSixFQUFqRTs7QUFBNkUsUUFBSXVsQixVQUFVLEdBQUMsU0FBWEEsVUFBVyxHQUFVO0FBQUMsVUFBRyxDQUFDNVAsS0FBSyxDQUFDakMsR0FBTixDQUFVN08sS0FBZCxFQUFvQjtBQUFDSCxhQUFLLENBQUNtQyxhQUFOLENBQW9CTixLQUFwQixFQUEwQixXQUExQixFQUFzQyxjQUF0QyxFQUFxREEsS0FBSyxDQUFDOFAsT0FBTixDQUFjblksSUFBbkUsRUFBd0UrbUIsUUFBUSxDQUFDdFAsS0FBSyxDQUFDcE8sS0FBUCxFQUFhLEVBQWIsQ0FBaEY7QUFBbUc7O0FBQy9Ob08sV0FBSyxDQUFDakMsR0FBTixDQUFVcUcsTUFBVixHQUFpQnBFLEtBQUssQ0FBQ25aLElBQU4sS0FBYSxRQUFiLEdBQXNCLElBQXRCLEdBQTJCLEtBQTVDOztBQUFrRCtKLFdBQUssQ0FBQytlLFNBQU4sQ0FBZ0IzUCxLQUFoQixFQUFzQmlQLFdBQXRCO0FBQW9DLEtBRFQ7O0FBQ1VsUixPQUFHLENBQUMyRSxNQUFKLEdBQVdrTixVQUFYOztBQUFzQjdSLE9BQUcsQ0FBQzhRLE9BQUosR0FBWSxVQUFTcG9CLENBQVQsRUFBVztBQUFDLFVBQUd1WixLQUFLLENBQUNuWixJQUFOLEtBQWEsT0FBaEIsRUFBd0I7QUFBQ21aLGFBQUssQ0FBQ2pDLEdBQU4sR0FBVWhQLEtBQUssQ0FBQ1UsUUFBTixDQUFlLEdBQWYsRUFBbUJtQixLQUFLLENBQUNrSyxHQUFOLEdBQVUsUUFBN0IsQ0FBVjtBQUFpRGtGLGFBQUssQ0FBQ2pDLEdBQU4sQ0FBVWlCLFdBQVYsR0FBc0JwTyxLQUFLLENBQUN4QixPQUFOLENBQWMrSSxTQUFwQztBQUE4QzZILGFBQUssQ0FBQ2pDLEdBQU4sQ0FBVTdPLEtBQVYsR0FBZ0IsSUFBaEI7O0FBQXFCMEIsYUFBSyxDQUFDdWUsV0FBTixDQUFrQm5QLEtBQWxCLEVBQXdCaVAsV0FBeEI7QUFBc0M7O0FBQ3hUbGdCLFdBQUssQ0FBQ21DLGFBQU4sQ0FBb0JOLEtBQXBCLEVBQTBCLFdBQTFCLEVBQXNDLFdBQXRDLEVBQWtEQSxLQUFLLENBQUM4UCxPQUFOLENBQWNuWSxJQUFoRSxFQUFxRSttQixRQUFRLENBQUN0UCxLQUFLLENBQUNwTyxLQUFQLEVBQWEsRUFBYixDQUE3RTtBQUErRmdlLGdCQUFVO0FBQUksS0FEQTs7QUFDQzdSLE9BQUcsQ0FBQ2tDLEdBQUosR0FBU0QsS0FBSyxDQUFDblosSUFBTixLQUFhLE9BQWQsR0FBdUJtWixLQUFLLENBQUN0RCxNQUE3QixHQUFvQ3NELEtBQUssQ0FBQ0MsR0FBbEQ7QUFBdUQsR0FIM0k7O0FBRzRJck4sT0FBSyxDQUFDaVcsV0FBTixHQUFrQixVQUFTblMsS0FBVCxFQUFlO0FBQUMsUUFBRyxDQUFDLEtBQUtnSyxPQUFULEVBQWlCO0FBQUM7QUFBUTs7QUFDbE8sUUFBSTlPLEtBQUssR0FBQzhFLEtBQUssQ0FBQ3NKLEtBQWhCO0FBQUEsUUFBc0JBLEtBQUssR0FBQyxLQUFLVSxPQUFMLENBQWE5TyxLQUFiLENBQTVCOztBQUFnRCxRQUFHLENBQUNvTyxLQUFELElBQVEsQ0FBQ0EsS0FBSyxDQUFDakMsR0FBbEIsRUFBc0I7QUFBQztBQUFROztBQUMvRSxRQUFHLEtBQUszTyxPQUFMLENBQWEwSCxNQUFiLElBQXFCa0osS0FBSyxDQUFDblosSUFBTixLQUFhLE9BQWxDLElBQTJDLENBQUNtWixLQUFLLENBQUNqQyxHQUFOLENBQVVxRyxNQUF0RCxJQUE4RCxDQUFDcEUsS0FBSyxDQUFDakMsR0FBTixDQUFVcFQsUUFBekUsSUFBbUYsQ0FBQ3FWLEtBQUssQ0FBQ2pDLEdBQU4sQ0FBVThSLFlBQWpHLEVBQThHO0FBQUM3UCxXQUFLLENBQUNqQyxHQUFOLENBQVUyRSxNQUFWLEdBQWlCLElBQWpCO0FBQXNCMUMsV0FBSyxDQUFDakMsR0FBTixDQUFVOFEsT0FBVixHQUFrQixJQUFsQjtBQUF1QjdPLFdBQUssQ0FBQ2pDLEdBQU4sQ0FBVWtDLEdBQVYsR0FBYyxFQUFkOztBQUFpQixVQUFHRCxLQUFLLENBQUNqQyxHQUFOLENBQVUrRCxHQUFiLEVBQWlCO0FBQUM5QixhQUFLLENBQUNqQyxHQUFOLENBQVUrRCxHQUFWLENBQWNZLE1BQWQsR0FBcUIsSUFBckI7QUFBMEIxQyxhQUFLLENBQUNqQyxHQUFOLENBQVUrRCxHQUFWLENBQWMrTSxPQUFkLEdBQXNCLElBQXRCO0FBQTJCN08sYUFBSyxDQUFDakMsR0FBTixDQUFVK0QsR0FBVixDQUFjN0IsR0FBZCxHQUFrQixFQUFsQjtBQUFxQixlQUFPRCxLQUFLLENBQUNqQyxHQUFOLENBQVUrRCxHQUFqQjtBQUFzQjs7QUFDL1IsYUFBTzlCLEtBQUssQ0FBQ2pDLEdBQWI7QUFBa0IsS0FEbEIsTUFFSyxJQUFHaUMsS0FBSyxDQUFDblosSUFBTixLQUFhLE9BQWIsSUFBc0JtWixLQUFLLENBQUNvQixNQUFOLEtBQWUsT0FBckMsSUFBOENwQixLQUFLLENBQUNqQyxHQUFOLENBQVVtUCxVQUEzRCxFQUFzRTtBQUFDbE4sV0FBSyxDQUFDNk0sS0FBTixHQUFZLElBQVo7QUFBaUI3TSxXQUFLLENBQUNqQyxHQUFOLENBQVV3TixXQUFWLENBQXNCdkwsS0FBSyxDQUFDakMsR0FBTixDQUFVbVAsVUFBaEM7QUFBNkM7QUFBQyxHQUoyQjs7QUFJMUJ0YSxPQUFLLENBQUNrVyxXQUFOLEdBQWtCLFVBQVN2TyxNQUFULEVBQWdCO0FBQUMsUUFBSThVLE9BQU8sR0FBQzlVLE1BQU0sQ0FBQzJTLFVBQW5COztBQUE4QixRQUFHLENBQUNtQyxPQUFKLEVBQVk7QUFBQztBQUFROztBQUNsTyxXQUFNQSxPQUFPLENBQUNuQyxVQUFkLEVBQXlCO0FBQUNtQyxhQUFPLENBQUM5RCxXQUFSLENBQW9COEQsT0FBTyxDQUFDbkMsVUFBNUI7QUFBeUM7QUFBQyxHQUR3RTs7QUFDdkV0YSxPQUFLLENBQUMrYyxTQUFOLEdBQWdCLFVBQVMzUCxLQUFULEVBQWVpUCxXQUFmLEVBQTJCO0FBQUMsUUFBSXhZLE1BQU0sR0FBQyxLQUFLQSxNQUFoQjs7QUFBdUIsUUFBRyxLQUFLckgsT0FBTCxDQUFhZ0ksWUFBYixJQUEyQixDQUFDWCxNQUFNLENBQUN0QyxNQUFuQyxJQUEyQyxDQUFDNkwsS0FBSyxDQUFDakMsR0FBTixDQUFVK1IsUUFBekQsRUFBa0U7QUFBQztBQUFROztBQUNuTixRQUFJcFosS0FBSyxHQUFDLEtBQUt1RSxNQUFMLENBQVlnVSxXQUFaLENBQVY7QUFBQSxRQUFtQ3ZPLE9BQU8sR0FBQyxLQUFLQSxPQUFoRDtBQUFBLFFBQXdEbkcsTUFBTSxHQUFDN0QsS0FBSyxDQUFDd1csVUFBckU7QUFBQSxRQUFnRjdvQixNQUFNLEdBQUNrVyxNQUFNLENBQUMyUyxVQUE5RjtBQUFBLFFBQXlHclcsT0FBTyxHQUFDLEtBQUt6SCxPQUFMLENBQWF5SCxPQUE5SDtBQUFzSSxTQUFLc1IsWUFBTCxDQUFrQm5JLEtBQWxCLEVBQXdCdEosS0FBeEI7O0FBQStCLFFBQUdzSixLQUFLLENBQUNwTyxLQUFOLEtBQWM4TyxPQUFPLENBQUM5TyxLQUF6QixFQUErQjtBQUFDLFdBQUt5VyxVQUFMO0FBQW1COztBQUN4TnRaLFNBQUssQ0FBQ2tELFFBQU4sQ0FBZStOLEtBQUssQ0FBQ2pDLEdBQXJCLEVBQXlCLEtBQUtqRCxHQUFMLEdBQVMsZUFBbEM7QUFBbURrRixTQUFLLENBQUNqQyxHQUFOLENBQVUrUixRQUFWLEdBQW1CLElBQW5COztBQUF3QixRQUFHcFosS0FBSyxDQUFDc0osS0FBTixLQUFjQSxLQUFLLENBQUNwTyxLQUF2QixFQUE2QjtBQUFDdk4sWUFBTSxDQUFDMkwsS0FBUCxDQUFhMGYsVUFBYixHQUF3QixRQUF4QjtBQUFpQ2hQLGFBQU8sQ0FBQzBELE1BQVIsSUFBZ0IsQ0FBaEI7O0FBQWtCLFVBQUcxRCxPQUFPLENBQUMwRCxNQUFSLEtBQWlCdk4sT0FBakIsSUFBMEJBLE9BQU8sR0FBQyxDQUFyQyxFQUF1QztBQUFDLGFBQUs4TixRQUFMLENBQWM5TixPQUFPLEdBQUMsQ0FBdEI7QUFBMEI7QUFBQzs7QUFDL04sUUFBR21KLEtBQUssQ0FBQ25aLElBQU4sS0FBYSxRQUFoQixFQUF5QjtBQUFDbVosV0FBSyxDQUFDakMsR0FBTixDQUFVcUcsTUFBVixHQUFpQixLQUFqQjtBQUF3QjtBQUFDLEdBSGtCOztBQUdqQnhSLE9BQUssQ0FBQ3VWLFlBQU4sR0FBbUIsVUFBU25JLEtBQVQsRUFBZXRKLEtBQWYsRUFBcUI7QUFBQyxRQUFJNkssTUFBTSxHQUFDdkIsS0FBSyxDQUFDakMsR0FBakI7QUFBQSxRQUFxQnRILE1BQU0sR0FBQyxLQUFLQSxNQUFqQztBQUFBLFFBQXdDc1osUUFBUSxHQUFDeE8sTUFBTSxDQUFDd08sUUFBeEQ7QUFBQSxRQUFpRXBaLE1BQU0sR0FBQyxLQUFLaVMsY0FBTCxFQUF4RTs7QUFBOEYsUUFBR3JILE1BQU0sQ0FBQ3JTLEtBQVYsRUFBZ0I7QUFBQztBQUFROztBQUNwTixRQUFHLENBQUM2Z0IsUUFBRCxJQUFXQSxRQUFRLENBQUN0a0IsS0FBVCxLQUFpQmdMLE1BQU0sQ0FBQ2hMLEtBQW5DLElBQTBDc2tCLFFBQVEsQ0FBQ3JrQixNQUFULEtBQWtCK0ssTUFBTSxDQUFDL0ssTUFBUCxHQUFjaUwsTUFBN0UsRUFBb0Y7QUFBQyxXQUFLcVosZ0JBQUwsQ0FBc0JoUSxLQUF0QixFQUE0QnRKLEtBQTVCO0FBQW1DLFdBQUt1WixZQUFMLENBQWtCalEsS0FBbEIsRUFBd0J0SixLQUF4QjtBQUErQixXQUFLd1osWUFBTCxDQUFrQmxRLEtBQWxCLEVBQXdCdEosS0FBeEI7QUFBK0IsV0FBS3laLGNBQUwsQ0FBb0JuUSxLQUFwQixFQUEwQnRKLEtBQTFCO0FBQWtDOztBQUN4TixRQUFJMUcsS0FBSyxHQUFDdVIsTUFBTSxDQUFDdlIsS0FBakI7QUFBdUJBLFNBQUssQ0FBQ3ZFLEtBQU4sR0FBWThWLE1BQU0sQ0FBQ2dILElBQVAsQ0FBWTljLEtBQVosR0FBa0IsSUFBOUI7QUFBbUN1RSxTQUFLLENBQUN0RSxNQUFOLEdBQWE2VixNQUFNLENBQUNnSCxJQUFQLENBQVk3YyxNQUFaLEdBQW1CLElBQWhDO0FBQXFDc0UsU0FBSyxDQUFDekUsSUFBTixHQUFXZ1csTUFBTSxDQUFDNk8sTUFBUCxDQUFjN2tCLElBQWQsR0FBbUIsSUFBOUI7QUFBbUN5RSxTQUFLLENBQUM3RSxHQUFOLEdBQVVvVyxNQUFNLENBQUM2TyxNQUFQLENBQWNqbEIsR0FBZCxHQUFrQixJQUE1QjtBQUFrQyxHQUZoSDs7QUFFaUh5SCxPQUFLLENBQUNvZCxnQkFBTixHQUF1QixVQUFTaFEsS0FBVCxFQUFldEosS0FBZixFQUFxQjtBQUFDLFFBQUllLE9BQU8sR0FBQyxLQUFLNkMsR0FBTCxDQUFTaUYsWUFBckI7QUFBQSxRQUFrQ1gsTUFBTSxHQUFDLEtBQUt0RSxHQUFMLENBQVNzRSxNQUFULENBQWdCbFQsTUFBekQ7QUFBQSxRQUFnRTJqQixPQUFPLEdBQUM1WCxPQUFPLENBQUM4TCxTQUFoRjtBQUFBLFFBQTBGNU0sTUFBTSxHQUFDLEtBQUtpUyxjQUFMLEVBQWpHOztBQUF1SCxRQUFHLEtBQUt4WixPQUFMLENBQWFxSSxPQUFiLElBQXNCLEtBQUswRCxNQUFMLENBQVkxRCxPQUFsQyxJQUEyQ3VJLEtBQUssQ0FBQ3ZJLE9BQXBELEVBQTREO0FBQUNBLGFBQU8sQ0FBQzhMLFNBQVIsR0FBa0J2RCxLQUFLLENBQUN2SSxPQUF4QjtBQUFnQ0EsYUFBTyxDQUFDL0wsTUFBUixHQUFlOEMsSUFBSSxDQUFDQyxHQUFMLENBQVNtUSxNQUFULEVBQWdCMFEsUUFBUSxDQUFDN1gsT0FBTyxDQUFDb0gsWUFBVCxFQUFzQixFQUF0QixDQUF4QixLQUFvREQsTUFBbkU7QUFBMEVuSCxhQUFPLENBQUM4TCxTQUFSLEdBQWtCOEwsT0FBbEI7QUFBMkIsS0FBbE0sTUFBc007QUFBQzVYLGFBQU8sQ0FBQy9MLE1BQVIsR0FBZWlMLE1BQU0sR0FBQyxDQUFELEdBQUdpSSxNQUF4QjtBQUFnQzs7QUFDaGpCbEksU0FBSyxDQUFDakwsS0FBTixHQUFZLEtBQUtnTCxNQUFMLENBQVloTCxLQUF4QjtBQUE4QmlMLFNBQUssQ0FBQ2hMLE1BQU4sR0FBYSxLQUFLK0ssTUFBTCxDQUFZL0ssTUFBWixHQUFtQmtULE1BQW5CLEdBQTBCbkgsT0FBTyxDQUFDL0wsTUFBbEMsR0FBeUNpTCxNQUF0RDtBQUE4RCxHQUR5RTs7QUFDeEUvRCxPQUFLLENBQUNxZCxZQUFOLEdBQW1CLFVBQVNqUSxLQUFULEVBQWV0SixLQUFmLEVBQXFCO0FBQUMsUUFBSTZSLElBQUksR0FBQ3ZJLEtBQUssQ0FBQ2pDLEdBQU4sQ0FBVXdLLElBQVYsR0FBZSxFQUF4Qjs7QUFBMkIsWUFBT3ZJLEtBQUssQ0FBQ25aLElBQWI7QUFBbUIsV0FBSSxPQUFKO0FBQVkwaEIsWUFBSSxDQUFDOWMsS0FBTCxHQUFXdVUsS0FBSyxDQUFDakMsR0FBTixDQUFVOFIsWUFBckI7QUFBa0N0SCxZQUFJLENBQUM3YyxNQUFMLEdBQVlzVSxLQUFLLENBQUNqQyxHQUFOLENBQVVzUyxhQUF0QjtBQUFvQzs7QUFBTSxXQUFJLE9BQUo7QUFBWTlILFlBQUksQ0FBQzljLEtBQUwsR0FBVyxLQUFLMkQsT0FBTCxDQUFhMEssYUFBeEI7QUFBc0N5TyxZQUFJLENBQUM3YyxNQUFMLEdBQVk2YyxJQUFJLENBQUM5YyxLQUFMLEdBQVcsS0FBSzJELE9BQUwsQ0FBYXlLLFVBQXBDO0FBQStDOztBQUFNLFdBQUksUUFBSjtBQUFhME8sWUFBSSxDQUFDOWMsS0FBTCxHQUFXdVUsS0FBSyxDQUFDdlUsS0FBTixHQUFZdVUsS0FBSyxDQUFDdlUsS0FBbEIsR0FBd0JpTCxLQUFLLENBQUNqTCxLQUFOLEdBQVksR0FBWixHQUFnQmlMLEtBQUssQ0FBQ2pMLEtBQU4sR0FBWSxHQUE1QixHQUFnQ2lMLEtBQUssQ0FBQ2pMLEtBQXpFO0FBQStFOGMsWUFBSSxDQUFDN2MsTUFBTCxHQUFZc1UsS0FBSyxDQUFDdFUsTUFBTixHQUFhc1UsS0FBSyxDQUFDdFUsTUFBbkIsR0FBMEJnTCxLQUFLLENBQUNoTCxNQUE1QztBQUFtRDs7QUFBTSxXQUFJLE1BQUo7QUFBVzZjLFlBQUksQ0FBQzljLEtBQUwsR0FBV3VVLEtBQUssQ0FBQ3ZVLEtBQU4sR0FBWXVVLEtBQUssQ0FBQ3ZVLEtBQWxCLEdBQXdCaUwsS0FBSyxDQUFDakwsS0FBekM7QUFBK0M4YyxZQUFJLENBQUM3YyxNQUFMLEdBQVlzVSxLQUFLLENBQUN0VSxNQUFOLEdBQWFzVSxLQUFLLENBQUN0VSxNQUFuQixHQUEwQmdMLEtBQUssQ0FBQ2hMLE1BQTVDO0FBQW1EO0FBQXBkO0FBQTRkLEdBQWhpQjs7QUFBaWlCa0gsT0FBSyxDQUFDc2QsWUFBTixHQUFtQixVQUFTbFEsS0FBVCxFQUFldEosS0FBZixFQUFxQjtBQUFDLFFBQUlELE1BQU0sR0FBQyxLQUFLQSxNQUFoQjtBQUFBLFFBQXVCckgsT0FBTyxHQUFDLEtBQUtBLE9BQXBDO0FBQUEsUUFBNEN5VSxJQUFJLEdBQUN6VSxPQUFPLENBQUNvSixNQUF6RDtBQUFBLFFBQWdFK1AsSUFBSSxHQUFDdkksS0FBSyxDQUFDakMsR0FBTixDQUFVd0ssSUFBL0U7QUFBQSxRQUFvRitILEtBQUssR0FBQy9ILElBQUksQ0FBQzljLEtBQUwsR0FBVzhjLElBQUksQ0FBQzdjLE1BQTFHO0FBQUEsUUFBaUhpTCxNQUFNLEdBQUMsS0FBS2lTLGNBQUwsRUFBeEg7QUFBQSxRQUE4STJILFdBQVcsR0FBQzlaLE1BQU0sQ0FBQ2hMLEtBQVAsSUFBYyxHQUFkLElBQW1CZ0wsTUFBTSxDQUFDL0ssTUFBUCxJQUFlLEdBQTVMO0FBQUEsUUFBZ004a0IsV0FBVyxHQUFDLENBQUMsT0FBRCxFQUFTLFFBQVQsRUFBa0IsTUFBbEIsRUFBMEJ4ZCxPQUExQixDQUFrQ2dOLEtBQUssQ0FBQ25aLElBQXhDLElBQThDLENBQTFQO0FBQUEsUUFBNFBvUixXQUFXLEdBQUM3SSxPQUFPLENBQUM2SSxXQUFSLElBQXFCc1ksV0FBN1I7QUFBQSxRQUF5UzlrQixLQUF6UztBQUFBLFFBQStTQyxNQUEvUztBQUFzVCxRQUFJK2tCLFNBQVMsR0FBQyxDQUFDL1osS0FBSyxDQUFDaEwsTUFBUCxDQUFkOztBQUE2QixRQUFHLENBQUN1TSxXQUFXLElBQUU3SSxPQUFPLENBQUM4SSxRQUF0QixLQUFpQ3NZLFdBQXBDLEVBQWdEO0FBQUNDLGVBQVMsQ0FBQ0MsT0FBVixDQUFrQmphLE1BQU0sQ0FBQy9LLE1BQVAsR0FBY2lMLE1BQWhDO0FBQXlDOztBQUNwbEM4WixhQUFTLENBQUN6ZixPQUFWLENBQWtCLFVBQVMrZSxRQUFULEVBQWtCO0FBQUMsVUFBRyxDQUFDcmtCLE1BQUQsSUFBU0EsTUFBTSxHQUFDK0ssTUFBTSxDQUFDL0ssTUFBUCxHQUFjaUwsTUFBakMsRUFBd0M7QUFBQ2xMLGFBQUssR0FBQytDLElBQUksQ0FBQ2daLEdBQUwsQ0FBU2UsSUFBSSxDQUFDOWMsS0FBZCxFQUFvQjZrQixLQUFLLEdBQUNQLFFBQTFCLENBQU47QUFBMEN0a0IsYUFBSyxHQUFDQSxLQUFLLEdBQUNpTCxLQUFLLENBQUNqTCxLQUFaLEdBQWtCaUwsS0FBSyxDQUFDakwsS0FBeEIsR0FBOEIrQyxJQUFJLENBQUNvSCxLQUFMLENBQVduSyxLQUFYLENBQXBDO0FBQXNEQyxjQUFNLEdBQUM4QyxJQUFJLENBQUNtaUIsSUFBTCxDQUFVLElBQUVMLEtBQUYsR0FBUTdrQixLQUFsQixDQUFQO0FBQWdDQyxjQUFNLEdBQUNBLE1BQU0sR0FBQ3FrQixRQUFQLEdBQWdCLENBQWhCLEdBQWtCQSxRQUFsQixHQUEyQnJrQixNQUFsQztBQUEwQztBQUFDLEtBQXpQO0FBQTJQLFFBQUk2RyxLQUFLLEdBQUNxZSxNQUFNLENBQUMsQ0FBQ3JJLElBQUksQ0FBQzljLEtBQUwsR0FBV0EsS0FBWixFQUFtQm9sQixPQUFuQixDQUEyQixDQUEzQixDQUFELENBQWhCO0FBQWdEaE4sUUFBSSxHQUFDQSxJQUFJLEtBQUcsTUFBUCxHQUFjdFIsS0FBZCxHQUFvQnNSLElBQXpCO0FBQThCN0QsU0FBSyxDQUFDakMsR0FBTixDQUFVd0ssSUFBVixHQUFlO0FBQUM5YyxXQUFLLEVBQUNBLEtBQVA7QUFBYUMsWUFBTSxFQUFDQSxNQUFwQjtBQUEyQjZHLFdBQUssRUFBQ0EsS0FBSyxJQUFFbkQsT0FBTyxDQUFDcUosT0FBZixHQUF1QmpLLElBQUksQ0FBQ2daLEdBQUwsQ0FBU3BZLE9BQU8sQ0FBQ3NKLE9BQWpCLEVBQXlCbUwsSUFBekIsQ0FBdkIsR0FBc0Q7QUFBdkYsS0FBZjtBQUEwRyxHQUQyTTs7QUFDMU1qUixPQUFLLENBQUN1ZCxjQUFOLEdBQXFCLFVBQVNuUSxLQUFULEVBQWV0SixLQUFmLEVBQXFCO0FBQUMsUUFBSTZSLElBQUksR0FBQ3ZJLEtBQUssQ0FBQ2pDLEdBQU4sQ0FBVXdLLElBQW5CO0FBQUEsUUFBd0I5UixNQUFNLEdBQUMsS0FBS0EsTUFBcEM7QUFBQSxRQUEyQ21JLE1BQU0sR0FBQyxLQUFLdEUsR0FBTCxDQUFTc0UsTUFBVCxDQUFnQmxULE1BQWxFO0FBQUEsUUFBeUVpTCxNQUFNLEdBQUMsS0FBS2lTLGNBQUwsRUFBaEY7QUFBQSxRQUFzR2tJLE9BQU8sR0FBQyxDQUE5Rzs7QUFBZ0gsUUFBR3ZJLElBQUksQ0FBQzdjLE1BQUwsSUFBYWdMLEtBQUssQ0FBQ2hMLE1BQXRCLEVBQTZCO0FBQUNvbEIsYUFBTyxHQUFDbFMsTUFBTSxHQUFDLENBQUNsSSxLQUFLLENBQUNoTCxNQUFOLEdBQWE2YyxJQUFJLENBQUM3YyxNQUFuQixJQUEyQixHQUExQztBQUErQzs7QUFDNXBCc1UsU0FBSyxDQUFDakMsR0FBTixDQUFVcVMsTUFBVixHQUFpQjtBQUFDamxCLFNBQUcsRUFBQzJsQixPQUFPLEdBQUMsQ0FBUixHQUFVLENBQVYsR0FBWXRpQixJQUFJLENBQUNvSCxLQUFMLENBQVdrYixPQUFYLENBQWpCO0FBQXFDdmxCLFVBQUksRUFBQ2lELElBQUksQ0FBQ29ILEtBQUwsQ0FBVyxDQUFDYyxLQUFLLENBQUNqTCxLQUFOLEdBQVk4YyxJQUFJLENBQUM5YyxLQUFsQixJQUF5QixHQUFwQztBQUExQyxLQUFqQjtBQUFxR3VVLFNBQUssQ0FBQ2pDLEdBQU4sQ0FBVWdTLFFBQVYsR0FBbUI7QUFBQ3RrQixXQUFLLEVBQUNnTCxNQUFNLENBQUNoTCxLQUFkO0FBQW9CQyxZQUFNLEVBQUMrSyxNQUFNLENBQUMvSyxNQUFQLEdBQWNpTDtBQUF6QyxLQUFuQjtBQUFxRSxHQUQwUTs7QUFDelEvRCxPQUFLLENBQUNtZSxhQUFOLEdBQW9CLFVBQVN4ZSxLQUFULEVBQWU7QUFBQyxRQUFJeU4sS0FBSyxHQUFDLEtBQUtzSSxRQUFMLEVBQVY7O0FBQTBCLFFBQUcsQ0FBQ3RJLEtBQUssQ0FBQ2pDLEdBQVAsSUFBWSxDQUFDaUMsS0FBSyxDQUFDakMsR0FBTixDQUFVd0ssSUFBMUIsRUFBK0I7QUFBQyxhQUFNO0FBQUNwZCxXQUFHLEVBQUMsQ0FBTDtBQUFPQyxjQUFNLEVBQUMsQ0FBZDtBQUFnQkcsWUFBSSxFQUFDLENBQXJCO0FBQXVCQyxhQUFLLEVBQUM7QUFBN0IsT0FBTjtBQUF1Qzs7QUFDaFQsUUFBSStjLElBQUksR0FBQ3ZJLEtBQUssQ0FBQ2pDLEdBQU4sQ0FBVXdLLElBQW5CO0FBQUEsUUFBd0I2SCxNQUFNLEdBQUNwUSxLQUFLLENBQUNqQyxHQUFOLENBQVVxUyxNQUF6QztBQUFBLFFBQWdEMWtCLE1BQU0sR0FBQyxLQUFLK0ssTUFBTCxDQUFZL0ssTUFBbkU7QUFBQSxRQUEwRUQsS0FBSyxHQUFDLEtBQUtnTCxNQUFMLENBQVloTCxLQUE1RjtBQUFBLFFBQWtHdWxCLElBQUksR0FBQyxDQUFDdGxCLE1BQU0sR0FBQzZjLElBQUksQ0FBQzdjLE1BQWIsSUFBcUIsR0FBNUg7QUFBQSxRQUFnSXVsQixJQUFJLEdBQUNiLE1BQU0sQ0FBQ2psQixHQUFQLEdBQVcsQ0FBWCxHQUFhNmxCLElBQWxKO0FBQUEsUUFBdUpFLElBQUksR0FBQyxDQUFDRixJQUFJLEdBQUNDLElBQU4sSUFBWSxHQUF4SztBQUFBLFFBQTRLRSxJQUFJLEdBQUNELElBQUksR0FBQzNlLEtBQUwsR0FBVzJlLElBQUksR0FBQyxDQUFoQixHQUFrQkQsSUFBbk07QUFBQSxRQUF3TTFsQixJQUFJLEdBQUNnZCxJQUFJLENBQUM5YyxLQUFMLEdBQVcsQ0FBWCxJQUFjOEcsS0FBSyxHQUFDLENBQXBCLElBQXVCNmQsTUFBTSxDQUFDN2tCLElBQTNPO0FBQUEsUUFBZ1BKLEdBQUcsR0FBQ29kLElBQUksQ0FBQzdjLE1BQUwsR0FBWTZHLEtBQVosSUFBbUI3RyxNQUFuQixHQUEwQndsQixJQUFJLEdBQUMzZSxLQUEvQixHQUFxQyxDQUFDZ1csSUFBSSxDQUFDN2MsTUFBTixHQUFhLENBQWIsSUFBZ0I2RyxLQUFLLEdBQUMsQ0FBdEIsSUFBeUI3RyxNQUF6QixHQUFnQzZjLElBQUksQ0FBQzdjLE1BQXJDLEdBQTRDeWxCLElBQXJVO0FBQUEsUUFBMFUvbEIsTUFBTSxHQUFDbWQsSUFBSSxDQUFDN2MsTUFBTCxHQUFZNkcsS0FBWixJQUFtQjdHLE1BQW5CLEdBQTBCd2xCLElBQUksR0FBQzNlLEtBQS9CLEdBQXFDZ1csSUFBSSxDQUFDN2MsTUFBTCxHQUFZLENBQVosSUFBZTZHLEtBQUssR0FBQyxDQUFyQixJQUF3QjRlLElBQTlZO0FBQW1aLFdBQU07QUFBQ2htQixTQUFHLEVBQUNvSCxLQUFLLElBQUUsQ0FBUCxHQUFTLENBQVQsR0FBVy9ELElBQUksQ0FBQ29ILEtBQUwsQ0FBV3pLLEdBQVgsQ0FBaEI7QUFBZ0NDLFlBQU0sRUFBQ21ILEtBQUssSUFBRSxDQUFQLEdBQVMsQ0FBVCxHQUFXL0QsSUFBSSxDQUFDb0gsS0FBTCxDQUFXeEssTUFBWCxDQUFsRDtBQUFxRUcsVUFBSSxFQUFDZ2QsSUFBSSxDQUFDOWMsS0FBTCxHQUFXOEcsS0FBWCxHQUFpQjlHLEtBQWpCLEdBQXVCLENBQXZCLEdBQXlCK0MsSUFBSSxDQUFDb0gsS0FBTCxDQUFXckssSUFBWCxDQUFuRztBQUFvSEMsV0FBSyxFQUFDK2MsSUFBSSxDQUFDOWMsS0FBTCxHQUFXOEcsS0FBWCxHQUFpQjlHLEtBQWpCLEdBQXVCLENBQXZCLEdBQXlCK0MsSUFBSSxDQUFDb0gsS0FBTCxDQUFXLENBQUNySyxJQUFaO0FBQW5KLEtBQU47QUFBNkssR0FEclo7O0FBQ3NacUgsT0FBSyxDQUFDK1IsUUFBTixHQUFlLFVBQVN5TSxNQUFULEVBQWdCO0FBQUMsUUFBSTFRLE9BQU8sR0FBQyxLQUFLQSxPQUFqQjtBQUFBLFFBQXlCekYsTUFBTSxHQUFDLEtBQUtBLE1BQXJDO0FBQUEsUUFBNENyRyxJQUFJLEdBQUMsS0FBS3VHLE1BQUwsQ0FBWXZHLElBQTdEO0FBQUEsUUFBa0V5YyxHQUFHLEdBQUMsS0FBS3hKLEtBQUwsRUFBdEU7QUFBQSxRQUFtRmpXLEtBQUssR0FBQ3BELElBQUksQ0FBQ29ILEtBQUwsQ0FBVyxDQUFDeWIsR0FBRCxHQUFLLEtBQUs1YSxNQUFMLENBQVloQyxRQUFaLENBQXFCckMsQ0FBMUIsR0FBNEI2SSxNQUFNLENBQUN4UCxLQUE5QyxDQUF6RjtBQUFBLFFBQThJM0csTUFBTSxHQUFDNGIsT0FBTyxDQUFDcUYsYUFBUixHQUFzQixDQUEzSztBQUFBLFFBQTZLdUwsTUFBTSxHQUFDLENBQXBMO0FBQUEsUUFBc0xDLE1BQU0sR0FBQyxFQUE3TDtBQUFBLFFBQWdNM3NCLENBQWhNOztBQUFrTSxRQUFHLENBQUN3c0IsTUFBRCxJQUFTLENBQUMxUSxPQUFPLENBQUMwRCxNQUFyQixFQUE0QjtBQUFDZ04sWUFBTSxHQUFDLENBQVA7O0FBQVMsV0FBSXhzQixDQUFDLEdBQUMsQ0FBTixFQUFRQSxDQUFDLEdBQUNxVyxNQUFNLENBQUNuVyxNQUFqQixFQUF3QkYsQ0FBQyxFQUF6QixFQUE0QjtBQUFDLFlBQUdxVyxNQUFNLENBQUNyVyxDQUFELENBQU4sQ0FBVXNvQixVQUFWLENBQXFCcUMsaUJBQXhCLEVBQTBDO0FBQUM2QixnQkFBTTtBQUFJO0FBQUM7O0FBQzU1QkEsWUFBTSxJQUFFLENBQVI7QUFBVTFRLGFBQU8sQ0FBQzBELE1BQVIsR0FBZSxLQUFLaFYsT0FBTCxDQUFheUgsT0FBNUI7QUFBcUM7O0FBQy9DLFlBQU91YSxNQUFQO0FBQWUsV0FBSyxDQUFMO0FBQU8sV0FBSyxDQUFMO0FBQU9HLGNBQU0sR0FBQyxDQUFDLENBQUQsQ0FBUDtBQUFXOztBQUFNLFdBQUssQ0FBTDtBQUFPLFdBQUssQ0FBTDtBQUFPQSxjQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUYsRUFBSSxDQUFKLEVBQU0sQ0FBTixDQUFQO0FBQWdCOztBQUFNO0FBQVFILGNBQU0sR0FBQyxDQUFQO0FBQVNHLGNBQU0sR0FBQyxDQUFDLENBQUMsQ0FBRixFQUFJLENBQUMsQ0FBTCxFQUFPLENBQVAsRUFBUyxDQUFULEVBQVcsQ0FBWCxDQUFQO0FBQW5HOztBQUNBLFFBQUcsQ0FBQzNjLElBQUosRUFBUztBQUFDLFVBQUk0YyxRQUFRLEdBQUM1ZixLQUFLLEdBQUMyZixNQUFNLENBQUNILE1BQU0sR0FBQyxDQUFSLENBQXpCO0FBQUEsVUFBb0NLLFFBQVEsR0FBQzdmLEtBQUssR0FBQzJmLE1BQU0sQ0FBQyxDQUFELENBQXpEO0FBQTZERCxZQUFNLEdBQUVHLFFBQVEsR0FBQyxDQUFWLEdBQWEsQ0FBQ0EsUUFBZCxHQUF1QixDQUE5QjtBQUFnQ0gsWUFBTSxHQUFFRSxRQUFRLEdBQUMxc0IsTUFBVixHQUFrQkEsTUFBTSxHQUFDMHNCLFFBQXpCLEdBQWtDRixNQUF6QztBQUFpRDs7QUFDeEpDLFVBQU0sR0FBQ0EsTUFBTSxDQUFDRyxHQUFQLENBQVcsVUFBUzlzQixDQUFULEVBQVc7QUFBQyxhQUFPbUssS0FBSyxDQUFDNEMsTUFBTixDQUFhK08sT0FBTyxDQUFDNWIsTUFBckIsRUFBNEJGLENBQUMsR0FBQzBzQixNQUFGLEdBQVMxZixLQUFyQyxDQUFQO0FBQW9ELEtBQTNFLENBQVA7O0FBQW9GLFNBQUloTixDQUFDLEdBQUMsQ0FBTixFQUFRQSxDQUFDLEdBQUNxVyxNQUFNLENBQUNuVyxNQUFqQixFQUF3QkYsQ0FBQyxFQUF6QixFQUE0QjtBQUFDLFVBQUk4UixLQUFLLEdBQUN1RSxNQUFNLENBQUNyVyxDQUFELENBQWhCO0FBQUEsVUFBb0JvcUIsV0FBVyxHQUFDamdCLEtBQUssQ0FBQzRDLE1BQU4sQ0FBYStPLE9BQU8sQ0FBQzViLE1BQXJCLEVBQTRCNFIsS0FBSyxDQUFDOUUsS0FBbEMsQ0FBaEM7O0FBQXlFLFVBQUcsQ0FBQ2dELElBQUQsSUFBTzhCLEtBQUssQ0FBQzlFLEtBQU4sR0FBWW9kLFdBQXRCLEVBQWtDO0FBQUM7QUFBVTs7QUFDdk8sVUFBR3VDLE1BQU0sQ0FBQ3ZlLE9BQVAsQ0FBZWdjLFdBQWYsSUFBNEIsQ0FBQyxDQUE3QixJQUFnQ3RZLEtBQUssQ0FBQ3NKLEtBQU4sS0FBY2dQLFdBQWpELEVBQTZEO0FBQUMsYUFBS25HLFdBQUwsQ0FBaUJuUyxLQUFqQjtBQUF3QixhQUFLcVksV0FBTCxDQUFpQkMsV0FBakIsRUFBNkJwcUIsQ0FBN0I7QUFBaUM7QUFBQztBQUFDLEdBTHdjOztBQUt2Y2dPLE9BQUssQ0FBQ2dTLGVBQU4sR0FBc0IsWUFBVTtBQUFDLFFBQUkzSixNQUFNLEdBQUMsS0FBS0EsTUFBaEI7QUFBQSxRQUF1QnlGLE9BQU8sR0FBQyxLQUFLQSxPQUFwQztBQUE0Q0EsV0FBTyxDQUFDOU8sS0FBUixHQUFjN0MsS0FBSyxDQUFDNEMsTUFBTixDQUFhK08sT0FBTyxDQUFDNWIsTUFBckIsRUFBNEJtVyxNQUFNLENBQUNySixLQUFuQyxDQUFkO0FBQXdELFNBQUt5VyxVQUFMO0FBQWtCLFNBQUtHLGNBQUw7QUFBc0IsU0FBS21KLGFBQUw7QUFBcUIsU0FBS0MsYUFBTDtBQUFxQixTQUFLQyxZQUFMO0FBQW9COWlCLFNBQUssQ0FBQ21DLGFBQU4sQ0FBb0IsSUFBcEIsRUFBeUIsV0FBekIsRUFBcUMsYUFBckMsRUFBbUQsS0FBS29YLFFBQUwsRUFBbkQ7QUFBcUUsR0FBaFQ7O0FBQWlUMVYsT0FBSyxDQUFDNlIsU0FBTixHQUFnQixZQUFVO0FBQUMsUUFBSS9ELE9BQU8sR0FBQyxLQUFLQSxPQUFqQjtBQUFBLFFBQXlCL0osTUFBTSxHQUFDLEtBQUtBLE1BQXJDO0FBQUEsUUFBNEM3UixNQUFNLEdBQUM0YixPQUFPLENBQUNxRixhQUEzRDtBQUFBLFFBQXlFbk8sVUFBVSxHQUFDLEtBQUt4SSxPQUFMLENBQWF3SSxVQUFqRztBQUFBLFFBQTRHNEgsWUFBWSxHQUFDLEtBQUtsRixHQUFMLENBQVNrRixZQUFsSTs7QUFBK0ksUUFBRyxDQUFDNUgsVUFBRCxJQUFhOVMsTUFBTSxHQUFDLENBQXZCLEVBQXlCO0FBQUMsV0FBS3dWLEdBQUwsQ0FBUzdDLE9BQVQsQ0FBaUJ6SCxLQUFqQixDQUF1QjVFLE1BQXZCLEdBQThCLENBQTlCO0FBQWdDb1Usa0JBQVksQ0FBQ3hQLEtBQWIsQ0FBbUIwZixVQUFuQixHQUE4QixRQUE5QjtBQUF1Q2xRLGtCQUFZLENBQUN4UCxLQUFiLENBQW1CdEUsTUFBbkIsR0FBMEIsQ0FBMUI7QUFBNEJpTCxZQUFNLENBQUNqTCxNQUFQLEdBQWNpTCxNQUFNLENBQUNvQixNQUFQLEdBQWMsQ0FBNUI7QUFBOEI7QUFBUTs7QUFDeHZCLFFBQUkrWixLQUFLLEdBQUMsS0FBSzFpQixPQUFMLENBQWEwSSxjQUF2QjtBQUFBLFFBQXNDaWEsT0FBTyxHQUFDdmpCLElBQUksQ0FBQ0MsR0FBTCxDQUFTbEwsTUFBTSxDQUFDeXVCLFVBQWhCLEVBQTJCeGpCLElBQUksQ0FBQ0MsR0FBTCxDQUFTa1QsTUFBTSxDQUFDbFcsS0FBaEIsRUFBc0JrVyxNQUFNLENBQUNqVyxNQUE3QixDQUEzQixDQUE5QztBQUFBLFFBQStHdW1CLE9BQU8sR0FBQyxDQUF2SDtBQUFBLFFBQXlIcnRCLENBQXpIO0FBQTJILFFBQUlzdEIsTUFBTSxHQUFDdmUsTUFBTSxDQUFDbUMsSUFBUCxDQUFZZ2MsS0FBWixFQUFtQkssSUFBbkIsQ0FBd0IsVUFBU250QixDQUFULEVBQVdvdEIsQ0FBWCxFQUFhO0FBQUMsYUFBT3B0QixDQUFDLEdBQUNvdEIsQ0FBVDtBQUFZLEtBQWxELENBQVg7O0FBQStELFNBQUl4dEIsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDc3RCLE1BQU0sQ0FBQ3B0QixNQUFqQixFQUF3QkYsQ0FBQyxFQUF6QixFQUE0QjtBQUFDLFVBQUkyakIsSUFBSSxHQUFDMkosTUFBTSxDQUFDdHRCLENBQUQsQ0FBZjtBQUFBLFVBQW1CNkcsS0FBSyxHQUFDN0csQ0FBQyxLQUFHc3RCLE1BQU0sQ0FBQ3B0QixNQUFQLEdBQWMsQ0FBbEIsR0FBb0JpdEIsT0FBcEIsR0FBNEJ2akIsSUFBSSxDQUFDZ1osR0FBTCxDQUFTdUssT0FBVCxFQUFpQnhKLElBQWpCLENBQXJEO0FBQUEsVUFBNEU2SSxNQUFNLEdBQUM1aUIsSUFBSSxDQUFDbWlCLElBQUwsQ0FBVWxsQixLQUFLLElBQUVxbUIsS0FBSyxDQUFDdkosSUFBRCxDQUFMLENBQVk5YyxLQUFaLEdBQWtCcW1CLEtBQUssQ0FBQ3ZKLElBQUQsQ0FBTCxDQUFZeFEsTUFBaEMsQ0FBTCxHQUE2QyxDQUF2RCxDQUFuRjs7QUFBNkksVUFBR3NhLFFBQVEsQ0FBQ2pCLE1BQUQsQ0FBUixJQUFrQkEsTUFBTSxHQUFDYSxPQUE1QixFQUFvQztBQUFDQSxlQUFPLEdBQUNiLE1BQVI7QUFBZ0I7O0FBQ3paLFVBQUc3SSxJQUFJLElBQUV3SixPQUFULEVBQWlCO0FBQUM7QUFBTztBQUFDOztBQUMxQixRQUFJdkMsUUFBUSxHQUFDbmYsUUFBUSxDQUFDMmMsc0JBQVQsRUFBYjtBQUErQ2xvQixVQUFNLEdBQUNBLE1BQU0sR0FBQyxFQUFQLEdBQVUwSixJQUFJLENBQUNnWixHQUFMLENBQVN5SyxPQUFULEVBQWlCbnRCLE1BQWpCLENBQVYsR0FBbUNBLE1BQTFDOztBQUFpRCxTQUFJRixDQUFDLEdBQUMsQ0FBTixFQUFRQSxDQUFDLEdBQUNFLE1BQVYsRUFBaUJGLENBQUMsRUFBbEIsRUFBcUI7QUFBQyxVQUFJK1gsS0FBSyxHQUFDNU4sS0FBSyxDQUFDVSxRQUFOLENBQWUsS0FBZixFQUFxQixLQUFLcUwsR0FBTCxHQUFTLFFBQTlCLENBQVY7QUFBa0QwVSxjQUFRLENBQUN2UixXQUFULENBQXFCdEIsS0FBckI7QUFBNkI7O0FBQ3JNLFNBQUtyQyxHQUFMLENBQVNtRixXQUFULENBQXFCeEIsV0FBckIsQ0FBaUN1UixRQUFqQztBQUEyQyxTQUFLdkgsaUJBQUw7QUFBMEIsR0FKc1c7O0FBSXJXclYsT0FBSyxDQUFDMGYsVUFBTixHQUFpQixVQUFTMXJCLEtBQVQsRUFBZTtBQUFDLFFBQUkrYixNQUFNLEdBQUMvYixLQUFLLENBQUMrYixNQUFqQjs7QUFBd0IsUUFBRyxDQUFDNVQsS0FBSyxDQUFDZ0QsUUFBTixDQUFlNFEsTUFBZixFQUFzQixLQUFLN0gsR0FBTCxHQUFTLFFBQS9CLENBQUosRUFBNkM7QUFBQzZILFlBQU0sR0FBQ0EsTUFBTSxDQUFDZ0IsVUFBZDtBQUEwQjs7QUFDdk0sUUFBRzJMLFFBQVEsQ0FBQzNNLE1BQU0sQ0FBQy9RLEtBQVIsRUFBYyxFQUFkLENBQVIsSUFBMkIsQ0FBOUIsRUFBZ0M7QUFBQyxXQUFLMGEsT0FBTCxDQUFhM0osTUFBTSxDQUFDL1EsS0FBcEI7QUFBNEI7QUFBQyxHQURROztBQUNQZ0IsT0FBSyxDQUFDMmYsU0FBTixHQUFnQixVQUFTNVYsS0FBVCxFQUFlL0ssS0FBZixFQUFxQjtBQUFDLFFBQUlvTyxLQUFLLEdBQUMsS0FBS1UsT0FBTCxDQUFhOU8sS0FBYixDQUFWO0FBQUEsUUFBOEJxTyxHQUE5Qjs7QUFBa0MsUUFBRyxDQUFDRCxLQUFLLENBQUNyRCxLQUFQLElBQWMsU0FBT3FELEtBQUssQ0FBQ3JELEtBQWIsTUFBcUIsUUFBdEMsRUFBK0M7QUFBQ3NELFNBQUcsR0FBQ0QsS0FBSyxDQUFDckQsS0FBVjtBQUFnQnFELFdBQUssQ0FBQ3JELEtBQU4sR0FBWTVOLEtBQUssQ0FBQ1UsUUFBTixDQUFlLEtBQWYsRUFBcUIsS0FBS3FMLEdBQUwsR0FBUyxXQUE5QixDQUFaO0FBQXVEa0YsV0FBSyxDQUFDckQsS0FBTixDQUFZM00sS0FBWixDQUFrQmdULGVBQWxCLEdBQWtDL0MsR0FBRyxJQUFFQSxHQUFHLENBQUNqTixPQUFKLENBQVksT0FBWixJQUFxQixDQUExQixHQUE0QixTQUFPaU4sR0FBUCxHQUFXLEdBQXZDLEdBQTJDLElBQTdFOztBQUFrRixVQUFHRCxLQUFLLENBQUNuWixJQUFOLEtBQWEsT0FBaEIsRUFBd0I7QUFBQ2tJLGFBQUssQ0FBQ2tELFFBQU4sQ0FBZStOLEtBQUssQ0FBQ3JELEtBQXJCLEVBQTJCLEtBQUs3QixHQUFMLEdBQVMsY0FBcEM7QUFBb0QvTCxhQUFLLENBQUNrRCxRQUFOLENBQWUrTixLQUFLLENBQUNyRCxLQUFyQixFQUEyQixLQUFLN0IsR0FBTCxHQUFTLGVBQXBDO0FBQXNEO0FBQUM7O0FBQ3BkLFFBQUlpQyxNQUFNLEdBQUNKLEtBQUssQ0FBQ3VRLFVBQU4sR0FBaUIsY0FBakIsR0FBZ0MsYUFBM0M7QUFBeUR2USxTQUFLLENBQUNJLE1BQUQsQ0FBTCxDQUFjaUQsS0FBSyxDQUFDckQsS0FBcEIsRUFBMEJBLEtBQUssQ0FBQ3VRLFVBQWhDO0FBQTRDdlEsU0FBSyxDQUFDcUQsS0FBTixHQUFZcE8sS0FBWjs7QUFBa0IsUUFBR3FPLEdBQUgsRUFBTztBQUFDLFVBQUlsQyxHQUFHLEdBQUMsSUFBSTFULEtBQUosRUFBUjs7QUFBb0IwVCxTQUFHLENBQUMyRSxNQUFKLEdBQVcsWUFBVTtBQUFDM1QsYUFBSyxDQUFDa0QsUUFBTixDQUFlK04sS0FBSyxDQUFDckQsS0FBckIsRUFBMkIsS0FBSzdCLEdBQUwsR0FBUyxlQUFwQztBQUFzRCxPQUFqRSxDQUFrRWhPLElBQWxFLENBQXVFLElBQXZFLENBQVg7O0FBQXdGaVIsU0FBRyxDQUFDa0MsR0FBSixHQUFRQSxHQUFSO0FBQWE7QUFBQyxHQUQxTDs7QUFDMkxyTixPQUFLLENBQUNpZixZQUFOLEdBQW1CLFlBQVU7QUFBQyxRQUFJblIsT0FBTyxHQUFDLEtBQUtBLE9BQWpCOztBQUF5QixRQUFHLENBQUMsS0FBS3RSLE9BQUwsQ0FBYXdJLFVBQWQsSUFBMEI4SSxPQUFPLENBQUNxRixhQUFSLEdBQXNCLENBQW5ELEVBQXFEO0FBQUM7QUFBUTs7QUFDL1csUUFBSXBQLE1BQU0sR0FBQyxLQUFLQSxNQUFoQjtBQUFBLFFBQXVCbEMsUUFBUSxHQUFDLEtBQUsrZCxnQkFBTCxDQUFzQjdiLE1BQXRCLENBQWhDO0FBQThEQSxVQUFNLENBQUNwQyxXQUFQOztBQUFxQixRQUFHRSxRQUFRLEtBQUdrQyxNQUFNLENBQUNsQyxRQUFQLENBQWdCckMsQ0FBOUIsRUFBZ0M7QUFBQyxXQUFLcWdCLFdBQUwsQ0FBaUI5YixNQUFqQjtBQUF5QjtBQUFROztBQUNySixRQUFHbkksSUFBSSxDQUFDaVosR0FBTCxDQUFTaFQsUUFBUSxHQUFDa0MsTUFBTSxDQUFDbEMsUUFBUCxDQUFnQnJDLENBQWxDLElBQXFDLEtBQUd1RSxNQUFNLENBQUM0UixJQUFsRCxFQUF1RDtBQUFDLFdBQUtqTyxHQUFMLENBQVNrRixZQUFULENBQXNCeFAsS0FBdEIsQ0FBNEIwZixVQUE1QixHQUF1QyxRQUF2QztBQUFnRC9ZLFlBQU0sQ0FBQ2xDLFFBQVAsQ0FBZ0JyQyxDQUFoQixHQUFrQnFDLFFBQWxCO0FBQTJCMUYsV0FBSyxDQUFDb0QsU0FBTixDQUFnQixLQUFLbUksR0FBTCxDQUFTbUYsV0FBekIsRUFBcUNoTCxRQUFyQyxFQUE4QyxDQUE5QztBQUFpRCxXQUFLaWUsWUFBTCxDQUFrQi9iLE1BQWxCO0FBQTBCLFdBQUsyRCxHQUFMLENBQVNrRixZQUFULENBQXNCeFAsS0FBdEIsQ0FBNEIwZixVQUE1QixHQUF1QyxFQUF2QztBQUEyQyxLQUF6UCxNQUE2UDtBQUFDL1ksWUFBTSxDQUFDekMsWUFBUDtBQUFzQnlDLFlBQU0sQ0FBQzNDLFdBQVA7QUFBcUIyQyxZQUFNLENBQUMxQyxTQUFQLENBQWlCO0FBQUM3QixTQUFDLEVBQUNxQztBQUFILE9BQWpCO0FBQWdDO0FBQUMsR0FGaEY7O0FBRWlGN0IsT0FBSyxDQUFDZ2YsYUFBTixHQUFvQixZQUFVO0FBQUMsUUFBRyxLQUFLeGlCLE9BQUwsQ0FBYXFJLE9BQWhCLEVBQXdCO0FBQUMsVUFBSXVJLEtBQUssR0FBQyxLQUFLc0ksUUFBTCxFQUFWO0FBQUEsVUFBMEIrRyxPQUFPLEdBQUNyUCxLQUFLLENBQUN2SSxPQUFOLEdBQWN1SSxLQUFLLENBQUN2SSxPQUFwQixHQUE0QixFQUE5RDtBQUFBLFVBQWlFQSxPQUFPLEdBQUMsS0FBSzZDLEdBQUwsQ0FBU2lGLFlBQWxGOztBQUErRixVQUFHOUgsT0FBTyxDQUFDOEwsU0FBUixLQUFvQjhMLE9BQXZCLEVBQStCO0FBQUM1WCxlQUFPLENBQUM4TCxTQUFSLEdBQWtCOEwsT0FBbEI7QUFBMkI7QUFBQztBQUFDLEdBQXBOOztBQUFxTnpjLE9BQUssQ0FBQytlLGFBQU4sR0FBb0IsWUFBVTtBQUFDLFFBQUcsS0FBS3ZpQixPQUFMLENBQWFvSSxjQUFoQixFQUErQjtBQUFDLFVBQUlrSixPQUFPLEdBQUMsS0FBS0EsT0FBakI7QUFBQSxVQUF5QjViLE1BQU0sR0FBQzRiLE9BQU8sQ0FBQ3FGLGFBQXhDO0FBQUEsVUFBc0RuVSxLQUFLLEdBQUM3QyxLQUFLLENBQUM0QyxNQUFOLENBQWE3TSxNQUFiLEVBQW9CNGIsT0FBTyxDQUFDOU8sS0FBNUIsQ0FBNUQ7QUFBQSxVQUErRnpDLE9BQU8sR0FBQyxLQUFLQyxPQUFMLENBQWFvSSxjQUFwSDtBQUFBLFVBQW1JNlgsT0FBTyxHQUFDbGdCLE9BQU8sQ0FBQ3VCLE9BQVIsQ0FBZ0IsU0FBaEIsRUFBMEJrQixLQUFLLEdBQUMsQ0FBaEMsRUFBbUNsQixPQUFuQyxDQUEyQyxTQUEzQyxFQUFxRDVMLE1BQXJELENBQTNJO0FBQUEsVUFBd01xYSxPQUFPLEdBQUMsS0FBSzdFLEdBQUwsQ0FBUzZFLE9BQXpOOztBQUFpTyxVQUFHQSxPQUFPLENBQUNILFdBQVIsS0FBc0JxUSxPQUF6QixFQUFpQztBQUFDbFEsZUFBTyxDQUFDSCxXQUFSLEdBQW9CcVEsT0FBcEI7QUFBNkI7QUFBQztBQUFDLEdBQWpXOztBQUFrV3pjLE9BQUssQ0FBQzBSLFVBQU4sR0FBaUIsWUFBVTtBQUFDLFFBQUkxUCxJQUFJLEdBQUMsS0FBS3hGLE9BQUwsQ0FBYXdGLElBQXRCO0FBQUEsUUFBMkI4TCxPQUFPLEdBQUMsS0FBS0EsT0FBeEM7QUFBQSxRQUFnRDViLE1BQU0sR0FBQzRiLE9BQU8sQ0FBQzViLE1BQS9EOztBQUFzRSxRQUFHLENBQUM0YixPQUFPLENBQUNxRixhQUFaLEVBQTBCO0FBQUNyRixhQUFPLENBQUNxRixhQUFSLEdBQXNCamhCLE1BQXRCO0FBQThCOztBQUM3aEMsU0FBS3FXLE1BQUwsQ0FBWXZHLElBQVosR0FBaUJBLElBQUksSUFBRUEsSUFBSSxJQUFFOVAsTUFBWixHQUFtQixJQUFuQixHQUF3QixLQUF6Qzs7QUFBK0MsUUFBRyxLQUFLcVcsTUFBTCxDQUFZdkcsSUFBWixJQUFrQjlQLE1BQU0sR0FBQyxLQUFLbVcsTUFBTCxDQUFZblcsTUFBeEMsRUFBK0M7QUFBQyxVQUFJNnRCLEdBQUcsR0FBQ25rQixJQUFJLENBQUNtaUIsSUFBTCxDQUFVLEtBQUsxVixNQUFMLENBQVluVyxNQUFaLEdBQW1CQSxNQUE3QixJQUFxQ0EsTUFBckMsR0FBNENBLE1BQXBEOztBQUEyRCxXQUFJLElBQUlGLENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBQyt0QixHQUFkLEVBQWtCL3RCLENBQUMsRUFBbkIsRUFBc0I7QUFBQyxZQUFJZ04sS0FBSyxHQUFDOU0sTUFBTSxHQUFDRixDQUFqQjtBQUFtQjhiLGVBQU8sQ0FBQzlPLEtBQUQsQ0FBUCxHQUFlN0MsS0FBSyxDQUFDa0IsV0FBTixDQUFrQnlRLE9BQU8sQ0FBQzNSLEtBQUssQ0FBQzRDLE1BQU4sQ0FBYTdNLE1BQWIsRUFBb0JGLENBQXBCLENBQUQsQ0FBekIsQ0FBZjtBQUFrRThiLGVBQU8sQ0FBQzlPLEtBQUQsQ0FBUCxDQUFlQSxLQUFmLEdBQXFCQSxLQUFyQjtBQUE0QjtBQUFDO0FBQUMsR0FEOGxCOztBQUM3bEJnQixPQUFLLENBQUM0UixTQUFOLEdBQWdCLFlBQVU7QUFBQyxRQUFJL04sTUFBTSxHQUFDLEtBQUtBLE1BQWhCO0FBQUEsUUFBdUJ3RSxNQUFNLEdBQUMsS0FBS0EsTUFBbkM7QUFBMEMsU0FBSzJYLFFBQUwsQ0FBY25jLE1BQWQsRUFBcUJ3RSxNQUFyQjtBQUE2QixTQUFLNFgsaUJBQUwsQ0FBdUJwYyxNQUF2QixFQUE4QndFLE1BQTlCO0FBQXNDLFNBQUs2WCxrQkFBTCxDQUF3QjdYLE1BQXhCO0FBQWdDLFNBQUtYLEdBQUwsQ0FBUzRELE9BQVQsQ0FBaUJsTyxLQUFqQixDQUF1QitpQixPQUF2QixHQUErQixDQUEvQjtBQUFrQyxHQUExTTs7QUFBMk1uZ0IsT0FBSyxDQUFDZ2dCLFFBQU4sR0FBZSxVQUFTbmMsTUFBVCxFQUFnQndFLE1BQWhCLEVBQXVCO0FBQUN4RSxVQUFNLENBQUNoTCxLQUFQLEdBQWE0RSxRQUFRLENBQUNzTyxJQUFULENBQWN1TSxXQUEzQjtBQUF1Q3pVLFVBQU0sQ0FBQy9LLE1BQVAsR0FBY25JLE1BQU0sQ0FBQ3l2QixXQUFyQjtBQUFpQy9YLFVBQU0sQ0FBQ3hQLEtBQVAsR0FBYWdMLE1BQU0sQ0FBQ2hMLEtBQVAsR0FBYStDLElBQUksQ0FBQ29ILEtBQUwsQ0FBV2EsTUFBTSxDQUFDaEwsS0FBUCxHQUFhLEtBQUsyRCxPQUFMLENBQWE0SSxPQUFyQyxDQUExQjtBQUF5RSxHQUF4TDs7QUFBeUxwRixPQUFLLENBQUNrZ0Isa0JBQU4sR0FBeUIsVUFBUzdYLE1BQVQsRUFBZ0I7QUFBQyxTQUFJLElBQUlyVyxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNxVyxNQUFNLENBQUNuVyxNQUFyQixFQUE0QkYsQ0FBQyxFQUE3QixFQUFnQztBQUFDcVcsWUFBTSxDQUFDclcsQ0FBRCxDQUFOLENBQVU2UCxRQUFWLEdBQW1CLElBQW5CO0FBQXdCLFdBQUt3ZSxlQUFMLENBQXFCcnVCLENBQXJCO0FBQXlCOztBQUNyeUIsU0FBS3N1QixXQUFMO0FBQW9CLEdBRHFwQjs7QUFDcHBCdGdCLE9BQUssQ0FBQ3FWLGlCQUFOLEdBQXdCLFlBQVU7QUFBQyxRQUFHLENBQUMsS0FBSzdZLE9BQUwsQ0FBYXdJLFVBQWQsSUFBMEIsS0FBSzhJLE9BQUwsQ0FBYXFGLGFBQWIsR0FBMkIsQ0FBeEQsRUFBMEQ7QUFBQztBQUFROztBQUMzSCxRQUFJcFAsTUFBTSxHQUFDLEtBQUtBLE1BQWhCO0FBQUEsUUFBdUJGLE1BQU0sR0FBQyxLQUFLQSxNQUFuQztBQUFBLFFBQTBDOEQsTUFBTSxHQUFDLEtBQUtELEdBQUwsQ0FBU2tGLFlBQTFEO0FBQUEsUUFBdUUyVCxLQUFLLEdBQUMsS0FBSzdZLEdBQUwsQ0FBU21GLFdBQXRGO0FBQUEsUUFBa0dxUyxLQUFLLEdBQUMsS0FBSzFpQixPQUFMLENBQWEwSSxjQUFySDtBQUFBLFFBQW9JdVosR0FBRyxHQUFDLEtBQUtqaUIsT0FBTCxDQUFhd0gsV0FBcko7QUFBQSxRQUFpS3NiLE1BQU0sR0FBQ3ZlLE1BQU0sQ0FBQ21DLElBQVAsQ0FBWWdjLEtBQVosRUFBbUJLLElBQW5CLENBQXdCLFVBQVNudEIsQ0FBVCxFQUFXb3RCLENBQVgsRUFBYTtBQUFDLGFBQU9BLENBQUMsR0FBQ3B0QixDQUFUO0FBQVksS0FBbEQsQ0FBeEs7QUFBQSxRQUE0TnlHLEtBQUssR0FBQytDLElBQUksQ0FBQ0MsR0FBTCxDQUFTeEgsS0FBVCxDQUFlLElBQWYsRUFBb0JpckIsTUFBcEIsQ0FBbE87QUFBQSxRQUE4UDFmLE9BQU8sR0FBQ2pQLE1BQU0sQ0FBQ3l1QixVQUE3UTtBQUFBLFFBQXdSekosSUFBeFI7O0FBQTZSLFNBQUksSUFBSTNqQixDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNzdEIsTUFBTSxDQUFDcHRCLE1BQXJCLEVBQTRCRixDQUFDLEVBQTdCLEVBQWdDO0FBQUMyakIsVUFBSSxHQUFDcUksTUFBTSxDQUFDc0IsTUFBTSxDQUFDdHRCLENBQUQsQ0FBUCxDQUFYOztBQUF1QixVQUFHNE4sT0FBTyxJQUFFK1YsSUFBWixFQUFpQjtBQUFDOWMsYUFBSyxHQUFDOGMsSUFBTjtBQUFZO0FBQUM7O0FBQ3BYNVIsVUFBTSxDQUFDbEwsS0FBUCxHQUFhbWxCLE1BQU0sQ0FBQ2tCLEtBQUssQ0FBQ3JtQixLQUFELENBQUwsQ0FBYUEsS0FBZCxDQUFuQjtBQUF3Q2tMLFVBQU0sQ0FBQ29CLE1BQVAsR0FBYzZZLE1BQU0sQ0FBQ2tCLEtBQUssQ0FBQ3JtQixLQUFELENBQUwsQ0FBYXNNLE1BQWQsQ0FBcEI7QUFBMENwQixVQUFNLENBQUNqTCxNQUFQLEdBQWNrbEIsTUFBTSxDQUFDa0IsS0FBSyxDQUFDcm1CLEtBQUQsQ0FBTCxDQUFhQyxNQUFkLENBQXBCO0FBQTBDaUwsVUFBTSxDQUFDNFIsSUFBUCxHQUFZNVIsTUFBTSxDQUFDbEwsS0FBUCxHQUFha0wsTUFBTSxDQUFDb0IsTUFBaEM7QUFBdUNwQixVQUFNLENBQUM3UixNQUFQLEdBQWMsS0FBSzRiLE9BQUwsQ0FBYXFGLGFBQTNCO0FBQXlDLFFBQUlxTixVQUFVLEdBQUN6YyxNQUFNLENBQUM3UixNQUFQLEdBQWM2UixNQUFNLENBQUM0UixJQUFwQztBQUF5QzVSLFVBQU0sQ0FBQzBjLEtBQVAsR0FBYTtBQUFDOW5CLFVBQUksRUFBQyxDQUFOO0FBQVFDLFdBQUssRUFBQzRuQixVQUFVLEdBQUMzYyxNQUFNLENBQUNoTCxLQUFsQixHQUF3QmdMLE1BQU0sQ0FBQ2hMLEtBQVAsR0FBYTJuQixVQUFyQyxHQUFnRDtBQUE5RCxLQUFiOztBQUE4RSxRQUFHL0IsR0FBSCxFQUFPO0FBQUMxYSxZQUFNLENBQUMwYyxLQUFQLENBQWE3bkIsS0FBYixHQUFtQjRuQixVQUFVLEdBQUMzYyxNQUFNLENBQUNoTCxLQUFsQixHQUF3QmdMLE1BQU0sQ0FBQ2hMLEtBQVAsR0FBYWtMLE1BQU0sQ0FBQzRSLElBQTVDLEdBQWlENkssVUFBVSxHQUFDemMsTUFBTSxDQUFDNFIsSUFBdEY7QUFBMkY1UixZQUFNLENBQUMwYyxLQUFQLENBQWE5bkIsSUFBYixHQUFrQjZuQixVQUFVLEdBQUN6YyxNQUFNLENBQUM0UixJQUFwQztBQUEwQzs7QUFDaGQsUUFBRyxLQUFLblosT0FBTCxDQUFheUksYUFBYixLQUE2QixVQUFoQyxFQUEyQztBQUFDbEIsWUFBTSxDQUFDMGMsS0FBUCxHQUFhO0FBQUM5bkIsWUFBSSxFQUFDNm5CLFVBQVUsR0FBQzNjLE1BQU0sQ0FBQ2hMLEtBQWxCLEdBQXdCK0MsSUFBSSxDQUFDOGtCLEtBQUwsQ0FBVzdjLE1BQU0sQ0FBQ2hMLEtBQVAsR0FBYSxHQUFiLEdBQWlCa0wsTUFBTSxDQUFDNFIsSUFBUCxHQUFZLEdBQXhDLENBQXhCLEdBQXFFL1osSUFBSSxDQUFDOGtCLEtBQUwsQ0FBV0YsVUFBVSxHQUFDLEdBQVgsR0FBZXpjLE1BQU0sQ0FBQzRSLElBQVAsR0FBWSxHQUF0QyxDQUEzRTtBQUFzSC9jLGFBQUssRUFBQzRuQixVQUFVLEdBQUMzYyxNQUFNLENBQUNoTCxLQUFsQixHQUF3QitDLElBQUksQ0FBQ21pQixJQUFMLENBQVVsYSxNQUFNLENBQUNoTCxLQUFQLEdBQWEsR0FBYixHQUFpQjJuQixVQUFqQixHQUE0QnpjLE1BQU0sQ0FBQzRSLElBQVAsR0FBWSxHQUFsRCxDQUF4QixHQUErRSxDQUFDL1osSUFBSSxDQUFDbWlCLElBQUwsQ0FBVXlDLFVBQVUsR0FBQyxHQUFYLEdBQWV6YyxNQUFNLENBQUM0UixJQUFQLEdBQVksR0FBckM7QUFBNU0sT0FBYjs7QUFBb1EsVUFBRzhJLEdBQUgsRUFBTztBQUFDMWEsY0FBTSxDQUFDMGMsS0FBUCxDQUFhN25CLEtBQWIsR0FBbUJtTCxNQUFNLENBQUMwYyxLQUFQLENBQWE5bkIsSUFBaEM7QUFBcUNvTCxjQUFNLENBQUMwYyxLQUFQLENBQWE5bkIsSUFBYixHQUFrQm9MLE1BQU0sQ0FBQzBjLEtBQVAsQ0FBYTluQixJQUFiLEdBQWtCNm5CLFVBQWxCLEdBQTZCemMsTUFBTSxDQUFDNFIsSUFBdEQ7QUFBNEQ7QUFBQzs7QUFDMVo1UixVQUFNLENBQUNqRCxZQUFQO0FBQXNCLFFBQUllLFFBQVEsR0FBQyxLQUFLK2QsZ0JBQUwsQ0FBc0I3YixNQUF0QixDQUFiO0FBQTJDQSxVQUFNLENBQUNsQyxRQUFQLENBQWdCckMsQ0FBaEIsR0FBa0JxQyxRQUFsQjtBQUEyQjFGLFNBQUssQ0FBQ29ELFNBQU4sQ0FBZ0JnaEIsS0FBaEIsRUFBc0IxZSxRQUF0QixFQUErQixDQUEvQjtBQUFrQyxRQUFJOGUsU0FBUyxHQUFDLEtBQUszSyxjQUFMLEVBQWQ7QUFBb0NyTyxVQUFNLENBQUN2SyxLQUFQLENBQWEwZixVQUFiLEdBQXdCNkQsU0FBUyxHQUFDLEVBQUQsR0FBSSxRQUFyQztBQUE4Q2haLFVBQU0sQ0FBQ3ZLLEtBQVAsQ0FBYXRFLE1BQWIsR0FBb0I2bkIsU0FBUyxHQUFDQSxTQUFTLEdBQUMsSUFBWCxHQUFnQixFQUE3QztBQUFnREosU0FBSyxDQUFDbmpCLEtBQU4sQ0FBWXRFLE1BQVosR0FBbUI2bkIsU0FBUyxHQUFDNWMsTUFBTSxDQUFDakwsTUFBUCxHQUFjOEMsSUFBSSxDQUFDZ1osR0FBTCxDQUFTLEVBQVQsRUFBWTdRLE1BQU0sQ0FBQ29CLE1BQW5CLENBQWQsR0FBeUMsSUFBMUMsR0FBK0MsRUFBM0U7QUFBOEVvYixTQUFLLENBQUNuakIsS0FBTixDQUFZdkUsS0FBWixHQUFrQmtMLE1BQU0sQ0FBQzdSLE1BQVAsR0FBYzZSLE1BQU0sQ0FBQzRSLElBQXJCLEdBQTBCLElBQTVDO0FBQWlENEssU0FBSyxDQUFDbmpCLEtBQU4sQ0FBWXhFLEtBQVosR0FBa0I0bkIsVUFBVSxHQUFDM2MsTUFBTSxDQUFDaEwsS0FBbEIsSUFBeUI0bEIsR0FBekIsR0FBNkIsTUFBN0IsR0FBb0MsRUFBdEQ7QUFBMEQsR0FKcGE7O0FBSXFhemUsT0FBSyxDQUFDNGYsZ0JBQU4sR0FBdUIsVUFBUzdiLE1BQVQsRUFBZ0I7QUFBQyxRQUFJRixNQUFNLEdBQUMsS0FBS0EsTUFBaEI7QUFBQSxRQUF1QmlLLE9BQU8sR0FBQyxLQUFLQSxPQUFwQztBQUFBLFFBQTRDOFMsUUFBUSxHQUFDLEtBQUtwa0IsT0FBTCxDQUFheUksYUFBbEU7QUFBQSxRQUFnRndaLEdBQUcsR0FBQyxLQUFLeEosS0FBTCxFQUFwRjtBQUFBLFFBQWlHdGMsSUFBSSxHQUFDOGxCLEdBQUcsR0FBQyxDQUFKLEdBQU0sT0FBTixHQUFjLE1BQXBIO0FBQUEsUUFBMkh6ZixLQUFLLEdBQUM3QyxLQUFLLENBQUM0QyxNQUFOLENBQWErTyxPQUFPLENBQUNxRixhQUFyQixFQUFtQ3JGLE9BQU8sQ0FBQzlPLEtBQTNDLENBQWpJO0FBQUEsUUFBbUw2aEIsUUFBUSxHQUFDaGQsTUFBTSxDQUFDaEwsS0FBUCxHQUFhLEdBQWIsR0FBaUJrTCxNQUFNLENBQUM0UixJQUFQLEdBQVksR0FBek47QUFBQSxRQUE2TjlULFFBQVEsR0FBQ2tDLE1BQU0sQ0FBQzBjLEtBQVAsQ0FBYTluQixJQUFiLElBQW1CcUcsS0FBSyxHQUFDK0UsTUFBTSxDQUFDNFIsSUFBYixHQUFrQjhJLEdBQTNRO0FBQStRNWMsWUFBUSxHQUFDLENBQUNrQyxNQUFNLENBQUMwYyxLQUFQLENBQWE5bkIsSUFBYixDQUFELEdBQW9Ca0osUUFBUSxHQUFDZ2YsUUFBN0IsR0FBc0NoZixRQUFRLElBQUU0YyxHQUFHLEdBQUMsQ0FBSixJQUFPbUMsUUFBUSxLQUFHLFVBQWxCLEdBQTZCLENBQUNDLFFBQTlCLEdBQXVDLENBQXpDLENBQXZEO0FBQW1HLFdBQU9qbEIsSUFBSSxDQUFDQyxHQUFMLENBQVNrSSxNQUFNLENBQUMwYyxLQUFQLENBQWE3bkIsS0FBdEIsRUFBNEJnRCxJQUFJLENBQUNnWixHQUFMLENBQVM3USxNQUFNLENBQUMwYyxLQUFQLENBQWE5bkIsSUFBdEIsRUFBMkJrSixRQUEzQixDQUE1QixDQUFQO0FBQTBFLEdBQXBlOztBQUFxZTdCLE9BQUssQ0FBQ3FnQixlQUFOLEdBQXNCLFVBQVNyaEIsS0FBVCxFQUFlO0FBQUMsUUFBSTBWLElBQUksR0FBQyxLQUFLcE0sS0FBTCxDQUFXdEosS0FBWCxDQUFUO0FBQTJCMFYsUUFBSSxDQUFDNVQsWUFBTDtBQUFvQjNFLFNBQUssQ0FBQ29ELFNBQU4sQ0FBZ0IsS0FBSzhJLE1BQUwsQ0FBWXJKLEtBQVosRUFBbUIwTSxRQUFuQixDQUE0QixDQUE1QixDQUFoQixFQUErQyxDQUEvQyxFQUFpRCxDQUFqRCxFQUFtRCxDQUFuRDtBQUF1RCxHQUE1STs7QUFBNkkxTCxPQUFLLENBQUNpZ0IsaUJBQU4sR0FBd0IsVUFBU3BjLE1BQVQsRUFBZ0J3RSxNQUFoQixFQUF1QjtBQUFDLFFBQUlvVyxHQUFHLEdBQUMsS0FBS2ppQixPQUFMLENBQWF3SCxXQUFyQjtBQUFBLFFBQWlDOGMsSUFBSSxHQUFDLENBQUN6WSxNQUFNLENBQUNySixLQUFSLEdBQWNxSixNQUFNLENBQUN4UCxLQUEzRDtBQUFpRWlvQixRQUFJLEdBQUNyQyxHQUFHLEdBQUMsQ0FBQ3FDLElBQUYsR0FBT0EsSUFBZjtBQUFvQmpkLFVBQU0sQ0FBQy9DLFlBQVA7QUFBc0IrQyxVQUFNLENBQUNoQyxRQUFQLENBQWdCckMsQ0FBaEIsR0FBa0JxRSxNQUFNLENBQUNqRCxVQUFQLENBQWtCcEIsQ0FBbEIsR0FBb0JzaEIsSUFBdEM7QUFBMkNqZCxVQUFNLENBQUM0YyxLQUFQLEdBQWE7QUFBQzluQixVQUFJLEVBQUMsQ0FBTjtBQUFRQyxXQUFLLEVBQUMsRUFBRSxLQUFLa1YsT0FBTCxDQUFhNWIsTUFBYixHQUFvQixDQUF0QixJQUF5Qm1XLE1BQU0sQ0FBQ3hQO0FBQTlDLEtBQWI7O0FBQWtFLFFBQUc0bEIsR0FBSCxFQUFPO0FBQUM1YSxZQUFNLENBQUM0YyxLQUFQLENBQWE5bkIsSUFBYixHQUFrQixDQUFDa0wsTUFBTSxDQUFDNGMsS0FBUCxDQUFhN25CLEtBQWhDO0FBQXNDaUwsWUFBTSxDQUFDNGMsS0FBUCxDQUFhN25CLEtBQWIsR0FBbUIsQ0FBbkI7QUFBc0I7O0FBQ3gzQ3VELFNBQUssQ0FBQ29ELFNBQU4sQ0FBZ0IsS0FBS21JLEdBQUwsQ0FBUzdELE1BQXpCLEVBQWdDaWQsSUFBaEMsRUFBcUMsQ0FBckM7QUFBeUMsR0FEbWdDOztBQUNsZ0M5Z0IsT0FBSyxDQUFDNEgsWUFBTixHQUFtQixZQUFVO0FBQUMsUUFBSS9ELE1BQU0sR0FBQyxLQUFLNkQsR0FBTCxDQUFTN0QsTUFBcEI7QUFBQSxRQUEyQmxELFFBQVEsR0FBQyxLQUFLbkUsT0FBTCxDQUFhbUUsUUFBakQ7QUFBQSxRQUEwREMsVUFBVSxHQUFDLEtBQUtwRSxPQUFMLENBQWFvRSxVQUFsRjtBQUE2RixTQUFLaUQsTUFBTCxHQUFZLElBQUlwRCxPQUFKLENBQVlvRCxNQUFaLEVBQW1CO0FBQUNyRSxPQUFDLEVBQUMsQ0FBSDtBQUFLQyxPQUFDLEVBQUM7QUFBUCxLQUFuQixFQUE2QjdELElBQUksQ0FBQ2daLEdBQUwsQ0FBU2haLElBQUksQ0FBQ0MsR0FBTCxDQUFTOEUsUUFBUSxDQUFDa0QsTUFBbEIsRUFBeUIsQ0FBekIsQ0FBVCxFQUFxQyxDQUFyQyxDQUE3QixFQUFxRWpJLElBQUksQ0FBQ2daLEdBQUwsQ0FBU2haLElBQUksQ0FBQ0MsR0FBTCxDQUFTK0UsVUFBVSxDQUFDaUQsTUFBcEIsRUFBMkIsQ0FBM0IsQ0FBVCxFQUF1QyxDQUF2QyxDQUFyRSxDQUFaO0FBQTRILFNBQUtBLE1BQUwsQ0FBWS9PLEVBQVosQ0FBZSxrQkFBZixFQUFrQyxLQUFLaXNCLFdBQUwsQ0FBaUI3bUIsSUFBakIsQ0FBc0IsSUFBdEIsQ0FBbEM7QUFBK0QsU0FBSzJKLE1BQUwsQ0FBWS9PLEVBQVosQ0FBZSxrQkFBZixFQUFrQyxLQUFLa3NCLFlBQUwsQ0FBa0I5bUIsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FBbEM7QUFBZ0UsUUFBSW1PLE1BQU0sR0FBQ3hFLE1BQU0sQ0FBQzZILFFBQWxCO0FBQUEsUUFBMkJ4WixNQUFNLEdBQUNtVyxNQUFNLENBQUNuVyxNQUF6Qzs7QUFBZ0QsU0FBSSxJQUFJRixDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNFLE1BQWQsRUFBcUJGLENBQUMsRUFBdEIsRUFBeUI7QUFBQyxXQUFLc1csS0FBTCxDQUFXdFcsQ0FBWCxJQUFjLElBQUl5TyxPQUFKLENBQVk0SCxNQUFNLENBQUNyVyxDQUFELENBQU4sQ0FBVTBaLFFBQVYsQ0FBbUIsQ0FBbkIsQ0FBWixFQUFrQztBQUFDbE0sU0FBQyxFQUFDLENBQUg7QUFBS0MsU0FBQyxFQUFDLENBQVA7QUFBU0MsU0FBQyxFQUFDO0FBQVgsT0FBbEMsRUFBZ0Q5RCxJQUFJLENBQUNnWixHQUFMLENBQVNoWixJQUFJLENBQUNDLEdBQUwsQ0FBUzhFLFFBQVEsQ0FBQ21ELEtBQWxCLEVBQXdCLENBQXhCLENBQVQsRUFBb0MsQ0FBcEMsQ0FBaEQsRUFBdUZsSSxJQUFJLENBQUNnWixHQUFMLENBQVNoWixJQUFJLENBQUNDLEdBQUwsQ0FBUytFLFVBQVUsQ0FBQ2tELEtBQXBCLEVBQTBCLENBQTFCLENBQVQsRUFBc0MsQ0FBdEMsQ0FBdkYsQ0FBZDtBQUErSSxXQUFLd0UsS0FBTCxDQUFXdFcsQ0FBWCxFQUFjOEMsRUFBZCxDQUFpQixrQkFBakIsRUFBb0MsS0FBS21zQixVQUFMLENBQWdCL21CLElBQWhCLENBQXFCLElBQXJCLENBQXBDO0FBQWdFLFdBQUtvTyxLQUFMLENBQVd0VyxDQUFYLEVBQWM4QyxFQUFkLENBQWlCLGtCQUFqQixFQUFvQyxLQUFLb3NCLFVBQUwsQ0FBZ0JobkIsSUFBaEIsQ0FBcUIsSUFBckIsQ0FBcEM7QUFBaUU7O0FBQzF2QixTQUFLNkosTUFBTCxHQUFZLElBQUl0RCxPQUFKLENBQVksS0FBS2lILEdBQUwsQ0FBU21GLFdBQXJCLEVBQWlDO0FBQUNyTixPQUFDLEVBQUM7QUFBSCxLQUFqQyxFQUF1QzVELElBQUksQ0FBQ2daLEdBQUwsQ0FBU2haLElBQUksQ0FBQ0MsR0FBTCxDQUFTOEUsUUFBUSxDQUFDb0QsTUFBbEIsRUFBeUIsQ0FBekIsQ0FBVCxFQUFxQyxDQUFyQyxDQUF2QyxFQUErRW5JLElBQUksQ0FBQ2daLEdBQUwsQ0FBU2haLElBQUksQ0FBQ0MsR0FBTCxDQUFTK0UsVUFBVSxDQUFDbUQsTUFBcEIsRUFBMkIsQ0FBM0IsQ0FBVCxFQUF1QyxDQUF2QyxDQUEvRSxDQUFaO0FBQXNJLFNBQUtBLE1BQUwsQ0FBWWpQLEVBQVosQ0FBZSxrQkFBZixFQUFrQyxLQUFLcXNCLFlBQUwsQ0FBa0JqbkIsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FBbEM7QUFBZ0UsU0FBSzZKLE1BQUwsQ0FBWWpQLEVBQVosQ0FBZSxrQkFBZixFQUFrQyxLQUFLZ3JCLFlBQUwsQ0FBa0I1bEIsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FBbEM7QUFBaUUsR0FEN047O0FBQzhOOEYsT0FBSyxDQUFDK2dCLFdBQU4sR0FBa0IsVUFBU2xkLE1BQVQsRUFBZ0I7QUFBQyxRQUFJdUosS0FBSjtBQUFValIsU0FBSyxDQUFDbUMsYUFBTixDQUFvQixJQUFwQixFQUF5QixXQUF6QixFQUFxQyxlQUFyQyxFQUFxRHVGLE1BQU0sQ0FBQ2hDLFFBQTVEOztBQUFzRSxRQUFHLEtBQUswRyxNQUFMLENBQVkrSCxJQUFmLEVBQW9CO0FBQUMsV0FBS3lCLFFBQUw7QUFBZ0IsV0FBS0UsWUFBTDtBQUFxQjs7QUFDcmIsUUFBRyxLQUFLelYsT0FBTCxDQUFhZ0ksWUFBaEIsRUFBNkI7QUFBQyxVQUFJNkQsTUFBTSxHQUFDLEtBQUtBLE1BQWhCOztBQUF1QixXQUFJLElBQUlyVyxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNxVyxNQUFNLENBQUNuVyxNQUFyQixFQUE0QkYsQ0FBQyxFQUE3QixFQUFnQztBQUFDLFlBQUlnTixLQUFLLEdBQUNxSixNQUFNLENBQUNyVyxDQUFELENBQU4sQ0FBVW9iLEtBQXBCO0FBQTBCQSxhQUFLLEdBQUMsS0FBS1UsT0FBTCxDQUFhOU8sS0FBYixDQUFOOztBQUEwQixZQUFHb08sS0FBSyxDQUFDakMsR0FBTixDQUFVcUcsTUFBYixFQUFvQjtBQUFDLGVBQUt1TCxTQUFMLENBQWUzUCxLQUFmLEVBQXFCcGIsQ0FBckI7QUFBeUI7QUFBQztBQUFDO0FBQUMsR0FENkU7O0FBQzVFZ08sT0FBSyxDQUFDaWhCLFVBQU4sR0FBaUIsVUFBU3ZNLElBQVQsRUFBYztBQUFDLFFBQUl2TSxPQUFPLEdBQUMsS0FBS0EsT0FBakI7O0FBQXlCLFFBQUdBLE9BQU8sQ0FBQ2laLE9BQVgsRUFBbUI7QUFBQ2psQixXQUFLLENBQUNtQyxhQUFOLENBQW9CLElBQXBCLEVBQXlCLFdBQXpCLEVBQXFDLGFBQXJDLEVBQW1ELElBQW5ELEVBQXdEb1csSUFBSSxDQUFDN1MsUUFBN0Q7QUFBd0U7O0FBQ2pWLFFBQUlzRyxPQUFPLENBQUNpWixPQUFSLElBQWlCalosT0FBTyxDQUFDa1osUUFBUixLQUFtQixLQUFyQyxJQUE2QyxDQUFDbFosT0FBTyxDQUFDaVosT0FBekQsRUFBaUU7QUFBQ2psQixXQUFLLENBQUNtQyxhQUFOLENBQW9CLElBQXBCLEVBQXlCLFdBQXpCLEVBQXFDLGdCQUFyQyxFQUFzRCxJQUF0RCxFQUEyRG9XLElBQUksQ0FBQzdTLFFBQWhFO0FBQTJFO0FBQUMsR0FEOEM7O0FBQzdDN0IsT0FBSyxDQUFDbWhCLFlBQU4sR0FBbUIsVUFBU3BkLE1BQVQsRUFBZ0I7QUFBQzVILFNBQUssQ0FBQ21DLGFBQU4sQ0FBb0IsSUFBcEIsRUFBeUIsV0FBekIsRUFBcUMsZUFBckMsRUFBcUQsSUFBckQsRUFBMER5RixNQUFNLENBQUNsQyxRQUFqRTtBQUE0RSxHQUFoSDs7QUFBaUg3QixPQUFLLENBQUNnaEIsWUFBTixHQUFtQixVQUFTbmQsTUFBVCxFQUFnQjtBQUFDLFNBQUt5YyxXQUFMO0FBQW1CLFFBQUk3QixHQUFHLEdBQUMsS0FBS3hKLEtBQUwsRUFBUjtBQUFBLFFBQXFCL2lCLE1BQU0sR0FBQyxLQUFLNGIsT0FBTCxDQUFhcUYsYUFBekM7QUFBQSxRQUF1RG1PLFFBQVEsR0FBQyxDQUFDN0MsR0FBRCxHQUFLNWEsTUFBTSxDQUFDaEMsUUFBUCxDQUFnQnJDLENBQXJCLEdBQXVCLEtBQUs2SSxNQUFMLENBQVl4UCxLQUFuRztBQUFBLFFBQXlHMG9CLFNBQVMsR0FBQ3BsQixLQUFLLENBQUM0QyxNQUFOLENBQWE3TSxNQUFiLEVBQW9Cb3ZCLFFBQXBCLENBQW5IO0FBQUEsUUFBaUpFLFFBQVEsR0FBQyxDQUFDRCxTQUFTLEdBQUNydkIsTUFBTSxHQUFDLEdBQWpCLEdBQXFCLENBQXJCLEdBQXVCcXZCLFNBQXhCLEtBQW9DcnZCLE1BQU0sR0FBQyxDQUEzQyxDQUExSjtBQUF3TWlLLFNBQUssQ0FBQ21DLGFBQU4sQ0FBb0IsSUFBcEIsRUFBeUIsV0FBekIsRUFBcUMsZ0JBQXJDLEVBQXNELElBQXRELEVBQTJEMUMsSUFBSSxDQUFDZ1osR0FBTCxDQUFTLENBQVQsRUFBV2haLElBQUksQ0FBQ0MsR0FBTCxDQUFTLENBQVQsRUFBVzJsQixRQUFYLENBQVgsQ0FBM0Q7QUFBOEYsR0FBN1Y7O0FBQThWeGhCLE9BQUssQ0FBQ2toQixVQUFOLEdBQWlCLFVBQVN4TSxJQUFULEVBQWM7QUFBQyxTQUFLK00sU0FBTCxDQUFlL00sSUFBZjtBQUFxQixRQUFJOE0sUUFBSjs7QUFBYSxRQUFHLEtBQUtyWixPQUFMLENBQWFsVSxJQUFiLEtBQW9CLE1BQXBCLElBQTRCLEtBQUtrVSxPQUFMLENBQWFpWixPQUF6QyxJQUFtRCxLQUFLalosT0FBTCxDQUFhbFUsSUFBYixLQUFvQixZQUFwQixJQUFrQ3lnQixJQUFJLENBQUM3UyxRQUFMLENBQWNwQyxDQUFkLEtBQWtCLENBQTFHLEVBQTZHO0FBQUMraEIsY0FBUSxHQUFDLElBQUU1bEIsSUFBSSxDQUFDaVosR0FBTCxDQUFTSCxJQUFJLENBQUM3UyxRQUFMLENBQWNwQyxDQUF2QixLQUEyQixLQUFLb0UsTUFBTCxDQUFZL0ssTUFBWixHQUFtQixHQUE5QyxDQUFYO0FBQThEcUQsV0FBSyxDQUFDbUMsYUFBTixDQUFvQixJQUFwQixFQUF5QixXQUF6QixFQUFxQyxjQUFyQyxFQUFvRCxJQUFwRCxFQUF5RGtqQixRQUF6RDtBQUFvRTs7QUFDaDVCLFFBQUcsS0FBS3JaLE9BQUwsQ0FBYWxVLElBQWIsS0FBb0IsTUFBcEIsSUFBNEJ5Z0IsSUFBSSxDQUFDN1MsUUFBTCxDQUFjbkMsQ0FBZCxLQUFrQixDQUFqRCxFQUFtRDtBQUFDOGhCLGNBQVEsR0FBQzlNLElBQUksQ0FBQzdTLFFBQUwsQ0FBY25DLENBQXZCO0FBQXlCdkQsV0FBSyxDQUFDbUMsYUFBTixDQUFvQixJQUFwQixFQUF5QixXQUF6QixFQUFxQyxpQkFBckMsRUFBdUQsSUFBdkQsRUFBNERrakIsUUFBNUQ7QUFBdUU7QUFBQyxHQUR5Yzs7QUFDeGN4aEIsT0FBSyxDQUFDOGYsWUFBTixHQUFtQixVQUFTL2IsTUFBVCxFQUFnQjtBQUFDLFNBQUs4YixXQUFMLENBQWlCOWIsTUFBakI7QUFBeUIsUUFBSXlkLFFBQVEsR0FBQ3pkLE1BQU0sQ0FBQzBjLEtBQVAsQ0FBYTluQixJQUFiLEtBQW9Cb0wsTUFBTSxDQUFDMGMsS0FBUCxDQUFhN25CLEtBQWpDLEdBQXVDLENBQUNtTCxNQUFNLENBQUMwYyxLQUFQLENBQWE5bkIsSUFBYixHQUFrQm9MLE1BQU0sQ0FBQ2xDLFFBQVAsQ0FBZ0JyQyxDQUFuQyxLQUF1Q3VFLE1BQU0sQ0FBQzBjLEtBQVAsQ0FBYTluQixJQUFiLEdBQWtCb0wsTUFBTSxDQUFDMGMsS0FBUCxDQUFhN25CLEtBQXRFLENBQXZDLEdBQW9ILENBQWpJO0FBQW1JdUQsU0FBSyxDQUFDbUMsYUFBTixDQUFvQixJQUFwQixFQUF5QixXQUF6QixFQUFxQyxnQkFBckMsRUFBc0QsSUFBdEQsRUFBMkRrakIsUUFBM0Q7QUFBc0UsR0FBdFE7O0FBQXVReGhCLE9BQUssQ0FBQzBoQixVQUFOLEdBQWlCLFVBQVMxdEIsS0FBVCxFQUFlO0FBQUMsUUFBSXFCLE9BQU8sR0FBQ3JCLEtBQUssQ0FBQytiLE1BQWxCO0FBQUEsUUFBeUJoWCxPQUFPLEdBQUMxRCxPQUFPLENBQUMwRCxPQUF6QztBQUFBLFFBQWlENEUsU0FBUyxHQUFDdEksT0FBTyxDQUFDc0ksU0FBbkU7O0FBQTZFLFFBQUczSixLQUFLLENBQUMydEIsS0FBTixLQUFjLENBQWQsSUFBaUJ0c0IsT0FBTyxLQUFHLEtBQUsrUyxPQUFMLENBQWE0QixJQUEzQyxFQUFnRDtBQUFDLFdBQUtnSixhQUFMO0FBQXNCOztBQUNsbEIsUUFBR2hmLEtBQUssQ0FBQzJ0QixLQUFOLEtBQWMsQ0FBZCxJQUFpQixDQUFDLEtBQUt2TixJQUFMLENBQVVwZ0IsS0FBVixDQUFsQixJQUFvQyxDQUFDLFFBQUQsRUFBVSxPQUFWLEVBQWtCLE9BQWxCLEVBQTBCLEdBQTFCLEVBQStCb00sT0FBL0IsQ0FBdUNySCxPQUF2QyxJQUFnRCxDQUFDLENBQXhGLEVBQTBGO0FBQUM7QUFBUTs7QUFDbkcsUUFBR0EsT0FBTyxLQUFHLEtBQVYsSUFBaUIsS0FBSytVLE9BQUwsQ0FBYTViLE1BQWIsR0FBb0IsQ0FBeEMsRUFBMEM7QUFBQ2lLLFdBQUssQ0FBQ2tELFFBQU4sQ0FBZSxLQUFLcUksR0FBTCxDQUFTQyxNQUF4QixFQUErQixLQUFLTyxHQUFMLEdBQVMsV0FBeEM7QUFBc0Q7O0FBQ2pHbFUsU0FBSyxDQUFDc2QsY0FBTjs7QUFBdUIsUUFBR25WLEtBQUssQ0FBQ2dELFFBQU4sQ0FBZSxLQUFLdUksR0FBTCxDQUFTQyxNQUF4QixFQUErQixLQUFLTyxHQUFMLEdBQVMsZUFBeEMsQ0FBSCxFQUE0RDtBQUFDO0FBQVE7O0FBQzVGLFFBQUcsQ0FBQyxLQUFLTSxRQUFMLENBQWN0VyxNQUFsQixFQUF5QjtBQUFDLFdBQUtpVyxPQUFMLENBQWFrWixRQUFiLEdBQXNCendCLFNBQXRCO0FBQWdDdUwsV0FBSyxDQUFDNEIsWUFBTixDQUFtQixJQUFuQixFQUF3QnBOLE1BQXhCLEVBQStCLEtBQUs4WCxVQUFMLENBQWdCdkgsSUFBL0MsRUFBb0QsV0FBcEQ7QUFBaUUvRSxXQUFLLENBQUM0QixZQUFOLENBQW1CLElBQW5CLEVBQXdCcE4sTUFBeEIsRUFBK0IsS0FBSzhYLFVBQUwsQ0FBZ0JZLEdBQS9DLEVBQW1ELFVBQW5EO0FBQWdFOztBQUMzTCxTQUFLdVksVUFBTCxDQUFnQjV0QixLQUFoQjs7QUFBdUIsUUFBRzJKLFNBQVMsQ0FBQ3lDLE9BQVYsQ0FBa0IsUUFBbEIsSUFBNEIsQ0FBL0IsRUFBaUM7QUFBQyxXQUFLeUQsTUFBTCxDQUFZbEMsV0FBWjtBQUEwQixVQUFJK1MsSUFBSSxHQUFDLEtBQUtDLE9BQUwsRUFBVDs7QUFBd0IsVUFBRy9ZLElBQUksQ0FBQ29ILEtBQUwsQ0FBVzBSLElBQUksQ0FBQzdTLFFBQUwsQ0FBY25DLENBQWQsR0FBZ0IsR0FBM0IsSUFBZ0MsR0FBaEMsS0FBc0MsQ0FBdEMsSUFBeUMsS0FBSzhJLFFBQUwsQ0FBY3RXLE1BQWQsS0FBdUIsQ0FBaEUsSUFBbUUsS0FBS2lXLE9BQUwsQ0FBYWlaLE9BQW5GLEVBQTJGO0FBQUMxTSxZQUFJLENBQUMvUyxXQUFMO0FBQW9CO0FBQUMsS0FBck0sTUFBeU07QUFBQyxXQUFLb0MsTUFBTCxDQUFZcEMsV0FBWjtBQUEyQjs7QUFDNVAsU0FBS2tnQixRQUFMLENBQWMsT0FBZDtBQUF3QixHQU5xWTs7QUFNcFk3aEIsT0FBSyxDQUFDOGhCLFNBQU4sR0FBZ0IsVUFBUzl0QixLQUFULEVBQWU7QUFBQyxTQUFLK3RCLGFBQUwsQ0FBbUIvdEIsS0FBbkI7QUFBMEIsUUFBSW1VLE9BQU8sR0FBQyxLQUFLQSxPQUFqQjtBQUF5QixRQUFJNlosU0FBUyxHQUFDLEtBQUt4WixRQUFMLENBQWN0VyxNQUE1QjtBQUFtQyxRQUFJK3ZCLFFBQVEsR0FBQyxLQUFLQyxjQUFMLEVBQWI7QUFBbUMsU0FBS0MsY0FBTDtBQUFzQixTQUFLTixRQUFMLENBQWMsTUFBZDs7QUFBc0IsUUFBRzFaLE9BQU8sQ0FBQ2xVLElBQVgsRUFBZ0I7QUFBQyxXQUFLa1UsT0FBTyxDQUFDbFUsSUFBYixFQUFtQkQsS0FBbkI7QUFBMEJtSSxXQUFLLENBQUNtQyxhQUFOLENBQW9CLElBQXBCLEVBQXlCLFdBQXpCLEVBQXFDNkosT0FBTyxDQUFDbFUsSUFBUixHQUFhLE1BQWxELEVBQXlERCxLQUF6RCxFQUErRG1VLE9BQS9EO0FBQXdFQSxhQUFPLENBQUNqSCxJQUFSLEdBQWEsSUFBYjtBQUFtQixLQUF0SSxNQUN6TixJQUFJOGdCLFNBQVMsS0FBRyxDQUFaLElBQWVDLFFBQWhCLElBQTJCcm1CLElBQUksQ0FBQ2laLEdBQUwsQ0FBUzFNLE9BQU8sQ0FBQ2lhLEVBQWpCLElBQXFCLEtBQUs1bEIsT0FBTCxDQUFhcEUsU0FBN0QsSUFBd0V3RCxJQUFJLENBQUNpWixHQUFMLENBQVMxTSxPQUFPLENBQUNrYSxFQUFqQixJQUFxQixLQUFLN2xCLE9BQUwsQ0FBYXBFLFNBQTdHLEVBQXVIO0FBQUMrUCxhQUFPLENBQUNtYSxFQUFSLElBQVluYSxPQUFPLENBQUNpYSxFQUFwQjtBQUF1QmphLGFBQU8sQ0FBQ29hLEVBQVIsSUFBWXBhLE9BQU8sQ0FBQ2thLEVBQXBCO0FBQXVCbGEsYUFBTyxDQUFDcWEsT0FBUixHQUFnQixLQUFLL00sVUFBTCxFQUFoQjtBQUFrQ3ROLGFBQU8sQ0FBQ2laLE9BQVIsR0FBZ0IsS0FBaEI7QUFBc0JqWixhQUFPLENBQUNsVSxJQUFSLEdBQWEySCxJQUFJLENBQUNpWixHQUFMLENBQVMxTSxPQUFPLENBQUNpYSxFQUFqQixJQUFxQnhtQixJQUFJLENBQUNpWixHQUFMLENBQVMxTSxPQUFPLENBQUNrYSxFQUFqQixJQUFxQixDQUExQyxHQUE0QyxLQUE1QyxHQUFrRCxZQUEvRDtBQUE0RWxhLGFBQU8sQ0FBQ2xVLElBQVIsR0FBYSxLQUFLdUksT0FBTCxDQUFhNkosV0FBYixJQUEwQixDQUFDOEIsT0FBTyxDQUFDbFUsSUFBbkMsSUFBeUNndUIsUUFBekMsR0FBa0QsTUFBbEQsR0FBeUQ5WixPQUFPLENBQUNsVSxJQUE5RTtBQUFtRmtVLGFBQU8sQ0FBQ2xVLElBQVIsR0FBYSxDQUFDLEtBQUt1SSxPQUFMLENBQWF5SixXQUFiLElBQTBCLEtBQUtzQyxNQUFMLENBQVkwSSxJQUF2QyxLQUE4QzlJLE9BQU8sQ0FBQ3FhLE9BQXRELElBQStEUCxRQUEvRCxLQUEwRUQsU0FBUyxLQUFHLENBQVosSUFBZSxLQUFLelosTUFBTCxDQUFZMEksSUFBckcsSUFBMkcsU0FBM0csR0FBcUg5SSxPQUFPLENBQUNsVSxJQUExSTtBQUErSWtVLGFBQU8sQ0FBQ2xVLElBQVIsR0FBYSxLQUFLdUksT0FBTCxDQUFhNEosWUFBYixJQUEyQitCLE9BQU8sQ0FBQ3hJLEtBQVIsR0FBYyxDQUF6QyxJQUE0Q3NpQixRQUE1QyxJQUFzREQsU0FBUyxLQUFHLENBQWxFLEdBQW9FLFNBQXBFLEdBQThFN1osT0FBTyxDQUFDbFUsSUFBbkc7QUFBd0drVSxhQUFPLENBQUNsVSxJQUFSLEdBQWFELEtBQUssQ0FBQytiLE1BQU4sQ0FBYXBTLFNBQWIsQ0FBdUJ5QyxPQUF2QixDQUErQixRQUEvQixJQUF5QyxDQUFDLENBQTFDLEdBQTRDLFlBQTVDLEdBQXlEK0gsT0FBTyxDQUFDbFUsSUFBOUU7O0FBQW1GLFVBQUdrVSxPQUFPLENBQUNsVSxJQUFSLEtBQWUsWUFBbEIsRUFBK0I7QUFBQyxhQUFLOGQsUUFBTDtBQUFpQjs7QUFDN3ZCLFVBQUcsQ0FBQyxZQUFELEVBQWMsWUFBZCxFQUE0QjNSLE9BQTVCLENBQW9DK0gsT0FBTyxDQUFDbFUsSUFBNUMsSUFBa0QsQ0FBQyxDQUF0RCxFQUF3RDtBQUFDLFlBQUl5Z0IsSUFBSSxHQUFDLEtBQUtDLE9BQUwsRUFBVDtBQUF3QkQsWUFBSSxDQUFDcFQsWUFBTDtBQUFvQm9ULFlBQUksQ0FBQ3RULFdBQUw7QUFBbUJzVCxZQUFJLENBQUNyVCxTQUFMLENBQWU7QUFBQzdCLFdBQUMsRUFBQyxDQUFIO0FBQUtDLFdBQUMsRUFBQyxDQUFQO0FBQVNDLFdBQUMsRUFBQztBQUFYLFNBQWY7QUFBK0I7O0FBQ3ZKLFVBQUd5SSxPQUFPLENBQUNsVSxJQUFSLEtBQWUsWUFBbEIsRUFBK0I7QUFBQyxZQUFJNFAsTUFBTSxHQUFDLEtBQUtBLE1BQWhCO0FBQUEsWUFBdUJ3RSxNQUFNLEdBQUMsS0FBS0EsTUFBbkM7QUFBMEMsWUFBSW9XLEdBQUcsR0FBQyxLQUFLeEosS0FBTCxFQUFSOztBQUFxQixZQUFHLENBQUN3SixHQUFELEdBQUs1YSxNQUFNLENBQUNoQyxRQUFQLENBQWdCckMsQ0FBckIsS0FBeUI2SSxNQUFNLENBQUNySixLQUFQLEdBQWFxSixNQUFNLENBQUN4UCxLQUFoRCxFQUFzRDtBQUFDZ0wsZ0JBQU0sQ0FBQ3ZDLFlBQVA7QUFBc0J1QyxnQkFBTSxDQUFDekMsV0FBUDtBQUFzQjtBQUFDOztBQUNuTSxVQUFHK0csT0FBTyxDQUFDbFUsSUFBWCxFQUFnQjtBQUFDLGFBQUtnZixVQUFMO0FBQWtCOVcsYUFBSyxDQUFDbUMsYUFBTixDQUFvQixJQUFwQixFQUF5QixXQUF6QixFQUFxQzZKLE9BQU8sQ0FBQ2xVLElBQVIsR0FBYSxPQUFsRCxFQUEwREQsS0FBMUQsRUFBZ0VtVSxPQUFoRTs7QUFBeUUsWUFBRyxLQUFLMkYsT0FBTCxDQUFhNWIsTUFBYixHQUFvQixDQUFwQixJQUF1QmlXLE9BQU8sQ0FBQ2xVLElBQVIsS0FBZSxZQUF6QyxFQUFzRDtBQUFDa0ksZUFBSyxDQUFDa0QsUUFBTixDQUFlLEtBQUtxSSxHQUFMLENBQVNDLE1BQXhCLEVBQStCLEtBQUtPLEdBQUwsR0FBUyxXQUF4QztBQUFzRDtBQUFDO0FBQUM7QUFBQyxHQUpuTTs7QUFJb01sSSxPQUFLLENBQUN5aUIsUUFBTixHQUFlLFVBQVN6dUIsS0FBVCxFQUFlO0FBQUMsU0FBSzB1QixhQUFMLENBQW1CMXVCLEtBQW5COztBQUEwQixRQUFHLENBQUMsS0FBS3dVLFFBQUwsQ0FBY3RXLE1BQWxCLEVBQXlCO0FBQUNpSyxXQUFLLENBQUNtRCxXQUFOLENBQWtCLEtBQUtvSSxHQUFMLENBQVNDLE1BQTNCLEVBQWtDLEtBQUtPLEdBQUwsR0FBUyxXQUEzQztBQUF3RC9MLFdBQUssQ0FBQzRCLFlBQU4sQ0FBbUIsSUFBbkIsRUFBd0JwTixNQUF4QixFQUErQixLQUFLOFgsVUFBTCxDQUFnQnZILElBQS9DLEVBQW9ELFdBQXBELEVBQWdFLEtBQWhFO0FBQXVFL0UsV0FBSyxDQUFDNEIsWUFBTixDQUFtQixJQUFuQixFQUF3QnBOLE1BQXhCLEVBQStCLEtBQUs4WCxVQUFMLENBQWdCWSxHQUEvQyxFQUFtRCxVQUFuRCxFQUE4RCxLQUE5RDs7QUFBcUUsVUFBRyxLQUFLNlksY0FBTCxFQUFILEVBQXlCO0FBQUMsWUFBSXZrQixTQUFTLEdBQUMzSixLQUFLLENBQUMrYixNQUFOLENBQWFwUyxTQUEzQjs7QUFBcUMsWUFBR3hCLEtBQUssQ0FBQ2dELFFBQU4sQ0FBZW5MLEtBQUssQ0FBQytiLE1BQXJCLEVBQTRCLEtBQUs3SCxHQUFMLEdBQVMsUUFBckMsQ0FBSCxFQUFrRDtBQUFDLGVBQUttSyxXQUFMO0FBQW9CLFNBQXZFLE1BQTRFLElBQUcsS0FBSzdWLE9BQUwsQ0FBYThKLFVBQWIsSUFBeUIsQ0FBQyxLQUFLaUMsTUFBTCxDQUFZMEksSUFBdEMsS0FBNkN0VCxTQUFTLEtBQUcsS0FBS3VLLEdBQUwsR0FBUyxhQUFyQixJQUFvQ3ZLLFNBQVMsS0FBRyxLQUFLdUssR0FBTCxHQUFTLFVBQXRHLEtBQW1IdE0sSUFBSSxDQUFDaVosR0FBTCxDQUFTLEtBQUsxTSxPQUFMLENBQWFpYSxFQUF0QixJQUEwQixLQUFLNWxCLE9BQUwsQ0FBYXBFLFNBQTdKLEVBQXVLO0FBQUMsZUFBSzBhLEtBQUw7QUFBYTtBQUFROztBQUM1ekIsWUFBRzllLEtBQUssQ0FBQytiLE1BQU4sQ0FBYWhYLE9BQWIsS0FBdUIsS0FBMUIsRUFBZ0M7QUFBQyxlQUFLNHBCLFNBQUwsQ0FBZTN1QixLQUFmO0FBQXVCO0FBQUM7O0FBQ3pELFVBQUcsS0FBS3dJLE9BQUwsQ0FBYXdJLFVBQWIsSUFBeUIsQ0FBQyxLQUFLbUQsT0FBTCxDQUFhakgsSUFBMUMsRUFBK0M7QUFBQyxhQUFLd2UsVUFBTCxDQUFnQjFyQixLQUFoQjtBQUF3Qjs7QUFDeEUsVUFBSTR1QixVQUFVLEdBQUMsS0FBS3phLE9BQUwsQ0FBYWxVLElBQWIsR0FBa0IsS0FBakM7O0FBQXVDLFVBQUcsS0FBS2tVLE9BQUwsQ0FBYWxVLElBQWIsSUFBbUIsT0FBTyxLQUFLMnVCLFVBQUwsQ0FBUCxLQUEwQixVQUFoRCxFQUEyRDtBQUFDLGFBQUtBLFVBQUwsRUFBaUI1dUIsS0FBakI7QUFBd0JtSSxhQUFLLENBQUNtQyxhQUFOLENBQW9CLElBQXBCLEVBQXlCLFdBQXpCLEVBQXFDc2tCLFVBQXJDLEVBQWdENXVCLEtBQWhELEVBQXNELEtBQUttVSxPQUEzRDtBQUFxRTs7QUFDaE0sV0FBS0EsT0FBTCxDQUFhbFUsSUFBYixHQUFrQixLQUFLa1UsT0FBTCxDQUFhakgsSUFBYixHQUFrQixLQUFwQzs7QUFBMEMsVUFBRyxDQUFDLEtBQUtxSCxNQUFMLENBQVkrSCxJQUFoQixFQUFxQjtBQUFDO0FBQVE7O0FBQ3hFLFVBQUlvRSxJQUFJLEdBQUMsS0FBS0MsT0FBTCxFQUFUOztBQUF3QixVQUFHLENBQUNELElBQUksQ0FBQ25ULE1BQVQsRUFBZ0I7QUFBQ21ULFlBQUksQ0FBQ3BULFlBQUw7QUFBb0JvVCxZQUFJLENBQUN0VCxXQUFMO0FBQW9COztBQUNqRixVQUFJeUMsTUFBTSxHQUFDLEtBQUtBLE1BQWhCOztBQUF1QixVQUFHLENBQUNBLE1BQU0sQ0FBQ3RDLE1BQVgsRUFBa0I7QUFBQ3NDLGNBQU0sQ0FBQ3ZDLFlBQVA7QUFBc0J1QyxjQUFNLENBQUN6QyxXQUFQO0FBQXNCO0FBQUM7QUFBQyxHQU5xSTs7QUFNcElwQixPQUFLLENBQUNtaUIsY0FBTixHQUFxQixZQUFVO0FBQUMsUUFBRyxLQUFLaGEsT0FBTCxDQUFhbFUsSUFBYixLQUFvQixTQUFwQixJQUErQixLQUFLdVUsUUFBTCxDQUFjdFcsTUFBZCxLQUF1QixDQUF0RCxJQUF5RCxLQUFLaVcsT0FBTCxDQUFhdEYsUUFBYixLQUF3QixDQUFwRixFQUFzRjtBQUFDLFVBQUlnZ0IsTUFBTSxHQUFDLEtBQUtsTyxPQUFMLEVBQVg7QUFBMEJrTyxZQUFNLENBQUNsaEIsV0FBUDtBQUFxQmtoQixZQUFNLENBQUN2aEIsWUFBUDtBQUFzQixXQUFLNkcsT0FBTCxDQUFhakgsSUFBYixHQUFrQixLQUFsQjtBQUF3QixXQUFLMmdCLFFBQUwsQ0FBYyxPQUFkO0FBQXVCLFdBQUtBLFFBQUwsQ0FBYyxNQUFkO0FBQXVCO0FBQUMsR0FBblE7O0FBQW9RN2hCLE9BQUssQ0FBQzJpQixTQUFOLEdBQWdCLFVBQVMzdUIsS0FBVCxFQUFlO0FBQUNBLFNBQUssQ0FBQ3NkLGNBQU47QUFBdUIsUUFBSXdSLE9BQU8sR0FBQyxLQUFLQyxVQUFMLENBQWdCL3VCLEtBQWhCLENBQVo7QUFBQSxRQUFtQzhnQixPQUFPLEdBQUNnTyxPQUFPLENBQUMsQ0FBRCxDQUFQLENBQVdoTyxPQUF0RDtBQUFBLFFBQThEQyxPQUFPLEdBQUMrTixPQUFPLENBQUMsQ0FBRCxDQUFQLENBQVcvTixPQUFqRjs7QUFBeUYsUUFBRyxPQUFPLEtBQUtpTyxHQUFaLEtBQWtCLFdBQWxCLElBQStCLENBQUMsSUFBSXRwQixJQUFKLEVBQUQsR0FBWSxLQUFLc3BCLEdBQUwsQ0FBU3Z2QixLQUFyQixHQUEyQixHQUExRCxJQUErRG1JLElBQUksQ0FBQ2laLEdBQUwsQ0FBUyxLQUFLbU8sR0FBTCxDQUFTaE0sTUFBVCxHQUFnQmxDLE9BQXpCLElBQWtDLEVBQWpHLElBQXFHbFosSUFBSSxDQUFDaVosR0FBTCxDQUFTLEtBQUttTyxHQUFMLENBQVN2TyxNQUFULEdBQWdCTSxPQUF6QixJQUFrQyxFQUExSSxFQUE2STtBQUFDLFVBQUcsS0FBS3hNLE1BQUwsQ0FBWTBhLE9BQWYsRUFBdUI7QUFBQ3BwQixvQkFBWSxDQUFDLEtBQUswTyxNQUFMLENBQVkwYSxPQUFiLENBQVo7QUFBbUM7O0FBQ3RyQixVQUFHLEtBQUt6bUIsT0FBTCxDQUFhdUosZUFBaEIsRUFBZ0M7QUFBQyxhQUFLSCxNQUFMLENBQVlrUCxPQUFaLEVBQW9CQyxPQUFwQjtBQUE4Qjs7QUFDL0QsV0FBS2lPLEdBQUwsR0FBU3B5QixTQUFUO0FBQW9CLEtBRnlkLE1BRXJkO0FBQUMsVUFBRyxLQUFLZ1AsT0FBTCxDQUFhK0ksV0FBYixJQUEwQixLQUFLbk0sT0FBTCxDQUFhMkgsVUFBdkMsSUFBbUQsQ0FBQyxLQUFLb0UsTUFBTCxDQUFZMkwsSUFBbkUsRUFBd0U7QUFBQyxhQUFLM0wsTUFBTCxDQUFZMGEsT0FBWixHQUFvQnZ2QixVQUFVLENBQUMsWUFBVTtBQUFDLGNBQUl5VyxNQUFNLEdBQUMsQ0FBQ2hPLEtBQUssQ0FBQ2dELFFBQU4sQ0FBZSxLQUFLdUksR0FBTCxDQUFTQyxNQUF4QixFQUErQixLQUFLTyxHQUFMLEdBQVMsT0FBeEMsQ0FBRCxHQUFrRCxLQUFsRCxHQUF3RCxRQUFuRTtBQUE0RS9MLGVBQUssQ0FBQ2dPLE1BQU0sR0FBQyxPQUFSLENBQUwsQ0FBc0IsS0FBS3pDLEdBQUwsQ0FBU0MsTUFBL0IsRUFBc0MsS0FBS08sR0FBTCxHQUFTLE9BQS9DO0FBQXlELFNBQWhKLENBQWlKaE8sSUFBakosQ0FBc0osSUFBdEosQ0FBRCxFQUE2SixHQUE3SixDQUE5QjtBQUFpTTs7QUFDblMsV0FBSzhvQixHQUFMLEdBQVM7QUFBQ3Z2QixhQUFLLEVBQUMsQ0FBQyxJQUFJaUcsSUFBSixFQUFSO0FBQW1Cc2QsY0FBTSxFQUFDOEwsT0FBTyxDQUFDLENBQUQsQ0FBUCxDQUFXaE8sT0FBckM7QUFBNkNMLGNBQU0sRUFBQ3FPLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBVy9OO0FBQS9ELE9BQVQ7QUFBa0Y7QUFBQyxHQUgwUTs7QUFHelEvVSxPQUFLLENBQUNraUIsY0FBTixHQUFxQixZQUFVO0FBQUMsUUFBRyxLQUFLL1osT0FBTCxDQUFhbFUsSUFBaEIsRUFBcUI7QUFBQyxhQUFPLEtBQVA7QUFBYzs7QUFDeEosUUFBSXdxQixHQUFHLEdBQUMsS0FBS3hKLEtBQUwsRUFBUjtBQUFBLFFBQXFCNU0sTUFBTSxHQUFDLEtBQUtBLE1BQWpDO0FBQUEsUUFBd0N4UCxLQUFLLEdBQUN3UCxNQUFNLENBQUN4UCxLQUFyRDtBQUFBLFFBQTJEcXFCLFFBQVEsR0FBQ3RuQixJQUFJLENBQUNpWixHQUFMLENBQVM0SixHQUFHLEdBQUMsS0FBSzVhLE1BQUwsQ0FBWWhDLFFBQVosQ0FBcUJyQyxDQUF6QixHQUEyQjZJLE1BQU0sQ0FBQ3JKLEtBQVAsR0FBYW5HLEtBQWpELElBQXdEQSxLQUF4RCxHQUE4RCxHQUFsSTtBQUFzSSxXQUFPcXFCLFFBQVEsSUFBRSxDQUFWLEdBQVksSUFBWixHQUFpQixLQUF4QjtBQUErQixHQURqRjs7QUFDa0ZsakIsT0FBSyxDQUFDK2lCLFVBQU4sR0FBaUIsVUFBUy91QixLQUFULEVBQWU7QUFBQyxXQUFPQSxLQUFLLENBQUM4dUIsT0FBTixHQUFjOXVCLEtBQUssQ0FBQ212QixjQUFwQixHQUFtQyxDQUFDbnZCLEtBQUQsQ0FBMUM7QUFBbUQsR0FBcEY7O0FBQXFGZ00sT0FBSyxDQUFDNGhCLFVBQU4sR0FBaUIsVUFBUzV0QixLQUFULEVBQWU7QUFBQyxRQUFJd1UsUUFBUSxHQUFDLEtBQUt1YSxVQUFMLENBQWdCL3VCLEtBQWhCLENBQWI7O0FBQW9DLFNBQUksSUFBSWhDLENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBQ3dXLFFBQVEsQ0FBQ3RXLE1BQXZCLEVBQThCRixDQUFDLEVBQS9CLEVBQWtDO0FBQUMsVUFBRyxLQUFLd1csUUFBTCxDQUFjdFcsTUFBZCxHQUFxQixDQUFyQixJQUF3QixDQUFDLFlBQUQsRUFBYyxNQUFkLEVBQXFCLFlBQXJCLEVBQW1Da08sT0FBbkMsQ0FBMkMsS0FBSytILE9BQUwsQ0FBYWxVLElBQXhELE1BQWdFLENBQUMsQ0FBNUYsRUFBOEY7QUFBQyxZQUFJb0ssRUFBRSxHQUFDbUssUUFBUSxDQUFDeFcsQ0FBRCxDQUFmO0FBQUEsWUFBbUI4SixFQUFFLEdBQUN1QyxFQUFFLENBQUMra0IsU0FBSCxLQUFleHlCLFNBQWYsR0FBeUJ5TixFQUFFLENBQUMra0IsU0FBNUIsR0FBc0Mva0IsRUFBRSxDQUFDZ2xCLFVBQS9EOztBQUEwRSxZQUFHLENBQUMsS0FBS0MsVUFBTCxDQUFnQnhuQixFQUFoQixDQUFKLEVBQXdCO0FBQUMsZUFBSzBNLFFBQUwsQ0FBYyxLQUFLQSxRQUFMLENBQWN0VyxNQUE1QixJQUFvQztBQUFDNEosY0FBRSxFQUFDQSxFQUFKO0FBQU8wRCxhQUFDLEVBQUM1RCxJQUFJLENBQUNvSCxLQUFMLENBQVczRSxFQUFFLENBQUN5VyxPQUFkLENBQVQ7QUFBZ0NyVixhQUFDLEVBQUM3RCxJQUFJLENBQUNvSCxLQUFMLENBQVczRSxFQUFFLENBQUMwVyxPQUFkO0FBQWxDLFdBQXBDO0FBQStGO0FBQUM7QUFBQztBQUFDLEdBQTVZOztBQUE2WS9VLE9BQUssQ0FBQytoQixhQUFOLEdBQW9CLFVBQVMvdEIsS0FBVCxFQUFlO0FBQUMsUUFBSXdVLFFBQVEsR0FBQyxLQUFLdWEsVUFBTCxDQUFnQi91QixLQUFoQixDQUFiOztBQUFvQyxTQUFJLElBQUloQyxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUN3VyxRQUFRLENBQUN0VyxNQUF2QixFQUE4QkYsQ0FBQyxFQUEvQixFQUFrQztBQUFDLFVBQUlxTSxFQUFFLEdBQUNtSyxRQUFRLENBQUN4VyxDQUFELENBQWY7QUFBQSxVQUFtQjhKLEVBQUUsR0FBQ3VDLEVBQUUsQ0FBQytrQixTQUFILEtBQWV4eUIsU0FBZixHQUF5QnlOLEVBQUUsQ0FBQytrQixTQUE1QixHQUFzQy9rQixFQUFFLENBQUNnbEIsVUFBL0Q7QUFBQSxVQUEwRUUsRUFBRSxHQUFDLEtBQUtELFVBQUwsQ0FBZ0J4bkIsRUFBaEIsQ0FBN0U7O0FBQWlHLFVBQUd5bkIsRUFBSCxFQUFNO0FBQUNBLFVBQUUsQ0FBQy9qQixDQUFILEdBQUs1RCxJQUFJLENBQUNvSCxLQUFMLENBQVczRSxFQUFFLENBQUN5VyxPQUFkLENBQUw7QUFBNEJ5TyxVQUFFLENBQUM5akIsQ0FBSCxHQUFLN0QsSUFBSSxDQUFDb0gsS0FBTCxDQUFXM0UsRUFBRSxDQUFDMFcsT0FBZCxDQUFMO0FBQTZCO0FBQUM7QUFBQyxHQUE5UTs7QUFBK1EvVSxPQUFLLENBQUMwaUIsYUFBTixHQUFvQixVQUFTMXVCLEtBQVQsRUFBZTtBQUFDLFFBQUl3VSxRQUFRLEdBQUMsS0FBS3VhLFVBQUwsQ0FBZ0IvdUIsS0FBaEIsQ0FBYjs7QUFBb0MsU0FBSSxJQUFJaEMsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDd1csUUFBUSxDQUFDdFcsTUFBdkIsRUFBOEJGLENBQUMsRUFBL0IsRUFBa0M7QUFBQyxVQUFJcU0sRUFBRSxHQUFDbUssUUFBUSxDQUFDeFcsQ0FBRCxDQUFmO0FBQUEsVUFBbUI4SixFQUFFLEdBQUN1QyxFQUFFLENBQUMra0IsU0FBSCxLQUFleHlCLFNBQWYsR0FBeUJ5TixFQUFFLENBQUMra0IsU0FBNUIsR0FBc0Mva0IsRUFBRSxDQUFDZ2xCLFVBQS9EOztBQUEwRSxXQUFJLElBQUl0bUIsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDLEtBQUt5TCxRQUFMLENBQWN0VyxNQUE1QixFQUFtQzZLLENBQUMsRUFBcEMsRUFBdUM7QUFBQyxZQUFHLEtBQUt5TCxRQUFMLENBQWN6TCxDQUFkLEVBQWlCakIsRUFBakIsS0FBc0JBLEVBQXpCLEVBQTRCO0FBQUMsZUFBSzBNLFFBQUwsQ0FBY25JLE1BQWQsQ0FBcUJ0RCxDQUFyQixFQUF1QixDQUF2QjtBQUEyQjtBQUFDO0FBQUM7QUFBQyxHQUF4Ujs7QUFBeVJpRCxPQUFLLENBQUNzakIsVUFBTixHQUFpQixVQUFTeG5CLEVBQVQsRUFBWTtBQUFDLFNBQUksSUFBSTBHLENBQVIsSUFBYSxLQUFLZ0csUUFBbEIsRUFBMkI7QUFBQyxVQUFHLEtBQUtBLFFBQUwsQ0FBY2hHLENBQWQsRUFBaUIxRyxFQUFqQixLQUFzQkEsRUFBekIsRUFBNEI7QUFBQyxlQUFPLEtBQUswTSxRQUFMLENBQWNoRyxDQUFkLENBQVA7QUFBeUI7QUFBQzs7QUFDanlDLFdBQU8sSUFBUDtBQUFhLEdBRG1xQzs7QUFDbHFDeEMsT0FBSyxDQUFDNmhCLFFBQU4sR0FBZSxVQUFTNXRCLElBQVQsRUFBYztBQUFDLFFBQUlLLENBQUMsR0FBQyxLQUFLNlQsT0FBWDtBQUFBLFFBQW1CSyxRQUFRLEdBQUMsS0FBS0EsUUFBakM7QUFBQSxRQUEwQzNGLFFBQTFDOztBQUFtRCxRQUFHLENBQUMyRixRQUFRLENBQUN0VyxNQUFiLEVBQW9CO0FBQUM7QUFBUTs7QUFDNUhvQyxLQUFDLENBQUM0RCxTQUFGLEdBQVk1RCxDQUFDLENBQUNrTCxDQUFGLEdBQUlnSixRQUFRLENBQUMsQ0FBRCxDQUFSLENBQVloSixDQUFaLEdBQWNsTCxDQUFDLENBQUNrTCxDQUFoQixHQUFrQixDQUFsQixHQUFvQixDQUFDLENBQXpCLEdBQTJCLENBQXZDO0FBQXlDbEwsS0FBQyxDQUFDa0wsQ0FBRixHQUFJZ0osUUFBUSxDQUFDLENBQUQsQ0FBUixDQUFZaEosQ0FBaEI7QUFBa0JsTCxLQUFDLENBQUNtTCxDQUFGLEdBQUkrSSxRQUFRLENBQUMsQ0FBRCxDQUFSLENBQVkvSSxDQUFoQjs7QUFBa0IsUUFBRytJLFFBQVEsQ0FBQ3RXLE1BQVQsS0FBa0IsQ0FBckIsRUFBdUI7QUFBQyxVQUFJc3hCLEVBQUUsR0FBQ2hiLFFBQVEsQ0FBQyxDQUFELENBQVIsQ0FBWWhKLENBQW5CO0FBQUEsVUFBcUJpa0IsRUFBRSxHQUFDamIsUUFBUSxDQUFDLENBQUQsQ0FBUixDQUFZL0ksQ0FBcEM7QUFBc0NvRCxjQUFRLEdBQUMsS0FBSzZnQixXQUFMLENBQWlCLENBQUNwdkIsQ0FBQyxDQUFDa0wsQ0FBSCxFQUFLbEwsQ0FBQyxDQUFDbUwsQ0FBUCxDQUFqQixFQUEyQixDQUFDK2pCLEVBQUQsRUFBSUMsRUFBSixDQUEzQixDQUFUO0FBQTZDbnZCLE9BQUMsQ0FBQ2tMLENBQUYsR0FBSWxMLENBQUMsQ0FBQ2tMLENBQUYsR0FBSSxDQUFDbEwsQ0FBQyxDQUFDa0wsQ0FBRixHQUFJZ2tCLEVBQUwsSUFBUyxDQUFqQjtBQUFtQmx2QixPQUFDLENBQUNtTCxDQUFGLEdBQUluTCxDQUFDLENBQUNtTCxDQUFGLEdBQUksQ0FBQ25MLENBQUMsQ0FBQ21MLENBQUYsR0FBSWdrQixFQUFMLElBQVMsQ0FBakI7QUFBb0I7O0FBQy9OLFFBQUd4dkIsSUFBSSxLQUFHLE9BQVYsRUFBa0I7QUFBQ0ssT0FBQyxDQUFDOHRCLEVBQUYsR0FBSyxDQUFMO0FBQU85dEIsT0FBQyxDQUFDK3RCLEVBQUYsR0FBSyxDQUFMO0FBQU8vdEIsT0FBQyxDQUFDZ3VCLEVBQUYsR0FBS2h1QixDQUFDLENBQUNrTCxDQUFQO0FBQVNsTCxPQUFDLENBQUNpdUIsRUFBRixHQUFLanVCLENBQUMsQ0FBQ21MLENBQVA7QUFBU25MLE9BQUMsQ0FBQ3VPLFFBQUYsR0FBV0EsUUFBUSxHQUFDQSxRQUFELEdBQVUsQ0FBN0I7QUFBZ0MsS0FBbkYsTUFBdUY7QUFBQ3ZPLE9BQUMsQ0FBQzh0QixFQUFGLEdBQUs5dEIsQ0FBQyxDQUFDa0wsQ0FBRixHQUFJbEwsQ0FBQyxDQUFDZ3VCLEVBQVg7QUFBY2h1QixPQUFDLENBQUMrdEIsRUFBRixHQUFLL3RCLENBQUMsQ0FBQ21MLENBQUYsR0FBSW5MLENBQUMsQ0FBQ2l1QixFQUFYO0FBQWNqdUIsT0FBQyxDQUFDcUwsS0FBRixHQUFRa0QsUUFBUSxJQUFFdk8sQ0FBQyxDQUFDdU8sUUFBWixHQUFxQkEsUUFBUSxHQUFDdk8sQ0FBQyxDQUFDdU8sUUFBaEMsR0FBeUMsQ0FBakQ7QUFBb0Q7QUFBQyxHQUYzSjs7QUFFNEo3QyxPQUFLLENBQUMwakIsV0FBTixHQUFrQixVQUFTQyxFQUFULEVBQVlDLEVBQVosRUFBZTtBQUFDLFFBQUlwa0IsQ0FBQyxHQUFDb2tCLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBTUQsRUFBRSxDQUFDLENBQUQsQ0FBZDtBQUFBLFFBQWtCbGtCLENBQUMsR0FBQ21rQixFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQU1ELEVBQUUsQ0FBQyxDQUFELENBQTVCO0FBQWdDLFdBQU8vbkIsSUFBSSxDQUFDaW9CLElBQUwsQ0FBV3JrQixDQUFDLEdBQUNBLENBQUgsR0FBT0MsQ0FBQyxHQUFDQSxDQUFuQixDQUFQO0FBQStCLEdBQWpHOztBQUFrR08sT0FBSyxDQUFDOGpCLElBQU4sR0FBVyxZQUFVO0FBQUMsUUFBSWpCLE1BQU0sR0FBQyxLQUFLbE8sT0FBTCxFQUFYO0FBQTBCa08sVUFBTSxDQUFDdmhCLFlBQVA7QUFBc0J1aEIsVUFBTSxDQUFDNWhCLFVBQVAsQ0FBa0I7QUFBQ3pCLE9BQUMsRUFBQ3FqQixNQUFNLENBQUNoaEIsUUFBUCxDQUFnQnJDLENBQW5CO0FBQXFCQyxPQUFDLEVBQUNvakIsTUFBTSxDQUFDamhCLEtBQVAsQ0FBYW5DLENBQWIsR0FBZSxLQUFLMEksT0FBTCxDQUFha2EsRUFBbkQ7QUFBc0QzaUIsT0FBQyxFQUFDbWpCLE1BQU0sQ0FBQ2hoQixRQUFQLENBQWdCbkM7QUFBeEUsS0FBbEI7QUFBK0YsR0FBcks7O0FBQXNLTSxPQUFLLENBQUMrakIsT0FBTixHQUFjLFlBQVU7QUFBQyxRQUFJQyxJQUFJLEdBQUMsQ0FBVDtBQUFBLFFBQVduQixNQUFNLEdBQUMsS0FBS2xPLE9BQUwsRUFBbEI7QUFBQSxRQUFpQzdiLE1BQU0sR0FBQyxLQUFLK0ssTUFBTCxDQUFZL0ssTUFBcEQ7QUFBQSxRQUEyRGlKLE9BQU8sR0FBQzhnQixNQUFNLENBQUM5Z0IsT0FBUCxDQUFldEMsQ0FBbEY7O0FBQW9GLFFBQUcsSUFBRTdELElBQUksQ0FBQ2laLEdBQUwsQ0FBUzlTLE9BQVQsS0FBbUJqSixNQUFNLEdBQUMsR0FBMUIsQ0FBRixHQUFpQyxHQUFwQyxFQUF3QztBQUFDa3JCLFVBQUksR0FBQ3BvQixJQUFJLENBQUNpWixHQUFMLENBQVM5UyxPQUFULElBQWtCakosTUFBTSxHQUFDLEdBQXpCLEdBQTZCOEMsSUFBSSxDQUFDaVosR0FBTCxDQUFTOVMsT0FBVCxJQUFrQkEsT0FBbEIsR0FBMEJqSixNQUExQixHQUFpQyxHQUE5RCxHQUFrRWlKLE9BQXZFO0FBQStFLFdBQUsrUSxLQUFMO0FBQWErUCxZQUFNLENBQUN4aEIsU0FBUCxDQUFpQjtBQUFDN0IsU0FBQyxFQUFDLENBQUg7QUFBS0MsU0FBQyxFQUFDdWtCLElBQVA7QUFBWXRrQixTQUFDLEVBQUNza0IsSUFBSSxHQUFDbkIsTUFBTSxDQUFDOWdCLE9BQVAsQ0FBZXJDLENBQWhCLEdBQWtCO0FBQXBDLE9BQWpCO0FBQXlEbWpCLFlBQU0sQ0FBQ3ZoQixZQUFQO0FBQXNCdWhCLFlBQU0sQ0FBQ3poQixXQUFQO0FBQXNCO0FBQUMsR0FBeFY7O0FBQXlWcEIsT0FBSyxDQUFDaWtCLE9BQU4sR0FBYyxZQUFVO0FBQUMsUUFBSXBCLE1BQU0sR0FBQyxLQUFLbE8sT0FBTCxFQUFYO0FBQUEsUUFBMEJ4TSxPQUFPLEdBQUMsS0FBS0EsT0FBdkM7QUFBQSxRQUErQ3NZLEtBQUssR0FBQyxLQUFLdEMsYUFBTCxDQUFtQjBFLE1BQU0sQ0FBQ2hoQixRQUFQLENBQWdCbkMsQ0FBbkMsQ0FBckQ7QUFBQSxRQUEyRm1HLE9BQU8sR0FBQyxLQUFLckosT0FBTCxDQUFhNEosWUFBYixJQUEyQitCLE9BQU8sQ0FBQ2taLFFBQW5DLEdBQTRDLEdBQTVDLEdBQWdELEdBQW5KO0FBQUEsUUFBdUoxaEIsS0FBSyxHQUFDL0QsSUFBSSxDQUFDZ1osR0FBTCxDQUFTLEtBQUtwWSxPQUFMLENBQWFzSixPQUFiLEdBQXFCLEdBQTlCLEVBQWtDbEssSUFBSSxDQUFDQyxHQUFMLENBQVNnSyxPQUFULEVBQWlCZ2QsTUFBTSxDQUFDamhCLEtBQVAsQ0FBYWxDLENBQWIsR0FBZXlJLE9BQU8sQ0FBQ3hJLEtBQXhDLENBQWxDLENBQTdKO0FBQUEsUUFBK09taEIsSUFBSSxHQUFDK0IsTUFBTSxDQUFDamhCLEtBQVAsQ0FBYXBDLENBQWIsR0FBZTJJLE9BQU8sQ0FBQ2lhLEVBQTNRO0FBQUEsUUFBOFE0QixJQUFJLEdBQUNuQixNQUFNLENBQUNqaEIsS0FBUCxDQUFhbkMsQ0FBYixHQUFlMEksT0FBTyxDQUFDa2EsRUFBMVM7QUFBQSxRQUE2UzZCLE9BQU8sR0FBQy9iLE9BQU8sQ0FBQ21hLEVBQVIsR0FBVyxLQUFLemUsTUFBTCxDQUFZaEwsS0FBWixHQUFrQixHQUFsVjtBQUFBLFFBQXNWc3JCLE9BQU8sR0FBQ2hjLE9BQU8sQ0FBQ29hLEVBQVIsR0FBVyxLQUFLMWUsTUFBTCxDQUFZL0ssTUFBWixHQUFtQixHQUE1WDs7QUFBZ1ksUUFBRyxDQUFDcVAsT0FBTyxDQUFDcWEsT0FBVCxJQUFtQixDQUFDLEtBQUtobUIsT0FBTCxDQUFheUosV0FBZCxJQUEyQixDQUFDLEtBQUtzQyxNQUFMLENBQVkwSSxJQUE5RCxFQUFvRTtBQUFDdFIsV0FBSyxHQUFDL0QsSUFBSSxDQUFDZ1osR0FBTCxDQUFTLENBQVQsRUFBV2pWLEtBQVgsQ0FBTjtBQUF5Qjs7QUFDbHdDLFFBQUcsQ0FBQyxLQUFLbkQsT0FBTCxDQUFheUosV0FBZCxJQUEyQixLQUFLc0MsTUFBTCxDQUFZMEksSUFBMUMsRUFBK0M7QUFBQ3RSLFdBQUssR0FBQ2tqQixNQUFNLENBQUNoaEIsUUFBUCxDQUFnQm5DLENBQXRCO0FBQXlCOztBQUN6RSxRQUFHLENBQUN5SSxPQUFPLENBQUNqSCxJQUFULElBQWUsS0FBS3NILFFBQUwsQ0FBY3RXLE1BQWQsS0FBdUIsQ0FBekMsRUFBMkM7QUFBQzJ3QixZQUFNLENBQUNqaEIsS0FBUCxDQUFhcEMsQ0FBYixJQUFnQnNoQixJQUFJLEdBQUNMLEtBQUssQ0FBQzluQixJQUFYLEdBQWdCbW9CLElBQUksR0FBQ0wsS0FBSyxDQUFDOW5CLElBQTNCLEdBQWdDbW9CLElBQUksR0FBQ0wsS0FBSyxDQUFDN25CLEtBQVgsR0FBaUJrb0IsSUFBSSxHQUFDTCxLQUFLLENBQUM3bkIsS0FBNUIsR0FBa0MsQ0FBbEY7QUFBb0ZpcUIsWUFBTSxDQUFDamhCLEtBQVAsQ0FBYW5DLENBQWIsSUFBZ0J1a0IsSUFBSSxHQUFDdkQsS0FBSyxDQUFDam9CLE1BQVgsR0FBa0J3ckIsSUFBSSxHQUFDdkQsS0FBSyxDQUFDam9CLE1BQTdCLEdBQW9Dd3JCLElBQUksR0FBQ3ZELEtBQUssQ0FBQ2xvQixHQUFYLEdBQWV5ckIsSUFBSSxHQUFDdkQsS0FBSyxDQUFDbG9CLEdBQTFCLEdBQThCLENBQWxGO0FBQXFGOztBQUNyTnVvQixRQUFJLEdBQUMzWSxPQUFPLENBQUNpYSxFQUFSLEdBQVc4QixPQUFYLEdBQW1CLENBQUNyQixNQUFNLENBQUNqaEIsS0FBUCxDQUFhcEMsQ0FBYixHQUFlMGtCLE9BQWhCLEtBQTBCdmtCLEtBQUssR0FBQ2tqQixNQUFNLENBQUNqaEIsS0FBUCxDQUFhbEMsQ0FBN0MsQ0FBeEI7QUFBd0Vza0IsUUFBSSxHQUFDN2IsT0FBTyxDQUFDa2EsRUFBUixHQUFXOEIsT0FBWCxHQUFtQixDQUFDdEIsTUFBTSxDQUFDamhCLEtBQVAsQ0FBYW5DLENBQWIsR0FBZTBrQixPQUFoQixLQUEwQnhrQixLQUFLLEdBQUNrakIsTUFBTSxDQUFDamhCLEtBQVAsQ0FBYWxDLENBQTdDLENBQXhCOztBQUF3RSxRQUFHLEtBQUs4SSxRQUFMLENBQWN0VyxNQUFkLEtBQXVCLENBQTFCLEVBQTRCO0FBQUM0dUIsVUFBSSxHQUFDQSxJQUFJLEdBQUNMLEtBQUssQ0FBQzluQixJQUFYLEdBQWdCLENBQUNtb0IsSUFBSSxHQUFDTCxLQUFLLENBQUM5bkIsSUFBWixJQUFrQixHQUFsQyxHQUFzQ21vQixJQUFJLEdBQUNMLEtBQUssQ0FBQzduQixLQUFYLEdBQWlCLENBQUNrb0IsSUFBSSxHQUFDTCxLQUFLLENBQUM3bkIsS0FBWixJQUFtQixHQUFwQyxHQUF3Q2tvQixJQUFuRjtBQUF3RmtELFVBQUksR0FBQ0EsSUFBSSxHQUFDdkQsS0FBSyxDQUFDam9CLE1BQVgsR0FBa0IsQ0FBQ3dyQixJQUFJLEdBQUN2RCxLQUFLLENBQUNqb0IsTUFBWixJQUFvQixHQUF0QyxHQUEwQ3dyQixJQUFJLEdBQUN2RCxLQUFLLENBQUNsb0IsR0FBWCxHQUFlLENBQUN5ckIsSUFBSSxHQUFDdkQsS0FBSyxDQUFDbG9CLEdBQVosSUFBaUIsR0FBaEMsR0FBb0N5ckIsSUFBbkY7QUFBeUY7O0FBQzlWbkIsVUFBTSxDQUFDdmhCLFlBQVA7QUFBc0J1aEIsVUFBTSxDQUFDNWhCLFVBQVAsQ0FBa0I7QUFBQ3pCLE9BQUMsRUFBQ3NoQixJQUFIO0FBQVFyaEIsT0FBQyxFQUFDdWtCLElBQVY7QUFBZXRrQixPQUFDLEVBQUNDO0FBQWpCLEtBQWxCO0FBQTJDLFNBQUt5a0IsVUFBTCxDQUFnQnprQixLQUFoQjtBQUF3QixHQUprckI7O0FBSWpyQkssT0FBSyxDQUFDcWtCLFVBQU4sR0FBaUIsWUFBVTtBQUFDLFFBQUl4QixNQUFNLEdBQUMsS0FBS2xPLE9BQUwsRUFBWDtBQUFBLFFBQTBCeE0sT0FBTyxHQUFDLEtBQUtBLE9BQXZDO0FBQUEsUUFBK0N4SSxLQUFLLEdBQUNrakIsTUFBTSxDQUFDOWdCLE9BQVAsQ0FBZXJDLENBQWYsR0FBaUIsS0FBS2xELE9BQUwsQ0FBYXNKLE9BQTlCLEdBQXNDLEtBQUt0SixPQUFMLENBQWFzSixPQUFuRCxHQUEyRCtjLE1BQU0sQ0FBQzlnQixPQUFQLENBQWVyQyxDQUFmLEdBQWlCLENBQWpCLEdBQW1CLENBQW5CLEdBQXFCbWpCLE1BQU0sQ0FBQzlnQixPQUFQLENBQWVyQyxDQUFwSjtBQUFBLFFBQXNKK2dCLEtBQUssR0FBQyxLQUFLdEMsYUFBTCxDQUFtQnhlLEtBQW5CLENBQTVKO0FBQUEsUUFBc0xtaEIsSUFBdEw7QUFBQSxRQUEyTGtELElBQTNMOztBQUFnTSxRQUFHcG9CLElBQUksQ0FBQ29ILEtBQUwsQ0FBVzZmLE1BQU0sQ0FBQzlnQixPQUFQLENBQWVyQyxDQUFmLEdBQWlCLEVBQTVCLElBQWdDLEVBQWhDLEdBQW1DLEtBQUtsRCxPQUFMLENBQWFzSixPQUFuRCxFQUEyRDtBQUFDLFVBQUlvZSxPQUFPLEdBQUMvYixPQUFPLENBQUN0RixRQUFSLEdBQWlCc0YsT0FBTyxDQUFDbWEsRUFBUixHQUFXLEtBQUt6ZSxNQUFMLENBQVloTCxLQUFaLEdBQWtCLEdBQTlDLEdBQWtELENBQTlEO0FBQWdFLFVBQUlzckIsT0FBTyxHQUFDaGMsT0FBTyxDQUFDdEYsUUFBUixHQUFpQnNGLE9BQU8sQ0FBQ29hLEVBQVIsR0FBVyxLQUFLMWUsTUFBTCxDQUFZL0ssTUFBWixHQUFtQixHQUEvQyxHQUFtRCxDQUEvRDtBQUFpRWdvQixVQUFJLEdBQUMzWSxPQUFPLENBQUNpYSxFQUFSLEdBQVc4QixPQUFYLEdBQW1CLENBQUNyQixNQUFNLENBQUNqaEIsS0FBUCxDQUFhcEMsQ0FBYixHQUFlMGtCLE9BQWhCLEtBQTBCdmtCLEtBQUssR0FBQ2tqQixNQUFNLENBQUNqaEIsS0FBUCxDQUFhbEMsQ0FBN0MsQ0FBeEI7QUFBd0Vza0IsVUFBSSxHQUFDN2IsT0FBTyxDQUFDa2EsRUFBUixHQUFXOEIsT0FBWCxHQUFtQixDQUFDdEIsTUFBTSxDQUFDamhCLEtBQVAsQ0FBYW5DLENBQWIsR0FBZTBrQixPQUFoQixLQUEwQnhrQixLQUFLLEdBQUNrakIsTUFBTSxDQUFDamhCLEtBQVAsQ0FBYWxDLENBQTdDLENBQXhCO0FBQXdFb2hCLFVBQUksR0FBQ0EsSUFBSSxHQUFDTCxLQUFLLENBQUM5bkIsSUFBWCxHQUFnQjhuQixLQUFLLENBQUM5bkIsSUFBdEIsR0FBMkJtb0IsSUFBSSxHQUFDTCxLQUFLLENBQUM3bkIsS0FBWCxHQUFpQjZuQixLQUFLLENBQUM3bkIsS0FBdkIsR0FBNkJrb0IsSUFBN0Q7QUFBa0VrRCxVQUFJLEdBQUNBLElBQUksR0FBQ3ZELEtBQUssQ0FBQ2pvQixNQUFYLEdBQWtCaW9CLEtBQUssQ0FBQ2pvQixNQUF4QixHQUErQndyQixJQUFJLEdBQUN2RCxLQUFLLENBQUNsb0IsR0FBWCxHQUFla29CLEtBQUssQ0FBQ2xvQixHQUFyQixHQUF5QnlyQixJQUE3RDtBQUFtRSxLQUFsZCxNQUFzZDtBQUFDbEQsVUFBSSxHQUFDK0IsTUFBTSxDQUFDOWdCLE9BQVAsQ0FBZXZDLENBQWYsR0FBaUJpaEIsS0FBSyxDQUFDOW5CLElBQXZCLEdBQTRCOG5CLEtBQUssQ0FBQzluQixJQUFsQyxHQUF1Q2txQixNQUFNLENBQUM5Z0IsT0FBUCxDQUFldkMsQ0FBZixHQUFpQmloQixLQUFLLENBQUM3bkIsS0FBdkIsR0FBNkI2bkIsS0FBSyxDQUFDN25CLEtBQW5DLEdBQXlDaEksU0FBckY7QUFBK0ZvekIsVUFBSSxHQUFDbkIsTUFBTSxDQUFDOWdCLE9BQVAsQ0FBZXRDLENBQWYsR0FBaUJnaEIsS0FBSyxDQUFDam9CLE1BQXZCLEdBQThCaW9CLEtBQUssQ0FBQ2pvQixNQUFwQyxHQUEyQ3FxQixNQUFNLENBQUM5Z0IsT0FBUCxDQUFldEMsQ0FBZixHQUFpQmdoQixLQUFLLENBQUNsb0IsR0FBdkIsR0FBMkJrb0IsS0FBSyxDQUFDbG9CLEdBQWpDLEdBQXFDM0gsU0FBckY7QUFBZ0c7O0FBQzU4QixRQUFHLEtBQUs0TCxPQUFMLENBQWE0SixZQUFiLElBQTJCeWMsTUFBTSxDQUFDOWdCLE9BQVAsQ0FBZXJDLENBQWYsR0FBaUIsR0FBNUMsSUFBaUR5SSxPQUFPLENBQUNrWixRQUE1RCxFQUFxRTtBQUFDMWhCLFdBQUssR0FBQ2tqQixNQUFNLENBQUM5Z0IsT0FBUCxDQUFlckMsQ0FBZixHQUFpQixHQUFqQixHQUFxQm1qQixNQUFNLENBQUM5Z0IsT0FBUCxDQUFlckMsQ0FBcEMsR0FBc0MsSUFBNUM7QUFBaURvaEIsVUFBSSxHQUFDK0IsTUFBTSxDQUFDOWdCLE9BQVAsQ0FBZXZDLENBQXBCO0FBQXNCd2tCLFVBQUksR0FBQ25CLE1BQU0sQ0FBQzlnQixPQUFQLENBQWV0QyxDQUFwQjtBQUFzQixXQUFLcVQsS0FBTDtBQUFjOztBQUNqTCtQLFVBQU0sQ0FBQ3hoQixTQUFQLENBQWlCO0FBQUM3QixPQUFDLEVBQUNzaEIsSUFBSDtBQUFRcmhCLE9BQUMsRUFBQ3VrQixJQUFWO0FBQWV0a0IsT0FBQyxFQUFDQyxLQUFLLEtBQUdrakIsTUFBTSxDQUFDOWdCLE9BQVAsQ0FBZXJDLENBQXZCLEdBQXlCQyxLQUF6QixHQUErQi9PO0FBQWhELEtBQWpCO0FBQTZFaXlCLFVBQU0sQ0FBQ3ZoQixZQUFQO0FBQXNCdWhCLFVBQU0sQ0FBQ3poQixXQUFQO0FBQXFCLFNBQUtnakIsVUFBTCxDQUFnQnZCLE1BQU0sQ0FBQzlnQixPQUFQLENBQWVyQyxDQUEvQjtBQUFtQyxHQUZqRTs7QUFFa0VNLE9BQUssQ0FBQ3NrQixVQUFOLEdBQWlCLFlBQVU7QUFBQyxRQUFJekIsTUFBTSxHQUFDLEtBQUs5ZSxNQUFoQjtBQUFBLFFBQXVCMGMsS0FBSyxHQUFDb0MsTUFBTSxDQUFDcEMsS0FBcEM7QUFBQSxRQUEwQ0ssSUFBSSxHQUFDK0IsTUFBTSxDQUFDamhCLEtBQVAsQ0FBYXBDLENBQWIsR0FBZSxLQUFLMkksT0FBTCxDQUFhaWEsRUFBM0U7O0FBQThFLFFBQUcsQ0FBQyxLQUFLamEsT0FBTCxDQUFhakgsSUFBakIsRUFBc0I7QUFBQzJoQixZQUFNLENBQUNqaEIsS0FBUCxDQUFhcEMsQ0FBYixJQUFnQnNoQixJQUFJLEdBQUNMLEtBQUssQ0FBQzluQixJQUFYLEdBQWdCbW9CLElBQUksR0FBQ0wsS0FBSyxDQUFDOW5CLElBQTNCLEdBQWdDbW9CLElBQUksR0FBQ0wsS0FBSyxDQUFDN25CLEtBQVgsR0FBaUJrb0IsSUFBSSxHQUFDTCxLQUFLLENBQUM3bkIsS0FBNUIsR0FBa0MsQ0FBbEY7QUFBb0Zrb0IsVUFBSSxHQUFDK0IsTUFBTSxDQUFDamhCLEtBQVAsQ0FBYXBDLENBQWIsR0FBZSxLQUFLMkksT0FBTCxDQUFhaWEsRUFBakM7QUFBcUM7O0FBQ3RadEIsUUFBSSxHQUFDQSxJQUFJLEdBQUNMLEtBQUssQ0FBQzluQixJQUFYLEdBQWdCLENBQUNtb0IsSUFBSSxHQUFDTCxLQUFLLENBQUM5bkIsSUFBWixJQUFrQixHQUFsQyxHQUFzQ21vQixJQUFJLEdBQUNMLEtBQUssQ0FBQzduQixLQUFYLEdBQWlCLENBQUNrb0IsSUFBSSxHQUFDTCxLQUFLLENBQUM3bkIsS0FBWixJQUFtQixHQUFwQyxHQUF3Q2tvQixJQUFuRjtBQUF3RitCLFVBQU0sQ0FBQ3ZoQixZQUFQO0FBQXNCdWhCLFVBQU0sQ0FBQ2ppQixVQUFQLENBQWtCcEIsQ0FBbEIsR0FBb0I1TyxTQUFwQjtBQUE4Qml5QixVQUFNLENBQUM1aEIsVUFBUCxDQUFrQjtBQUFDekIsT0FBQyxFQUFDc2hCO0FBQUgsS0FBbEI7QUFBNkIsR0FEYjs7QUFDYzlnQixPQUFLLENBQUN1a0IsYUFBTixHQUFvQixZQUFVO0FBQUMsUUFBSTFCLE1BQU0sR0FBQyxLQUFLOWUsTUFBaEI7QUFBQSxRQUF1QjBjLEtBQUssR0FBQ29DLE1BQU0sQ0FBQ3BDLEtBQXBDO0FBQUEsUUFBMENLLElBQUksR0FBQytCLE1BQU0sQ0FBQzlnQixPQUFQLENBQWV2QyxDQUE5RDtBQUFnRXNoQixRQUFJLEdBQUNBLElBQUksR0FBQ0wsS0FBSyxDQUFDOW5CLElBQVgsR0FBZ0I4bkIsS0FBSyxDQUFDOW5CLElBQXRCLEdBQTJCbW9CLElBQUksR0FBQ0wsS0FBSyxDQUFDN25CLEtBQVgsR0FBaUI2bkIsS0FBSyxDQUFDN25CLEtBQXZCLEdBQTZCa29CLElBQTdEOztBQUFrRSxRQUFHQSxJQUFJLEtBQUcrQixNQUFNLENBQUM5Z0IsT0FBUCxDQUFldkMsQ0FBekIsRUFBMkI7QUFBQ3FqQixZQUFNLENBQUN4aEIsU0FBUCxDQUFpQjtBQUFDN0IsU0FBQyxFQUFDc2hCO0FBQUgsT0FBakI7QUFBNEI7O0FBQ25ZK0IsVUFBTSxDQUFDdmhCLFlBQVA7QUFBc0J1aEIsVUFBTSxDQUFDemhCLFdBQVA7QUFBc0IsR0FEOEg7O0FBQzdIcEIsT0FBSyxDQUFDd2tCLFVBQU4sR0FBaUIsWUFBVTtBQUFDLFFBQUcsS0FBSzFXLE9BQUwsQ0FBYTViLE1BQWIsS0FBc0IsQ0FBekIsRUFBMkI7QUFBQztBQUFROztBQUM3RyxRQUFJMndCLE1BQU0sR0FBQyxLQUFLaGYsTUFBaEI7QUFBQSxRQUF1QmlkLElBQUksR0FBQytCLE1BQU0sQ0FBQ2poQixLQUFQLENBQWFwQyxDQUFiLEdBQWUsS0FBSzJJLE9BQUwsQ0FBYWlhLEVBQXhEOztBQUEyRCxRQUFHLENBQUMsS0FBSzdaLE1BQUwsQ0FBWXZHLElBQWhCLEVBQXFCO0FBQUMsVUFBSXllLEtBQUssR0FBQ29DLE1BQU0sQ0FBQ3BDLEtBQWpCOztBQUF1QixVQUFHLENBQUMsS0FBS3RZLE9BQUwsQ0FBYWpILElBQWpCLEVBQXNCO0FBQUMyaEIsY0FBTSxDQUFDamhCLEtBQVAsQ0FBYXBDLENBQWIsSUFBZ0JzaEIsSUFBSSxHQUFDTCxLQUFLLENBQUM5bkIsSUFBWCxHQUFnQm1vQixJQUFJLEdBQUNMLEtBQUssQ0FBQzluQixJQUEzQixHQUFnQ21vQixJQUFJLEdBQUNMLEtBQUssQ0FBQzduQixLQUFYLEdBQWlCa29CLElBQUksR0FBQ0wsS0FBSyxDQUFDN25CLEtBQTVCLEdBQWtDLENBQWxGO0FBQW9Ga29CLFlBQUksR0FBQytCLE1BQU0sQ0FBQ2poQixLQUFQLENBQWFwQyxDQUFiLEdBQWUsS0FBSzJJLE9BQUwsQ0FBYWlhLEVBQWpDO0FBQXFDOztBQUN4UHRCLFVBQUksR0FBQ0EsSUFBSSxHQUFDTCxLQUFLLENBQUM5bkIsSUFBWCxHQUFnQixDQUFDbW9CLElBQUksR0FBQ0wsS0FBSyxDQUFDOW5CLElBQVosSUFBa0IsR0FBbEMsR0FBc0Ntb0IsSUFBSSxHQUFDTCxLQUFLLENBQUM3bkIsS0FBWCxHQUFpQixDQUFDa29CLElBQUksR0FBQ0wsS0FBSyxDQUFDN25CLEtBQVosSUFBbUIsR0FBcEMsR0FBd0Nrb0IsSUFBbkY7QUFBeUY7O0FBQ3pGK0IsVUFBTSxDQUFDdmhCLFlBQVA7QUFBc0J1aEIsVUFBTSxDQUFDNWhCLFVBQVAsQ0FBa0I7QUFBQ3pCLE9BQUMsRUFBQ3NoQjtBQUFILEtBQWxCO0FBQTZCLEdBSE47O0FBR085Z0IsT0FBSyxDQUFDeWtCLGFBQU4sR0FBb0IsWUFBVTtBQUFDLFFBQUcsS0FBSzNXLE9BQUwsQ0FBYTViLE1BQWIsS0FBc0IsQ0FBekIsRUFBMkI7QUFBQztBQUFROztBQUN2SCxRQUFJMndCLE1BQU0sR0FBQyxLQUFLaGYsTUFBaEI7QUFBQSxRQUF1QndFLE1BQU0sR0FBQyxLQUFLQSxNQUFuQztBQUFBLFFBQTBDcWMsUUFBUSxHQUFDcmMsTUFBTSxDQUFDckosS0FBMUQ7QUFBQSxRQUFnRXlmLEdBQUcsR0FBQyxLQUFLeEosS0FBTCxFQUFwRTtBQUFBLFFBQWlGMFAsUUFBUSxHQUFDOUIsTUFBTSxDQUFDOWdCLE9BQVAsQ0FBZXZDLENBQXpHO0FBQUEsUUFBMkdvbEIsU0FBUyxHQUFDL0IsTUFBTSxDQUFDaGhCLFFBQVAsQ0FBZ0JyQyxDQUFySTtBQUF1SSxTQUFLcWxCLGVBQUwsQ0FBcUJELFNBQXJCLEVBQStCRCxRQUEvQjs7QUFBeUMsUUFBR0QsUUFBUSxLQUFHcmMsTUFBTSxDQUFDckosS0FBckIsRUFBMkI7QUFBQyxXQUFLZ1QsZUFBTDtBQUF3Qjs7QUFDcE8sU0FBS25PLE1BQUwsQ0FBWXhDLFNBQVosQ0FBc0I7QUFBQzdCLE9BQUMsRUFBQyxDQUFDaWYsR0FBRCxHQUFLcFcsTUFBTSxDQUFDckosS0FBWixHQUFrQnFKLE1BQU0sQ0FBQ3hQLEtBQTVCO0FBQWtDNEcsT0FBQyxFQUFDN08sU0FBcEM7QUFBOEM4TyxPQUFDLEVBQUM5TztBQUFoRCxLQUF0QjtBQUFrRml5QixVQUFNLENBQUN2aEIsWUFBUDtBQUFzQnVoQixVQUFNLENBQUN6aEIsV0FBUDtBQUFzQixHQUYxRTs7QUFFMkVwQixPQUFLLENBQUM2a0IsZUFBTixHQUFzQixVQUFTRCxTQUFULEVBQW1CRCxRQUFuQixFQUE0QjtBQUFDLFFBQUl6c0IsU0FBUyxHQUFDLEtBQUtpUSxPQUFMLENBQWFqUSxTQUEzQjtBQUFBLFFBQXFDNFYsT0FBTyxHQUFDLEtBQUtBLE9BQWxEO0FBQUEsUUFBMER6RixNQUFNLEdBQUMsS0FBS0EsTUFBdEU7QUFBQSxRQUE2RTJPLE1BQU0sR0FBQyxLQUFLN08sT0FBTCxDQUFhaWEsRUFBakc7QUFBQSxRQUFvRzNELEdBQUcsR0FBQyxLQUFLeEosS0FBTCxFQUF4RztBQUFBLFFBQXFIalcsS0FBSyxHQUFDcEQsSUFBSSxDQUFDb0gsS0FBTCxDQUFXLENBQUN5YixHQUFELEdBQUttRyxTQUFMLEdBQWV2YyxNQUFNLENBQUN4UCxLQUFqQyxDQUEzSDtBQUFBLFFBQW1LaXNCLEtBQUssR0FBQ2xwQixJQUFJLENBQUNpWixHQUFMLENBQVM4UCxRQUFRLEdBQUNDLFNBQWxCLENBQXpLOztBQUFzTSxRQUFHaHBCLElBQUksQ0FBQ2laLEdBQUwsQ0FBU21DLE1BQVQsSUFBaUIzTyxNQUFNLENBQUN4UCxLQUFQLEdBQWEsR0FBOUIsSUFBbUNpc0IsS0FBdEMsRUFBNEM7QUFBQyxVQUFHOU4sTUFBTSxHQUFDLENBQVAsSUFBVTllLFNBQVMsR0FBQyxDQUF2QixFQUF5QjtBQUFDOEcsYUFBSyxJQUFFLElBQUV5ZixHQUFUO0FBQWMsT0FBeEMsTUFBNkMsSUFBR3pILE1BQU0sR0FBQyxDQUFQLElBQVU5ZSxTQUFTLEdBQUMsQ0FBdkIsRUFBeUI7QUFBQzhHLGFBQUssSUFBRSxJQUFFeWYsR0FBVDtBQUFjO0FBQUM7O0FBQzNmLFFBQUlzRyxHQUFHLEdBQUNucEIsSUFBSSxDQUFDQyxHQUFMLENBQVMsQ0FBQyxDQUFWLEVBQVlELElBQUksQ0FBQ2daLEdBQUwsQ0FBUyxDQUFULEVBQVc1VixLQUFLLEdBQUNxSixNQUFNLENBQUNySixLQUF4QixDQUFaLENBQVI7O0FBQW9ELFFBQUcsQ0FBQyxLQUFLdUosTUFBTCxDQUFZdkcsSUFBYixLQUFxQjhMLE9BQU8sQ0FBQzlPLEtBQVIsR0FBYytsQixHQUFkLEdBQWtCLENBQW5CLElBQXdCalgsT0FBTyxDQUFDOU8sS0FBUixHQUFjK2xCLEdBQWQsR0FBa0JqWCxPQUFPLENBQUM1YixNQUFSLEdBQWUsQ0FBN0UsQ0FBSCxFQUFvRjtBQUFDO0FBQVE7O0FBQ2pKbVcsVUFBTSxDQUFDckosS0FBUCxJQUFjK2xCLEdBQWQ7QUFBbUIsR0FGNEc7O0FBRTNHL2tCLE9BQUssQ0FBQ3NnQixXQUFOLEdBQWtCLFlBQVU7QUFBQyxRQUFJalksTUFBTSxHQUFDLEtBQUtBLE1BQWhCO0FBQUEsUUFBdUJ5RixPQUFPLEdBQUMsS0FBS0EsT0FBcEM7QUFBQSxRQUE0QzlMLElBQUksR0FBQyxLQUFLdUcsTUFBTCxDQUFZdkcsSUFBN0Q7QUFBQSxRQUFrRXljLEdBQUcsR0FBQyxLQUFLeEosS0FBTCxFQUF0RTtBQUFBLFFBQW1GckIsSUFBSSxHQUFDNkssR0FBRyxHQUFDN2lCLElBQUksQ0FBQ29ILEtBQUwsQ0FBVyxDQUFDLEtBQUthLE1BQUwsQ0FBWWhDLFFBQVosQ0FBcUJyQyxDQUF0QixHQUF3QjZJLE1BQU0sQ0FBQ3hQLEtBQTFDLENBQUosR0FBcUQsQ0FBN0k7QUFBQSxRQUErSW1zQixFQUFFLEdBQUNwUixJQUFJLEdBQUMsQ0FBdko7O0FBQXlKLFFBQUcsQ0FBQzVSLElBQUQsSUFBT2dqQixFQUFFLEdBQUNsWCxPQUFPLENBQUNxRixhQUFSLEdBQXNCLENBQW5DLEVBQXFDO0FBQUNTLFVBQUksR0FBQzlGLE9BQU8sQ0FBQ3FGLGFBQVIsR0FBc0IsQ0FBM0I7QUFBNkI2UixRQUFFLEdBQUNwUixJQUFJLEdBQUMsQ0FBUjtBQUFXOztBQUN4UixRQUFHLENBQUM1UixJQUFELElBQU80UixJQUFJLEdBQUMsQ0FBZixFQUFpQjtBQUFDQSxVQUFJLEdBQUMsQ0FBTDtBQUFPb1IsUUFBRSxHQUFDLENBQUg7QUFBTTs7QUFDL0IsU0FBSSxJQUFJaHpCLENBQUMsR0FBQzRoQixJQUFWLEVBQWU1aEIsQ0FBQyxHQUFDZ3pCLEVBQWpCLEVBQW9CaHpCLENBQUMsRUFBckIsRUFBd0I7QUFBQyxVQUFJNlAsUUFBUSxHQUFDNGMsR0FBRyxHQUFDenNCLENBQUosR0FBTXFXLE1BQU0sQ0FBQ3hQLEtBQTFCO0FBQUEsVUFBZ0N3akIsV0FBVyxHQUFDbGdCLEtBQUssQ0FBQzRDLE1BQU4sQ0FBYXNKLE1BQU0sQ0FBQ25XLE1BQXBCLEVBQTJCRixDQUEzQixDQUE1QztBQUFBLFVBQTBFOFIsS0FBSyxHQUFDdUUsTUFBTSxDQUFDZ1UsV0FBRCxDQUF0Rjs7QUFBb0csVUFBR3ZZLEtBQUssQ0FBQzlFLEtBQU4sS0FBY2hOLENBQWQsSUFBaUI4UixLQUFLLENBQUNqQyxRQUFOLEtBQWlCQSxRQUFyQyxFQUE4QztBQUFDaUMsYUFBSyxDQUFDOUUsS0FBTixHQUFZaE4sQ0FBWjtBQUFjOFIsYUFBSyxDQUFDakMsUUFBTixHQUFlQSxRQUFmO0FBQXdCaUMsYUFBSyxDQUFDMUcsS0FBTixDQUFZekUsSUFBWixHQUFpQmtKLFFBQVEsR0FBQyxJQUExQjtBQUFnQztBQUFDOztBQUNuUCxRQUFHLEtBQUswRyxNQUFMLENBQVkrSCxJQUFmLEVBQW9CO0FBQUMsV0FBS3lCLFFBQUwsQ0FBYyxDQUFkO0FBQWtCO0FBQUMsR0FIcEI7O0FBR3FCL1IsT0FBSyxDQUFDNmYsV0FBTixHQUFrQixVQUFTOWIsTUFBVCxFQUFnQjtBQUFDLFFBQUlraEIsS0FBSyxHQUFDLEtBQUt2ZCxHQUFMLENBQVNtRixXQUFULENBQXFCbkIsUUFBL0I7QUFBQSxRQUF3QzdILE1BQU0sR0FBQyxLQUFLQSxNQUFwRDtBQUFBLFFBQTJEaUssT0FBTyxHQUFDLEtBQUtBLE9BQXhFO0FBQUEsUUFBZ0YyUSxHQUFHLEdBQUMsS0FBS3hKLEtBQUwsRUFBcEY7QUFBQSxRQUFpRy9pQixNQUFNLEdBQUMreUIsS0FBSyxDQUFDL3lCLE1BQTlHO0FBQUEsUUFBcUg4TSxLQUFLLEdBQUM3QyxLQUFLLENBQUM0QyxNQUFOLENBQWErTyxPQUFPLENBQUNxRixhQUFyQixFQUFtQ3JGLE9BQU8sQ0FBQzlPLEtBQTNDLENBQTNIO0FBQUEsUUFBNktuRyxLQUFLLEdBQUNrTCxNQUFNLENBQUM0UixJQUFQLEdBQWF6akIsTUFBYixHQUFxQixHQUF4TTtBQUFBLFFBQTRNZ3pCLE1BQU0sR0FBQ3RwQixJQUFJLENBQUNvSCxLQUFMLENBQVcsQ0FBQyxDQUFDeWIsR0FBRCxHQUFLMWEsTUFBTSxDQUFDbEMsUUFBUCxDQUFnQnJDLENBQXJCLEdBQXVCaWYsR0FBRyxHQUFDNWxCLEtBQUosR0FBVSxHQUFsQyxJQUF1Q2tMLE1BQU0sQ0FBQzRSLElBQXpELENBQW5OO0FBQUEsUUFBa1IvQixJQUFJLEdBQUNoWSxJQUFJLENBQUNDLEdBQUwsQ0FBUyxDQUFULEVBQVdxcEIsTUFBTSxHQUFDdHBCLElBQUksQ0FBQzhrQixLQUFMLENBQVd4dUIsTUFBTSxHQUFDLENBQWxCLENBQWxCLENBQXZSO0FBQUEsUUFBK1Q4eUIsRUFBRSxHQUFDcFIsSUFBSSxHQUFDMWhCLE1BQXZVO0FBQThVLFFBQUlpekIsU0FBUyxHQUFDdGhCLE1BQU0sQ0FBQ2hMLEtBQVAsR0FBYSxHQUEzQjtBQUFBLFFBQStCdXNCLFNBQVMsR0FBQ3JoQixNQUFNLENBQUNsQyxRQUFQLENBQWdCckMsQ0FBaEIsR0FBa0IybEIsU0FBM0Q7QUFBQSxRQUFxRUUsVUFBVSxHQUFDdGhCLE1BQU0sQ0FBQ2xDLFFBQVAsQ0FBZ0JyQyxDQUFoQixHQUFrQnFFLE1BQU0sQ0FBQ2hMLEtBQXpCLEdBQStCc3NCLFNBQS9HOztBQUF5SCxRQUFHSCxFQUFFLEdBQUNsWCxPQUFPLENBQUNxRixhQUFkLEVBQTRCO0FBQUM2UixRQUFFLEdBQUNsWCxPQUFPLENBQUNxRixhQUFYO0FBQXlCUyxVQUFJLEdBQUNvUixFQUFFLEdBQUM5eUIsTUFBUjtBQUFnQjs7QUFDemxCLFFBQUc4eUIsRUFBRSxLQUFHbFgsT0FBTyxDQUFDcUYsYUFBUixHQUFzQixDQUEzQixJQUE4QlMsSUFBSSxHQUFDb1IsRUFBTCxHQUFROXlCLE1BQXpDLEVBQWdEO0FBQUMwaEIsVUFBSSxHQUFDOUYsT0FBTyxDQUFDcUYsYUFBUixHQUFzQmpoQixNQUEzQjtBQUFtQzs7QUFDcEYsU0FBSSxJQUFJRixDQUFDLEdBQUM0aEIsSUFBVixFQUFlNWhCLENBQUMsR0FBQ2d6QixFQUFqQixFQUFvQmh6QixDQUFDLEVBQXJCLEVBQXdCO0FBQUMsVUFBSStYLEtBQUssR0FBQ2tiLEtBQUssQ0FBQzlvQixLQUFLLENBQUM0QyxNQUFOLENBQWE3TSxNQUFiLEVBQW9CRixDQUFwQixDQUFELENBQWY7QUFBQSxVQUF3QzZQLFFBQVEsR0FBQzRjLEdBQUcsR0FBQ3pzQixDQUFKLEdBQU0rUixNQUFNLENBQUM0UixJQUFiLEdBQWtCNVIsTUFBTSxDQUFDb0IsTUFBUCxHQUFjLEdBQWpGO0FBQUEsVUFBcUZ4SCxTQUFTLEdBQUMsS0FBS3VLLEdBQUwsR0FBUyxlQUF4RztBQUFBLFVBQXdIb2QsUUFBUSxHQUFDbnBCLEtBQUssQ0FBQ2dELFFBQU4sQ0FBZTRLLEtBQWYsRUFBcUJwTSxTQUFyQixDQUFqSTs7QUFBaUssVUFBR29NLEtBQUssQ0FBQy9LLEtBQU4sS0FBY2hOLENBQWQsSUFBaUIrWCxLQUFLLENBQUNsSSxRQUFOLEtBQWlCQSxRQUFyQyxFQUE4QztBQUFDa0ksYUFBSyxDQUFDL0ssS0FBTixHQUFZaE4sQ0FBWjtBQUFjK1gsYUFBSyxDQUFDbEksUUFBTixHQUFlQSxRQUFmO0FBQXdCa0ksYUFBSyxDQUFDM00sS0FBTixDQUFZekUsSUFBWixHQUFpQmtKLFFBQVEsR0FBQyxJQUExQjtBQUFnQzs7QUFDL1MsV0FBSzBqQixZQUFMLENBQWtCeGIsS0FBbEIsRUFBd0JoRyxNQUF4Qjs7QUFBZ0MsVUFBRyxDQUFDZ0csS0FBSyxDQUFDbEksUUFBUCxJQUFpQnVqQixTQUFqQixJQUE0QixDQUFDcmIsS0FBSyxDQUFDbEksUUFBUCxJQUFpQndqQixVQUE3QyxJQUF5RHRiLEtBQUssQ0FBQ3FELEtBQU4sS0FBY3BiLENBQTFFLEVBQTRFO0FBQUMsYUFBSzJ0QixTQUFMLENBQWU1VixLQUFmLEVBQXFCL1gsQ0FBckI7QUFBeUI7O0FBQ3RJLFVBQUdzekIsUUFBUSxJQUFFdG1CLEtBQUssS0FBR2hOLENBQXJCLEVBQXVCO0FBQUNtSyxhQUFLLENBQUNtRCxXQUFOLENBQWtCeUssS0FBbEIsRUFBd0JwTSxTQUF4QjtBQUFvQyxPQUE1RCxNQUFpRSxJQUFHLENBQUMybkIsUUFBRCxJQUFXdG1CLEtBQUssS0FBR2hOLENBQXRCLEVBQXdCO0FBQUNtSyxhQUFLLENBQUNrRCxRQUFOLENBQWUwSyxLQUFmLEVBQXFCcE0sU0FBckI7QUFBaUM7QUFBQztBQUFDLEdBSnBGOztBQUlxRnFDLE9BQUssQ0FBQ3VsQixZQUFOLEdBQW1CLFVBQVN4YixLQUFULEVBQWVoRyxNQUFmLEVBQXNCO0FBQUMsUUFBR2dHLEtBQUssQ0FBQ2xSLEtBQU4sS0FBY2tMLE1BQU0sQ0FBQ2xMLEtBQXJCLElBQTRCa1IsS0FBSyxDQUFDalIsTUFBTixLQUFlaUwsTUFBTSxDQUFDakwsTUFBbEQsSUFBMERpUixLQUFLLENBQUM1RSxNQUFOLEtBQWVwQixNQUFNLENBQUNvQixNQUFuRixFQUEwRjtBQUFDNEUsV0FBSyxDQUFDbFIsS0FBTixHQUFZa0wsTUFBTSxDQUFDbEwsS0FBbkI7QUFBeUJrUixXQUFLLENBQUNqUixNQUFOLEdBQWFpTCxNQUFNLENBQUNqTCxNQUFwQjtBQUEyQmlSLFdBQUssQ0FBQzVFLE1BQU4sR0FBYXBCLE1BQU0sQ0FBQ29CLE1BQXBCO0FBQTJCNEUsV0FBSyxDQUFDM00sS0FBTixDQUFZdkUsS0FBWixHQUFrQmtMLE1BQU0sQ0FBQ2xMLEtBQVAsR0FBYSxJQUEvQjtBQUFvQ2tSLFdBQUssQ0FBQzNNLEtBQU4sQ0FBWXRFLE1BQVosR0FBbUJpTCxNQUFNLENBQUNqTCxNQUFQLEdBQWMsSUFBakM7QUFBdUM7QUFBQyxHQUFoUzs7QUFBaVNrSCxPQUFLLENBQUN5aEIsU0FBTixHQUFnQixVQUFTL00sSUFBVCxFQUFjO0FBQUMsUUFBSXlMLE9BQU8sR0FBQyxLQUFLelksR0FBTCxDQUFTNEQsT0FBVCxDQUFpQmxPLEtBQWpCLENBQXVCK2lCLE9BQW5DO0FBQUEsUUFBMkNrQixRQUFRLEdBQUMsS0FBS2xaLE9BQUwsQ0FBYWtaLFFBQWpFO0FBQUEsUUFBMEVtRSxXQUFXLEdBQUMsS0FBS3JkLE9BQUwsQ0FBYWxVLElBQW5HO0FBQUEsUUFBd0d3eEIsWUFBWSxHQUFDLEtBQUt0ZCxPQUFMLENBQWFpWixPQUFsSTtBQUFBLFFBQTBJaGIsWUFBWSxHQUFDb2YsV0FBVyxLQUFHLFNBQWQsSUFBeUJDLFlBQVksS0FBRyxTQUEvTDtBQUFBLFFBQXlNQyxZQUFZLEdBQUNGLFdBQVcsS0FBRyxNQUFkLElBQXNCQyxZQUFZLEtBQUcsTUFBM1A7O0FBQWtRLFFBQUcvUSxJQUFJLENBQUM3UyxRQUFMLENBQWNuQyxDQUFkLEdBQWdCLEdBQWhCLElBQXFCLE9BQU8yaEIsUUFBUCxLQUFrQixXQUExQyxFQUFzRDtBQUFDLFdBQUtsWixPQUFMLENBQWFrWixRQUFiLEdBQXNCLEtBQXRCO0FBQTZCLEtBQXBGLE1BQXlGLElBQUczTSxJQUFJLENBQUM3UyxRQUFMLENBQWNuQyxDQUFkLEdBQWdCLENBQWhCLElBQW1CLE9BQU8yaEIsUUFBUCxLQUFrQixXQUF4QyxFQUFvRDtBQUFDLFdBQUtsWixPQUFMLENBQWFrWixRQUFiLEdBQXNCLElBQXRCO0FBQTRCOztBQUMxMkIsUUFBRyxLQUFLN2tCLE9BQUwsQ0FBYTRKLFlBQWIsSUFBMkJBLFlBQTNCLElBQXlDLEtBQUsrQixPQUFMLENBQWFrWixRQUF6RCxFQUFrRTtBQUFDbEIsYUFBTyxHQUFDekwsSUFBSSxDQUFDN1MsUUFBTCxDQUFjbkMsQ0FBdEI7QUFBd0IsV0FBS3lJLE9BQUwsQ0FBYWlaLE9BQWIsR0FBcUIsU0FBckI7QUFBZ0MsS0FBM0gsTUFBZ0ksSUFBR3NFLFlBQUgsRUFBZ0I7QUFBQ3ZGLGFBQU8sR0FBQyxJQUFFdmtCLElBQUksQ0FBQ2laLEdBQUwsQ0FBU0gsSUFBSSxDQUFDN1MsUUFBTCxDQUFjcEMsQ0FBdkIsS0FBMkIsS0FBS29FLE1BQUwsQ0FBWS9LLE1BQVosR0FBbUIsR0FBOUMsQ0FBVjtBQUE2RCxXQUFLcVAsT0FBTCxDQUFhaVosT0FBYixHQUFxQixNQUFyQjtBQUE2QixLQUEzRyxNQUFnSCxJQUFHakIsT0FBTyxJQUFFQSxPQUFPLEdBQUMsQ0FBcEIsRUFBc0I7QUFBQ0EsYUFBTyxHQUFDLENBQVI7QUFBVSxXQUFLaFksT0FBTCxDQUFhaVosT0FBYixHQUFxQixLQUFyQjtBQUE0Qjs7QUFDN1NqQixXQUFPLEdBQUMsQ0FBQ0EsT0FBRCxHQUFTLENBQVQsR0FBV3ZrQixJQUFJLENBQUNDLEdBQUwsQ0FBUyxDQUFULEVBQVdELElBQUksQ0FBQ2daLEdBQUwsQ0FBUyxDQUFULEVBQVd1TCxPQUFYLENBQVgsQ0FBbkI7QUFBbUQsUUFBSWhXLE1BQU0sR0FBQ2dXLE9BQU8sSUFBRSxHQUFULElBQWMsQ0FBQ0EsT0FBZixHQUF1QixLQUF2QixHQUE2QixRQUF4QztBQUFpRGhrQixTQUFLLENBQUNnTyxNQUFNLEdBQUMsT0FBUixDQUFMLENBQXNCLEtBQUt6QyxHQUFMLENBQVNDLE1BQS9CLEVBQXNDLEtBQUtPLEdBQUwsR0FBUyxhQUEvQztBQUE4RCxTQUFLUixHQUFMLENBQVM0RCxPQUFULENBQWlCbE8sS0FBakIsQ0FBdUIraUIsT0FBdkIsR0FBK0JBLE9BQS9CO0FBQXdDLEdBRnFOOztBQUVwTm5nQixPQUFLLENBQUNxVCxJQUFOLEdBQVdsWCxLQUFLLENBQUNwSSxRQUFOLENBQWUsWUFBVTtBQUFDLFFBQUcsQ0FBQyxLQUFLb1UsT0FBTCxDQUFhakgsSUFBakIsRUFBc0I7QUFBQyxXQUFLd1ksT0FBTCxDQUFhLEtBQUtyUixNQUFMLENBQVlySixLQUFaLEdBQWtCLElBQUUsS0FBS2lXLEtBQUwsRUFBakM7QUFBZ0Q7QUFBQyxHQUFsRyxFQUFtRyxHQUFuRyxDQUFYO0FBQW1IalYsT0FBSyxDQUFDeVEsSUFBTixHQUFXdFUsS0FBSyxDQUFDcEksUUFBTixDQUFlLFlBQVU7QUFBQyxRQUFHLENBQUMsS0FBS29VLE9BQUwsQ0FBYWpILElBQWpCLEVBQXNCO0FBQUMsV0FBS3dZLE9BQUwsQ0FBYSxLQUFLclIsTUFBTCxDQUFZckosS0FBWixHQUFrQixJQUFFLEtBQUtpVyxLQUFMLEVBQWpDO0FBQWdEO0FBQUMsR0FBbEcsRUFBbUcsR0FBbkcsQ0FBWDs7QUFBbUhqVixPQUFLLENBQUMwWixPQUFOLEdBQWMsVUFBU3NMLEVBQVQsRUFBWTFZLFNBQVosRUFBc0I7QUFBQyxRQUFJakUsTUFBTSxHQUFDLEtBQUtBLE1BQWhCO0FBQUEsUUFBdUJ5RixPQUFPLEdBQUMsS0FBS0EsT0FBcEM7QUFBQSxRQUE0Q25HLE1BQU0sR0FBQyxLQUFLRCxHQUFMLENBQVM3RCxNQUE1RDtBQUFBLFFBQW1FNGEsR0FBRyxHQUFDLEtBQUt4SixLQUFMLEVBQXZFO0FBQUEsUUFBb0YvaUIsTUFBTSxHQUFDNGIsT0FBTyxDQUFDcUYsYUFBbkc7QUFBQSxRQUFpSHdTLFFBQVEsR0FBQ3hwQixLQUFLLENBQUM0QyxNQUFOLENBQWE3TSxNQUFiLEVBQW9COHlCLEVBQXBCLENBQTFIO0FBQUEsUUFBa0pZLFVBQVUsR0FBQ3pwQixLQUFLLENBQUM0QyxNQUFOLENBQWE3TSxNQUFiLEVBQW9CNGIsT0FBTyxDQUFDOU8sS0FBNUIsQ0FBN0o7QUFBQSxRQUFnTTZtQixPQUFPLEdBQUNGLFFBQVEsR0FBQ0MsVUFBak47QUFBQSxRQUE0TkUsUUFBUSxHQUFDNXpCLE1BQU0sR0FBQzBKLElBQUksQ0FBQ2laLEdBQUwsQ0FBU2dSLE9BQVQsQ0FBNU87O0FBQThQLFFBQUcsQ0FBQyxLQUFLdGQsTUFBTCxDQUFZdkcsSUFBYixLQUFvQmdqQixFQUFFLEdBQUMsQ0FBSCxJQUFNQSxFQUFFLEdBQUMsS0FBS2xYLE9BQUwsQ0FBYXFGLGFBQWIsR0FBMkIsQ0FBeEQsQ0FBSCxFQUE4RDtBQUFDO0FBQVE7O0FBQzN4QixRQUFHLEtBQUs1SyxNQUFMLENBQVl2RyxJQUFaLElBQWtCOGpCLFFBQVEsR0FBQyxDQUEzQixJQUE4QkEsUUFBUSxHQUFDLENBQVQsR0FBVzV6QixNQUE1QyxFQUFtRDtBQUFDMnpCLGFBQU8sR0FBQ0EsT0FBTyxHQUFDLENBQVIsR0FBVUMsUUFBVixHQUFtQixDQUFDQSxRQUE1QjtBQUFzQzs7QUFDMUYsUUFBR0gsUUFBUSxLQUFHWCxFQUFkLEVBQWlCO0FBQUNBLFFBQUUsR0FBQzNjLE1BQU0sQ0FBQ3JKLEtBQVAsR0FBYTZtQixPQUFoQjtBQUF5Qjs7QUFDM0NBLFdBQU8sR0FBQ2IsRUFBRSxHQUFDM2MsTUFBTSxDQUFDckosS0FBbEI7O0FBQXdCLFFBQUcsQ0FBQzZtQixPQUFKLEVBQVk7QUFBQztBQUFROztBQUM3QyxRQUFHLEtBQUt0ZCxNQUFMLENBQVkwSSxJQUFmLEVBQW9CO0FBQUMsV0FBS0EsSUFBTDtBQUFhOztBQUNsQyxTQUFLZ0MsVUFBTDtBQUFrQixTQUFLcEosS0FBTDs7QUFBYSxRQUFHLENBQUN5QyxTQUFKLEVBQWM7QUFBQyxXQUFLMEcsYUFBTDtBQUFzQjs7QUFDcEUzSyxVQUFNLENBQUNySixLQUFQLEdBQWFnbUIsRUFBYjtBQUFnQixRQUFJbmhCLE1BQU0sR0FBQyxLQUFLQSxNQUFoQjs7QUFBdUIsUUFBR2pJLElBQUksQ0FBQ2laLEdBQUwsQ0FBU2dSLE9BQVQsSUFBa0IsQ0FBckIsRUFBdUI7QUFBQzFwQixXQUFLLENBQUNrRCxRQUFOLENBQWVzSSxNQUFmLEVBQXNCLEtBQUtPLEdBQUwsR0FBUyxPQUEvQjtBQUF3QyxXQUFLK1gsaUJBQUwsQ0FBdUJwYyxNQUF2QixFQUE4QndFLE1BQTlCO0FBQXNDLFdBQUs2WCxrQkFBTCxDQUF3QjdYLE1BQXhCO0FBQWdDLFVBQUkwZCxNQUFNLEdBQUN0SCxHQUFHLEdBQUNwVyxNQUFNLENBQUN4UCxLQUFYLEdBQWlCK0MsSUFBSSxDQUFDZ1osR0FBTCxDQUFTLENBQVQsRUFBV2haLElBQUksQ0FBQ2laLEdBQUwsQ0FBU2dSLE9BQVQsQ0FBWCxDQUFqQixHQUErQ2pxQixJQUFJLENBQUNpWixHQUFMLENBQVNnUixPQUFULENBQS9DLEdBQWlFQSxPQUE1RTtBQUFvRmhpQixZQUFNLENBQUNoQyxRQUFQLENBQWdCckMsQ0FBaEIsR0FBa0JxRSxNQUFNLENBQUNqRCxVQUFQLENBQWtCcEIsQ0FBbEIsR0FBb0JxRSxNQUFNLENBQUNoQyxRQUFQLENBQWdCckMsQ0FBaEIsR0FBa0J1bUIsTUFBeEQ7QUFBK0Q1cEIsV0FBSyxDQUFDb0QsU0FBTixDQUFnQm9JLE1BQWhCLEVBQXVCOUQsTUFBTSxDQUFDaEMsUUFBUCxDQUFnQnJDLENBQXZDLEVBQXlDLENBQXpDO0FBQTRDbUksWUFBTSxDQUFDaVQsY0FBUDtBQUF5Qjs7QUFDclksU0FBSzVJLGVBQUw7QUFBdUI3VixTQUFLLENBQUNtRCxXQUFOLENBQWtCcUksTUFBbEIsRUFBeUIsS0FBS08sR0FBTCxHQUFTLE9BQWxDO0FBQTJDckUsVUFBTSxDQUFDdkMsWUFBUDtBQUFzQnVDLFVBQU0sQ0FBQ3pDLFdBQVA7QUFBcUJ5QyxVQUFNLENBQUN4QyxTQUFQLENBQWlCO0FBQUM3QixPQUFDLEVBQUMsQ0FBQ2lmLEdBQUQsR0FBS3VHLEVBQUwsR0FBUTNjLE1BQU0sQ0FBQ3hQLEtBQWxCO0FBQXdCNEcsT0FBQyxFQUFDLENBQTFCO0FBQTRCQyxPQUFDLEVBQUM5TztBQUE5QixLQUFqQjtBQUE0RCxHQVB3UTs7QUFPdlFvUCxPQUFLLENBQUNnbUIsT0FBTixHQUFjLFVBQVNoeUIsS0FBVCxFQUFlO0FBQUMsUUFBSXNpQixHQUFHLEdBQUN0aUIsS0FBSyxDQUFDaXlCLE9BQWQ7QUFBQSxRQUFzQjdhLEdBQUcsR0FBQyxLQUFLNU8sT0FBL0I7O0FBQXVDLFFBQUc0TyxHQUFHLENBQUMzRixXQUFQLEVBQW1CO0FBQUMsVUFBRzZRLEdBQUcsS0FBRyxFQUFULEVBQVk7QUFBQyxhQUFLakQsSUFBTCxDQUFVcmYsS0FBVjtBQUFrQixPQUEvQixNQUFvQyxJQUFHc2lCLEdBQUcsS0FBRyxFQUFULEVBQVk7QUFBQyxhQUFLN0YsSUFBTCxDQUFVemMsS0FBVjtBQUFrQjtBQUFDOztBQUN2VSxRQUFHc2lCLEdBQUcsS0FBRyxFQUFOLElBQVVsTCxHQUFHLENBQUNsRixhQUFqQixFQUErQjtBQUFDLFdBQUs0TSxLQUFMO0FBQWM7O0FBQzlDLFFBQUcsQ0FBQzFILEdBQUcsQ0FBQy9HLFVBQUwsSUFBaUIsQ0FBQyxFQUFELEVBQUksRUFBSixFQUFPLEVBQVAsRUFBVSxFQUFWLEVBQWEsRUFBYixFQUFnQixFQUFoQixFQUFtQixFQUFuQixFQUF1QmpFLE9BQXZCLENBQStCa1csR0FBL0IsSUFBb0MsQ0FBQyxDQUF6RCxFQUEyRDtBQUFDdGlCLFdBQUssQ0FBQ3NkLGNBQU47QUFBdUIsYUFBTyxLQUFQO0FBQWM7QUFBQyxHQUZ3RTs7QUFFdkV0UixPQUFLLENBQUNpUixJQUFOLEdBQVcsWUFBVTtBQUFDLFNBQUtyTCxNQUFMO0FBQWUsR0FBckM7O0FBQXNDNUYsT0FBSyxDQUFDNEYsTUFBTixHQUFhLFVBQVNwRyxDQUFULEVBQVdDLENBQVgsRUFBYUUsS0FBYixFQUFtQjtBQUFDLFFBQUcsQ0FBQyxLQUFLdWlCLGNBQUwsRUFBRCxJQUF5QixDQUFDLEtBQUt6TSxVQUFMLEVBQUQsSUFBb0I5VixLQUFLLEdBQUMsQ0FBdEQsRUFBeUQ7QUFBQztBQUFROztBQUM1TyxTQUFLd0ksT0FBTCxDQUFhaVosT0FBYixHQUFxQixLQUFyQjtBQUEyQixRQUFJaFUsS0FBSyxHQUFDLEtBQUtzSSxRQUFMLEVBQVY7QUFBMEIvVixTQUFLLEdBQUNBLEtBQUssR0FBQ0EsS0FBRCxHQUFPLEtBQUs0SSxNQUFMLENBQVkwSSxJQUFaLEdBQWlCLENBQWpCLEdBQW1CN0QsS0FBSyxDQUFDakMsR0FBTixDQUFVd0ssSUFBVixDQUFlaFcsS0FBcEQ7QUFBMEQsUUFBSStVLElBQUksR0FBQyxLQUFLQyxPQUFMLEVBQVQ7QUFBQSxRQUF3QjhMLEtBQUssR0FBQyxLQUFLdEMsYUFBTCxDQUFtQnhlLEtBQW5CLENBQTlCO0FBQUEsUUFBd0R1bUIsS0FBSyxHQUFDMW1CLENBQUMsR0FBQ0EsQ0FBQyxHQUFDLEtBQUtxRSxNQUFMLENBQVloTCxLQUFaLEdBQWtCLEdBQXJCLEdBQXlCLENBQXhGO0FBQUEsUUFBMEZzdEIsS0FBSyxHQUFDMW1CLENBQUMsR0FBQ0EsQ0FBQyxHQUFDLEtBQUtvRSxNQUFMLENBQVkvSyxNQUFaLEdBQW1CLEdBQXRCLEdBQTBCLENBQTNIO0FBQUEsUUFBNkhzdEIsSUFBSSxHQUFDem1CLEtBQUssR0FBQyxDQUFOLEdBQVEvRCxJQUFJLENBQUNtaUIsSUFBTCxDQUFVbUksS0FBSyxHQUFDLENBQUN4UixJQUFJLENBQUM3UyxRQUFMLENBQWNyQyxDQUFkLEdBQWdCMG1CLEtBQWpCLEtBQXlCdm1CLEtBQUssR0FBQytVLElBQUksQ0FBQzdTLFFBQUwsQ0FBY25DLENBQTdDLENBQWhCLENBQVIsR0FBeUUsQ0FBM007QUFBQSxRQUE2TTJtQixJQUFJLEdBQUMxbUIsS0FBSyxHQUFDLENBQU4sR0FBUS9ELElBQUksQ0FBQ21pQixJQUFMLENBQVVvSSxLQUFLLEdBQUMsQ0FBQ3pSLElBQUksQ0FBQzdTLFFBQUwsQ0FBY3BDLENBQWQsR0FBZ0IwbUIsS0FBakIsS0FBeUJ4bUIsS0FBSyxHQUFDK1UsSUFBSSxDQUFDN1MsUUFBTCxDQUFjbkMsQ0FBN0MsQ0FBaEIsQ0FBUixHQUF5RSxDQUEzUjtBQUE2UmdWLFFBQUksQ0FBQ3BULFlBQUw7QUFBb0JvVCxRQUFJLENBQUN0VCxXQUFMO0FBQW1Cc1QsUUFBSSxDQUFDclQsU0FBTCxDQUFlO0FBQUM3QixPQUFDLEVBQUM0bUIsSUFBSSxHQUFDM0YsS0FBSyxDQUFDOW5CLElBQVgsR0FBZ0I4bkIsS0FBSyxDQUFDOW5CLElBQXRCLEdBQTJCeXRCLElBQUksR0FBQzNGLEtBQUssQ0FBQzduQixLQUFYLEdBQWlCNm5CLEtBQUssQ0FBQzduQixLQUF2QixHQUE2Qnd0QixJQUEzRDtBQUFnRTNtQixPQUFDLEVBQUM0bUIsSUFBSSxHQUFDNUYsS0FBSyxDQUFDam9CLE1BQVgsR0FBa0Jpb0IsS0FBSyxDQUFDam9CLE1BQXhCLEdBQStCNnRCLElBQUksR0FBQzVGLEtBQUssQ0FBQ2xvQixHQUFYLEdBQWVrb0IsS0FBSyxDQUFDbG9CLEdBQXJCLEdBQXlCOHRCLElBQTFIO0FBQStIM21CLE9BQUMsRUFBQ0M7QUFBakksS0FBZjtBQUF3SixTQUFLeWtCLFVBQUwsQ0FBZ0J6a0IsS0FBaEI7QUFBd0IsR0FEMWQ7O0FBQzJkSyxPQUFLLENBQUNva0IsVUFBTixHQUFpQixVQUFTemtCLEtBQVQsRUFBZTtBQUFDLFNBQUs0SSxNQUFMLENBQVkwSSxJQUFaLEdBQWlCdFIsS0FBSyxHQUFDLENBQU4sR0FBUSxJQUFSLEdBQWEsS0FBOUI7QUFBb0N4RCxTQUFLLENBQUMsS0FBS29NLE1BQUwsQ0FBWTBJLElBQVosR0FBaUIsVUFBakIsR0FBNEIsYUFBN0IsQ0FBTCxDQUFpRCxLQUFLdkosR0FBTCxDQUFTQyxNQUExRCxFQUFpRSxLQUFLTyxHQUFMLEdBQVMsVUFBMUU7QUFBdUYsR0FBNUo7O0FBQTZKbEksT0FBSyxDQUFDOUosT0FBTixHQUFjLFlBQVU7QUFBQyxRQUFHLENBQUMsS0FBS29OLElBQVQsRUFBYztBQUFDO0FBQVE7O0FBQ2p6QixRQUFHLEtBQUtpRixNQUFMLENBQVkrSCxJQUFmLEVBQW9CO0FBQUMsV0FBS3dDLEtBQUw7QUFBYzs7QUFDbkMsUUFBSTlGLFNBQVMsR0FBQyxLQUFLeFEsT0FBTCxDQUFhb0gsYUFBM0I7QUFBQSxRQUF5Q3FKLE9BQU8sR0FBQyxFQUFqRDs7QUFBb0QsUUFBRztBQUFDQSxhQUFPLEdBQUN4UCxRQUFRLENBQUN5UCxnQkFBVCxDQUEwQkYsU0FBMUIsQ0FBUjtBQUE4QyxLQUFsRCxDQUFrRCxPQUFNMVEsS0FBTixFQUFZLENBQUU7O0FBQ3BILFNBQUksSUFBSXRLLENBQUMsR0FBQyxDQUFOLEVBQVFDLENBQUMsR0FBQ2diLE9BQU8sQ0FBQy9hLE1BQXRCLEVBQTZCRixDQUFDLEdBQUNDLENBQS9CLEVBQWlDRCxDQUFDLEVBQWxDLEVBQXFDO0FBQUMsVUFBSW1iLE1BQU0sR0FBQ0YsT0FBTyxDQUFDamIsQ0FBRCxDQUFsQjs7QUFBc0IsVUFBR21iLE1BQU0sQ0FBQ2dFLFlBQVYsRUFBdUI7QUFBQ2hFLGNBQU0sQ0FBQ2lFLG1CQUFQLENBQTJCLE9BQTNCLEVBQW1DakUsTUFBTSxDQUFDZ0UsWUFBMUMsRUFBdUQsS0FBdkQ7QUFBK0Q7QUFBQzs7QUFDcEosU0FBS2dCLFVBQUwsQ0FBZ0IsS0FBaEI7QUFBdUIsU0FBS3RPLE1BQUwsQ0FBWS9DLFlBQVo7O0FBQTJCLFNBQUk5TyxDQUFDLEdBQUMsQ0FBTixFQUFRQSxDQUFDLEdBQUMsS0FBS3FXLE1BQUwsQ0FBWW5XLE1BQXRCLEVBQTZCRixDQUFDLEVBQTlCLEVBQWlDO0FBQUMsV0FBS3NXLEtBQUwsQ0FBV3RXLENBQVgsRUFBYzhPLFlBQWQ7QUFBOEI7O0FBQ2xILFFBQUcsS0FBS2lELE1BQVIsRUFBZTtBQUFDLFdBQUtBLE1BQUwsQ0FBWWpELFlBQVo7QUFBNEI7O0FBQzVDLFNBQUs0RyxHQUFMLENBQVNDLE1BQVQsQ0FBZ0JvSixVQUFoQixDQUEyQjRILFdBQTNCLENBQXVDLEtBQUtqUixHQUFMLENBQVNDLE1BQWhEO0FBQXdELFNBQUtELEdBQUwsQ0FBU21FLE9BQVQsQ0FBaUJrRixVQUFqQixDQUE0QjRILFdBQTVCLENBQXdDLEtBQUtqUixHQUFMLENBQVNtRSxPQUFqRDtBQUEwRCxXQUFPdEksU0FBUyxDQUFDLEtBQUtELElBQU4sQ0FBaEI7QUFBNEIsV0FBTyxLQUFLb0UsR0FBTCxDQUFTQyxNQUFULENBQWdCckUsSUFBdkI7QUFBNEIsV0FBTyxLQUFLQSxJQUFaO0FBQWtCLEdBTnFrQjs7QUFNcGtCLE1BQUcsT0FBT3hTLE1BQVAsS0FBZ0IsV0FBbkIsRUFBK0I7QUFBQyxLQUFDLFVBQVNELENBQVQsRUFBVztBQUFDQSxPQUFDLENBQUN3VyxTQUFGLEdBQVksVUFBUzdLLE9BQVQsRUFBaUI7QUFBQyxlQUFPLElBQUk2SyxTQUFKLENBQWM3SyxPQUFkLENBQVA7QUFBK0IsT0FBN0Q7QUFBK0QsS0FBNUUsRUFBOEUxTCxNQUE5RTtBQUF1Rjs7QUFDcFQsU0FBT3VXLFNBQVA7QUFBa0IsQ0EzTTZXLENBQUQ7O0FBMk16VztBQUFDdlcsTUFBTSxDQUFDLFlBQVU7QUFBQ3cxQixnQkFBYyxDQUFDOWUsSUFBZjtBQUF1QixDQUFuQyxDQUFOO0FBQTJDLElBQUk4ZSxjQUFjLEdBQUM7QUFBQ0MsVUFBUSxFQUFDLEtBQVY7QUFBZ0J2akIsT0FBSyxFQUFDLElBQXRCO0FBQTJCd0UsTUFBSSxFQUFDLGdCQUFVO0FBQUMsU0FBS2dmLElBQUwsQ0FBVSxNQUFWOztBQUFrQixRQUFHLEtBQUtDLE9BQVIsRUFBZ0I7QUFBQyxXQUFLRCxJQUFMLENBQVUsZ0JBQVY7O0FBQTRCLFdBQUtDLE9BQUwsR0FBYSxJQUFiO0FBQWtCO0FBQVE7O0FBQ3hOLFNBQUtDLHNCQUFMLEdBQTZCLFlBQVU7QUFBQyxhQUFPLzFCLE1BQU0sQ0FBQzRLLHFCQUFQLElBQThCNUssTUFBTSxDQUFDNkssMkJBQXJDLElBQWtFN0ssTUFBTSxDQUFDZzJCLHdCQUF6RSxJQUFtR2gyQixNQUFNLENBQUNpMkIsc0JBQTFHLElBQWtJajJCLE1BQU0sQ0FBQ2syQix1QkFBekksSUFBa0ssVUFBU3h0QixRQUFULEVBQWtCaEUsT0FBbEIsRUFBMEI7QUFBQzFFLGNBQU0sQ0FBQytDLFVBQVAsQ0FBa0IyRixRQUFsQixFQUEyQixPQUFLLEVBQWhDO0FBQXFDLE9BQXpPO0FBQTJPLEtBQXZQLEVBQTVCOztBQUF1UixTQUFLeXRCLFNBQUwsQ0FBZSxJQUFmO0FBQXNCLEdBRHpOO0FBQzBOTCxTQUFPLEVBQUMsS0FEbE87QUFDd09NLGFBQVcsRUFBQyxDQUFDLEdBQUQsRUFBSyxHQUFMLEVBQVMsR0FBVCxFQUFhLFNBQWIsRUFBdUIsU0FBdkIsRUFBaUMsU0FBakMsRUFBMkMsUUFBM0MsRUFBb0QsUUFBcEQsRUFBNkQsUUFBN0QsRUFBc0UsT0FBdEUsQ0FEcFA7QUFDbVVMLHdCQUFzQixFQUFDLElBRDFWO0FBQytWRixNQUFJLEVBQUMsY0FBU2pxQixPQUFULEVBQWlCO0FBQUMsUUFBRyxLQUFLZ3FCLFFBQVIsRUFBaUJscUIsT0FBTyxDQUFDMnFCLEdBQVIsQ0FBWSx1QkFBcUJ6cUIsT0FBakM7QUFBMkMsR0FEbGI7QUFDbWJ1cUIsV0FBUyxFQUFDLG1CQUFTRyxRQUFULEVBQWtCO0FBQUMsUUFBSUMsTUFBTSxHQUFDcDJCLE1BQU0sQ0FBQzJNLFFBQUQsQ0FBTixDQUFpQjBwQixTQUFqQixFQUFYO0FBQXdDLFFBQUlDLFlBQVksR0FBQ3QyQixNQUFNLENBQUNILE1BQUQsQ0FBTixDQUFlbUksTUFBZixFQUFqQjs7QUFBeUMsU0FBSzB0QixJQUFMLENBQVUsY0FBWVUsTUFBdEI7O0FBQThCcDJCLFVBQU0sQ0FBQyxpQkFBRCxDQUFOLENBQTBCdTJCLElBQTFCLENBQStCdjJCLE1BQU0sQ0FBQ3cyQixLQUFQLENBQWEsVUFBU3RvQixLQUFULEVBQWVwQyxFQUFmLEVBQWtCO0FBQUMsVUFBSTJxQixHQUFHLEdBQUN6MkIsTUFBTSxDQUFDOEwsRUFBRCxDQUFkO0FBQW1CLFVBQUk0cUIsVUFBVSxHQUFDLEVBQWY7QUFBa0IsVUFBSUMsZUFBZSxHQUFDLEtBQXBCO0FBQTBCLFVBQUlycUIsS0FBSyxHQUFDbXFCLEdBQUcsQ0FBQy95QixJQUFKLENBQVMsT0FBVCxDQUFWOztBQUE0QixVQUFHNEksS0FBSyxJQUFFeE0sU0FBVixFQUFvQjtBQUFDd00sYUFBSyxHQUFDbXFCLEdBQUcsQ0FBQzl4QixJQUFKLENBQVMsT0FBVCxLQUFtQixFQUF6QjtBQUE0Qjh4QixXQUFHLENBQUMveUIsSUFBSixDQUFTLE9BQVQsRUFBaUI0SSxLQUFqQjtBQUF5Qjs7QUFDdjNCLFVBQUlzcUIsS0FBSyxHQUFDLENBQUNILEdBQUcsQ0FBQy95QixJQUFKLENBQVMsVUFBVCxDQUFELENBQVY7QUFBaUMsVUFBSW16QixLQUFKOztBQUFVLFdBQUlBLEtBQUssR0FBQyxDQUFWLEdBQWFBLEtBQUssRUFBbEIsRUFBcUI7QUFBQyxZQUFHSixHQUFHLENBQUMveUIsSUFBSixDQUFTLGFBQVdtekIsS0FBcEIsQ0FBSCxFQUE4QjtBQUFDRCxlQUFLLENBQUN0ekIsSUFBTixDQUFXbXpCLEdBQUcsQ0FBQy95QixJQUFKLENBQVMsY0FBWW16QixLQUFyQixDQUFYO0FBQXlDLFNBQXhFLE1BQzdEO0FBQUM7QUFBTztBQUFDOztBQUNiLFVBQUlDLFdBQVcsR0FBQ0YsS0FBSyxDQUFDeDFCLE1BQXRCOztBQUE2QixXQUFJeTFCLEtBQUssR0FBQyxDQUFWLEVBQVlBLEtBQUssR0FBQ0MsV0FBbEIsRUFBOEJELEtBQUssRUFBbkMsRUFBc0M7QUFBQyxZQUFJbnpCLElBQUksR0FBQ2t6QixLQUFLLENBQUNDLEtBQUQsQ0FBZDtBQUFzQixZQUFJRSxVQUFVLEdBQUNyekIsSUFBSSxDQUFDLGFBQUQsQ0FBbkI7QUFBbUMsWUFBR3F6QixVQUFVLElBQUVqM0IsU0FBZixFQUF5QmkzQixVQUFVLEdBQUNqc0IsSUFBSSxDQUFDQyxHQUFMLENBQVMsQ0FBVCxFQUFXL0ssTUFBTSxDQUFDOEwsRUFBRCxDQUFOLENBQVc0Z0IsTUFBWCxHQUFvQmpsQixHQUFwQixHQUF3QjZ1QixZQUFuQyxDQUFYO0FBQTREUyxrQkFBVSxHQUFDQSxVQUFVLEdBQUMsQ0FBdEI7QUFBd0IsWUFBSUMsY0FBYyxHQUFDdHpCLElBQUksQ0FBQyxVQUFELENBQXZCO0FBQW9DLFlBQUl1ekIsUUFBUSxHQUFDdnpCLElBQUksQ0FBQyxXQUFELENBQWpCO0FBQStCLFlBQUdzekIsY0FBYyxJQUFFbDNCLFNBQWhCLElBQTJCbTNCLFFBQVEsSUFBRW4zQixTQUF4QyxFQUFrRGszQixjQUFjLEdBQUNWLFlBQWY7QUFBNEJVLHNCQUFjLEdBQUNsc0IsSUFBSSxDQUFDQyxHQUFMLENBQVNpc0IsY0FBYyxHQUFDLENBQXhCLEVBQTBCLENBQTFCLENBQWY7QUFBNEMsWUFBSUUsTUFBTSxHQUFDeHpCLElBQUksQ0FBQyxRQUFELENBQWY7QUFBMEIsWUFBSXl6QixZQUFZLEdBQUN6ekIsSUFBSSxDQUFDLGVBQUQsQ0FBckI7QUFBdUMsWUFBR3d6QixNQUFNLElBQUVwM0IsU0FBUixJQUFtQixDQUFDRSxNQUFNLENBQUNrM0IsTUFBM0IsSUFBbUMsQ0FBQ2wzQixNQUFNLENBQUNrM0IsTUFBUCxDQUFjQSxNQUFkLENBQXZDLEVBQTZEQSxNQUFNLEdBQUMsSUFBUDtBQUFZLFlBQUdDLFlBQVksSUFBRXIzQixTQUFkLElBQXlCLENBQUNFLE1BQU0sQ0FBQ2szQixNQUFqQyxJQUF5QyxDQUFDbDNCLE1BQU0sQ0FBQ2szQixNQUFQLENBQWNDLFlBQWQsQ0FBN0MsRUFBeUVBLFlBQVksR0FBQ0QsTUFBYjs7QUFBb0IsWUFBR0EsTUFBSCxFQUFVO0FBQUMsY0FBSUUsU0FBUyxHQUFDMXpCLElBQUksQ0FBQyxVQUFELENBQWxCO0FBQStCLGNBQUcwekIsU0FBUyxJQUFFdDNCLFNBQWQsRUFBd0JzM0IsU0FBUyxHQUFDSixjQUFWO0FBQXlCSSxtQkFBUyxHQUFDdHNCLElBQUksQ0FBQ0MsR0FBTCxDQUFTcXNCLFNBQVMsR0FBQyxDQUFuQixFQUFxQixDQUFyQixDQUFWO0FBQWtDLGNBQUlDLGVBQWUsR0FBQzN6QixJQUFJLENBQUMsaUJBQUQsQ0FBeEI7QUFBNEMsY0FBRzJ6QixlQUFlLElBQUV2M0IsU0FBcEIsRUFBOEJ1M0IsZUFBZSxHQUFDRCxTQUFoQjtBQUEwQkosd0JBQWMsR0FBQyxDQUFmO0FBQWlCLGNBQUlNLFdBQVcsR0FBQ2IsR0FBRyxDQUFDL3lCLElBQUosQ0FBUyxjQUFULENBQWhCO0FBQXlDLGNBQUc0ekIsV0FBVyxJQUFFeDNCLFNBQWhCLEVBQTBCdzNCLFdBQVcsR0FBQyxDQUFaO0FBQWU7O0FBQ2w5QixZQUFHTCxRQUFRLElBQUVuM0IsU0FBYixFQUF1Qm0zQixRQUFRLEdBQUNGLFVBQVUsR0FBQ0MsY0FBcEI7QUFBbUNDLGdCQUFRLEdBQUNBLFFBQVEsR0FBQyxDQUFsQjtBQUFvQixZQUFJTSxVQUFVLEdBQUM3ekIsSUFBSSxDQUFDLFlBQUQsQ0FBbkI7QUFBa0MsWUFBRzZ6QixVQUFVLElBQUV6M0IsU0FBZixFQUF5QnkzQixVQUFVLEdBQUMsRUFBWDtBQUFjQSxrQkFBVSxHQUFDQSxVQUFVLEdBQUMsQ0FBdEI7QUFBd0IsWUFBR3BCLFFBQVEsSUFBRW9CLFVBQVUsSUFBRSxDQUF6QixFQUEyQkEsVUFBVSxHQUFDLENBQVg7QUFBYUEsa0JBQVUsR0FBQ0EsVUFBVSxHQUFDLENBQXRCO0FBQXdCLFlBQUlDLGFBQWEsR0FBQ3BCLE1BQWxCO0FBQXlCb0IscUJBQWEsR0FBQzFzQixJQUFJLENBQUNDLEdBQUwsQ0FBU3lzQixhQUFULEVBQXVCVCxVQUF2QixDQUFkO0FBQWlEUyxxQkFBYSxHQUFDMXNCLElBQUksQ0FBQ2daLEdBQUwsQ0FBUzBULGFBQVQsRUFBdUJQLFFBQXZCLENBQWQ7O0FBQStDLFlBQUdDLE1BQUgsRUFBVTtBQUFDLGNBQUdULEdBQUcsQ0FBQy95QixJQUFKLENBQVMsTUFBVCxLQUFrQjVELFNBQXJCLEVBQStCMjJCLEdBQUcsQ0FBQy95QixJQUFKLENBQVMsTUFBVCxFQUFnQixNQUFoQjs7QUFBd0IsY0FBRzh6QixhQUFhLEdBQUNULFVBQWpCLEVBQTRCO0FBQUMsZ0JBQUdOLEdBQUcsQ0FBQy95QixJQUFKLENBQVMsTUFBVCxLQUFrQixNQUFyQixFQUE0QjtBQUFDNHpCLHlCQUFXLEdBQUMsQ0FBWjtBQUFjYixpQkFBRyxDQUFDL3lCLElBQUosQ0FBUyxNQUFULEVBQWdCLElBQWhCO0FBQXVCLGFBQWxFLE1BQ25jO0FBQUM0ekIseUJBQVc7QUFBSTtBQUFDOztBQUNyQixjQUFHRSxhQUFhLEdBQUNQLFFBQWpCLEVBQTBCO0FBQUMsZ0JBQUdSLEdBQUcsQ0FBQy95QixJQUFKLENBQVMsTUFBVCxLQUFrQixJQUFyQixFQUEwQjtBQUFDNHpCLHlCQUFXLEdBQUMsQ0FBWjtBQUFjYixpQkFBRyxDQUFDL3lCLElBQUosQ0FBUyxNQUFULEVBQWdCLE1BQWhCO0FBQXlCLGFBQWxFLE1BQ3ZCO0FBQUM0ekIseUJBQVc7QUFBSTtBQUFDOztBQUNyQixjQUFHbkIsUUFBSCxFQUFZbUIsV0FBVyxHQUFDRixTQUFaO0FBQXNCWCxhQUFHLENBQUMveUIsSUFBSixDQUFTLGNBQVQsRUFBd0I0ekIsV0FBeEI7QUFBc0M7O0FBQ3hFLGFBQUtyQixXQUFMLENBQWlCakksR0FBakIsQ0FBcUJodUIsTUFBTSxDQUFDdzJCLEtBQVAsQ0FBYSxVQUFTNXFCLElBQVQsRUFBYztBQUFDLGNBQUk2ckIsV0FBVyxHQUFDLENBQWhCO0FBQWtCLGNBQUl2RCxFQUFFLEdBQUN4d0IsSUFBSSxDQUFDa0ksSUFBRCxDQUFYO0FBQWtCLGNBQUdzb0IsRUFBRSxJQUFFcDBCLFNBQVAsRUFBaUI7O0FBQU8sY0FBRzhMLElBQUksSUFBRSxPQUFOLElBQWVBLElBQUksSUFBRSxRQUFyQixJQUErQkEsSUFBSSxJQUFFLFFBQXJDLElBQStDQSxJQUFJLElBQUUsUUFBeEQsRUFBaUU7QUFBQzZyQix1QkFBVyxHQUFDLENBQVo7QUFBZSxXQUFqRixNQUN6RztBQUFDdkQsY0FBRSxHQUFDQSxFQUFFLEdBQUMsQ0FBTjtBQUFTOztBQUNkLGNBQUkzUixJQUFJLEdBQUNrVSxHQUFHLENBQUMveUIsSUFBSixDQUFTLE1BQUlrSSxJQUFiLENBQVQ7QUFBNEIsY0FBRzJXLElBQUksSUFBRXppQixTQUFULEVBQW1CeWlCLElBQUksR0FBQ2tWLFdBQUw7QUFBaUIsY0FBSTlYLElBQUksR0FBRSxDQUFDdVUsRUFBRSxHQUFDdUQsV0FBSixLQUFrQixDQUFDRCxhQUFhLEdBQUNULFVBQWYsS0FBNEJFLFFBQVEsR0FBQ0YsVUFBckMsQ0FBbEIsQ0FBRCxHQUFzRVUsV0FBL0U7QUFBMkYsY0FBSUMsR0FBRyxHQUFDblYsSUFBSSxHQUFDLENBQUM1QyxJQUFJLEdBQUM0QyxJQUFOLElBQVlnVixVQUF6Qjs7QUFBb0MsY0FBR0wsTUFBTSxJQUFFSSxXQUFXLEdBQUMsQ0FBcEIsSUFBdUJBLFdBQVcsSUFBRUYsU0FBdkMsRUFBaUQ7QUFBQyxnQkFBSXRVLElBQUksR0FBQzJVLFdBQVQ7O0FBQXFCLGdCQUFHaEIsR0FBRyxDQUFDL3lCLElBQUosQ0FBUyxNQUFULEtBQWtCLE1BQXJCLEVBQTRCO0FBQUNvZixrQkFBSSxHQUFDb1IsRUFBTDtBQUFRQSxnQkFBRSxHQUFDLENBQUNBLEVBQUo7QUFBT2dELG9CQUFNLEdBQUNDLFlBQVA7QUFBb0JDLHVCQUFTLEdBQUNDLGVBQVY7QUFBMkI7O0FBQ2pXSyxlQUFHLEdBQUMxM0IsTUFBTSxDQUFDazNCLE1BQVAsQ0FBY0EsTUFBZCxFQUFzQixJQUF0QixFQUEyQkksV0FBM0IsRUFBdUN4VSxJQUF2QyxFQUE0Q29SLEVBQTVDLEVBQStDa0QsU0FBL0MsQ0FBSjtBQUErRDs7QUFDL0RNLGFBQUcsR0FBQzVzQixJQUFJLENBQUNtaUIsSUFBTCxDQUFVeUssR0FBRyxHQUFDLEtBQUt4bEIsS0FBbkIsSUFBMEIsS0FBS0EsS0FBbkM7QUFBeUMsY0FBR3dsQixHQUFHLElBQUVuVixJQUFMLElBQVc1QyxJQUFJLElBQUV1VSxFQUFwQixFQUF1QndELEdBQUcsR0FBQ3hELEVBQUo7QUFBTyxjQUFHLENBQUN3QyxVQUFVLENBQUM5cUIsSUFBRCxDQUFkLEVBQXFCOHFCLFVBQVUsQ0FBQzlxQixJQUFELENBQVYsR0FBaUIsQ0FBakI7QUFBbUI4cUIsb0JBQVUsQ0FBQzlxQixJQUFELENBQVYsSUFBa0I4ckIsR0FBbEI7O0FBQXNCLGNBQUduVixJQUFJLElBQUVtVSxVQUFVLENBQUM5cUIsSUFBRCxDQUFuQixFQUEwQjtBQUFDNnFCLGVBQUcsQ0FBQy95QixJQUFKLENBQVMsTUFBSWtJLElBQWIsRUFBa0I4cUIsVUFBVSxDQUFDOXFCLElBQUQsQ0FBNUI7QUFBb0MrcUIsMkJBQWUsR0FBQyxJQUFoQjtBQUFzQjtBQUFDLFNBSnRNLEVBSXVNLElBSnZNLENBQXJCO0FBSW9POztBQUNwTyxVQUFHQSxlQUFILEVBQW1CO0FBQUMsWUFBR0QsVUFBVSxDQUFDLEdBQUQsQ0FBVixJQUFpQjUyQixTQUFwQixFQUE4QjtBQUFDLGNBQUk2M0IsV0FBVyxHQUFDajBCLElBQUksQ0FBQyxhQUFELENBQXBCO0FBQW9DLGNBQUdpMEIsV0FBVyxJQUFFNzNCLFNBQWhCLEVBQTBCNjNCLFdBQVcsR0FBQyxHQUFaO0FBQWdCLGNBQUlDLE9BQU8sR0FBQ25CLEdBQUcsQ0FBQzFXLE1BQUosRUFBWjtBQUF5QixjQUFHLENBQUM2WCxPQUFPLENBQUNsMEIsSUFBUixDQUFhLE9BQWIsQ0FBSixFQUEwQmswQixPQUFPLENBQUNsMEIsSUFBUixDQUFhLE9BQWIsRUFBcUJrMEIsT0FBTyxDQUFDanpCLElBQVIsQ0FBYSxPQUFiLEtBQXVCLEVBQTVDO0FBQWdEaXpCLGlCQUFPLENBQUNqekIsSUFBUixDQUFhLE9BQWIsRUFBcUIsaUJBQWVnekIsV0FBZixHQUEyQiwwQkFBM0IsR0FBc0RBLFdBQXRELEdBQWtFLE1BQWxFLEdBQXlFQyxPQUFPLENBQUNsMEIsSUFBUixDQUFhLE9BQWIsQ0FBOUY7QUFBc0g7O0FBQzFWLFlBQUdnekIsVUFBVSxDQUFDLFFBQUQsQ0FBVixJQUFzQjUyQixTQUF6QixFQUFtQzQyQixVQUFVLENBQUMsUUFBRCxDQUFWLEdBQXFCLENBQXJCO0FBQXVCLFlBQUdBLFVBQVUsQ0FBQyxRQUFELENBQVYsSUFBc0I1MkIsU0FBekIsRUFBbUM0MkIsVUFBVSxDQUFDLFFBQUQsQ0FBVixHQUFxQixDQUFyQjtBQUF1QixZQUFHQSxVQUFVLENBQUMsUUFBRCxDQUFWLElBQXNCNTJCLFNBQXpCLEVBQW1DNDJCLFVBQVUsQ0FBQyxRQUFELENBQVYsR0FBcUIsQ0FBckI7O0FBQXVCLFlBQUdBLFVBQVUsQ0FBQyxPQUFELENBQVYsSUFBcUI1MkIsU0FBeEIsRUFBa0M7QUFBQzQyQixvQkFBVSxDQUFDLFFBQUQsQ0FBVixJQUFzQkEsVUFBVSxDQUFDLE9BQUQsQ0FBaEM7QUFBMENBLG9CQUFVLENBQUMsUUFBRCxDQUFWLElBQXNCQSxVQUFVLENBQUMsT0FBRCxDQUFoQztBQUEwQ0Esb0JBQVUsQ0FBQyxRQUFELENBQVYsSUFBc0JBLFVBQVUsQ0FBQyxPQUFELENBQWhDO0FBQTJDOztBQUNoVixZQUFJbUIsV0FBVyxHQUFDLGtCQUFnQm5CLFVBQVUsQ0FBQyxHQUFELENBQVYsR0FBZ0JBLFVBQVUsQ0FBQyxHQUFELENBQTFCLEdBQWdDLENBQWhELElBQW1ELE1BQW5ELElBQTJEQSxVQUFVLENBQUMsR0FBRCxDQUFWLEdBQWdCQSxVQUFVLENBQUMsR0FBRCxDQUExQixHQUFnQyxDQUEzRixJQUE4RixNQUE5RixJQUFzR0EsVUFBVSxDQUFDLEdBQUQsQ0FBVixHQUFnQkEsVUFBVSxDQUFDLEdBQUQsQ0FBMUIsR0FBZ0MsQ0FBdEksSUFBeUksS0FBeko7QUFBK0osWUFBSW9CLFFBQVEsR0FBQyxjQUFZcEIsVUFBVSxDQUFDLFNBQUQsQ0FBVixHQUFzQkEsVUFBVSxDQUFDLFNBQUQsQ0FBaEMsR0FBNEMsQ0FBeEQsSUFBMkQsZUFBM0QsSUFBNEVBLFVBQVUsQ0FBQyxTQUFELENBQVYsR0FBc0JBLFVBQVUsQ0FBQyxTQUFELENBQWhDLEdBQTRDLENBQXhILElBQTJILGVBQTNILElBQTRJQSxVQUFVLENBQUMsU0FBRCxDQUFWLEdBQXNCQSxVQUFVLENBQUMsU0FBRCxDQUFoQyxHQUE0QyxDQUF4TCxJQUEyTCxNQUF4TTtBQUErTSxZQUFJcUIsT0FBTyxHQUFDLFlBQVVyQixVQUFVLENBQUMsUUFBRCxDQUFwQixHQUErQixXQUEvQixHQUEyQ0EsVUFBVSxDQUFDLFFBQUQsQ0FBckQsR0FBZ0UsV0FBaEUsR0FBNEVBLFVBQVUsQ0FBQyxRQUFELENBQXRGLEdBQWlHLEdBQTdHO0FBQWlILFlBQUlzQixZQUFZLEdBQUNILFdBQVcsR0FBQyxHQUFaLEdBQWdCQyxRQUFoQixHQUF5QixHQUF6QixHQUE2QkMsT0FBN0IsR0FBcUMsR0FBdEQ7O0FBQTBELGFBQUtyQyxJQUFMLENBQVVzQyxZQUFWOztBQUF3QnZCLFdBQUcsQ0FBQzl4QixJQUFKLENBQVMsT0FBVCxFQUFpQixlQUFhcXpCLFlBQWIsR0FBMEIscUJBQTFCLEdBQWdEQSxZQUFoRCxHQUE2RCxHQUE3RCxHQUFpRTFyQixLQUFsRjtBQUEwRjtBQUFDLEtBaEJzQyxFQWdCckMsSUFoQnFDLENBQS9COztBQWdCQyxRQUFHek0sTUFBTSxDQUFDNEsscUJBQVYsRUFBZ0M7QUFBQzVLLFlBQU0sQ0FBQzRLLHFCQUFQLENBQTZCekssTUFBTSxDQUFDdzJCLEtBQVAsQ0FBYSxLQUFLUixTQUFsQixFQUE0QixJQUE1QixFQUFpQyxLQUFqQyxDQUE3QjtBQUF1RSxLQUF4RyxNQUNocEI7QUFBQyxXQUFLSixzQkFBTCxDQUE0QjUxQixNQUFNLENBQUN3MkIsS0FBUCxDQUFhLEtBQUtSLFNBQWxCLEVBQTRCLElBQTVCLEVBQWlDLEtBQWpDLENBQTVCO0FBQXNFO0FBQUM7QUFsQlEsQ0FBbkI7QUFrQmE7QUFDOUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxXQUFTajJCLENBQVQsRUFBV0YsTUFBWCxFQUFrQjhNLFFBQWxCLEVBQTJCO0FBQUMsV0FBU3NyQixZQUFULENBQXNCQyxRQUF0QixFQUErQnZ1QixLQUEvQixFQUFxQ3d1QixNQUFyQyxFQUE0QztBQUFDLGFBQVNDLEtBQVQsQ0FBZXJyQixNQUFmLEVBQXNCO0FBQUMsYUFBT0EsTUFBTSxDQUFDWCxNQUFQLENBQWMsQ0FBZCxFQUFpQkMsV0FBakIsS0FBK0JVLE1BQU0sQ0FBQzdELEtBQVAsQ0FBYSxDQUFiLENBQXRDO0FBQXVEOztBQUN4SixRQUFJbXZCLE1BQU0sR0FBQyxDQUFDLFFBQUQsRUFBVSxLQUFWLEVBQWdCLElBQWhCLEVBQXFCLEdBQXJCLENBQVg7QUFBQSxRQUFxQzNCLFVBQVUsR0FBQyxFQUFoRDs7QUFBbUQsU0FBSSxJQUFJeDFCLENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBQ20zQixNQUFNLENBQUNqM0IsTUFBckIsRUFBNEJGLENBQUMsRUFBN0IsRUFBZ0M7QUFBQyxVQUFHaTNCLE1BQUgsRUFBVTtBQUFDeHVCLGFBQUssR0FBQ0EsS0FBSyxDQUFDcUQsT0FBTixDQUFjbXJCLE1BQWQsRUFBcUIsTUFBSUUsTUFBTSxDQUFDbjNCLENBQUQsQ0FBVixHQUFjLEdBQWQsR0FBa0JpM0IsTUFBdkMsQ0FBTjtBQUFzRDs7QUFDckp6QixnQkFBVSxDQUFDMEIsS0FBSyxDQUFDQyxNQUFNLENBQUNuM0IsQ0FBRCxDQUFQLENBQUwsR0FBaUJrM0IsS0FBSyxDQUFDRixRQUFELENBQXZCLENBQVYsR0FBNkN2dUIsS0FBN0M7QUFBb0Q7O0FBQ3BEK3NCLGNBQVUsQ0FBQ3dCLFFBQUQsQ0FBVixHQUFxQnZ1QixLQUFyQjtBQUEyQixXQUFPK3NCLFVBQVA7QUFBbUI7O0FBQzlDLFdBQVM0QixRQUFULENBQWtCbHhCLFNBQWxCLEVBQTRCO0FBQUMsUUFBSVksTUFBTSxHQUFDakksQ0FBQyxDQUFDRixNQUFELENBQUQsQ0FBVW1JLE1BQVYsRUFBWDtBQUFBLFFBQThCRCxLQUFLLEdBQUNoSSxDQUFDLENBQUNGLE1BQUQsQ0FBRCxDQUFVa0ksS0FBVixFQUFwQzs7QUFBc0QsU0FBSSxJQUFJN0csQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDbkIsQ0FBQyxDQUFDSyxFQUFGLENBQUttNEIsTUFBTCxDQUFZOTJCLEtBQVosQ0FBa0JMLE1BQWhDLEVBQXVDRixDQUFDLEVBQXhDLEVBQTJDO0FBQUMsVUFBSXMzQixLQUFLLEdBQUN6NEIsQ0FBQyxDQUFDSyxFQUFGLENBQUttNEIsTUFBTCxDQUFZOTJCLEtBQVosQ0FBa0JQLENBQWxCLENBQVY7QUFBQSxVQUErQm1rQixNQUFNLEdBQUNtVCxLQUFLLENBQUNuVCxNQUE1Qzs7QUFBbUQsVUFBRyxDQUFDbVQsS0FBSyxDQUFDbnFCLFFBQU4sQ0FBZSxTQUFmLENBQUosRUFDbEw7QUFBQyxZQUFJcWUsTUFBTSxHQUFFLENBQUN0bEIsU0FBRCxJQUFZQSxTQUFTLEtBQUcsTUFBWixJQUFvQm94QixLQUFLLENBQUN2ekIsR0FBTixDQUFVLFNBQVYsTUFBdUIsR0FBeEQsR0FBNkQsQ0FBN0QsR0FBK0RvZ0IsTUFBTSxDQUFDcUgsTUFBakY7QUFBQSxZQUF3RitMLE9BQU8sR0FBQzE0QixDQUFDLENBQUNGLE1BQUQsQ0FBRCxDQUFVdzJCLFNBQVYsS0FBc0JydUIsTUFBdEIsR0FBNkJ3d0IsS0FBSyxDQUFDOUwsTUFBTixHQUFlamxCLEdBQTVJOztBQUFnSixZQUFHLE9BQU9pbEIsTUFBUCxLQUFnQixRQUFoQixJQUEwQkEsTUFBTSxDQUFDcGQsT0FBUCxDQUFlLEdBQWYsQ0FBN0IsRUFBaUQ7QUFBQ29kLGdCQUFNLEdBQUNkLFFBQVEsQ0FBQ2MsTUFBRCxDQUFSLEdBQWlCLEdBQWpCLEdBQXFCMWtCLE1BQTVCO0FBQW9DOztBQUN2TyxZQUFHeXdCLE9BQU8sR0FBQy9MLE1BQVIsSUFBZ0J0bEIsU0FBUyxJQUFFLE9BQTlCLEVBQXNDO0FBQUMsY0FBRyxDQUFDc3hCLEtBQUssQ0FBQ3JULE1BQU0sQ0FBQ2dLLE9BQVIsQ0FBVCxFQUEwQjtBQUFDbUosaUJBQUssQ0FBQ3Z6QixHQUFOLENBQVU7QUFBQ29xQixxQkFBTyxFQUFDaEssTUFBTSxDQUFDZ0s7QUFBaEIsYUFBVjtBQUFxQzs7QUFDdkcsY0FBSXNKLFVBQVUsR0FBQyxFQUFmO0FBQUEsY0FBa0JqQyxVQUFVLEdBQUMsQ0FBQyxNQUFELEVBQVEsUUFBUixFQUFpQixPQUFqQixFQUF5QixPQUF6QixFQUFpQyxPQUFqQyxFQUF5QyxRQUF6QyxFQUFrRCxVQUFsRCxFQUE2RCxTQUE3RCxFQUF1RSxTQUF2RSxFQUFpRixTQUFqRixFQUEyRixPQUEzRixFQUFtRyxTQUFuRyxFQUE2RyxRQUE3RyxFQUFzSCxRQUF0SCxFQUErSCxNQUEvSCxFQUFzSSxPQUF0SSxFQUE4SSxPQUE5SSxDQUE3Qjs7QUFBb0wsZUFBSSxJQUFJenFCLENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBQ3lxQixVQUFVLENBQUN0MUIsTUFBekIsRUFBZ0M2SyxDQUFDLEVBQWpDLEVBQW9DO0FBQUMsZ0JBQUcsT0FBT29aLE1BQU0sQ0FBQ3FSLFVBQVUsQ0FBQ3pxQixDQUFELENBQVgsQ0FBYixLQUErQixXQUFsQyxFQUE4QztBQUFDMHNCLHdCQUFVLENBQUNqQyxVQUFVLENBQUN6cUIsQ0FBRCxDQUFYLENBQVYsR0FBMEJvWixNQUFNLENBQUNxUixVQUFVLENBQUN6cUIsQ0FBRCxDQUFYLENBQWhDO0FBQWlEO0FBQUM7O0FBQzFULGNBQUkyc0IsU0FBUyxHQUFDLEVBQWQ7O0FBQWlCLGVBQUksSUFBSUMsQ0FBUixJQUFhRixVQUFiLEVBQXdCO0FBQUNDLHFCQUFTLElBQUVDLENBQUMsQ0FBQzdyQixPQUFGLENBQVUsTUFBVixFQUFpQixXQUFqQixJQUE4QixHQUE5QixHQUFrQzJyQixVQUFVLENBQUNFLENBQUQsQ0FBNUMsR0FBZ0QsSUFBM0Q7QUFBaUU7O0FBQzNHLGNBQUdELFNBQUgsRUFBYTtBQUFDSixpQkFBSyxDQUFDdnpCLEdBQU4sQ0FBVWd6QixZQUFZLENBQUMsV0FBRCxFQUFhVyxTQUFiLENBQXRCO0FBQStDSixpQkFBSyxDQUFDelksTUFBTixHQUFlOWEsR0FBZixDQUFtQmd6QixZQUFZLENBQUMsYUFBRCxFQUFlNVMsTUFBTSxDQUFDc1MsV0FBdEIsQ0FBL0I7O0FBQW1FLGdCQUFHdFMsTUFBTSxDQUFDeVQsZUFBVixFQUEwQjtBQUFDTixtQkFBSyxDQUFDdnpCLEdBQU4sQ0FBVWd6QixZQUFZLENBQUMsaUJBQUQsRUFBbUI1UyxNQUFNLENBQUN5VCxlQUExQixDQUF0QjtBQUFtRTtBQUFDOztBQUMvTixjQUFHLE9BQU96VCxNQUFNLENBQUMxaUIsS0FBZCxLQUFzQixXQUF0QixJQUFtQzBpQixNQUFNLENBQUMxaUIsS0FBUCxHQUFhLENBQW5ELEVBQ0E7QUFBQzYxQixpQkFBSyxDQUFDdnpCLEdBQU4sQ0FBVSxrQkFBVixFQUE2QjJtQixRQUFRLENBQUN2RyxNQUFNLENBQUMxaUIsS0FBUixDQUFSLEdBQXVCLElBQXBEO0FBQTJEOztBQUM1RDYxQixlQUFLLENBQUNqcUIsUUFBTixDQUFlLGVBQWY7QUFBaUMsU0FOakMsTUFNcUM7QUFBQyxjQUFHLENBQUNpcUIsS0FBSyxDQUFDbnFCLFFBQU4sQ0FBZSxlQUFmLENBQUQsS0FBbUMsSUFBdEMsRUFDdEM7QUFBQ21xQixpQkFBSyxDQUFDTyxJQUFOLEdBQWE5ekIsR0FBYixDQUFpQixTQUFqQixFQUEyQixDQUEzQixFQUE4QkEsR0FBOUIsQ0FBa0NnekIsWUFBWSxDQUFDLFdBQUQsRUFBYSxxQkFBYixDQUE5QyxFQUFtRmh6QixHQUFuRixDQUF1RixXQUF2RixFQUFtRyxFQUFuRztBQUF1R3V6QixpQkFBSyxDQUFDanFCLFFBQU4sQ0FBZSxTQUFmO0FBQTBCaXFCLGlCQUFLLENBQUN6WSxNQUFOLEdBQWV4UixRQUFmLENBQXdCLFNBQXhCO0FBQW9DLFdBRGhJLE1BR3RDO0FBQUN2TyxrQkFBTSxDQUFDLE1BQUQsQ0FBTixDQUFldU8sUUFBZixDQUF3QixZQUF4Qjs7QUFBc0MsZ0JBQUdrcUIsT0FBTyxHQUFDL0wsTUFBUixJQUFnQjFzQixNQUFNLENBQUNILE1BQUQsQ0FBTixDQUFlbUksTUFBZixNQUF5QmhJLE1BQU0sQ0FBQzJNLFFBQUQsQ0FBTixDQUFpQjNFLE1BQWpCLEVBQTVDLEVBQ3ZDO0FBQUN3d0IsbUJBQUssQ0FBQ08sSUFBTixHQUFhQyxLQUFiLENBQW1CLFVBQVNyWixJQUFULEVBQWM7QUFBQzNmLHNCQUFNLENBQUMsSUFBRCxDQUFOLENBQWErNEIsSUFBYixHQUFvQjl6QixHQUFwQixDQUF3QixTQUF4QixFQUFrQyxDQUFsQyxFQUFxQ0EsR0FBckMsQ0FBeUNnekIsWUFBWSxDQUFDLFdBQUQsRUFBYSxxQkFBYixDQUFyRCxFQUEwRmh6QixHQUExRixDQUE4RixXQUE5RixFQUEwRyxFQUExRztBQUE4R3V6QixxQkFBSyxDQUFDanFCLFFBQU4sQ0FBZSxTQUFmO0FBQTBCaXFCLHFCQUFLLENBQUN6WSxNQUFOLEdBQWV4UixRQUFmLENBQXdCLFNBQXhCO0FBQW1DaXFCLHFCQUFLLENBQUNocUIsV0FBTixDQUFrQixlQUFsQjtBQUFtQ21SLG9CQUFJO0FBQUksZUFBeFA7QUFBMlAsYUFEck4sTUFHdkM7QUFBQzZZLG1CQUFLLENBQUNPLElBQU4sR0FBYXAyQixLQUFiLENBQW1CLElBQW5CLEVBQXlCcTJCLEtBQXpCLENBQStCLFVBQVNyWixJQUFULEVBQWM7QUFBQzlmLHNCQUFNLENBQUNvM0IsUUFBUCxDQUFnQnAzQixNQUFNLENBQUNvNUIsT0FBdkIsRUFBK0JwNUIsTUFBTSxDQUFDcTVCLE9BQVAsR0FBZSxDQUE5QztBQUFrRCxlQUFoRztBQUFrR1YsbUJBQUssQ0FBQ2hxQixXQUFOLENBQWtCLGVBQWxCO0FBQW9DO0FBQUM7QUFBQztBQUFDO0FBQUM7QUFBQzs7QUFDNUksV0FBU3ZMLFFBQVQsQ0FBa0I3QyxFQUFsQixFQUFxQis0QixVQUFyQixFQUFnQ0MsS0FBaEMsRUFBc0M7QUFBQ0QsY0FBVSxHQUFDQSxVQUFVLElBQUUsR0FBdkI7QUFBMkIsUUFBSUUsSUFBSixFQUFTQyxVQUFUO0FBQW9CLFdBQU8sWUFBVTtBQUFDLFVBQUlDLE9BQU8sR0FBQ0gsS0FBSyxJQUFFLElBQW5CO0FBQUEsVUFBd0JwckIsR0FBRyxHQUFDLENBQUMsSUFBSXBGLElBQUosRUFBN0I7QUFBQSxVQUF3Q0ssSUFBSSxHQUFDRSxTQUE3Qzs7QUFBdUQsVUFBR2t3QixJQUFJLElBQUVyckIsR0FBRyxHQUFDcXJCLElBQUksR0FBQ0YsVUFBbEIsRUFBNkI7QUFBQ3B3QixvQkFBWSxDQUFDdXdCLFVBQUQsQ0FBWjtBQUF5QkEsa0JBQVUsR0FBQzEyQixVQUFVLENBQUMsWUFBVTtBQUFDeTJCLGNBQUksR0FBQ3JyQixHQUFMO0FBQVM1TixZQUFFLENBQUNtRCxLQUFILENBQVNnMkIsT0FBVCxFQUFpQnR3QixJQUFqQjtBQUF3QixTQUE3QyxFQUE4Q2t3QixVQUE5QyxDQUFyQjtBQUFnRixPQUF2SSxNQUEySTtBQUFDRSxZQUFJLEdBQUNyckIsR0FBTDtBQUFTNU4sVUFBRSxDQUFDbUQsS0FBSCxDQUFTZzJCLE9BQVQsRUFBaUJ0d0IsSUFBakI7QUFBd0I7QUFBQyxLQUF2UDtBQUF5UDs7QUFDL1VsSixHQUFDLENBQUNLLEVBQUYsQ0FBS200QixNQUFMLEdBQVksVUFBUzdzQixPQUFULEVBQWlCO0FBQUMzTCxLQUFDLENBQUNLLEVBQUYsQ0FBS200QixNQUFMLENBQVk3aEIsSUFBWixDQUFpQixJQUFqQixFQUFzQjNXLENBQUMsQ0FBQ3dKLE1BQUYsQ0FBUyxFQUFULEVBQVl4SixDQUFDLENBQUNLLEVBQUYsQ0FBS200QixNQUFMLENBQVk1c0IsUUFBeEIsRUFBaUNELE9BQWpDLENBQXRCO0FBQWlFLFdBQU8sSUFBUDtBQUFhLEdBQTVHOztBQUE2RzNMLEdBQUMsQ0FBQ0ssRUFBRixDQUFLbTRCLE1BQUwsQ0FBWTkyQixLQUFaLEdBQWtCLEVBQWxCO0FBQXFCMUIsR0FBQyxDQUFDSyxFQUFGLENBQUttNEIsTUFBTCxDQUFZN1gsTUFBWixHQUFtQixLQUFuQjtBQUF5QjNnQixHQUFDLENBQUNLLEVBQUYsQ0FBS200QixNQUFMLENBQVk1c0IsUUFBWixHQUFxQjtBQUFDK2dCLFVBQU0sRUFBQyxLQUFSO0FBQWMyQyxXQUFPLEVBQUMsQ0FBdEI7QUFBd0Ixc0IsU0FBSyxFQUFDLEtBQTlCO0FBQW9DNjJCLFlBQVEsRUFBQyxPQUE3QztBQUFxREMsY0FBVSxFQUFDLEVBQWhFO0FBQW1FQyxrQkFBYyxFQUFDLGFBQWxGO0FBQWdHWixtQkFBZSxFQUFDLEtBQWhIO0FBQXNIbkIsZUFBVyxFQUFDLElBQWxJO0FBQXVJZ0MsYUFBUyxFQUFDLEdBQWpKO0FBQXFKQyxjQUFVLEVBQUM7QUFBaEssR0FBckI7O0FBQTRMNzVCLEdBQUMsQ0FBQ0ssRUFBRixDQUFLbTRCLE1BQUwsQ0FBWTdoQixJQUFaLEdBQWlCLFVBQVNqVixLQUFULEVBQWVsQixRQUFmLEVBQXdCO0FBQUNrQixTQUFLLENBQUM4MEIsSUFBTixDQUFXLFlBQVU7QUFBQyxVQUFJaUMsS0FBSyxHQUFDejRCLENBQUMsQ0FBQyxJQUFELENBQVg7QUFBQSxVQUFrQnNsQixNQUFNLEdBQUNtVCxLQUFLLENBQUNuVCxNQUFOLEdBQWF0bEIsQ0FBQyxDQUFDd0osTUFBRixDQUFTLEVBQVQsRUFBWWhKLFFBQVosRUFBcUJpNEIsS0FBSyxDQUFDOTBCLElBQU4sRUFBckIsQ0FBdEM7QUFBeUU4MEIsV0FBSyxDQUFDOTBCLElBQU4sQ0FBVyxLQUFYLEVBQWlCODBCLEtBQUssQ0FBQzlMLE1BQU4sR0FBZWpsQixHQUFoQztBQUFxQzRkLFlBQU0sQ0FBQ29VLFVBQVAsR0FBa0J4QixZQUFZLENBQUMsWUFBRCxFQUFjNVMsTUFBTSxDQUFDb1UsVUFBckIsRUFBZ0MsV0FBaEMsQ0FBOUI7QUFBMkVqQixXQUFLLENBQUN2ekIsR0FBTixDQUFVb2dCLE1BQU0sQ0FBQ29VLFVBQWpCO0FBQTZCMTVCLE9BQUMsQ0FBQ0ssRUFBRixDQUFLbTRCLE1BQUwsQ0FBWTkyQixLQUFaLENBQWtCNkIsSUFBbEIsQ0FBdUJrMUIsS0FBdkI7QUFBK0IsS0FBM1E7O0FBQTZRLFFBQUcsQ0FBQ3o0QixDQUFDLENBQUNLLEVBQUYsQ0FBS200QixNQUFMLENBQVk3WCxNQUFoQixFQUF1QjtBQUFDM2dCLE9BQUMsQ0FBQ0ssRUFBRixDQUFLbTRCLE1BQUwsQ0FBWTdYLE1BQVosR0FBbUIsSUFBbkI7QUFBd0IsVUFBSW1aLFNBQVMsR0FBQyxDQUFkO0FBQUEsVUFBZ0JDLFNBQVMsR0FBQy81QixDQUFDLENBQUNGLE1BQUQsQ0FBRCxDQUFVbUksTUFBVixFQUExQjtBQUFBLFVBQTZDK3hCLFFBQVEsR0FBQ2g2QixDQUFDLENBQUNGLE1BQUQsQ0FBRCxDQUFVa0ksS0FBVixFQUF0RDtBQUFBLFVBQXdFaXlCLFlBQVksR0FBQ2o2QixDQUFDLENBQUM0TSxRQUFELENBQUQsQ0FBWTNFLE1BQVosRUFBckY7QUFBQSxVQUEwR2l5QixRQUExRzs7QUFBbUgsVUFBR2w2QixDQUFDLENBQUMsTUFBRCxDQUFELENBQVVnSSxLQUFWLE9BQW9CaEksQ0FBQyxDQUFDRixNQUFELENBQUQsQ0FBVWtJLEtBQVYsRUFBdkIsRUFBeUM7QUFBQ2hJLFNBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVXdPLFFBQVYsQ0FBbUIsaUJBQW5CO0FBQXVDOztBQUNsNEIsVUFBR3hPLENBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVXNPLFFBQVYsQ0FBbUIseUJBQW5CLENBQUgsRUFDQTtBQUFDdE8sU0FBQyxDQUFDLGlDQUFELENBQUQsQ0FBcUNtNkIsS0FBckMsQ0FBMkMsWUFBVTtBQUFDNUIsa0JBQVEsQ0FBQyxPQUFELENBQVI7QUFBa0J2NEIsV0FBQyxDQUFDRixNQUFELENBQUQsQ0FBVW1FLEVBQVYsQ0FBYSxRQUFiLEVBQXNCZixRQUFRLENBQUMsWUFBVTtBQUFDLGdCQUFJazNCLFNBQVMsR0FBQ3A2QixDQUFDLENBQUNGLE1BQUQsQ0FBRCxDQUFVdzJCLFNBQVYsRUFBZDtBQUFBLGdCQUFvQ2p2QixTQUFTLEdBQUUreUIsU0FBUyxHQUFDTixTQUFYLEdBQXNCenlCLFNBQVMsR0FBQyxJQUFoQyxHQUFxQyxNQUFuRjtBQUEwRnl5QixxQkFBUyxHQUFDTSxTQUFWO0FBQW9CN0Isb0JBQVEsQ0FBQ2x4QixTQUFELENBQVI7QUFBcUIsV0FBL0ksRUFBZ0osR0FBaEosQ0FBOUI7QUFBb0x2SCxnQkFBTSxDQUFDbzNCLFFBQVAsQ0FBZ0JwM0IsTUFBTSxDQUFDbzVCLE9BQXZCLEVBQStCcDVCLE1BQU0sQ0FBQ3E1QixPQUFQLEdBQWUsQ0FBOUM7QUFBa0QsU0FBOVM7QUFBaVQsT0FEbFQsTUFHQTtBQUFDbDVCLGNBQU0sQ0FBQzJNLFFBQUQsQ0FBTixDQUFpQnV0QixLQUFqQixDQUF1QixZQUFVO0FBQUM1QixrQkFBUSxDQUFDLE1BQUQsQ0FBUjtBQUFpQnY0QixXQUFDLENBQUNGLE1BQUQsQ0FBRCxDQUFVbUUsRUFBVixDQUFhLFFBQWIsRUFBc0JmLFFBQVEsQ0FBQyxZQUFVO0FBQUMsZ0JBQUlrM0IsU0FBUyxHQUFDcDZCLENBQUMsQ0FBQ0YsTUFBRCxDQUFELENBQVV3MkIsU0FBVixFQUFkO0FBQUEsZ0JBQW9DanZCLFNBQVMsR0FBRSt5QixTQUFTLEdBQUNOLFNBQVgsR0FBc0J6eUIsU0FBUyxHQUFDLElBQWhDLEdBQXFDLE1BQW5GO0FBQTBGeXlCLHFCQUFTLEdBQUNNLFNBQVY7QUFBb0I3QixvQkFBUSxDQUFDbHhCLFNBQUQsQ0FBUjtBQUFxQixXQUEvSSxFQUFnSixHQUFoSixDQUE5Qjs7QUFBb0wsY0FBR3BILE1BQU0sQ0FBQ0gsTUFBRCxDQUFOLENBQWVtSSxNQUFmLE1BQXlCaEksTUFBTSxDQUFDMk0sUUFBRCxDQUFOLENBQWlCM0UsTUFBakIsRUFBNUIsRUFDeE87QUFBQ3N3QixvQkFBUSxDQUFDLE1BQUQsQ0FBUjtBQUFrQjs7QUFDbkIsY0FBSThCLFNBQVMsR0FBQ2pqQixTQUFTLENBQUNrakIsU0FBVixDQUFvQm55QixXQUFwQixHQUFrQ29ILE9BQWxDLENBQTBDLFNBQTFDLElBQXFELENBQUMsQ0FBcEU7O0FBQXNFLGNBQUc4cUIsU0FBSCxFQUFhO0FBQUN2NkIsa0JBQU0sQ0FBQ28zQixRQUFQLENBQWdCcDNCLE1BQU0sQ0FBQ281QixPQUF2QixFQUErQnA1QixNQUFNLENBQUNxNUIsT0FBUCxHQUFlLENBQTlDO0FBQWtEOztBQUN0SSxjQUFJb0IsT0FBTyxJQUFFLGtCQUFpQjN0QixRQUFRLENBQUM2VixlQUE1QixDQUFYOztBQUF3RCxjQUFHOFgsT0FBSCxFQUFXO0FBQUN6NkIsa0JBQU0sQ0FBQ28zQixRQUFQLENBQWdCcDNCLE1BQU0sQ0FBQ281QixPQUF2QixFQUErQnA1QixNQUFNLENBQUNxNUIsT0FBUCxHQUFlLENBQTlDO0FBQWtEO0FBQUMsU0FIdEg7QUFHeUg7QUFBQztBQUFDLEdBUDJOO0FBT3pOLENBM0I3SCxFQTJCOEhsNUIsTUEzQjlILEVBMkJxSUgsTUEzQnJJLEVBMkI0SThNLFFBM0I1SSxDQUFEOztBQTJCd0o7O0FBQUMsQ0FBQyxVQUFTL0ksQ0FBVCxFQUFXO0FBQUMsTUFBRywrQkFBTzIyQixPQUFQLE9BQWlCLFFBQWpCLElBQTJCLE9BQU9DLE1BQVAsS0FBZ0IsV0FBOUMsRUFBMEQ7QUFBQ0EsVUFBTSxDQUFDRCxPQUFQLEdBQWUzMkIsQ0FBQyxFQUFoQjtBQUFtQixHQUE5RSxNQUFtRixJQUFHLElBQUgsRUFBMEM7QUFBQ3dILHFDQUFPLEVBQUQsb0NBQUl4SCxDQUFKO0FBQUE7QUFBQTtBQUFBLG9HQUFOO0FBQWEsR0FBeEQsTUFBNEQsVUFBbUs7QUFBQyxDQUFoVSxFQUFrVSxZQUFVO0FBQUMsTUFBSXdILE1BQUosRUFBV292QixNQUFYLEVBQWtCRCxPQUFsQjtBQUEwQixTQUFPLFNBQVN4M0IsQ0FBVCxDQUFXODFCLENBQVgsRUFBYTRCLENBQWIsRUFBZUMsQ0FBZixFQUFpQjtBQUFDLGFBQVM5ckIsQ0FBVCxDQUFXK3JCLENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsVUFBRyxDQUFDSCxDQUFDLENBQUNFLENBQUQsQ0FBTCxFQUFTO0FBQUMsWUFBRyxDQUFDOUIsQ0FBQyxDQUFDOEIsQ0FBRCxDQUFMLEVBQVM7QUFBQyxjQUFJcjVCLENBQUMsR0FBQyxPQUFPdTVCLE9BQVAsSUFBZ0IsVUFBaEIsSUFBNEJBLE9BQWxDO0FBQTBDLGNBQUcsQ0FBQ0QsQ0FBRCxJQUFJdDVCLENBQVAsRUFBUyxPQUFPQSxPQUFDLENBQUNxNUIsQ0FBRCxFQUFHLENBQUMsQ0FBSixDQUFSO0FBQWUsY0FBR3o1QixDQUFILEVBQUssT0FBT0EsQ0FBQyxDQUFDeTVCLENBQUQsRUFBRyxDQUFDLENBQUosQ0FBUjtBQUFlLGNBQUkvMkIsQ0FBQyxHQUFDLElBQUlrM0IsS0FBSixDQUFVLHlCQUF1QkgsQ0FBdkIsR0FBeUIsR0FBbkMsQ0FBTjtBQUE4QyxnQkFBTS8yQixDQUFDLENBQUNtM0IsSUFBRixHQUFPLGtCQUFQLEVBQTBCbjNCLENBQWhDO0FBQWtDOztBQUFBLFlBQUl6QyxDQUFDLEdBQUNzNUIsQ0FBQyxDQUFDRSxDQUFELENBQUQsR0FBSztBQUFDSixpQkFBTyxFQUFDO0FBQVQsU0FBWDtBQUF3QjFCLFNBQUMsQ0FBQzhCLENBQUQsQ0FBRCxDQUFLLENBQUwsRUFBUTd4QixJQUFSLENBQWEzSCxDQUFDLENBQUNvNUIsT0FBZixFQUF1QixVQUFTeDNCLENBQVQsRUFBVztBQUFDLGNBQUkwM0IsQ0FBQyxHQUFDNUIsQ0FBQyxDQUFDOEIsQ0FBRCxDQUFELENBQUssQ0FBTCxFQUFRNTNCLENBQVIsQ0FBTjtBQUFpQixpQkFBTzZMLENBQUMsQ0FBQzZyQixDQUFDLEdBQUNBLENBQUQsR0FBRzEzQixDQUFMLENBQVI7QUFBZ0IsU0FBcEUsRUFBcUU1QixDQUFyRSxFQUF1RUEsQ0FBQyxDQUFDbzVCLE9BQXpFLEVBQWlGeDNCLENBQWpGLEVBQW1GODFCLENBQW5GLEVBQXFGNEIsQ0FBckYsRUFBdUZDLENBQXZGO0FBQTBGOztBQUFBLGFBQU9ELENBQUMsQ0FBQ0UsQ0FBRCxDQUFELENBQUtKLE9BQVo7QUFBb0I7O0FBQUEsUUFBSXI1QixDQUFDLEdBQUMsT0FBTzI1QixPQUFQLElBQWdCLFVBQWhCLElBQTRCQSxPQUFsQzs7QUFBMEMsU0FBSSxJQUFJRixDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNELENBQUMsQ0FBQ3Q1QixNQUFoQixFQUF1QnU1QixDQUFDLEVBQXhCO0FBQTJCL3JCLE9BQUMsQ0FBQzhyQixDQUFDLENBQUNDLENBQUQsQ0FBRixDQUFEO0FBQTNCOztBQUFtQyxXQUFPL3JCLENBQVA7QUFBUyxHQUF6YixDQUEyYjtBQUFDLE9BQUUsQ0FBQyxVQUFTaXNCLE9BQVQsRUFBaUJMLE1BQWpCLEVBQXdCRCxPQUF4QixFQUFnQztBQUFDOztBQUFhLFVBQUlTLHFCQUFxQixHQUFDL3FCLE1BQU0sQ0FBQytxQixxQkFBakM7QUFBdUQsVUFBSW52QixjQUFjLEdBQUNvRSxNQUFNLENBQUNsUCxTQUFQLENBQWlCOEssY0FBcEM7QUFBbUQsVUFBSW92QixnQkFBZ0IsR0FBQ2hyQixNQUFNLENBQUNsUCxTQUFQLENBQWlCbTZCLG9CQUF0Qzs7QUFBMkQsZUFBU0MsUUFBVCxDQUFrQnpELEdBQWxCLEVBQXNCO0FBQUMsWUFBR0EsR0FBRyxLQUFHLElBQU4sSUFBWUEsR0FBRyxLQUFHNTNCLFNBQXJCLEVBQStCO0FBQUMsZ0JBQU0sSUFBSXM3QixTQUFKLENBQWMsdURBQWQsQ0FBTjtBQUE4RTs7QUFDN3hDLGVBQU9uckIsTUFBTSxDQUFDeW5CLEdBQUQsQ0FBYjtBQUFvQjs7QUFDcEIsZUFBUzJELGVBQVQsR0FBMEI7QUFBQyxZQUFHO0FBQUMsY0FBRyxDQUFDcHJCLE1BQU0sQ0FBQ3FyQixNQUFYLEVBQWtCO0FBQUMsbUJBQU8sS0FBUDtBQUFjOztBQUNoRSxjQUFJQyxLQUFLLEdBQUMsSUFBSXhSLE1BQUosQ0FBVyxLQUFYLENBQVY7QUFBNEJ3UixlQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVMsSUFBVDs7QUFBYyxjQUFHdHJCLE1BQU0sQ0FBQ3VyQixtQkFBUCxDQUEyQkQsS0FBM0IsRUFBa0MsQ0FBbEMsTUFBdUMsR0FBMUMsRUFBOEM7QUFBQyxtQkFBTyxLQUFQO0FBQWM7O0FBQ3ZHLGNBQUlFLEtBQUssR0FBQyxFQUFWOztBQUFhLGVBQUksSUFBSXY2QixDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUMsRUFBZCxFQUFpQkEsQ0FBQyxFQUFsQixFQUFxQjtBQUFDdTZCLGlCQUFLLENBQUMsTUFBSTFSLE1BQU0sQ0FBQzJSLFlBQVAsQ0FBb0J4NkIsQ0FBcEIsQ0FBTCxDQUFMLEdBQWtDQSxDQUFsQztBQUFxQzs7QUFDeEUsY0FBSXk2QixNQUFNLEdBQUMxckIsTUFBTSxDQUFDdXJCLG1CQUFQLENBQTJCQyxLQUEzQixFQUFrQ3pOLEdBQWxDLENBQXNDLFVBQVN5TSxDQUFULEVBQVc7QUFBQyxtQkFBT2dCLEtBQUssQ0FBQ2hCLENBQUQsQ0FBWjtBQUFpQixXQUFuRSxDQUFYOztBQUFnRixjQUFHa0IsTUFBTSxDQUFDNVYsSUFBUCxDQUFZLEVBQVosTUFBa0IsWUFBckIsRUFBa0M7QUFBQyxtQkFBTyxLQUFQO0FBQWM7O0FBQ2pJLGNBQUk2VixLQUFLLEdBQUMsRUFBVjtBQUFhLGlDQUF1QnZ6QixLQUF2QixDQUE2QixFQUE3QixFQUFpQ2lGLE9BQWpDLENBQXlDLFVBQVN1dUIsTUFBVCxFQUFnQjtBQUFDRCxpQkFBSyxDQUFDQyxNQUFELENBQUwsR0FBY0EsTUFBZDtBQUFzQixXQUFoRjs7QUFBa0YsY0FBRzVyQixNQUFNLENBQUNtQyxJQUFQLENBQVluQyxNQUFNLENBQUNxckIsTUFBUCxDQUFjLEVBQWQsRUFBaUJNLEtBQWpCLENBQVosRUFBcUM3VixJQUFyQyxDQUEwQyxFQUExQyxNQUFnRCxzQkFBbkQsRUFBMEU7QUFBQyxtQkFBTyxLQUFQO0FBQWM7O0FBQ3hMLGlCQUFPLElBQVA7QUFBYSxTQUxjLENBS2QsT0FBTStWLEdBQU4sRUFBVTtBQUFDLGlCQUFPLEtBQVA7QUFBYztBQUFDOztBQUN2Q3RCLFlBQU0sQ0FBQ0QsT0FBUCxHQUFlYyxlQUFlLEtBQUdwckIsTUFBTSxDQUFDcXJCLE1BQVYsR0FBaUIsVUFBU3JjLE1BQVQsRUFBZ0I1QyxNQUFoQixFQUF1QjtBQUFDLFlBQUl5RyxJQUFKO0FBQVMsWUFBSW9SLEVBQUUsR0FBQ2lILFFBQVEsQ0FBQ2xjLE1BQUQsQ0FBZjtBQUF3QixZQUFJOGMsT0FBSjs7QUFBWSxhQUFJLElBQUludEIsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDekYsU0FBUyxDQUFDL0gsTUFBeEIsRUFBK0J3TixDQUFDLEVBQWhDLEVBQW1DO0FBQUNrVSxjQUFJLEdBQUM3UyxNQUFNLENBQUM5RyxTQUFTLENBQUN5RixDQUFELENBQVYsQ0FBWDs7QUFBMEIsZUFBSSxJQUFJNFcsR0FBUixJQUFlMUMsSUFBZixFQUFvQjtBQUFDLGdCQUFHalgsY0FBYyxDQUFDL0MsSUFBZixDQUFvQmdhLElBQXBCLEVBQXlCMEMsR0FBekIsQ0FBSCxFQUFpQztBQUFDME8sZ0JBQUUsQ0FBQzFPLEdBQUQsQ0FBRixHQUFRMUMsSUFBSSxDQUFDMEMsR0FBRCxDQUFaO0FBQW1CO0FBQUM7O0FBQzdQLGNBQUd3VixxQkFBSCxFQUF5QjtBQUFDZSxtQkFBTyxHQUFDZixxQkFBcUIsQ0FBQ2xZLElBQUQsQ0FBN0I7O0FBQW9DLGlCQUFJLElBQUk1aEIsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDNjZCLE9BQU8sQ0FBQzM2QixNQUF0QixFQUE2QkYsQ0FBQyxFQUE5QixFQUFpQztBQUFDLGtCQUFHKzVCLGdCQUFnQixDQUFDbnlCLElBQWpCLENBQXNCZ2EsSUFBdEIsRUFBMkJpWixPQUFPLENBQUM3NkIsQ0FBRCxDQUFsQyxDQUFILEVBQTBDO0FBQUNnekIsa0JBQUUsQ0FBQzZILE9BQU8sQ0FBQzc2QixDQUFELENBQVIsQ0FBRixHQUFlNGhCLElBQUksQ0FBQ2laLE9BQU8sQ0FBQzc2QixDQUFELENBQVIsQ0FBbkI7QUFBaUM7QUFBQztBQUFDO0FBQUM7O0FBQy9LLGVBQU9nekIsRUFBUDtBQUFXLE9BRlg7QUFFYSxLQVZ1N0IsRUFVdDdCLEVBVnM3QixDQUFIO0FBVS82QixPQUFFLENBQUMsVUFBUzJHLE9BQVQsRUFBaUJMLE1BQWpCLEVBQXdCRCxPQUF4QixFQUFnQztBQUFDLE9BQUMsVUFBU3lCLE9BQVQsRUFBaUI7QUFBQyxTQUFDLFlBQVU7QUFBQyxjQUFJQyxjQUFKLEVBQW1CQyxNQUFuQixFQUEwQkMsUUFBMUIsRUFBbUNDLGNBQW5DLEVBQWtEQyxZQUFsRCxFQUErREMsTUFBL0Q7O0FBQXNFLGNBQUksT0FBT0MsV0FBUCxLQUFxQixXQUFyQixJQUFrQ0EsV0FBVyxLQUFHLElBQWpELElBQXdEQSxXQUFXLENBQUN2dUIsR0FBdkUsRUFBMkU7QUFBQ3dzQixrQkFBTSxDQUFDRCxPQUFQLEdBQWUsWUFBVTtBQUFDLHFCQUFPZ0MsV0FBVyxDQUFDdnVCLEdBQVosRUFBUDtBQUEwQixhQUFwRDtBQUFzRCxXQUFsSSxNQUF1SSxJQUFJLE9BQU9ndUIsT0FBUCxLQUFpQixXQUFqQixJQUE4QkEsT0FBTyxLQUFHLElBQXpDLElBQWdEQSxPQUFPLENBQUNFLE1BQTNELEVBQWtFO0FBQUMxQixrQkFBTSxDQUFDRCxPQUFQLEdBQWUsWUFBVTtBQUFDLHFCQUFNLENBQUMwQixjQUFjLEtBQUdJLFlBQWxCLElBQWdDLEdBQXRDO0FBQTJDLGFBQXJFOztBQUFzRUgsa0JBQU0sR0FBQ0YsT0FBTyxDQUFDRSxNQUFmOztBQUFzQkQsMEJBQWMsR0FBQywwQkFBVTtBQUFDLGtCQUFJTyxFQUFKO0FBQU9BLGdCQUFFLEdBQUNOLE1BQU0sRUFBVDtBQUFZLHFCQUFPTSxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQU0sR0FBTixHQUFVQSxFQUFFLENBQUMsQ0FBRCxDQUFuQjtBQUF3QixhQUFyRTs7QUFBc0VKLDBCQUFjLEdBQUNILGNBQWMsRUFBN0I7QUFBZ0NLLGtCQUFNLEdBQUNOLE9BQU8sQ0FBQ1MsTUFBUixLQUFpQixHQUF4QjtBQUE0Qkosd0JBQVksR0FBQ0QsY0FBYyxHQUFDRSxNQUE1QjtBQUFvQyxXQUFyVSxNQUEwVSxJQUFHMXpCLElBQUksQ0FBQ29GLEdBQVIsRUFBWTtBQUFDd3NCLGtCQUFNLENBQUNELE9BQVAsR0FBZSxZQUFVO0FBQUMscUJBQU8zeEIsSUFBSSxDQUFDb0YsR0FBTCxLQUFXbXVCLFFBQWxCO0FBQTRCLGFBQXREOztBQUF1REEsb0JBQVEsR0FBQ3Z6QixJQUFJLENBQUNvRixHQUFMLEVBQVQ7QUFBcUIsV0FBekYsTUFBNkY7QUFBQ3dzQixrQkFBTSxDQUFDRCxPQUFQLEdBQWUsWUFBVTtBQUFDLHFCQUFPLElBQUkzeEIsSUFBSixHQUFXZ0MsT0FBWCxLQUFxQnV4QixRQUE1QjtBQUFzQyxhQUFoRTs7QUFBaUVBLG9CQUFRLEdBQUMsSUFBSXZ6QixJQUFKLEdBQVdnQyxPQUFYLEVBQVQ7QUFBK0I7QUFBQyxTQUFsdUIsRUFBb3VCOUIsSUFBcHVCLENBQXl1QixJQUF6dUI7QUFBZ3ZCLE9BQW53QixFQUFxd0JBLElBQXJ3QixDQUEwd0IsSUFBMXdCLEVBQSt3Qit4QixPQUFPLENBQUMsVUFBRCxDQUF0eEI7QUFBb3lCLEtBQXQwQixFQUF1MEI7QUFBQyxrQkFBVztBQUFaLEtBQXYwQixDQVY2NkI7QUFVdEYsT0FBRSxDQUFDLFVBQVNBLE9BQVQsRUFBaUJMLE1BQWpCLEVBQXdCRCxPQUF4QixFQUFnQztBQUFDLFVBQUl5QixPQUFPLEdBQUN4QixNQUFNLENBQUNELE9BQVAsR0FBZSxFQUEzQjtBQUE4QixVQUFJbUMsZ0JBQUo7QUFBcUIsVUFBSUMsa0JBQUo7O0FBQXVCLGVBQVNDLGdCQUFULEdBQTJCO0FBQUMsY0FBTSxJQUFJOUIsS0FBSixDQUFVLGlDQUFWLENBQU47QUFBb0Q7O0FBQ3ppQyxlQUFTK0IsbUJBQVQsR0FBOEI7QUFBQyxjQUFNLElBQUkvQixLQUFKLENBQVUsbUNBQVYsQ0FBTjtBQUFzRDs7QUFDcEYsbUJBQVU7QUFBQyxZQUFHO0FBQUMsY0FBRyxPQUFPbDRCLFVBQVAsS0FBb0IsVUFBdkIsRUFBa0M7QUFBQzg1Qiw0QkFBZ0IsR0FBQzk1QixVQUFqQjtBQUE2QixXQUFoRSxNQUFvRTtBQUFDODVCLDRCQUFnQixHQUFDRSxnQkFBakI7QUFBbUM7QUFBQyxTQUE3RyxDQUE2RyxPQUFNNzVCLENBQU4sRUFBUTtBQUFDMjVCLDBCQUFnQixHQUFDRSxnQkFBakI7QUFBbUM7O0FBQ3JLLFlBQUc7QUFBQyxjQUFHLE9BQU83ekIsWUFBUCxLQUFzQixVQUF6QixFQUFvQztBQUFDNHpCLDhCQUFrQixHQUFDNXpCLFlBQW5CO0FBQWlDLFdBQXRFLE1BQTBFO0FBQUM0ekIsOEJBQWtCLEdBQUNFLG1CQUFuQjtBQUF3QztBQUFDLFNBQXhILENBQXdILE9BQU05NUIsQ0FBTixFQUFRO0FBQUM0NUIsNEJBQWtCLEdBQUNFLG1CQUFuQjtBQUF3QztBQUFDLE9BRHpLLEdBQUQ7O0FBRUEsZUFBU0MsVUFBVCxDQUFvQkMsR0FBcEIsRUFBd0I7QUFBQyxZQUFHTCxnQkFBZ0IsS0FBRzk1QixVQUF0QixFQUFpQztBQUFDLGlCQUFPQSxVQUFVLENBQUNtNkIsR0FBRCxFQUFLLENBQUwsQ0FBakI7QUFBMEI7O0FBQ3JGLFlBQUcsQ0FBQ0wsZ0JBQWdCLEtBQUdFLGdCQUFuQixJQUFxQyxDQUFDRixnQkFBdkMsS0FBMEQ5NUIsVUFBN0QsRUFBd0U7QUFBQzg1QiwwQkFBZ0IsR0FBQzk1QixVQUFqQjtBQUE0QixpQkFBT0EsVUFBVSxDQUFDbTZCLEdBQUQsRUFBSyxDQUFMLENBQWpCO0FBQTBCOztBQUMvSCxZQUFHO0FBQUMsaUJBQU9MLGdCQUFnQixDQUFDSyxHQUFELEVBQUssQ0FBTCxDQUF2QjtBQUFnQyxTQUFwQyxDQUFvQyxPQUFNaDZCLENBQU4sRUFBUTtBQUFDLGNBQUc7QUFBQyxtQkFBTzI1QixnQkFBZ0IsQ0FBQzV6QixJQUFqQixDQUFzQixJQUF0QixFQUEyQmkwQixHQUEzQixFQUErQixDQUEvQixDQUFQO0FBQTBDLFdBQTlDLENBQThDLE9BQU1oNkIsQ0FBTixFQUFRO0FBQUMsbUJBQU8yNUIsZ0JBQWdCLENBQUM1ekIsSUFBakIsQ0FBc0IsSUFBdEIsRUFBMkJpMEIsR0FBM0IsRUFBK0IsQ0FBL0IsQ0FBUDtBQUEwQztBQUFDO0FBQUM7O0FBQ2hKLGVBQVNDLGVBQVQsQ0FBeUJDLE1BQXpCLEVBQWdDO0FBQUMsWUFBR04sa0JBQWtCLEtBQUc1ekIsWUFBeEIsRUFBcUM7QUFBQyxpQkFBT0EsWUFBWSxDQUFDazBCLE1BQUQsQ0FBbkI7QUFBNkI7O0FBQ3BHLFlBQUcsQ0FBQ04sa0JBQWtCLEtBQUdFLG1CQUFyQixJQUEwQyxDQUFDRixrQkFBNUMsS0FBaUU1ekIsWUFBcEUsRUFBaUY7QUFBQzR6Qiw0QkFBa0IsR0FBQzV6QixZQUFuQjtBQUFnQyxpQkFBT0EsWUFBWSxDQUFDazBCLE1BQUQsQ0FBbkI7QUFBNkI7O0FBQy9JLFlBQUc7QUFBQyxpQkFBT04sa0JBQWtCLENBQUNNLE1BQUQsQ0FBekI7QUFBbUMsU0FBdkMsQ0FBdUMsT0FBTWw2QixDQUFOLEVBQVE7QUFBQyxjQUFHO0FBQUMsbUJBQU80NUIsa0JBQWtCLENBQUM3ekIsSUFBbkIsQ0FBd0IsSUFBeEIsRUFBNkJtMEIsTUFBN0IsQ0FBUDtBQUE2QyxXQUFqRCxDQUFpRCxPQUFNbDZCLENBQU4sRUFBUTtBQUFDLG1CQUFPNDVCLGtCQUFrQixDQUFDN3pCLElBQW5CLENBQXdCLElBQXhCLEVBQTZCbTBCLE1BQTdCLENBQVA7QUFBNkM7QUFBQztBQUFDOztBQUN6SixVQUFJakUsS0FBSyxHQUFDLEVBQVY7QUFBYSxVQUFJa0UsUUFBUSxHQUFDLEtBQWI7QUFBbUIsVUFBSUMsWUFBSjtBQUFpQixVQUFJQyxVQUFVLEdBQUMsQ0FBQyxDQUFoQjs7QUFBa0IsZUFBU0MsZUFBVCxHQUEwQjtBQUFDLFlBQUcsQ0FBQ0gsUUFBRCxJQUFXLENBQUNDLFlBQWYsRUFBNEI7QUFBQztBQUFROztBQUNuSUQsZ0JBQVEsR0FBQyxLQUFUOztBQUFlLFlBQUdDLFlBQVksQ0FBQy83QixNQUFoQixFQUF1QjtBQUFDNDNCLGVBQUssR0FBQ21FLFlBQVksQ0FBQ3p2QixNQUFiLENBQW9Cc3JCLEtBQXBCLENBQU47QUFBa0MsU0FBMUQsTUFBOEQ7QUFBQ29FLG9CQUFVLEdBQUMsQ0FBQyxDQUFaO0FBQWU7O0FBQzdGLFlBQUdwRSxLQUFLLENBQUM1M0IsTUFBVCxFQUFnQjtBQUFDazhCLG9CQUFVO0FBQUk7QUFBQzs7QUFDaEMsZUFBU0EsVUFBVCxHQUFxQjtBQUFDLFlBQUdKLFFBQUgsRUFBWTtBQUFDO0FBQVE7O0FBQzNDLFlBQUkxMEIsT0FBTyxHQUFDczBCLFVBQVUsQ0FBQ08sZUFBRCxDQUF0QjtBQUF3Q0gsZ0JBQVEsR0FBQyxJQUFUO0FBQWMsWUFBSUssR0FBRyxHQUFDdkUsS0FBSyxDQUFDNTNCLE1BQWQ7O0FBQXFCLGVBQU1tOEIsR0FBTixFQUFVO0FBQUNKLHNCQUFZLEdBQUNuRSxLQUFiO0FBQW1CQSxlQUFLLEdBQUMsRUFBTjs7QUFBUyxpQkFBTSxFQUFFb0UsVUFBRixHQUFhRyxHQUFuQixFQUF1QjtBQUFDLGdCQUFHSixZQUFILEVBQWdCO0FBQUNBLDBCQUFZLENBQUNDLFVBQUQsQ0FBWixDQUF5QnYwQixHQUF6QjtBQUFnQztBQUFDOztBQUM1THUwQixvQkFBVSxHQUFDLENBQUMsQ0FBWjtBQUFjRyxhQUFHLEdBQUN2RSxLQUFLLENBQUM1M0IsTUFBVjtBQUFrQjs7QUFDaEMrN0Isb0JBQVksR0FBQyxJQUFiO0FBQWtCRCxnQkFBUSxHQUFDLEtBQVQ7QUFBZUYsdUJBQWUsQ0FBQ3gwQixPQUFELENBQWY7QUFBMEI7O0FBQzNEd3pCLGFBQU8sQ0FBQ3dCLFFBQVIsR0FBaUIsVUFBU1QsR0FBVCxFQUFhO0FBQUMsWUFBSTl6QixJQUFJLEdBQUMsSUFBSXcwQixLQUFKLENBQVV0MEIsU0FBUyxDQUFDL0gsTUFBVixHQUFpQixDQUEzQixDQUFUOztBQUF1QyxZQUFHK0gsU0FBUyxDQUFDL0gsTUFBVixHQUFpQixDQUFwQixFQUFzQjtBQUFDLGVBQUksSUFBSUYsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDaUksU0FBUyxDQUFDL0gsTUFBeEIsRUFBK0JGLENBQUMsRUFBaEMsRUFBbUM7QUFBQytILGdCQUFJLENBQUMvSCxDQUFDLEdBQUMsQ0FBSCxDQUFKLEdBQVVpSSxTQUFTLENBQUNqSSxDQUFELENBQW5CO0FBQXdCO0FBQUM7O0FBQzFKODNCLGFBQUssQ0FBQzExQixJQUFOLENBQVcsSUFBSW82QixJQUFKLENBQVNYLEdBQVQsRUFBYTl6QixJQUFiLENBQVg7O0FBQStCLFlBQUcrdkIsS0FBSyxDQUFDNTNCLE1BQU4sS0FBZSxDQUFmLElBQWtCLENBQUM4N0IsUUFBdEIsRUFBK0I7QUFBQ0osb0JBQVUsQ0FBQ1EsVUFBRCxDQUFWO0FBQXdCO0FBQUMsT0FEeEY7O0FBQ3lGLGVBQVNJLElBQVQsQ0FBY1gsR0FBZCxFQUFrQlksS0FBbEIsRUFBd0I7QUFBQyxhQUFLWixHQUFMLEdBQVNBLEdBQVQ7QUFBYSxhQUFLWSxLQUFMLEdBQVdBLEtBQVg7QUFBa0I7O0FBQ2pKRCxVQUFJLENBQUMzOEIsU0FBTCxDQUFlOEgsR0FBZixHQUFtQixZQUFVO0FBQUMsYUFBS2swQixHQUFMLENBQVN4NUIsS0FBVCxDQUFlLElBQWYsRUFBb0IsS0FBS282QixLQUF6QjtBQUFpQyxPQUEvRDs7QUFBZ0UzQixhQUFPLENBQUMxZCxLQUFSLEdBQWMsU0FBZDtBQUF3QjBkLGFBQU8sQ0FBQ2x0QixPQUFSLEdBQWdCLElBQWhCO0FBQXFCa3RCLGFBQU8sQ0FBQzRCLEdBQVIsR0FBWSxFQUFaO0FBQWU1QixhQUFPLENBQUM2QixJQUFSLEdBQWEsRUFBYjtBQUFnQjdCLGFBQU8sQ0FBQ3pwQixPQUFSLEdBQWdCLEVBQWhCO0FBQW1CeXBCLGFBQU8sQ0FBQzhCLFFBQVIsR0FBaUIsRUFBakI7O0FBQW9CLGVBQVNoNEIsSUFBVCxHQUFlLENBQUU7O0FBQ3BNazJCLGFBQU8sQ0FBQ2g0QixFQUFSLEdBQVc4QixJQUFYO0FBQWdCazJCLGFBQU8sQ0FBQytCLFdBQVIsR0FBb0JqNEIsSUFBcEI7QUFBeUJrMkIsYUFBTyxDQUFDZ0MsSUFBUixHQUFhbDRCLElBQWI7QUFBa0JrMkIsYUFBTyxDQUFDMTFCLEdBQVIsR0FBWVIsSUFBWjtBQUFpQmsyQixhQUFPLENBQUNpQyxjQUFSLEdBQXVCbjRCLElBQXZCO0FBQTRCazJCLGFBQU8sQ0FBQ2tDLGtCQUFSLEdBQTJCcDRCLElBQTNCO0FBQWdDazJCLGFBQU8sQ0FBQ21DLElBQVIsR0FBYXI0QixJQUFiO0FBQWtCazJCLGFBQU8sQ0FBQ29DLGVBQVIsR0FBd0J0NEIsSUFBeEI7QUFBNkJrMkIsYUFBTyxDQUFDcUMsbUJBQVIsR0FBNEJ2NEIsSUFBNUI7O0FBQWlDazJCLGFBQU8sQ0FBQzNzQixTQUFSLEdBQWtCLFVBQVN4SyxJQUFULEVBQWM7QUFBQyxlQUFNLEVBQU47QUFBUyxPQUExQzs7QUFDeE5tM0IsYUFBTyxDQUFDc0MsT0FBUixHQUFnQixVQUFTejVCLElBQVQsRUFBYztBQUFDLGNBQU0sSUFBSWkyQixLQUFKLENBQVUsa0NBQVYsQ0FBTjtBQUFxRCxPQUFwRjs7QUFBcUZrQixhQUFPLENBQUN1QyxHQUFSLEdBQVksWUFBVTtBQUFDLGVBQU0sR0FBTjtBQUFVLE9BQWpDOztBQUFrQ3ZDLGFBQU8sQ0FBQ3dDLEtBQVIsR0FBYyxVQUFTQyxHQUFULEVBQWE7QUFBQyxjQUFNLElBQUkzRCxLQUFKLENBQVUsZ0NBQVYsQ0FBTjtBQUFtRCxPQUEvRTs7QUFBZ0ZrQixhQUFPLENBQUMwQyxLQUFSLEdBQWMsWUFBVTtBQUFDLGVBQU8sQ0FBUDtBQUFVLE9BQW5DO0FBQXFDLEtBckJpb0IsRUFxQmhvQixFQXJCZ29CLENBVm9GO0FBK0JodEIsT0FBRSxDQUFDLFVBQVM3RCxPQUFULEVBQWlCTCxNQUFqQixFQUF3QkQsT0FBeEIsRUFBZ0M7QUFBQyxPQUFDLFVBQVNvRSxNQUFULEVBQWdCO0FBQUMsWUFBSTN3QixHQUFHLEdBQUM2c0IsT0FBTyxDQUFDLGlCQUFELENBQWY7QUFBQSxZQUFtQzN2QixJQUFJLEdBQUMsT0FBT3JMLE1BQVAsS0FBZ0IsV0FBaEIsR0FBNEI4K0IsTUFBNUIsR0FBbUM5K0IsTUFBM0U7QUFBQSxZQUFrRisrQixPQUFPLEdBQUMsQ0FBQyxLQUFELEVBQU8sUUFBUCxDQUExRjtBQUFBLFlBQTJHQyxNQUFNLEdBQUMsZ0JBQWxIO0FBQUEsWUFBbUlDLEdBQUcsR0FBQzV6QixJQUFJLENBQUMsWUFBVTJ6QixNQUFYLENBQTNJO0FBQUEsWUFBOEpFLEdBQUcsR0FBQzd6QixJQUFJLENBQUMsV0FBUzJ6QixNQUFWLENBQUosSUFBdUIzekIsSUFBSSxDQUFDLGtCQUFnQjJ6QixNQUFqQixDQUE3TDs7QUFDdlMsYUFBSSxJQUFJMzlCLENBQUMsR0FBQyxDQUFWLEVBQVksQ0FBQzQ5QixHQUFELElBQU01OUIsQ0FBQyxHQUFDMDlCLE9BQU8sQ0FBQ3g5QixNQUE1QixFQUFtQ0YsQ0FBQyxFQUFwQyxFQUF1QztBQUFDNDlCLGFBQUcsR0FBQzV6QixJQUFJLENBQUMwekIsT0FBTyxDQUFDMTlCLENBQUQsQ0FBUCxHQUFXLFNBQVgsR0FBcUIyOUIsTUFBdEIsQ0FBUjtBQUN4Q0UsYUFBRyxHQUFDN3pCLElBQUksQ0FBQzB6QixPQUFPLENBQUMxOUIsQ0FBRCxDQUFQLEdBQVcsUUFBWCxHQUFvQjI5QixNQUFyQixDQUFKLElBQWtDM3pCLElBQUksQ0FBQzB6QixPQUFPLENBQUMxOUIsQ0FBRCxDQUFQLEdBQVcsZUFBWCxHQUEyQjI5QixNQUE1QixDQUExQztBQUE4RTs7QUFDOUUsWUFBRyxDQUFDQyxHQUFELElBQU0sQ0FBQ0MsR0FBVixFQUFjO0FBQUMsY0FBSTFGLElBQUksR0FBQyxDQUFUO0FBQUEsY0FBV3J1QixFQUFFLEdBQUMsQ0FBZDtBQUFBLGNBQWdCZ3VCLEtBQUssR0FBQyxFQUF0QjtBQUFBLGNBQXlCZ0csYUFBYSxHQUFDLE9BQUssRUFBNUM7O0FBQ2ZGLGFBQUcsR0FBQyxhQUFTdjJCLFFBQVQsRUFBa0I7QUFBQyxnQkFBR3l3QixLQUFLLENBQUM1M0IsTUFBTixLQUFlLENBQWxCLEVBQW9CO0FBQUMsa0JBQUk2OUIsSUFBSSxHQUFDanhCLEdBQUcsRUFBWjtBQUFBLGtCQUFlMlIsSUFBSSxHQUFDN1UsSUFBSSxDQUFDQyxHQUFMLENBQVMsQ0FBVCxFQUFXaTBCLGFBQWEsSUFBRUMsSUFBSSxHQUFDNUYsSUFBUCxDQUF4QixDQUFwQjs7QUFDNUNBLGtCQUFJLEdBQUMxWixJQUFJLEdBQUNzZixJQUFWO0FBQ0FyOEIsd0JBQVUsQ0FBQyxZQUFVO0FBQUMsb0JBQUlzOEIsRUFBRSxHQUFDbEcsS0FBSyxDQUFDOXZCLEtBQU4sQ0FBWSxDQUFaLENBQVA7QUFDdEI4dkIscUJBQUssQ0FBQzUzQixNQUFOLEdBQWEsQ0FBYjs7QUFDQSxxQkFBSSxJQUFJRixDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNnK0IsRUFBRSxDQUFDOTlCLE1BQWpCLEVBQXdCRixDQUFDLEVBQXpCLEVBQTRCO0FBQUMsc0JBQUcsQ0FBQ2crQixFQUFFLENBQUNoK0IsQ0FBRCxDQUFGLENBQU1pK0IsU0FBVixFQUFvQjtBQUFDLHdCQUFHO0FBQUNELHdCQUFFLENBQUNoK0IsQ0FBRCxDQUFGLENBQU1xSCxRQUFOLENBQWU4d0IsSUFBZjtBQUFxQixxQkFBekIsQ0FBeUIsT0FBTXQyQixDQUFOLEVBQVE7QUFBQ0gsZ0NBQVUsQ0FBQyxZQUFVO0FBQUMsOEJBQU1HLENBQU47QUFBUSx1QkFBcEIsRUFBcUIsQ0FBckIsQ0FBVjtBQUFrQztBQUFDO0FBQUM7QUFBQyxlQUYvRyxFQUVnSCtILElBQUksQ0FBQ29ILEtBQUwsQ0FBV3lOLElBQVgsQ0FGaEgsQ0FBVjtBQUU0STs7QUFDNUlxWixpQkFBSyxDQUFDMTFCLElBQU4sQ0FBVztBQUFDODdCLG9CQUFNLEVBQUMsRUFBRXAwQixFQUFWO0FBQWF6QyxzQkFBUSxFQUFDQSxRQUF0QjtBQUErQjQyQix1QkFBUyxFQUFDO0FBQXpDLGFBQVg7QUFDQSxtQkFBT24wQixFQUFQO0FBQVUsV0FOVjs7QUFPQSt6QixhQUFHLEdBQUMsYUFBU0ssTUFBVCxFQUFnQjtBQUFDLGlCQUFJLElBQUlsK0IsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDODNCLEtBQUssQ0FBQzUzQixNQUFwQixFQUEyQkYsQ0FBQyxFQUE1QixFQUErQjtBQUFDLGtCQUFHODNCLEtBQUssQ0FBQzkzQixDQUFELENBQUwsQ0FBU2srQixNQUFULEtBQWtCQSxNQUFyQixFQUE0QjtBQUFDcEcscUJBQUssQ0FBQzkzQixDQUFELENBQUwsQ0FBU2krQixTQUFULEdBQW1CLElBQW5CO0FBQXdCO0FBQUM7QUFBQyxXQUE1RztBQUE2Rzs7QUFDN0czRSxjQUFNLENBQUNELE9BQVAsR0FBZSxVQUFTbjZCLEVBQVQsRUFBWTtBQUFDLGlCQUFPMCtCLEdBQUcsQ0FBQ2gyQixJQUFKLENBQVNvQyxJQUFULEVBQWM5SyxFQUFkLENBQVA7QUFBeUIsU0FBckQ7O0FBQ0FvNkIsY0FBTSxDQUFDRCxPQUFQLENBQWU4RSxNQUFmLEdBQXNCLFlBQVU7QUFBQ04sYUFBRyxDQUFDeDdCLEtBQUosQ0FBVTJILElBQVYsRUFBZS9CLFNBQWY7QUFBMEIsU0FBM0Q7O0FBQ0FxeEIsY0FBTSxDQUFDRCxPQUFQLENBQWUrRSxRQUFmLEdBQXdCLFlBQVU7QUFBQ3AwQixjQUFJLENBQUNULHFCQUFMLEdBQTJCcTBCLEdBQTNCO0FBQ25DNXpCLGNBQUksQ0FBQ0Qsb0JBQUwsR0FBMEI4ekIsR0FBMUI7QUFBOEIsU0FEOUI7QUFDK0IsT0Fmc1AsRUFlcFBqMkIsSUFmb1AsQ0FlL08sSUFmK08sRUFlMU8sT0FBTzYxQixNQUFQLEtBQWdCLFdBQWhCLEdBQTRCQSxNQUE1QixHQUFtQyxPQUFPNXdCLElBQVAsS0FBYyxXQUFkLEdBQTBCQSxJQUExQixHQUErQixPQUFPbE8sTUFBUCxLQUFnQixXQUFoQixHQUE0QkEsTUFBNUIsR0FBbUMsRUFmcUk7QUFlakksS0FmK0YsRUFlOUY7QUFBQyx5QkFBa0I7QUFBbkIsS0FmOEYsQ0EvQjhzQjtBQThDcnhCLE9BQUUsQ0FBQyxVQUFTZzdCLE9BQVQsRUFBaUJMLE1BQWpCLEVBQXdCRCxPQUF4QixFQUFnQztBQUFDOztBQUFhLFVBQUlnRixZQUFZLEdBQUMsWUFBVTtBQUFDLGlCQUFTQyxnQkFBVCxDQUEwQnZnQixNQUExQixFQUFpQ3dnQixLQUFqQyxFQUF1QztBQUFDLGVBQUksSUFBSXYrQixDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUN1K0IsS0FBSyxDQUFDcitCLE1BQXBCLEVBQTJCRixDQUFDLEVBQTVCLEVBQStCO0FBQUMsZ0JBQUl3K0IsVUFBVSxHQUFDRCxLQUFLLENBQUN2K0IsQ0FBRCxDQUFwQjtBQUF3QncrQixzQkFBVSxDQUFDQyxVQUFYLEdBQXNCRCxVQUFVLENBQUNDLFVBQVgsSUFBdUIsS0FBN0M7QUFBbURELHNCQUFVLENBQUNFLFlBQVgsR0FBd0IsSUFBeEI7QUFBNkIsZ0JBQUcsV0FBVUYsVUFBYixFQUF3QkEsVUFBVSxDQUFDRyxRQUFYLEdBQW9CLElBQXBCO0FBQXlCNXZCLGtCQUFNLENBQUM2dkIsY0FBUCxDQUFzQjdnQixNQUF0QixFQUE2QnlnQixVQUFVLENBQUNsYSxHQUF4QyxFQUE0Q2thLFVBQTVDO0FBQXlEO0FBQUM7O0FBQUEsZUFBTyxVQUFTSyxXQUFULEVBQXFCQyxVQUFyQixFQUFnQ0MsV0FBaEMsRUFBNEM7QUFBQyxjQUFHRCxVQUFILEVBQWNSLGdCQUFnQixDQUFDTyxXQUFXLENBQUNoL0IsU0FBYixFQUF1QmkvQixVQUF2QixDQUFoQjtBQUFtRCxjQUFHQyxXQUFILEVBQWVULGdCQUFnQixDQUFDTyxXQUFELEVBQWFFLFdBQWIsQ0FBaEI7QUFBMEMsaUJBQU9GLFdBQVA7QUFBb0IsU0FBbE07QUFBb00sT0FBMWUsRUFBakI7O0FBQThmLGVBQVNHLGVBQVQsQ0FBeUIxK0IsUUFBekIsRUFBa0N1K0IsV0FBbEMsRUFBOEM7QUFBQyxZQUFHLEVBQUV2K0IsUUFBUSxZQUFZdStCLFdBQXRCLENBQUgsRUFBc0M7QUFBQyxnQkFBTSxJQUFJM0UsU0FBSixDQUFjLG1DQUFkLENBQU47QUFBMEQ7QUFBQzs7QUFDNTJCLFVBQUkrRSxNQUFNLEdBQUN0RixPQUFPLENBQUMsS0FBRCxDQUFsQjs7QUFBMEIsVUFBSXVGLFlBQVksR0FBQ3ZGLE9BQU8sQ0FBQyxlQUFELENBQXhCOztBQUEwQyxVQUFJd0YsT0FBTyxHQUFDO0FBQUNDLHFCQUFhLEVBQUMsRUFBZjtBQUFrQjFCLGVBQU8sRUFBQyxDQUFDLElBQUQsRUFBTSxDQUFDLFVBQUQsRUFBWSxRQUFaLENBQU4sRUFBNEIsQ0FBQyxPQUFELEVBQVMsS0FBVCxDQUE1QixFQUE0QyxDQUFDLEtBQUQsRUFBTyxHQUFQLENBQTVDLEVBQXdELENBQUMsTUFBRCxFQUFRLElBQVIsQ0FBeEQsQ0FBMUI7QUFBaUcyQixhQUFLLEVBQUMsU0FBU0EsS0FBVCxDQUFlNTJCLEtBQWYsRUFBcUJtYSxHQUFyQixFQUF5Qi9ZLEdBQXpCLEVBQTZCO0FBQUMsaUJBQU8rWSxHQUFHLEdBQUMvWSxHQUFKLEdBQVFwQixLQUFLLEdBQUNtYSxHQUFOLEdBQVVBLEdBQVYsR0FBY25hLEtBQUssR0FBQ29CLEdBQU4sR0FBVUEsR0FBVixHQUFjcEIsS0FBcEMsR0FBMENBLEtBQUssR0FBQ29CLEdBQU4sR0FBVUEsR0FBVixHQUFjcEIsS0FBSyxHQUFDbWEsR0FBTixHQUFVQSxHQUFWLEdBQWNuYSxLQUE3RTtBQUFvRixTQUF6TjtBQUEwTmpHLFlBQUksRUFBQyxTQUFTQSxJQUFULENBQWNhLE9BQWQsRUFBc0JNLElBQXRCLEVBQTJCO0FBQUMsaUJBQU93N0IsT0FBTyxDQUFDRyxXQUFSLENBQW9CajhCLE9BQU8sQ0FBQ2lZLFlBQVIsQ0FBcUIsVUFBUTNYLElBQTdCLENBQXBCLENBQVA7QUFBZ0UsU0FBM1Q7QUFBNFQyN0IsbUJBQVcsRUFBQyxTQUFTQSxXQUFULENBQXFCNzJCLEtBQXJCLEVBQTJCO0FBQUMsY0FBR0EsS0FBSyxLQUFHLE1BQVgsRUFBa0I7QUFBQyxtQkFBTyxJQUFQO0FBQWEsV0FBaEMsTUFBcUMsSUFBR0EsS0FBSyxLQUFHLE9BQVgsRUFBbUI7QUFBQyxtQkFBTyxLQUFQO0FBQWMsV0FBbEMsTUFBdUMsSUFBR0EsS0FBSyxLQUFHLE1BQVgsRUFBa0I7QUFBQyxtQkFBTyxJQUFQO0FBQWEsV0FBaEMsTUFBcUMsSUFBRyxDQUFDK3VCLEtBQUssQ0FBQzNhLFVBQVUsQ0FBQ3BVLEtBQUQsQ0FBWCxDQUFOLElBQTJCZ2xCLFFBQVEsQ0FBQ2hsQixLQUFELENBQXRDLEVBQThDO0FBQUMsbUJBQU9vVSxVQUFVLENBQUNwVSxLQUFELENBQWpCO0FBQTBCLFdBQXpFLE1BQTZFO0FBQUMsbUJBQU9BLEtBQVA7QUFBYztBQUFDLFNBQWxqQjtBQUFtakI4MkIsaUJBQVMsRUFBQyxTQUFTQSxTQUFULENBQW1COTJCLEtBQW5CLEVBQXlCO0FBQUMsaUJBQU9BLEtBQUssQ0FBQ3FELE9BQU4sQ0FBYyxTQUFkLEVBQXdCLFVBQVNzQixLQUFULEVBQWVveUIsU0FBZixFQUF5QjtBQUFDLG1CQUFPQSxTQUFTLEdBQUNBLFNBQVMsQ0FBQ3IwQixXQUFWLEVBQUQsR0FBeUIsRUFBekM7QUFBNkMsV0FBL0YsQ0FBUDtBQUF5RyxTQUFoc0I7QUFBaXNCczBCLGtCQUFVLEVBQUMsU0FBU0EsVUFBVCxDQUFvQnA4QixPQUFwQixFQUE0QjtBQUFDODdCLGlCQUFPLENBQUNwN0IsR0FBUixDQUFZVixPQUFaLEVBQW9CLFdBQXBCLEVBQWdDLHNDQUFoQztBQUF3RTg3QixpQkFBTyxDQUFDcDdCLEdBQVIsQ0FBWVYsT0FBWixFQUFvQixpQkFBcEIsRUFBc0MsYUFBdEM7QUFBcUQ4N0IsaUJBQU8sQ0FBQ3A3QixHQUFSLENBQVlWLE9BQVosRUFBb0IscUJBQXBCLEVBQTBDLFFBQTFDO0FBQXFELFNBQTM1QjtBQUE0NUJxOEIsd0JBQWdCLEVBQUMsU0FBU0EsZ0JBQVQsQ0FBMEJqM0IsS0FBMUIsRUFBZ0M7QUFBQyxjQUFJcEYsT0FBTyxHQUFDb0ksUUFBUSxDQUFDQyxhQUFULENBQXVCLEtBQXZCLENBQVo7QUFBQSxjQUEwQ2kwQixlQUFlLEdBQUMsS0FBMUQ7QUFBQSxjQUFnRUMsYUFBYSxHQUFDLElBQTlFO0FBQUEsY0FBbUZDLGNBQWMsR0FBQyxLQUFsRztBQUFBLGNBQXdHQyxXQUFXLEdBQUMsSUFBcEg7QUFBQSxjQUF5SEMsVUFBVSxHQUFDLElBQXBJOztBQUF5SSxlQUFJLElBQUkvL0IsQ0FBQyxHQUFDLENBQU4sRUFBUUMsQ0FBQyxHQUFDay9CLE9BQU8sQ0FBQ3pCLE9BQVIsQ0FBZ0J4OUIsTUFBOUIsRUFBcUNGLENBQUMsR0FBQ0MsQ0FBdkMsRUFBeUNELENBQUMsRUFBMUMsRUFBNkM7QUFBQyxnQkFBR20vQixPQUFPLENBQUN6QixPQUFSLENBQWdCMTlCLENBQWhCLE1BQXFCLElBQXhCLEVBQTZCO0FBQUM4L0IseUJBQVcsR0FBQ1gsT0FBTyxDQUFDekIsT0FBUixDQUFnQjE5QixDQUFoQixFQUFtQixDQUFuQixJQUFzQixXQUFsQztBQUE4QysvQix3QkFBVSxHQUFDWixPQUFPLENBQUN6QixPQUFSLENBQWdCMTlCLENBQWhCLEVBQW1CLENBQW5CLElBQXNCLFdBQWpDO0FBQThDLGFBQTFILE1BQThIO0FBQUM4L0IseUJBQVcsR0FBQyxXQUFaO0FBQXdCQyx3QkFBVSxHQUFDLFdBQVg7QUFBd0I7O0FBQ3A0QyxnQkFBRzE4QixPQUFPLENBQUMrSCxLQUFSLENBQWMyMEIsVUFBZCxNQUE0Qm5oQyxTQUEvQixFQUF5QztBQUFDK2dDLDZCQUFlLEdBQUMsSUFBaEI7QUFBcUI7QUFBTztBQUFDOztBQUN2RSxrQkFBT2wzQixLQUFQO0FBQWMsaUJBQUksSUFBSjtBQUFTbzNCLDRCQUFjLEdBQUNGLGVBQWY7QUFBK0I7O0FBQU0saUJBQUksSUFBSjtBQUFTLGtCQUFHQSxlQUFILEVBQW1CO0FBQUMsb0JBQUk1bEIsSUFBSSxHQUFDdE8sUUFBUSxDQUFDc08sSUFBVCxJQUFldE8sUUFBUSxDQUFDQyxhQUFULENBQXVCLE1BQXZCLENBQXhCO0FBQUEsb0JBQXVENFYsZUFBZSxHQUFDN1YsUUFBUSxDQUFDNlYsZUFBaEY7QUFBQSxvQkFBZ0cwZSxnQkFBZ0IsR0FBQzFlLGVBQWUsQ0FBQ2xXLEtBQWhCLENBQXNCa0ksUUFBdkk7QUFBQSxvQkFBZ0oyc0IsYUFBYSxHQUFDLEtBQTlKOztBQUFvSyxvQkFBRyxDQUFDeDBCLFFBQVEsQ0FBQ3NPLElBQWIsRUFBa0I7QUFBQ2ttQiwrQkFBYSxHQUFDLElBQWQ7QUFBbUIzZSxpQ0FBZSxDQUFDbFcsS0FBaEIsQ0FBc0JrSSxRQUF0QixHQUErQixRQUEvQjtBQUF3Q2dPLGlDQUFlLENBQUNqSSxXQUFoQixDQUE0QlUsSUFBNUI7QUFBa0NBLHNCQUFJLENBQUMzTyxLQUFMLENBQVdrSSxRQUFYLEdBQW9CLFFBQXBCO0FBQTZCeUcsc0JBQUksQ0FBQzNPLEtBQUwsQ0FBVzgwQixVQUFYLEdBQXNCLEVBQXRCO0FBQTBCOztBQUNwYW5tQixvQkFBSSxDQUFDVixXQUFMLENBQWlCaFcsT0FBakI7QUFBMEJBLHVCQUFPLENBQUMrSCxLQUFSLENBQWMyMEIsVUFBZCxJQUEwQiwwQkFBMUI7QUFBcURILDZCQUFhLEdBQUNqaEMsTUFBTSxDQUFDd2hDLGdCQUFQLENBQXdCOThCLE9BQXhCLEVBQWlDKzhCLGdCQUFqQyxDQUFrRE4sV0FBbEQsQ0FBZDtBQUE2RUQsOEJBQWMsR0FBQ0QsYUFBYSxLQUFHaGhDLFNBQWhCLElBQTJCZ2hDLGFBQWEsQ0FBQzEvQixNQUFkLEdBQXFCLENBQWhELElBQW1EMC9CLGFBQWEsS0FBRyxNQUFsRjtBQUF5RnRlLCtCQUFlLENBQUNsVyxLQUFoQixDQUFzQmtJLFFBQXRCLEdBQStCMHNCLGdCQUEvQjtBQUFnRGptQixvQkFBSSxDQUFDNE0sV0FBTCxDQUFpQnRqQixPQUFqQjs7QUFBMEIsb0JBQUc0OEIsYUFBSCxFQUFpQjtBQUFDbG1CLHNCQUFJLENBQUMvVSxlQUFMLENBQXFCLE9BQXJCO0FBQThCK1Usc0JBQUksQ0FBQ2dGLFVBQUwsQ0FBZ0I0SCxXQUFoQixDQUE0QjVNLElBQTVCO0FBQW1DO0FBQUM7O0FBQ25aO0FBRkE7O0FBR0EsaUJBQU84bEIsY0FBUDtBQUF1QixTQUx5RDtBQUt4RDk3QixXQUFHLEVBQUMsU0FBU0EsR0FBVCxDQUFhVixPQUFiLEVBQXFCMnpCLFFBQXJCLEVBQThCdnVCLEtBQTlCLEVBQW9DO0FBQUMsY0FBSXMzQixVQUFVLEdBQUNaLE9BQU8sQ0FBQ0MsYUFBUixDQUFzQnBJLFFBQXRCLENBQWY7O0FBQStDLGNBQUcsQ0FBQytJLFVBQUosRUFBZTtBQUFDLGlCQUFJLElBQUkvL0IsQ0FBQyxHQUFDLENBQU4sRUFBUUMsQ0FBQyxHQUFDay9CLE9BQU8sQ0FBQ3pCLE9BQVIsQ0FBZ0J4OUIsTUFBOUIsRUFBcUNGLENBQUMsR0FBQ0MsQ0FBdkMsRUFBeUNELENBQUMsRUFBMUMsRUFBNkM7QUFBQyxrQkFBR20vQixPQUFPLENBQUN6QixPQUFSLENBQWdCMTlCLENBQWhCLE1BQXFCLElBQXhCLEVBQTZCO0FBQUMrL0IsMEJBQVUsR0FBQ1osT0FBTyxDQUFDSSxTQUFSLENBQWtCSixPQUFPLENBQUN6QixPQUFSLENBQWdCMTlCLENBQWhCLEVBQW1CLENBQW5CLElBQXNCLEdBQXRCLEdBQTBCZzNCLFFBQTVDLENBQVg7QUFBa0UsZUFBaEcsTUFBb0c7QUFBQytJLDBCQUFVLEdBQUMvSSxRQUFYO0FBQXFCOztBQUN4UyxrQkFBRzN6QixPQUFPLENBQUMrSCxLQUFSLENBQWMyMEIsVUFBZCxNQUE0Qm5oQyxTQUEvQixFQUF5QztBQUFDdWdDLHVCQUFPLENBQUNDLGFBQVIsQ0FBc0JwSSxRQUF0QixJQUFnQytJLFVBQWhDO0FBQTJDO0FBQU87QUFBQztBQUFDOztBQUM5RjE4QixpQkFBTyxDQUFDK0gsS0FBUixDQUFjMjBCLFVBQWQsSUFBMEJ0M0IsS0FBMUI7QUFBaUM7QUFQK0MsT0FBWjtBQU9qQyxVQUFJNDNCLFlBQVksR0FBQyxFQUFqQjtBQUFBLFVBQW9CQyxRQUFRLEdBQUM7QUFBQ0MscUJBQWEsRUFBQyxLQUFmO0FBQXFCQyx5QkFBaUIsRUFBQyxLQUF2QztBQUE2Q0Msb0JBQVksRUFBQyxJQUExRDtBQUErREMsaUJBQVMsRUFBQyxLQUF6RTtBQUErRUMsNEJBQW9CLEVBQUMsR0FBcEc7QUFBd0dDLHdCQUFnQixFQUFDLEdBQXpIO0FBQTZIQyxvQkFBWSxFQUFDLEdBQTFJO0FBQThJQyxrQkFBVSxFQUFDLEtBQXpKO0FBQStKQyxrQkFBVSxFQUFDLElBQTFLO0FBQStLQyxlQUFPLEVBQUMsSUFBdkw7QUFBNExDLGVBQU8sRUFBQyxJQUFwTTtBQUF5TUMsY0FBTSxFQUFDLEtBQWhOO0FBQXNOQyxjQUFNLEVBQUMsS0FBN047QUFBbU9DLGVBQU8sRUFBQyxJQUEzTztBQUFnUEMsZUFBTyxFQUFDLElBQXhQO0FBQTZQQyxpQkFBUyxFQUFDLEdBQXZRO0FBQTJRQyxpQkFBUyxFQUFDLEdBQXJSO0FBQXlSQyxlQUFPLEVBQUMsR0FBalM7QUFBcVNDLGVBQU8sRUFBQyxHQUE3UztBQUFpVEMscUJBQWEsRUFBQyxLQUEvVDtBQUFxVUMsaUJBQVMsRUFBQyxDQUEvVTtBQUFpVkMsZUFBTyxFQUFDLElBQXpWO0FBQThWQyxnQkFBUSxFQUFDO0FBQXZXLE9BQTdCOztBQUEwWSxVQUFJQyxRQUFRLEdBQUMsWUFBVTtBQUFDLGlCQUFTQSxRQUFULENBQWtCeitCLE9BQWxCLEVBQTBCbUgsT0FBMUIsRUFBa0M7QUFBQ3cwQix5QkFBZSxDQUFDLElBQUQsRUFBTThDLFFBQU4sQ0FBZjs7QUFBK0IsZUFBS3orQixPQUFMLEdBQWFBLE9BQWI7QUFBcUIsY0FBSWIsSUFBSSxHQUFDO0FBQUNzK0Isc0JBQVUsRUFBQzNCLE9BQU8sQ0FBQzM4QixJQUFSLENBQWEsS0FBS2EsT0FBbEIsRUFBMEIsYUFBMUIsQ0FBWjtBQUFxRDA5QixzQkFBVSxFQUFDNUIsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixhQUExQixDQUFoRTtBQUF5RzI5QixtQkFBTyxFQUFDN0IsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixVQUExQixDQUFqSDtBQUF1SjQ5QixtQkFBTyxFQUFDOUIsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixVQUExQixDQUEvSjtBQUFxTTY5QixrQkFBTSxFQUFDL0IsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixTQUExQixDQUE1TTtBQUFpUDg5QixrQkFBTSxFQUFDaEMsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixTQUExQixDQUF4UDtBQUE2Uis5QixtQkFBTyxFQUFDakMsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixVQUExQixDQUFyUztBQUEyVWcrQixtQkFBTyxFQUFDbEMsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixVQUExQixDQUFuVjtBQUF5WGkrQixxQkFBUyxFQUFDbkMsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixZQUExQixDQUFuWTtBQUEyYWsrQixxQkFBUyxFQUFDcEMsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixZQUExQixDQUFyYjtBQUE2ZG0rQixtQkFBTyxFQUFDckMsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixVQUExQixDQUFyZTtBQUEyZ0JvK0IsbUJBQU8sRUFBQ3RDLE9BQU8sQ0FBQzM4QixJQUFSLENBQWEsS0FBS2EsT0FBbEIsRUFBMEIsVUFBMUIsQ0FBbmhCO0FBQXlqQnErQix5QkFBYSxFQUFDdkMsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixnQkFBMUIsQ0FBdmtCO0FBQW1uQnMrQixxQkFBUyxFQUFDeEMsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixXQUExQixDQUE3bkI7QUFBb3FCazlCLHlCQUFhLEVBQUNwQixPQUFPLENBQUMzOEIsSUFBUixDQUFhLEtBQUthLE9BQWxCLEVBQTBCLGdCQUExQixDQUFsckI7QUFBOHRCbTlCLDZCQUFpQixFQUFDckIsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixxQkFBMUIsQ0FBaHZCO0FBQWl5QnE5QixxQkFBUyxFQUFDdkIsT0FBTyxDQUFDMzhCLElBQVIsQ0FBYSxLQUFLYSxPQUFsQixFQUEwQixZQUExQixDQUEzeUI7QUFBbTFCbzlCLHdCQUFZLEVBQUNoMUIsUUFBUSxDQUFDNkosYUFBVCxDQUF1QjZwQixPQUFPLENBQUMzOEIsSUFBUixDQUFhLEtBQUthLE9BQWxCLEVBQTBCLGVBQTFCLENBQXZCLENBQWgyQjtBQUFtNkJ3K0Isb0JBQVEsRUFBQzFDLE9BQU8sQ0FBQzM4QixJQUFSLENBQWEsS0FBS2EsT0FBbEIsRUFBMEIsVUFBMUI7QUFBNTZCLFdBQVQ7O0FBQTQ5QixlQUFJLElBQUlpaEIsR0FBUixJQUFlOWhCLElBQWYsRUFBb0I7QUFBQyxnQkFBR0EsSUFBSSxDQUFDOGhCLEdBQUQsQ0FBSixLQUFZLElBQWYsRUFBb0I7QUFBQyxxQkFBTzloQixJQUFJLENBQUM4aEIsR0FBRCxDQUFYO0FBQWtCO0FBQUM7O0FBQ3JqRDRhLHNCQUFZLENBQUMsSUFBRCxFQUFNb0IsUUFBTixFQUFlOTlCLElBQWYsRUFBb0JnSSxPQUFwQixDQUFaOztBQUF5QyxjQUFHLENBQUMsS0FBS2kyQixZQUFULEVBQXNCO0FBQUMsaUJBQUtBLFlBQUwsR0FBa0IsS0FBS3A5QixPQUF2QjtBQUFnQzs7QUFDaEcsZUFBSzArQixnQkFBTCxHQUFzQixJQUF0QjtBQUEyQixlQUFLQyxlQUFMLEdBQXFCLElBQXJCO0FBQTBCLGVBQUtDLE9BQUwsR0FBYSxLQUFiO0FBQW1CLGVBQUtDLE9BQUwsR0FBYSxFQUFiO0FBQWdCLGVBQUtDLE9BQUwsR0FBYSxFQUFiO0FBQWdCLGVBQUt2RSxHQUFMLEdBQVMsSUFBVDtBQUFjLGVBQUt3RSxNQUFMLEdBQVksSUFBWjtBQUFpQixlQUFLQyxnQkFBTCxHQUFzQixDQUF0QjtBQUF3QixlQUFLQyxnQkFBTCxHQUFzQixDQUF0QjtBQUF3QixlQUFLQyxZQUFMLEdBQWtCLENBQWxCO0FBQW9CLGVBQUtDLGFBQUwsR0FBbUIsQ0FBbkI7QUFBcUIsZUFBS0MsY0FBTCxHQUFvQixDQUFwQjtBQUFzQixlQUFLQyxjQUFMLEdBQW9CLENBQXBCO0FBQXNCLGVBQUtDLGFBQUwsR0FBbUIsQ0FBbkI7QUFBcUIsZUFBS0MsYUFBTCxHQUFtQixDQUFuQjtBQUFxQixlQUFLQyxZQUFMLEdBQWtCLENBQWxCO0FBQW9CLGVBQUtDLFlBQUwsR0FBa0IsQ0FBbEI7QUFBb0IsZUFBS0MsTUFBTCxHQUFZLENBQVo7QUFBYyxlQUFLQyxNQUFMLEdBQVksQ0FBWjtBQUFjLGVBQUtDLE9BQUwsR0FBYSxDQUFiO0FBQWUsZUFBS0MsT0FBTCxHQUFhLENBQWI7QUFBZSxlQUFLQyxTQUFMLEdBQWUsQ0FBZjtBQUFpQixlQUFLQyxTQUFMLEdBQWUsQ0FBZjtBQUFpQixlQUFLQyxXQUFMLEdBQWlCLEtBQUtBLFdBQUwsQ0FBaUJuN0IsSUFBakIsQ0FBc0IsSUFBdEIsQ0FBakI7QUFBNkMsZUFBS283QixtQkFBTCxHQUF5QixLQUFLQSxtQkFBTCxDQUF5QnA3QixJQUF6QixDQUE4QixJQUE5QixDQUF6QjtBQUE2RCxlQUFLcTdCLGNBQUwsR0FBb0IsS0FBS0EsY0FBTCxDQUFvQnI3QixJQUFwQixDQUF5QixJQUF6QixDQUFwQjtBQUFtRCxlQUFLczdCLGtCQUFMLEdBQXdCLEtBQUtBLGtCQUFMLENBQXdCdDdCLElBQXhCLENBQTZCLElBQTdCLENBQXhCO0FBQTJELGVBQUt1N0IsYUFBTCxHQUFtQixLQUFLQSxhQUFMLENBQW1CdjdCLElBQW5CLENBQXdCLElBQXhCLENBQW5CO0FBQWlELGVBQUt3N0Isa0JBQUwsR0FBd0IsS0FBS0Esa0JBQUwsQ0FBd0J4N0IsSUFBeEIsQ0FBNkIsSUFBN0IsQ0FBeEI7QUFBMkQsZUFBS3k3QixnQkFBTCxHQUFzQixLQUFLQSxnQkFBTCxDQUFzQno3QixJQUF0QixDQUEyQixJQUEzQixDQUF0QjtBQUF1RCxlQUFLMDdCLGNBQUwsR0FBb0IsS0FBS0EsY0FBTCxDQUFvQjE3QixJQUFwQixDQUF5QixJQUF6QixDQUFwQjtBQUFtRCxlQUFLMjdCLFdBQUwsR0FBaUIsSUFBakI7QUFBc0IsZUFBS3pPLFlBQUwsR0FBa0IsSUFBbEI7QUFBdUIsZUFBSzBPLGFBQUwsR0FBbUIsSUFBbkI7QUFBd0IsZUFBS0MsYUFBTCxHQUFtQixJQUFuQjtBQUF3QixlQUFLQyxhQUFMLEdBQW1CLElBQW5CO0FBQXdCLGVBQUtDLGFBQUwsR0FBbUIsSUFBbkI7QUFBd0IsZUFBS0MsUUFBTCxHQUFjLEtBQWQ7QUFBb0IsZUFBS0MsT0FBTCxHQUFhLENBQUNsdUIsU0FBUyxDQUFDa2pCLFNBQVYsQ0FBb0IvckIsS0FBcEIsQ0FBMEIsNEVBQTFCLENBQWQ7QUFBc0gsZUFBS2czQixhQUFMLEdBQW1CLENBQUMsQ0FBQ3psQyxNQUFNLENBQUMwbEMsaUJBQVQsSUFBNEIsQ0FBQyxLQUFLRixPQUFyRDtBQUE2RCxlQUFLRyxrQkFBTCxHQUF3QixDQUFDLENBQUMzbEMsTUFBTSxDQUFDNGxDLHNCQUFULElBQWlDLENBQUMsS0FBS0osT0FBL0Q7QUFBdUUsZUFBS0ssaUJBQUwsR0FBdUIsQ0FBdkI7QUFBeUIsZUFBS0MsWUFBTCxHQUFrQixDQUFsQjtBQUFvQixlQUFLQyxVQUFMO0FBQW1COztBQUNuMENyRyxvQkFBWSxDQUFDeUQsUUFBRCxFQUFVLENBQUM7QUFBQ3hkLGFBQUcsRUFBQyxZQUFMO0FBQWtCN2IsZUFBSyxFQUFDLFNBQVNpOEIsVUFBVCxHQUFxQjtBQUFDLGdCQUFHLEtBQUtDLGtCQUFMLEtBQTBCL2xDLFNBQTdCLEVBQXVDO0FBQUMsbUJBQUsrbEMsa0JBQUwsR0FBd0J4RixPQUFPLENBQUNPLGdCQUFSLENBQXlCLElBQXpCLENBQXhCO0FBQXVELG1CQUFLa0Ysa0JBQUwsR0FBd0J6RixPQUFPLENBQUNPLGdCQUFSLENBQXlCLElBQXpCLENBQXhCO0FBQXdEOztBQUM1TixnQkFBRyxLQUFLa0Ysa0JBQVIsRUFBMkI7QUFBQ3pGLHFCQUFPLENBQUNNLFVBQVIsQ0FBbUIsS0FBS3A4QixPQUF4QjtBQUFrQzs7QUFDOUQsZ0JBQUkrSCxLQUFLLEdBQUN6TSxNQUFNLENBQUN3aEMsZ0JBQVAsQ0FBd0IsS0FBSzk4QixPQUE3QixDQUFWOztBQUFnRCxnQkFBRytILEtBQUssQ0FBQ2cxQixnQkFBTixDQUF1QixVQUF2QixNQUFxQyxRQUF4QyxFQUFpRDtBQUFDLG1CQUFLLzhCLE9BQUwsQ0FBYStILEtBQWIsQ0FBbUJ5RSxRQUFuQixHQUE0QixVQUE1QjtBQUF3Qzs7QUFDMUksZ0JBQUcsQ0FBQyxLQUFLNnhCLGFBQVQsRUFBdUI7QUFBQyxtQkFBS3IrQixPQUFMLENBQWErSCxLQUFiLENBQW1CczJCLGFBQW5CLEdBQWlDLE1BQWpDO0FBQXlDOztBQUNqRSxpQkFBS21ELFlBQUw7QUFBb0IsaUJBQUtDLGdCQUFMO0FBQXdCLGlCQUFLQyxNQUFMO0FBQWMsaUJBQUtDLGdCQUFMLENBQXNCLEtBQUtwRSxnQkFBM0I7QUFBOEM7QUFKakYsU0FBRCxFQUlvRjtBQUFDdGMsYUFBRyxFQUFDLGlCQUFMO0FBQXVCN2IsZUFBSyxFQUFDLFNBQVN3OEIsZUFBVCxHQUEwQjtBQUFDLGdCQUFHLEtBQUtyRCxPQUFSLEVBQWdCO0FBQUMsbUJBQUtBLE9BQUw7QUFBZ0I7QUFBQztBQUExRixTQUpwRixFQUlnTDtBQUFDdGQsYUFBRyxFQUFDLGNBQUw7QUFBb0I3YixlQUFLLEVBQUMsU0FBU284QixZQUFULEdBQXVCO0FBQUMsZ0JBQUcsS0FBS2hELFFBQVIsRUFBaUI7QUFBQyxtQkFBS3FELE1BQUwsR0FBWSxLQUFLN2hDLE9BQUwsQ0FBYTZYLGdCQUFiLENBQThCLEtBQUsybUIsUUFBbkMsQ0FBWjtBQUEwRCxhQUE1RSxNQUFnRjtBQUFDLG1CQUFLcUQsTUFBTCxHQUFZLEtBQUs3aEMsT0FBTCxDQUFhcVcsUUFBekI7QUFBbUM7O0FBQzVXLGdCQUFHLENBQUMsS0FBS3dyQixNQUFMLENBQVlobEMsTUFBaEIsRUFBdUI7QUFBQ21LLHFCQUFPLENBQUM4NkIsSUFBUixDQUFhLGtEQUFiO0FBQWtFOztBQUMxRixpQkFBS2pELE9BQUwsR0FBYSxFQUFiO0FBQWdCLGlCQUFLQyxPQUFMLEdBQWEsRUFBYjs7QUFBZ0IsaUJBQUksSUFBSW4xQixLQUFLLEdBQUMsQ0FBZCxFQUFnQkEsS0FBSyxHQUFDLEtBQUtrNEIsTUFBTCxDQUFZaGxDLE1BQWxDLEVBQXlDOE0sS0FBSyxFQUE5QyxFQUFpRDtBQUFDLGtCQUFJbzRCLEtBQUssR0FBQyxLQUFLRixNQUFMLENBQVlsNEIsS0FBWixDQUFWOztBQUE2QixrQkFBRyxLQUFLNDNCLGtCQUFSLEVBQTJCO0FBQUN6Rix1QkFBTyxDQUFDTSxVQUFSLENBQW1CMkYsS0FBbkI7QUFBMkI7O0FBQ3RLQSxtQkFBSyxDQUFDaDZCLEtBQU4sQ0FBWXlFLFFBQVosR0FBcUI3QyxLQUFLLEdBQUMsVUFBRCxHQUFZLFVBQXRDO0FBQWlEbzRCLG1CQUFLLENBQUNoNkIsS0FBTixDQUFZOFYsT0FBWixHQUFvQixPQUFwQjtBQUE0QmtrQixtQkFBSyxDQUFDaDZCLEtBQU4sQ0FBWXpFLElBQVosR0FBaUIsQ0FBakI7QUFBbUJ5K0IsbUJBQUssQ0FBQ2g2QixLQUFOLENBQVk3RSxHQUFaLEdBQWdCLENBQWhCO0FBQWtCLGtCQUFJOCtCLEtBQUssR0FBQ2xHLE9BQU8sQ0FBQzM4QixJQUFSLENBQWE0aUMsS0FBYixFQUFtQixPQUFuQixLQUE2QixDQUF2QztBQUF5QyxtQkFBS2xELE9BQUwsQ0FBYTkvQixJQUFiLENBQWtCKzhCLE9BQU8sQ0FBQzM4QixJQUFSLENBQWE0aUMsS0FBYixFQUFtQixTQUFuQixLQUErQkMsS0FBakQ7QUFBd0QsbUJBQUtsRCxPQUFMLENBQWEvL0IsSUFBYixDQUFrQis4QixPQUFPLENBQUMzOEIsSUFBUixDQUFhNGlDLEtBQWIsRUFBbUIsU0FBbkIsS0FBK0JDLEtBQWpEO0FBQXlEO0FBQUM7QUFIdkUsU0FKaEwsRUFPeVA7QUFBQy9nQixhQUFHLEVBQUMsa0JBQUw7QUFBd0I3YixlQUFLLEVBQUMsU0FBU3E4QixnQkFBVCxHQUEyQjtBQUFDLGlCQUFLakIsV0FBTCxHQUFpQmxsQyxNQUFNLENBQUN5dUIsVUFBeEI7QUFBbUMsaUJBQUtnSSxZQUFMLEdBQWtCejJCLE1BQU0sQ0FBQ3l2QixXQUF6QjtBQUFxQyxpQkFBSzBWLGFBQUwsR0FBbUIsS0FBS0QsV0FBTCxHQUFpQixLQUFLckMsT0FBekM7QUFBaUQsaUJBQUt1QyxhQUFMLEdBQW1CLEtBQUszTyxZQUFMLEdBQWtCLEtBQUtxTSxPQUExQztBQUFrRCxpQkFBS3VDLGFBQUwsR0FBbUJwNkIsSUFBSSxDQUFDQyxHQUFMLENBQVMsS0FBS2k2QixhQUFkLEVBQTRCLEtBQUtELFdBQUwsR0FBaUIsS0FBS0MsYUFBbEQsQ0FBbkI7QUFBb0YsaUJBQUtHLGFBQUwsR0FBbUJyNkIsSUFBSSxDQUFDQyxHQUFMLENBQVMsS0FBS2s2QixhQUFkLEVBQTRCLEtBQUszTyxZQUFMLEdBQWtCLEtBQUsyTyxhQUFuRCxDQUFuQjtBQUFzRjtBQUEvWSxTQVB6UCxFQU8wb0I7QUFBQ3pmLGFBQUcsRUFBQyxjQUFMO0FBQW9CN2IsZUFBSyxFQUFDLFNBQVM2OEIsWUFBVCxHQUF1QjtBQUFDLGlCQUFLbEQsTUFBTCxHQUFZLEtBQUszQixZQUFMLENBQWtCeDZCLHFCQUFsQixFQUFaO0FBQXNELGlCQUFLbzhCLGdCQUFMLEdBQXNCLEtBQUtELE1BQUwsQ0FBWXo3QixJQUFsQztBQUF1QyxpQkFBSzI3QixnQkFBTCxHQUFzQixLQUFLRixNQUFMLENBQVk3N0IsR0FBbEM7QUFBc0MsaUJBQUtnOEIsWUFBTCxHQUFrQixLQUFLSCxNQUFMLENBQVl2N0IsS0FBOUI7QUFBb0MsaUJBQUsyN0IsYUFBTCxHQUFtQixLQUFLSixNQUFMLENBQVl0N0IsTUFBL0I7QUFBc0MsaUJBQUsyN0IsY0FBTCxHQUFvQixLQUFLRixZQUFMLEdBQWtCLEtBQUtmLE9BQTNDO0FBQW1ELGlCQUFLa0IsY0FBTCxHQUFvQixLQUFLRixhQUFMLEdBQW1CLEtBQUtmLE9BQTVDO0FBQW9ELGlCQUFLa0IsYUFBTCxHQUFtQi80QixJQUFJLENBQUNDLEdBQUwsQ0FBUyxLQUFLNDRCLGNBQWQsRUFBNkIsS0FBS0YsWUFBTCxHQUFrQixLQUFLRSxjQUFwRCxDQUFuQjtBQUF1RixpQkFBS0csYUFBTCxHQUFtQmg1QixJQUFJLENBQUNDLEdBQUwsQ0FBUyxLQUFLNjRCLGNBQWQsRUFBNkIsS0FBS0YsYUFBTCxHQUFtQixLQUFLRSxjQUFyRCxDQUFuQjtBQUF5RjtBQUF0aEIsU0FQMW9CLEVBT2txQztBQUFDcGUsYUFBRyxFQUFDLGtCQUFMO0FBQXdCN2IsZUFBSyxFQUFDLFNBQVN1OEIsZ0JBQVQsQ0FBMEJ2akMsS0FBMUIsRUFBZ0M7QUFBQ29HLHdCQUFZLENBQUMsS0FBS2s2QixnQkFBTixDQUFaO0FBQW9DLGlCQUFLQSxnQkFBTCxHQUFzQnJnQyxVQUFVLENBQUMsS0FBS2dpQyxrQkFBTixFQUF5QmppQyxLQUF6QixDQUFoQztBQUFpRTtBQUFwSyxTQVBscUMsRUFPdzBDO0FBQUM2aUIsYUFBRyxFQUFDLFFBQUw7QUFBYzdiLGVBQUssRUFBQyxTQUFTczhCLE1BQVQsR0FBaUI7QUFBQyxnQkFBRyxLQUFLOUMsT0FBUixFQUFnQjtBQUFDO0FBQVE7O0FBQzc1QyxpQkFBS0EsT0FBTCxHQUFhLElBQWI7O0FBQWtCLGdCQUFHLEtBQUtxQyxrQkFBUixFQUEyQjtBQUFDLG1CQUFLSixRQUFMLEdBQWMsS0FBZDtBQUFvQnZsQyxvQkFBTSxDQUFDMGdCLGdCQUFQLENBQXdCLG1CQUF4QixFQUE0QyxLQUFLaWtCLG1CQUFqRDtBQUFzRSxtQkFBS2lDLGNBQUwsR0FBb0I3akMsVUFBVSxDQUFDLEtBQUs4aEMsa0JBQU4sRUFBeUIsS0FBSzNDLFlBQTlCLENBQTlCO0FBQTJFLGFBQWpNLE1BQXNNLElBQUcsS0FBS3VELGFBQVIsRUFBc0I7QUFBQyxtQkFBS0YsUUFBTCxHQUFjLEtBQWQ7QUFBb0J2bEMsb0JBQU0sQ0FBQzBnQixnQkFBUCxDQUF3QixjQUF4QixFQUF1QyxLQUFLa2tCLGNBQTVDO0FBQTRELG1CQUFLZ0MsY0FBTCxHQUFvQjdqQyxVQUFVLENBQUMsS0FBSytoQyxhQUFOLEVBQW9CLEtBQUs1QyxZQUF6QixDQUE5QjtBQUFzRSxhQUE3SyxNQUFpTDtBQUFDLG1CQUFLZ0MsWUFBTCxHQUFrQixDQUFsQjtBQUFvQixtQkFBS0MsWUFBTCxHQUFrQixDQUFsQjtBQUFvQixtQkFBS29CLFFBQUwsR0FBYyxLQUFkO0FBQW9CdmxDLG9CQUFNLENBQUMwZ0IsZ0JBQVAsQ0FBd0IsV0FBeEIsRUFBb0MsS0FBS2drQixXQUF6QztBQUFzRCxtQkFBSzRCLGVBQUw7QUFBd0I7O0FBQ3BoQnRtQyxrQkFBTSxDQUFDMGdCLGdCQUFQLENBQXdCLFFBQXhCLEVBQWlDLEtBQUt1a0IsY0FBdEM7QUFBc0QsaUJBQUtoRyxHQUFMLEdBQVNxQixNQUFNLENBQUMsS0FBSzBFLGdCQUFOLENBQWY7QUFBd0M7QUFGZ3dDLFNBUHgwQyxFQVMwRTtBQUFDcmYsYUFBRyxFQUFDLFNBQUw7QUFBZTdiLGVBQUssRUFBQyxTQUFTKzhCLE9BQVQsR0FBa0I7QUFBQyxnQkFBRyxDQUFDLEtBQUt2RCxPQUFULEVBQWlCO0FBQUM7QUFBUTs7QUFDbEssaUJBQUtBLE9BQUwsR0FBYSxLQUFiOztBQUFtQixnQkFBRyxLQUFLcUMsa0JBQVIsRUFBMkI7QUFBQzNsQyxvQkFBTSxDQUFDeWdCLG1CQUFQLENBQTJCLG1CQUEzQixFQUErQyxLQUFLa2tCLG1CQUFwRDtBQUEwRSxhQUF0RyxNQUEyRyxJQUFHLEtBQUtjLGFBQVIsRUFBc0I7QUFBQ3psQyxvQkFBTSxDQUFDeWdCLG1CQUFQLENBQTJCLGNBQTNCLEVBQTBDLEtBQUtta0IsY0FBL0M7QUFBZ0UsYUFBdkYsTUFBMkY7QUFBQzVrQyxvQkFBTSxDQUFDeWdCLG1CQUFQLENBQTJCLFdBQTNCLEVBQXVDLEtBQUtpa0IsV0FBNUM7QUFBMEQ7O0FBQ3BSMWtDLGtCQUFNLENBQUN5Z0IsbUJBQVAsQ0FBMkIsUUFBM0IsRUFBb0MsS0FBS3drQixjQUF6QztBQUF5RDNFLGtCQUFNLENBQUNkLE1BQVAsQ0FBYyxLQUFLUCxHQUFuQjtBQUF5QjtBQUZjLFNBVDFFLEVBVzhEO0FBQUN0WixhQUFHLEVBQUMsV0FBTDtBQUFpQjdiLGVBQUssRUFBQyxTQUFTZzlCLFNBQVQsQ0FBbUJqNEIsQ0FBbkIsRUFBcUJDLENBQXJCLEVBQXVCO0FBQUMsaUJBQUtxekIsVUFBTCxHQUFnQnR6QixDQUFDLEtBQUc1TyxTQUFKLEdBQWMsS0FBS2tpQyxVQUFuQixHQUE4QnR6QixDQUE5QztBQUFnRCxpQkFBS3V6QixVQUFMLEdBQWdCdHpCLENBQUMsS0FBRzdPLFNBQUosR0FBYyxLQUFLbWlDLFVBQW5CLEdBQThCdHpCLENBQTlDO0FBQWlEO0FBQWhKLFNBWDlELEVBV2dOO0FBQUM2VyxhQUFHLEVBQUMsUUFBTDtBQUFjN2IsZUFBSyxFQUFDLFNBQVNpOUIsTUFBVCxDQUFnQmw0QixDQUFoQixFQUFrQkMsQ0FBbEIsRUFBb0I7QUFBQyxpQkFBS3V6QixPQUFMLEdBQWF4ekIsQ0FBQyxLQUFHNU8sU0FBSixHQUFjLEtBQUtvaUMsT0FBbkIsR0FBMkJ4ekIsQ0FBeEM7QUFBMEMsaUJBQUt5ekIsT0FBTCxHQUFheHpCLENBQUMsS0FBRzdPLFNBQUosR0FBYyxLQUFLcWlDLE9BQW5CLEdBQTJCeHpCLENBQXhDO0FBQTJDO0FBQTlILFNBWGhOLEVBV2dWO0FBQUM2VyxhQUFHLEVBQUMsVUFBTDtBQUFnQjdiLGVBQUssRUFBQyxTQUFTa0csUUFBVCxDQUFrQm5CLENBQWxCLEVBQW9CQyxDQUFwQixFQUFzQjtBQUFDLGlCQUFLNnpCLFNBQUwsR0FBZTl6QixDQUFDLEtBQUc1TyxTQUFKLEdBQWMsS0FBSzBpQyxTQUFuQixHQUE2Qjl6QixDQUE1QztBQUE4QyxpQkFBSyt6QixTQUFMLEdBQWU5ekIsQ0FBQyxLQUFHN08sU0FBSixHQUFjLEtBQUsyaUMsU0FBbkIsR0FBNkI5ekIsQ0FBNUM7QUFBK0M7QUFBMUksU0FYaFYsRUFXNGQ7QUFBQzZXLGFBQUcsRUFBQyxRQUFMO0FBQWM3YixlQUFLLEVBQUMsU0FBU2s5QixNQUFULENBQWdCbjRCLENBQWhCLEVBQWtCQyxDQUFsQixFQUFvQjtBQUFDLGlCQUFLMnpCLE9BQUwsR0FBYTV6QixDQUFDLEtBQUc1TyxTQUFKLEdBQWMsS0FBS3dpQyxPQUFuQixHQUEyQjV6QixDQUF4QztBQUEwQyxpQkFBSzZ6QixPQUFMLEdBQWE1ekIsQ0FBQyxLQUFHN08sU0FBSixHQUFjLEtBQUt5aUMsT0FBbkIsR0FBMkI1ekIsQ0FBeEM7QUFBMkM7QUFBOUgsU0FYNWQsRUFXNGxCO0FBQUM2VyxhQUFHLEVBQUMsT0FBTDtBQUFhN2IsZUFBSyxFQUFDLFNBQVNtRSxLQUFULENBQWVZLENBQWYsRUFBaUJDLENBQWpCLEVBQW1CO0FBQUMsaUJBQUt5ekIsTUFBTCxHQUFZMXpCLENBQUMsS0FBRzVPLFNBQUosR0FBYyxLQUFLc2lDLE1BQW5CLEdBQTBCMXpCLENBQXRDO0FBQXdDLGlCQUFLMnpCLE1BQUwsR0FBWTF6QixDQUFDLEtBQUc3TyxTQUFKLEdBQWMsS0FBS3VpQyxNQUFuQixHQUEwQjF6QixDQUF0QztBQUF5QztBQUF4SCxTQVg1bEIsRUFXc3RCO0FBQUM2VyxhQUFHLEVBQUMsUUFBTDtBQUFjN2IsZUFBSyxFQUFDLFNBQVNtOUIsTUFBVCxDQUFnQnA0QixDQUFoQixFQUFrQkMsQ0FBbEIsRUFBb0I7QUFBQyxpQkFBSyt6QixPQUFMLEdBQWFoMEIsQ0FBQyxLQUFHNU8sU0FBSixHQUFjLEtBQUs0aUMsT0FBbkIsR0FBMkJoMEIsQ0FBeEM7QUFBMEMsaUJBQUtpMEIsT0FBTCxHQUFhaDBCLENBQUMsS0FBRzdPLFNBQUosR0FBYyxLQUFLNmlDLE9BQW5CLEdBQTJCaDBCLENBQXhDO0FBQTJDO0FBQTlILFNBWHR0QixFQVdzMUI7QUFBQzZXLGFBQUcsRUFBQyxpQkFBTDtBQUF1QjdiLGVBQUssRUFBQyxTQUFTbzlCLGVBQVQsQ0FBeUJ4aUMsT0FBekIsRUFBaUM7QUFBQyxpQkFBS285QixZQUFMLEdBQWtCcDlCLE9BQWxCO0FBQTBCLGlCQUFLeWhDLGdCQUFMO0FBQXlCO0FBQWxILFNBWHQxQixFQVcwOEI7QUFBQ3hnQixhQUFHLEVBQUMsYUFBTDtBQUFtQjdiLGVBQUssRUFBQyxTQUFTcTlCLFdBQVQsQ0FBcUJ6aUMsT0FBckIsRUFBNkJtSyxDQUE3QixFQUErQkMsQ0FBL0IsRUFBaUM7QUFBQ0QsYUFBQyxHQUFDQSxDQUFDLENBQUN5ZSxPQUFGLENBQVUsS0FBSzBWLFNBQWYsSUFBMEIsSUFBNUI7QUFBaUNsMEIsYUFBQyxHQUFDQSxDQUFDLENBQUN3ZSxPQUFGLENBQVUsS0FBSzBWLFNBQWYsSUFBMEIsSUFBNUI7O0FBQWlDLGdCQUFHLEtBQUtpRCxrQkFBUixFQUEyQjtBQUFDekYscUJBQU8sQ0FBQ3A3QixHQUFSLENBQVlWLE9BQVosRUFBb0IsV0FBcEIsRUFBZ0MsaUJBQWVtSyxDQUFmLEdBQWlCLEdBQWpCLEdBQXFCQyxDQUFyQixHQUF1QixLQUF2RDtBQUErRCxhQUEzRixNQUFnRyxJQUFHLEtBQUtrM0Isa0JBQVIsRUFBMkI7QUFBQ3hGLHFCQUFPLENBQUNwN0IsR0FBUixDQUFZVixPQUFaLEVBQW9CLFdBQXBCLEVBQWdDLGVBQWFtSyxDQUFiLEdBQWUsR0FBZixHQUFtQkMsQ0FBbkIsR0FBcUIsR0FBckQ7QUFBMkQsYUFBdkYsTUFBMkY7QUFBQ3BLLHFCQUFPLENBQUMrSCxLQUFSLENBQWN6RSxJQUFkLEdBQW1CNkcsQ0FBbkI7QUFBcUJuSyxxQkFBTyxDQUFDK0gsS0FBUixDQUFjN0UsR0FBZCxHQUFrQmtILENBQWxCO0FBQXFCO0FBQUM7QUFBcFcsU0FYMThCLEVBV2d6QztBQUFDNlcsYUFBRyxFQUFDLG9CQUFMO0FBQTBCN2IsZUFBSyxFQUFDLFNBQVMrNkIsa0JBQVQsR0FBNkI7QUFBQyxnQkFBRyxLQUFLYyxrQkFBTCxJQUF5QixLQUFLRSxpQkFBTCxLQUF5QixDQUFyRCxFQUF1RDtBQUFDLG1CQUFLZ0IsT0FBTDtBQUFlLG1CQUFLbEIsa0JBQUwsR0FBd0IsS0FBeEI7QUFBOEIsbUJBQUtTLE1BQUw7QUFBZSxhQUFwSCxNQUF3SDtBQUFDLG1CQUFLRSxlQUFMO0FBQXdCO0FBQUM7QUFBaE4sU0FYaHpDLEVBV2tnRDtBQUFDM2dCLGFBQUcsRUFBQyxlQUFMO0FBQXFCN2IsZUFBSyxFQUFDLFNBQVNnN0IsYUFBVCxHQUF3QjtBQUFDLGdCQUFHLEtBQUtXLGFBQUwsSUFBb0IsS0FBS0ssWUFBTCxLQUFvQixDQUEzQyxFQUE2QztBQUFDLG1CQUFLZSxPQUFMO0FBQWUsbUJBQUtwQixhQUFMLEdBQW1CLEtBQW5CO0FBQXlCLG1CQUFLVyxNQUFMO0FBQWUsYUFBckcsTUFBeUc7QUFBQyxtQkFBS0UsZUFBTDtBQUF3QjtBQUFDO0FBQXZMLFNBWGxnRCxFQVcyckQ7QUFBQzNnQixhQUFHLEVBQUMsb0JBQUw7QUFBMEI3YixlQUFLLEVBQUMsU0FBU2k3QixrQkFBVCxHQUE2QjtBQUFDLGlCQUFLMUIsZUFBTCxHQUFxQixJQUFyQjtBQUEyQjtBQUF6RixTQVgzckQsRUFXc3hEO0FBQUMxZCxhQUFHLEVBQUMsZ0JBQUw7QUFBc0I3YixlQUFLLEVBQUMsU0FBU203QixjQUFULEdBQXlCO0FBQUMsaUJBQUtrQixnQkFBTDtBQUF5QjtBQUEvRSxTQVh0eEQsRUFXdTJEO0FBQUN4Z0IsYUFBRyxFQUFDLGtCQUFMO0FBQXdCN2IsZUFBSyxFQUFDLFNBQVNrN0IsZ0JBQVQsR0FBMkI7QUFBQyxpQkFBSzJCLFlBQUw7QUFBb0IsZ0JBQUlTLGdCQUFnQixHQUFDLEtBQUtoRCxNQUFMLEdBQVksS0FBS0YsWUFBdEM7QUFBQSxnQkFBbURtRCxnQkFBZ0IsR0FBQyxLQUFLaEQsTUFBTCxHQUFZLEtBQUtGLFlBQXJGOztBQUFrRyxnQkFBR2w1QixJQUFJLENBQUNpWixHQUFMLENBQVNrakIsZ0JBQVQsSUFBMkIsS0FBS3BGLG9CQUFoQyxJQUFzRC8yQixJQUFJLENBQUNpWixHQUFMLENBQVNtakIsZ0JBQVQsSUFBMkIsS0FBS3JGLG9CQUF6RixFQUE4RztBQUFDLG1CQUFLcUUsZ0JBQUwsQ0FBc0IsQ0FBdEI7QUFBMEI7O0FBQ3RyRSxnQkFBRyxLQUFLZCxRQUFSLEVBQWlCO0FBQUMsbUJBQUtqQixPQUFMLEdBQWEsS0FBS25DLFVBQUwsR0FBZ0JrRixnQkFBaEIsR0FBaUMsS0FBS2hELE1BQW5EO0FBQTBELG1CQUFLRSxPQUFMLEdBQWEsS0FBS25DLFVBQUwsR0FBZ0JnRixnQkFBaEIsR0FBaUMsS0FBS2hELE1BQW5EO0FBQTJELGFBQXZJLE1BQTJJO0FBQUMsbUJBQUtFLE9BQUwsR0FBYSxLQUFLbkMsVUFBTCxHQUFnQmlGLGdCQUFoQixHQUFpQyxLQUFLaEQsTUFBbkQ7QUFBMEQsbUJBQUtHLE9BQUwsR0FBYSxLQUFLbkMsVUFBTCxHQUFnQmlGLGdCQUFoQixHQUFpQyxLQUFLaEQsTUFBbkQ7QUFBMkQ7O0FBQ2pRLGlCQUFLQyxPQUFMLElBQWMsS0FBS1YsWUFBTCxJQUFtQixLQUFLbkIsT0FBTCxHQUFhLEdBQWhDLENBQWQ7QUFBbUQsaUJBQUs4QixPQUFMLElBQWMsS0FBS1YsYUFBTCxJQUFvQixLQUFLbkIsT0FBTCxHQUFhLEdBQWpDLENBQWQ7O0FBQW9ELGdCQUFHLENBQUM3SixLQUFLLENBQUMzYSxVQUFVLENBQUMsS0FBS3FrQixNQUFOLENBQVgsQ0FBVCxFQUFtQztBQUFDLG1CQUFLK0IsT0FBTCxHQUFhOUQsT0FBTyxDQUFDRSxLQUFSLENBQWMsS0FBSzRELE9BQW5CLEVBQTJCLENBQUMsS0FBSy9CLE1BQWpDLEVBQXdDLEtBQUtBLE1BQTdDLENBQWI7QUFBbUU7O0FBQzlNLGdCQUFHLENBQUMxSixLQUFLLENBQUMzYSxVQUFVLENBQUMsS0FBS3NrQixNQUFOLENBQVgsQ0FBVCxFQUFtQztBQUFDLG1CQUFLK0IsT0FBTCxHQUFhL0QsT0FBTyxDQUFDRSxLQUFSLENBQWMsS0FBSzZELE9BQW5CLEVBQTJCLENBQUMsS0FBSy9CLE1BQWpDLEVBQXdDLEtBQUtBLE1BQTdDLENBQWI7QUFBbUU7O0FBQ3ZHLGlCQUFLZ0MsU0FBTCxJQUFnQixDQUFDLEtBQUtGLE9BQUwsR0FBYSxLQUFLRSxTQUFuQixJQUE4QixLQUFLN0IsU0FBbkQ7QUFBNkQsaUJBQUs4QixTQUFMLElBQWdCLENBQUMsS0FBS0YsT0FBTCxHQUFhLEtBQUtFLFNBQW5CLElBQThCLEtBQUs3QixTQUFuRDs7QUFBNkQsaUJBQUksSUFBSXYwQixLQUFLLEdBQUMsQ0FBZCxFQUFnQkEsS0FBSyxHQUFDLEtBQUtrNEIsTUFBTCxDQUFZaGxDLE1BQWxDLEVBQXlDOE0sS0FBSyxFQUE5QyxFQUFpRDtBQUFDLGtCQUFJbzRCLEtBQUssR0FBQyxLQUFLRixNQUFMLENBQVlsNEIsS0FBWixDQUFWO0FBQUEsa0JBQTZCaTVCLE1BQU0sR0FBQyxLQUFLL0QsT0FBTCxDQUFhbDFCLEtBQWIsQ0FBcEM7QUFBQSxrQkFBd0RrNUIsTUFBTSxHQUFDLEtBQUsvRCxPQUFMLENBQWFuMUIsS0FBYixDQUEvRDtBQUFBLGtCQUFtRm01QixPQUFPLEdBQUMsS0FBS2hELFNBQUwsSUFBZ0I4QyxNQUFNLElBQUUsS0FBS2pGLE9BQUwsR0FBYSxDQUFDLENBQWQsR0FBZ0IsQ0FBbEIsQ0FBdEIsQ0FBM0Y7QUFBQSxrQkFBdUlvRixPQUFPLEdBQUMsS0FBS2hELFNBQUwsSUFBZ0I4QyxNQUFNLElBQUUsS0FBS2pGLE9BQUwsR0FBYSxDQUFDLENBQWQsR0FBZ0IsQ0FBbEIsQ0FBdEIsQ0FBL0k7QUFBMkwsbUJBQUs2RSxXQUFMLENBQWlCVixLQUFqQixFQUF1QmUsT0FBdkIsRUFBK0JDLE9BQS9CO0FBQXlDOztBQUNoWixpQkFBS3hJLEdBQUwsR0FBU3FCLE1BQU0sQ0FBQyxLQUFLMEUsZ0JBQU4sQ0FBZjtBQUF3QztBQUxxMUQsU0FYdjJELEVBZ0JvQjtBQUFDcmYsYUFBRyxFQUFDLFFBQUw7QUFBYzdiLGVBQUssRUFBQyxTQUFTNDlCLE1BQVQsQ0FBZ0JDLElBQWhCLEVBQXFCQyxLQUFyQixFQUEyQjtBQUFDLGdCQUFJLzRCLENBQUMsR0FBQyxDQUFDODRCLElBQUksSUFBRSxDQUFQLElBQVVqRyxZQUFoQjtBQUFBLGdCQUE2QjV5QixDQUFDLEdBQUMsQ0FBQzg0QixLQUFLLElBQUUsQ0FBUixJQUFXbEcsWUFBMUM7QUFBdUQsZ0JBQUk2RCxRQUFRLEdBQUMsS0FBSzlPLFlBQUwsR0FBa0IsS0FBS3lPLFdBQXBDOztBQUFnRCxnQkFBRyxLQUFLSyxRQUFMLEtBQWdCQSxRQUFuQixFQUE0QjtBQUFDLG1CQUFLQSxRQUFMLEdBQWNBLFFBQWQ7QUFBdUIsbUJBQUtsQyxlQUFMLEdBQXFCLElBQXJCO0FBQTJCOztBQUNoUixnQkFBRyxLQUFLQSxlQUFSLEVBQXdCO0FBQUMsbUJBQUtBLGVBQUwsR0FBcUIsS0FBckI7QUFBMkIsbUJBQUthLFlBQUwsR0FBa0JyMUIsQ0FBbEI7QUFBb0IsbUJBQUtzMUIsWUFBTCxHQUFrQnIxQixDQUFsQjtBQUFxQjs7QUFDN0YsaUJBQUtzMUIsTUFBTCxHQUFZdjFCLENBQVo7QUFBYyxpQkFBS3cxQixNQUFMLEdBQVl2MUIsQ0FBWjtBQUFlO0FBRmEsU0FoQnBCLEVBa0JTO0FBQUM2VyxhQUFHLEVBQUMscUJBQUw7QUFBMkI3YixlQUFLLEVBQUMsU0FBUzY2QixtQkFBVCxDQUE2QnRoQyxLQUE3QixFQUFtQztBQUFDLGdCQUFJc2tDLElBQUksR0FBQ3RrQyxLQUFLLENBQUNza0MsSUFBZjtBQUFvQixnQkFBSUMsS0FBSyxHQUFDdmtDLEtBQUssQ0FBQ3VrQyxLQUFoQjs7QUFBc0IsZ0JBQUdELElBQUksS0FBRyxJQUFQLElBQWFDLEtBQUssS0FBRyxJQUF4QixFQUE2QjtBQUFDLG1CQUFLL0IsaUJBQUwsR0FBdUIsQ0FBdkI7QUFBeUIsbUJBQUs2QixNQUFMLENBQVlDLElBQVosRUFBaUJDLEtBQWpCO0FBQXlCO0FBQUM7QUFBaE0sU0FsQlQsRUFrQjJNO0FBQUNqaUIsYUFBRyxFQUFDLGdCQUFMO0FBQXNCN2IsZUFBSyxFQUFDLFNBQVM4NkIsY0FBVCxDQUF3QnZoQyxLQUF4QixFQUE4QjtBQUFDLGdCQUFJc2tDLElBQUksR0FBQ3RrQyxLQUFLLENBQUN3a0MsWUFBTixDQUFtQkYsSUFBNUI7QUFBaUMsZ0JBQUlDLEtBQUssR0FBQ3ZrQyxLQUFLLENBQUN3a0MsWUFBTixDQUFtQkQsS0FBN0I7O0FBQW1DLGdCQUFHRCxJQUFJLEtBQUcsSUFBUCxJQUFhQyxLQUFLLEtBQUcsSUFBeEIsRUFBNkI7QUFBQyxtQkFBSzlCLFlBQUwsR0FBa0IsQ0FBbEI7QUFBb0IsbUJBQUs0QixNQUFMLENBQVlDLElBQVosRUFBaUJDLEtBQWpCO0FBQXlCO0FBQUM7QUFBM00sU0FsQjNNLEVBa0J3WjtBQUFDamlCLGFBQUcsRUFBQyxhQUFMO0FBQW1CN2IsZUFBSyxFQUFDLFNBQVM0NkIsV0FBVCxDQUFxQnJoQyxLQUFyQixFQUEyQjtBQUFDLGdCQUFJOGdCLE9BQU8sR0FBQzlnQixLQUFLLENBQUM4Z0IsT0FBbEI7QUFBQSxnQkFBMEJDLE9BQU8sR0FBQy9nQixLQUFLLENBQUMrZ0IsT0FBeEM7O0FBQWdELGdCQUFHLEtBQUsyZCxTQUFMLEtBQWlCNWQsT0FBTyxHQUFDLEtBQUt1ZixnQkFBYixJQUErQnZmLE9BQU8sR0FBQyxLQUFLdWYsZ0JBQUwsR0FBc0IsS0FBS0UsWUFBbEUsSUFBZ0Z4ZixPQUFPLEdBQUMsS0FBS3VmLGdCQUE3RixJQUErR3ZmLE9BQU8sR0FBQyxLQUFLdWYsZ0JBQUwsR0FBc0IsS0FBS0UsYUFBbkssQ0FBSCxFQUFxTDtBQUFDLG1CQUFLTyxNQUFMLEdBQVksQ0FBWjtBQUFjLG1CQUFLQyxNQUFMLEdBQVksQ0FBWjtBQUFjO0FBQVE7O0FBQzd1QixnQkFBRyxLQUFLekMsYUFBUixFQUFzQjtBQUFDLGtCQUFHLEtBQUtDLGlCQUFSLEVBQTBCO0FBQUMxZCx1QkFBTyxHQUFDbFosSUFBSSxDQUFDQyxHQUFMLENBQVNpWixPQUFULEVBQWlCLEtBQUt1ZixnQkFBdEIsQ0FBUjtBQUFnRHZmLHVCQUFPLEdBQUNsWixJQUFJLENBQUNnWixHQUFMLENBQVNFLE9BQVQsRUFBaUIsS0FBS3VmLGdCQUFMLEdBQXNCLEtBQUtFLFlBQTVDLENBQVI7QUFBa0V4Zix1QkFBTyxHQUFDblosSUFBSSxDQUFDQyxHQUFMLENBQVNrWixPQUFULEVBQWlCLEtBQUt1ZixnQkFBdEIsQ0FBUjtBQUFnRHZmLHVCQUFPLEdBQUNuWixJQUFJLENBQUNnWixHQUFMLENBQVNHLE9BQVQsRUFBaUIsS0FBS3VmLGdCQUFMLEdBQXNCLEtBQUtFLGFBQTVDLENBQVI7QUFBb0U7O0FBQ3hSLGtCQUFHLEtBQUtHLGFBQUwsSUFBb0IsS0FBS0MsYUFBNUIsRUFBMEM7QUFBQyxxQkFBS0csTUFBTCxHQUFZLENBQUNqZ0IsT0FBTyxHQUFDLEtBQUt1ZixnQkFBYixHQUE4QixLQUFLSSxjQUFwQyxJQUFvRCxLQUFLRSxhQUFyRTtBQUFtRixxQkFBS0ssTUFBTCxHQUFZLENBQUNqZ0IsT0FBTyxHQUFDLEtBQUt1ZixnQkFBYixHQUE4QixLQUFLSSxjQUFwQyxJQUFvRCxLQUFLRSxhQUFyRTtBQUFvRjtBQUFDLGFBRG5OLE1BQ3VOO0FBQUMsa0JBQUcsS0FBS29CLGFBQUwsSUFBb0IsS0FBS0MsYUFBNUIsRUFBMEM7QUFBQyxxQkFBS2xCLE1BQUwsR0FBWSxDQUFDamdCLE9BQU8sR0FBQyxLQUFLZ2hCLGFBQWQsSUFBNkIsS0FBS0UsYUFBOUM7QUFBNEQscUJBQUtoQixNQUFMLEdBQVksQ0FBQ2pnQixPQUFPLEdBQUMsS0FBS2doQixhQUFkLElBQTZCLEtBQUtFLGFBQTlDO0FBQTZEO0FBQUM7QUFBQztBQUZnRCxTQWxCeFosRUFvQjBXO0FBQUMzZixhQUFHLEVBQUMsU0FBTDtBQUFlN2IsZUFBSyxFQUFDLFNBQVN2RSxPQUFULEdBQWtCO0FBQUMsaUJBQUtzaEMsT0FBTDtBQUFlMzlCLHdCQUFZLENBQUMsS0FBS2s2QixnQkFBTixDQUFaO0FBQW9DbDZCLHdCQUFZLENBQUMsS0FBSzA5QixjQUFOLENBQVo7QUFBa0MsaUJBQUtsaUMsT0FBTCxDQUFhMkIsZUFBYixDQUE2QixPQUE3Qjs7QUFBc0MsaUJBQUksSUFBSWdJLEtBQUssR0FBQyxDQUFkLEVBQWdCQSxLQUFLLEdBQUMsS0FBS2s0QixNQUFMLENBQVlobEMsTUFBbEMsRUFBeUM4TSxLQUFLLEVBQTlDLEVBQWlEO0FBQUMsbUJBQUtrNEIsTUFBTCxDQUFZbDRCLEtBQVosRUFBbUJoSSxlQUFuQixDQUFtQyxPQUFuQztBQUE2Qzs7QUFDbG9CLG1CQUFPLEtBQUszQixPQUFaO0FBQW9CLG1CQUFPLEtBQUs2aEMsTUFBWjtBQUFvQjtBQUR3VixTQXBCMVcsRUFxQm9CO0FBQUM1Z0IsYUFBRyxFQUFDLFNBQUw7QUFBZTdiLGVBQUssRUFBQyxTQUFTNEksT0FBVCxHQUFrQjtBQUFDLG1CQUFNLE9BQU47QUFBZTtBQUF2RCxTQXJCcEIsQ0FBVixDQUFaOztBQXFCcUcsZUFBT3l3QixRQUFQO0FBQWlCLE9BeEJvVSxFQUFiOztBQXdCcFR4SSxZQUFNLENBQUNELE9BQVAsR0FBZXlJLFFBQWY7QUFBeUIsS0FoQzRCLEVBZ0MzQjtBQUFDLHVCQUFnQixDQUFqQjtBQUFtQixhQUFNO0FBQXpCLEtBaEMyQjtBQTlDbXhCLEdBQTNiLEVBOEVyVixFQTlFcVYsRUE4RWxWLENBQUMsQ0FBRCxDQTlFa1YsRUE4RTdVLENBOUU2VSxDQUFOO0FBOEVwVSxDQTlFbkM7O0FBOEVxQztBQUM5TDs7QUFDQTs7QUFBQyxDQUFDLFVBQVNqakMsQ0FBVCxFQUFXRixNQUFYLEVBQWtCOE0sUUFBbEIsRUFBMkI3TSxTQUEzQixFQUFxQztBQUFDLE1BQUk2bkMsRUFBRSxHQUFFLFlBQVU7QUFBQyxRQUFHaDdCLFFBQVEsQ0FBQ2k3QixZQUFaLEVBQXlCO0FBQUMsYUFBT2o3QixRQUFRLENBQUNpN0IsWUFBaEI7QUFBOEIsS0FBeEQsTUFBNEQ7QUFBQyxXQUFJLElBQUkxbUMsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDLENBQWQsRUFBZ0JBLENBQUMsRUFBakIsRUFBb0I7QUFBQyxZQUFJMm1DLEdBQUcsR0FBQ2w3QixRQUFRLENBQUNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBUjtBQUFzQ2k3QixXQUFHLENBQUNob0IsU0FBSixHQUFjLGdCQUFjM2UsQ0FBZCxHQUFnQiw2QkFBOUI7O0FBQTRELFlBQUcybUMsR0FBRyxDQUFDbnBCLG9CQUFKLENBQXlCLE1BQXpCLEVBQWlDdGQsTUFBcEMsRUFBMkM7QUFBQ3ltQyxhQUFHLEdBQUMsSUFBSjtBQUFTLGlCQUFPM21DLENBQVA7QUFBVTs7QUFDOVMybUMsV0FBRyxHQUFDLElBQUo7QUFBVTtBQUFDOztBQUNYLFdBQU8vbkMsU0FBUDtBQUFrQixHQUY2QixFQUFQOztBQUVsQixNQUFJeUwsT0FBTyxHQUFDMUwsTUFBTSxDQUFDMEwsT0FBUCxJQUFnQjtBQUFDMnFCLE9BQUcsRUFBQyxlQUFVLENBQUUsQ0FBakI7QUFBa0J6UCxRQUFJLEVBQUMsZ0JBQVUsQ0FBRTtBQUFuQyxHQUE1QjtBQUFpRSxNQUFJcWhCLElBQUksR0FBQyxPQUFUO0FBQUEsTUFBaUJDLGVBQWUsR0FBQztBQUFDQyxvQkFBZ0IsRUFBQyxnQ0FBbEI7QUFBbURDLGdCQUFZLEVBQUM7QUFBaEUsR0FBakM7QUFBQSxNQUFnTEMsR0FBRyxHQUFDO0FBQUNDLGlCQUFhLEVBQUMsSUFBSS81QixNQUFKLENBQVcsT0FBSzI1QixlQUFlLENBQUNFLFlBQXJCLEdBQWtDLDhGQUFsQyxHQUFpSUYsZUFBZSxDQUFDRSxZQUFqSixHQUE4SixHQUF6SyxFQUE2SyxJQUE3SyxDQUFmO0FBQWtNRyxtQkFBZSxFQUFDLElBQUloNkIsTUFBSixDQUFXLE1BQUkyNUIsZUFBZSxDQUFDRSxZQUFwQixHQUFpQyxNQUFqQyxHQUF3Q0YsZUFBZSxDQUFDRSxZQUF4RCxHQUFxRSxHQUFoRixFQUFvRixJQUFwRixDQUFsTjtBQUE0U0ksMkJBQXVCLEVBQUMsSUFBSWo2QixNQUFKLENBQVcsT0FBSzI1QixlQUFlLENBQUNDLGdCQUFyQixHQUFzQyxHQUFqRCxDQUFwVTtBQUEwWE0sdUJBQW1CLEVBQUMsSUFBSWw2QixNQUFKLENBQVcsT0FBSzI1QixlQUFlLENBQUNDLGdCQUFyQixHQUFzQyxNQUF0QyxHQUE2Q0QsZUFBZSxDQUFDQyxnQkFBN0QsR0FBOEUsS0FBekYsRUFBK0YsR0FBL0YsQ0FBOVk7QUFBa2ZPLG1CQUFlLEVBQUMsaUNBQWxnQjtBQUFvaUJDLGtCQUFjLEVBQUMsSUFBSXA2QixNQUFKLENBQVcsVUFBUTA1QixJQUFSLEdBQWEsT0FBeEIsRUFBZ0MsSUFBaEM7QUFBbmpCLEdBQXBMOztBQUE4d0IvbkMsR0FBQyxDQUFDSyxFQUFGLENBQUswbkMsSUFBTCxJQUFXLFVBQVNwOEIsT0FBVCxFQUFpQjtBQUFDLGFBQVMrOEIsaUJBQVQsQ0FBMkJ4aEIsSUFBM0IsRUFBZ0M7QUFBQyxhQUFPQSxJQUFJLENBQUNqYSxPQUFMLENBQWFrN0IsR0FBRyxDQUFDQyxhQUFqQixFQUErQixVQUFTNzVCLEtBQVQsRUFBZTtBQUFDLGVBQU9BLEtBQUssQ0FBQ3RCLE9BQU4sQ0FBYyxLQUFkLEVBQW9CLFFBQXBCLENBQVA7QUFBc0MsT0FBckYsRUFBdUZBLE9BQXZGLENBQStGazdCLEdBQUcsQ0FBQ0UsZUFBbkcsRUFBbUgsVUFBUzk1QixLQUFULEVBQWU7QUFBQyxlQUFPQSxLQUFLLENBQUN0QixPQUFOLENBQWMsS0FBZCxFQUFvQixRQUFwQixDQUFQO0FBQXNDLE9BQXpLLENBQVA7QUFBbUw7O0FBQ3RsQyxhQUFTMDdCLGlCQUFULENBQTJCemhCLElBQTNCLEVBQWdDO0FBQUMsYUFBT0EsSUFBSSxDQUFDamEsT0FBTCxDQUFhLGdCQUFiLEVBQThCLFVBQVMyN0IsU0FBVCxFQUFtQkMsUUFBbkIsRUFBNEI7QUFBQyxlQUFPN2UsTUFBTSxDQUFDMlIsWUFBUCxDQUFvQmtOLFFBQXBCLENBQVA7QUFBc0MsT0FBakcsQ0FBUDtBQUEyRzs7QUFDNUksYUFBU0MsUUFBVCxDQUFrQjdvQixJQUFsQixFQUF1QjhvQixJQUF2QixFQUE0QjtBQUFDLFVBQUlDLE9BQU8sR0FBQ3A4QixRQUFRLENBQUNDLGFBQVQsQ0FBdUJrOEIsSUFBSSxDQUFDdGtDLEdBQTVCLENBQVo7QUFBNkN1a0MsYUFBTyxDQUFDbDhCLFNBQVIsR0FBa0JpN0IsSUFBbEI7O0FBQXVCLFVBQUdnQixJQUFJLENBQUNFLFdBQVIsRUFBb0I7QUFBQ0QsZUFBTyxDQUFDbDhCLFNBQVIsSUFBbUIsTUFBSWk4QixJQUFJLENBQUNFLFdBQTVCOztBQUF3QyxZQUFHRixJQUFJLENBQUNHLGVBQVIsRUFBd0I7QUFBQ0YsaUJBQU8sQ0FBQy85QixFQUFSLEdBQVc4OUIsSUFBSSxDQUFDRSxXQUFMLEdBQWlCLEdBQWpCLEdBQXFCRSxPQUFPLENBQUNDLFlBQXhDO0FBQXNEO0FBQUM7O0FBQzlPLFVBQUdMLElBQUksQ0FBQ00sU0FBTCxLQUFpQixLQUFqQixJQUF3QixLQUFLNXJCLElBQUwsQ0FBVXdDLElBQUksQ0FBQ3RjLElBQWYsQ0FBM0IsRUFBZ0Q7QUFBQ3FsQyxlQUFPLENBQUN6OEIsS0FBUixDQUFjKzhCLFVBQWQsR0FBeUIsVUFBekI7QUFBcUM7O0FBQ3RGLFVBQUdQLElBQUksQ0FBQ1Esa0JBQUwsS0FBMEIsSUFBMUIsSUFBZ0MsQ0FBQ1IsSUFBSSxDQUFDbG5CLE1BQXRDLEtBQStDa25CLElBQUksQ0FBQ00sU0FBTCxLQUFpQixXQUFqQixJQUE4Qk4sSUFBSSxDQUFDTSxTQUFMLEtBQWlCLE1BQTlGLENBQUgsRUFBeUc7QUFBQyxZQUFJRyxVQUFKO0FBQUEsWUFBZXRpQixJQUFJLEdBQUNqSCxJQUFJLENBQUN0YyxJQUF6Qjs7QUFBOEIsWUFBR29sQyxJQUFJLENBQUNNLFNBQUwsS0FBaUIsTUFBakIsSUFBeUJsQixHQUFHLENBQUNHLHVCQUFKLENBQTRCN3FCLElBQTVCLENBQWlDeUosSUFBakMsQ0FBNUIsRUFBbUU7QUFBQ0EsY0FBSSxHQUFDQSxJQUFJLENBQUNqYSxPQUFMLENBQWFrN0IsR0FBRyxDQUFDSSxtQkFBakIsRUFBcUMsRUFBckMsQ0FBTDtBQUErQzs7QUFDM1BpQixrQkFBVSxHQUFDekIsSUFBSSxHQUFDLEdBQUwsR0FBU2dCLElBQUksQ0FBQ00sU0FBTCxDQUFlbGhDLFdBQWYsRUFBVCxHQUFzQyxHQUF0QyxHQUEwQytlLElBQUksQ0FBQy9lLFdBQUwsRUFBckQ7QUFBd0U2Z0MsZUFBTyxDQUFDbDhCLFNBQVIsSUFBbUIsTUFBSTA4QixVQUF2QjtBQUFtQzs7QUFDM0csVUFBR1QsSUFBSSxDQUFDVSxJQUFSLEVBQWE7QUFBQ1QsZUFBTyxDQUFDanVCLFlBQVIsQ0FBcUIsYUFBckIsRUFBbUMsTUFBbkM7QUFBNEM7O0FBQzFEaXVCLGFBQU8sQ0FBQ3h1QixXQUFSLENBQW9CeUYsSUFBSSxDQUFDdEYsU0FBTCxDQUFlLEtBQWYsQ0FBcEI7QUFBMkMsYUFBT3F1QixPQUFQO0FBQWdCOztBQUMzRCxhQUFTVSxXQUFULENBQXFCenBCLElBQXJCLEVBQTBCOG9CLElBQTFCLEVBQStCO0FBQUMsVUFBSVksYUFBYSxHQUFDLENBQUMsQ0FBbkI7QUFBQSxVQUFxQkMsV0FBVyxHQUFDLENBQWpDOztBQUFtQyxVQUFHM3BCLElBQUksQ0FBQzRwQixRQUFMLEtBQWdCLENBQWhCLElBQW1CNXBCLElBQUksQ0FBQ3RjLElBQUwsQ0FBVXRDLE1BQWhDLEVBQXVDO0FBQUMsWUFBRzhuQyxPQUFPLENBQUNXLGFBQVgsRUFBeUI7QUFBQzdwQixjQUFJLENBQUN0YyxJQUFMLEdBQVcsQ0FBQ29sQyxJQUFJLENBQUNsbkIsTUFBTixJQUFja25CLElBQUksQ0FBQ00sU0FBTCxLQUFpQixVQUFoQyxHQUE0Q1gsaUJBQWlCLENBQUN6b0IsSUFBSSxDQUFDdGMsSUFBTixDQUE3RCxHQUF5RWdsQyxpQkFBaUIsQ0FBQzFvQixJQUFJLENBQUN0YyxJQUFOLENBQXBHO0FBQWdId2xDLGlCQUFPLENBQUNXLGFBQVIsR0FBc0IsS0FBdEI7QUFBNkI7O0FBQ2xSSCxxQkFBYSxHQUFDMXBCLElBQUksQ0FBQ3RjLElBQUwsQ0FBVWtlLE1BQVYsQ0FBaUJrb0IsY0FBakIsQ0FBZDs7QUFBK0MsWUFBR0osYUFBYSxLQUFHLENBQUMsQ0FBcEIsRUFBc0I7QUFBQyxjQUFJcDdCLEtBQUssR0FBQzBSLElBQUksQ0FBQ3RjLElBQUwsQ0FBVTRLLEtBQVYsQ0FBZ0J3N0IsY0FBaEIsQ0FBVjtBQUFBLGNBQTBDQyxTQUFTLEdBQUN6N0IsS0FBSyxDQUFDLENBQUQsQ0FBekQ7QUFBQSxjQUE2RDA3QixZQUFZLEdBQUMxN0IsS0FBSyxDQUFDLENBQUQsQ0FBTCxJQUFVLEtBQXBGOztBQUEwRixjQUFHeTdCLFNBQVMsS0FBRyxFQUFmLEVBQWtCO0FBQUNMLHlCQUFhO0FBQUksV0FBcEMsTUFBeUMsSUFBR00sWUFBWSxJQUFFQSxZQUFZLEtBQUdELFNBQWhDLEVBQTBDO0FBQUNMLHlCQUFhLElBQUVLLFNBQVMsQ0FBQ3o2QixPQUFWLENBQWtCMDZCLFlBQWxCLENBQWY7QUFBK0NELHFCQUFTLEdBQUNDLFlBQVY7QUFBd0I7O0FBQzNULGNBQUlDLFNBQVMsR0FBQ2pxQixJQUFJLENBQUNrcUIsU0FBTCxDQUFlUixhQUFmLENBQWQ7QUFBNENPLG1CQUFTLENBQUNDLFNBQVYsQ0FBb0JILFNBQVMsQ0FBQzNvQyxNQUE5QjtBQUFzQ3VvQyxxQkFBVyxHQUFDLENBQVo7O0FBQWMsY0FBRyxDQUFDYixJQUFJLENBQUNsbkIsTUFBTixJQUFja25CLElBQUksQ0FBQ00sU0FBTCxLQUFpQixVQUFsQyxFQUE2QztBQUFDYSxxQkFBUyxDQUFDdm1DLElBQVYsR0FBZWdsQyxpQkFBaUIsQ0FBQ3VCLFNBQVMsQ0FBQ3ZtQyxJQUFYLENBQWhDO0FBQWtEOztBQUNoTSxjQUFJeW1DLFdBQVcsR0FBQ3RCLFFBQVEsQ0FBQ29CLFNBQUQsRUFBV25CLElBQVgsRUFBZ0JJLE9BQU8sQ0FBQ0MsWUFBeEIsQ0FBeEI7QUFBOERjLG1CQUFTLENBQUNocUIsVUFBVixDQUFxQm1xQixZQUFyQixDQUFrQ0QsV0FBbEMsRUFBOENGLFNBQTlDO0FBQXlEZixpQkFBTyxDQUFDbUIsUUFBUixDQUFpQi9tQyxJQUFqQixDQUFzQjZtQyxXQUF0QjtBQUFtQ2pCLGlCQUFPLENBQUNDLFlBQVI7QUFBd0I7QUFBQyxPQUhoSCxNQUdxSCxJQUFHbnBCLElBQUksQ0FBQzRwQixRQUFMLEtBQWdCLENBQWhCLElBQW1CNXBCLElBQUksQ0FBQ3NxQixhQUFMLEVBQW5CLElBQXlDLENBQUNwQyxHQUFHLENBQUNLLGVBQUosQ0FBb0IvcUIsSUFBcEIsQ0FBeUJ3QyxJQUFJLENBQUMvWCxPQUE5QixDQUExQyxJQUFrRixDQUFDaWdDLEdBQUcsQ0FBQ00sY0FBSixDQUFtQmhyQixJQUFuQixDQUF3QndDLElBQUksQ0FBQ25ULFNBQTdCLENBQXRGLEVBQThIO0FBQUMsYUFBSSxJQUFJM0wsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDOGUsSUFBSSxDQUFDdXFCLFVBQUwsQ0FBZ0JucEMsTUFBOUIsRUFBcUNGLENBQUMsRUFBdEMsRUFBeUM7QUFBQ2dvQyxpQkFBTyxDQUFDVyxhQUFSLEdBQXNCLElBQXRCO0FBQTJCM29DLFdBQUMsSUFBRXVvQyxXQUFXLENBQUN6cEIsSUFBSSxDQUFDdXFCLFVBQUwsQ0FBZ0JycEMsQ0FBaEIsQ0FBRCxFQUFvQjRuQyxJQUFwQixDQUFkO0FBQXlDO0FBQUM7O0FBQ3RhLGFBQU9hLFdBQVA7QUFBb0I7O0FBQ3BCLFFBQUliLElBQUksR0FBQy9vQyxDQUFDLENBQUN3SixNQUFGLENBQVMsRUFBVCxFQUFZeEosQ0FBQyxDQUFDSyxFQUFGLENBQUswbkMsSUFBTCxFQUFXbjhCLFFBQXZCLEVBQWdDRCxPQUFoQyxDQUFUO0FBQUEsUUFBa0RvK0IsY0FBbEQ7QUFBQSxRQUFpRVosT0FBTyxHQUFDLEVBQXpFOztBQUE0RSxRQUFHSixJQUFJLENBQUNsbkIsTUFBTCxDQUFZeGdCLE1BQVosS0FBcUIsT0FBTzBuQyxJQUFJLENBQUNsbkIsTUFBWixLQUFxQixRQUFyQixJQUErQixNQUFNcEUsSUFBTixDQUFXTyxVQUFVLENBQUMrcUIsSUFBSSxDQUFDbG5CLE1BQU4sQ0FBckIsQ0FBcEQsQ0FBSCxFQUE0RjtBQUFDa25CLFVBQUksQ0FBQ00sU0FBTCxHQUFlTixJQUFJLENBQUNsbkIsTUFBTCxDQUFZNG9CLFFBQVosR0FBdUJ4OUIsT0FBdkIsQ0FBK0IseUJBQS9CLEVBQXlELE1BQXpELENBQWY7QUFBZ0Y4OEIsb0JBQWMsR0FBQyxJQUFJMTdCLE1BQUosQ0FBVyxhQUFXMjVCLGVBQWUsQ0FBQ0UsWUFBM0IsR0FBd0MsS0FBeEMsR0FBOENhLElBQUksQ0FBQ00sU0FBbkQsR0FBNkQsYUFBN0QsR0FBMkVyQixlQUFlLENBQUNFLFlBQTNGLEdBQXdHLElBQW5ILEVBQXdILEdBQXhILENBQWY7QUFBNkksS0FBMVQsTUFBOFQ7QUFBQyxVQUFHLE9BQU9hLElBQUksQ0FBQ00sU0FBWixLQUF3QixRQUEzQixFQUFvQztBQUFDTixZQUFJLENBQUNNLFNBQUwsR0FBZU4sSUFBSSxDQUFDTSxTQUFMLENBQWVsaEMsV0FBZixFQUFmO0FBQTZDOztBQUM3ZCxjQUFPNGdDLElBQUksQ0FBQ00sU0FBWjtBQUF1QixhQUFJLEtBQUo7QUFBVVUsd0JBQWMsR0FBQyxLQUFmO0FBQXFCOztBQUFNLGFBQUksUUFBSjtBQUFhLGFBQUksTUFBSjtBQUFXLGFBQUksV0FBSjtBQUFnQkEsd0JBQWMsR0FBQyxNQUFmO0FBQXNCOztBQUFNLGFBQUksTUFBSjtBQUFXQSx3QkFBYyxHQUFDLGFBQWY7QUFBNkI7O0FBQU0sYUFBSSxVQUFKO0FBQWVBLHdCQUFjLEdBQUMsaUVBQWY7QUFBaUY7O0FBQU0sYUFBSSxTQUFKO0FBQWNBLHdCQUFjLEdBQUMsbUJBQWY7QUFBbUM7O0FBQU07QUFBUSxjQUFHaEIsSUFBSSxDQUFDTSxTQUFMLFlBQTBCaDdCLE1BQTdCLEVBQW9DO0FBQUMwN0IsMEJBQWMsR0FBQ2hCLElBQUksQ0FBQ00sU0FBcEI7QUFBK0IsV0FBcEUsTUFBd0U7QUFBQzc5QixtQkFBTyxDQUFDMnFCLEdBQVIsQ0FBWTRSLElBQUksR0FBQyxtRkFBakI7QUFBc0csbUJBQU8sSUFBUDtBQUFhOztBQUEvZ0I7QUFBaWhCOztBQUNqaEIsU0FBS3ZSLElBQUwsQ0FBVSxZQUFVO0FBQUMsVUFBSWtVLEtBQUssR0FBQzFxQyxDQUFDLENBQUMsSUFBRCxDQUFYO0FBQUEsVUFBa0JrbkIsSUFBSSxHQUFDd2pCLEtBQUssQ0FBQ3hqQixJQUFOLEVBQXZCOztBQUFvQyxVQUFHdmIsT0FBTyxLQUFHLEtBQWIsRUFBbUI7QUFBQ3c5QixlQUFPLEdBQUM7QUFBQ0Msc0JBQVksRUFBQyxDQUFkO0FBQWdCVSx1QkFBYSxFQUFDLEtBQTlCO0FBQW9DUSxrQkFBUSxFQUFDbkIsT0FBTyxDQUFDbUIsUUFBUixJQUFrQjtBQUEvRCxTQUFSOztBQUEyRSxZQUFHSSxLQUFLLENBQUMvbUMsSUFBTixDQUFXb2tDLElBQVgsTUFBbUJob0MsU0FBbkIsS0FBK0IycUMsS0FBSyxDQUFDL21DLElBQU4sQ0FBV29rQyxJQUFYLE1BQW1CLFFBQW5CLElBQTZCZ0IsSUFBSSxDQUFDbG5CLE1BQUwsS0FBYyxLQUExRSxDQUFILEVBQW9GO0FBQUNqRyxpQkFBTyxDQUFDOHVCLEtBQUQsRUFBTzNCLElBQVAsQ0FBUDtBQUFvQixjQUFHQSxJQUFJLENBQUM0QixLQUFSLEVBQWNuL0IsT0FBTyxDQUFDMnFCLEdBQVIsQ0FBWTRSLElBQUksR0FBQywwQ0FBakI7QUFBOEQ7O0FBQzdVMkMsYUFBSyxDQUFDL21DLElBQU4sQ0FBV29rQyxJQUFYLEVBQWdCZ0IsSUFBSSxDQUFDbG5CLE1BQUwsS0FBYyxLQUFkLEdBQW9CLFFBQXBCLEdBQTZCa25CLElBQUksQ0FBQ00sU0FBbEQ7O0FBQTZELFlBQUdOLElBQUksQ0FBQ1UsSUFBUixFQUFhO0FBQUNpQixlQUFLLENBQUM5bEMsSUFBTixDQUFXLFlBQVgsRUFBd0JzaUIsSUFBeEI7QUFBK0I7O0FBQzFHLFlBQUc2aEIsSUFBSSxDQUFDNkIsYUFBUixFQUFzQjtBQUFDRixlQUFLLENBQUNHLElBQU4sQ0FBVzNqQixJQUFYO0FBQWtCOztBQUN6QyxZQUFHO0FBQUN0YSxrQkFBUSxDQUFDQyxhQUFULENBQXVCazhCLElBQUksQ0FBQ3RrQyxHQUE1QjtBQUFrQyxTQUF0QyxDQUFzQyxPQUFNZ0gsS0FBTixFQUFZO0FBQUNzOUIsY0FBSSxDQUFDdGtDLEdBQUwsR0FBUyxNQUFUO0FBQWdCLGNBQUdza0MsSUFBSSxDQUFDNEIsS0FBUixFQUFjbi9CLE9BQU8sQ0FBQzJxQixHQUFSLENBQVk0UixJQUFJLEdBQUMsNkNBQWpCO0FBQWlFOztBQUNsSjJDLGFBQUssQ0FBQ2w4QixRQUFOLENBQWV1NUIsSUFBSSxHQUFDLE9BQXBCO0FBQTZCLFlBQUdnQixJQUFJLENBQUM0QixLQUFSLEVBQWNuL0IsT0FBTyxDQUFDa2IsSUFBUixDQUFhcWhCLElBQWI7QUFBbUIyQixtQkFBVyxDQUFDLElBQUQsRUFBTVgsSUFBTixDQUFYO0FBQXVCLFlBQUdBLElBQUksQ0FBQzRCLEtBQVIsRUFBY24vQixPQUFPLENBQUNzL0IsT0FBUixDQUFnQi9DLElBQWhCO0FBQXVCLE9BSmpFLE1BSXNFLElBQUdwOEIsT0FBTyxLQUFHLEtBQVYsSUFBaUIrK0IsS0FBSyxDQUFDL21DLElBQU4sQ0FBV29rQyxJQUFYLE1BQW1CaG9DLFNBQXZDLEVBQWlEO0FBQUM2YixlQUFPLENBQUM4dUIsS0FBRCxFQUFPM0IsSUFBUCxDQUFQO0FBQXFCOztBQUN0TSxVQUFHQSxJQUFJLENBQUM0QixLQUFSLEVBQWM7QUFBQzNxQyxTQUFDLENBQUN3MkIsSUFBRixDQUFPMlMsT0FBTyxDQUFDbUIsUUFBZixFQUF3QixVQUFTbjhCLEtBQVQsRUFBZTNKLE9BQWYsRUFBdUI7QUFBQ2dILGlCQUFPLENBQUMycUIsR0FBUixDQUFZNFIsSUFBSSxHQUFDLElBQUwsR0FBVWdCLElBQUksQ0FBQ00sU0FBZixHQUF5QixJQUF6QixHQUE4QixLQUFLMEIsU0FBL0M7QUFBMEQsZUFBS3grQixLQUFMLENBQVd5K0IsZUFBWCxHQUEyQjc4QixLQUFLLEdBQUMsQ0FBTixHQUFRLFNBQVIsR0FBa0IsU0FBN0M7QUFBd0QsU0FBbEs7QUFBcUs7QUFBQyxLQUxyTDs7QUFLdUwsYUFBU3lOLE9BQVQsQ0FBaUI4dUIsS0FBakIsRUFBdUIzQixJQUF2QixFQUE0QjtBQUFDLFVBQUdBLElBQUksQ0FBQzRCLEtBQVIsRUFBY24vQixPQUFPLENBQUNrYixJQUFSLENBQWEsZ0JBQWI7QUFBK0IsVUFBSXVrQixxQkFBcUIsR0FBQyxLQUExQjtBQUFnQ1AsV0FBSyxDQUFDajhCLFdBQU4sQ0FBa0JzNUIsSUFBSSxHQUFDLE9BQXZCLEVBQWdDemhDLFVBQWhDLENBQTJDLFlBQTNDLEVBQXlENGtDLElBQXpELENBQThELE1BQUluRCxJQUFsRSxFQUF3RXZSLElBQXhFLENBQTZFLFlBQVU7QUFBQyxZQUFJa1UsS0FBSyxHQUFDMXFDLENBQUMsQ0FBQyxJQUFELENBQVg7O0FBQWtCLFlBQUcsQ0FBQzBxQyxLQUFLLENBQUNTLE9BQU4sQ0FBYyxNQUFJcEQsSUFBSixHQUFTLE9BQXZCLEVBQWdDMW1DLE1BQXBDLEVBQTJDO0FBQUMsY0FBSStwQyxjQUFjLEdBQUMsS0FBS2xyQixVQUF4QjtBQUFtQyxjQUFHMG5CLEVBQUUsSUFBRSxDQUFQLEVBQVV3RCxjQUFjLENBQUMzaEIsVUFBZixDQUEwQnZHLFFBQTNCO0FBQXFDa29CLHdCQUFjLENBQUNmLFlBQWYsQ0FBNEIsS0FBSzVnQixVQUFqQyxFQUE0QyxJQUE1QztBQUFrRDJoQix3QkFBYyxDQUFDQyxTQUFmO0FBQTRCLFNBQTNNLE1BQStNO0FBQUNKLCtCQUFxQixHQUFDLElBQXRCO0FBQTRCO0FBQUMsT0FBdlY7O0FBQXlWLFVBQUduckMsTUFBTSxDQUFDSSxLQUFWLEVBQWdCO0FBQUN3cUMsYUFBSyxDQUFDL21DLElBQU4sQ0FBV29rQyxJQUFYLEVBQWdCaG9DLFNBQWhCO0FBQTRCLE9BQTdDLE1BQWlEO0FBQUMycUMsYUFBSyxDQUFDWSxVQUFOLENBQWlCdkQsSUFBakI7QUFBd0I7O0FBQ3BzQixVQUFHZ0IsSUFBSSxDQUFDNEIsS0FBUixFQUFjO0FBQUNuL0IsZUFBTyxDQUFDMnFCLEdBQVIsQ0FBWTRSLElBQUksR0FBQyxrQkFBTCxJQUF5QjJDLEtBQUssQ0FBQzlsQyxJQUFOLENBQVcsSUFBWCxJQUFpQixVQUFROGxDLEtBQUssQ0FBQzlsQyxJQUFOLENBQVcsSUFBWCxDQUFSLEdBQXlCLEdBQTFDLEdBQThDLEdBQXZFLEtBQTZFcW1DLHFCQUFxQixHQUFDLDRFQUFELEdBQThFLEVBQWhMLENBQVo7QUFBaU16L0IsZUFBTyxDQUFDcy9CLE9BQVIsQ0FBZ0IsZ0JBQWhCO0FBQW1DO0FBQUM7O0FBQ3BQLFFBQUduL0IsT0FBTyxLQUFHLEtBQVYsSUFBaUJvOUIsSUFBSSxDQUFDd0MsZUFBTCxLQUF1QixJQUEzQyxFQUFnRDtBQUFDLFVBQUlDLFFBQVEsR0FBQ3hyQyxDQUFDLEdBQUdrdkIsR0FBSixDQUFRaWEsT0FBTyxDQUFDbUIsUUFBaEIsQ0FBYjtBQUF1Q2tCLGNBQVEsQ0FBQ0MsVUFBVCxHQUFvQixJQUFwQjtBQUF5QkQsY0FBUSxDQUFDaFMsT0FBVCxHQUFpQixLQUFLQSxPQUF0QjtBQUE4QixhQUFPZ1MsUUFBUDtBQUFpQixLQUFoSyxNQUFvSztBQUFDLGFBQU8sSUFBUDtBQUFhO0FBQUMsR0F0QmtyQjs7QUFzQmpyQnhyQyxHQUFDLENBQUNLLEVBQUYsQ0FBS3FyQyxLQUFMLENBQVc5L0IsUUFBWCxHQUFvQjtBQUFDMi9CLG1CQUFlLEVBQUMsSUFBakI7QUFBc0JsQyxhQUFTLEVBQUMsTUFBaEM7QUFBdUM1a0MsT0FBRyxFQUFDLE1BQTNDO0FBQWtEb2QsVUFBTSxFQUFDLEtBQXpEO0FBQStEb25CLGVBQVcsRUFBQyxFQUEzRTtBQUE4RUMsbUJBQWUsRUFBQyxLQUE5RjtBQUFvR0ssc0JBQWtCLEVBQUMsS0FBdkg7QUFBNkhxQixpQkFBYSxFQUFDLEtBQTNJO0FBQWlKbkIsUUFBSSxFQUFDLElBQXRKO0FBQTJKa0IsU0FBSyxFQUFDO0FBQWpLLEdBQXBCO0FBQTZMLENBeEJoWCxFQXdCa1g3cUMsTUFBTSxDQUFDRyxNQUFQLElBQWVILE1BQU0sQ0FBQ0ksS0F4QnhZLEVBd0I4WUosTUF4QjlZLEVBd0JxWjhNLFFBeEJyWjs7QUF3QitaO0FBQ2hhO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxDQUFDLFVBQVMrK0IsT0FBVCxFQUFpQjtBQUFDLE1BQUlDLGdCQUFnQixHQUFDLEVBQXJCOztBQUF3QixXQUFTQyxtQkFBVCxDQUE2QkMsUUFBN0IsRUFBc0M7QUFBQyxRQUFHRixnQkFBZ0IsQ0FBQ0UsUUFBRCxDQUFuQixFQUE4QjtBQUFDLGFBQU9GLGdCQUFnQixDQUFDRSxRQUFELENBQWhCLENBQTJCdFIsT0FBbEM7QUFBMkM7O0FBQzVKLFFBQUlDLE1BQU0sR0FBQ21SLGdCQUFnQixDQUFDRSxRQUFELENBQWhCLEdBQTJCO0FBQUMzcUMsT0FBQyxFQUFDMnFDLFFBQUg7QUFBWTFxQyxPQUFDLEVBQUMsS0FBZDtBQUFvQm81QixhQUFPLEVBQUM7QUFBNUIsS0FBdEM7QUFBc0VtUixXQUFPLENBQUNHLFFBQUQsQ0FBUCxDQUFrQi9pQyxJQUFsQixDQUF1QjB4QixNQUFNLENBQUNELE9BQTlCLEVBQXNDQyxNQUF0QyxFQUE2Q0EsTUFBTSxDQUFDRCxPQUFwRCxFQUE0RHFSLG1CQUE1RDtBQUFpRnBSLFVBQU0sQ0FBQ3I1QixDQUFQLEdBQVMsSUFBVDtBQUFjLFdBQU9xNUIsTUFBTSxDQUFDRCxPQUFkO0FBQXVCOztBQUM1THFSLHFCQUFtQixDQUFDRSxDQUFwQixHQUFzQkosT0FBdEI7QUFBOEJFLHFCQUFtQixDQUFDdnFDLENBQXBCLEdBQXNCc3FDLGdCQUF0Qjs7QUFBdUNDLHFCQUFtQixDQUFDRyxDQUFwQixHQUFzQixVQUFTeFIsT0FBVCxFQUFpQjExQixJQUFqQixFQUFzQm1uQyxNQUF0QixFQUE2QjtBQUFDLFFBQUcsQ0FBQ0osbUJBQW1CLENBQUNqUixDQUFwQixDQUFzQkosT0FBdEIsRUFBOEIxMUIsSUFBOUIsQ0FBSixFQUF3QztBQUFDb0wsWUFBTSxDQUFDNnZCLGNBQVAsQ0FBc0J2RixPQUF0QixFQUE4QjExQixJQUE5QixFQUFtQztBQUFDODZCLGtCQUFVLEVBQUMsSUFBWjtBQUFpQnNNLFdBQUcsRUFBQ0Q7QUFBckIsT0FBbkM7QUFBa0U7QUFBQyxHQUFoSzs7QUFBaUtKLHFCQUFtQixDQUFDbFIsQ0FBcEIsR0FBc0IsVUFBU0gsT0FBVCxFQUFpQjtBQUFDLFFBQUcsT0FBTzJSLE1BQVAsS0FBZ0IsV0FBaEIsSUFBNkJBLE1BQU0sQ0FBQ0MsV0FBdkMsRUFBbUQ7QUFBQ2w4QixZQUFNLENBQUM2dkIsY0FBUCxDQUFzQnZGLE9BQXRCLEVBQThCMlIsTUFBTSxDQUFDQyxXQUFyQyxFQUFpRDtBQUFDeGlDLGFBQUssRUFBQztBQUFQLE9BQWpEO0FBQW9FOztBQUN0WXNHLFVBQU0sQ0FBQzZ2QixjQUFQLENBQXNCdkYsT0FBdEIsRUFBOEIsWUFBOUIsRUFBMkM7QUFBQzV3QixXQUFLLEVBQUM7QUFBUCxLQUEzQztBQUEwRCxHQUQ0Szs7QUFDM0tpaUMscUJBQW1CLENBQUMvUyxDQUFwQixHQUFzQixVQUFTbHZCLEtBQVQsRUFBZXlpQyxJQUFmLEVBQW9CO0FBQUMsUUFBR0EsSUFBSSxHQUFDLENBQVIsRUFBVXppQyxLQUFLLEdBQUNpaUMsbUJBQW1CLENBQUNqaUMsS0FBRCxDQUF6QjtBQUFpQyxRQUFHeWlDLElBQUksR0FBQyxDQUFSLEVBQVUsT0FBT3ppQyxLQUFQO0FBQWEsUUFBSXlpQyxJQUFJLEdBQUMsQ0FBTixJQUFVLFNBQU96aUMsS0FBUCxNQUFlLFFBQXpCLElBQW1DQSxLQUFuQyxJQUEwQ0EsS0FBSyxDQUFDMGlDLFVBQW5ELEVBQThELE9BQU8xaUMsS0FBUDtBQUFhLFFBQUkyaUMsRUFBRSxHQUFDcjhCLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLElBQWQsQ0FBUDs7QUFBMkIwN0IsdUJBQW1CLENBQUNsUixDQUFwQixDQUFzQjRSLEVBQXRCOztBQUEwQnI4QixVQUFNLENBQUM2dkIsY0FBUCxDQUFzQndNLEVBQXRCLEVBQXlCLFNBQXpCLEVBQW1DO0FBQUMzTSxnQkFBVSxFQUFDLElBQVo7QUFBaUJoMkIsV0FBSyxFQUFDQTtBQUF2QixLQUFuQztBQUFrRSxRQUFHeWlDLElBQUksR0FBQyxDQUFMLElBQVEsT0FBT3ppQyxLQUFQLElBQWMsUUFBekIsRUFBa0MsS0FBSSxJQUFJNmIsR0FBUixJQUFlN2IsS0FBZjtBQUFxQmlpQyx5QkFBbUIsQ0FBQ0csQ0FBcEIsQ0FBc0JPLEVBQXRCLEVBQXlCOW1CLEdBQXpCLEVBQTZCLFVBQVNBLEdBQVQsRUFBYTtBQUFDLGVBQU83YixLQUFLLENBQUM2YixHQUFELENBQVo7QUFBbUIsT0FBakMsQ0FBa0NwYyxJQUFsQyxDQUF1QyxJQUF2QyxFQUE0Q29jLEdBQTVDLENBQTdCO0FBQXJCO0FBQW9HLFdBQU84bUIsRUFBUDtBQUFXLEdBQWhjOztBQUFpY1YscUJBQW1CLENBQUNuUixDQUFwQixHQUFzQixVQUFTRCxNQUFULEVBQWdCO0FBQUMsUUFBSXdSLE1BQU0sR0FBQ3hSLE1BQU0sSUFBRUEsTUFBTSxDQUFDNlIsVUFBZixHQUEwQixTQUFTRSxVQUFULEdBQXFCO0FBQUMsYUFBTy9SLE1BQU0sQ0FBQyxTQUFELENBQWI7QUFBMEIsS0FBMUUsR0FBMkUsU0FBU2dTLGdCQUFULEdBQTJCO0FBQUMsYUFBT2hTLE1BQVA7QUFBZSxLQUFqSTs7QUFBa0lvUix1QkFBbUIsQ0FBQ0csQ0FBcEIsQ0FBc0JDLE1BQXRCLEVBQTZCLEdBQTdCLEVBQWlDQSxNQUFqQzs7QUFBeUMsV0FBT0EsTUFBUDtBQUFlLEdBQWpPOztBQUFrT0oscUJBQW1CLENBQUNqUixDQUFwQixHQUFzQixVQUFTOWMsTUFBVCxFQUFnQnFhLFFBQWhCLEVBQXlCO0FBQUMsV0FBT2pvQixNQUFNLENBQUNsUCxTQUFQLENBQWlCOEssY0FBakIsQ0FBZ0MvQyxJQUFoQyxDQUFxQytVLE1BQXJDLEVBQTRDcWEsUUFBNUMsQ0FBUDtBQUE4RCxHQUE5Rzs7QUFBK0cwVCxxQkFBbUIsQ0FBQzMvQixDQUFwQixHQUFzQixFQUF0QjtBQUF5QixTQUFPMi9CLG1CQUFtQixDQUFDQSxtQkFBbUIsQ0FBQ2g5QixDQUFwQixHQUFzQixFQUF2QixDQUExQjtBQUFzRCxDQUg1NUIsRUFJQyxJQUFJLFVBQVM0ckIsTUFBVCxFQUFnQkQsT0FBaEIsRUFBd0I7QUFBQ0MsUUFBTSxDQUFDRCxPQUFQLEdBQWUsVUFBU2h5QixRQUFULEVBQWtCO0FBQUMsUUFBR29FLFFBQVEsQ0FBQzgvQixVQUFULEtBQXNCLFVBQXRCLElBQWtDOS9CLFFBQVEsQ0FBQzgvQixVQUFULEtBQXNCLGFBQTNELEVBQXlFO0FBQUNsa0MsY0FBUSxDQUFDTyxJQUFUO0FBQWlCLEtBQTNGLE1BQWdHLElBQUc2RCxRQUFRLENBQUMrL0IsV0FBWixFQUF3QjtBQUFDLy9CLGNBQVEsQ0FBQysvQixXQUFULENBQXFCLG9CQUFyQixFQUEwQyxZQUFVO0FBQUMsWUFBRy8vQixRQUFRLENBQUM4L0IsVUFBVCxLQUFzQixhQUF6QixFQUF1Q2xrQyxRQUFRLENBQUNPLElBQVQ7QUFBaUIsT0FBN0c7QUFBZ0gsS0FBekksTUFBOEksSUFBRzZELFFBQVEsQ0FBQzRULGdCQUFaLEVBQTZCO0FBQUM1VCxjQUFRLENBQUM0VCxnQkFBVCxDQUEwQixrQkFBMUIsRUFBNkNoWSxRQUE3QztBQUF3RDtBQUFDLEdBQXZXO0FBQXlXLENBQXRZLEVBQXlZLFVBQVNpeUIsTUFBVCxFQUFnQkQsT0FBaEIsRUFBd0JxUixtQkFBeEIsRUFBNEM7QUFBRSxhQUFTak4sTUFBVCxFQUFnQjtBQUFDLFFBQUlwMEIsR0FBSjs7QUFBUSxRQUFHLE9BQU8xSyxNQUFQLEtBQWdCLFdBQW5CLEVBQStCO0FBQUMwSyxTQUFHLEdBQUMxSyxNQUFKO0FBQVksS0FBNUMsTUFBaUQsSUFBRyxPQUFPOCtCLE1BQVAsS0FBZ0IsV0FBbkIsRUFBK0I7QUFBQ3AwQixTQUFHLEdBQUNvMEIsTUFBSjtBQUFZLEtBQTVDLE1BQWlELElBQUcsT0FBTzV3QixJQUFQLEtBQWMsV0FBakIsRUFBNkI7QUFBQ3hELFNBQUcsR0FBQ3dELElBQUo7QUFBVSxLQUF4QyxNQUE0QztBQUFDeEQsU0FBRyxHQUFDLEVBQUo7QUFBUTs7QUFDeG1CaXdCLFVBQU0sQ0FBQ0QsT0FBUCxHQUFlaHdCLEdBQWY7QUFBb0IsR0FEb2EsRUFDbmF6QixJQURtYSxDQUM5WixJQUQ4WixFQUN6WjhpQyxtQkFBbUIsQ0FBQyxDQUFELENBRHNZLENBQUQ7QUFDL1gsQ0FEdkQsRUFDMEQsVUFBU3BSLE1BQVQsRUFBZ0JELE9BQWhCLEVBQXdCO0FBQUMsV0FBU29TLE9BQVQsQ0FBaUJuZ0MsR0FBakIsRUFBcUI7QUFBQzs7QUFBMEIsUUFBRyxPQUFPMC9CLE1BQVAsS0FBZ0IsVUFBaEIsSUFBNEIsT0FBT0EsTUFBTSxDQUFDVSxRQUFkLEtBQXlCLFFBQXhELEVBQWlFO0FBQUNELGFBQU8sR0FBQyxTQUFTQSxPQUFULENBQWlCbmdDLEdBQWpCLEVBQXFCO0FBQUMsZUFBTyxPQUFPQSxHQUFkO0FBQW1CLE9BQWpEO0FBQW1ELEtBQXJILE1BQXlIO0FBQUNtZ0MsYUFBTyxHQUFDLFNBQVNBLE9BQVQsQ0FBaUJuZ0MsR0FBakIsRUFBcUI7QUFBQyxlQUFPQSxHQUFHLElBQUUsT0FBTzAvQixNQUFQLEtBQWdCLFVBQXJCLElBQWlDMS9CLEdBQUcsQ0FBQ3FnQyxXQUFKLEtBQWtCWCxNQUFuRCxJQUEyRDEvQixHQUFHLEtBQUcwL0IsTUFBTSxDQUFDbnJDLFNBQXhFLEdBQWtGLFFBQWxGLEdBQTJGLE9BQU95TCxHQUF6RztBQUE4RyxPQUE1STtBQUE4STs7QUFBQSxXQUFPbWdDLE9BQU8sQ0FBQ25nQyxHQUFELENBQWQ7QUFBcUI7O0FBQ2phLE1BQUloSixDQUFKOztBQUFNQSxHQUFDLEdBQUMsWUFBVTtBQUFDLFdBQU8sSUFBUDtBQUFhLEdBQXhCLEVBQUY7O0FBQTZCLE1BQUc7QUFBQ0EsS0FBQyxHQUFDQSxDQUFDLElBQUUsSUFBSXNwQyxRQUFKLENBQWEsYUFBYixHQUFMO0FBQW9DLEdBQXhDLENBQXdDLE9BQU0vcEMsQ0FBTixFQUFRO0FBQUMsUUFBRyxDQUFDLE9BQU9sRCxNQUFQLEtBQWdCLFdBQWhCLEdBQTRCLFdBQTVCLEdBQXdDOHNDLE9BQU8sQ0FBQzlzQyxNQUFELENBQWhELE1BQTRELFFBQS9ELEVBQXdFMkQsQ0FBQyxHQUFDM0QsTUFBRjtBQUFVOztBQUN0SzI2QixRQUFNLENBQUNELE9BQVAsR0FBZS8yQixDQUFmO0FBQWtCLENBSGpCLE9BR3lCLFVBQVNnM0IsTUFBVCxFQUFnQkQsT0FBaEIsRUFBd0JxUixtQkFBeEIsRUFBNEM7QUFBQ3BSLFFBQU0sQ0FBQ0QsT0FBUCxHQUFlcVIsbUJBQW1CLENBQUMsRUFBRCxDQUFsQztBQUF3QyxDQUg5RyxFQUdpSCxVQUFTcFIsTUFBVCxFQUFnQnVTLG1CQUFoQixFQUFvQ25CLG1CQUFwQyxFQUF3RDtBQUFDOztBQUFhQSxxQkFBbUIsQ0FBQ2xSLENBQXBCLENBQXNCcVMsbUJBQXRCOztBQUEyQyxNQUFJQyx1Q0FBdUMsR0FBQ3BCLG1CQUFtQixDQUFDLENBQUQsQ0FBL0Q7O0FBQW1FLE1BQUlxQiwrQ0FBK0MsR0FBQ3JCLG1CQUFtQixDQUFDblIsQ0FBcEIsQ0FBc0J1Uyx1Q0FBdEIsQ0FBcEQ7O0FBQW1ILE1BQUlFLG1DQUFtQyxHQUFDdEIsbUJBQW1CLENBQUMsQ0FBRCxDQUEzRDs7QUFBK0QsTUFBSXVCLDJDQUEyQyxHQUFDdkIsbUJBQW1CLENBQUNuUixDQUFwQixDQUFzQnlTLG1DQUF0QixDQUFoRDs7QUFBMkcsTUFBSUUsMENBQTBDLEdBQUN4QixtQkFBbUIsQ0FBQyxFQUFELENBQWxFOztBQUF1RSxXQUFTZSxPQUFULENBQWlCbmdDLEdBQWpCLEVBQXFCO0FBQUM7O0FBQTBCLFFBQUcsT0FBTzAvQixNQUFQLEtBQWdCLFVBQWhCLElBQTRCLE9BQU9BLE1BQU0sQ0FBQ1UsUUFBZCxLQUF5QixRQUF4RCxFQUFpRTtBQUFDRCxhQUFPLEdBQUMsU0FBU0EsT0FBVCxDQUFpQm5nQyxHQUFqQixFQUFxQjtBQUFDLGVBQU8sT0FBT0EsR0FBZDtBQUFtQixPQUFqRDtBQUFtRCxLQUFySCxNQUF5SDtBQUFDbWdDLGFBQU8sR0FBQyxTQUFTQSxPQUFULENBQWlCbmdDLEdBQWpCLEVBQXFCO0FBQUMsZUFBT0EsR0FBRyxJQUFFLE9BQU8wL0IsTUFBUCxLQUFnQixVQUFyQixJQUFpQzEvQixHQUFHLENBQUNxZ0MsV0FBSixLQUFrQlgsTUFBbkQsSUFBMkQxL0IsR0FBRyxLQUFHMC9CLE1BQU0sQ0FBQ25yQyxTQUF4RSxHQUFrRixRQUFsRixHQUEyRixPQUFPeUwsR0FBekc7QUFBOEcsT0FBNUk7QUFBOEk7O0FBQUEsV0FBT21nQyxPQUFPLENBQUNuZ0MsR0FBRCxDQUFkO0FBQXFCOztBQUN2OUIsTUFBSTZnQyxTQUFTLEdBQUNILG1DQUFtQyxDQUFDLFFBQUQsQ0FBbkMsQ0FBOENJLFFBQTVEO0FBQXFFSixxQ0FBbUMsQ0FBQyxRQUFELENBQW5DLENBQThDSSxRQUE5QyxHQUF1REYsMENBQTBDLENBQUMsU0FBRCxDQUFqRzs7QUFBNkdGLHFDQUFtQyxDQUFDLFFBQUQsQ0FBbkMsQ0FBOENJLFFBQTlDLENBQXVEQyxVQUF2RCxHQUFrRSxZQUFVO0FBQUNMLHVDQUFtQyxDQUFDLFFBQUQsQ0FBbkMsQ0FBOENJLFFBQTlDLEdBQXVERCxTQUF2RDtBQUFpRSxXQUFPLElBQVA7QUFBYSxHQUEzSjs7QUFBNEosTUFBRyxnQkFBYyxPQUFPSCxtQ0FBbUMsQ0FBQyxRQUFELENBQTNELEVBQXNFO0FBQUMsUUFBSU0sWUFBWSxHQUFDLFNBQVNBLFlBQVQsR0FBdUI7QUFBQyxXQUFJLElBQUlDLElBQUksR0FBQ3RrQyxTQUFTLENBQUMvSCxNQUFuQixFQUEwQjZILElBQUksR0FBQyxJQUFJdzBCLEtBQUosQ0FBVWdRLElBQVYsQ0FBL0IsRUFBK0NDLElBQUksR0FBQyxDQUF4RCxFQUEwREEsSUFBSSxHQUFDRCxJQUEvRCxFQUFvRUMsSUFBSSxFQUF4RSxFQUEyRTtBQUFDemtDLFlBQUksQ0FBQ3lrQyxJQUFELENBQUosR0FBV3ZrQyxTQUFTLENBQUN1a0MsSUFBRCxDQUFwQjtBQUE0Qjs7QUFDdGlCalEsV0FBSyxDQUFDMThCLFNBQU4sQ0FBZ0Jpc0IsT0FBaEIsQ0FBd0Jsa0IsSUFBeEIsQ0FBNkJHLElBQTdCLEVBQWtDLElBQWxDOztBQUF3QyxVQUFJMGtDLEdBQUcsR0FBQ1AsMENBQTBDLENBQUMsU0FBRCxDQUExQyxDQUFzRDdwQyxLQUF0RCxDQUE0RDJwQyxtQ0FBbUMsQ0FBQyxRQUFELENBQS9GLEVBQTBHamtDLElBQTFHLENBQVI7O0FBQXdILGFBQU0sYUFBVzBqQyxPQUFPLENBQUNnQixHQUFELENBQWxCLEdBQXdCQSxHQUF4QixHQUE0QixJQUFsQztBQUF3QyxLQUQ2TTs7QUFDNU1ILGdCQUFZLENBQUNYLFdBQWIsR0FBeUJPLDBDQUEwQyxDQUFDLFNBQUQsQ0FBMUMsQ0FBc0RQLFdBQS9FO0FBQTJGLFFBQUllLFdBQVcsR0FBQ1YsbUNBQW1DLENBQUMsUUFBRCxDQUFuQyxDQUE4QzlzQyxFQUE5QyxDQUFpRGt0QyxRQUFqRTtBQUEwRUosdUNBQW1DLENBQUMsUUFBRCxDQUFuQyxDQUE4QzlzQyxFQUE5QyxDQUFpRGt0QyxRQUFqRCxHQUEwREUsWUFBMUQ7O0FBQXVFTix1Q0FBbUMsQ0FBQyxRQUFELENBQW5DLENBQThDOXNDLEVBQTlDLENBQWlEa3RDLFFBQWpELENBQTBEQyxVQUExRCxHQUFxRSxZQUFVO0FBQUNMLHlDQUFtQyxDQUFDLFFBQUQsQ0FBbkMsQ0FBOEM5c0MsRUFBOUMsQ0FBaURrdEMsUUFBakQsR0FBMERNLFdBQTFEO0FBQXNFLGFBQU8sSUFBUDtBQUFhLEtBQW5LO0FBQXFLOztBQUMxbEJYLGlEQUErQyxHQUFHLFlBQVU7QUFBQ2g5QixVQUFNLENBQUNtOUIsMENBQTBDLENBQUMsU0FBRCxDQUEzQyxDQUFOLENBQThEemdDLFFBQVEsQ0FBQ3lQLGdCQUFULENBQTBCLGlCQUExQixDQUE5RDtBQUE2RyxHQUEzSCxDQUEvQztBQUE2SyxDQU41SyxFQU0rSyxVQUFTb2UsTUFBVCxFQUFnQnVTLG1CQUFoQixFQUFvQ25CLG1CQUFwQyxFQUF3RDtBQUFDOztBQUFhQSxxQkFBbUIsQ0FBQ2xSLENBQXBCLENBQXNCcVMsbUJBQXRCOztBQUEyQyxNQUFJQyx1Q0FBdUMsR0FBQ3BCLG1CQUFtQixDQUFDLENBQUQsQ0FBL0Q7O0FBQW1FLE1BQUlxQiwrQ0FBK0MsR0FBQ3JCLG1CQUFtQixDQUFDblIsQ0FBcEIsQ0FBc0J1Uyx1Q0FBdEIsQ0FBcEQ7O0FBQW1ILE1BQUlFLG1DQUFtQyxHQUFDdEIsbUJBQW1CLENBQUMsQ0FBRCxDQUEzRDs7QUFBK0QsTUFBSXVCLDJDQUEyQyxHQUFDdkIsbUJBQW1CLENBQUNuUixDQUFwQixDQUFzQnlTLG1DQUF0QixDQUFoRDs7QUFBMkcsV0FBU1csY0FBVCxDQUF3QkMsR0FBeEIsRUFBNEI1c0MsQ0FBNUIsRUFBOEI7QUFBQyxXQUFPNnNDLGVBQWUsQ0FBQ0QsR0FBRCxDQUFmLElBQXNCRSxxQkFBcUIsQ0FBQ0YsR0FBRCxFQUFLNXNDLENBQUwsQ0FBM0MsSUFBb0Qrc0MsMkJBQTJCLENBQUNILEdBQUQsRUFBSzVzQyxDQUFMLENBQS9FLElBQXdGZ3RDLGdCQUFnQixFQUEvRztBQUFtSDs7QUFDbnhCLFdBQVNBLGdCQUFULEdBQTJCO0FBQUMsVUFBTSxJQUFJOVMsU0FBSixDQUFjLDJJQUFkLENBQU47QUFBa0s7O0FBQzlMLFdBQVM2UywyQkFBVCxDQUFxQ3RULENBQXJDLEVBQXVDd1QsTUFBdkMsRUFBOEM7QUFBQyxRQUFHLENBQUN4VCxDQUFKLEVBQU07QUFBTyxRQUFHLE9BQU9BLENBQVAsS0FBVyxRQUFkLEVBQXVCLE9BQU95VCxpQkFBaUIsQ0FBQ3pULENBQUQsRUFBR3dULE1BQUgsQ0FBeEI7QUFBbUMsUUFBSTFULENBQUMsR0FBQ3hxQixNQUFNLENBQUNsUCxTQUFQLENBQWlCeXBDLFFBQWpCLENBQTBCMWhDLElBQTFCLENBQStCNnhCLENBQS9CLEVBQWtDenhCLEtBQWxDLENBQXdDLENBQXhDLEVBQTBDLENBQUMsQ0FBM0MsQ0FBTjtBQUFvRCxRQUFHdXhCLENBQUMsS0FBRyxRQUFKLElBQWNFLENBQUMsQ0FBQ2tTLFdBQW5CLEVBQStCcFMsQ0FBQyxHQUFDRSxDQUFDLENBQUNrUyxXQUFGLENBQWNob0MsSUFBaEI7QUFBcUIsUUFBRzQxQixDQUFDLEtBQUcsS0FBSixJQUFXQSxDQUFDLEtBQUcsS0FBbEIsRUFBd0IsT0FBT2dELEtBQUssQ0FBQzNhLElBQU4sQ0FBVzZYLENBQVgsQ0FBUDtBQUFxQixRQUFHRixDQUFDLEtBQUcsV0FBSixJQUFpQiwyQ0FBMkNqZCxJQUEzQyxDQUFnRGlkLENBQWhELENBQXBCLEVBQXVFLE9BQU8yVCxpQkFBaUIsQ0FBQ3pULENBQUQsRUFBR3dULE1BQUgsQ0FBeEI7QUFBb0M7O0FBQ3RYLFdBQVNDLGlCQUFULENBQTJCTixHQUEzQixFQUErQnZRLEdBQS9CLEVBQW1DO0FBQUMsUUFBR0EsR0FBRyxJQUFFLElBQUwsSUFBV0EsR0FBRyxHQUFDdVEsR0FBRyxDQUFDMXNDLE1BQXRCLEVBQTZCbThCLEdBQUcsR0FBQ3VRLEdBQUcsQ0FBQzFzQyxNQUFSOztBQUFlLFNBQUksSUFBSUYsQ0FBQyxHQUFDLENBQU4sRUFBUW10QyxJQUFJLEdBQUMsSUFBSTVRLEtBQUosQ0FBVUYsR0FBVixDQUFqQixFQUFnQ3I4QixDQUFDLEdBQUNxOEIsR0FBbEMsRUFBc0NyOEIsQ0FBQyxFQUF2QyxFQUEwQztBQUFDbXRDLFVBQUksQ0FBQ250QyxDQUFELENBQUosR0FBUTRzQyxHQUFHLENBQUM1c0MsQ0FBRCxDQUFYO0FBQWdCOztBQUFBLFdBQU9tdEMsSUFBUDtBQUFhOztBQUN4SixXQUFTTCxxQkFBVCxDQUErQkYsR0FBL0IsRUFBbUM1c0MsQ0FBbkMsRUFBcUM7QUFBQyxRQUFHLE9BQU9nckMsTUFBUCxLQUFnQixXQUFoQixJQUE2QixFQUFFQSxNQUFNLENBQUNVLFFBQVAsSUFBbUIzOEIsTUFBTSxDQUFDNjlCLEdBQUQsQ0FBM0IsQ0FBaEMsRUFBa0U7QUFBTyxRQUFJUSxJQUFJLEdBQUMsRUFBVDtBQUFZLFFBQUlDLEVBQUUsR0FBQyxJQUFQO0FBQVksUUFBSUMsRUFBRSxHQUFDLEtBQVA7QUFBYSxRQUFJQyxFQUFFLEdBQUMzdUMsU0FBUDs7QUFBaUIsUUFBRztBQUFDLFdBQUksSUFBSTR1QyxFQUFFLEdBQUNaLEdBQUcsQ0FBQzVCLE1BQU0sQ0FBQ1UsUUFBUixDQUFILEVBQVAsRUFBOEIrQixFQUFsQyxFQUFxQyxFQUFFSixFQUFFLEdBQUMsQ0FBQ0ksRUFBRSxHQUFDRCxFQUFFLENBQUMvdUIsSUFBSCxFQUFKLEVBQWVpdkIsSUFBcEIsQ0FBckMsRUFBK0RMLEVBQUUsR0FBQyxJQUFsRSxFQUF1RTtBQUFDRCxZQUFJLENBQUNockMsSUFBTCxDQUFVcXJDLEVBQUUsQ0FBQ2hsQyxLQUFiOztBQUFvQixZQUFHekksQ0FBQyxJQUFFb3RDLElBQUksQ0FBQ2x0QyxNQUFMLEtBQWNGLENBQXBCLEVBQXNCO0FBQU87QUFBQyxLQUE5SCxDQUE4SCxPQUFNNDZCLEdBQU4sRUFBVTtBQUFDMFMsUUFBRSxHQUFDLElBQUg7QUFBUUMsUUFBRSxHQUFDM1MsR0FBSDtBQUFRLEtBQXpKLFNBQWdLO0FBQUMsVUFBRztBQUFDLFlBQUcsQ0FBQ3lTLEVBQUQsSUFBS0csRUFBRSxDQUFDLFFBQUQsQ0FBRixJQUFjLElBQXRCLEVBQTJCQSxFQUFFLENBQUMsUUFBRCxDQUFGO0FBQWdCLE9BQS9DLFNBQXNEO0FBQUMsWUFBR0YsRUFBSCxFQUFNLE1BQU1DLEVBQU47QUFBVTtBQUFDOztBQUFBLFdBQU9ILElBQVA7QUFBYTs7QUFDM1osV0FBU1AsZUFBVCxDQUF5QkQsR0FBekIsRUFBNkI7QUFBQyxRQUFHclEsS0FBSyxDQUFDNThCLE9BQU4sQ0FBY2l0QyxHQUFkLENBQUgsRUFBc0IsT0FBT0EsR0FBUDtBQUFZOztBQUNoRSxXQUFTbkIsT0FBVCxDQUFpQm5nQyxHQUFqQixFQUFxQjtBQUFDOztBQUEwQixRQUFHLE9BQU8wL0IsTUFBUCxLQUFnQixVQUFoQixJQUE0QixPQUFPQSxNQUFNLENBQUNVLFFBQWQsS0FBeUIsUUFBeEQsRUFBaUU7QUFBQ0QsYUFBTyxHQUFDLFNBQVNBLE9BQVQsQ0FBaUJuZ0MsR0FBakIsRUFBcUI7QUFBQyxlQUFPLE9BQU9BLEdBQWQ7QUFBbUIsT0FBakQ7QUFBbUQsS0FBckgsTUFBeUg7QUFBQ21nQyxhQUFPLEdBQUMsU0FBU0EsT0FBVCxDQUFpQm5nQyxHQUFqQixFQUFxQjtBQUFDLGVBQU9BLEdBQUcsSUFBRSxPQUFPMC9CLE1BQVAsS0FBZ0IsVUFBckIsSUFBaUMxL0IsR0FBRyxDQUFDcWdDLFdBQUosS0FBa0JYLE1BQW5ELElBQTJEMS9CLEdBQUcsS0FBRzAvQixNQUFNLENBQUNuckMsU0FBeEUsR0FBa0YsUUFBbEYsR0FBMkYsT0FBT3lMLEdBQXpHO0FBQThHLE9BQTVJO0FBQThJOztBQUFBLFdBQU9tZ0MsT0FBTyxDQUFDbmdDLEdBQUQsQ0FBZDtBQUFxQjs7QUFDN1UsV0FBUzB6QixlQUFULENBQXlCMStCLFFBQXpCLEVBQWtDdStCLFdBQWxDLEVBQThDO0FBQUMsUUFBRyxFQUFFditCLFFBQVEsWUFBWXUrQixXQUF0QixDQUFILEVBQXNDO0FBQUMsWUFBTSxJQUFJM0UsU0FBSixDQUFjLG1DQUFkLENBQU47QUFBMEQ7QUFBQzs7QUFDakosV0FBU3lULGlCQUFULENBQTJCNXZCLE1BQTNCLEVBQWtDd2dCLEtBQWxDLEVBQXdDO0FBQUMsU0FBSSxJQUFJditCLENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBQ3UrQixLQUFLLENBQUNyK0IsTUFBcEIsRUFBMkJGLENBQUMsRUFBNUIsRUFBK0I7QUFBQyxVQUFJdytCLFVBQVUsR0FBQ0QsS0FBSyxDQUFDditCLENBQUQsQ0FBcEI7QUFBd0J3K0IsZ0JBQVUsQ0FBQ0MsVUFBWCxHQUFzQkQsVUFBVSxDQUFDQyxVQUFYLElBQXVCLEtBQTdDO0FBQW1ERCxnQkFBVSxDQUFDRSxZQUFYLEdBQXdCLElBQXhCO0FBQTZCLFVBQUcsV0FBVUYsVUFBYixFQUF3QkEsVUFBVSxDQUFDRyxRQUFYLEdBQW9CLElBQXBCO0FBQXlCNXZCLFlBQU0sQ0FBQzZ2QixjQUFQLENBQXNCN2dCLE1BQXRCLEVBQTZCeWdCLFVBQVUsQ0FBQ2xhLEdBQXhDLEVBQTRDa2EsVUFBNUM7QUFBeUQ7QUFBQzs7QUFDNVIsV0FBU0gsWUFBVCxDQUFzQlEsV0FBdEIsRUFBa0NDLFVBQWxDLEVBQTZDQyxXQUE3QyxFQUF5RDtBQUFDLFFBQUdELFVBQUgsRUFBYzZPLGlCQUFpQixDQUFDOU8sV0FBVyxDQUFDaC9CLFNBQWIsRUFBdUJpL0IsVUFBdkIsQ0FBakI7QUFBb0QsUUFBR0MsV0FBSCxFQUFlNE8saUJBQWlCLENBQUM5TyxXQUFELEVBQWFFLFdBQWIsQ0FBakI7QUFBMkMsV0FBT0YsV0FBUDtBQUFvQjs7QUFDMU0sTUFBSTVvQixTQUFTLEdBQUMrMUIsbUNBQW1DLENBQUMsUUFBRCxDQUFuQyxDQUE4Qy8xQixTQUE1RDtBQUFzRSxNQUFJMjNCLElBQUksR0FBQyxDQUFDLENBQUQsR0FBRzMzQixTQUFTLENBQUNrakIsU0FBVixDQUFvQi9xQixPQUFwQixDQUE0QixPQUE1QixDQUFILElBQXlDLENBQUMsQ0FBRCxHQUFHNkgsU0FBUyxDQUFDa2pCLFNBQVYsQ0FBb0IvcUIsT0FBcEIsQ0FBNEIsVUFBNUIsQ0FBNUMsSUFBcUYsQ0FBQyxDQUFELEdBQUc2SCxTQUFTLENBQUNrakIsU0FBVixDQUFvQi9xQixPQUFwQixDQUE0QixPQUE1QixDQUFqRztBQUFzSSxNQUFJeS9CLFFBQVEsR0FBQyxpRUFBaUV2eEIsSUFBakUsQ0FBc0VyRyxTQUFTLENBQUNrakIsU0FBaEYsQ0FBYjs7QUFBd0csTUFBSTJVLGdCQUFnQixHQUFDLFlBQVU7QUFBQyxRQUFJaGpDLFFBQVEsR0FBQyx5Q0FBeUMzRCxLQUF6QyxDQUErQyxHQUEvQyxDQUFiO0FBQWlFLFFBQUl3L0IsR0FBRyxHQUFDbDdCLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixLQUF2QixDQUFSOztBQUFzQyxTQUFJLElBQUkxTCxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUM4SyxRQUFRLENBQUM1SyxNQUF2QixFQUE4QkYsQ0FBQyxJQUFFLENBQWpDLEVBQW1DO0FBQUMsVUFBRzJtQyxHQUFHLElBQUVBLEdBQUcsQ0FBQ3Y3QixLQUFKLENBQVVOLFFBQVEsQ0FBQzlLLENBQUQsQ0FBbEIsTUFBeUJwQixTQUFqQyxFQUEyQztBQUFDLGVBQU9rTSxRQUFRLENBQUM5SyxDQUFELENBQWY7QUFBb0I7QUFBQzs7QUFDaGlCLFdBQU8sS0FBUDtBQUFjLEdBRDJULEVBQXJCOztBQUNuUyxNQUFJK3RDLGFBQUo7O0FBQWtCLFdBQVNDLGVBQVQsR0FBMEI7QUFBQyxRQUFHLENBQUNELGFBQUQsSUFBZ0J0aUMsUUFBUSxDQUFDc08sSUFBNUIsRUFBaUM7QUFBQ2cwQixtQkFBYSxHQUFDdGlDLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixLQUF2QixDQUFkO0FBQTRDcWlDLG1CQUFhLENBQUMzaUMsS0FBZCxDQUFvQjZpQyxPQUFwQixHQUE0QixrRUFBNUI7QUFBK0Z4aUMsY0FBUSxDQUFDc08sSUFBVCxDQUFjVixXQUFkLENBQTBCMDBCLGFBQTFCO0FBQTBDOztBQUNyUixXQUFNLENBQUNBLGFBQWEsR0FBQ0EsYUFBYSxDQUFDOXpCLFlBQWYsR0FBNEIsQ0FBMUMsS0FBOEMreEIsbUNBQW1DLENBQUMsUUFBRCxDQUFuQyxDQUE4QzVkLFdBQTVGLElBQXlHM2lCLFFBQVEsQ0FBQzZWLGVBQVQsQ0FBeUJySCxZQUF4STtBQUFzSjs7QUFDdEosTUFBSWkwQixJQUFKOztBQUFTLFdBQVNDLGFBQVQsR0FBd0I7QUFBQyxRQUFHTixRQUFILEVBQVk7QUFBQ0ssVUFBSSxHQUFDRixlQUFlLEVBQXBCO0FBQXdCLEtBQXJDLE1BQXlDO0FBQUNFLFVBQUksR0FBQ2xDLG1DQUFtQyxDQUFDLFFBQUQsQ0FBbkMsQ0FBOEM1ZCxXQUE5QyxJQUEyRDNpQixRQUFRLENBQUM2VixlQUFULENBQXlCckgsWUFBekY7QUFBdUc7QUFBQzs7QUFDcExrMEIsZUFBYTtBQUFHbkMscUNBQW1DLENBQUMsUUFBRCxDQUFuQyxDQUE4QzNzQixnQkFBOUMsQ0FBK0QsUUFBL0QsRUFBd0U4dUIsYUFBeEU7QUFBdUZuQyxxQ0FBbUMsQ0FBQyxRQUFELENBQW5DLENBQThDM3NCLGdCQUE5QyxDQUErRCxtQkFBL0QsRUFBbUY4dUIsYUFBbkY7QUFBa0duQyxxQ0FBbUMsQ0FBQyxRQUFELENBQW5DLENBQThDM3NCLGdCQUE5QyxDQUErRCxNQUEvRCxFQUFzRTh1QixhQUF0RTtBQUFxRnBDLGlEQUErQyxHQUFHLFlBQVU7QUFBQ29DLGlCQUFhLENBQUM7QUFBQ2xzQyxVQUFJLEVBQUM7QUFBTixLQUFELENBQWI7QUFBb0MsR0FBbEQsQ0FBL0M7QUFBbUcsTUFBSW1zQyxZQUFZLEdBQUMsRUFBakI7O0FBQW9CLFdBQVNDLFVBQVQsQ0FBb0JDLElBQXBCLEVBQXlCO0FBQUMsUUFBSUMsT0FBTyxHQUFDLEVBQVo7O0FBQWUsV0FBTSxTQUFPRCxJQUFJLENBQUNuckIsYUFBbEIsRUFBZ0M7QUFBQ21yQixVQUFJLEdBQUNBLElBQUksQ0FBQ25yQixhQUFWOztBQUF3QixVQUFHLE1BQUltckIsSUFBSSxDQUFDNUYsUUFBWixFQUFxQjtBQUFDNkYsZUFBTyxDQUFDbnNDLElBQVIsQ0FBYWtzQyxJQUFiO0FBQW9CO0FBQUM7O0FBQ2xpQixXQUFPQyxPQUFQO0FBQWdCOztBQUNoQixXQUFTQyxjQUFULEdBQXlCO0FBQUMsUUFBRyxDQUFDSixZQUFZLENBQUNsdUMsTUFBakIsRUFBd0I7QUFBQztBQUFROztBQUMzRGt1QyxnQkFBWSxDQUFDaGlDLE9BQWIsQ0FBcUIsVUFBUzVKLElBQVQsRUFBY2dPLENBQWQsRUFBZ0I7QUFBQyxVQUFJbFEsUUFBUSxHQUFDa0MsSUFBSSxDQUFDbEMsUUFBbEI7QUFBQSxVQUEyQm11QyxPQUFPLEdBQUNqc0MsSUFBSSxDQUFDaXNDLE9BQXhDO0FBQWdELFVBQUlDLFVBQVUsR0FBQ3B1QyxRQUFRLENBQUNnM0IsS0FBVCxDQUFlcnhCLHFCQUFmLEVBQWY7QUFBc0QsVUFBSTBvQyxPQUFPLEdBQUM7QUFBQzluQyxhQUFLLEVBQUM2bkMsVUFBVSxDQUFDN25DLEtBQWxCO0FBQXdCQyxjQUFNLEVBQUM0bkMsVUFBVSxDQUFDNW5DLE1BQTFDO0FBQWlEUCxXQUFHLEVBQUNtb0MsVUFBVSxDQUFDbm9DLEdBQWhFO0FBQW9FQyxjQUFNLEVBQUNrb0MsVUFBVSxDQUFDbG9DLE1BQXRGO0FBQTZGb29DLFlBQUksRUFBQzVDLG1DQUFtQyxDQUFDLFFBQUQsQ0FBbkMsQ0FBOEM1ZSxVQUFoSjtBQUEySjhnQixZQUFJLEVBQUNBO0FBQWhLLE9BQVo7QUFBa0wsVUFBSVcsU0FBUyxHQUFDLENBQUNKLE9BQUQsSUFBVUEsT0FBTyxDQUFDRyxJQUFSLEtBQWVELE9BQU8sQ0FBQ0MsSUFBakMsSUFBdUNILE9BQU8sQ0FBQ1AsSUFBUixLQUFlUyxPQUFPLENBQUNULElBQTlELElBQW9FTyxPQUFPLENBQUM1bkMsS0FBUixLQUFnQjhuQyxPQUFPLENBQUM5bkMsS0FBNUYsSUFBbUc0bkMsT0FBTyxDQUFDM25DLE1BQVIsS0FBaUI2bkMsT0FBTyxDQUFDN25DLE1BQTFJO0FBQWlKLFVBQUlnb0MsVUFBVSxHQUFDRCxTQUFTLElBQUUsQ0FBQ0osT0FBWixJQUFxQkEsT0FBTyxDQUFDbG9DLEdBQVIsS0FBY29vQyxPQUFPLENBQUNwb0MsR0FBM0MsSUFBZ0Rrb0MsT0FBTyxDQUFDam9DLE1BQVIsS0FBaUJtb0MsT0FBTyxDQUFDbm9DLE1BQXhGO0FBQStGNG5DLGtCQUFZLENBQUM1OUIsQ0FBRCxDQUFaLENBQWdCaStCLE9BQWhCLEdBQXdCRSxPQUF4Qjs7QUFBZ0MsVUFBR0UsU0FBSCxFQUFhO0FBQUN2dUMsZ0JBQVEsQ0FBQ3l1QyxRQUFUO0FBQXFCOztBQUNqbkIsVUFBR0QsVUFBSCxFQUFjO0FBQUN4dUMsZ0JBQVEsQ0FBQzB1QyxRQUFUO0FBQXFCO0FBQUMsS0FEckM7QUFDdUNoRCx1Q0FBbUMsQ0FBQyxRQUFELENBQW5DLENBQThDemlDLHFCQUE5QyxDQUFvRWlsQyxjQUFwRTtBQUFxRjs7QUFDNUgsTUFBSVMsVUFBVSxHQUFDLENBQWY7O0FBQWlCLE1BQUlDLFFBQVEsR0FBQyxZQUFVO0FBQUMsYUFBU0EsUUFBVCxDQUFrQnRzQyxJQUFsQixFQUF1QnVzQyxXQUF2QixFQUFtQztBQUFDblEscUJBQWUsQ0FBQyxJQUFELEVBQU1rUSxRQUFOLENBQWY7O0FBQStCLFVBQUlyaUMsSUFBSSxHQUFDLElBQVQ7QUFBY0EsVUFBSSxDQUFDb2lDLFVBQUwsR0FBZ0JBLFVBQWhCO0FBQTJCQSxnQkFBVSxJQUFFLENBQVo7QUFBY3BpQyxVQUFJLENBQUN5cUIsS0FBTCxHQUFXMTBCLElBQVg7QUFBZ0JpSyxVQUFJLENBQUNwQyxRQUFMLEdBQWM7QUFBQ3hJLFlBQUksRUFBQyxRQUFOO0FBQWVtdEMsYUFBSyxFQUFDLEdBQXJCO0FBQXlCQyxjQUFNLEVBQUMsSUFBaEM7QUFBcUNDLGtCQUFVLEVBQUMsZUFBaEQ7QUFBZ0VDLGVBQU8sRUFBQyxPQUF4RTtBQUFnRkMsbUJBQVcsRUFBQyxTQUE1RjtBQUFzR0MsaUJBQVMsRUFBQyxXQUFoSDtBQUE0SEMsZUFBTyxFQUFDLEtBQXBJO0FBQTBJQyx5QkFBaUIsRUFBQyxJQUE1SjtBQUFpS0MsY0FBTSxFQUFDLENBQUMsR0FBeks7QUFBNktDLHVCQUFlLEVBQUMsS0FBN0w7QUFBbU1DLG9CQUFZLEVBQUMsS0FBaE47QUFBc05DLGdCQUFRLEVBQUMsSUFBL047QUFBb09DLHNCQUFjLEVBQUMsQ0FBblA7QUFBcVBDLG9CQUFZLEVBQUMsQ0FBbFE7QUFBb1E1bUIsbUJBQVcsRUFBQyxDQUFoUjtBQUFrUjZtQixpQkFBUyxFQUFDLElBQTVSO0FBQWlTQyw0QkFBb0IsRUFBQyxJQUF0VDtBQUEyVEMsd0JBQWdCLEVBQUMsSUFBNVU7QUFBaVZwQixnQkFBUSxFQUFDLElBQTFWO0FBQStWcUIsY0FBTSxFQUFDLElBQXRXO0FBQTJXQyxpQkFBUyxFQUFDLElBQXJYO0FBQTBYQyxvQkFBWSxFQUFDO0FBQXZZLE9BQWQ7QUFBMlosVUFBSUMsV0FBVyxHQUFDM2pDLElBQUksQ0FBQ3lxQixLQUFMLENBQVdtWixPQUFYLElBQW9CLEVBQXBDO0FBQXVDLFVBQUlDLGVBQWUsR0FBQyxFQUFwQjtBQUF1QjNoQyxZQUFNLENBQUNtQyxJQUFQLENBQVlzL0IsV0FBWixFQUF5QnBrQyxPQUF6QixDQUFpQyxVQUFTa1ksR0FBVCxFQUFhO0FBQUMsWUFBSXFzQixjQUFjLEdBQUNyc0IsR0FBRyxDQUFDbEksTUFBSixDQUFXLENBQVgsRUFBYSxDQUFiLEVBQWdCcFYsV0FBaEIsS0FBOEJzZCxHQUFHLENBQUNsSSxNQUFKLENBQVcsQ0FBWCxDQUFqRDs7QUFBK0QsWUFBR3UwQixjQUFjLElBQUUsZ0JBQWMsT0FBTzlqQyxJQUFJLENBQUNwQyxRQUFMLENBQWNrbUMsY0FBZCxDQUF4QyxFQUFzRTtBQUFDRCx5QkFBZSxDQUFDQyxjQUFELENBQWYsR0FBZ0NILFdBQVcsQ0FBQ2xzQixHQUFELENBQTNDO0FBQWtEO0FBQUMsT0FBeE87QUFBME96WCxVQUFJLENBQUNyQyxPQUFMLEdBQWFxQyxJQUFJLENBQUN4RSxNQUFMLENBQVksRUFBWixFQUFld0UsSUFBSSxDQUFDcEMsUUFBcEIsRUFBNkJpbUMsZUFBN0IsRUFBNkN2QixXQUE3QyxDQUFiO0FBQXVFdGlDLFVBQUksQ0FBQytqQyxXQUFMLEdBQWlCL2pDLElBQUksQ0FBQ3hFLE1BQUwsQ0FBWSxFQUFaLEVBQWV3RSxJQUFJLENBQUNyQyxPQUFwQixDQUFqQjtBQUE4Q3VFLFlBQU0sQ0FBQ21DLElBQVAsQ0FBWXJFLElBQUksQ0FBQ3JDLE9BQWpCLEVBQTBCNEIsT0FBMUIsQ0FBa0MsVUFBU2tZLEdBQVQsRUFBYTtBQUFDLFlBQUcsV0FBU3pYLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYThaLEdBQWIsQ0FBWixFQUE4QjtBQUFDelgsY0FBSSxDQUFDckMsT0FBTCxDQUFhOFosR0FBYixJQUFrQixJQUFsQjtBQUF3QixTQUF2RCxNQUE0RCxJQUFHLFlBQVV6WCxJQUFJLENBQUNyQyxPQUFMLENBQWE4WixHQUFiLENBQWIsRUFBK0I7QUFBQ3pYLGNBQUksQ0FBQ3JDLE9BQUwsQ0FBYThaLEdBQWIsSUFBa0IsS0FBbEI7QUFBeUI7QUFBQyxPQUF0SztBQUF3S3pYLFVBQUksQ0FBQ3JDLE9BQUwsQ0FBYTRrQyxLQUFiLEdBQW1CeGxDLElBQUksQ0FBQ2daLEdBQUwsQ0FBUyxDQUFULEVBQVdoWixJQUFJLENBQUNDLEdBQUwsQ0FBUyxDQUFDLENBQVYsRUFBWWdULFVBQVUsQ0FBQ2hRLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYTRrQyxLQUFkLENBQXRCLENBQVgsQ0FBbkI7O0FBQTJFLFVBQUcsYUFBVyxPQUFPdmlDLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYXFsQyxlQUFsQyxFQUFrRDtBQUFDaGpDLFlBQUksQ0FBQ3JDLE9BQUwsQ0FBYXFsQyxlQUFiLEdBQTZCLElBQUkzaUMsTUFBSixDQUFXTCxJQUFJLENBQUNyQyxPQUFMLENBQWFxbEMsZUFBeEIsQ0FBN0I7QUFBdUU7O0FBQ3gxQyxVQUFHaGpDLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYXFsQyxlQUFiLFlBQXdDM2lDLE1BQTNDLEVBQWtEO0FBQUMsWUFBSTJqQyxxQkFBcUIsR0FBQ2hrQyxJQUFJLENBQUNyQyxPQUFMLENBQWFxbEMsZUFBdkM7O0FBQXVEaGpDLFlBQUksQ0FBQ3JDLE9BQUwsQ0FBYXFsQyxlQUFiLEdBQTZCLFlBQVU7QUFBQyxpQkFBT2dCLHFCQUFxQixDQUFDdjBCLElBQXRCLENBQTJCckcsU0FBUyxDQUFDa2pCLFNBQXJDLENBQVA7QUFBd0QsU0FBaEc7QUFBa0c7O0FBQzVNLFVBQUcsZUFBYSxPQUFPdHNCLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYXFsQyxlQUFwQyxFQUFvRDtBQUFDaGpDLFlBQUksQ0FBQ3JDLE9BQUwsQ0FBYXFsQyxlQUFiLEdBQTZCLFlBQVU7QUFBQyxpQkFBTyxLQUFQO0FBQWMsU0FBdEQ7QUFBd0Q7O0FBQzdHLFVBQUcsYUFBVyxPQUFPaGpDLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYXNsQyxZQUFsQyxFQUErQztBQUFDampDLFlBQUksQ0FBQ3JDLE9BQUwsQ0FBYXNsQyxZQUFiLEdBQTBCLElBQUk1aUMsTUFBSixDQUFXTCxJQUFJLENBQUNyQyxPQUFMLENBQWFzbEMsWUFBeEIsQ0FBMUI7QUFBaUU7O0FBQ2pILFVBQUdqakMsSUFBSSxDQUFDckMsT0FBTCxDQUFhc2xDLFlBQWIsWUFBcUM1aUMsTUFBeEMsRUFBK0M7QUFBQyxZQUFJNGpDLGtCQUFrQixHQUFDamtDLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYXNsQyxZQUFwQzs7QUFBaURqakMsWUFBSSxDQUFDckMsT0FBTCxDQUFhc2xDLFlBQWIsR0FBMEIsWUFBVTtBQUFDLGlCQUFPZ0Isa0JBQWtCLENBQUN4MEIsSUFBbkIsQ0FBd0JyRyxTQUFTLENBQUNrakIsU0FBbEMsQ0FBUDtBQUFxRCxTQUExRjtBQUE0Rjs7QUFDN0wsVUFBRyxlQUFhLE9BQU90c0IsSUFBSSxDQUFDckMsT0FBTCxDQUFhc2xDLFlBQXBDLEVBQWlEO0FBQUNqakMsWUFBSSxDQUFDckMsT0FBTCxDQUFhc2xDLFlBQWIsR0FBMEIsWUFBVTtBQUFDLGlCQUFPLEtBQVA7QUFBYyxTQUFuRDtBQUFxRDs7QUFDdkcsVUFBSWlCLFdBQVcsR0FBQ2xrQyxJQUFJLENBQUNyQyxPQUFMLENBQWFtbEMsaUJBQTdCOztBQUErQyxVQUFHb0IsV0FBVyxJQUFFLGFBQVd0RixPQUFPLENBQUNzRixXQUFELENBQS9CLElBQThDLGdCQUFjLE9BQU9BLFdBQVcsQ0FBQzd3QyxNQUFsRixFQUF5RjtBQUFDLFlBQUk4d0MsWUFBWSxHQUFDRCxXQUFqQjs7QUFBNkIsWUFBSUUsYUFBYSxHQUFDdEUsY0FBYyxDQUFDcUUsWUFBRCxFQUFjLENBQWQsQ0FBaEM7O0FBQWlERCxtQkFBVyxHQUFDRSxhQUFhLENBQUMsQ0FBRCxDQUF6QjtBQUE4Qjs7QUFDclAsVUFBRyxFQUFFRixXQUFXLFlBQVkvSSxPQUF6QixDQUFILEVBQXFDO0FBQUMrSSxtQkFBVyxHQUFDLElBQVo7QUFBa0I7O0FBQ3hEbGtDLFVBQUksQ0FBQ3JDLE9BQUwsQ0FBYW1sQyxpQkFBYixHQUErQm9CLFdBQS9CO0FBQTJDbGtDLFVBQUksQ0FBQzhZLEtBQUwsR0FBVztBQUFDdEssV0FBRyxFQUFDeE8sSUFBSSxDQUFDckMsT0FBTCxDQUFhNmtDLE1BQWIsSUFBcUIsSUFBMUI7QUFBK0I2QixrQkFBVSxFQUFDLElBQTFDO0FBQStDQyxpQkFBUyxFQUFDLEtBQXpEO0FBQStEdGhDLGdCQUFRLEVBQUMsMkJBQTJCeU0sSUFBM0IsQ0FBZ0NyRyxTQUFTLENBQUNrakIsU0FBMUMsSUFBcUQsVUFBckQsR0FBZ0U7QUFBeEksT0FBWDs7QUFBNEosVUFBR3RzQixJQUFJLENBQUN1a0MsT0FBTCxNQUFnQnZrQyxJQUFJLENBQUN3a0MsZUFBTCxFQUFuQixFQUEwQztBQUFDeGtDLFlBQUksQ0FBQzJJLElBQUw7QUFBYTtBQUFDOztBQUNoUTZvQixnQkFBWSxDQUFDNlEsUUFBRCxFQUFVLENBQUM7QUFBQzVxQixTQUFHLEVBQUMsS0FBTDtBQUFXN2IsV0FBSyxFQUFDLFNBQVMxRSxHQUFULENBQWE2RyxFQUFiLEVBQWdCMG1DLE1BQWhCLEVBQXVCO0FBQUMsWUFBRyxhQUFXLE9BQU9BLE1BQXJCLEVBQTRCO0FBQUMsaUJBQU90RixtQ0FBbUMsQ0FBQyxRQUFELENBQW5DLENBQThDN0wsZ0JBQTlDLENBQStEdjFCLEVBQS9ELEVBQW1FdzFCLGdCQUFuRSxDQUFvRmtSLE1BQXBGLENBQVA7QUFBb0c7O0FBQ2pNLFlBQUdBLE1BQU0sQ0FBQzVaLFNBQVAsSUFBa0JvVyxnQkFBckIsRUFBc0M7QUFBQ3dELGdCQUFNLENBQUN4RCxnQkFBRCxDQUFOLEdBQXlCd0QsTUFBTSxDQUFDNVosU0FBaEM7QUFBMkM7O0FBQ2xGM29CLGNBQU0sQ0FBQ21DLElBQVAsQ0FBWW9nQyxNQUFaLEVBQW9CbGxDLE9BQXBCLENBQTRCLFVBQVNrWSxHQUFULEVBQWE7QUFBQzFaLFlBQUUsQ0FBQ1EsS0FBSCxDQUFTa1osR0FBVCxJQUFjZ3RCLE1BQU0sQ0FBQ2h0QixHQUFELENBQXBCO0FBQTJCLFNBQXJFO0FBQXVFLGVBQU8xWixFQUFQO0FBQVc7QUFGM0QsS0FBRCxFQUU4RDtBQUFDMFosU0FBRyxFQUFDLFFBQUw7QUFBYzdiLFdBQUssRUFBQyxTQUFTSixNQUFULENBQWdCa3BDLEdBQWhCLEVBQW9CO0FBQUMsYUFBSSxJQUFJaEYsSUFBSSxHQUFDdGtDLFNBQVMsQ0FBQy9ILE1BQW5CLEVBQTBCNkgsSUFBSSxHQUFDLElBQUl3MEIsS0FBSixDQUFVZ1EsSUFBSSxHQUFDLENBQUwsR0FBT0EsSUFBSSxHQUFDLENBQVosR0FBYyxDQUF4QixDQUEvQixFQUEwREMsSUFBSSxHQUFDLENBQW5FLEVBQXFFQSxJQUFJLEdBQUNELElBQTFFLEVBQStFQyxJQUFJLEVBQW5GLEVBQXNGO0FBQUN6a0MsY0FBSSxDQUFDeWtDLElBQUksR0FBQyxDQUFOLENBQUosR0FBYXZrQyxTQUFTLENBQUN1a0MsSUFBRCxDQUF0QjtBQUE4Qjs7QUFDbFArRSxXQUFHLEdBQUNBLEdBQUcsSUFBRSxFQUFUO0FBQVl4aUMsY0FBTSxDQUFDbUMsSUFBUCxDQUFZbkosSUFBWixFQUFrQnFFLE9BQWxCLENBQTBCLFVBQVNwTSxDQUFULEVBQVc7QUFBQyxjQUFHLENBQUMrSCxJQUFJLENBQUMvSCxDQUFELENBQVIsRUFBWTtBQUFDO0FBQVE7O0FBQ3ZFK08sZ0JBQU0sQ0FBQ21DLElBQVAsQ0FBWW5KLElBQUksQ0FBQy9ILENBQUQsQ0FBaEIsRUFBcUJvTSxPQUFyQixDQUE2QixVQUFTa1ksR0FBVCxFQUFhO0FBQUNpdEIsZUFBRyxDQUFDanRCLEdBQUQsQ0FBSCxHQUFTdmMsSUFBSSxDQUFDL0gsQ0FBRCxDQUFKLENBQVFza0IsR0FBUixDQUFUO0FBQXVCLFdBQWxFO0FBQXFFLFNBRHpEO0FBQzJELGVBQU9pdEIsR0FBUDtBQUFZO0FBRkMsS0FGOUQsRUFJK0Q7QUFBQ2p0QixTQUFHLEVBQUMsZUFBTDtBQUFxQjdiLFdBQUssRUFBQyxTQUFTK29DLGFBQVQsR0FBd0I7QUFBQyxlQUFNO0FBQUMzcUMsZUFBSyxFQUFDbWxDLG1DQUFtQyxDQUFDLFFBQUQsQ0FBbkMsQ0FBOEM1ZSxVQUE5QyxJQUEwRDNoQixRQUFRLENBQUM2VixlQUFULENBQXlCZ0YsV0FBMUY7QUFBc0d4ZixnQkFBTSxFQUFDb25DLElBQTdHO0FBQWtIemdDLFdBQUMsRUFBQ2hDLFFBQVEsQ0FBQzZWLGVBQVQsQ0FBeUI2VDtBQUE3SSxTQUFOO0FBQStKO0FBQW5OLEtBSi9ELEVBSW9SO0FBQUM3USxTQUFHLEVBQUMsU0FBTDtBQUFlN2IsV0FBSyxFQUFDLFNBQVMyb0MsT0FBVCxHQUFrQjtBQUFDLFlBQUl2a0MsSUFBSSxHQUFDLElBQVQ7QUFBYyxZQUFJNGtDLFdBQVcsR0FBQzVrQyxJQUFJLENBQUNyQyxPQUFMLENBQWE4a0MsVUFBN0I7O0FBQXdDLFlBQUdtQyxXQUFXLElBQUUsYUFBVyxPQUFPQSxXQUFsQyxFQUE4QztBQUFDQSxxQkFBVyxHQUFDNWtDLElBQUksQ0FBQ3lxQixLQUFMLENBQVdoaUIsYUFBWCxDQUF5Qm04QixXQUF6QixDQUFaO0FBQW1EOztBQUMxZSxZQUFHLEVBQUVBLFdBQVcsWUFBWXpKLE9BQXpCLENBQUgsRUFBcUM7QUFBQyxjQUFHbjdCLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYTZrQyxNQUFoQixFQUF1QjtBQUFDb0MsdUJBQVcsR0FBQyxJQUFJaHNDLEtBQUosRUFBWjtBQUF3QmdzQyx1QkFBVyxDQUFDcDJCLEdBQVosR0FBZ0J4TyxJQUFJLENBQUNyQyxPQUFMLENBQWE2a0MsTUFBN0I7QUFBcUMsV0FBckYsTUFBeUY7QUFBQ29DLHVCQUFXLEdBQUMsSUFBWjtBQUFrQjtBQUFDOztBQUNuSixZQUFHQSxXQUFILEVBQWU7QUFBQyxjQUFHNWtDLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYWtsQyxPQUFoQixFQUF3QjtBQUFDN2lDLGdCQUFJLENBQUM4WSxLQUFMLENBQVcyUixLQUFYLEdBQWlCbWEsV0FBVyxDQUFDajRCLFNBQVosQ0FBc0IsSUFBdEIsQ0FBakI7QUFBOEMsV0FBdkUsTUFBMkU7QUFBQzNNLGdCQUFJLENBQUM4WSxLQUFMLENBQVcyUixLQUFYLEdBQWlCbWEsV0FBakI7QUFBNkI1a0MsZ0JBQUksQ0FBQzhZLEtBQUwsQ0FBVytyQixXQUFYLEdBQXVCRCxXQUFXLENBQUMxeUIsVUFBbkM7QUFBK0M7O0FBQ3hLbFMsY0FBSSxDQUFDOFksS0FBTCxDQUFXd3JCLFNBQVgsR0FBcUIsSUFBckI7QUFBMkI7O0FBQzNCLFlBQUd0a0MsSUFBSSxDQUFDOFksS0FBTCxDQUFXMlIsS0FBZCxFQUFvQjtBQUFDLGlCQUFPLElBQVA7QUFBYTs7QUFDbEMsWUFBRyxTQUFPenFCLElBQUksQ0FBQzhZLEtBQUwsQ0FBV3RLLEdBQXJCLEVBQXlCO0FBQUN4TyxjQUFJLENBQUM4WSxLQUFMLENBQVd0SyxHQUFYLEdBQWUsZ0ZBQWY7QUFBZ0d4TyxjQUFJLENBQUM4WSxLQUFMLENBQVdnc0IsT0FBWCxHQUFtQjlrQyxJQUFJLENBQUM5SSxHQUFMLENBQVM4SSxJQUFJLENBQUN5cUIsS0FBZCxFQUFvQixrQkFBcEIsQ0FBbkI7QUFBNEQ7O0FBQ3RMLGVBQU0sRUFBRSxDQUFDenFCLElBQUksQ0FBQzhZLEtBQUwsQ0FBV2dzQixPQUFaLElBQXFCLFdBQVM5a0MsSUFBSSxDQUFDOFksS0FBTCxDQUFXZ3NCLE9BQTNDLENBQU47QUFBMkQ7QUFOK08sS0FKcFIsRUFVdUM7QUFBQ3J0QixTQUFHLEVBQUMsaUJBQUw7QUFBdUI3YixXQUFLLEVBQUMsU0FBUzRvQyxlQUFULEdBQTBCO0FBQUMsZUFBT3ZELGdCQUFnQixJQUFFLENBQUMsS0FBS3RqQyxPQUFMLENBQWFxbEMsZUFBYixFQUExQjtBQUEwRDtBQUFsSCxLQVZ2QyxFQVUySjtBQUFDdnJCLFNBQUcsRUFBQyxNQUFMO0FBQVk3YixXQUFLLEVBQUMsU0FBUytNLElBQVQsR0FBZTtBQUFDLFlBQUkzSSxJQUFJLEdBQUMsSUFBVDtBQUFjLFlBQUkra0MsZUFBZSxHQUFDO0FBQUMvaEMsa0JBQVEsRUFBQyxVQUFWO0FBQXFCdEosYUFBRyxFQUFDLENBQXpCO0FBQTJCSSxjQUFJLEVBQUMsQ0FBaEM7QUFBa0NFLGVBQUssRUFBQyxNQUF4QztBQUErQ0MsZ0JBQU0sRUFBQyxNQUF0RDtBQUE2RHdNLGtCQUFRLEVBQUM7QUFBdEUsU0FBcEI7QUFBb0csWUFBSXUrQixXQUFXLEdBQUM7QUFBQ25RLHVCQUFhLEVBQUMsTUFBZjtBQUFzQmxKLHdCQUFjLEVBQUMsYUFBckM7QUFBbURzWiw0QkFBa0IsRUFBQyxRQUF0RTtBQUErRUMsb0JBQVUsRUFBQztBQUExRixTQUFoQjs7QUFBK0gsWUFBRyxDQUFDbGxDLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYWtsQyxPQUFqQixFQUF5QjtBQUFDLGNBQUlzQyxRQUFRLEdBQUNubEMsSUFBSSxDQUFDeXFCLEtBQUwsQ0FBV2hjLFlBQVgsQ0FBd0IsT0FBeEIsQ0FBYjs7QUFBOEMsY0FBRzAyQixRQUFILEVBQVk7QUFBQ25sQyxnQkFBSSxDQUFDeXFCLEtBQUwsQ0FBVzFkLFlBQVgsQ0FBd0IsK0JBQXhCLEVBQXdEbzRCLFFBQXhEO0FBQW1FOztBQUM1bEIsY0FBR25sQyxJQUFJLENBQUM4WSxLQUFMLENBQVd3ckIsU0FBZCxFQUF3QjtBQUFDLGdCQUFJYyxXQUFXLEdBQUNwbEMsSUFBSSxDQUFDOFksS0FBTCxDQUFXMlIsS0FBWCxDQUFpQmhjLFlBQWpCLENBQThCLE9BQTlCLENBQWhCOztBQUF1RCxnQkFBRzIyQixXQUFILEVBQWU7QUFBQ3BsQyxrQkFBSSxDQUFDOFksS0FBTCxDQUFXMlIsS0FBWCxDQUFpQjFkLFlBQWpCLENBQThCLCtCQUE5QixFQUE4RHE0QixXQUE5RDtBQUE0RTtBQUFDO0FBQUM7O0FBQzlLLFlBQUcsYUFBV3BsQyxJQUFJLENBQUM5SSxHQUFMLENBQVM4SSxJQUFJLENBQUN5cUIsS0FBZCxFQUFvQixVQUFwQixDQUFkLEVBQThDO0FBQUN6cUIsY0FBSSxDQUFDOUksR0FBTCxDQUFTOEksSUFBSSxDQUFDeXFCLEtBQWQsRUFBb0I7QUFBQ3puQixvQkFBUSxFQUFDO0FBQVYsV0FBcEI7QUFBNEM7O0FBQzNGLFlBQUcsV0FBU2hELElBQUksQ0FBQzlJLEdBQUwsQ0FBUzhJLElBQUksQ0FBQ3lxQixLQUFkLEVBQW9CLFNBQXBCLENBQVosRUFBMkM7QUFBQ3pxQixjQUFJLENBQUM5SSxHQUFMLENBQVM4SSxJQUFJLENBQUN5cUIsS0FBZCxFQUFvQjtBQUFDc1ksa0JBQU0sRUFBQztBQUFSLFdBQXBCO0FBQWlDOztBQUM3RS9pQyxZQUFJLENBQUM4WSxLQUFMLENBQVd1ckIsVUFBWCxHQUFzQnpsQyxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBdEI7QUFBb0RtQixZQUFJLENBQUM5SSxHQUFMLENBQVM4SSxJQUFJLENBQUM4WSxLQUFMLENBQVd1ckIsVUFBcEIsRUFBK0JVLGVBQS9CO0FBQWdEL2tDLFlBQUksQ0FBQzlJLEdBQUwsQ0FBUzhJLElBQUksQ0FBQzhZLEtBQUwsQ0FBV3VyQixVQUFwQixFQUErQjtBQUFDLHFCQUFVcmtDLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYW9sQztBQUF4QixTQUEvQjs7QUFBZ0UsWUFBR2hDLElBQUgsRUFBUTtBQUFDL2dDLGNBQUksQ0FBQzlJLEdBQUwsQ0FBUzhJLElBQUksQ0FBQzhZLEtBQUwsQ0FBV3VyQixVQUFwQixFQUErQjtBQUFDL2lCLG1CQUFPLEVBQUM7QUFBVCxXQUEvQjtBQUFrRDs7QUFDL050aEIsWUFBSSxDQUFDOFksS0FBTCxDQUFXdXJCLFVBQVgsQ0FBc0J0M0IsWUFBdEIsQ0FBbUMsSUFBbkMsRUFBd0Msc0JBQXNCcE4sTUFBdEIsQ0FBNkJLLElBQUksQ0FBQ29pQyxVQUFsQyxDQUF4QztBQUF1RnBpQyxZQUFJLENBQUN5cUIsS0FBTCxDQUFXamUsV0FBWCxDQUF1QnhNLElBQUksQ0FBQzhZLEtBQUwsQ0FBV3VyQixVQUFsQzs7QUFBOEMsWUFBR3JrQyxJQUFJLENBQUM4WSxLQUFMLENBQVd3ckIsU0FBZCxFQUF3QjtBQUFDVSxxQkFBVyxHQUFDaGxDLElBQUksQ0FBQ3hFLE1BQUwsQ0FBWTtBQUFDLDBCQUFhd0UsSUFBSSxDQUFDckMsT0FBTCxDQUFhK2tDLE9BQTNCO0FBQW1DLCtCQUFrQjFpQyxJQUFJLENBQUNyQyxPQUFMLENBQWFnbEMsV0FBbEU7QUFBOEUsMkJBQWMsZUFBZWhqQyxNQUFmLENBQXNCSyxJQUFJLENBQUNyQyxPQUFMLENBQWEra0MsT0FBbkMsRUFBMkMscUJBQTNDLEVBQWtFL2lDLE1BQWxFLENBQXlFSyxJQUFJLENBQUNyQyxPQUFMLENBQWFnbEMsV0FBdEYsRUFBa0csR0FBbEcsQ0FBNUY7QUFBbU0seUJBQVk7QUFBL00sV0FBWixFQUFtT29DLGVBQW5PLEVBQW1QQyxXQUFuUCxDQUFaO0FBQTZRLFNBQXRTLE1BQTBTO0FBQUNobEMsY0FBSSxDQUFDOFksS0FBTCxDQUFXMlIsS0FBWCxHQUFpQjdyQixRQUFRLENBQUNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBakI7O0FBQStDLGNBQUdtQixJQUFJLENBQUM4WSxLQUFMLENBQVd0SyxHQUFkLEVBQWtCO0FBQUN3MkIsdUJBQVcsR0FBQ2hsQyxJQUFJLENBQUN4RSxNQUFMLENBQVk7QUFBQyxxQ0FBc0J3RSxJQUFJLENBQUNyQyxPQUFMLENBQWFnbEMsV0FBcEM7QUFBZ0QsaUNBQWtCM2lDLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYStrQyxPQUEvRTtBQUF1RixtQ0FBb0IxaUMsSUFBSSxDQUFDckMsT0FBTCxDQUFhaWxDLFNBQXhIO0FBQWtJLGtDQUFtQjVpQyxJQUFJLENBQUM4WSxLQUFMLENBQVdnc0IsT0FBWCxJQUFvQixTQUFTbmxDLE1BQVQsQ0FBZ0JLLElBQUksQ0FBQzhZLEtBQUwsQ0FBV3RLLEdBQTNCLEVBQStCLEtBQS9CO0FBQXpLLGFBQVosRUFBNE51MkIsZUFBNU4sRUFBNE9DLFdBQTVPLENBQVo7QUFBc1E7QUFBQzs7QUFDenZCLFlBQUcsY0FBWWhsQyxJQUFJLENBQUNyQyxPQUFMLENBQWF2SSxJQUF6QixJQUErQixZQUFVNEssSUFBSSxDQUFDckMsT0FBTCxDQUFhdkksSUFBdEQsSUFBNEQsb0JBQWtCNEssSUFBSSxDQUFDckMsT0FBTCxDQUFhdkksSUFBM0YsSUFBaUcsTUFBSTRLLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYTRrQyxLQUFySCxFQUEySDtBQUFDdmlDLGNBQUksQ0FBQzhZLEtBQUwsQ0FBVzlWLFFBQVgsR0FBb0IsVUFBcEI7QUFBZ0M7O0FBQzVKLFlBQUcsWUFBVWhELElBQUksQ0FBQzhZLEtBQUwsQ0FBVzlWLFFBQXhCLEVBQWlDO0FBQUMsY0FBSXFpQyxRQUFRLEdBQUM3RCxVQUFVLENBQUN4aEMsSUFBSSxDQUFDeXFCLEtBQU4sQ0FBVixDQUF1Qi8wQixNQUF2QixDQUE4QixVQUFTcUksRUFBVCxFQUFZO0FBQUMsZ0JBQUkwbUMsTUFBTSxHQUFDdEYsbUNBQW1DLENBQUMsUUFBRCxDQUFuQyxDQUE4QzdMLGdCQUE5QyxDQUErRHYxQixFQUEvRCxDQUFYO0FBQThFLGdCQUFJdW5DLGVBQWUsR0FBQ2IsTUFBTSxDQUFDLG1CQUFELENBQU4sSUFBNkJBLE1BQU0sQ0FBQyxnQkFBRCxDQUFuQyxJQUF1REEsTUFBTSxDQUFDNVosU0FBbEY7QUFBNEYsZ0JBQUkwYSxhQUFhLEdBQUMsZUFBbEI7QUFBa0MsbUJBQU9ELGVBQWUsSUFBRSxXQUFTQSxlQUExQixJQUEyQ0MsYUFBYSxDQUFDOTFCLElBQWQsQ0FBbUJnMUIsTUFBTSxDQUFDaCtCLFFBQVAsR0FBZ0JnK0IsTUFBTSxDQUFDLFlBQUQsQ0FBdEIsR0FBcUNBLE1BQU0sQ0FBQyxZQUFELENBQTlELENBQWxEO0FBQWlJLFdBQXhYLENBQWI7QUFBdVl6a0MsY0FBSSxDQUFDOFksS0FBTCxDQUFXOVYsUUFBWCxHQUFvQnFpQyxRQUFRLENBQUNoeUMsTUFBVCxHQUFnQixVQUFoQixHQUEyQixPQUEvQztBQUF3RDs7QUFDamUyeEMsbUJBQVcsQ0FBQ2hpQyxRQUFaLEdBQXFCaEQsSUFBSSxDQUFDOFksS0FBTCxDQUFXOVYsUUFBaEM7QUFBeUNoRCxZQUFJLENBQUM5SSxHQUFMLENBQVM4SSxJQUFJLENBQUM4WSxLQUFMLENBQVcyUixLQUFwQixFQUEwQnVhLFdBQTFCO0FBQXVDaGxDLFlBQUksQ0FBQzhZLEtBQUwsQ0FBV3VyQixVQUFYLENBQXNCNzNCLFdBQXRCLENBQWtDeE0sSUFBSSxDQUFDOFksS0FBTCxDQUFXMlIsS0FBN0M7QUFBb0R6cUIsWUFBSSxDQUFDa2lDLFFBQUw7QUFBZ0JsaUMsWUFBSSxDQUFDbWlDLFFBQUwsQ0FBYyxJQUFkOztBQUFvQixZQUFHbmlDLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYTZsQyxNQUFoQixFQUF1QjtBQUFDeGpDLGNBQUksQ0FBQ3JDLE9BQUwsQ0FBYTZsQyxNQUFiLENBQW9Cem9DLElBQXBCLENBQXlCaUYsSUFBekI7QUFBZ0M7O0FBQ2hPLFlBQUcsV0FBU0EsSUFBSSxDQUFDOUksR0FBTCxDQUFTOEksSUFBSSxDQUFDeXFCLEtBQWQsRUFBb0Isa0JBQXBCLENBQVosRUFBb0Q7QUFBQ3pxQixjQUFJLENBQUM5SSxHQUFMLENBQVM4SSxJQUFJLENBQUN5cUIsS0FBZCxFQUFvQjtBQUFDLGdDQUFtQjtBQUFwQixXQUFwQjtBQUFrRDs7QUFDdkd6cUIsWUFBSSxDQUFDd2xDLGlCQUFMO0FBQTBCO0FBVnVKLEtBVjNKLEVBb0JNO0FBQUMvdEIsU0FBRyxFQUFDLG1CQUFMO0FBQXlCN2IsV0FBSyxFQUFDLFNBQVM0cEMsaUJBQVQsR0FBNEI7QUFBQ2pFLG9CQUFZLENBQUNoc0MsSUFBYixDQUFrQjtBQUFDOUIsa0JBQVEsRUFBQztBQUFWLFNBQWxCOztBQUFtQyxZQUFHLE1BQUk4dEMsWUFBWSxDQUFDbHVDLE1BQXBCLEVBQTJCO0FBQUM4ckMsNkNBQW1DLENBQUMsUUFBRCxDQUFuQyxDQUE4Q3ppQyxxQkFBOUMsQ0FBb0VpbEMsY0FBcEU7QUFBcUY7QUFBQztBQUFqTixLQXBCTixFQW9CeU47QUFBQ2xxQixTQUFHLEVBQUMsd0JBQUw7QUFBOEI3YixXQUFLLEVBQUMsU0FBUzZwQyxzQkFBVCxHQUFpQztBQUFDLFlBQUl6bEMsSUFBSSxHQUFDLElBQVQ7QUFBY3VoQyxvQkFBWSxDQUFDaGlDLE9BQWIsQ0FBcUIsVUFBUzVKLElBQVQsRUFBYzhoQixHQUFkLEVBQWtCO0FBQUMsY0FBRzloQixJQUFJLENBQUNsQyxRQUFMLENBQWMydUMsVUFBZCxLQUEyQnBpQyxJQUFJLENBQUNvaUMsVUFBbkMsRUFBOEM7QUFBQ2Isd0JBQVksQ0FBQy8vQixNQUFiLENBQW9CaVcsR0FBcEIsRUFBd0IsQ0FBeEI7QUFBNEI7QUFBQyxTQUFwSDtBQUF1SDtBQUEzTSxLQXBCek4sRUFvQnNhO0FBQUNBLFNBQUcsRUFBQyxTQUFMO0FBQWU3YixXQUFLLEVBQUMsU0FBU3ZFLE9BQVQsR0FBa0I7QUFBQyxZQUFJMkksSUFBSSxHQUFDLElBQVQ7QUFBY0EsWUFBSSxDQUFDeWxDLHNCQUFMO0FBQThCLFlBQUlDLGlCQUFpQixHQUFDMWxDLElBQUksQ0FBQ3lxQixLQUFMLENBQVdoYyxZQUFYLENBQXdCLCtCQUF4QixDQUF0QjtBQUErRXpPLFlBQUksQ0FBQ3lxQixLQUFMLENBQVd0eUIsZUFBWCxDQUEyQiwrQkFBM0I7O0FBQTRELFlBQUcsQ0FBQ3V0QyxpQkFBSixFQUFzQjtBQUFDMWxDLGNBQUksQ0FBQ3lxQixLQUFMLENBQVd0eUIsZUFBWCxDQUEyQixPQUEzQjtBQUFxQyxTQUE1RCxNQUFnRTtBQUFDNkgsY0FBSSxDQUFDeXFCLEtBQUwsQ0FBVzFkLFlBQVgsQ0FBd0IsT0FBeEIsRUFBZ0MyNEIsaUJBQWhDO0FBQW9EOztBQUNoeEIsWUFBRzFsQyxJQUFJLENBQUM4WSxLQUFMLENBQVd3ckIsU0FBZCxFQUF3QjtBQUFDLGNBQUlxQixvQkFBb0IsR0FBQzNsQyxJQUFJLENBQUM4WSxLQUFMLENBQVcyUixLQUFYLENBQWlCaGMsWUFBakIsQ0FBOEIsK0JBQTlCLENBQXpCO0FBQXdGek8sY0FBSSxDQUFDOFksS0FBTCxDQUFXMlIsS0FBWCxDQUFpQnR5QixlQUFqQixDQUFpQywrQkFBakM7O0FBQWtFLGNBQUcsQ0FBQ3d0QyxvQkFBSixFQUF5QjtBQUFDM2xDLGdCQUFJLENBQUM4WSxLQUFMLENBQVcyUixLQUFYLENBQWlCdHlCLGVBQWpCLENBQWlDLE9BQWpDO0FBQTJDLFdBQXJFLE1BQXlFO0FBQUM2SCxnQkFBSSxDQUFDOFksS0FBTCxDQUFXMlIsS0FBWCxDQUFpQjFkLFlBQWpCLENBQThCLE9BQTlCLEVBQXNDMjRCLGlCQUF0QztBQUEwRDs7QUFDdlQsY0FBRzFsQyxJQUFJLENBQUM4WSxLQUFMLENBQVcrckIsV0FBZCxFQUEwQjtBQUFDN2tDLGdCQUFJLENBQUM4WSxLQUFMLENBQVcrckIsV0FBWCxDQUF1QnI0QixXQUF2QixDQUFtQ3hNLElBQUksQ0FBQzhZLEtBQUwsQ0FBVzJSLEtBQTlDO0FBQXNEO0FBQUM7O0FBQ2xGLFlBQUd6cUIsSUFBSSxDQUFDNGxDLFdBQVIsRUFBb0I7QUFBQzVsQyxjQUFJLENBQUM0bEMsV0FBTCxDQUFpQjF6QixVQUFqQixDQUE0QjRILFdBQTVCLENBQXdDOVosSUFBSSxDQUFDNGxDLFdBQTdDO0FBQTJEOztBQUNoRixZQUFHNWxDLElBQUksQ0FBQzhZLEtBQUwsQ0FBV3VyQixVQUFkLEVBQXlCO0FBQUNya0MsY0FBSSxDQUFDOFksS0FBTCxDQUFXdXJCLFVBQVgsQ0FBc0JueUIsVUFBdEIsQ0FBaUM0SCxXQUFqQyxDQUE2QzlaLElBQUksQ0FBQzhZLEtBQUwsQ0FBV3VyQixVQUF4RDtBQUFxRTs7QUFDL0YsWUFBR3JrQyxJQUFJLENBQUNyQyxPQUFMLENBQWE4bEMsU0FBaEIsRUFBMEI7QUFBQ3pqQyxjQUFJLENBQUNyQyxPQUFMLENBQWE4bEMsU0FBYixDQUF1QjFvQyxJQUF2QixDQUE0QmlGLElBQTVCO0FBQW1DOztBQUM5RCxlQUFPQSxJQUFJLENBQUN5cUIsS0FBTCxDQUFXOFUsUUFBbEI7QUFBNEI7QUFOZ2EsS0FwQnRhLEVBMEJRO0FBQUM5bkIsU0FBRyxFQUFDLGVBQUw7QUFBcUI3YixXQUFLLEVBQUMsU0FBU2lxQyxhQUFULEdBQXdCO0FBQUMsWUFBRyxZQUFVLEtBQUsvc0IsS0FBTCxDQUFXOVYsUUFBeEIsRUFBaUM7QUFBQztBQUFROztBQUM1SCxZQUFJaEQsSUFBSSxHQUFDLElBQVQ7QUFBYyxZQUFJOGxDLElBQUksR0FBQzlsQyxJQUFJLENBQUM4WSxLQUFMLENBQVd1ckIsVUFBWCxDQUFzQmpyQyxxQkFBdEIsRUFBVDtBQUF1RCxZQUFJWSxLQUFLLEdBQUM4ckMsSUFBSSxDQUFDOXJDLEtBQWY7QUFBQSxZQUFxQkMsTUFBTSxHQUFDNnJDLElBQUksQ0FBQzdyQyxNQUFqQzs7QUFBd0MsWUFBRyxDQUFDK0YsSUFBSSxDQUFDNGxDLFdBQVQsRUFBcUI7QUFBQzVsQyxjQUFJLENBQUM0bEMsV0FBTCxHQUFpQmhuQyxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBakI7QUFBaURtQixjQUFJLENBQUM0bEMsV0FBTCxDQUFpQjc0QixZQUFqQixDQUE4QixNQUE5QixFQUFxQyxVQUFyQztBQUFpRC9NLGNBQUksQ0FBQzRsQyxXQUFMLENBQWlCNzRCLFlBQWpCLENBQThCLElBQTlCLEVBQW1DLGlCQUFpQnBOLE1BQWpCLENBQXdCSyxJQUFJLENBQUNvaUMsVUFBN0IsQ0FBbkM7QUFBNkUsY0FBSTJELElBQUksR0FBQ25uQyxRQUFRLENBQUNtbkMsSUFBVCxJQUFlbm5DLFFBQVEsQ0FBQytSLG9CQUFULENBQThCLE1BQTlCLEVBQXNDLENBQXRDLENBQXhCO0FBQWlFbzFCLGNBQUksQ0FBQ3Y1QixXQUFMLENBQWlCeE0sSUFBSSxDQUFDNGxDLFdBQXRCO0FBQW9DOztBQUN2WixZQUFJbkIsTUFBTSxHQUFDLHVCQUF1QjlrQyxNQUF2QixDQUE4QkssSUFBSSxDQUFDb2lDLFVBQW5DLEVBQThDLCtCQUE5QyxFQUErRXppQyxNQUEvRSxDQUFzRjNGLEtBQXRGLEVBQTRGLEtBQTVGLEVBQW1HMkYsTUFBbkcsQ0FBMEcxRixNQUExRyxFQUFpSCxvQ0FBakgsRUFBdUowRixNQUF2SixDQUE4SjNGLEtBQTlKLEVBQW9LLE1BQXBLLEVBQTRLMkYsTUFBNUssQ0FBbUwxRixNQUFuTCxFQUEwTCw4SkFBMUwsQ0FBWDs7QUFBcVcsWUFBRytGLElBQUksQ0FBQzRsQyxXQUFMLENBQWlCSSxVQUFwQixFQUErQjtBQUFDaG1DLGNBQUksQ0FBQzRsQyxXQUFMLENBQWlCSSxVQUFqQixDQUE0QjVFLE9BQTVCLEdBQW9DcUQsTUFBcEM7QUFBNEMsU0FBNUUsTUFBZ0Y7QUFBQ3prQyxjQUFJLENBQUM0bEMsV0FBTCxDQUFpQjl6QixTQUFqQixHQUEyQjJ5QixNQUEzQjtBQUFtQztBQUFDO0FBRjViLEtBMUJSLEVBNEJzYztBQUFDaHRCLFNBQUcsRUFBQyxZQUFMO0FBQWtCN2IsV0FBSyxFQUFDLFNBQVNxcUMsVUFBVCxHQUFxQjtBQUFDLFlBQUlqbUMsSUFBSSxHQUFDLElBQVQ7QUFBYyxZQUFJOGxDLElBQUksR0FBQzlsQyxJQUFJLENBQUM4WSxLQUFMLENBQVd1ckIsVUFBWCxDQUFzQmpyQyxxQkFBdEIsRUFBVDtBQUF1RCxZQUFJOHNDLEtBQUssR0FBQ0osSUFBSSxDQUFDN3JDLE1BQWY7QUFBc0IsWUFBSXNvQyxLQUFLLEdBQUN2aUMsSUFBSSxDQUFDckMsT0FBTCxDQUFhNGtDLEtBQXZCO0FBQTZCLFlBQUk0RCxRQUFRLEdBQUMsYUFBV25tQyxJQUFJLENBQUNyQyxPQUFMLENBQWF2SSxJQUF4QixJQUE4QixxQkFBbUI0SyxJQUFJLENBQUNyQyxPQUFMLENBQWF2SSxJQUEzRTtBQUFnRixZQUFJZ3hDLFVBQVUsR0FBQyxDQUFmO0FBQWlCLFlBQUlDLE9BQU8sR0FBQ0gsS0FBWjtBQUFrQixZQUFJSSxRQUFRLEdBQUMsQ0FBYjs7QUFBZSxZQUFHSCxRQUFILEVBQVk7QUFBQyxjQUFHLElBQUU1RCxLQUFMLEVBQVc7QUFBQzZELHNCQUFVLEdBQUM3RCxLQUFLLEdBQUN4bEMsSUFBSSxDQUFDQyxHQUFMLENBQVNrcEMsS0FBVCxFQUFlN0UsSUFBZixDQUFqQjs7QUFBc0MsZ0JBQUdBLElBQUksR0FBQzZFLEtBQVIsRUFBYztBQUFDRSx3QkFBVSxJQUFFN0QsS0FBSyxJQUFFMkQsS0FBSyxHQUFDN0UsSUFBUixDQUFqQjtBQUFnQztBQUFDLFdBQWxHLE1BQXNHO0FBQUMrRSxzQkFBVSxHQUFDN0QsS0FBSyxJQUFFMkQsS0FBSyxHQUFDN0UsSUFBUixDQUFoQjtBQUErQjs7QUFDdjVCLGNBQUcsSUFBRWtCLEtBQUwsRUFBVztBQUFDOEQsbUJBQU8sR0FBQ3RwQyxJQUFJLENBQUNpWixHQUFMLENBQVNvd0IsVUFBVSxHQUFDL0UsSUFBcEIsQ0FBUjtBQUFtQyxXQUEvQyxNQUFvRCxJQUFHLElBQUVrQixLQUFMLEVBQVc7QUFBQzhELG1CQUFPLEdBQUNELFVBQVUsR0FBQzdELEtBQVgsR0FBaUJ4bEMsSUFBSSxDQUFDaVosR0FBTCxDQUFTb3dCLFVBQVQsQ0FBekI7QUFBK0MsV0FBM0QsTUFBK0Q7QUFBQ0MsbUJBQU8sSUFBRSxDQUFDaEYsSUFBSSxHQUFDNkUsS0FBTixLQUFjLElBQUUzRCxLQUFoQixDQUFUO0FBQWlDOztBQUNySjZELG9CQUFVLElBQUUsQ0FBWjtBQUFlOztBQUNmcG1DLFlBQUksQ0FBQ3VtQyxzQkFBTCxHQUE0QkgsVUFBNUI7O0FBQXVDLFlBQUdELFFBQUgsRUFBWTtBQUFDRyxrQkFBUSxHQUFDLENBQUNqRixJQUFJLEdBQUNnRixPQUFOLElBQWUsQ0FBeEI7QUFBMkIsU0FBeEMsTUFBNEM7QUFBQ0Msa0JBQVEsR0FBQyxDQUFDSixLQUFLLEdBQUNHLE9BQVAsSUFBZ0IsQ0FBekI7QUFBNEI7O0FBQ2hIcm1DLFlBQUksQ0FBQzlJLEdBQUwsQ0FBUzhJLElBQUksQ0FBQzhZLEtBQUwsQ0FBVzJSLEtBQXBCLEVBQTBCO0FBQUN4d0IsZ0JBQU0sRUFBQyxHQUFHMEYsTUFBSCxDQUFVMG1DLE9BQVYsRUFBa0IsSUFBbEIsQ0FBUjtBQUFnQ0csbUJBQVMsRUFBQyxHQUFHN21DLE1BQUgsQ0FBVTJtQyxRQUFWLEVBQW1CLElBQW5CLENBQTFDO0FBQW1FeHNDLGNBQUksRUFBQyxZQUFVa0csSUFBSSxDQUFDOFksS0FBTCxDQUFXOVYsUUFBckIsR0FBOEIsR0FBR3JELE1BQUgsQ0FBVW1tQyxJQUFJLENBQUNoc0MsSUFBZixFQUFvQixJQUFwQixDQUE5QixHQUF3RCxHQUFoSTtBQUFvSUUsZUFBSyxFQUFDLEdBQUcyRixNQUFILENBQVVtbUMsSUFBSSxDQUFDOXJDLEtBQWYsRUFBcUIsSUFBckI7QUFBMUksU0FBMUI7O0FBQWlNLFlBQUdnRyxJQUFJLENBQUNyQyxPQUFMLENBQWErbEMsWUFBaEIsRUFBNkI7QUFBQzFqQyxjQUFJLENBQUNyQyxPQUFMLENBQWErbEMsWUFBYixDQUEwQjNvQyxJQUExQixDQUErQmlGLElBQS9CO0FBQXNDOztBQUNyUSxlQUFNO0FBQUM4WSxlQUFLLEVBQUM7QUFBQzdlLGtCQUFNLEVBQUNvc0MsT0FBUjtBQUFnQkcscUJBQVMsRUFBQ0Y7QUFBMUIsV0FBUDtBQUEyQ0csbUJBQVMsRUFBQ1g7QUFBckQsU0FBTjtBQUFrRTtBQUwwWixLQTVCdGMsRUFpQzhDO0FBQUNydUIsU0FBRyxFQUFDLFdBQUw7QUFBaUI3YixXQUFLLEVBQUMsU0FBUzhxQyxTQUFULEdBQW9CO0FBQUMsZUFBTyxLQUFLQyxtQkFBTCxJQUEwQixLQUFqQztBQUF3QztBQUFwRixLQWpDOUMsRUFpQ29JO0FBQUNsdkIsU0FBRyxFQUFDLFVBQUw7QUFBZ0I3YixXQUFLLEVBQUMsU0FBU3VtQyxRQUFULENBQWtCbG1DLEtBQWxCLEVBQXdCO0FBQUMsWUFBSStELElBQUksR0FBQyxJQUFUO0FBQWMsWUFBSThsQyxJQUFJLEdBQUM5bEMsSUFBSSxDQUFDeXFCLEtBQUwsQ0FBV3J4QixxQkFBWCxFQUFUO0FBQTRDLFlBQUl3dEMsS0FBSyxHQUFDZCxJQUFJLENBQUNwc0MsR0FBZjtBQUFtQixZQUFJd3NDLEtBQUssR0FBQ0osSUFBSSxDQUFDN3JDLE1BQWY7QUFBc0IsWUFBSXdxQyxNQUFNLEdBQUMsRUFBWDtBQUFjLFlBQUlvQyxZQUFZLEdBQUNmLElBQWpCOztBQUFzQixZQUFHOWxDLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYW1sQyxpQkFBaEIsRUFBa0M7QUFBQytELHNCQUFZLEdBQUM3bUMsSUFBSSxDQUFDckMsT0FBTCxDQUFhbWxDLGlCQUFiLENBQStCMXBDLHFCQUEvQixFQUFiO0FBQXFFOztBQUN4YjRHLFlBQUksQ0FBQzJtQyxtQkFBTCxHQUF5QixLQUFHRSxZQUFZLENBQUNsdEMsTUFBaEIsSUFBd0IsS0FBR2t0QyxZQUFZLENBQUM5c0MsS0FBeEMsSUFBK0M4c0MsWUFBWSxDQUFDbnRDLEdBQWIsSUFBa0IybkMsSUFBakUsSUFBdUV3RixZQUFZLENBQUMvc0MsSUFBYixJQUFtQnFsQyxtQ0FBbUMsQ0FBQyxRQUFELENBQW5DLENBQThDNWUsVUFBaks7O0FBQTRLLFlBQUd0a0IsS0FBSyxHQUFDLEtBQUQsR0FBTyxDQUFDK0QsSUFBSSxDQUFDMm1DLG1CQUFyQixFQUF5QztBQUFDO0FBQVE7O0FBQzlOLFlBQUlHLFNBQVMsR0FBQy9wQyxJQUFJLENBQUNDLEdBQUwsQ0FBUyxDQUFULEVBQVc0cEMsS0FBWCxDQUFkO0FBQWdDLFlBQUlHLFlBQVksR0FBQ2hxQyxJQUFJLENBQUNDLEdBQUwsQ0FBUyxDQUFULEVBQVdrcEMsS0FBSyxHQUFDVSxLQUFqQixDQUFqQjtBQUF5QyxZQUFJSSxRQUFRLEdBQUNqcUMsSUFBSSxDQUFDQyxHQUFMLENBQVMsQ0FBVCxFQUFXLENBQUM0cEMsS0FBWixDQUFiO0FBQWdDLFlBQUlLLFlBQVksR0FBQ2xxQyxJQUFJLENBQUNDLEdBQUwsQ0FBUyxDQUFULEVBQVc0cEMsS0FBSyxHQUFDVixLQUFOLEdBQVk3RSxJQUF2QixDQUFqQjtBQUE4QyxZQUFJNkYsZUFBZSxHQUFDbnFDLElBQUksQ0FBQ0MsR0FBTCxDQUFTLENBQVQsRUFBV2twQyxLQUFLLElBQUVVLEtBQUssR0FBQ1YsS0FBTixHQUFZN0UsSUFBZCxDQUFoQixDQUFwQjtBQUF5RCxZQUFJOEYsV0FBVyxHQUFDcHFDLElBQUksQ0FBQ0MsR0FBTCxDQUFTLENBQVQsRUFBVyxDQUFDNHBDLEtBQUQsR0FBT3ZGLElBQVAsR0FBWTZFLEtBQXZCLENBQWhCO0FBQThDLFlBQUlrQixrQkFBa0IsR0FBQyxJQUFFLEtBQUcsQ0FBQy9GLElBQUksR0FBQ3VGLEtBQU4sS0FBY3ZGLElBQUksR0FBQzZFLEtBQW5CLENBQUgsQ0FBekI7QUFBdUQsWUFBSW1CLGNBQWMsR0FBQyxDQUFuQjs7QUFBcUIsWUFBR25CLEtBQUssR0FBQzdFLElBQVQsRUFBYztBQUFDZ0csd0JBQWMsR0FBQyxJQUFFLENBQUNMLFFBQVEsSUFBRUMsWUFBWCxJQUF5QmYsS0FBMUM7QUFBaUQsU0FBaEUsTUFBcUUsSUFBR2EsWUFBWSxJQUFFMUYsSUFBakIsRUFBc0I7QUFBQ2dHLHdCQUFjLEdBQUNOLFlBQVksR0FBQzFGLElBQTVCO0FBQWtDLFNBQXpELE1BQThELElBQUc2RixlQUFlLElBQUU3RixJQUFwQixFQUF5QjtBQUFDZ0csd0JBQWMsR0FBQ0gsZUFBZSxHQUFDN0YsSUFBL0I7QUFBcUM7O0FBQzVnQixZQUFHLGNBQVlyaEMsSUFBSSxDQUFDckMsT0FBTCxDQUFhdkksSUFBekIsSUFBK0Isb0JBQWtCNEssSUFBSSxDQUFDckMsT0FBTCxDQUFhdkksSUFBOUQsSUFBb0UscUJBQW1CNEssSUFBSSxDQUFDckMsT0FBTCxDQUFhdkksSUFBdkcsRUFBNEc7QUFBQ3F2QyxnQkFBTSxDQUFDNVosU0FBUCxHQUFpQixvQkFBakI7QUFBc0M0WixnQkFBTSxDQUFDbmpCLE9BQVAsR0FBZStsQixjQUFmO0FBQStCOztBQUNsTCxZQUFHLFlBQVVybkMsSUFBSSxDQUFDckMsT0FBTCxDQUFhdkksSUFBdkIsSUFBNkIsb0JBQWtCNEssSUFBSSxDQUFDckMsT0FBTCxDQUFhdkksSUFBL0QsRUFBb0U7QUFBQyxjQUFJMEwsS0FBSyxHQUFDLENBQVY7O0FBQVksY0FBRyxJQUFFZCxJQUFJLENBQUNyQyxPQUFMLENBQWE0a0MsS0FBbEIsRUFBd0I7QUFBQ3poQyxpQkFBSyxJQUFFZCxJQUFJLENBQUNyQyxPQUFMLENBQWE0a0MsS0FBYixHQUFtQjhFLGNBQTFCO0FBQTBDLFdBQW5FLE1BQXVFO0FBQUN2bUMsaUJBQUssSUFBRWQsSUFBSSxDQUFDckMsT0FBTCxDQUFhNGtDLEtBQWIsSUFBb0IsSUFBRThFLGNBQXRCLENBQVA7QUFBOEM7O0FBQ3ZNNUMsZ0JBQU0sQ0FBQzVaLFNBQVAsR0FBaUIsU0FBU2xyQixNQUFULENBQWdCbUIsS0FBaEIsRUFBc0Isc0JBQXRCLENBQWpCO0FBQWdFOztBQUNoRSxZQUFHLGFBQVdkLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYXZJLElBQXhCLElBQThCLHFCQUFtQjRLLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYXZJLElBQWpFLEVBQXNFO0FBQUMsY0FBSWt5QyxTQUFTLEdBQUN0bkMsSUFBSSxDQUFDdW1DLHNCQUFMLEdBQTRCYSxrQkFBMUM7O0FBQTZELGNBQUcsZUFBYXBuQyxJQUFJLENBQUM4WSxLQUFMLENBQVc5VixRQUEzQixFQUFvQztBQUFDc2tDLHFCQUFTLElBQUVWLEtBQVg7QUFBa0I7O0FBQzNMbkMsZ0JBQU0sQ0FBQzVaLFNBQVAsR0FBaUIsaUJBQWlCbHJCLE1BQWpCLENBQXdCMm5DLFNBQXhCLEVBQWtDLE9BQWxDLENBQWpCO0FBQTZEOztBQUM3RHRuQyxZQUFJLENBQUM5SSxHQUFMLENBQVM4SSxJQUFJLENBQUM4WSxLQUFMLENBQVcyUixLQUFwQixFQUEwQmdhLE1BQTFCOztBQUFrQyxZQUFHemtDLElBQUksQ0FBQ3JDLE9BQUwsQ0FBYXdrQyxRQUFoQixFQUF5QjtBQUFDbmlDLGNBQUksQ0FBQ3JDLE9BQUwsQ0FBYXdrQyxRQUFiLENBQXNCcG5DLElBQXRCLENBQTJCaUYsSUFBM0IsRUFBZ0M7QUFBQ3VuQyxtQkFBTyxFQUFDekIsSUFBVDtBQUFjZ0IscUJBQVMsRUFBQ0EsU0FBeEI7QUFBa0NDLHdCQUFZLEVBQUNBLFlBQS9DO0FBQTREQyxvQkFBUSxFQUFDQSxRQUFyRTtBQUE4RUMsd0JBQVksRUFBQ0EsWUFBM0Y7QUFBd0dDLDJCQUFlLEVBQUNBLGVBQXhIO0FBQXdJQyx1QkFBVyxFQUFDQSxXQUFwSjtBQUFnS0UsMEJBQWMsRUFBQ0EsY0FBL0s7QUFBOExELDhCQUFrQixFQUFDQTtBQUFqTixXQUFoQztBQUF1UTtBQUFDO0FBUjFLLEtBakNwSSxFQXlDZ1Q7QUFBQzN2QixTQUFHLEVBQUMsVUFBTDtBQUFnQjdiLFdBQUssRUFBQyxTQUFTc21DLFFBQVQsR0FBbUI7QUFBQyxhQUFLK0QsVUFBTDtBQUFrQixhQUFLSixhQUFMO0FBQXNCO0FBQWxGLEtBekNoVCxDQUFWLENBQVo7O0FBeUM0WixXQUFPeEQsUUFBUDtBQUFpQixHQWxEL1ksRUFBYjs7QUFrRCtaLE1BQUltRixNQUFNLEdBQUMsU0FBU0EsTUFBVCxDQUFnQjl6QyxLQUFoQixFQUFzQmlLLE9BQXRCLEVBQThCO0FBQUMsUUFBRyxjQUFZLE9BQU84cEMsV0FBUCxLQUFxQixXQUFyQixHQUFpQyxXQUFqQyxHQUE2QzdJLE9BQU8sQ0FBQzZJLFdBQUQsQ0FBaEUsSUFBK0UvekMsS0FBSyxZQUFZK3pDLFdBQWhHLEdBQTRHL3pDLEtBQUssSUFBRSxhQUFXa3JDLE9BQU8sQ0FBQ2xyQyxLQUFELENBQXpCLElBQWtDLFNBQU9BLEtBQXpDLElBQWdELE1BQUlBLEtBQUssQ0FBQ21vQyxRQUExRCxJQUFvRSxhQUFXLE9BQU9ub0MsS0FBSyxDQUFDd2hCLFFBQTNNLEVBQW9OO0FBQUN4aEIsV0FBSyxHQUFDLENBQUNBLEtBQUQsQ0FBTjtBQUFlOztBQUM5ckIsUUFBSTg3QixHQUFHLEdBQUM5N0IsS0FBSyxDQUFDTCxNQUFkO0FBQXFCLFFBQUlzUSxDQUFDLEdBQUMsQ0FBTjtBQUFRLFFBQUkrakMsR0FBSjs7QUFBUSxTQUFJLElBQUlDLEtBQUssR0FBQ3ZzQyxTQUFTLENBQUMvSCxNQUFwQixFQUEyQjZILElBQUksR0FBQyxJQUFJdzBCLEtBQUosQ0FBVWlZLEtBQUssR0FBQyxDQUFOLEdBQVFBLEtBQUssR0FBQyxDQUFkLEdBQWdCLENBQTFCLENBQWhDLEVBQTZEQyxLQUFLLEdBQUMsQ0FBdkUsRUFBeUVBLEtBQUssR0FBQ0QsS0FBL0UsRUFBcUZDLEtBQUssRUFBMUYsRUFBNkY7QUFBQzFzQyxVQUFJLENBQUMwc0MsS0FBSyxHQUFDLENBQVAsQ0FBSixHQUFjeHNDLFNBQVMsQ0FBQ3dzQyxLQUFELENBQXZCO0FBQWdDOztBQUNuSyxTQUFJamtDLENBQUosRUFBTUEsQ0FBQyxHQUFDNnJCLEdBQVIsRUFBWTdyQixDQUFDLElBQUUsQ0FBZixFQUFpQjtBQUFDLFVBQUcsYUFBV2k3QixPQUFPLENBQUNqaEMsT0FBRCxDQUFsQixJQUE2QixnQkFBYyxPQUFPQSxPQUFyRCxFQUE2RDtBQUFDLFlBQUcsQ0FBQ2pLLEtBQUssQ0FBQ2lRLENBQUQsQ0FBTCxDQUFTNDdCLFFBQWIsRUFBc0I7QUFBQzdyQyxlQUFLLENBQUNpUSxDQUFELENBQUwsQ0FBUzQ3QixRQUFULEdBQWtCLElBQUk4QyxRQUFKLENBQWEzdUMsS0FBSyxDQUFDaVEsQ0FBRCxDQUFsQixFQUFzQmhHLE9BQXRCLENBQWxCO0FBQWtEO0FBQUMsT0FBeEksTUFBNkksSUFBR2pLLEtBQUssQ0FBQ2lRLENBQUQsQ0FBTCxDQUFTNDdCLFFBQVosRUFBcUI7QUFBQ21JLFdBQUcsR0FBQ2gwQyxLQUFLLENBQUNpUSxDQUFELENBQUwsQ0FBUzQ3QixRQUFULENBQWtCNWhDLE9BQWxCLEVBQTJCbkksS0FBM0IsQ0FBaUM5QixLQUFLLENBQUNpUSxDQUFELENBQUwsQ0FBUzQ3QixRQUExQyxFQUFtRHJrQyxJQUFuRCxDQUFKO0FBQThEOztBQUNuUCxVQUFHLGdCQUFjLE9BQU93c0MsR0FBeEIsRUFBNEI7QUFBQyxlQUFPQSxHQUFQO0FBQVk7QUFBQzs7QUFDMUMsV0FBT2gwQyxLQUFQO0FBQWMsR0FKa2E7O0FBSWphOHpDLFFBQU0sQ0FBQzFJLFdBQVAsR0FBbUJ1RCxRQUFuQjtBQUE0QnJELHFCQUFtQixDQUFDLFNBQUQsQ0FBbkIsR0FBZ0N3SSxNQUFoQztBQUF5QyxDQS9FbkYsQ0FKRDs7QUFvRkE7QUFBQztBQUNEO0FBQ0E7O0FBQ0EsQ0FBQyxZQUFVO0FBQUMsTUFBSTdtQixDQUFKLEVBQU05cUIsQ0FBTjtBQUFROHFCLEdBQUMsR0FBQyxLQUFLMXVCLE1BQUwsSUFBYUgsTUFBTSxDQUFDRyxNQUF0QjtBQUE2QjRELEdBQUMsR0FBQzhxQixDQUFDLENBQUM3dUIsTUFBRCxDQUFIOztBQUFZNnVCLEdBQUMsQ0FBQ3R1QixFQUFGLENBQUt3MUMsZUFBTCxHQUFxQixVQUFTN0osQ0FBVCxFQUFXO0FBQUMsUUFBSThKLENBQUosRUFBTUMsQ0FBTixFQUFRQyxDQUFSLEVBQVV0YixDQUFWLEVBQVl1YixDQUFaLEVBQWNDLENBQWQsRUFBZ0JocUMsQ0FBaEIsRUFBa0JpcUMsQ0FBbEIsRUFBb0J4a0MsQ0FBcEIsRUFBc0J5a0MsQ0FBdEIsRUFBd0J0ZCxDQUF4QjtBQUEwQixZQUFNa1QsQ0FBTixLQUFVQSxDQUFDLEdBQUMsRUFBWjtBQUFnQmxULEtBQUMsR0FBQ2tULENBQUMsQ0FBQ3FLLFlBQUo7QUFBaUJKLEtBQUMsR0FBQ2pLLENBQUMsQ0FBQ3NLLGVBQUo7QUFBb0JGLEtBQUMsR0FBQ3BLLENBQUMsQ0FBQ3VLLFlBQUo7QUFBaUI1a0MsS0FBQyxHQUFDcTZCLENBQUMsQ0FBQ2hzQixNQUFKO0FBQVdtMkIsS0FBQyxHQUFDbkssQ0FBQyxDQUFDd0ssVUFBSjtBQUFldHFDLEtBQUMsR0FBQzgvQixDQUFDLENBQUN5SyxNQUFKO0FBQVdWLEtBQUMsR0FBQy9KLENBQUMsQ0FBQzBLLFNBQUo7QUFBYyxZQUFNUCxDQUFOLEtBQVVBLENBQUMsR0FBQyxDQUFaO0FBQWUsWUFBTXhrQyxDQUFOLEtBQVVBLENBQUMsR0FBQyxLQUFLLENBQWpCO0FBQW9CLFlBQU1za0MsQ0FBTixLQUFVQSxDQUFDLEdBQUMsQ0FBQyxDQUFiO0FBQWdCLFlBQU1uZCxDQUFOLEtBQVVBLENBQUMsR0FBQyxVQUFaO0FBQXdCZ2QsS0FBQyxHQUFDbm5CLENBQUMsQ0FBQy9oQixRQUFELENBQUg7QUFBYyxZQUFNbXBDLENBQU4sS0FBVUEsQ0FBQyxHQUFDLENBQUMsQ0FBYjs7QUFBZ0JDLEtBQUMsR0FBQyxXQUFTejBDLENBQVQsRUFBV3lxQyxDQUFYLEVBQWF0UixDQUFiLEVBQWVpYyxDQUFmLEVBQWlCQyxDQUFqQixFQUFtQi9iLENBQW5CLEVBQXFCRixDQUFyQixFQUF1QmtjLENBQXZCLEVBQXlCO0FBQUMsVUFBSUMsQ0FBSixFQUFNQyxFQUFOLEVBQVFoTCxDQUFSLEVBQVVpTCxDQUFWLEVBQVlDLENBQVosRUFBYzMxQyxDQUFkLEVBQWdCbUMsQ0FBaEIsRUFBa0JrTCxDQUFsQixFQUFvQkMsQ0FBcEIsRUFBc0Jzb0MsQ0FBdEIsRUFBd0JDLENBQXhCLEVBQTBCLzFDLENBQTFCOztBQUE0QixVQUFHLENBQUNHLENBQUMsQ0FBQ29DLElBQUYsQ0FBTyxZQUFQLENBQUosRUFBeUI7QUFBQ3BDLFNBQUMsQ0FBQ29DLElBQUYsQ0FBTyxZQUFQLEVBQW9CLENBQUMsQ0FBckI7QUFBd0JzekMsU0FBQyxHQUFDbkIsQ0FBQyxDQUFDN3RDLE1BQUYsRUFBRjtBQUFheEUsU0FBQyxHQUFDbEMsQ0FBQyxDQUFDeWUsTUFBRixFQUFGO0FBQWEsZ0JBQU1yTyxDQUFOLEtBQVVsTyxDQUFDLEdBQUNBLENBQUMsQ0FBQzBuQyxPQUFGLENBQVV4NUIsQ0FBVixDQUFaO0FBQzlkLFlBQUcsQ0FBQ2xPLENBQUMsQ0FBQ3BDLE1BQU4sRUFBYSxNQUFLLDZCQUFMO0FBQW1DeTFDLFNBQUMsR0FBQy9LLENBQUMsR0FBQyxDQUFDLENBQUw7QUFBTyxTQUFDb0wsQ0FBQyxHQUFDLFFBQU1qckMsQ0FBTixHQUFRQSxDQUFDLElBQUUzSyxDQUFDLENBQUM0cEMsT0FBRixDQUFVai9CLENBQVYsQ0FBWCxHQUF3QnlpQixDQUFDLENBQUMsU0FBRCxDQUE1QixLQUEwQ3dvQixDQUFDLENBQUNqeUMsR0FBRixDQUFNLFVBQU4sRUFBaUIzRCxDQUFDLENBQUMyRCxHQUFGLENBQU0sVUFBTixDQUFqQixDQUExQzs7QUFBOEV5SixTQUFDLEdBQUMsYUFBVTtBQUFDLGNBQUlyTixDQUFKLEVBQU11QyxDQUFOLEVBQVFiLENBQVI7QUFBVSxjQUFHLENBQUM2ekMsQ0FBRCxLQUFLSSxDQUFDLEdBQUNuQixDQUFDLENBQUM3dEMsTUFBRixFQUFGLEVBQWEzRyxDQUFDLEdBQUN1cUIsUUFBUSxDQUFDcG9CLENBQUMsQ0FBQ3lCLEdBQUYsQ0FBTSxrQkFBTixDQUFELEVBQTJCLEVBQTNCLENBQXZCLEVBQXNEckIsQ0FBQyxHQUFDZ29CLFFBQVEsQ0FBQ3BvQixDQUFDLENBQUN5QixHQUFGLENBQU0sYUFBTixDQUFELEVBQXNCLEVBQXRCLENBQWhFLEVBQTBGOG1DLENBQUMsR0FBQ25nQixRQUFRLENBQUNwb0IsQ0FBQyxDQUFDeUIsR0FBRixDQUFNLGdCQUFOLENBQUQsRUFBeUIsRUFBekIsQ0FBcEcsRUFBaUl3MUIsQ0FBQyxHQUFDajNCLENBQUMsQ0FBQ2twQixNQUFGLEdBQVdqbEIsR0FBWCxHQUFlcEcsQ0FBZixHQUFpQnVDLENBQXBKLEVBQXNKOHlDLENBQUMsR0FBQ2x6QyxDQUFDLENBQUN3RSxNQUFGLEVBQXhKLEVBQW1LOGpDLENBQUMsS0FBRytLLENBQUMsR0FBQy9LLENBQUMsR0FBQyxDQUFDLENBQUwsRUFBTyxRQUFNNy9CLENBQU4sS0FBVTNLLENBQUMsQ0FBQzYxQyxXQUFGLENBQWNELENBQWQsR0FBaUJBLENBQUMsQ0FBQ0UsTUFBRixFQUEzQixDQUFQLEVBQThDOTFDLENBQUMsQ0FBQzJELEdBQUYsQ0FBTTtBQUFDOEwsb0JBQVEsRUFBQyxFQUFWO0FBQWF0SixlQUFHLEVBQUMsRUFBakI7QUFBb0JNLGlCQUFLLEVBQUMsRUFBMUI7QUFBNkJMLGtCQUFNLEVBQUM7QUFBcEMsV0FBTixFQUErQzhHLFdBQS9DLENBQTJEcXFCLENBQTNELENBQTlDLEVBQTRHOTFCLENBQUMsR0FBQyxDQUFDLENBQWxILENBQXBLLEVBQXlSNHpDLENBQUMsR0FBQ3IxQyxDQUFDLENBQUNvckIsTUFBRixHQUFXamxCLEdBQVgsSUFBZ0Jta0IsUUFBUSxDQUFDdHFCLENBQUMsQ0FBQzJELEdBQUYsQ0FBTSxZQUFOLENBQUQsRUFBcUIsRUFBckIsQ0FBUixJQUFrQyxDQUFsRCxJQUFxRGl4QyxDQUFoVixFQUNwS3RiLENBQUMsR0FBQ3Q1QixDQUFDLENBQUNnbUIsV0FBRixDQUFjLENBQUMsQ0FBZixDQURrSyxFQUNoSm9ULENBQUMsR0FBQ3A1QixDQUFDLENBQUMyRCxHQUFGLENBQU0sT0FBTixDQUQ4SSxFQUMvSGl5QyxDQUFDLElBQUVBLENBQUMsQ0FBQ2p5QyxHQUFGLENBQU07QUFBQzhDLGlCQUFLLEVBQUN6RyxDQUFDLENBQUM4bEIsVUFBRixDQUFhLENBQUMsQ0FBZCxDQUFQO0FBQXdCcGYsa0JBQU0sRUFBQzR5QixDQUEvQjtBQUFpQ3hZLG1CQUFPLEVBQUM5Z0IsQ0FBQyxDQUFDMkQsR0FBRixDQUFNLFNBQU4sQ0FBekM7QUFBMEQsOEJBQWlCM0QsQ0FBQyxDQUFDMkQsR0FBRixDQUFNLGdCQUFOLENBQTNFO0FBQW1HLHFCQUFReTFCO0FBQTNHLFdBQU4sQ0FENEgsRUFDUDMzQixDQURFLENBQUgsRUFDSSxPQUFPNUIsQ0FBQyxFQUFSO0FBQVcsU0FEdEM7O0FBQ3VDdU4sU0FBQztBQUFHLFlBQUdrc0IsQ0FBQyxLQUFHOGIsQ0FBUCxFQUFTLE9BQU9LLENBQUMsR0FBQyxLQUFLLENBQVAsRUFBUzExQyxDQUFDLEdBQUM2MEMsQ0FBWCxFQUFhZSxDQUFDLEdBQUNkLENBQWYsRUFBaUJoMUMsQ0FBQyxHQUFDLGFBQVU7QUFBQyxjQUFJdXRCLENBQUosRUFBTXZ0QixDQUFOLEVBQVE0QixDQUFSLEVBQVUyTyxDQUFWO0FBQVksY0FBRyxDQUFDa2xDLENBQUQsS0FBSzd6QyxDQUFDLEdBQUMsQ0FBQyxDQUFILEVBQUssUUFBTWswQyxDQUFOLEtBQVUsRUFBRUEsQ0FBRixFQUFJLEtBQUdBLENBQUgsS0FBT0EsQ0FBQyxHQUFDZCxDQUFGLEVBQUl6bkMsQ0FBQyxFQUFMLEVBQVEzTCxDQUFDLEdBQUMsQ0FBQyxDQUFsQixDQUFkLENBQUwsRUFBeUNBLENBQUMsSUFBRTh5QyxDQUFDLENBQUM3dEMsTUFBRixPQUFhZ3ZDLENBQWhCLElBQW1CdG9DLENBQUMsRUFBN0QsRUFBZ0UzTCxDQUFDLEdBQUNhLENBQUMsQ0FBQ3l5QixTQUFGLEVBQWxFLEVBQWdGLFFBQU0wZ0IsQ0FBTixLQUFVNTFDLENBQUMsR0FBQzRCLENBQUMsR0FBQ2cwQyxDQUFkLENBQWhGLEVBQWlHQSxDQUFDLEdBQUNoMEMsQ0FBbkcsRUFBcUcrb0MsQ0FBQyxJQUFFZ0ssQ0FBQyxLQUFHcGtDLENBQUMsR0FBQzNPLENBQUMsR0FBQzYzQixDQUFGLEdBQUl2NUIsQ0FBSixHQUFNcTFDLENBQUMsR0FBQ2pjLENBQVYsRUFBWW9jLENBQUMsSUFBRSxDQUFDbmxDLENBQUosS0FBUW1sQyxDQUFDLEdBQUMsQ0FBQyxDQUFILEVBQUt2MUMsQ0FBQyxDQUFDMkQsR0FBRixDQUFNO0FBQUM4TCxvQkFBUSxFQUFDLE9BQVY7QUFBa0JySixrQkFBTSxFQUFDLEVBQXpCO0FBQTRCRCxlQUFHLEVBQUNwRztBQUFoQyxXQUFOLEVBQTBDb0YsT0FBMUMsQ0FBa0QscUJBQWxELENBQWIsQ0FBZixDQUFELEVBQXdHMUQsQ0FBQyxHQUFDNHpDLENBQUYsS0FBTTdLLENBQUMsR0FBQyxDQUFDLENBQUgsRUFBS3pxQyxDQUFDLEdBQUM2MEMsQ0FBUCxFQUFTLFFBQU1qcUMsQ0FBTixLQUFVLFdBQVN5dUIsQ0FBVCxJQUFZLFlBQVVBLENBQXRCLElBQXlCcDVCLENBQUMsQ0FBQzYxQyxXQUFGLENBQWNELENBQWQsQ0FBekIsRUFDM2RBLENBQUMsQ0FBQ0UsTUFBRixFQURpZCxDQUFULEVBQzViMW9CLENBQUMsR0FBQztBQUFDM2Qsb0JBQVEsRUFBQyxFQUFWO0FBQWFoSixpQkFBSyxFQUFDLEVBQW5CO0FBQXNCTixlQUFHLEVBQUM7QUFBMUIsV0FEMGIsRUFDNVpuRyxDQUFDLENBQUMyRCxHQUFGLENBQU15cEIsQ0FBTixFQUFTbGdCLFdBQVQsQ0FBcUJxcUIsQ0FBckIsRUFBd0JweUIsT0FBeEIsQ0FBZ0Msb0JBQWhDLENBRHNaLENBQXhHLEVBQ3ZQdXZDLENBQUMsS0FBR3RuQixDQUFDLEdBQUM5cUIsQ0FBQyxDQUFDb0UsTUFBRixFQUFGLEVBQWE0eUIsQ0FBQyxHQUFDc2IsQ0FBRixHQUFJeG5CLENBQUosSUFBTyxDQUFDbW9CLENBQVIsS0FBWXgxQyxDQUFDLElBQUVGLENBQUgsRUFBS0UsQ0FBQyxHQUFDeUosSUFBSSxDQUFDQyxHQUFMLENBQVMyakIsQ0FBQyxHQUFDa00sQ0FBWCxFQUFhdjVCLENBQWIsQ0FBUCxFQUF1QkEsQ0FBQyxHQUFDeUosSUFBSSxDQUFDZ1osR0FBTCxDQUFTb3lCLENBQVQsRUFBVzcwQyxDQUFYLENBQXpCLEVBQXVDeXFDLENBQUMsSUFBRXhxQyxDQUFDLENBQUMyRCxHQUFGLENBQU07QUFBQ3dDLGVBQUcsRUFBQ3BHLENBQUMsR0FBQztBQUFQLFdBQU4sQ0FBdEQsQ0FBaEIsQ0FEb1AsSUFDdkowQixDQUFDLEdBQUM0ekMsQ0FBRixLQUFNN0ssQ0FBQyxHQUFDLENBQUMsQ0FBSCxFQUFLcGQsQ0FBQyxHQUFDO0FBQUMzZCxvQkFBUSxFQUFDLE9BQVY7QUFBa0J0SixlQUFHLEVBQUNwRztBQUF0QixXQUFQLEVBQWdDcXRCLENBQUMsQ0FBQzNtQixLQUFGLEdBQVEsaUJBQWV6RyxDQUFDLENBQUMyRCxHQUFGLENBQU0sWUFBTixDQUFmLEdBQW1DM0QsQ0FBQyxDQUFDOGxCLFVBQUYsS0FBZSxJQUFsRCxHQUF1RDlsQixDQUFDLENBQUN5RyxLQUFGLEtBQVUsSUFBekcsRUFBOEd6RyxDQUFDLENBQUMyRCxHQUFGLENBQU15cEIsQ0FBTixFQUFTbmdCLFFBQVQsQ0FBa0JzcUIsQ0FBbEIsQ0FBOUcsRUFBbUksUUFBTTVzQixDQUFOLEtBQVUzSyxDQUFDLENBQUMrMUMsS0FBRixDQUFRSCxDQUFSLEdBQVcsV0FBU3hjLENBQVQsSUFBWSxZQUFVQSxDQUF0QixJQUF5QndjLENBQUMsQ0FBQ0ksTUFBRixDQUFTaDJDLENBQVQsQ0FBOUMsQ0FBbkksRUFBOExBLENBQUMsQ0FBQ21GLE9BQUYsQ0FBVSxrQkFBVixDQUFwTSxDQURpRCxFQUNrTHFsQyxDQUFDLElBQUVnSyxDQUFILEtBQU8sUUFBTXBrQyxDQUFOLEtBQVVBLENBQUMsR0FBQzNPLENBQUMsR0FBQzYzQixDQUFGLEdBQUl2NUIsQ0FBSixHQUFNcTFDLENBQUMsR0FBQ2pjLENBQXBCLEdBQXVCLENBQUNvYyxDQUFELElBQUlubEMsQ0FBbEMsQ0FEdkwsQ0FBSCxFQUNnTyxPQUFPbWxDLENBQUMsR0FBQyxDQUFDLENBQUgsRUFBSyxhQUFXcnpDLENBQUMsQ0FBQ3lCLEdBQUYsQ0FBTSxVQUFOLENBQVgsSUFBOEJ6QixDQUFDLENBQUN5QixHQUFGLENBQU07QUFBQzhMLG9CQUFRLEVBQUM7QUFBVixXQUFOLENBQW5DLEVBQ2pkelAsQ0FBQyxDQUFDMkQsR0FBRixDQUFNO0FBQUM4TCxvQkFBUSxFQUFDLFVBQVY7QUFBcUJySixrQkFBTSxFQUFDcWtDLENBQTVCO0FBQThCdGtDLGVBQUcsRUFBQztBQUFsQyxXQUFOLEVBQWlEaEIsT0FBakQsQ0FBeUQsbUJBQXpELENBRDBjO0FBQzVYLFNBRmtILEVBRWpIa0ksQ0FBQyxHQUFDLGFBQVU7QUFBQ0QsV0FBQztBQUFHLGlCQUFPdk4sQ0FBQyxFQUFSO0FBQVcsU0FGcUYsRUFFcEYyMUMsRUFBQyxHQUFDLGFBQVU7QUFBQ0YsV0FBQyxHQUFDLENBQUMsQ0FBSDtBQUFLaHpDLFdBQUMsQ0FBQzBDLEdBQUYsQ0FBTSxXQUFOLEVBQWtCbkYsQ0FBbEI7QUFBcUJ5QyxXQUFDLENBQUMwQyxHQUFGLENBQU0sUUFBTixFQUFlbkYsQ0FBZjtBQUFrQnlDLFdBQUMsQ0FBQzBDLEdBQUYsQ0FBTSxRQUFOLEVBQWVxSSxDQUFmO0FBQWtCK2YsV0FBQyxDQUFDL2hCLFFBQVEsQ0FBQ3NPLElBQVYsQ0FBRCxDQUFpQjNVLEdBQWpCLENBQXFCLG1CQUFyQixFQUF5Q3FJLENBQXpDO0FBQTRDck4sV0FBQyxDQUFDZ0YsR0FBRixDQUFNLG1CQUFOLEVBQTBCd3dDLEVBQTFCO0FBQTZCeDFDLFdBQUMsQ0FBQytwQyxVQUFGLENBQWEsWUFBYjtBQUEyQi9wQyxXQUFDLENBQUMyRCxHQUFGLENBQU07QUFBQzhMLG9CQUFRLEVBQUMsRUFBVjtBQUFhckosa0JBQU0sRUFBQyxFQUFwQjtBQUF1QkQsZUFBRyxFQUFDLEVBQTNCO0FBQThCTSxpQkFBSyxFQUFDO0FBQXBDLFdBQU47QUFBK0N2RSxXQUFDLENBQUN1TixRQUFGLENBQVcsVUFBWCxFQUFzQixFQUF0QjtBQUEwQixjQUFHKzZCLENBQUgsRUFBSyxPQUFPLFFBQU03L0IsQ0FBTixLQUFVLFdBQVN5dUIsQ0FBVCxJQUFZLFlBQVVBLENBQXRCLElBQXlCcDVCLENBQUMsQ0FBQzYxQyxXQUFGLENBQWNELENBQWQsQ0FBekIsRUFBMENBLENBQUMsQ0FBQ253QyxNQUFGLEVBQXBELEdBQWdFekYsQ0FBQyxDQUFDa04sV0FBRixDQUFjcXFCLENBQWQsQ0FBdkU7QUFBd0YsU0FGalEsRUFFa1FqMUIsQ0FBQyxDQUFDSSxFQUFGLENBQUssV0FBTCxFQUFpQjdDLENBQWpCLENBRmxRLEVBRXNSeUMsQ0FBQyxDQUFDSSxFQUFGLENBQUssUUFBTCxFQUFjN0MsQ0FBZCxDQUZ0UixFQUV1U3lDLENBQUMsQ0FBQ0ksRUFBRixDQUFLLFFBQUwsRUFDdmUySyxDQUR1ZSxDQUZ2UyxFQUc3TCtmLENBQUMsQ0FBQy9oQixRQUFRLENBQUNzTyxJQUFWLENBQUQsQ0FBaUJqWCxFQUFqQixDQUFvQixtQkFBcEIsRUFBd0MySyxDQUF4QyxDQUg2TCxFQUdsSnJOLENBQUMsQ0FBQzBDLEVBQUYsQ0FBSyxtQkFBTCxFQUF5Qjh5QyxFQUF6QixDQUhrSixFQUd0SGwwQyxVQUFVLENBQUN6QixDQUFELEVBQUcsQ0FBSCxDQUhxRztBQUcvRjtBQUFDLEtBTCtQOztBQUs5UHM1QixLQUFDLEdBQUMsQ0FBRjs7QUFBSSxTQUFJd2IsQ0FBQyxHQUFDLEtBQUs3MEMsTUFBWCxFQUFrQnE1QixDQUFDLEdBQUN3YixDQUFwQixFQUFzQnhiLENBQUMsRUFBdkI7QUFBMEJzUixPQUFDLEdBQUMsS0FBS3RSLENBQUwsQ0FBRixFQUFVc2IsQ0FBQyxDQUFDcm5CLENBQUMsQ0FBQ3FkLENBQUQsQ0FBRixDQUFYO0FBQTFCOztBQUE0QyxXQUFPLElBQVA7QUFBWSxHQUwzRjtBQUs0RixDQUx6SixFQUsySmpqQyxJQUwzSixDQUtnSyxJQUxoSyxFIiwiZmlsZSI6IjIuanMiLCJzb3VyY2VzQ29udGVudCI6WyI7XG4vKiFcbiAqIGpRdWVyeSAmIFplcHRvIExhenkgLSB2MS43LjdcbiAqIGh0dHA6Ly9qcXVlcnkuZWlzYmVoci5kZS9sYXp5L1xuICpcbiAqIENvcHlyaWdodCAyMDEyIC0gMjAxNywgRGFuaWVsICdFaXNiZWhyJyBLZXJuXG4gKlxuICogRHVhbCBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGFuZCBHUEwtMi4wIGxpY2Vuc2VzOlxuICogaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAqIGh0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxcbiAqXG4gKiAkKFwiaW1nLmxhenlcIikubGF6eSgpO1xuICovXG4oZnVuY3Rpb24od2luZG93LHVuZGVmaW5lZCl7XCJ1c2Ugc3RyaWN0XCI7dmFyICQ9d2luZG93LmpRdWVyeXx8d2luZG93LlplcHRvLGxhenlJbnN0YW5jZUlkPTAsd2luZG93TG9hZGVkPWZhbHNlOyQuZm4uTGF6eT0kLmZuLmxhenk9ZnVuY3Rpb24oc2V0dGluZ3Mpe3JldHVybiBuZXcgTGF6eVBsdWdpbih0aGlzLHNldHRpbmdzKTt9OyQuTGF6eT0kLmxhenk9ZnVuY3Rpb24obmFtZXMsZWxlbWVudHMsbG9hZGVyKXtpZigkLmlzRnVuY3Rpb24oZWxlbWVudHMpKXtsb2FkZXI9ZWxlbWVudHM7ZWxlbWVudHM9W107fVxuaWYoISQuaXNGdW5jdGlvbihsb2FkZXIpKXtyZXR1cm47fVxubmFtZXM9JC5pc0FycmF5KG5hbWVzKT9uYW1lczpbbmFtZXNdO2VsZW1lbnRzPSQuaXNBcnJheShlbGVtZW50cyk/ZWxlbWVudHM6W2VsZW1lbnRzXTt2YXIgY29uZmlnPUxhenlQbHVnaW4ucHJvdG90eXBlLmNvbmZpZyxmb3JjZWQ9Y29uZmlnLl9mfHwoY29uZmlnLl9mPXt9KTtmb3IodmFyIGk9MCxsPW5hbWVzLmxlbmd0aDtpPGw7aSsrKXtpZihjb25maWdbbmFtZXNbaV1dPT09dW5kZWZpbmVkfHwkLmlzRnVuY3Rpb24oY29uZmlnW25hbWVzW2ldXSkpe2NvbmZpZ1tuYW1lc1tpXV09bG9hZGVyO319XG5mb3IodmFyIGM9MCxhPWVsZW1lbnRzLmxlbmd0aDtjPGE7YysrKXtmb3JjZWRbZWxlbWVudHNbY11dPW5hbWVzWzBdO319O2Z1bmN0aW9uIF9leGVjdXRlTGF6eShpbnN0YW5jZSxjb25maWcsaXRlbXMsZXZlbnRzLG5hbWVzcGFjZSl7dmFyIF9hd2FpdGluZ0FmdGVyTG9hZD0wLF9hY3R1YWxXaWR0aD0tMSxfYWN0dWFsSGVpZ2h0PS0xLF9pc1JldGluYURpc3BsYXk9ZmFsc2UsX2FmdGVyTG9hZD1cImFmdGVyTG9hZFwiLF9sb2FkPVwibG9hZFwiLF9lcnJvcj1cImVycm9yXCIsX2ltZz1cImltZ1wiLF9zcmM9XCJzcmNcIixfc3Jjc2V0PVwic3Jjc2V0XCIsX3NpemVzPVwic2l6ZXNcIixfYmFja2dyb3VuZEltYWdlPVwiYmFja2dyb3VuZC1pbWFnZVwiO2Z1bmN0aW9uIF9pbml0aWFsaXplKCl7X2lzUmV0aW5hRGlzcGxheT13aW5kb3cuZGV2aWNlUGl4ZWxSYXRpbz4xO2l0ZW1zPV9wcmVwYXJlSXRlbXMoaXRlbXMpO2lmKGNvbmZpZy5kZWxheT49MCl7c2V0VGltZW91dChmdW5jdGlvbigpe19sYXp5TG9hZEl0ZW1zKHRydWUpO30sY29uZmlnLmRlbGF5KTt9XG5pZihjb25maWcuZGVsYXk8MHx8Y29uZmlnLmNvbWJpbmVkKXtldmVudHMuZT1fdGhyb3R0bGUoY29uZmlnLnRocm90dGxlLGZ1bmN0aW9uKGV2ZW50KXtpZihldmVudC50eXBlPT09XCJyZXNpemVcIil7X2FjdHVhbFdpZHRoPV9hY3R1YWxIZWlnaHQ9LTE7fVxuX2xhenlMb2FkSXRlbXMoZXZlbnQuYWxsKTt9KTtldmVudHMuYT1mdW5jdGlvbihhZGRpdGlvbmFsSXRlbXMpe2FkZGl0aW9uYWxJdGVtcz1fcHJlcGFyZUl0ZW1zKGFkZGl0aW9uYWxJdGVtcyk7aXRlbXMucHVzaC5hcHBseShpdGVtcyxhZGRpdGlvbmFsSXRlbXMpO307ZXZlbnRzLmc9ZnVuY3Rpb24oKXtyZXR1cm4oaXRlbXM9JChpdGVtcykuZmlsdGVyKGZ1bmN0aW9uKCl7cmV0dXJuISQodGhpcykuZGF0YShjb25maWcubG9hZGVkTmFtZSk7fSkpO307ZXZlbnRzLmY9ZnVuY3Rpb24oZm9yY2VkSXRlbXMpe2Zvcih2YXIgaT0wO2k8Zm9yY2VkSXRlbXMubGVuZ3RoO2krKyl7dmFyIGl0ZW09aXRlbXMuZmlsdGVyKGZ1bmN0aW9uKCl7cmV0dXJuIHRoaXM9PT1mb3JjZWRJdGVtc1tpXTt9KTtpZihpdGVtLmxlbmd0aCl7X2xhenlMb2FkSXRlbXMoZmFsc2UsaXRlbSk7fX19O19sYXp5TG9hZEl0ZW1zKCk7JChjb25maWcuYXBwZW5kU2Nyb2xsKS5vbihcInNjcm9sbC5cIituYW1lc3BhY2UrXCIgcmVzaXplLlwiK25hbWVzcGFjZSxldmVudHMuZSk7fX1cbmZ1bmN0aW9uIF9wcmVwYXJlSXRlbXMoaXRlbXMpe3ZhciBkZWZhdWx0SW1hZ2U9Y29uZmlnLmRlZmF1bHRJbWFnZSxwbGFjZWhvbGRlcj1jb25maWcucGxhY2Vob2xkZXIsaW1hZ2VCYXNlPWNvbmZpZy5pbWFnZUJhc2Usc3Jjc2V0QXR0cmlidXRlPWNvbmZpZy5zcmNzZXRBdHRyaWJ1dGUsbG9hZGVyQXR0cmlidXRlPWNvbmZpZy5sb2FkZXJBdHRyaWJ1dGUsZm9yY2VkVGFncz1jb25maWcuX2Z8fHt9O2l0ZW1zPSQoaXRlbXMpLmZpbHRlcihmdW5jdGlvbigpe3ZhciBlbGVtZW50PSQodGhpcyksdGFnPV9nZXRFbGVtZW50VGFnTmFtZSh0aGlzKTtyZXR1cm4hZWxlbWVudC5kYXRhKGNvbmZpZy5oYW5kbGVkTmFtZSkmJihlbGVtZW50LmF0dHIoY29uZmlnLmF0dHJpYnV0ZSl8fGVsZW1lbnQuYXR0cihzcmNzZXRBdHRyaWJ1dGUpfHxlbGVtZW50LmF0dHIobG9hZGVyQXR0cmlidXRlKXx8Zm9yY2VkVGFnc1t0YWddIT09dW5kZWZpbmVkKTt9KS5kYXRhKFwicGx1Z2luX1wiK2NvbmZpZy5uYW1lLGluc3RhbmNlKTtmb3IodmFyIGk9MCxsPWl0ZW1zLmxlbmd0aDtpPGw7aSsrKXt2YXIgZWxlbWVudD0kKGl0ZW1zW2ldKSx0YWc9X2dldEVsZW1lbnRUYWdOYW1lKGl0ZW1zW2ldKSxlbGVtZW50SW1hZ2VCYXNlPWVsZW1lbnQuYXR0cihjb25maWcuaW1hZ2VCYXNlQXR0cmlidXRlKXx8aW1hZ2VCYXNlO2lmKHRhZz09PV9pbWcmJmVsZW1lbnRJbWFnZUJhc2UmJmVsZW1lbnQuYXR0cihzcmNzZXRBdHRyaWJ1dGUpKXtlbGVtZW50LmF0dHIoc3Jjc2V0QXR0cmlidXRlLF9nZXRDb3JyZWN0ZWRTcmNTZXQoZWxlbWVudC5hdHRyKHNyY3NldEF0dHJpYnV0ZSksZWxlbWVudEltYWdlQmFzZSkpO31cbmlmKGZvcmNlZFRhZ3NbdGFnXSE9PXVuZGVmaW5lZCYmIWVsZW1lbnQuYXR0cihsb2FkZXJBdHRyaWJ1dGUpKXtlbGVtZW50LmF0dHIobG9hZGVyQXR0cmlidXRlLGZvcmNlZFRhZ3NbdGFnXSk7fVxuaWYodGFnPT09X2ltZyYmZGVmYXVsdEltYWdlJiYhZWxlbWVudC5hdHRyKF9zcmMpKXtlbGVtZW50LmF0dHIoX3NyYyxkZWZhdWx0SW1hZ2UpO31cbmVsc2UgaWYodGFnIT09X2ltZyYmcGxhY2Vob2xkZXImJighZWxlbWVudC5jc3MoX2JhY2tncm91bmRJbWFnZSl8fGVsZW1lbnQuY3NzKF9iYWNrZ3JvdW5kSW1hZ2UpPT09XCJub25lXCIpKXtlbGVtZW50LmNzcyhfYmFja2dyb3VuZEltYWdlLFwidXJsKCdcIitwbGFjZWhvbGRlcitcIicpXCIpO319XG5yZXR1cm4gaXRlbXM7fVxuZnVuY3Rpb24gX2xhenlMb2FkSXRlbXMoYWxsSXRlbXMsZm9yY2VkKXtpZighaXRlbXMubGVuZ3RoKXtpZihjb25maWcuYXV0b0Rlc3Ryb3kpe2luc3RhbmNlLmRlc3Ryb3koKTt9XG5yZXR1cm47fVxudmFyIGVsZW1lbnRzPWZvcmNlZHx8aXRlbXMsbG9hZFRyaWdnZXJlZD1mYWxzZSxpbWFnZUJhc2U9Y29uZmlnLmltYWdlQmFzZXx8XCJcIixzcmNzZXRBdHRyaWJ1dGU9Y29uZmlnLnNyY3NldEF0dHJpYnV0ZSxoYW5kbGVkTmFtZT1jb25maWcuaGFuZGxlZE5hbWU7Zm9yKHZhciBpPTA7aTxlbGVtZW50cy5sZW5ndGg7aSsrKXtpZihhbGxJdGVtc3x8Zm9yY2VkfHxfaXNJbkxvYWRhYmxlQXJlYShlbGVtZW50c1tpXSkpe3ZhciBlbGVtZW50PSQoZWxlbWVudHNbaV0pLHRhZz1fZ2V0RWxlbWVudFRhZ05hbWUoZWxlbWVudHNbaV0pLGF0dHJpYnV0ZT1lbGVtZW50LmF0dHIoY29uZmlnLmF0dHJpYnV0ZSksZWxlbWVudEltYWdlQmFzZT1lbGVtZW50LmF0dHIoY29uZmlnLmltYWdlQmFzZUF0dHJpYnV0ZSl8fGltYWdlQmFzZSxjdXN0b21Mb2FkZXI9ZWxlbWVudC5hdHRyKGNvbmZpZy5sb2FkZXJBdHRyaWJ1dGUpO2lmKCFlbGVtZW50LmRhdGEoaGFuZGxlZE5hbWUpJiYoIWNvbmZpZy52aXNpYmxlT25seXx8ZWxlbWVudC5pcyhcIjp2aXNpYmxlXCIpKSYmKChhdHRyaWJ1dGV8fGVsZW1lbnQuYXR0cihzcmNzZXRBdHRyaWJ1dGUpKSYmKCh0YWc9PT1faW1nJiYoZWxlbWVudEltYWdlQmFzZSthdHRyaWJ1dGUhPT1lbGVtZW50LmF0dHIoX3NyYyl8fGVsZW1lbnQuYXR0cihzcmNzZXRBdHRyaWJ1dGUpIT09ZWxlbWVudC5hdHRyKF9zcmNzZXQpKSl8fCh0YWchPT1faW1nJiZlbGVtZW50SW1hZ2VCYXNlK2F0dHJpYnV0ZSE9PWVsZW1lbnQuY3NzKF9iYWNrZ3JvdW5kSW1hZ2UpKSl8fGN1c3RvbUxvYWRlcikpXG57bG9hZFRyaWdnZXJlZD10cnVlO2VsZW1lbnQuZGF0YShoYW5kbGVkTmFtZSx0cnVlKTtfaGFuZGxlSXRlbShlbGVtZW50LHRhZyxlbGVtZW50SW1hZ2VCYXNlLGN1c3RvbUxvYWRlcik7fX19XG5pZihsb2FkVHJpZ2dlcmVkKXtpdGVtcz0kKGl0ZW1zKS5maWx0ZXIoZnVuY3Rpb24oKXtyZXR1cm4hJCh0aGlzKS5kYXRhKGhhbmRsZWROYW1lKTt9KTt9fVxuZnVuY3Rpb24gX2hhbmRsZUl0ZW0oZWxlbWVudCx0YWcsaW1hZ2VCYXNlLGN1c3RvbUxvYWRlcil7KytfYXdhaXRpbmdBZnRlckxvYWQ7dmFyIGVycm9yQ2FsbGJhY2s9ZnVuY3Rpb24oKXtfdHJpZ2dlckNhbGxiYWNrKFwib25FcnJvclwiLGVsZW1lbnQpO19yZWR1Y2VBd2FpdGluZygpO2Vycm9yQ2FsbGJhY2s9JC5ub29wO307X3RyaWdnZXJDYWxsYmFjayhcImJlZm9yZUxvYWRcIixlbGVtZW50KTt2YXIgc3JjQXR0cmlidXRlPWNvbmZpZy5hdHRyaWJ1dGUsc3Jjc2V0QXR0cmlidXRlPWNvbmZpZy5zcmNzZXRBdHRyaWJ1dGUsc2l6ZXNBdHRyaWJ1dGU9Y29uZmlnLnNpemVzQXR0cmlidXRlLHJldGluYUF0dHJpYnV0ZT1jb25maWcucmV0aW5hQXR0cmlidXRlLHJlbW92ZUF0dHJpYnV0ZT1jb25maWcucmVtb3ZlQXR0cmlidXRlLGxvYWRlZE5hbWU9Y29uZmlnLmxvYWRlZE5hbWUsZWxlbWVudFJldGluYT1lbGVtZW50LmF0dHIocmV0aW5hQXR0cmlidXRlKTtpZihjdXN0b21Mb2FkZXIpe3ZhciBsb2FkQ2FsbGJhY2s9ZnVuY3Rpb24oKXtpZihyZW1vdmVBdHRyaWJ1dGUpe2VsZW1lbnQucmVtb3ZlQXR0cihjb25maWcubG9hZGVyQXR0cmlidXRlKTt9XG5lbGVtZW50LmRhdGEobG9hZGVkTmFtZSx0cnVlKTtfdHJpZ2dlckNhbGxiYWNrKF9hZnRlckxvYWQsZWxlbWVudCk7c2V0VGltZW91dChfcmVkdWNlQXdhaXRpbmcsMSk7bG9hZENhbGxiYWNrPSQubm9vcDt9O2VsZW1lbnQub2ZmKF9lcnJvcikub25lKF9lcnJvcixlcnJvckNhbGxiYWNrKS5vbmUoX2xvYWQsbG9hZENhbGxiYWNrKTtpZighX3RyaWdnZXJDYWxsYmFjayhjdXN0b21Mb2FkZXIsZWxlbWVudCxmdW5jdGlvbihyZXNwb25zZSl7aWYocmVzcG9uc2Upe2VsZW1lbnQub2ZmKF9sb2FkKTtsb2FkQ2FsbGJhY2soKTt9XG5lbHNle2VsZW1lbnQub2ZmKF9lcnJvcik7ZXJyb3JDYWxsYmFjaygpO319KSllbGVtZW50LnRyaWdnZXIoX2Vycm9yKTt9XG5lbHNle3ZhciBpbWFnZU9iaj0kKG5ldyBJbWFnZSgpKTtpbWFnZU9iai5vbmUoX2Vycm9yLGVycm9yQ2FsbGJhY2spLm9uZShfbG9hZCxmdW5jdGlvbigpe2VsZW1lbnQuaGlkZSgpO2lmKHRhZz09PV9pbWcpe2VsZW1lbnQuYXR0cihfc2l6ZXMsaW1hZ2VPYmouYXR0cihfc2l6ZXMpKS5hdHRyKF9zcmNzZXQsaW1hZ2VPYmouYXR0cihfc3Jjc2V0KSkuYXR0cihfc3JjLGltYWdlT2JqLmF0dHIoX3NyYykpO31cbmVsc2V7ZWxlbWVudC5jc3MoX2JhY2tncm91bmRJbWFnZSxcInVybCgnXCIraW1hZ2VPYmouYXR0cihfc3JjKStcIicpXCIpO31cbmVsZW1lbnRbY29uZmlnLmVmZmVjdF0oY29uZmlnLmVmZmVjdFRpbWUpO2lmKHJlbW92ZUF0dHJpYnV0ZSl7ZWxlbWVudC5yZW1vdmVBdHRyKHNyY0F0dHJpYnV0ZStcIiBcIitzcmNzZXRBdHRyaWJ1dGUrXCIgXCIrcmV0aW5hQXR0cmlidXRlK1wiIFwiK2NvbmZpZy5pbWFnZUJhc2VBdHRyaWJ1dGUpO2lmKHNpemVzQXR0cmlidXRlIT09X3NpemVzKXtlbGVtZW50LnJlbW92ZUF0dHIoc2l6ZXNBdHRyaWJ1dGUpO319XG5lbGVtZW50LmRhdGEobG9hZGVkTmFtZSx0cnVlKTtfdHJpZ2dlckNhbGxiYWNrKF9hZnRlckxvYWQsZWxlbWVudCk7aW1hZ2VPYmoucmVtb3ZlKCk7X3JlZHVjZUF3YWl0aW5nKCk7fSk7dmFyIGltYWdlU3JjPShfaXNSZXRpbmFEaXNwbGF5JiZlbGVtZW50UmV0aW5hP2VsZW1lbnRSZXRpbmE6ZWxlbWVudC5hdHRyKHNyY0F0dHJpYnV0ZSkpfHxcIlwiO2ltYWdlT2JqLmF0dHIoX3NpemVzLGVsZW1lbnQuYXR0cihzaXplc0F0dHJpYnV0ZSkpLmF0dHIoX3NyY3NldCxlbGVtZW50LmF0dHIoc3Jjc2V0QXR0cmlidXRlKSkuYXR0cihfc3JjLGltYWdlU3JjP2ltYWdlQmFzZStpbWFnZVNyYzpudWxsKTtpbWFnZU9iai5jb21wbGV0ZSYmaW1hZ2VPYmoudHJpZ2dlcihfbG9hZCk7fX1cbmZ1bmN0aW9uIF9pc0luTG9hZGFibGVBcmVhKGVsZW1lbnQpe3ZhciBlbGVtZW50Qm91bmQ9ZWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSxkaXJlY3Rpb249Y29uZmlnLnNjcm9sbERpcmVjdGlvbix0aHJlc2hvbGQ9Y29uZmlnLnRocmVzaG9sZCx2ZXJ0aWNhbD0oKF9nZXRBY3R1YWxIZWlnaHQoKSt0aHJlc2hvbGQpPmVsZW1lbnRCb3VuZC50b3ApJiYoLXRocmVzaG9sZDxlbGVtZW50Qm91bmQuYm90dG9tKSxob3Jpem9udGFsPSgoX2dldEFjdHVhbFdpZHRoKCkrdGhyZXNob2xkKT5lbGVtZW50Qm91bmQubGVmdCkmJigtdGhyZXNob2xkPGVsZW1lbnRCb3VuZC5yaWdodCk7aWYoZGlyZWN0aW9uPT09XCJ2ZXJ0aWNhbFwiKXtyZXR1cm4gdmVydGljYWw7fVxuZWxzZSBpZihkaXJlY3Rpb249PT1cImhvcml6b250YWxcIil7cmV0dXJuIGhvcml6b250YWw7fVxucmV0dXJuIHZlcnRpY2FsJiZob3Jpem9udGFsO31cbmZ1bmN0aW9uIF9nZXRBY3R1YWxXaWR0aCgpe3JldHVybiBfYWN0dWFsV2lkdGg+PTA/X2FjdHVhbFdpZHRoOihfYWN0dWFsV2lkdGg9JCh3aW5kb3cpLndpZHRoKCkpO31cbmZ1bmN0aW9uIF9nZXRBY3R1YWxIZWlnaHQoKXtyZXR1cm4gX2FjdHVhbEhlaWdodD49MD9fYWN0dWFsSGVpZ2h0OihfYWN0dWFsSGVpZ2h0PSQod2luZG93KS5oZWlnaHQoKSk7fVxuZnVuY3Rpb24gX2dldEVsZW1lbnRUYWdOYW1lKGVsZW1lbnQpe3JldHVybiBlbGVtZW50LnRhZ05hbWUudG9Mb3dlckNhc2UoKTt9XG5mdW5jdGlvbiBfZ2V0Q29ycmVjdGVkU3JjU2V0KHNyY3NldCxpbWFnZUJhc2Upe2lmKGltYWdlQmFzZSl7dmFyIGVudHJpZXM9c3Jjc2V0LnNwbGl0KFwiLFwiKTtzcmNzZXQ9XCJcIjtmb3IodmFyIGk9MCxsPWVudHJpZXMubGVuZ3RoO2k8bDtpKyspe3NyY3NldCs9aW1hZ2VCYXNlK2VudHJpZXNbaV0udHJpbSgpKyhpIT09bC0xP1wiLFwiOlwiXCIpO319XG5yZXR1cm4gc3Jjc2V0O31cbmZ1bmN0aW9uIF90aHJvdHRsZShkZWxheSxjYWxsYmFjayl7dmFyIHRpbWVvdXQsbGFzdEV4ZWN1dGU9MDtyZXR1cm4gZnVuY3Rpb24oZXZlbnQsaWdub3JlVGhyb3R0bGUpe3ZhciBlbGFwc2VkPStuZXcgRGF0ZSgpLWxhc3RFeGVjdXRlO2Z1bmN0aW9uIHJ1bigpe2xhc3RFeGVjdXRlPStuZXcgRGF0ZSgpO2NhbGxiYWNrLmNhbGwoaW5zdGFuY2UsZXZlbnQpO31cbnRpbWVvdXQmJmNsZWFyVGltZW91dCh0aW1lb3V0KTtpZihlbGFwc2VkPmRlbGF5fHwhY29uZmlnLmVuYWJsZVRocm90dGxlfHxpZ25vcmVUaHJvdHRsZSl7cnVuKCk7fVxuZWxzZXt0aW1lb3V0PXNldFRpbWVvdXQocnVuLGRlbGF5LWVsYXBzZWQpO319O31cbmZ1bmN0aW9uIF9yZWR1Y2VBd2FpdGluZygpey0tX2F3YWl0aW5nQWZ0ZXJMb2FkO2lmKCFpdGVtcy5sZW5ndGgmJiFfYXdhaXRpbmdBZnRlckxvYWQpe190cmlnZ2VyQ2FsbGJhY2soXCJvbkZpbmlzaGVkQWxsXCIpO319XG5mdW5jdGlvbiBfdHJpZ2dlckNhbGxiYWNrKGNhbGxiYWNrLGVsZW1lbnQsYXJncyl7aWYoKGNhbGxiYWNrPWNvbmZpZ1tjYWxsYmFja10pKXtjYWxsYmFjay5hcHBseShpbnN0YW5jZSxbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywxKSk7cmV0dXJuIHRydWU7fVxucmV0dXJuIGZhbHNlO31cbmlmKGNvbmZpZy5iaW5kPT09XCJldmVudFwifHx3aW5kb3dMb2FkZWQpe19pbml0aWFsaXplKCk7fVxuZWxzZXskKHdpbmRvdykub24oX2xvYWQrXCIuXCIrbmFtZXNwYWNlLF9pbml0aWFsaXplKTt9fVxuZnVuY3Rpb24gTGF6eVBsdWdpbihlbGVtZW50cyxzZXR0aW5ncyl7dmFyIF9pbnN0YW5jZT10aGlzLF9jb25maWc9JC5leHRlbmQoe30sX2luc3RhbmNlLmNvbmZpZyxzZXR0aW5ncyksX2V2ZW50cz17fSxfbmFtZXNwYWNlPV9jb25maWcubmFtZStcIi1cIisoKytsYXp5SW5zdGFuY2VJZCk7X2luc3RhbmNlLmNvbmZpZz1mdW5jdGlvbihlbnRyeU5hbWUsdmFsdWUpe2lmKHZhbHVlPT09dW5kZWZpbmVkKXtyZXR1cm4gX2NvbmZpZ1tlbnRyeU5hbWVdO31cbl9jb25maWdbZW50cnlOYW1lXT12YWx1ZTtyZXR1cm4gX2luc3RhbmNlO307X2luc3RhbmNlLmFkZEl0ZW1zPWZ1bmN0aW9uKGl0ZW1zKXtfZXZlbnRzLmEmJl9ldmVudHMuYSgkLnR5cGUoaXRlbXMpPT09XCJzdHJpbmdcIj8kKGl0ZW1zKTppdGVtcyk7cmV0dXJuIF9pbnN0YW5jZTt9O19pbnN0YW5jZS5nZXRJdGVtcz1mdW5jdGlvbigpe3JldHVybiBfZXZlbnRzLmc/X2V2ZW50cy5nKCk6e307fTtfaW5zdGFuY2UudXBkYXRlPWZ1bmN0aW9uKHVzZVRocm90dGxlKXtfZXZlbnRzLmUmJl9ldmVudHMuZSh7fSwhdXNlVGhyb3R0bGUpO3JldHVybiBfaW5zdGFuY2U7fTtfaW5zdGFuY2UuZm9yY2U9ZnVuY3Rpb24oaXRlbXMpe19ldmVudHMuZiYmX2V2ZW50cy5mKCQudHlwZShpdGVtcyk9PT1cInN0cmluZ1wiPyQoaXRlbXMpOml0ZW1zKTtyZXR1cm4gX2luc3RhbmNlO307X2luc3RhbmNlLmxvYWRBbGw9ZnVuY3Rpb24oKXtfZXZlbnRzLmUmJl9ldmVudHMuZSh7YWxsOnRydWV9LHRydWUpO3JldHVybiBfaW5zdGFuY2U7fTtfaW5zdGFuY2UuZGVzdHJveT1mdW5jdGlvbigpeyQoX2NvbmZpZy5hcHBlbmRTY3JvbGwpLm9mZihcIi5cIitfbmFtZXNwYWNlLF9ldmVudHMuZSk7JCh3aW5kb3cpLm9mZihcIi5cIitfbmFtZXNwYWNlKTtfZXZlbnRzPXt9O3JldHVybiB1bmRlZmluZWQ7fTtfZXhlY3V0ZUxhenkoX2luc3RhbmNlLF9jb25maWcsZWxlbWVudHMsX2V2ZW50cyxfbmFtZXNwYWNlKTtyZXR1cm4gX2NvbmZpZy5jaGFpbmFibGU/ZWxlbWVudHM6X2luc3RhbmNlO31cbkxhenlQbHVnaW4ucHJvdG90eXBlLmNvbmZpZz17bmFtZTpcImxhenlcIixjaGFpbmFibGU6dHJ1ZSxhdXRvRGVzdHJveTp0cnVlLGJpbmQ6XCJsb2FkXCIsdGhyZXNob2xkOjUwMCx2aXNpYmxlT25seTpmYWxzZSxhcHBlbmRTY3JvbGw6d2luZG93LHNjcm9sbERpcmVjdGlvbjpcImJvdGhcIixpbWFnZUJhc2U6bnVsbCxkZWZhdWx0SW1hZ2U6XCJkYXRhOmltYWdlL2dpZjtiYXNlNjQsUjBsR09EbGhBUUFCQUlBQUFQLy8vd0FBQUNINUJBRUFBQUFBTEFBQUFBQUJBQUVBQUFJQ1JBRUFPdz09XCIscGxhY2Vob2xkZXI6bnVsbCxkZWxheTotMSxjb21iaW5lZDpmYWxzZSxhdHRyaWJ1dGU6XCJkYXRhLXNyY1wiLHNyY3NldEF0dHJpYnV0ZTpcImRhdGEtc3Jjc2V0XCIsc2l6ZXNBdHRyaWJ1dGU6XCJkYXRhLXNpemVzXCIscmV0aW5hQXR0cmlidXRlOlwiZGF0YS1yZXRpbmFcIixsb2FkZXJBdHRyaWJ1dGU6XCJkYXRhLWxvYWRlclwiLGltYWdlQmFzZUF0dHJpYnV0ZTpcImRhdGEtaW1hZ2ViYXNlXCIscmVtb3ZlQXR0cmlidXRlOnRydWUsaGFuZGxlZE5hbWU6XCJoYW5kbGVkXCIsbG9hZGVkTmFtZTpcImxvYWRlZFwiLGVmZmVjdDpcInNob3dcIixlZmZlY3RUaW1lOjAsZW5hYmxlVGhyb3R0bGU6dHJ1ZSx0aHJvdHRsZToyNTAsYmVmb3JlTG9hZDp1bmRlZmluZWQsYWZ0ZXJMb2FkOnVuZGVmaW5lZCxvbkVycm9yOnVuZGVmaW5lZCxvbkZpbmlzaGVkQWxsOnVuZGVmaW5lZH07JCh3aW5kb3cpLm9uKFwibG9hZFwiLGZ1bmN0aW9uKCl7d2luZG93TG9hZGVkPXRydWU7fSk7fSkod2luZG93KTs7KGZ1bmN0aW9uKCl7dmFyIHdpbj13aW5kb3csbGFzdFRpbWU9MDt3aW4ucmVxdWVzdEFuaW1hdGlvbkZyYW1lPXdpbi5yZXF1ZXN0QW5pbWF0aW9uRnJhbWV8fHdpbi53ZWJraXRSZXF1ZXN0QW5pbWF0aW9uRnJhbWU7aWYoIXdpbi5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUpe3dpbi5yZXF1ZXN0QW5pbWF0aW9uRnJhbWU9ZnVuY3Rpb24oY2FsbGJhY2spe3ZhciBjdXJyVGltZT1uZXcgRGF0ZSgpLmdldFRpbWUoKSx0aW1lVG9DYWxsPU1hdGgubWF4KDAsMTYtKGN1cnJUaW1lLWxhc3RUaW1lKSksaWQ9c2V0VGltZW91dChjYWxsYmFjayx0aW1lVG9DYWxsKTtsYXN0VGltZT1jdXJyVGltZSt0aW1lVG9DYWxsO3JldHVybiBpZDt9O31cbmlmKCF3aW4uY2FuY2VsQW5pbWF0aW9uRnJhbWUpe3dpbi5jYW5jZWxBbmltYXRpb25GcmFtZT1mdW5jdGlvbihpZCl7Y2xlYXJUaW1lb3V0KGlkKTt9O319KCkpOyhmdW5jdGlvbihyb290LGZhY3Rvcnkpe2lmKHR5cGVvZiBkZWZpbmU9PT0nZnVuY3Rpb24nJiZkZWZpbmUuYW1kKXtkZWZpbmUoJ3RoZW1lb25lLXV0aWxzL3V0aWxzJyxmYWN0b3J5KTt9ZWxzZSBpZih0eXBlb2YgbW9kdWxlPT09J29iamVjdCcmJm1vZHVsZS5leHBvcnRzKXttb2R1bGUuZXhwb3J0cz1mYWN0b3J5KCk7fWVsc2V7cm9vdC5UaGVtZW9uZVV0aWxzPWZhY3RvcnkoKTt9fSh0aGlzLGZ1bmN0aW9uKCl7XCJ1c2Ugc3RyaWN0XCI7dmFyIHV0aWxzPXt9O3ZhciBDb25zb2xlPXdpbmRvdy5jb25zb2xlO3V0aWxzLmVycm9yPWZ1bmN0aW9uKG1lc3NhZ2Upe2lmKHR5cGVvZiBDb25zb2xlIT09J3VuZGVmaW5lZCcpe0NvbnNvbGUuZXJyb3IobWVzc2FnZSk7fX07dXRpbHMuZXh0ZW5kPWZ1bmN0aW9uKG9wdGlvbnMsZGVmYXVsdHMpe2lmKG9wdGlvbnMpe2lmKHR5cGVvZiBvcHRpb25zIT09J29iamVjdCcpe3RoaXMuZXJyb3IoJ0N1c3RvbSBvcHRpb25zIG11c3QgYmUgYW4gb2JqZWN0Jyk7fWVsc2V7Zm9yKHZhciBwcm9wIGluIGRlZmF1bHRzKXtpZihkZWZhdWx0cy5oYXNPd25Qcm9wZXJ0eShwcm9wKSYmb3B0aW9ucy5oYXNPd25Qcm9wZXJ0eShwcm9wKSl7ZGVmYXVsdHNbcHJvcF09b3B0aW9uc1twcm9wXTt9fX19XG5yZXR1cm4gZGVmYXVsdHM7fTt1dGlscy5wcm9wPWZ1bmN0aW9uKHByb3Ape3ZhciBlbD10aGlzLmNyZWF0ZUVsKCkscHJlZml4ZXM9WycnLCdXZWJraXQnLCdNb3onLCdtcycsJ08nXTtmb3IodmFyIHA9MCxwbD1wcmVmaXhlcy5sZW5ndGg7cDxwbDtwKyspe3ZhciBwcmVmaXhlZFByb3A9cHJlZml4ZXNbcF0/cHJlZml4ZXNbcF0rcHJvcC5jaGFyQXQoMCkudG9VcHBlckNhc2UoKStwcm9wLnNsaWNlKDEpOnByb3A7aWYoZWwuc3R5bGVbcHJlZml4ZWRQcm9wXSE9PXVuZGVmaW5lZCl7cmV0dXJuIHByZWZpeGVkUHJvcDt9fVxucmV0dXJuJyc7fTt1dGlscy5jbG9uZU9iamVjdD1mdW5jdGlvbihvYmope3ZhciBjb3B5PXt9O2Zvcih2YXIgYXR0ciBpbiBvYmope2lmKG9iai5oYXNPd25Qcm9wZXJ0eShhdHRyKSl7Y29weVthdHRyXT1vYmpbYXR0cl07fX1cbnJldHVybiBjb3B5O307dXRpbHMuY3JlYXRlRWw9ZnVuY3Rpb24odGFnLGNsYXNzZXMpe3ZhciBlbD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KHRhZ3x8J2RpdicpO2lmKGNsYXNzZXMpe2VsLmNsYXNzTmFtZT1jbGFzc2VzO31cbnJldHVybiBlbDt9O3V0aWxzLmNhbWVsaXplPWZ1bmN0aW9uKHN0cmluZyl7cmV0dXJuIHN0cmluZy5yZXBsYWNlKC8tKFthLXpdKS9nLGZ1bmN0aW9uKGcpe3JldHVybiBnWzFdLnRvVXBwZXJDYXNlKCk7fSk7fTt1dGlscy5oYW5kbGVFdmVudHM9ZnVuY3Rpb24oX3RoaXMsZWwsZXZlbnQsZm4saXNCaW5kKXtpZih0eXBlb2YgdGhpcy5ldmVudF9oYW5kbGVycyE9PSdvYmplY3QnKXt0aGlzLmV2ZW50X2hhbmRsZXJzPXt9O31cbmlmKCF0aGlzLmV2ZW50X2hhbmRsZXJzW2ZuXSl7dGhpcy5ldmVudF9oYW5kbGVyc1tmbl09X3RoaXNbZm5dLmJpbmQoX3RoaXMpO31cbmlzQmluZD1pc0JpbmQ9PT11bmRlZmluZWQ/dHJ1ZTohIWlzQmluZDt2YXIgYmluZE1ldGhvZD1pc0JpbmQ/J2FkZEV2ZW50TGlzdGVuZXInOidyZW1vdmVFdmVudExpc3RlbmVyJztldmVudC5mb3JFYWNoKGZ1bmN0aW9uKGV2KXtlbFtiaW5kTWV0aG9kXShldix0aGlzLmV2ZW50X2hhbmRsZXJzW2ZuXSxmYWxzZSk7fS5iaW5kKHRoaXMpKTt9O3V0aWxzLmRpc3BhdGNoRXZlbnQ9ZnVuY3Rpb24oX3RoaXMsbmFtZXNwYWNlLHR5cGUsZXZlbnQsYXJncyl7dHlwZSs9bmFtZXNwYWNlPycuJytuYW1lc3BhY2U6Jyc7dmFyIGVtaXRBcmdzPWV2ZW50P1tldmVudF0uY29uY2F0KGFyZ3MpOlthcmdzXTtfdGhpcy5lbWl0RXZlbnQodHlwZSxlbWl0QXJncyk7fTt1dGlscy50aHJvdHRsZT1mdW5jdGlvbihmdW5jLGRlbGF5KXt2YXIgdGltZXN0YW1wPW51bGwsbGltaXQ9ZGVsYXk7cmV0dXJuIGZ1bmN0aW9uKCl7dmFyIHNlbGY9dGhpcyxhcmdzPWFyZ3VtZW50cyxub3c9RGF0ZS5ub3coKTtpZighdGltZXN0YW1wfHxub3ctdGltZXN0YW1wPj1saW1pdCl7dGltZXN0YW1wPW5vdztmdW5jLmFwcGx5KHNlbGYsYXJncyk7fX07fTt1dGlscy5tb2R1bG89ZnVuY3Rpb24obGVuZ3RoLGluZGV4KXtyZXR1cm4obGVuZ3RoKyhpbmRleCVsZW5ndGgpKSVsZW5ndGg7fTt1dGlscy5jbGFzc1JlZz1mdW5jdGlvbihjbGFzc05hbWUpe3JldHVybiBuZXcgUmVnRXhwKCcoXnxcXFxccyspJytjbGFzc05hbWUrJyhcXFxccyt8JCknKTt9O3V0aWxzLmhhc0NsYXNzPWZ1bmN0aW9uKGVsLGNsYXNzTmFtZSl7cmV0dXJuISFlbC5jbGFzc05hbWUubWF0Y2godGhpcy5jbGFzc1JlZyhjbGFzc05hbWUpKTt9O3V0aWxzLmFkZENsYXNzPWZ1bmN0aW9uKGVsLGNsYXNzTmFtZSl7aWYoIXRoaXMuaGFzQ2xhc3MoZWwsY2xhc3NOYW1lKSl7ZWwuY2xhc3NOYW1lKz0oZWwuY2xhc3NOYW1lPycgJzonJykrY2xhc3NOYW1lO319O3V0aWxzLnJlbW92ZUNsYXNzPWZ1bmN0aW9uKGVsLGNsYXNzTmFtZSl7aWYodGhpcy5oYXNDbGFzcyhlbCxjbGFzc05hbWUpKXtlbC5jbGFzc05hbWU9ZWwuY2xhc3NOYW1lLnJlcGxhY2UodGhpcy5jbGFzc1JlZyhjbGFzc05hbWUpLCcgJykucmVwbGFjZSgvXFxzKyQvLCcnKTt9fTt1dGlscy50cmFuc2xhdGU9ZnVuY3Rpb24oZWwseCx5LHMpe3ZhciBzY2FsZT1zPycgc2NhbGUoJytzKycsJytzKycpJzonJztlbC5zdHlsZVt0aGlzLmJyb3dzZXIudHJhbnNdPSh0aGlzLmJyb3dzZXIuZ3B1KT8ndHJhbnNsYXRlM2QoJysoeHx8MCkrJ3B4LCAnKyh5fHwwKSsncHgsIDApJytzY2FsZTondHJhbnNsYXRlKCcrKHh8fDApKydweCwgJysoeXx8MCkrJ3B4KScrc2NhbGU7fTt1dGlscy5icm93c2VyPXt0cmFuczp1dGlscy5wcm9wKCd0cmFuc2Zvcm0nKSxncHU6dXRpbHMucHJvcCgncGVyc3BlY3RpdmUnKT90cnVlOmZhbHNlfTtyZXR1cm4gdXRpbHM7fSkpOyhmdW5jdGlvbihyb290LGZhY3Rvcnkpe2lmKHR5cGVvZiBkZWZpbmU9PT0nZnVuY3Rpb24nJiZkZWZpbmUuYW1kKXtkZWZpbmUoJ3RoZW1lb25lLWV2ZW50L2V2ZW50JyxmYWN0b3J5KTt9ZWxzZSBpZih0eXBlb2YgbW9kdWxlPT09J29iamVjdCcmJm1vZHVsZS5leHBvcnRzKXttb2R1bGUuZXhwb3J0cz1mYWN0b3J5KCk7fWVsc2V7cm9vdC5UaGVtZW9uZUV2ZW50PWZhY3RvcnkoKTt9fSh0eXBlb2Ygd2luZG93IT09J3VuZGVmaW5lZCc/d2luZG93OnRoaXMsZnVuY3Rpb24oKXtcInVzZSBzdHJpY3RcIjt2YXIgRXZFbWl0dGVyPWZ1bmN0aW9uKCl7fSxwcm90bz1FdkVtaXR0ZXIucHJvdG90eXBlO3Byb3RvLm9uPWZ1bmN0aW9uKGV2ZW50TmFtZSxsaXN0ZW5lcil7aWYoIWV2ZW50TmFtZXx8IWxpc3RlbmVyKXtyZXR1cm4gbnVsbDt9XG52YXIgZXZlbnRzPXRoaXMuX2V2ZW50cz10aGlzLl9ldmVudHN8fHt9O3ZhciBsaXN0ZW5lcnM9ZXZlbnRzW2V2ZW50TmFtZV09ZXZlbnRzW2V2ZW50TmFtZV18fFtdO2lmKGxpc3RlbmVycy5pbmRleE9mKGxpc3RlbmVyKT09PS0xKXtsaXN0ZW5lcnMucHVzaChsaXN0ZW5lcik7fVxucmV0dXJuIHRoaXM7fTtwcm90by5vZmY9ZnVuY3Rpb24oZXZlbnROYW1lLGxpc3RlbmVyKXt2YXIgbGlzdGVuZXJzPXRoaXMuX2V2ZW50cyYmdGhpcy5fZXZlbnRzW2V2ZW50TmFtZV07aWYoIWxpc3RlbmVyc3x8IWxpc3RlbmVycy5sZW5ndGgpe3JldHVybiBudWxsO31cbnZhciBpbmRleD1saXN0ZW5lcnMuaW5kZXhPZihsaXN0ZW5lcik7aWYoaW5kZXghPT0tMSl7bGlzdGVuZXJzLnNwbGljZShpbmRleCwxKTt9XG5yZXR1cm4gdGhpczt9O3Byb3RvLmVtaXRFdmVudD1mdW5jdGlvbihldmVudE5hbWUsYXJncyl7dmFyIGxpc3RlbmVycz10aGlzLl9ldmVudHMmJnRoaXMuX2V2ZW50c1tldmVudE5hbWVdO2lmKCFsaXN0ZW5lcnN8fCFsaXN0ZW5lcnMubGVuZ3RoKXtyZXR1cm4gbnVsbDt9XG52YXIgaT0wLGxpc3RlbmVyPWxpc3RlbmVyc1tpXTthcmdzPWFyZ3N8fFtdO3ZhciBvbmNlTGlzdGVuZXJzPXRoaXMuX29uY2VFdmVudHMmJnRoaXMuX29uY2VFdmVudHNbZXZlbnROYW1lXTt3aGlsZShsaXN0ZW5lcil7dmFyIGlzT25jZT1vbmNlTGlzdGVuZXJzJiZvbmNlTGlzdGVuZXJzW2xpc3RlbmVyXTtpZihpc09uY2Upe3RoaXMub2ZmKGV2ZW50TmFtZSxsaXN0ZW5lcik7ZGVsZXRlIG9uY2VMaXN0ZW5lcnNbbGlzdGVuZXJdO31cbmxpc3RlbmVyLmFwcGx5KHRoaXMsYXJncyk7aSs9aXNPbmNlPzA6MTtsaXN0ZW5lcj1saXN0ZW5lcnNbaV07fVxucmV0dXJuIHRoaXM7fTtyZXR1cm4gRXZFbWl0dGVyO30pKTsoZnVuY3Rpb24ocm9vdCxmYWN0b3J5KXtpZih0eXBlb2YgZGVmaW5lPT09J2Z1bmN0aW9uJyYmZGVmaW5lLmFtZCl7ZGVmaW5lKCd0aGVtZW9uZS1hbmltYXRlL2FuaW1hdGUnLFsndGhlbWVvbmUtdXRpbHMvdXRpbHMnLCd0aGVtZW9uZS1ldmVudC9ldmVudCddLGZhY3RvcnkpO31lbHNlIGlmKHR5cGVvZiBtb2R1bGU9PT0nb2JqZWN0JyYmbW9kdWxlLmV4cG9ydHMpe21vZHVsZS5leHBvcnRzPWZhY3RvcnkocmVxdWlyZSgndGhlbWVvbmUtdXRpbHMnKSxyZXF1aXJlKCd0aGVtZW9uZS1ldmVudCcpKTt9ZWxzZXtyb290LlRoZW1lb25lQW5pbWF0ZT1mYWN0b3J5KHJvb3QuVGhlbWVvbmVVdGlscyxyb290LlRoZW1lb25lRXZlbnQpO319KHRoaXMsZnVuY3Rpb24odXRpbHMsRXZFbWl0dGVyKXsndXNlIHN0cmljdCc7dmFyIEFuaW1hdGU9ZnVuY3Rpb24oZWxlbWVudCxwb3NpdGlvbnMsZnJpY3Rpb24sYXR0cmFjdGlvbil7dGhpcy5lbGVtZW50PWVsZW1lbnQ7dGhpcy5kZWZhdWx0cz1wb3NpdGlvbnM7dGhpcy5mb3JjZXM9e2ZyaWN0aW9uOmZyaWN0aW9ufHwwLjI4LGF0dHJhY3Rpb246YXR0cmFjdGlvbnx8MC4wMjh9O3RoaXMucmVzZXRBbmltYXRlKCk7fTt2YXIgcHJvdG89QW5pbWF0ZS5wcm90b3R5cGU9T2JqZWN0LmNyZWF0ZShFdkVtaXR0ZXIucHJvdG90eXBlKTtwcm90by51cGRhdGVEcmFnPWZ1bmN0aW9uKG9iail7dGhpcy5tb3ZlPXRydWU7dGhpcy5kcmFnPW9iajt9O3Byb3RvLnJlbGVhc2VEcmFnPWZ1bmN0aW9uKCl7dGhpcy5tb3ZlPWZhbHNlO307cHJvdG8uYW5pbWF0ZVRvPWZ1bmN0aW9uKG9iail7dGhpcy5hdHRyYWN0aW9uPW9iajt9O3Byb3RvLnN0YXJ0QW5pbWF0ZT1mdW5jdGlvbigpe3RoaXMubW92ZT10cnVlO3RoaXMuc2V0dGxlPWZhbHNlO3RoaXMucmVzdGluZ0ZyYW1lcz0wO2lmKCF0aGlzLlJBRil7dGhpcy5hbmltYXRlKCk7fX07cHJvdG8uc3RvcEFuaW1hdGU9ZnVuY3Rpb24oKXt0aGlzLm1vdmU9ZmFsc2U7dGhpcy5yZXN0aW5nRnJhbWVzPTA7aWYodGhpcy5SQUYpe2NhbmNlbEFuaW1hdGlvbkZyYW1lKHRoaXMuUkFGKTt0aGlzLlJBRj1mYWxzZTt9XG50aGlzLnN0YXJ0PXV0aWxzLmNsb25lT2JqZWN0KHRoaXMucG9zaXRpb24pO3RoaXMudmVsb2NpdHk9e3g6MCx5OjAsczowfTt9O3Byb3RvLnJlc2V0QW5pbWF0ZT1mdW5jdGlvbigpe3RoaXMuc3RvcEFuaW1hdGUoKTt0aGlzLnNldHRsZT10cnVlO3RoaXMuZHJhZz11dGlscy5jbG9uZU9iamVjdCh0aGlzLmRlZmF1bHRzKTt0aGlzLnN0YXJ0PXV0aWxzLmNsb25lT2JqZWN0KHRoaXMuZGVmYXVsdHMpO3RoaXMucmVzdGluZz11dGlscy5jbG9uZU9iamVjdCh0aGlzLmRlZmF1bHRzKTt0aGlzLnBvc2l0aW9uPXV0aWxzLmNsb25lT2JqZWN0KHRoaXMuZGVmYXVsdHMpO3RoaXMuYXR0cmFjdGlvbj11dGlscy5jbG9uZU9iamVjdCh0aGlzLmRlZmF1bHRzKTt9O3Byb3RvLmFuaW1hdGU9ZnVuY3Rpb24oKXt2YXIgbG9vcD0oZnVuY3Rpb24oKXtpZih0eXBlb2YgdGhpcy5wb3NpdGlvbiE9PSd1bmRlZmluZWQnKXt2YXIgcHJldmlvdXM9dXRpbHMuY2xvbmVPYmplY3QodGhpcy5wb3NpdGlvbik7dGhpcy5hcHBseURyYWdGb3JjZSgpO3RoaXMuYXBwbHlBdHRyYWN0aW9uRm9yY2UoKTt1dGlscy5kaXNwYXRjaEV2ZW50KHRoaXMsJ3RvYW5pbWF0ZScsJ3JlbmRlcicsdGhpcyk7dGhpcy5pbnRlZ3JhdGVQaHlzaWNzKCk7dGhpcy5nZXRSZXN0aW5nUG9zaXRpb24oKTt0aGlzLnJlbmRlcigxMDApO3RoaXMuUkFGPXJlcXVlc3RBbmltYXRpb25GcmFtZShsb29wKTt0aGlzLmNoZWNrU2V0dGxlKHByZXZpb3VzKTt9fSkuYmluZCh0aGlzKTt0aGlzLlJBRj1yZXF1ZXN0QW5pbWF0aW9uRnJhbWUobG9vcCk7fTtwcm90by5pbnRlZ3JhdGVQaHlzaWNzPWZ1bmN0aW9uKCl7Zm9yKHZhciBrIGluIHRoaXMucG9zaXRpb24pe2lmKHR5cGVvZiB0aGlzLnBvc2l0aW9uW2tdIT09J3VuZGVmaW5lZCcpe3RoaXMucG9zaXRpb25ba10rPXRoaXMudmVsb2NpdHlba107dGhpcy5wb3NpdGlvbltrXT0oaz09PSdzJyk/TWF0aC5tYXgoMC4xLHRoaXMucG9zaXRpb25ba10pOnRoaXMucG9zaXRpb25ba107dGhpcy52ZWxvY2l0eVtrXSo9dGhpcy5nZXRGcmljdGlvbkZhY3RvcigpO319fTtwcm90by5hcHBseURyYWdGb3JjZT1mdW5jdGlvbigpe2lmKHRoaXMubW92ZSl7Zm9yKHZhciBrIGluIHRoaXMuZHJhZyl7aWYodHlwZW9mIHRoaXMuZHJhZ1trXSE9PSd1bmRlZmluZWQnKXt2YXIgZHJhZ1ZlbG9jaXR5PXRoaXMuZHJhZ1trXS10aGlzLnBvc2l0aW9uW2tdO3ZhciBkcmFnRm9yY2U9ZHJhZ1ZlbG9jaXR5LXRoaXMudmVsb2NpdHlba107dGhpcy5hcHBseUZvcmNlKGssZHJhZ0ZvcmNlKTt9fX19O3Byb3RvLmFwcGx5QXR0cmFjdGlvbkZvcmNlPWZ1bmN0aW9uKCl7aWYoIXRoaXMubW92ZSl7Zm9yKHZhciBrIGluIHRoaXMuYXR0cmFjdGlvbil7aWYodHlwZW9mIHRoaXMuYXR0cmFjdGlvbltrXSE9PSd1bmRlZmluZWQnKXt2YXIgZGlzdGFuY2U9dGhpcy5hdHRyYWN0aW9uW2tdLXRoaXMucG9zaXRpb25ba107dmFyIGZvcmNlPWRpc3RhbmNlKnRoaXMuZm9yY2VzLmF0dHJhY3Rpb247dGhpcy5hcHBseUZvcmNlKGssZm9yY2UpO319fX07cHJvdG8uZ2V0UmVzdGluZ1Bvc2l0aW9uPWZ1bmN0aW9uKCl7Zm9yKHZhciBrIGluIHRoaXMucG9zaXRpb24pe2lmKHR5cGVvZiB0aGlzLnBvc2l0aW9uW2tdIT09J3VuZGVmaW5lZCcpe3RoaXMucmVzdGluZ1trXT10aGlzLnBvc2l0aW9uW2tdK3RoaXMudmVsb2NpdHlba10vKDEtdGhpcy5nZXRGcmljdGlvbkZhY3RvcigpKTt9fX07cHJvdG8uYXBwbHlGb3JjZT1mdW5jdGlvbihkaXJlY3Rpb24sZm9yY2Upe3RoaXMudmVsb2NpdHlbZGlyZWN0aW9uXSs9Zm9yY2U7fTtwcm90by5nZXRGcmljdGlvbkZhY3Rvcj1mdW5jdGlvbigpe3JldHVybiAxLXRoaXMuZm9yY2VzLmZyaWN0aW9uO307cHJvdG8ucm91bmRWYWx1ZXM9ZnVuY3Rpb24odmFsdWVzLHJvdW5kKXtmb3IodmFyIGsgaW4gdmFsdWVzKXtpZih0eXBlb2YgdmFsdWVzW2tdIT09J3VuZGVmaW5lZCcpe3JvdW5kPWs9PT0ncyc/cm91bmQqMTAwOnJvdW5kO3ZhbHVlc1trXT1NYXRoLnJvdW5kKHZhbHVlc1trXSpyb3VuZCkvcm91bmQ7fX19O3Byb3RvLmNoZWNrU2V0dGxlPWZ1bmN0aW9uKHByZXZpb3VzKXtpZighdGhpcy5tb3ZlKXt2YXIgY291bnQ9MDtmb3IodmFyIGsgaW4gdGhpcy5wb3NpdGlvbil7aWYodHlwZW9mIHRoaXMucG9zaXRpb25ba10hPT0ndW5kZWZpbmVkJyl7dmFyIHJvdW5kPWs9PT0ncyc/MTAwMDA6MTAwO2lmKE1hdGgucm91bmQodGhpcy5wb3NpdGlvbltrXSpyb3VuZCk9PT1NYXRoLnJvdW5kKHByZXZpb3VzW2tdKnJvdW5kKSl7Y291bnQrKztpZihjb3VudD09PU9iamVjdC5rZXlzKHRoaXMucG9zaXRpb24pLmxlbmd0aCl7dGhpcy5yZXN0aW5nRnJhbWVzKys7fX19fX1cbmlmKHRoaXMucmVzdGluZ0ZyYW1lcz4yKXt0aGlzLnN0b3BBbmltYXRlKCk7dGhpcy5yZW5kZXIodGhpcy5wb3NpdGlvbi5zPjE/MTA6MSk7dGhpcy5zZXR0bGU9dHJ1ZTtpZihKU09OLnN0cmluZ2lmeSh0aGlzLnN0YXJ0KSE9PUpTT04uc3RyaW5naWZ5KHRoaXMucG9zaXRpb24pKXt1dGlscy5kaXNwYXRjaEV2ZW50KHRoaXMsJ3RvYW5pbWF0ZScsJ3NldHRsZScsdGhpcyk7fX19O3Byb3RvLnJlbmRlcj1mdW5jdGlvbihyb3VuZCl7dGhpcy5yb3VuZFZhbHVlcyh0aGlzLnBvc2l0aW9uLHJvdW5kKTt1dGlscy50cmFuc2xhdGUodGhpcy5lbGVtZW50LHRoaXMucG9zaXRpb24ueCx0aGlzLnBvc2l0aW9uLnksdGhpcy5wb3NpdGlvbi5zKTt9O3JldHVybiBBbmltYXRlO30pKTsoZnVuY3Rpb24ocm9vdCxmYWN0b3J5KXtpZih0eXBlb2YgZGVmaW5lPT09J2Z1bmN0aW9uJyYmZGVmaW5lLmFtZCl7ZGVmaW5lKFsndGhlbWVvbmUtdXRpbHMvdXRpbHMnLCd0aGVtZW9uZS1ldmVudC9ldmVudCcsJ3RoZW1lb25lLWFuaW1hdGUvYW5pbWF0ZSddLGZhY3RvcnkpO31lbHNlIGlmKHR5cGVvZiBleHBvcnRzPT09J29iamVjdCcmJm1vZHVsZS5leHBvcnRzKXttb2R1bGUuZXhwb3J0cz1mYWN0b3J5KHJlcXVpcmUoJ3RoZW1lb25lLXV0aWxzJykscmVxdWlyZSgndGhlbWVvbmUtZXZlbnQnKSxyZXF1aXJlKCd0aGVtZW9uZS1hbmltYXRlJykpO31lbHNle3Jvb3QuTW9kdWxvQm94PWZhY3Rvcnkocm9vdC5UaGVtZW9uZVV0aWxzLHJvb3QuVGhlbWVvbmVFdmVudCxyb290LlRoZW1lb25lQW5pbWF0ZSk7fX0odGhpcyxmdW5jdGlvbih1dGlscyxFdkVtaXR0ZXIsQW5pbWF0ZSl7J3VzZSBzdHJpY3QnO3ZhciB2ZXJzaW9uPScxLjUuMCc7dmFyIEdVSUQ9MDt2YXIgaW5zdGFuY2VzPXt9O3ZhciBleHBhbmRvPSdtb2J4JysodmVyc2lvbitNYXRoLnJhbmRvbSgpKS5yZXBsYWNlKC9cXEQvZywnJyk7dmFyIGNhY2hlPXt1aWQ6MH07dmFyIGRlZmF1bHRzPXttZWRpYVNlbGVjdG9yOicubW9ieCcsdGhyZXNob2xkOjUsYXR0cmFjdGlvbjp7c2xpZGVyOjAuMDU1LHNsaWRlOjAuMDE4LHRodW1iczowLjAxNn0sZnJpY3Rpb246e3NsaWRlcjowLjYyLHNsaWRlOjAuMTgsdGh1bWJzOjAuMjJ9LHJpZ2h0VG9MZWZ0OmZhbHNlLGxvb3A6MyxwcmVsb2FkOjEsdW5sb2FkOmZhbHNlLHRpbWVUb0lkbGU6NDAwMCxoaXN0b3J5OmZhbHNlLG1vdXNlV2hlZWw6dHJ1ZSxjb250ZXh0TWVudTp0cnVlLHNjcm9sbEJhcjp0cnVlLGZhZGVJZlNldHRsZTpmYWxzZSxjb250cm9sczpbJ2Nsb3NlJ10scHJldk5leHQ6dHJ1ZSxwcmV2TmV4dFRvdWNoOmZhbHNlLGNvdW50ZXJNZXNzYWdlOidbaW5kZXhdIC8gW3RvdGFsXScsY2FwdGlvbjp0cnVlLGF1dG9DYXB0aW9uOmZhbHNlLGNhcHRpb25TbWFsbERldmljZTp0cnVlLHRodW1ibmFpbHM6dHJ1ZSx0aHVtYm5haWxzTmF2OidiYXNpYycsdGh1bWJuYWlsU2l6ZXM6ezE5MjA6e3dpZHRoOjExMCxoZWlnaHQ6ODAsZ3V0dGVyOjEwfSwxMjgwOnt3aWR0aDo5MCxoZWlnaHQ6NjUsZ3V0dGVyOjEwfSw2ODA6e3dpZHRoOjcwLGhlaWdodDo1MCxndXR0ZXI6OH0sNDgwOnt3aWR0aDo2MCxoZWlnaHQ6NDQsZ3V0dGVyOjV9fSxzcGFjaW5nOjAuMSxzbWFydFJlc2l6ZTp0cnVlLG92ZXJmbG93OmZhbHNlLGxvYWRFcnJvcjonU29ycnksIGFuIGVycm9yIG9jY3VyZWQgd2hpbGUgbG9hZGluZyB0aGUgY29udGVudC4uLicsbm9Db250ZW50OidTb3JyeSwgbm8gY29udGVudCB3YXMgZm91bmQhJyxwcmV2TmV4dEtleTp0cnVlLHNjcm9sbFRvTmF2OmZhbHNlLHNjcm9sbFNlbnNpdGl2aXR5OjE1LHpvb21UbzonYXV0bycsbWluWm9vbToxLjIsbWF4Wm9vbTo0LGRvdWJsZVRhcFRvWm9vbTp0cnVlLHNjcm9sbFRvWm9vbTpmYWxzZSxwaW5jaFRvWm9vbTp0cnVlLGVzY2FwZVRvQ2xvc2U6dHJ1ZSxzY3JvbGxUb0Nsb3NlOmZhbHNlLHBpbmNoVG9DbG9zZTp0cnVlLGRyYWdUb0Nsb3NlOnRydWUsdGFwVG9DbG9zZTp0cnVlLHNoYXJlQnV0dG9uczpbJ2ZhY2Vib29rJywnZ29vZ2xlcGx1cycsJ3R3aXR0ZXInLCdwaW50ZXJlc3QnLCdsaW5rZWRpbicsJ3JlZGRpdCddLHNoYXJlVGV4dDonU2hhcmUgb24nLHNoYXJlZFVybDonZGVlcGxpbmsnLHNsaWRlU2hvd0ludGVydmFsOjQwMDAsc2xpZGVTaG93QXV0b1BsYXk6ZmFsc2Usc2xpZGVTaG93QXV0b1N0b3A6ZmFsc2UsY291bnRUaW1lcjp0cnVlLGNvdW50VGltZXJCZzoncmdiYSgyNTUsMjU1LDI1NSwwLjI1KScsY291bnRUaW1lckNvbG9yOidyZ2JhKDI1NSwyNTUsMjU1LDAuNzUpJyxtZWRpYWVsZW1lbnQ6ZmFsc2UsdmlkZW9SYXRpbzoxNi85LHZpZGVvTWF4V2lkdGg6MTE4MCx2aWRlb0F1dG9QbGF5OmZhbHNlLHZpZGVvVGh1bWJuYWlsOmZhbHNlfTt2YXIgTW9kdWxvQm94PWZ1bmN0aW9uKG9wdGlvbnMpe3ZhciBlbGVtZW50PWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5tb2J4LWhvbGRlcicpO2lmKGVsZW1lbnQmJmVsZW1lbnQuR1VJRCl7cmV0dXJuIGluc3RhbmNlc1tlbGVtZW50LkdVSURdO31cbnRoaXMub3B0aW9ucz11dGlscy5leHRlbmQob3B0aW9ucyxkZWZhdWx0cyk7dGhpcy5zZXRWYXIoKTt9O3ZhciBwcm90bz1Nb2R1bG9Cb3gucHJvdG90eXBlPU9iamVjdC5jcmVhdGUoRXZFbWl0dGVyLnByb3RvdHlwZSk7cHJvdG8uaW5pdD1mdW5jdGlvbigpe2lmKHRoaXMuR1VJRCl7cmV0dXJuO31cbnRoaXMuY3JlYXRlRE9NKCk7dGhpcy5HVUlEPSsrR1VJRDt0aGlzLkRPTS5ob2xkZXIuR1VJRD1HVUlEO2luc3RhbmNlc1t0aGlzLkdVSURdPXRoaXM7dGhpcy5zZXRBbmltYXRpb24oKTt0aGlzLmdldEdhbGxlcmllcygpO3RoaXMub3BlbkZyb21RdWVyeSgpO307cHJvdG8uc2V0VmFyPWZ1bmN0aW9uKCl7dmFyIHdpbj13aW5kb3csZG9jPWRvY3VtZW50LG5hdj1uYXZpZ2F0b3I7dGhpcy5wcmU9J21vYngnO3RoaXMuZ2VzdHVyZT17fTt0aGlzLmJ1dHRvbnM9e307dGhpcy5zbGlkZXI9e307dGhpcy5zbGlkZXM9e307dGhpcy5jZWxscz17fTt0aGlzLnN0YXRlcz17fTt0aGlzLnBvaW50ZXJzPVtdO3RoaXMuZXhwYW5kbz1leHBhbmRvO3RoaXMuY2FjaGU9Y2FjaGU7dGhpcy5kcmFnRXZlbnRzPXRoaXMuZGV0ZWN0UG9pbnRlckV2ZW50cygpO3RoaXMuYnJvd3Nlcj17dG91Y2hEZXZpY2U6KCdvbnRvdWNoc3RhcnQnaW4gd2luKXx8KG5hdi5tYXhUb3VjaFBvaW50cz4wKXx8KG5hdi5tc01heFRvdWNoUG9pbnRzPjApLHB1c2hTdGF0ZTonaGlzdG9yeSdpbiB3aW4mJidwdXNoU3RhdGUnaW4gaGlzdG9yeSxmdWxsU2NyZWVuOnRoaXMuZGV0ZWN0RnVsbFNjcmVlbigpLG1vdXNlV2hlZWw6J29ud2hlZWwnaW4gZG9jLmNyZWF0ZUVsZW1lbnQoJ2RpdicpPyd3aGVlbCc6ZG9jLm9ubW91c2V3aGVlbCE9PXVuZGVmaW5lZD8nbW91c2V3aGVlbCc6J0RPTU1vdXNlU2Nyb2xsJ307dGhpcy5pZnJhbWVWaWRlbz10aGlzLmlmcmFtZVZpZGVvKCk7dGhpcy5zb2NpYWxNZWRpYT10aGlzLnNvY2lhbE1lZGlhKCk7fTtwcm90by5kZXRlY3RQb2ludGVyRXZlbnRzPWZ1bmN0aW9uKCl7dmFyIG5hdj1uYXZpZ2F0b3I7aWYobmF2LnBvaW50ZXJFbmFibGVkKXtyZXR1cm57c3RhcnQ6Wydwb2ludGVyZG93biddLG1vdmU6Wydwb2ludGVybW92ZSddLGVuZDpbJ3BvaW50ZXJ1cCcsJ3BvaW50ZXJjYW5jZWwnXX07fVxuaWYobmF2Lm1zUG9pbnRlckVuYWJsZWQpe3JldHVybntzdGFydDpbJ01TUG9pbnRlckRvd24nXSxtb3ZlOlsnTVNQb2ludGVyTW92ZSddLGVuZDpbJ01TUG9pbnRlclVwJywnTVNQb2ludGVyQ2FuY2VsJ119O31cbnJldHVybntzdGFydDpbJ21vdXNlZG93bicsJ3RvdWNoc3RhcnQnXSxtb3ZlOlsnbW91c2Vtb3ZlJywndG91Y2htb3ZlJ10sZW5kOlsnbW91c2V1cCcsJ21vdXNlbGVhdmUnLCd0b3VjaGVuZCcsJ3RvdWNoY2FuY2VsJ119O307cHJvdG8uZGV0ZWN0RnVsbFNjcmVlbj1mdW5jdGlvbigpe3ZhciBmdWxsU2NyZWVuPVsnZnVsbHNjcmVlbkVuYWJsZWQnLCd3ZWJraXRGdWxsc2NyZWVuRW5hYmxlZCcsJ21vekZ1bGxTY3JlZW5FbmFibGVkJywnbXNGdWxsc2NyZWVuRW5hYmxlZCddO2Zvcih2YXIgaT0wLGw9ZnVsbFNjcmVlbi5sZW5ndGg7aTxsO2krKyl7aWYoZG9jdW1lbnRbZnVsbFNjcmVlbltpXV0pe3JldHVybntlbGVtZW50OlsnZnVsbHNjcmVlbkVsZW1lbnQnLCd3ZWJraXRGdWxsc2NyZWVuRWxlbWVudCcsJ21vekZ1bGxTY3JlZW5FbGVtZW50JywnbXNGdWxsc2NyZWVuRWxlbWVudCddW2ldLHJlcXVlc3Q6WydyZXF1ZXN0RnVsbHNjcmVlbicsJ3dlYmtpdFJlcXVlc3RGdWxsc2NyZWVuJywnbW96UmVxdWVzdEZ1bGxTY3JlZW4nLCdtc1JlcXVlc3RGdWxsc2NyZWVuJ11baV0sY2hhbmdlOlsnZnVsbHNjcmVlbmNoYW5nZScsJ3dlYmtpdGZ1bGxzY3JlZW5jaGFuZ2UnLCdtb3pmdWxsc2NyZWVuY2hhbmdlJywnTVNGdWxsc2NyZWVuQ2hhbmdlJ11baV0sZXhpdDpbJ2V4aXRGdWxsc2NyZWVuJywnd2Via2l0RXhpdEZ1bGxzY3JlZW4nLCdtb3pDYW5jZWxGdWxsU2NyZWVuJywnbXNFeGl0RnVsbHNjcmVlbiddW2ldfTt9fVxudmFyIGNvbnRyb2xzPXRoaXMub3B0aW9ucy5jb250cm9scyxpbmRleD1jb250cm9scy5pbmRleE9mKCdmdWxsU2NyZWVuJyk7aWYoaW5kZXg+LTEpe2NvbnRyb2xzLnNwbGljZShpbmRleCwxKTt9XG5yZXR1cm4gbnVsbDt9O3Byb3RvLmlmcmFtZVZpZGVvPWZ1bmN0aW9uKCl7cmV0dXJue3lvdXR1YmU6e3JlZzovKD86d3d3XFwuKT95b3V0dVxcLj9iZSg/OlxcLmNvbSk/XFwvPy4qPyg/OndhdGNofGVtYmVkKT8oPzouKnY9fHZcXC98d2F0Y2glM0Z2JTNEfFxcLykoW1xcd1xcLV9dKykmPy9pLHVybDonaHR0cHM6Ly93d3cueW91dHViZS5jb20vZW1iZWQvW0lEXT9lbmFibGVqc2FwaT0xJnJlbD0wJmF1dG9wbGF5PTEnLHNoYXJlOidodHRwczovL3d3dy55b3V0dWJlLmNvbS93YXRjaD92PVtJRF0nLHBvc3RlcjonaHR0cHM6Ly9pbWcueW91dHViZS5jb20vdmkvW0lEXS9tYXhyZXNkZWZhdWx0LmpwZycsdGh1bWI6J2h0dHBzOi8vaW1nLnlvdXR1YmUuY29tL3ZpL1tJRF0vZGVmYXVsdC5qcGcnLHBsYXk6e2V2ZW50Oidjb21tYW5kJyxmdW5jOidwbGF5VmlkZW8nfSxwYXVzZTp7ZXZlbnQ6J2NvbW1hbmQnLGZ1bmM6J3BhdXNlVmlkZW8nfX0sdmltZW86e3JlZzovKD86d3d3XFwufHBsYXllclxcLik/dmltZW8uY29tXFwvKD86Y2hhbm5lbHNcXC8oPzpcXHcrXFwvKT98Z3JvdXBzXFwvKD86W15cXC9dKilcXC92aWRlb3NcXC98YWxidW1cXC8oPzpcXGQrKVxcL3ZpZGVvXFwvfHZpZGVvXFwvKT8oXFxkKykoPzpbYS16QS1aMC05X1xcLV0rKT8vaSx1cmw6J2h0dHBzOi8vcGxheWVyLnZpbWVvLmNvbS92aWRlby9bSURdP2F1dG9wbGF5PTEmYXBpPTEnLHNoYXJlOidodHRwczovL3ZpbWVvLmNvbS9bSURdJyxwb3N0ZXI6J2h0dHBzOi8vdmltZW8uY29tL2FwaS92Mi92aWRlby9bSURdLmpzb24nLHBsYXk6e2V2ZW50Oidjb21tYW5kJyxtZXRob2Q6J3BsYXknfSxwYXVzZTp7ZXZlbnQ6J2NvbW1hbmQnLG1ldGhvZDoncGF1c2UnfX0sZGFpbHltb3Rpb246e3JlZzovKD86d3d3XFwuKT8oPzpkYWlseW1vdGlvblxcLmNvbSg/OlxcL2VtYmVkKSg/OlxcL3ZpZGVvfFxcL2h1Yil8ZGFpXFwubHkpXFwvKFswLTlhLXpdKykoPzpbXFwtXzAtOWEtekEtWl0rI3ZpZGVvPSg/OlthLXpBLVowLTlfXFwtXSspKT8vaSx1cmw6J2h0dHBzOi8vZGFpbHltb3Rpb24uY29tL2VtYmVkL3ZpZGVvL1tJRF0/YXV0b3BsYXk9MSZhcGk9cG9zdE1lc3NhZ2UnLHNoYXJlOidodHRwczovL3d3dy5kYWlseW1vdGlvbi5jb20vdmlkZW8vW0lEXScscG9zdGVyOidodHRwczovL3d3dy5kYWlseW1vdGlvbi5jb20vdGh1bWJuYWlsL3ZpZGVvL1tJRF0nLHRodW1iOidodHRwczovL3d3dy5kYWlseW1vdGlvbi5jb20vdGh1bWJuYWlsL3ZpZGVvL1tJRF0nLHBsYXk6J3BsYXknLHBhdXNlOidwYXVzZSd9LHdpc3RpYTp7cmVnOi8oPzp3d3dcXC4pPyg/Ondpc3RpYVxcLig/OmNvbXxuZXQpfHdpXFwuc3QpXFwvKD86KD86bXxtZWRpYXN8cHJvamVjdHMpfGVtYmVkXFwvKD86aWZyYW1lfHBsYXlsaXN0cykpXFwvKFthLXpBLVowLTlfXFwtXSspL2ksdXJsOidodHRwczovL2Zhc3Qud2lzdGlhLm5ldC9lbWJlZC9pZnJhbWUvW0lEXT92ZXJzaW9uPTMmZW5hYmxlanNhcGk9MSZodG1sNT0xJmF1dG9wbGF5PTEnLHNoYXJlOidodHRwczovL2Zhc3Qud2lzdGlhLm5ldC9lbWJlZC9pZnJhbWUvW0lEXScscG9zdGVyOidodHRwczovL2Zhc3Qud2lzdGlhLmNvbS9vZW1iZWQ/dXJsPWh0dHBzOi8vaG9tZS53aXN0aWEuY29tL21lZGlhcy9bSURdLmpzb24nLHBsYXk6e2V2ZW50OidjbWQnLG1ldGhvZDoncGxheSd9LHBhdXNlOntldmVudDonY21kJyxtZXRob2Q6J3BhdXNlJ319fTt9O3Byb3RvLnNvY2lhbE1lZGlhPWZ1bmN0aW9uKCl7cmV0dXJue2ZhY2Vib29rOidodHRwczovL3d3dy5mYWNlYm9vay5jb20vc2hhcmVyL3NoYXJlci5waHA/dT1bdXJsXScsZ29vZ2xlcGx1czonaHR0cHM6Ly9wbHVzLmdvb2dsZS5jb20vc2hhcmU/dXJsPVt1cmxdJyx0d2l0dGVyOidodHRwczovL3R3aXR0ZXIuY29tL2ludGVudC90d2VldD90ZXh0PVt0ZXh0XSZ1cmw9W3VybF0nLHBpbnRlcmVzdDonaHR0cHM6Ly93d3cucGludGVyZXN0LmNvbS9waW4vY3JlYXRlL2J1dHRvbi8/dXJsPVt1cmxdJm1lZGlhPVtpbWFnZV0mZGVzY3JpcHRpb249W3RleHRdJyxsaW5rZWRpbjonaHR0cHM6Ly93d3cubGlua2VkaW4uY29tL3NoYXJlQXJ0aWNsZT91cmw9W3VybF0mbWluaT10cnVlJnRpdGxlPVt0ZXh0XScscmVkZGl0OidodHRwczovL3d3dy5yZWRkaXQuY29tL3N1Ym1pdD91cmw9W3VybF0mdGl0bGU9W3RleHRdJyxzdHVtYmxldXBvbjonaHR0cHM6Ly93d3cuc3R1bWJsZXVwb24uY29tL2JhZGdlP3VybD1bdXJsXSZ0aXRsZT1bdGV4dF0nLHR1bWJscjonaHR0cHM6Ly93d3cudHVtYmxyLmNvbS9zaGFyZT92PTMmdT1bdXJsXSZ0PVt0ZXh0XScsYmxvZ2dlcjonaHR0cHM6Ly93d3cuYmxvZ2dlci5jb20vYmxvZ190aGlzLnB5cmE/dCZ1PVt1cmxdJm49W3RleHRdJyxidWZmZXI6J2h0dHBzOi8vYnVmZmVyYXBwLmNvbS9hZGQ/dXJsPVt1cmxddGl0bGU9W3RleHRdJyxkaWdnOidodHRwczovL2RpZ2cuY29tL3N1Ym1pdD91cmw9W3VybF0mdGl0bGU9W3RleHRdJyxldmVybm90ZTonaHR0cHM6Ly93d3cuZXZlcm5vdGUuY29tL2NsaXAuYWN0aW9uP3VybD1bdXJsXSZ0aXRsZT1bdGV4dF0nfTt9O3Byb3RvLmNyZWF0ZURPTT1mdW5jdGlvbigpe3RoaXMuRE9NPXt9O3ZhciBlbGVtZW50cz1bJ2hvbGRlcicsJ292ZXJsYXknLCdzbGlkZXInLCdpdGVtJywnaXRlbS1pbm5lcicsJ3VpJywndG9wLWJhcicsJ2JvdHRvbS1iYXInLCdzaGFyZS10b29sdGlwJywnY291bnRlcicsJ2NhcHRpb24nLCdjYXB0aW9uLWlubmVyJywndGh1bWJzLWhvbGRlcicsJ3RodW1icy1pbm5lciddO2Zvcih2YXIgaT0wO2k8ZWxlbWVudHMubGVuZ3RoO2krKyl7dGhpcy5ET01bdXRpbHMuY2FtZWxpemUoZWxlbWVudHNbaV0pXT11dGlscy5jcmVhdGVFbCgnZGl2Jyx0aGlzLnByZSsnLScrZWxlbWVudHNbaV0pO31cbnRoaXMuYXBwZW5kRE9NKHRoaXMuRE9NKTt9O3Byb3RvLmFwcGVuZERPTT1mdW5jdGlvbihkb20pe3ZhciBvcHQ9dGhpcy5vcHRpb25zO2RvbS5ob2xkZXIuYXBwZW5kQ2hpbGQoZG9tLm92ZXJsYXkpO2RvbS5ob2xkZXIuYXBwZW5kQ2hpbGQoZG9tLnNsaWRlcik7ZG9tLmhvbGRlci5hcHBlbmRDaGlsZChkb20udWkpO2Zvcih2YXIgaT0wO2k8NTtpKyspe3ZhciBzbGlkZT1kb20uaXRlbS5jbG9uZU5vZGUodHJ1ZSk7c2xpZGUuYXBwZW5kQ2hpbGQoZG9tLml0ZW1Jbm5lci5jbG9uZU5vZGUodHJ1ZSkpO2RvbS5zbGlkZXIuYXBwZW5kQ2hpbGQoc2xpZGUpO3RoaXMuc2xpZGVzW2ldPXNsaWRlO31cbnRoaXMuc2xpZGVzLmxlbmd0aD1kb20uc2xpZGVyLmNoaWxkcmVuLmxlbmd0aDt0aGlzLmNyZWF0ZVVJKGRvbSxvcHQpO2RvbS5ob2xkZXIuc2V0QXR0cmlidXRlKCd0YWJpbmRleCcsLTEpO2RvbS5ob2xkZXIuc2V0QXR0cmlidXRlKCdhcmlhLWhpZGRlbicsdHJ1ZSk7dGhpcy5ET00uY29tbWVudD1kb2N1bWVudC5jcmVhdGVDb21tZW50KCcgTW9kdWxvQm94ICh2Jyt2ZXJzaW9uKycpIGJ5IFRoZW1lb25lICcpO2RvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5ET00uY29tbWVudCk7dXRpbHMuZGlzcGF0Y2hFdmVudCh0aGlzLCdtb2R1bG9ib3gnLCdiZWZvcmVBcHBlbmRET00nLGRvbSk7ZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChkb20uaG9sZGVyKTtkb20udG9wQmFyLmhlaWdodD1kb20udG9wQmFyLmNsaWVudEhlaWdodDt9O3Byb3RvLmNyZWF0ZVVJPWZ1bmN0aW9uKGRvbSxvcHQpe3ZhciBzaGFyZUluZGV4PW9wdC5jb250cm9scy5pbmRleE9mKCdzaGFyZScpO2lmKHNoYXJlSW5kZXg+LTEpe3ZhciBidXR0b25zPW9wdC5zaGFyZUJ1dHRvbnMsaT1idXR0b25zLmxlbmd0aDt3aGlsZShpLS0pe2lmKCF0aGlzLnNvY2lhbE1lZGlhLmhhc093blByb3BlcnR5KGJ1dHRvbnNbaV0pKXtidXR0b25zLnNwbGljZShpLDEpO319XG5pZihidXR0b25zLmxlbmd0aCl7ZG9tLnVpLmFwcGVuZENoaWxkKGRvbS5zaGFyZVRvb2x0aXApO2lmKG9wdC5zaGFyZVRleHQpe2RvbS5zaGFyZVRvb2x0aXAuYXBwZW5kQ2hpbGQodXRpbHMuY3JlYXRlRWwoJ3NwYW4nKSkudGV4dENvbnRlbnQ9b3B0LnNoYXJlVGV4dDt9XG50aGlzLmNyZWF0ZUJ1dHRvbnMoYnV0dG9ucyxkb20uc2hhcmVUb29sdGlwLCdzaGFyZU9uJyk7fWVsc2V7b3B0LmNvbnRyb2xzLnNwbGljZShzaGFyZUluZGV4LDEpO319XG5pZihvcHQuY29udHJvbHMubGVuZ3RofHxvcHQuY291bnRlck1lc3NhZ2Upe3ZhciBzbGlkZVNob3c9b3B0LmNvbnRyb2xzLmluZGV4T2YoJ3BsYXknKTtkb20udWkuYXBwZW5kQ2hpbGQoZG9tLnRvcEJhcik7aWYob3B0LmNvdW50ZXJNZXNzYWdlKXtkb20udG9wQmFyLmFwcGVuZENoaWxkKGRvbS5jb3VudGVyKTt9XG5pZihvcHQuc2xpZGVTaG93SW50ZXJ2YWw8MSYmc2xpZGVTaG93Pi0xKXtvcHQuY29udHJvbHMuc3BsaWNlKHNsaWRlU2hvdywxKTt9XG5pZihvcHQuY291bnRUaW1lciYmc2xpZGVTaG93Pi0xKXt2YXIgdGltZXI9dGhpcy5ET00udGltZXI9dXRpbHMuY3JlYXRlRWwoJ2NhbnZhcycsdGhpcy5wcmUrJy10aW1lcicpO3RpbWVyLnNldEF0dHJpYnV0ZSgnd2lkdGgnLDQ4KTt0aW1lci5zZXRBdHRyaWJ1dGUoJ2hlaWdodCcsNDgpO2RvbS50b3BCYXIuYXBwZW5kQ2hpbGQodGltZXIpO31cbmlmKG9wdC5jb250cm9scy5sZW5ndGgpe3ZhciBjb250cm9scz1vcHQuY29udHJvbHMuc2xpY2UoKTt0aGlzLmNyZWF0ZUJ1dHRvbnMoY29udHJvbHMucmV2ZXJzZSgpLGRvbS50b3BCYXIpO319XG5pZihvcHQuY2FwdGlvbnx8b3B0LnRodW1ibmFpbHMpe2RvbS51aS5hcHBlbmRDaGlsZChkb20uYm90dG9tQmFyKTtpZihvcHQuY2FwdGlvbil7ZG9tLmJvdHRvbUJhci5hcHBlbmRDaGlsZChkb20uY2FwdGlvbikuYXBwZW5kQ2hpbGQoZG9tLmNhcHRpb25Jbm5lcik7fVxuaWYob3B0LnRodW1ibmFpbHMpe2RvbS5ib3R0b21CYXIuYXBwZW5kQ2hpbGQoZG9tLnRodW1ic0hvbGRlcikuYXBwZW5kQ2hpbGQoZG9tLnRodW1ic0lubmVyKTt9fVxuaWYob3B0LnByZXZOZXh0KXt0aGlzLmNyZWF0ZUJ1dHRvbnMoWydwcmV2JywnbmV4dCddLGRvbS51aSk7fX07cHJvdG8uY3JlYXRlQnV0dG9ucz1mdW5jdGlvbihidXR0b25zLGRvbSxldmVudCl7dmFyIGxlbmd0aD1idXR0b25zLmxlbmd0aDtmb3IodmFyIGk9MDtpPGxlbmd0aDtpKyspe3ZhciB0eXBlPWJ1dHRvbnNbaV07dGhpcy5idXR0b25zW3R5cGVdPXV0aWxzLmNyZWF0ZUVsKCdCVVRUT04nLHRoaXMucHJlKyctJyt0eXBlLnRvTG93ZXJDYXNlKCkpO2RvbS5hcHBlbmRDaGlsZCh0aGlzLmJ1dHRvbnNbdHlwZV0pO2lmKCh0eXBlJiZ0eXBlb2YgdGhpc1t0eXBlXT09PSdmdW5jdGlvbicpfHxldmVudCl7dGhpcy5idXR0b25zW3R5cGVdLmV2ZW50PWV2ZW50P2V2ZW50OnR5cGU7dGhpcy5idXR0b25zW3R5cGVdLmFjdGlvbj10eXBlO2lmKGV2ZW50PT09J3NoYXJlT24nKXt0aGlzLmJ1dHRvbnNbdHlwZV0uc2V0QXR0cmlidXRlKCd0aXRsZScsdHlwZS5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSt0eXBlLnNsaWNlKDEpKTt9fX19O3Byb3RvLmdldEdhbGxlcmllcz1mdW5jdGlvbigpe3RoaXMuZ2FsbGVyaWVzPXt9O3ZhciBzZWxlY3RvcnM9dGhpcy5vcHRpb25zLm1lZGlhU2VsZWN0b3Isc291cmNlcz0nJztpZighc2VsZWN0b3JzKXtyZXR1cm4gZmFsc2U7fVxudHJ5e3NvdXJjZXM9ZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChzZWxlY3RvcnMpO31jYXRjaChlcnJvcil7dXRpbHMuZXJyb3IoJ1lvdXIgY3VycmVudCBtZWRpYVNlbGVjdG9yIGlzIG5vdCBhIHZhbGlkIHNlbGVjdG9yOiBcIicrc2VsZWN0b3JzKydcIicpO31cbmZvcih2YXIgaT0wLGw9c291cmNlcy5sZW5ndGg7aTxsO2krKyl7dmFyIHNvdXJjZT1zb3VyY2VzW2ldLG1lZGlhPXt9O21lZGlhLnNyYz1zb3VyY2UudGFnTmFtZT09PSdBJz9zb3VyY2UuZ2V0QXR0cmlidXRlKCdocmVmJyk6bnVsbDttZWRpYS5zcmM9c291cmNlLnRhZ05hbWU9PT0nSU1HJz9zb3VyY2UuY3VycmVudFNyY3x8c291cmNlLnNyYzptZWRpYS5zcmM7bWVkaWEuc3JjPXNvdXJjZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtc3JjJyl8fG1lZGlhLnNyYztpZihtZWRpYS5zcmMpe3RoaXMuZ2V0TWVkaWFBdHRzKHNvdXJjZSxtZWRpYSk7dGhpcy5zZXRNZWRpYVR5cGUobWVkaWEpO2lmKG1lZGlhLnR5cGUpe3RoaXMuZ2V0TWVkaWFUaHVtYihzb3VyY2UsbWVkaWEpO3RoaXMuZ2V0VmlkZW9UaHVtYihtZWRpYSk7dGhpcy5nZXRNZWRpYUNhcHRpb24oc291cmNlLG1lZGlhKTt0aGlzLnNldE1lZGlhQ2FwdGlvbihtZWRpYSk7dmFyIGdhbGxlcnk9dGhpcy5zZXRHYWxsZXJ5TmFtZShzb3VyY2UpO3RoaXMuc2V0R2FsbGVyeUZlYXR1cmVzKGdhbGxlcnksbWVkaWEpO21lZGlhLmluZGV4PWdhbGxlcnkubGVuZ3RoO2dhbGxlcnkucHVzaChtZWRpYSk7dGhpcy5zZXRNZWRpYUV2ZW50KHNvdXJjZSxnYWxsZXJ5Lm5hbWUsbWVkaWEuaW5kZXgpO319fVxudXRpbHMuZGlzcGF0Y2hFdmVudCh0aGlzLCdtb2R1bG9ib3gnLCd1cGRhdGVHYWxsZXJpZXMnLHRoaXMuZ2FsbGVyaWVzKTt9O3Byb3RvLmFkZE1lZGlhPWZ1bmN0aW9uKG5hbWUsbWVkaWEpe2lmKCFtZWRpYXx8dHlwZW9mIG1lZGlhIT09J29iamVjdCcpe3V0aWxzLmVycm9yKCdObyBtZWRpYSB3YXMgZm91bmQgdG8gYWRkTWVkaWEoKSBpbiBhIGdhbGxlcnknKTtyZXR1cm4gZmFsc2U7fVxubmFtZT1uYW1lPT09Jyc/MTpuYW1lO3ZhciBnYWxsZXJ5PXRoaXMuZ2FsbGVyaWVzW25hbWVdO2dhbGxlcnk9IWdhbGxlcnk/KHRoaXMuZ2FsbGVyaWVzW25hbWVdPVtdKTpnYWxsZXJ5O2dhbGxlcnkubmFtZT1uYW1lO3ZhciBsZW5ndGg9bWVkaWEubGVuZ3RoO2Zvcih2YXIgaT0wO2k8bGVuZ3RoO2krKyl7dmFyIGl0ZW09dXRpbHMuY2xvbmVPYmplY3QobWVkaWFbaV0pO2lmKGl0ZW0uc3JjKXt0aGlzLnNldE1lZGlhVHlwZShpdGVtKTt0aGlzLmdldFZpZGVvVGh1bWIoaXRlbSk7dGhpcy5zZXRNZWRpYUNhcHRpb24oaXRlbSk7dGhpcy5zZXRHYWxsZXJ5RmVhdHVyZXMoZ2FsbGVyeSxpdGVtKTtpdGVtLmluZGV4PWdhbGxlcnkubGVuZ3RoO2dhbGxlcnkucHVzaChpdGVtKTt9fX07cHJvdG8uc2V0TWVkaWFUeXBlPWZ1bmN0aW9uKG1lZGlhKXtpZihbJ2ltYWdlJywndmlkZW8nLCdpZnJhbWUnLCdIVE1MJ10uaW5kZXhPZihtZWRpYS50eXBlKT4tMSl7cmV0dXJuO31cbm1lZGlhLnR5cGU9bnVsbDt2YXIgc291cmNlPW1lZGlhLnNyYz9tZWRpYS5zcmM6bnVsbDt2YXIgZXh0ZW5zaW9uPShzb3VyY2Uuc3BsaXQoL1s/I10vKVswXXx8c291cmNlKS5zdWJzdHIoKH4tc291cmNlLmxhc3RJbmRleE9mKCcuJyk+Pj4wKSsyKTtpZigvKGpwZ3xqcGVnfHBuZ3xibXB8Z2lmfHRpZnx0aWZmfGpmaXxqZmlmfGV4aWZ8c3ZnKS9pLnRlc3QoZXh0ZW5zaW9uKXx8WydleHRlcm5hbC54eC5mYmNkbicsJ2Ryc2Nkbi41MDBweC5vcmcnXS5pbmRleE9mKHNvdXJjZSk+LTEpe21lZGlhLnR5cGU9J2ltYWdlJzttZWRpYS5zcmM9dGhpcy5nZXRTcmMoc291cmNlKTt9ZWxzZSBpZigvKG1wNHx3ZWJtfG9ndikvaS50ZXN0KGV4dGVuc2lvbikpe21lZGlhLnR5cGU9J3ZpZGVvJzttZWRpYS5mb3JtYXQ9J2h0bWw1Jzt9ZWxzZXt2YXIgc3RyZWFtPXRoaXMuaWZyYW1lVmlkZW87Zm9yKHZhciB0eXBlIGluIHN0cmVhbSl7aWYoc3RyZWFtLmhhc093blByb3BlcnR5KHR5cGUpKXt2YXIgcmVncz1zb3VyY2UubWF0Y2goc3RyZWFtW3R5cGVdLnJlZyk7aWYocmVncyYmcmVnc1sxXSl7dmFyIG9iamVjdD1zdHJlYW1bdHlwZV07bWVkaWEudHlwZT0ndmlkZW8nO21lZGlhLmZvcm1hdD10eXBlO21lZGlhLnNoYXJlPW9iamVjdC5zaGFyZS5yZXBsYWNlKCdbSURdJyxyZWdzWzFdKTttZWRpYS5zcmM9b2JqZWN0LnVybC5yZXBsYWNlKCdbSURdJyxyZWdzWzFdKTttZWRpYS5wYXVzZT1vYmplY3QucGF1c2U7bWVkaWEucGxheT1vYmplY3QucGxheTtpZih0aGlzLm9wdGlvbnMudmlkZW9UaHVtYm5haWwpe21lZGlhLnBvc3Rlcj0hbWVkaWEucG9zdGVyJiZvYmplY3QucG9zdGVyP29iamVjdC5wb3N0ZXIucmVwbGFjZSgnW0lEXScscmVnc1sxXSk6bWVkaWEucG9zdGVyO21lZGlhLnRodW1iPSFtZWRpYS50aHVtYiYmb2JqZWN0LnBvc3Rlcj9vYmplY3QucG9zdGVyLnJlcGxhY2UoJ1tJRF0nLHJlZ3NbMV0pOm1lZGlhLnRodW1iO31cbmJyZWFrO319fX19O3Byb3RvLmdldFNyYz1mdW5jdGlvbihzb3VyY2Upe3ZhciBzcmNzZXQ9KHNvdXJjZXx8JycpLnNwbGl0KC8sLyksbGVuZ3RoPXNyY3NldC5sZW5ndGgsd2lkdGg9MDtpZihsZW5ndGg8PTEpe3JldHVybiBzb3VyY2U7fVxuZm9yKHZhciBpPTA7aTxsZW5ndGg7aSsrKXt2YXIgcGFydHM9c3Jjc2V0W2ldLnJlcGxhY2UoL1xccysvZywnICcpLnRyaW0oKS5zcGxpdCgvIC8pLHZhbHVlPXBhcnNlRmxvYXQocGFydHNbMV0pfHwwLHVuaXQ9cGFydHNbMV0/cGFydHNbMV0uc2xpY2UoLTEpOm51bGw7aWYoKHVuaXQ9PT0ndycmJnNjcmVlbi53aWR0aD49dmFsdWUmJnZhbHVlPndpZHRoKXx8IXZhbHVlfHxpPT09MCl7d2lkdGg9dmFsdWU7c291cmNlPXBhcnRzWzBdO319XG5yZXR1cm4gc291cmNlO307cHJvdG8uZ2V0TWVkaWFBdHRzPWZ1bmN0aW9uKHNvdXJjZSxtZWRpYSl7dmFyIGF1dG89dGhpcy5vcHRpb25zLmF1dG9DYXB0aW9uLGRhdGE9dGhpcy5nZXRBdHRyKHNvdXJjZSksaW1nPXNvdXJjZS5maXJzdEVsZW1lbnRDaGlsZDtpbWc9c291cmNlLnRhZ05hbWUhPT0nSU1HJyYmaW1nJiZpbWcudGFnTmFtZT09PSdJTUcnP2ltZzpzb3VyY2U7bWVkaWEudHlwZT0hbWVkaWEudHlwZT9kYXRhLnR5cGV8fHNvdXJjZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtdHlwZScpOm1lZGlhLnR5cGU7bWVkaWEudGl0bGU9ZGF0YS50aXRsZXx8c291cmNlLmdldEF0dHJpYnV0ZSgnZGF0YS10aXRsZScpfHwoYXV0bz9pbWcudGl0bGU6bnVsbCk7bWVkaWEuZGVzYz1kYXRhLmRlc2N8fHNvdXJjZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtZGVzYycpfHwoYXV0bz9pbWcuYWx0Om51bGwpO21lZGlhLnRodW1iPWRhdGEudGh1bWJ8fHNvdXJjZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtdGh1bWInKTttZWRpYS5wb3N0ZXI9dGhpcy5nZXRTcmMoZGF0YS5wb3N0ZXJ8fHNvdXJjZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtcG9zdGVyJykpO21lZGlhLndpZHRoPWRhdGEud2lkdGh8fHNvdXJjZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtd2lkdGgnKTttZWRpYS5oZWlnaHQ9ZGF0YS5oZWlnaHR8fHNvdXJjZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtaGVpZ2h0Jyk7aWYobWVkaWEudGl0bGU9PT1tZWRpYS5kZXNjKXttZWRpYS5kZXNjPW51bGw7fX07cHJvdG8uZ2V0TWVkaWFUaHVtYj1mdW5jdGlvbihzb3VyY2UsbWVkaWEpe3ZhciB0aHVtYm5haWw9c291cmNlLmdldEVsZW1lbnRzQnlUYWdOYW1lKCdpbWcnKTtpZighbWVkaWEudGh1bWImJnRodW1ibmFpbFswXSl7bWVkaWEudGh1bWI9dGh1bWJuYWlsWzBdLnNyYzt9fTtwcm90by5nZXRWaWRlb1RodW1iPWZ1bmN0aW9uKG1lZGlhKXtpZighdGhpcy5vcHRpb25zLnZpZGVvVGh1bWJuYWlsfHxtZWRpYS50eXBlIT09J3ZpZGVvJ3x8bWVkaWEuZm9ybWF0PT09J2h0bWw1Jyl7cmV0dXJuO31cbnZhciBoYXNQb3N0ZXI9bWVkaWEucG9zdGVyJiZtZWRpYS5wb3N0ZXIuaW5kZXhPZignLmpzb24nKT4tMSxoYXNUaHVtYj1tZWRpYS50aHVtYiYmbWVkaWEudGh1bWIuaW5kZXhPZignLmpzb24nKT4tMTtpZihoYXNQb3N0ZXJ8fGhhc1RodW1iKXt2YXIgdXJpPWhhc1Bvc3Rlcj9tZWRpYS5wb3N0ZXI6bWVkaWEudGh1bWIseGhyPW5ldyBYTUxIdHRwUmVxdWVzdCgpO3hoci5vbmxvYWQ9ZnVuY3Rpb24oZXZlbnQpe3ZhciByZXNwb25zZT1ldmVudC50YXJnZXQucmVzcG9uc2VUZXh0O3Jlc3BvbnNlPUpTT04ucGFyc2UocmVzcG9uc2UpO3Jlc3BvbnNlPXJlc3BvbnNlLmhhc093blByb3BlcnR5KDApP3Jlc3BvbnNlWzBdOnJlc3BvbnNlO2lmKHJlc3BvbnNlKXttZWRpYS5wb3N0ZXI9cmVzcG9uc2UudGh1bWJuYWlsX2xhcmdlfHxyZXNwb25zZS50aHVtYm5haWxfdXJsO2lmKG1lZGlhLmRvbSl7bWVkaWEuZG9tLnN0eWxlLmJhY2tncm91bmRJbWFnZT0ndXJsKFwiJyttZWRpYS5wb3N0ZXIrJ1wiKSc7fVxuaWYoaGFzVGh1bWIpe3ZhciB0aHVtYj1yZXNwb25zZS50aHVtYm5haWxfc21hbGx8fHJlc3BvbnNlLnRodW1ibmFpbF91cmw7aWYodHlwZW9mIG1lZGlhLnRodW1iPT09J29iamVjdCcpe21lZGlhLnRodW1iLnN0eWxlLmJhY2tncm91bmRJbWFnZT0ndXJsKFwiJyt0aHVtYisnXCIpJzt9ZWxzZXttZWRpYS50aHVtYj10aHVtYjt9fX19LmJpbmQodGhpcyk7eGhyLm9wZW4oJ0dFVCcsZW5jb2RlVVJJKHVyaSksdHJ1ZSk7c2V0VGltZW91dChmdW5jdGlvbigpe3hoci5zZW5kKCk7fSwwKTt9fTtwcm90by5nZXRNZWRpYUNhcHRpb249ZnVuY3Rpb24oc291cmNlLG1lZGlhKXt2YXIgbmV4dD1zb3VyY2UubmV4dEVsZW1lbnRTaWJsaW5nO2lmKG5leHQmJm5leHQudGFnTmFtZT09PSdGSUdDQVBUSU9OJyl7dmFyIGNhcHRpb249bmV4dC5pbm5lckhUTUw7aWYoIW1lZGlhLnRpdGxlKXttZWRpYS50aXRsZT1jYXB0aW9uO31lbHNlIGlmKCFtZWRpYS5kZXNjKXttZWRpYS5kZXNjPWNhcHRpb247fX19O3Byb3RvLnNldE1lZGlhQ2FwdGlvbj1mdW5jdGlvbihtZWRpYSl7bWVkaWEudGl0bGU9bWVkaWEudGl0bGU/JzxkaXYgY2xhc3M9XCInK3RoaXMucHJlKyctdGl0bGVcIj4nK21lZGlhLnRpdGxlLnRyaW0oKSsnPC9kaXY+JzonJzttZWRpYS5kZXNjPW1lZGlhLmRlc2M/JzxkaXYgY2xhc3M9XCInK3RoaXMucHJlKyctZGVzY1wiPicrbWVkaWEuZGVzYy50cmltKCkrJzwvZGl2Pic6Jyc7bWVkaWEuY2FwdGlvbj1tZWRpYS50aXRsZSttZWRpYS5kZXNjO307cHJvdG8uZ2V0R2FsbGVyeU5hbWU9ZnVuY3Rpb24oc291cmNlKXt2YXIgcGFyZW50PXNvdXJjZSxub2RlPTA7d2hpbGUocGFyZW50JiZub2RlPDIpe3BhcmVudD1wYXJlbnQucGFyZW50Tm9kZTtpZihwYXJlbnQmJnBhcmVudC50YWdOYW1lPT09J0ZJR1VSRScmJnBhcmVudC5wYXJlbnROb2RlKXtyZXR1cm4gcGFyZW50LnBhcmVudE5vZGUuZ2V0QXR0cmlidXRlKCdpZCcpO31cbm5vZGUrKzt9fTtwcm90by5zZXRHYWxsZXJ5TmFtZT1mdW5jdGlvbihzb3VyY2Upe3ZhciBkYXRhPXRoaXMuZ2V0QXR0cihzb3VyY2UpO3ZhciBuYW1lPWRhdGEucmVsfHxzb3VyY2UuZ2V0QXR0cmlidXRlKCdkYXRhLXJlbCcpO25hbWU9IW5hbWU/dGhpcy5nZXRHYWxsZXJ5TmFtZShzb3VyY2UpOm5hbWU7bmFtZT0hbmFtZT9PYmplY3Qua2V5cyh0aGlzLmdhbGxlcmllcykubGVuZ3RoKzE6bmFtZTt2YXIgZ2FsbGVyeT10aGlzLmdhbGxlcmllc1tuYW1lXTtnYWxsZXJ5PSFnYWxsZXJ5Pyh0aGlzLmdhbGxlcmllc1tuYW1lXT1bXSk6Z2FsbGVyeTtnYWxsZXJ5Lm5hbWU9bmFtZTtyZXR1cm4gZ2FsbGVyeTt9O3Byb3RvLnNldEdhbGxlcnlGZWF0dXJlcz1mdW5jdGlvbihnYWxsZXJ5LG1lZGlhKXtpZighZ2FsbGVyeS56b29tJiZtZWRpYS50eXBlPT09J2ltYWdlJyl7Z2FsbGVyeS56b29tPXRydWU7fVxuaWYoIWdhbGxlcnkuZG93bmxvYWQmJihtZWRpYS50eXBlPT09J2ltYWdlJ3x8bWVkaWEuZm9ybWF0PT09J2h0bWw1Jykpe2dhbGxlcnkuZG93bmxvYWQ9dHJ1ZTt9fTtwcm90by5zZXRNZWRpYUV2ZW50PWZ1bmN0aW9uKHNvdXJjZSxuYW1lLGluZGV4KXtpZihzb3VyY2UubW9ieExpc3RlbmVyKXtzb3VyY2UucmVtb3ZlRXZlbnRMaXN0ZW5lcignY2xpY2snLHNvdXJjZS5tb2J4TGlzdGVuZXIsZmFsc2UpO31cbnNvdXJjZS5tb2J4TGlzdGVuZXI9dGhpcy5vcGVuLmJpbmQodGhpcyxuYW1lLGluZGV4KTtzb3VyY2UuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLHNvdXJjZS5tb2J4TGlzdGVuZXIsZmFsc2UpO307cHJvdG8ub3Blbj1mdW5jdGlvbihuYW1lLGluZGV4LGV2ZW50KXtpZihldmVudCl7ZXZlbnQucHJldmVudERlZmF1bHQoKTtldmVudC5zdG9wUHJvcGFnYXRpb24oKTt9XG5pZighdGhpcy5HVUlEKXtyZXR1cm4gZmFsc2U7fVxuaWYoIXRoaXMuZ2FsbGVyaWVzLmhhc093blByb3BlcnR5KG5hbWUpKXt1dGlscy5lcnJvcignVGhpcyBnYWxsZXJ5IG5hbWUgOiBcIicrbmFtZSsnXCIsIGRvZXMgbm90IGV4aXN0IScpO3JldHVybiBmYWxzZTt9XG5pZighdGhpcy5nYWxsZXJpZXNbbmFtZV0ubGVuZ3RoKXt1dGlscy5lcnJvcignU29ycnksIG5vIG1lZGlhIHdhcyBmb3VuZCBmb3IgdGhlIGN1cnJlbnQgZ2FsbGVyeS4nKTtyZXR1cm4gZmFsc2U7fVxuaWYoIXRoaXMuZ2FsbGVyaWVzW25hbWVdW2luZGV4XSl7dXRpbHMuZXJyb3IoJ1NvcnJ5LCBubyBtZWRpYSB3YXMgZm91bmQgZm9yIHRoZSBjdXJyZW50IG1lZGlhIGluZGV4OiAnK2luZGV4KTtyZXR1cm4gZmFsc2U7fVxudXRpbHMuZGlzcGF0Y2hFdmVudCh0aGlzLCdtb2R1bG9ib3gnLCdiZWZvcmVPcGVuJyxuYW1lLGluZGV4KTt0aGlzLnNsaWRlcy5pbmRleD1pbmRleDt0aGlzLmdhbGxlcnk9dGhpcy5nYWxsZXJpZXNbbmFtZV07dGhpcy5nYWxsZXJ5Lm5hbWU9bmFtZTt0aGlzLmdhbGxlcnkuaW5kZXg9aW5kZXg7dGhpcy5nYWxsZXJ5LmxvYWRlZD1mYWxzZTt0aGlzLnJlbW92ZUNvbnRlbnQoKTt0aGlzLndyYXBBcm91bmQoKTt0aGlzLmhpZGVTY3JvbGxCYXIoKTt0aGlzLnNldFNsaWRlcigpO3RoaXMuc2V0VGh1bWJzKCk7dGhpcy5zZXRDYXB0aW9uKCk7dGhpcy5zZXRNZWRpYSh0aGlzLm9wdGlvbnMucHJlbG9hZCk7dGhpcy51cGRhdGVNZWRpYUluZm8oKTt0aGlzLnJlcGxhY2VTdGF0ZSgpO3RoaXMuc2V0Q29udHJvbHMoKTt0aGlzLmJpbmRFdmVudHModHJ1ZSk7dGhpcy5zaG93KCk7aWYodGhpcy5vcHRpb25zLnZpZGVvQXV0b1BsYXkpe3RoaXMuYXBwZW5kVmlkZW8oKTt9XG5pZih0aGlzLm9wdGlvbnMuc2xpZGVTaG93QXV0b1BsYXkmJnRoaXMub3B0aW9ucy5jb250cm9scy5pbmRleE9mKCdwbGF5Jyk+LTEmJighdGhpcy5vcHRpb25zLnZpZGVvQXV0b1BsYXl8fHRoaXMuZ2FsbGVyaWVzW25hbWVdW2luZGV4XS50eXBlIT09J3ZpZGVvJykpe3RoaXMuc3RhcnRTbGlkZVNob3coKTt9XG50aGlzLnN0YXRlcy56b29tPWZhbHNlO3RoaXMuc3RhdGVzLm9wZW49dHJ1ZTt9O3Byb3RvLm9wZW5Gcm9tUXVlcnk9ZnVuY3Rpb24oKXt2YXIgcXVlcnk9dGhpcy5nZXRRdWVyeVN0cmluZyh3aW5kb3cubG9jYXRpb24uc2VhcmNoKTtpZihxdWVyeS5oYXNPd25Qcm9wZXJ0eSgnZ3VpZCcpJiZxdWVyeS5oYXNPd25Qcm9wZXJ0eSgnbWlkJykpe3ZhciBvcGVuPXRoaXMub3BlbihkZWNvZGVVUklDb21wb25lbnQocXVlcnkuZ3VpZCksZGVjb2RlVVJJQ29tcG9uZW50KHF1ZXJ5Lm1pZCktMSk7aWYob3Blbj09PWZhbHNlKXt0aGlzLnJlcGxhY2VTdGF0ZSh0cnVlKTt9fX07cHJvdG8uc2hvdz1mdW5jdGlvbigpe3ZhciBob2xkZXI9dGhpcy5ET00uaG9sZGVyLG1ldGhvZD10aGlzLm9wdGlvbnMucmlnaHRUb0xlZnQ/J2FkZCc6J3JlbW92ZSc7aG9sZGVyLnNldEF0dHJpYnV0ZSgnYXJpYS1oaWRkZW4nLGZhbHNlKTt1dGlscy5yZW1vdmVDbGFzcyhob2xkZXIsdGhpcy5wcmUrJy1pZGxlJyk7dXRpbHMucmVtb3ZlQ2xhc3MoaG9sZGVyLHRoaXMucHJlKyctcGFuem9vbScpO3V0aWxzLnJlbW92ZUNsYXNzKGhvbGRlcix0aGlzLnByZSsnLXdpbGwtY2xvc2UnKTt1dGlsc1ttZXRob2QrJ0NsYXNzJ10oaG9sZGVyLHRoaXMucHJlKyctcnRsJyk7dXRpbHMuYWRkQ2xhc3MoaG9sZGVyLHRoaXMucHJlKyctb3BlbicpO307cHJvdG8uY2xvc2U9ZnVuY3Rpb24oZXZlbnQpe2lmKGV2ZW50KXtldmVudC5wcmV2ZW50RGVmYXVsdCgpO31cbnZhciBob2xkZXI9dGhpcy5ET00uaG9sZGVyLGdhbGxlcnk9dGhpcy5nYWxsZXJ5LGluZGV4PWdhbGxlcnk/Z2FsbGVyeS5pbmRleDondW5kZWZpbmVkJyxuYW1lPWdhbGxlcnk/Z2FsbGVyeS5uYW1lOid1bmRlZmluZWQnO3V0aWxzLmRpc3BhdGNoRXZlbnQodGhpcywnbW9kdWxvYm94JywnYmVmb3JlQ2xvc2UnLG5hbWUsaW5kZXgpO2lmKHRoaXMuc3RhdGVzLmZ1bGxTY3JlZW4pe3RoaXMuZXhpdEZ1bGxTY3JlZW4oKTt1dGlscy5yZW1vdmVDbGFzcyhob2xkZXIsdGhpcy5wcmUrJy1mdWxsc2NyZWVuJyk7fVxudGhpcy5zaGFyZSgpO3RoaXMuc3RvcFNsaWRlU2hvdygpO3RoaXMucGF1c2VWaWRlbygpO3RoaXMuYmluZEV2ZW50cyhmYWxzZSk7dGhpcy5yZXBsYWNlU3RhdGUodHJ1ZSk7dGhpcy5oaWRlU2Nyb2xsQmFyKCk7aG9sZGVyLnNldEF0dHJpYnV0ZSgnYXJpYS1oaWRkZW4nLHRydWUpO3V0aWxzLnJlbW92ZUNsYXNzKGhvbGRlcix0aGlzLnByZSsnLW9wZW4nKTt0aGlzLnN0YXRlcy5vcGVuPWZhbHNlO307cHJvdG8uc2V0Q29udHJvbHM9ZnVuY3Rpb24oKXt2YXIgZ2FsbGVyeT10aGlzLmdhbGxlcnksb3B0aW9ucz10aGlzLm9wdGlvbnMsYnV0dG9ucz10aGlzLmJ1dHRvbnM7aWYodGhpcy5ET00uY291bnRlcil7dGhpcy5ET00uY291bnRlci5zdHlsZS5kaXNwbGF5PShnYWxsZXJ5LmluaXRpYWxMZW5ndGg+MSk/Jyc6J25vbmUnO31cbmlmKG9wdGlvbnMuY29udHJvbHMuaW5kZXhPZigncGxheScpPi0xKXtidXR0b25zLnBsYXkuc3R5bGUuZGlzcGxheT0oZ2FsbGVyeS5pbml0aWFsTGVuZ3RoPjEpPycnOidub25lJzt9XG5pZihvcHRpb25zLmNvbnRyb2xzLmluZGV4T2YoJ3pvb20nKT4tMSl7YnV0dG9ucy56b29tLnN0eWxlLmRpc3BsYXk9IWdhbGxlcnkuem9vbT8nbm9uZSc6Jyc7fVxuaWYob3B0aW9ucy5jb250cm9scy5pbmRleE9mKCdkb3dubG9hZCcpPi0xKXtidXR0b25zLmRvd25sb2FkLnN0eWxlLmRpc3BsYXk9IWdhbGxlcnkuZG93bmxvYWQ/J25vbmUnOicnO31cbnRoaXMuc2V0UHJldk5leHRCdXR0b25zKCk7fTtwcm90by5zZXRQcmV2TmV4dEJ1dHRvbnM9ZnVuY3Rpb24oKXtpZih0aGlzLm9wdGlvbnMucHJldk5leHQpe3ZhciBoaWRlPXRoaXMuc2xpZGVyLndpZHRoPDY4MCYmdGhpcy5icm93c2VyLnRvdWNoRGV2aWNlJiYhdGhpcy5vcHRpb25zLnByZXZOZXh0VG91Y2g7dGhpcy5idXR0b25zLnByZXYuc3R5bGUuZGlzcGxheT10aGlzLmJ1dHRvbnMubmV4dC5zdHlsZS5kaXNwbGF5PSh0aGlzLmdhbGxlcnkubGVuZ3RoPjEmJiFoaWRlKT8nJzonbm9uZSc7fX07cHJvdG8uc2V0Q2FwdGlvbj1mdW5jdGlvbigpe3RoaXMuc3RhdGVzLmNhcHRpb249ISghdGhpcy5vcHRpb25zLmNhcHRpb25TbWFsbERldmljZSYmKHRoaXMuc2xpZGVyLndpZHRoPD00ODB8fHRoaXMuc2xpZGVyLmhlaWdodDw9NDgwKSk7dGhpcy5ET00uY2FwdGlvbi5zdHlsZS5kaXNwbGF5PXRoaXMuc3RhdGVzLmNhcHRpb24/Jyc6J25vbmUnO307cHJvdG8uaGlkZVNjcm9sbEJhcj1mdW5jdGlvbigpe2lmKCF0aGlzLm9wdGlvbnMuc2Nyb2xsQmFyKXt2YXIgb3Blbj10aGlzLnN0YXRlcy5vcGVuLHNjcm9sbEJhcj1vcGVuPT09J3VuZGVmaW5lZCd8fCFvcGVuP3RydWU6ZmFsc2U7ZG9jdW1lbnQuYm9keS5zdHlsZS5vdmVyZmxvdz1kb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUub3ZlcmZsb3c9c2Nyb2xsQmFyPydoaWRkZW4nOicnO319O3Byb3RvLmJpbmRFdmVudHM9ZnVuY3Rpb24oYmluZCl7dmFyIHdpbj13aW5kb3csZG9jPWRvY3VtZW50LG9wdD10aGlzLm9wdGlvbnMsaG9sZGVyPXRoaXMuRE9NLmhvbGRlcixidXR0b25zPXRoaXMuYnV0dG9ucyxzY3JvbGxNZXRob2Q7Zm9yKHZhciB0eXBlIGluIGJ1dHRvbnMpe2lmKGJ1dHRvbnMuaGFzT3duUHJvcGVydHkodHlwZSkpe3ZhciBET009KHR5cGUhPT0nc2hhcmUnKT9idXR0b25zW3R5cGVdOndpbjt1dGlscy5oYW5kbGVFdmVudHModGhpcyxET00sWydjbGljaycsJ3RvdWNoZW5kJ10sYnV0dG9uc1t0eXBlXS5ldmVudCxiaW5kKTt9fVxudXRpbHMuaGFuZGxlRXZlbnRzKHRoaXMsaG9sZGVyLHRoaXMuZHJhZ0V2ZW50cy5zdGFydCwndG91Y2hTdGFydCcsYmluZCk7dXRpbHMuaGFuZGxlRXZlbnRzKHRoaXMsd2luLFsna2V5ZG93biddLCdrZXlEb3duJyxiaW5kKTt1dGlscy5oYW5kbGVFdmVudHModGhpcyx3aW4sWydyZXNpemUnLCdvcmllbnRhdGlvbmNoYW5nZSddLCdyZXNpemUnLGJpbmQpO3V0aWxzLmhhbmRsZUV2ZW50cyh0aGlzLGhvbGRlcixbJ3RyYW5zaXRpb25lbmQnLCd3ZWJraXRUcmFuc2l0aW9uRW5kJywnb1RyYW5zaXRpb25FbmQnLCdvdHJhbnNpdGlvbmVuZCcsJ01TVHJhbnNpdGlvbkVuZCddLCdvcGVuZWQnKTt1dGlscy5oYW5kbGVFdmVudHModGhpcyxob2xkZXIsWyd0b3VjaGVuZCddLCdkaXNhYmxlWm9vbScsYmluZCk7aWYodGhpcy5icm93c2VyLmZ1bGxTY3JlZW4pe3V0aWxzLmhhbmRsZUV2ZW50cyh0aGlzLGRvYyxbdGhpcy5icm93c2VyLmZ1bGxTY3JlZW4uY2hhbmdlXSwndG9nZ2xlRnVsbFNjcmVlbicsYmluZCk7fVxuaWYob3B0Lmhpc3Rvcnkpe3V0aWxzLmhhbmRsZUV2ZW50cyh0aGlzLHdpbixbJ21vdXNlb3V0J10sJ21vdXNlT3V0JyxiaW5kKTt9XG5pZihvcHQudGltZVRvSWRsZT4wKXt1dGlscy5oYW5kbGVFdmVudHModGhpcyxob2xkZXIsWydtb3VzZW1vdmUnXSwnbW91c2VNb3ZlJyxiaW5kKTt9XG5pZighb3B0LmNvbnRleHRNZW51KXt1dGlscy5oYW5kbGVFdmVudHModGhpcyxob2xkZXIsWydjb250ZXh0bWVudSddLCdjb250ZXh0TWVudScsYmluZCk7fVxuaWYoIW9wdC5tb3VzZVdoZWVsKXt0aGlzLmRpc2FibGVTY3JvbGwoYmluZCk7fVxuaWYob3B0LnNjcm9sbFRvWm9vbSl7c2Nyb2xsTWV0aG9kPSdzY3JvbGxUb1pvb20nO31cbmVsc2UgaWYob3B0LnNjcm9sbFRvTmF2KXtzY3JvbGxNZXRob2Q9J3Njcm9sbFRvTmF2Jzt9XG5lbHNlIGlmKG9wdC5zY3JvbGxUb0Nsb3NlKXtzY3JvbGxNZXRob2Q9J3Njcm9sbFRvQ2xvc2UnO31cbmlmKHNjcm9sbE1ldGhvZCl7dXRpbHMuaGFuZGxlRXZlbnRzKHRoaXMsaG9sZGVyLFt0aGlzLmJyb3dzZXIubW91c2VXaGVlbF0sc2Nyb2xsTWV0aG9kLGJpbmQpO319O3Byb3RvLm9wZW5lZD1mdW5jdGlvbihldmVudCl7aWYoZXZlbnQucHJvcGVydHlOYW1lPT09J3Zpc2liaWxpdHknJiZldmVudC50YXJnZXQ9PT10aGlzLkRPTS5ob2xkZXIpe3ZhciBuYW1lPXRoaXMuZ2FsbGVyeS5uYW1lLGluZGV4PXRoaXMuZ2FsbGVyeS5pbmRleDtpZighdGhpcy5zdGF0ZXMub3Blbil7dGhpcy5yZW1vdmVDb250ZW50KCk7dXRpbHMuZGlzcGF0Y2hFdmVudCh0aGlzLCdtb2R1bG9ib3gnLCdhZnRlckNsb3NlJyxuYW1lLGluZGV4KTt9ZWxzZXt1dGlscy5kaXNwYXRjaEV2ZW50KHRoaXMsJ21vZHVsb2JveCcsJ2FmdGVyT3BlbicsbmFtZSxpbmRleCk7fX19O3Byb3RvLm1vdXNlT3V0PWZ1bmN0aW9uKGV2ZW50KXt2YXIgZT1ldmVudD9ldmVudDp3aW5kb3cuZXZlbnQsZnJvbT1lLnJlbGF0ZWRUYXJnZXR8fGUudG9FbGVtZW50O2lmKCFmcm9tfHxmcm9tLm5vZGVOYW1lPT09J0hUTUwnKXt0aGlzLnJlcGxhY2VTdGF0ZSgpO319O3Byb3RvLm1vdXNlTW92ZT1mdW5jdGlvbigpe3ZhciBob2xkZXI9dGhpcy5ET00uaG9sZGVyLGlkbGVDbGFzcz10aGlzLnByZSsnLWlkbGUnO2NsZWFyVGltZW91dCh0aGlzLnN0YXRlcy5pZGxlKTt0aGlzLnN0YXRlcy5pZGxlPXNldFRpbWVvdXQoZnVuY3Rpb24oKXtpZighdXRpbHMuaGFzQ2xhc3MoaG9sZGVyLHRoaXMucHJlKyctb3Blbi10b29sdGlwJykpe3V0aWxzLmFkZENsYXNzKGhvbGRlcixpZGxlQ2xhc3MpO319LmJpbmQodGhpcyksdGhpcy5vcHRpb25zLnRpbWVUb0lkbGUpO3V0aWxzLnJlbW92ZUNsYXNzKGhvbGRlcixpZGxlQ2xhc3MpO307cHJvdG8uY29udGV4dE1lbnU9ZnVuY3Rpb24oZXZlbnQpe3ZhciB0YXJnZXQ9ZXZlbnQudGFyZ2V0LHRhZ05hbWU9dGFyZ2V0LnRhZ05hbWUsY2xhc3NOYW1lPXRhcmdldC5jbGFzc05hbWU7aWYodGFnTmFtZT09PSdJTUcnfHx0YWdOYW1lPT09J1ZJREVPJ3x8Y2xhc3NOYW1lLmluZGV4T2YodGhpcy5wcmUrJy12aWRlbycpPi0xfHxjbGFzc05hbWUuaW5kZXhPZih0aGlzLnByZSsnLXRodW1iLWJnJyk+LTF8fGNsYXNzTmFtZT09PXRoaXMucHJlKyctdGh1bWInKXtldmVudC5wcmV2ZW50RGVmYXVsdCgpO319O3Byb3RvLmRpc2FibGVTY3JvbGw9ZnVuY3Rpb24oYmluZCl7dmFyIGRvYz1kb2N1bWVudCx3aW49d2luZG93O3ZhciBwcmV2ZW50PWZ1bmN0aW9uKGUpe2lmKCF0aGlzLmlzRWwoZSkpe3JldHVybjt9XG5lPWV8fHdpbi5ldmVudDtpZihlLnByZXZlbnREZWZhdWx0KXtlLnByZXZlbnREZWZhdWx0KCk7ZS5yZXR1cm5WYWx1ZT1mYWxzZTtyZXR1cm4gZmFsc2U7fX07d2luLm9ud2hlZWw9d2luLm9udG91Y2htb3ZlPXdpbi5vbm1vdXNld2hlZWw9ZG9jLm9ubW91c2V3aGVlbD1kb2Mub25tb3VzZXdoZWVsPWJpbmQ/cHJldmVudC5iaW5kKHRoaXMpOm51bGw7fTtwcm90by5zY3JvbGxUb1pvb209ZnVuY3Rpb24oZXZlbnQpe2lmKCF0aGlzLmlzRWwoZXZlbnQpKXtyZXR1cm47fVxudmFyIGRhdGE9dGhpcy5ub3JtYWxpemVXaGVlbChldmVudCk7aWYoZGF0YSYmZGF0YS5kZWx0YVkpe3ZhciBjZWxsPXRoaXMuZ2V0Q2VsbCgpLHNjYWxlPWNlbGwuYXR0cmFjdGlvbi5zfHxjZWxsLnBvc2l0aW9uLnM7c2NhbGU9TWF0aC5taW4odGhpcy5vcHRpb25zLm1heFpvb20sTWF0aC5tYXgoMSxzY2FsZS1NYXRoLmFicyhkYXRhLmRlbHRhWSkvZGF0YS5kZWx0YVkpKTt0aGlzLnN0b3BTbGlkZVNob3coKTt0aGlzLnpvb21UbyhldmVudC5jbGllbnRYLGV2ZW50LmNsaWVudFksTWF0aC5yb3VuZChzY2FsZSoxMCkvMTApO319O3Byb3RvLnNjcm9sbFRvTmF2PWZ1bmN0aW9uKGV2ZW50KXtpZighdGhpcy5pc0VsKGV2ZW50KSl7cmV0dXJuO31cbnZhciBkYXRhPXRoaXMubm9ybWFsaXplV2hlZWwoZXZlbnQpO2lmKGRhdGEmJmRhdGEuZGVsdGEpe3RoaXNbZGF0YS5kZWx0YSp0aGlzLmlzUlRMKCk8MD8ncHJldic6J25leHQnXSgpO319O3Byb3RvLnNjcm9sbFRvQ2xvc2U9ZnVuY3Rpb24oZXZlbnQpe2lmKCF0aGlzLmlzRWwoZXZlbnQpKXtyZXR1cm47fVxuZXZlbnQucHJldmVudERlZmF1bHQoKTt0aGlzLmNsb3NlKCk7fTtwcm90by5kaXNhYmxlWm9vbT1mdW5jdGlvbihldmVudCl7dmFyIG5vZGU9ZXZlbnQudGFyZ2V0O3doaWxlKG5vZGUpe2lmKFsnVklERU8nLCdJTlBVVCcsJ0EnXS5pbmRleE9mKG5vZGUudGFnTmFtZSk+LTEpe3JldHVybjt9XG5ub2RlPW5vZGUucGFyZW50RWxlbWVudDt9XG5ldmVudC5wcmV2ZW50RGVmYXVsdCgpO307cHJvdG8ucmVzaXplPWZ1bmN0aW9uKGV2ZW50KXt0aGlzLkRPTS50b3BCYXIuaGVpZ2h0PXRoaXMuRE9NLnRvcEJhci5jbGllbnRIZWlnaHQ7dGhpcy5zaGFyZSgpO3RoaXMuc2V0U2xpZGVyKCk7dGhpcy5zZXRUaHVtYnNQb3NpdGlvbigpO3RoaXMuc2V0Q2FwdGlvbigpO3RoaXMucmVzaXplTWVkaWEoKTt0aGlzLnVwZGF0ZU1lZGlhSW5mbygpO3RoaXMuc2V0UHJldk5leHRCdXR0b25zKCk7dGhpcy5zdGF0ZXMuem9vbT1mYWxzZTt1dGlscy5yZW1vdmVDbGFzcyh0aGlzLkRPTS5ob2xkZXIsdGhpcy5wcmUrJy1wYW56b29tJyk7dXRpbHMuZGlzcGF0Y2hFdmVudCh0aGlzLCdtb2R1bG9ib3gnLCdyZXNpemUnLGV2ZW50KTt9O3Byb3RvLnJlc2l6ZU1lZGlhPWZ1bmN0aW9uKCl7dmFyIHNsaWRlcz10aGlzLnNsaWRlcztmb3IodmFyIGk9MDtpPHNsaWRlcy5sZW5ndGg7aSsrKXtpZighdGhpcy5nYWxsZXJ5KXticmVhazt9XG52YXIgbWVkaWE9dGhpcy5nYWxsZXJ5W3NsaWRlc1tpXS5tZWRpYV07aWYobWVkaWEmJigobWVkaWEuZG9tJiZtZWRpYS5kb20ubG9hZGVkKXx8KG1lZGlhLmRvbSYmWyd2aWRlbycsJ2lmcmFtZScsJ0hUTUwnXS5pbmRleE9mKG1lZGlhLnR5cGUpPi0xKSkpe3RoaXMuc2V0TWVkaWFTaXplKG1lZGlhLHNsaWRlc1tpXSk7fX19O3Byb3RvLmlzRWw9ZnVuY3Rpb24oZXZlbnQpe3ZhciBuYW1lPWV2ZW50LnRhcmdldC5jbGFzc05hbWU7bmFtZT10eXBlb2YgbmFtZT09PSdzdHJpbmcnP25hbWU6bmFtZS5iYXNlVmFsO3JldHVybiBuYW1lLmluZGV4T2YodGhpcy5wcmUpPi0xO307cHJvdG8uaXNab29tYWJsZT1mdW5jdGlvbigpe3ZhciBtZWRpYT10aGlzLmdldE1lZGlhKCksem9vbT1mYWxzZTtpZihtZWRpYS50eXBlPT09J2ltYWdlJyYmbWVkaWEuZG9tJiZtZWRpYS5kb20uc2l6ZSYmbWVkaWEuZG9tLnNpemUuc2NhbGU+MSl7em9vbT10cnVlO31cbnRoaXMuRE9NLmhvbGRlci5zZXRBdHRyaWJ1dGUoJ2RhdGEtem9vbScsem9vbSk7cmV0dXJuIHpvb207fTtwcm90by5pc0Rvd25sb2FkYWJsZT1mdW5jdGlvbigpe3ZhciBtZWRpYT10aGlzLmdldE1lZGlhKCksZG93bmxvYWQ9dHJ1ZTtpZihtZWRpYS50eXBlIT09J2ltYWdlJyYmbWVkaWEuZm9ybWF0IT09J2h0bWw1Jyl7ZG93bmxvYWQ9ZmFsc2U7fVxudGhpcy5ET00uaG9sZGVyLnNldEF0dHJpYnV0ZSgnZGF0YS1kb3dubG9hZCcsZG93bmxvYWQpO3JldHVybiBkb3dubG9hZDt9O3Byb3RvLmlzUlRMPWZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMub3B0aW9ucy5yaWdodFRvTGVmdD8tMToxO307cHJvdG8uYWRkQXR0cj1mdW5jdGlvbihlbCxhdHRycyl7dmFyIGNhY2hlSUQ7aWYodHlwZW9mIGVsW3RoaXMuZXhwYW5kb109PT0ndW5kZWZpbmVkJyl7Y2FjaGVJRD10aGlzLmNhY2hlLnVpZCsrO2VsW3RoaXMuZXhwYW5kb109Y2FjaGVJRDt0aGlzLmNhY2hlW2NhY2hlSURdPXt9O31lbHNle2NhY2hlSUQ9ZWxbdGhpcy5leHBhbmRvXTt9XG5mb3IodmFyIGF0dHIgaW4gYXR0cnMpe2lmKGF0dHJzLmhhc093blByb3BlcnR5KGF0dHIpKXt0aGlzLmNhY2hlW2NhY2hlSURdW2F0dHJdPWF0dHJzW2F0dHJdO319fTtwcm90by5nZXRBdHRyPWZ1bmN0aW9uKGVsKXtyZXR1cm4gdGhpcy5jYWNoZVtlbFt0aGlzLmV4cGFuZG9dXXx8e307fTtwcm90by5nZXRUaHVtYkhlaWdodD1mdW5jdGlvbigpe3ZhciB0aHVtYj10aGlzLnRodW1icztyZXR1cm4gdGh1bWIuaGVpZ2h0PjAmJnRodW1iLndpZHRoPjA/dGh1bWIuaGVpZ2h0K01hdGgubWluKDEwLHRodW1iLmd1dHRlcikqMjowO307cHJvdG8uZ2V0TWVkaWE9ZnVuY3Rpb24oKXt2YXIgZ2FsbGVyeT10aGlzLmdhbGxlcnk7cmV0dXJuIGdhbGxlcnk/Z2FsbGVyeVtnYWxsZXJ5LmluZGV4XTpudWxsO307cHJvdG8uZ2V0Q2VsbD1mdW5jdGlvbigpe3ZhciBzbGlkZXM9dGhpcy5zbGlkZXMsaW5kZXg9dXRpbHMubW9kdWxvKHNsaWRlcy5sZW5ndGgsc2xpZGVzLmluZGV4KTtyZXR1cm4gdGhpcy5jZWxsc1tpbmRleF07fTtwcm90by5yZW1vdmVDb250ZW50PWZ1bmN0aW9uKCl7Zm9yKHZhciBpPTA7aTx0aGlzLnNsaWRlcy5sZW5ndGg7aSsrKXt2YXIgc2xpZGU9dGhpcy5zbGlkZXNbaV07dGhpcy51bmxvYWRNZWRpYShzbGlkZSk7dGhpcy5yZW1vdmVNZWRpYShzbGlkZSk7c2xpZGUuaW5kZXg9c2xpZGUubWVkaWE9bnVsbDt9XG50aGlzLnJlbW92ZU1lZGlhKHRoaXMuRE9NLnRodW1ic0hvbGRlcik7fTtwcm90by5nZXRRdWVyeVN0cmluZz1mdW5jdGlvbihzZWFyY2gpe3ZhciBwYXJhbXM9e307c2VhcmNoLnN1YnN0cigxKS5zcGxpdCgnJicpLmZvckVhY2goZnVuY3Rpb24ocGFyYW0pe3BhcmFtPXBhcmFtLnNwbGl0KCc9Jyk7cGFyYW1zW2RlY29kZVVSSUNvbXBvbmVudChwYXJhbVswXSldPXBhcmFtLmxlbmd0aD4xP2RlY29kZVVSSUNvbXBvbmVudChwYXJhbVsxXSk6Jyc7fSk7cmV0dXJuIHBhcmFtczt9O3Byb3RvLnNldFF1ZXJ5U3RyaW5nPWZ1bmN0aW9uKGtleSl7dmFyIHNlYXJjaD13aW5kb3cubG9jYXRpb24uc2VhcmNoLHF1ZXJ5PXRoaXMuZ2V0UXVlcnlTdHJpbmcoc2VhcmNoKTtzZWFyY2g9ZGVjb2RlVVJJKHNlYXJjaCk7Zm9yKHZhciBwcm9wIGluIGtleSl7aWYoa2V5Lmhhc093blByb3BlcnR5KHByb3ApKXt2YXIgcmVwbGFjZT1lbmNvZGVVUklDb21wb25lbnQoa2V5W3Byb3BdKTtpZihxdWVyeS5oYXNPd25Qcm9wZXJ0eShwcm9wKSl7dmFyIHZhbHVlPXF1ZXJ5W3Byb3BdO2lmKCFyZXBsYWNlKXtzZWFyY2g9c2VhcmNoLnJlcGxhY2UoJyYnK3Byb3ArJz0nK3ZhbHVlLCcnKTtzZWFyY2g9c2VhcmNoLnJlcGxhY2UocHJvcCsnPScrdmFsdWUsJycpO31lbHNle3NlYXJjaD1zZWFyY2gucmVwbGFjZShwcm9wKyc9Jyt2YWx1ZSxwcm9wKyc9JytyZXBsYWNlKTt9fWVsc2V7aWYocmVwbGFjZSl7c2VhcmNoPXNlYXJjaCsoIXNlYXJjaD8nPyc6JyYnKStwcm9wKyc9JytyZXBsYWNlO31lbHNle3NlYXJjaD1zZWFyY2gucmVwbGFjZShwcm9wKyc9JywnJyk7fX19fVxudmFyIGJhc2U9W2xvY2F0aW9uLnByb3RvY29sLCcvLycsbG9jYXRpb24uaG9zdCxsb2NhdGlvbi5wYXRobmFtZV0uam9pbignJyk7c2VhcmNoPSFzZWFyY2guc3Vic3RyKDEpP3NlYXJjaC5zdWJzdHIoMSk6c2VhcmNoO3JldHVybiBlbmNvZGVVUkkoYmFzZStzZWFyY2gpO307cHJvdG8ucmVwbGFjZVN0YXRlPWZ1bmN0aW9uKHJlbW92ZSl7aWYoKHRoaXMub3B0aW9ucy5oaXN0b3J5fHxyZW1vdmUpJiZ0aGlzLmJyb3dzZXIucHVzaFN0YXRlJiYhdGhpcy5zdGF0ZXMucHVzaCl7dmFyIHByZXZEYXRhPXdpbmRvdy5oaXN0b3J5LnN0YXRlLGRhdGE9e2d1aWQ6IXJlbW92ZT90aGlzLmdhbGxlcnkubmFtZTonJyxtaWQ6IXJlbW92ZT91dGlscy5tb2R1bG8odGhpcy5nYWxsZXJ5LmluaXRpYWxMZW5ndGgsdGhpcy5nYWxsZXJ5LmluZGV4KSsxOicnfTtpZighcHJldkRhdGF8fHByZXZEYXRhLm1pZCE9PWRhdGEubWlkKXt2YXIgc2VhcmNoPXRoaXMuc2V0UXVlcnlTdHJpbmcoZGF0YSk7dHJ5e3dpbmRvdy5oaXN0b3J5LnJlcGxhY2VTdGF0ZShkYXRhLCcnLHNlYXJjaCk7fWNhdGNoKGVycm9yKXt0aGlzLm9wdGlvbnMuaGlzdG9yeT1mYWxzZTt1dGlscy5lcnJvcignU2VjdXJpdHlFcnJvcjogQSBoaXN0b3J5IHN0YXRlIG9iamVjdCB3aXRoIG9yaWdpbiBcXCdudWxsXFwnIGNhbm5vdCBiZSBjcmVhdGVkLiBQbGVhc2UgcnVuIHRoZSBzY3JpcHQgb24gYSBzZXJ2ZXIuJyk7fX19XG50aGlzLnN0YXRlcy5wdXNoPWZhbHNlO307cHJvdG8ubm9ybWFsaXplV2hlZWw9ZnVuY3Rpb24oZXZlbnQpe3ZhciBldj1ldmVudHx8d2luZG93LmV2ZW50LGRhdGE9bnVsbCxkZWx0YVgsZGVsdGFZLGRlbHRhO2V2ZW50LnByZXZlbnREZWZhdWx0KCk7aWYoJ2RldGFpbCdpbiBldil7ZGVsdGFZPWV2LmRldGFpbCotMTt9XG5pZignd2hlZWxEZWx0YSdpbiBldil7ZGVsdGFZPWV2LndoZWVsRGVsdGEqLTE7fVxuaWYoJ3doZWVsRGVsdGFZJ2luIGV2KXtkZWx0YVk9ZXYud2hlZWxEZWx0YVkqLTE7fVxuaWYoJ3doZWVsRGVsdGFYJ2luIGV2KXtkZWx0YVg9ZXYud2hlZWxEZWx0YVgqLTE7fVxuaWYoJ2RlbHRhWSdpbiBldil7ZGVsdGFZPWV2LmRlbHRhWTt9XG5pZignZGVsdGFYJ2luIGV2KXtkZWx0YVg9ZXYuZGVsdGFYKi0xO31cbmlmKGV2LmRlbHRhTW9kZT09PTEpe2RlbHRhWCo9NDA7ZGVsdGFZKj00MDt9ZWxzZSBpZihldi5kZWx0YU1vZGU9PT0yKXtkZWx0YVgqPTEwMDtkZWx0YVkqPTEwMDt9XG5kZWx0YT1NYXRoLmFicyhkZWx0YVgpPk1hdGguYWJzKGRlbHRhWSk/ZGVsdGFYOmRlbHRhWTtkZWx0YT1NYXRoLm1pbigxMDAsTWF0aC5tYXgoLTEwMCxkZWx0YSkpO2lmKE1hdGguYWJzKGRlbHRhKTx0aGlzLm9wdGlvbnMuc2Nyb2xsU2Vuc2l0aXZpdHkpe3RoaXMuc3RhdGVzLnByZXZEZWx0YT1kZWx0YTtyZXR1cm4gZGF0YTt9XG52YXIgdGltZT0rbmV3IERhdGUoKTtpZihNYXRoLmFicyhkZWx0YSk+TWF0aC5hYnModGhpcy5zdGF0ZXMucHJldkRlbHRhKXx8dGltZS10aGlzLnN0YXRlcy5wcmV2U2Nyb2xsPjYwKXtkYXRhPXsnZGVsdGFYJzpkZWx0YVgsJ2RlbHRhWSc6ZGVsdGFZLCdkZWx0YSc6ZGVsdGF9O31cbnRoaXMuc3RhdGVzLnByZXZEZWx0YT1kZWx0YTt0aGlzLnN0YXRlcy5wcmV2U2Nyb2xsPXRpbWU7cmV0dXJuIGRhdGE7fTtwcm90by5zaGFyZT1mdW5jdGlvbihldmVudCl7aWYoZXZlbnQmJmV2ZW50LnRhcmdldC50YWdOYW1lPT09J1ZJREVPJyl7cmV0dXJuO31cbnZhciBob2xkZXI9dGhpcy5ET00uaG9sZGVyLGNsYXNzTmFtZT10aGlzLnByZSsnLW9wZW4tdG9vbHRpcCcsZWxlbWVudD1ldmVudD9ldmVudC50YXJnZXQuY2xhc3NOYW1lOm51bGwsbWV0aG9kPXV0aWxzLmhhc0NsYXNzKGhvbGRlcixjbGFzc05hbWUpPydyZW1vdmUnOidhZGQnO2lmKChtZXRob2Q9PT0ncmVtb3ZlJyYmKGVsZW1lbnQhPT10aGlzLnByZSsnLXNoYXJlJ3x8IWV2ZW50KSl8fGVsZW1lbnQ9PT10aGlzLnByZSsnLXNoYXJlJyl7aWYobWV0aG9kPT09J2FkZCcpe3RoaXMuc2V0U2hhcmVUb29sdGlwKCk7fVxudXRpbHNbbWV0aG9kKydDbGFzcyddKGhvbGRlcixjbGFzc05hbWUpO319O3Byb3RvLnNoYXJlT249ZnVuY3Rpb24oZXZlbnQpe3ZhciB0eXBlPWV2ZW50LnRhcmdldC5hY3Rpb24sZ2FsbGVyeT10aGlzLmdhbGxlcnksbWVkaWE9dGhpcy5nZXRNZWRpYSgpLGltYWdlPW1lZGlhLnR5cGU9PT0naW1hZ2UnP21lZGlhLnNyYzptZWRpYS5wb3N0ZXIsdXJsPXRoaXMuc29jaWFsTWVkaWFbdHlwZV0sdXJpO2lmKHVybCl7aWYodGhpcy5vcHRpb25zLnNoYXJlZFVybD09PSdwYWdlJyl7dXJpPVtsb2NhdGlvbi5wcm90b2NvbCwnLy8nLGxvY2F0aW9uLmhvc3QsbG9jYXRpb24ucGF0aG5hbWVdLmpvaW4oJycpO31lbHNlIGlmKHRoaXMub3B0aW9ucy5zaGFyZWRVcmw9PT0nZGVlcGxpbmsnfHxbJ2lmcmFtZScsJ0hUTUwnXS5pbmRleE9mKG1lZGlhLnR5cGUpPi0xKXt1cmk9dGhpcy5zZXRRdWVyeVN0cmluZyh7Z3VpZDpnYWxsZXJ5Lm5hbWUsbWlkOmdhbGxlcnkuaW5kZXgrMX0pO31lbHNle3VyaT1tZWRpYS5zcmMucmVwbGFjZSgvXFxzL2csJycpLnNwbGl0KCcsJylbMF07aWYobWVkaWEudHlwZT09PSd2aWRlbycmJm1lZGlhLmZvcm1hdCE9PSdodG1sNScpe3VyaT1tZWRpYS5zaGFyZTt9fVxudmFyIGxpbms9dXRpbHMuY3JlYXRlRWwoJ2EnKTtsaW5rLmhyZWY9aW1hZ2U7aW1hZ2U9bGluay5ocmVmO2xpbmsuaHJlZj11cmk7dXJpPWxpbmsuaHJlZjt2YXIgdG1wPXV0aWxzLmNyZWF0ZUVsKCdkaXYnKTt0bXAuaW5uZXJIVE1MPW1lZGlhLmNhcHRpb247dmFyIHRleHQ9KHRtcC50ZXh0Q29udGVudHx8dG1wLmlubmVyVGV4dCkucmVwbGFjZSgvXFxzKy9nLCcgJykudHJpbSgpfHwnJzt1cmw9dXJsLnJlcGxhY2UoJ1t1cmxdJyxlbmNvZGVVUklDb21wb25lbnQodXJpKSkucmVwbGFjZSgnW2ltYWdlXScsZW5jb2RlVVJJQ29tcG9uZW50KGltYWdlKSkucmVwbGFjZSgnW3RleHRdJyxlbmNvZGVVUklDb21wb25lbnQodGV4dHx8ZG9jdW1lbnQudGl0bGUpKTtpZih1cmwpe3ZhciBsZWZ0PU1hdGgucm91bmQod2luZG93LnNjcmVlblgrKHdpbmRvdy5vdXRlcldpZHRoLTYyNikvMiksdG9wPU1hdGgucm91bmQod2luZG93LnNjcmVlblkrKHdpbmRvdy5vdXRlckhlaWdodC00MzYpLzIpO3dpbmRvdy5vcGVuKHVybCx0aGlzLnByZSsnX3NoYXJlJywnc3RhdHVzPTAscmVzaXphYmxlPTEsbG9jYXRpb249MSx0b29sYmFyPTAsd2lkdGg9NjI2LGhlaWdodD00MzYsdG9wPScrdG9wKycsbGVmdD0nK2xlZnQpO319ZWxzZXt1dGlscy5lcnJvcignVGhpcyBzb2NpYWwgc2hhcmUgbWVkaWEgZG9lcyBub3QgZXhpc3QnKTt9XG5yZXR1cm4gZmFsc2U7fTtwcm90by5zZXRTaGFyZVRvb2x0aXA9ZnVuY3Rpb24oKXtpZih0aGlzLm9wdGlvbnMuY29udHJvbHMuaW5kZXhPZignc2hhcmUnKT4tMSl7dmFyIGF0dHJpYnV0ZT0ncmlnaHQnLHRvb2x0aXA9dGhpcy5ET00uc2hhcmVUb29sdGlwLHdpZHRoPXRvb2x0aXAuY2xpZW50V2lkdGgsYnV0dG9uPXRoaXMuYnV0dG9ucy5zaGFyZS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSxwb3NpdGlvbj1idXR0b24ubGVmdC13aWR0aCtidXR0b24ud2lkdGgvMisyMDtpZihwb3NpdGlvbjwwKXthdHRyaWJ1dGU9J2xlZnQnO3Bvc2l0aW9uPWJ1dHRvbi5sZWZ0K2J1dHRvbi53aWR0aC8yLTIwO31cbnRvb2x0aXAuc2V0QXR0cmlidXRlKCdkYXRhLXBvc2l0aW9uJyxhdHRyaWJ1dGUpO3Rvb2x0aXAuc3R5bGUudG9wPXRoaXMuRE9NLnRvcEJhci5oZWlnaHQrNisncHgnO3Rvb2x0aXAuc3R5bGUubGVmdD1wb3NpdGlvbisncHgnO319O3Byb3RvLmRvd25sb2FkPWZ1bmN0aW9uKCl7aWYoIXRoaXMuaXNEb3dubG9hZGFibGUoKSl7cmV0dXJuIGZhbHNlO31cbnZhciBtZWRpYT10aGlzLmdldE1lZGlhKCksdXJsPW1lZGlhLnNyYy5yZXBsYWNlKC9cXHMvZywnJykuc3BsaXQoJywnKVswXSxsaW5rPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKSxncm91cD11cmwuc3BsaXQoJy8nKTtsaW5rLmhyZWY9dXJsO2xpbmsuZG93bmxvYWQ9Z3JvdXAucG9wKCkuc3BsaXQoJz8nKVswXTtsaW5rLnNldEF0dHJpYnV0ZSgndGFyZ2V0JywnX2JsYW5rJyk7ZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChsaW5rKTtsaW5rLmNsaWNrKCk7ZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChsaW5rKTt9O3Byb3RvLmZ1bGxTY3JlZW49ZnVuY3Rpb24oKXt2YXIgZnVsbFNjcmVlbkVsZW1lbnQ9dGhpcy5icm93c2VyLmZ1bGxTY3JlZW4uZWxlbWVudDtpZighZG9jdW1lbnRbZnVsbFNjcmVlbkVsZW1lbnRdKXt0aGlzLnJlcXVlc3RGdWxsU2NyZWVuKCk7fWVsc2V7dGhpcy5leGl0RnVsbFNjcmVlbigpO319O3Byb3RvLnRvZ2dsZUZ1bGxTY3JlZW49ZnVuY3Rpb24oKXt2YXIgaG9sZGVyPXRoaXMuRE9NLmhvbGRlcixmdWxsU2NyZWVuRWxlbWVudD1kb2N1bWVudFt0aGlzLmJyb3dzZXIuZnVsbFNjcmVlbi5lbGVtZW50XTtpZighZnVsbFNjcmVlbkVsZW1lbnQpe3RoaXMuc2hhcmUoKTt0aGlzLnN0YXRlcy5mdWxsU2NyZWVuPWZhbHNlO3V0aWxzLnJlbW92ZUNsYXNzKGhvbGRlcix0aGlzLnByZSsnLWZ1bGxzY3JlZW4nKTt9ZWxzZSBpZihmdWxsU2NyZWVuRWxlbWVudD09PWhvbGRlcil7dGhpcy5zZXRTaGFyZVRvb2x0aXAoKTt0aGlzLnN0YXRlcy5mdWxsU2NyZWVuPXRydWU7dXRpbHMuYWRkQ2xhc3MoaG9sZGVyLHRoaXMucHJlKyctZnVsbHNjcmVlbicpO31cbnRoaXMudmlkZW9GdWxsU2NyZWVuKCk7fTtwcm90by5yZXF1ZXN0RnVsbFNjcmVlbj1mdW5jdGlvbigpe3ZhciByZXF1ZXN0PXRoaXMuYnJvd3Nlci5mdWxsU2NyZWVuLnJlcXVlc3Q7aWYoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50W3JlcXVlc3RdKXt0aGlzLkRPTS5ob2xkZXJbcmVxdWVzdF0oKTt9fTtwcm90by5leGl0RnVsbFNjcmVlbj1mdW5jdGlvbigpe3ZhciBleGl0PXRoaXMuYnJvd3Nlci5mdWxsU2NyZWVuLmV4aXQ7aWYoZG9jdW1lbnRbZXhpdF0pe2RvY3VtZW50W2V4aXRdKCk7fX07cHJvdG8ucGxheT1mdW5jdGlvbigpe2lmKHRoaXMuc3RhdGVzLnBsYXkpe3RoaXMuc3RvcFNsaWRlU2hvdygpO31lbHNle3RoaXMuc3RhcnRTbGlkZVNob3coKTt9fTtwcm90by5zdGFydFNsaWRlU2hvdz1mdW5jdGlvbigpe3ZhciBzdGFydD0wLGdhbGxlcnk9dGhpcy5nYWxsZXJ5LG9wdGlvbnM9dGhpcy5vcHRpb25zLGxvb3A9dGhpcy5zdGF0ZXMubG9vcCxhdXRvU3RvcD1vcHRpb25zLnNsaWRlU2hvd0F1dG9TdG9wLGN5Y2xlPU1hdGgubWF4KDEyMCxvcHRpb25zLnNsaWRlU2hvd0ludGVydmFsKSxjb3VudD1vcHRpb25zLmNvdW50VGltZXIsY2FudmFzPWNvdW50JiZ0aGlzLkRPTS50aW1lcj90aGlzLkRPTS50aW1lci5nZXRDb250ZXh0KCcyZCcpOm51bGw7dmFyIGNvdW50RG93bj0oZnVuY3Rpb24obm93KXtub3c9IW5vdz8rbmV3IERhdGUoKTpub3c7c3RhcnQ9IXN0YXJ0P25vdzpzdGFydDtpZighbG9vcHx8YXV0b1N0b3Ape2lmKGdhbGxlcnkuaW5kZXg9PT1nYWxsZXJ5LmluaXRpYWxMZW5ndGgtMSl7dGhpcy5zdG9wU2xpZGVTaG93KCk7cmV0dXJuO319XG5pZihjb3VudCYmY2FudmFzKXt2YXIgcGVyY2VudD1NYXRoLm1pbigxLChub3ctc3RhcnQrY3ljbGUpL2N5Y2xlLTEpLHJhZGlhbnM9cGVyY2VudCozNjAqKE1hdGguUEkvMTgwKTtjYW52YXMuY2xlYXJSZWN0KDAsMCw0OCw0OCk7dGhpcy50aW1lclByb2dyZXNzKGNhbnZhcyxvcHRpb25zLmNvdW50VGltZXJCZywxMDApO3RoaXMudGltZXJQcm9ncmVzcyhjYW52YXMsb3B0aW9ucy5jb3VudFRpbWVyQ29sb3IscmFkaWFucyk7fVxuaWYobm93Pj1zdGFydCtjeWNsZSl7c3RhcnQ9bm93O3RoaXMuc2xpZGVUbyh0aGlzLnNsaWRlcy5pbmRleCsxLHRydWUpO31cbnRoaXMudGltZXI9cmVxdWVzdEFuaW1hdGlvbkZyYW1lKGNvdW50RG93bik7fSkuYmluZCh0aGlzKTt1dGlscy5hZGRDbGFzcyh0aGlzLkRPTS5ob2xkZXIsdGhpcy5wcmUrJy1hdXRvcGxheScpO3RoaXMuc3RhdGVzLnBsYXk9dHJ1ZTt0aGlzLnRpbWVyPXJlcXVlc3RBbmltYXRpb25GcmFtZShjb3VudERvd24pO307cHJvdG8uc3RvcFNsaWRlU2hvdz1mdW5jdGlvbigpe2NhbmNlbEFuaW1hdGlvbkZyYW1lKHRoaXMudGltZXIpO3V0aWxzLnJlbW92ZUNsYXNzKHRoaXMuRE9NLmhvbGRlcix0aGlzLnByZSsnLWF1dG9wbGF5Jyk7dGhpcy5zdGF0ZXMucGxheT1mYWxzZTt9O3Byb3RvLnRpbWVyUHJvZ3Jlc3M9ZnVuY3Rpb24oY2FudmFzLGNvbG9yLGVuZCl7dmFyIHN0YXJ0PTEuNSpNYXRoLlBJO2NhbnZhcy5zdHJva2VTdHlsZT1jb2xvcjtjYW52YXMubGluZVdpZHRoPTU7Y2FudmFzLmJlZ2luUGF0aCgpO2NhbnZhcy5hcmMoMjQsMjQsMTgsc3RhcnQsc3RhcnQrZW5kLGZhbHNlKTtjYW52YXMuc3Ryb2tlKCk7fTtwcm90by5hcHBlbmRWaWRlbz1mdW5jdGlvbigpe3ZhciBtZWRpYT10aGlzLmdldE1lZGlhKCk7aWYobWVkaWEudHlwZSE9PSd2aWRlbycpe3JldHVybjt9XG51dGlscy5hZGRDbGFzcyhtZWRpYS5kb20sdGhpcy5wcmUrJy1sb2FkaW5nJyk7dXRpbHMucmVtb3ZlQ2xhc3MobWVkaWEuZG9tLHRoaXMucHJlKyctcGxheWluZycpO2lmKCFtZWRpYS52aWRlbyl7aWYobWVkaWEuZm9ybWF0PT09J2h0bWw1Jyl7bWVkaWEudmlkZW89dXRpbHMuY3JlYXRlRWwoJ3ZpZGVvJyk7bWVkaWEudmlkZW8uc2V0QXR0cmlidXRlKCdjb250cm9scycsJycpO21lZGlhLnZpZGVvLnNldEF0dHJpYnV0ZSgnYXV0b3BsYXknLCcnKTt2YXIgdXJscz1tZWRpYS5zcmMucmVwbGFjZSgvXFxzL2csJycpLnNwbGl0KCcsJyk7Zm9yKHZhciBpPTA7aTx1cmxzLmxlbmd0aDtpKyspe3ZhciBmcmFnPWRvY3VtZW50LmNyZWF0ZURvY3VtZW50RnJhZ21lbnQoKSxzb3VyY2U9dXRpbHMuY3JlYXRlRWwoJ3NvdXJjZScpLHR5cGU9KC9eLitcXC4oW14uXSspJC8pLmV4ZWModXJsc1tpXSk7aWYodHlwZSYmWydtcDQnLCd3ZWJtJywnb2d2J10uaW5kZXhPZih0eXBlWzFdKT4tMSl7c291cmNlLnNyYz11cmxzW2ldO3NvdXJjZS5zZXRBdHRyaWJ1dGUoJ3R5cGUnLCd2aWRlby8nKyh0eXBlWzFdPT09J29ndic/J29nZyc6dHlwZVsxXSkpO2ZyYWcuYXBwZW5kQ2hpbGQoc291cmNlKTt9XG5tZWRpYS52aWRlby5hcHBlbmRDaGlsZChmcmFnKTt9fVxuZWxzZSBpZihtZWRpYS5mb3JtYXQpe21lZGlhLnZpZGVvPXV0aWxzLmNyZWF0ZUVsKCdpZnJhbWUnKTttZWRpYS52aWRlby5zcmM9bWVkaWEuc3JjO21lZGlhLnZpZGVvLnNldEF0dHJpYnV0ZSgnZnJhbWVib3JkZXInLDApO21lZGlhLnZpZGVvLnNldEF0dHJpYnV0ZSgnYWxsb3dmdWxsc2NyZWVuJywnJyk7fVxubWVkaWEudmlkZW8uc2V0QXR0cmlidXRlKCd3aWR0aCcsJzEwMCUnKTttZWRpYS52aWRlby5zZXRBdHRyaWJ1dGUoJ2hlaWdodCcsJzEwMCUnKTt9XG5pZighbWVkaWEuZG9tLmZpcnN0Q2hpbGQpe21lZGlhLmRvbS5hcHBlbmRDaGlsZChtZWRpYS52aWRlbyk7aWYobWVkaWEuZm9ybWF0IT09J2h0bWw1Jyl7bWVkaWEudmlkZW8ubG9hZGVkPWZhbHNlO319XG50aGlzLnBsYXlWaWRlbyhtZWRpYSk7fTtwcm90by5vblZpZGVvTG9hZGVkPWZ1bmN0aW9uKG1lZGlhKXttZWRpYS52aWRlby5sb2FkZWQ9dHJ1ZTt1dGlscy5yZW1vdmVDbGFzcyhtZWRpYS5kb20sdGhpcy5wcmUrJy1sb2FkaW5nJyk7dXRpbHMuYWRkQ2xhc3MobWVkaWEuZG9tLHRoaXMucHJlKyctcGxheWluZycpO3RoaXMuY2xvbmVWaWRlbyhtZWRpYSk7fTtwcm90by5jbG9uZVZpZGVvPWZ1bmN0aW9uKG1lZGlhKXtpZih0aGlzLnN0YXRlcy5sb29wJiZtZWRpYS5mb3JtYXQ9PT0naHRtbDUnKXt2YXIgZ2FsbGVyeT10aGlzLmdhbGxlcnksbGVuZ3RoPWdhbGxlcnkubGVuZ3RoLGluaXRpYWw9Z2FsbGVyeS5pbml0aWFsTGVuZ3RoLGN1cnJlbnQ9dXRpbHMubW9kdWxvKGluaXRpYWwsbWVkaWEuaW5kZXgpO2Zvcih2YXIgaT0wO2k8bGVuZ3RoO2krKyl7dmFyIGluZGV4PXV0aWxzLm1vZHVsbyhpbml0aWFsLGdhbGxlcnlbaV0uaW5kZXgpO2lmKGluZGV4PT09Y3VycmVudCYmZ2FsbGVyeVtpXS5pbmRleCE9PW1lZGlhLmluZGV4KXtnYWxsZXJ5W2ldLnZpZGVvPW1lZGlhLnZpZGVvO319fX07cHJvdG8udmlkZW9GdWxsU2NyZWVuPWZ1bmN0aW9uKCl7dmFyIG1lZGlhPXRoaXMuZ2V0TWVkaWEoKSxmdWxsU2NyZWVuPXRoaXMuc3RhdGVzLmZ1bGxTY3JlZW47aWYobWVkaWEudHlwZT09PSd2aWRlbycmJm1lZGlhLmZvcm1hdCE9PSdodG1sNScmJm1lZGlhLnZpZGVvKXttZWRpYS52aWRlb1tmdWxsU2NyZWVuPydyZW1vdmVBdHRyaWJ1dGUnOidzZXRBdHRyaWJ1dGUnXSgnYWxsb3dmdWxsc2NyZWVuJywnJyk7fX07cHJvdG8ucGxheVZpZGVvPWZ1bmN0aW9uKG1lZGlhKXtpZihtZWRpYS52aWRlby5sb2FkZWQpe21lZGlhLnZpZGVvLmdldENsaWVudFJlY3RzKCk7dXRpbHMucmVtb3ZlQ2xhc3MobWVkaWEuZG9tLHRoaXMucHJlKyctbG9hZGluZycpO3V0aWxzLmFkZENsYXNzKG1lZGlhLmRvbSx0aGlzLnByZSsnLXBsYXlpbmcnKTtpZihtZWRpYS5mb3JtYXQhPT0naHRtbDUnKXtpZihtZWRpYS5wbGF5KXt2YXIgbWVzc2FnZT10eXBlb2YgbWVkaWEucGxheT09PSdvYmplY3QnP0pTT04uc3RyaW5naWZ5KG1lZGlhLnBsYXkpOlN0cmluZyhtZWRpYS5wbGF5KTttZWRpYS52aWRlby5jb250ZW50V2luZG93LnBvc3RNZXNzYWdlKG1lc3NhZ2UsJyonKTt9fWVsc2UgaWYoIW1lZGlhLnZpZGVvLmVycm9yKXtpZih0eXBlb2YgTWVkaWFFbGVtZW50UGxheWVyPT09J2Z1bmN0aW9uJyYmdGhpcy5vcHRpb25zLm1lZGlhZWxlbWVudCl7dmFyIG1lanM9KG1lZGlhLnZpZGVvLnRhZ05hbWU9PT0nVklERU8nKT9tZWRpYS52aWRlbzptZWRpYS52aWRlby5nZXRFbGVtZW50c0J5VGFnTmFtZSgndmlkZW8nKVswXTtpZihtZWpzLnBsYXllcil7bWVqcy5wbGF5ZXIuc2V0Q29udHJvbHNTaXplKCk7fVxubWVqcy5wbGF5KCk7fWVsc2V7bWVkaWEudmlkZW8ucGxheSgpO319fWVsc2V7dmFyIF90aGlzPXRoaXM7aWYodHlwZW9mIE1lZGlhRWxlbWVudFBsYXllcj09PSdmdW5jdGlvbicmJiFtZWRpYS5wbGF5JiZ0aGlzLm9wdGlvbnMubWVkaWFlbGVtZW50JiYhbWVkaWEudmlkZW8ucGxheWVyKXtuZXcgTWVkaWFFbGVtZW50UGxheWVyKG1lZGlhLnZpZGVvLHtmZWF0dXJlczpbJ3BsYXlwYXVzZScsJ3N0b3AnLCdjdXJyZW50JywncHJvZ3Jlc3MnLCdkdXJhdGlvbicsJ3ZvbHVtZScsJ2Z1bGxzY3JlZW4nXSx2aWRlb1ZvbHVtZTonaG9yaXpvbnRhbCcsc3RhcnRWb2x1bWU6MC44LGNsYXNzUHJlZml4OidtZWpzLScsa2V5QWN0aW9uczpbXSxlbmFibGVLZXlib2FyZDpmYWxzZSxpUGFkVXNlTmF0aXZlQ29udHJvbHM6dHJ1ZSxpUGhvbmVVc2VOYXRpdmVDb250cm9sczp0cnVlLEFuZHJvaWRVc2VOYXRpdmVDb250cm9sczp0cnVlLHN1Y2Nlc3M6ZnVuY3Rpb24obWVqcyl7bWVqcy5hZGRFdmVudExpc3RlbmVyKCdsb2FkZWRkYXRhJyxmdW5jdGlvbigpe21lZGlhLnZpZGVvPW1lZGlhLmRvbS5sYXN0Q2hpbGQ7aWYobWVkaWEudmlkZW8pe3ZhciBvZmZTY3JlZW49bWVkaWEudmlkZW8ucHJldmlvdXNTaWJsaW5nO2lmKG9mZlNjcmVlbiYmb2ZmU2NyZWVuLnBhcmVudE5vZGUpe29mZlNjcmVlbi5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKG9mZlNjcmVlbik7fVxuX3RoaXMub25WaWRlb0xvYWRlZChtZWRpYSk7fX0sbWVkaWEsZmFsc2UpO30sZXJyb3I6ZnVuY3Rpb24oKXtfdGhpcy5vblZpZGVvTG9hZGVkKG1lZGlhKTt9fSk7fWVsc2UgaWYoIW1lZGlhLnZpZGVvLm9ubG9hZCl7bWVkaWEudmlkZW8ub25sb2FkPW1lZGlhLnZpZGVvLm9uZXJyb3I9bWVkaWEudmlkZW8ub25sb2FkZWRtZXRhZGF0YT1mdW5jdGlvbigpe2lmKG1lZGlhLmRvbS5maXJzdENoaWxkKXtfdGhpcy5vblZpZGVvTG9hZGVkKG1lZGlhKTtfdGhpcy52aWRlb0Z1bGxTY3JlZW4oKTt9fTttZWRpYS52aWRlby5zcmM9bWVkaWEuc3JjLnJlcGxhY2UoL1xccy9nLCcnKS5zcGxpdCgnLCcpWzBdO319fTtwcm90by5wYXVzZVZpZGVvPWZ1bmN0aW9uKCl7dmFyIG1lZGlhPXRoaXMuZ2V0TWVkaWEoKTtpZihtZWRpYSYmbWVkaWEudHlwZT09PSd2aWRlbycmJm1lZGlhLnZpZGVvKXt1dGlscy5yZW1vdmVDbGFzcyhtZWRpYS5kb20sdGhpcy5wcmUrJy1wbGF5aW5nJyk7aWYoIW1lZGlhLnZpZGVvLmxvYWRlZCl7bWVkaWEuZG9tLmlubmVySFRNTD0nJzt1dGlscy5yZW1vdmVDbGFzcyhtZWRpYS5kb20sdGhpcy5wcmUrJy1sb2FkaW5nJyk7cmV0dXJuO31cbmlmKG1lZGlhLmZvcm1hdD09PSdodG1sNScpe2lmKHR5cGVvZiBNZWRpYUVsZW1lbnRQbGF5ZXI9PT0nZnVuY3Rpb24nJiZ0aGlzLm9wdGlvbnMubWVkaWFlbGVtZW50KXt2YXIgbWVqcz0obWVkaWEudmlkZW8udGFnTmFtZT09PSdWSURFTycpP21lZGlhLnZpZGVvOm1lZGlhLnZpZGVvLmdldEVsZW1lbnRzQnlUYWdOYW1lKCd2aWRlbycpWzBdO21lanMucGF1c2UoKTt9ZWxzZXttZWRpYS52aWRlby5wYXVzZSgpO319ZWxzZXtpZihtZWRpYS5wYXVzZSYmbWVkaWEuZm9ybWF0IT09J2RhaWx5bW90aW9uJyl7dmFyIG1lc3NhZ2U9dHlwZW9mIG1lZGlhLnBhdXNlPT09J29iamVjdCc/SlNPTi5zdHJpbmdpZnkobWVkaWEucGF1c2UpOlN0cmluZyhtZWRpYS5wYXVzZSk7bWVkaWEudmlkZW8uY29udGVudFdpbmRvdy5wb3N0TWVzc2FnZShtZXNzYWdlLCcqJyk7fWVsc2V7bWVkaWEuZG9tLmlubmVySFRNTD0nJzttZWRpYS52aWRlbz1udWxsO319fX07cHJvdG8uaW5zZXJ0TWVkaWE9ZnVuY3Rpb24obWVkaWFfaW5kZXgsc2xpZGVfaW5kZXgpe3ZhciBtZWRpYT10aGlzLmdhbGxlcnlbbWVkaWFfaW5kZXhdO2lmKCFtZWRpYSl7cmV0dXJuO31cbmlmKHR5cGVvZiBtZWRpYS5pbmRleD09PSd1bmRlZmluZWQnKXttZWRpYS5pbmRleD10aGlzLmdhbGxlcnkuaW5kZXhPZihtZWRpYSk7fVxudGhpcy5idWlsZE1lZGlhKG1lZGlhKTt0aGlzLmFwcGVuZE1lZGlhKG1lZGlhLHNsaWRlX2luZGV4KTt0aGlzLmxvYWRNZWRpYShtZWRpYSxzbGlkZV9pbmRleCk7fTtwcm90by5idWlsZE1lZGlhPWZ1bmN0aW9uKG1lZGlhKXtpZih0eXBlb2YgbWVkaWEuZG9tPT09J3VuZGVmaW5lZCcpe3N3aXRjaChtZWRpYS50eXBlKXtjYXNlJ2ltYWdlJzptZWRpYS5kb209dXRpbHMuY3JlYXRlRWwoJ2ltZycsdGhpcy5wcmUrJy1pbWcnKTttZWRpYS5kb20uc3JjPW1lZGlhLnNyYzticmVhaztjYXNlJ3ZpZGVvJzptZWRpYS5kb209dXRpbHMuY3JlYXRlRWwoJ2RpdicsdGhpcy5wcmUrJy12aWRlbycpO2lmKG1lZGlhLnBvc3Rlcil7bWVkaWEuZG9tLnN0eWxlLmJhY2tncm91bmRJbWFnZT0ndXJsKFwiJyttZWRpYS5wb3N0ZXIrJ1wiKSc7fWVsc2V7bWVkaWEuZG9tLmxvYWRlZD10cnVlO31cbmJyZWFrO2Nhc2UnaWZyYW1lJzptZWRpYS5kb209dXRpbHMuY3JlYXRlRWwoJ2lmcmFtZScsdGhpcy5wcmUrJy1pZnJhbWUnKTttZWRpYS5kb20uc2V0QXR0cmlidXRlKCdhbGxvd2Z1bGxzY3JlZW4nLCcnKTttZWRpYS5kb20uc2V0QXR0cmlidXRlKCdmcmFtZWJvcmRlcicsMCk7bWVkaWEuZG9tLnNyYz1tZWRpYS5zcmM7YnJlYWs7Y2FzZSdIVE1MJzp2YXIgZWxlbWVudD1tZWRpYS5zcmM7dmFyIGNvbnRlbnQ9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbGVtZW50KTttZWRpYS5kb209dXRpbHMuY3JlYXRlRWwoJ2RpdicsdGhpcy5wcmUrJy1odG1sJyk7bWVkaWEuZG9tLmFwcGVuZENoaWxkKHV0aWxzLmNyZWF0ZUVsKCdkaXYnLHRoaXMucHJlKyctaHRtbC1pbm5lcicpKTttZWRpYS5kb20uZmlyc3RDaGlsZC5pbm5lckhUTUw9Y29udGVudD9jb250ZW50LmlubmVySFRNTDpudWxsO21lZGlhLnNyYz1jb250ZW50P2NvbnRlbnQ6Jyc7bWVkaWEuZG9tLmxvYWRlZD10cnVlO2JyZWFrO31cbmlmKCFtZWRpYS50eXBlfHwhbWVkaWEuc3JjKXttZWRpYS5kb209dXRpbHMuY3JlYXRlRWwoJ2RpdicsdGhpcy5wcmUrJy1lcnJvcicpO21lZGlhLmRvbS50ZXh0Q29udGVudD10aGlzLm9wdGlvbnMubm9Db250ZW50O21lZGlhLmRvbS5sb2FkZWQ9dHJ1ZTttZWRpYS5kb20uZXJyb3I9dHJ1ZTt1dGlscy5kaXNwYXRjaEV2ZW50KHRoaXMsJ21vZHVsb2JveCcsJ25vQ29udGVudCcsdGhpcy5nYWxsZXJ5Lm5hbWUscGFyc2VJbnQobWVkaWEuaW5kZXgsMTApKTt9fX07cHJvdG8uYXBwZW5kTWVkaWE9ZnVuY3Rpb24obWVkaWEsc2xpZGVfaW5kZXgpe3ZhciBzbGlkZT10aGlzLnNsaWRlc1tzbGlkZV9pbmRleF0saG9sZGVyPXNsaWRlLmZpcnN0Q2hpbGQsbG9hZGVyO2lmKCFob2xkZXIuY2hpbGRFbGVtZW50Q291bnQpe3ZhciBmcmFnbWVudD1kb2N1bWVudC5jcmVhdGVEb2N1bWVudEZyYWdtZW50KCk7bG9hZGVyPXV0aWxzLmNyZWF0ZUVsKCdkaXYnLHRoaXMucHJlKyctbG9hZGVyJyk7ZnJhZ21lbnQuYXBwZW5kQ2hpbGQobG9hZGVyKTtmcmFnbWVudC5hcHBlbmRDaGlsZChtZWRpYS5kb20pO2hvbGRlci5hcHBlbmRDaGlsZChmcmFnbWVudCk7fWVsc2V7dmFyIG9sZE1lZGlhPWhvbGRlci5sYXN0Q2hpbGQ7bG9hZGVyPWhvbGRlci5maXJzdENoaWxkO2xvYWRlci5zdHlsZS52aXNpYmlsaXR5PScnO2lmKG1lZGlhLmRvbSE9PW9sZE1lZGlhKXt2YXIgbWV0aG9kPWhvbGRlci5jaGlsZEVsZW1lbnRDb3VudD09PTE/J2FwcGVuZENoaWxkJzoncmVwbGFjZUNoaWxkJztob2xkZXJbbWV0aG9kXShtZWRpYS5kb20sb2xkTWVkaWEpO319XG5zbGlkZS5tZWRpYT1tZWRpYS5pbmRleDt9O3Byb3RvLmxvYWRNZWRpYT1mdW5jdGlvbihtZWRpYSxzbGlkZV9pbmRleCl7aWYobWVkaWEuZG9tLmxvYWRlZCl7dGhpcy5zaG93TWVkaWEobWVkaWEsc2xpZGVfaW5kZXgpO3JldHVybjt9XG52YXIgX3RoaXM9dGhpcyxkb209bWVkaWEudHlwZT09PSdpZnJhbWUnP21lZGlhLmRvbTptZWRpYS5kb20uaW1nPW5ldyBJbWFnZSgpO3ZhciBvbkNvbXBsZXRlPWZ1bmN0aW9uKCl7aWYoIW1lZGlhLmRvbS5lcnJvcil7dXRpbHMuZGlzcGF0Y2hFdmVudChfdGhpcywnbW9kdWxvYm94JywnbG9hZENvbXBsZXRlJyxfdGhpcy5nYWxsZXJ5Lm5hbWUscGFyc2VJbnQobWVkaWEuaW5kZXgsMTApKTt9XG5tZWRpYS5kb20ubG9hZGVkPW1lZGlhLnR5cGUhPT0naWZyYW1lJz90cnVlOmZhbHNlO190aGlzLnNob3dNZWRpYShtZWRpYSxzbGlkZV9pbmRleCk7fTtkb20ub25sb2FkPW9uQ29tcGxldGU7ZG9tLm9uZXJyb3I9ZnVuY3Rpb24oZSl7aWYobWVkaWEudHlwZSE9PSd2aWRlbycpe21lZGlhLmRvbT11dGlscy5jcmVhdGVFbCgncCcsX3RoaXMucHJlKyctZXJyb3InKTttZWRpYS5kb20udGV4dENvbnRlbnQ9X3RoaXMub3B0aW9ucy5sb2FkRXJyb3I7bWVkaWEuZG9tLmVycm9yPXRydWU7X3RoaXMuYXBwZW5kTWVkaWEobWVkaWEsc2xpZGVfaW5kZXgpO31cbnV0aWxzLmRpc3BhdGNoRXZlbnQoX3RoaXMsJ21vZHVsb2JveCcsJ2xvYWRFcnJvcicsX3RoaXMuZ2FsbGVyeS5uYW1lLHBhcnNlSW50KG1lZGlhLmluZGV4LDEwKSk7b25Db21wbGV0ZSgpO307ZG9tLnNyYz0obWVkaWEudHlwZT09PSd2aWRlbycpP21lZGlhLnBvc3RlcjptZWRpYS5zcmM7fTtwcm90by51bmxvYWRNZWRpYT1mdW5jdGlvbihzbGlkZSl7aWYoIXRoaXMuZ2FsbGVyeSl7cmV0dXJuO31cbnZhciBpbmRleD1zbGlkZS5tZWRpYSxtZWRpYT10aGlzLmdhbGxlcnlbaW5kZXhdO2lmKCFtZWRpYXx8IW1lZGlhLmRvbSl7cmV0dXJuO31cbmlmKHRoaXMub3B0aW9ucy51bmxvYWQmJm1lZGlhLnR5cGU9PT0naW1hZ2UnJiYhbWVkaWEuZG9tLmxvYWRlZCYmIW1lZGlhLmRvbS5jb21wbGV0ZSYmIW1lZGlhLmRvbS5uYXR1cmFsV2lkdGgpe21lZGlhLmRvbS5vbmxvYWQ9bnVsbDttZWRpYS5kb20ub25lcnJvcj1udWxsO21lZGlhLmRvbS5zcmM9Jyc7aWYobWVkaWEuZG9tLmltZyl7bWVkaWEuZG9tLmltZy5vbmxvYWQ9bnVsbDttZWRpYS5kb20uaW1nLm9uZXJyb3I9bnVsbDttZWRpYS5kb20uaW1nLnNyYz0nJztkZWxldGUgbWVkaWEuZG9tLmltZzt9XG5kZWxldGUgbWVkaWEuZG9tO31cbmVsc2UgaWYobWVkaWEudHlwZT09PSd2aWRlbycmJm1lZGlhLmZvcm1hdCE9PSdodG1sNScmJm1lZGlhLmRvbS5maXJzdENoaWxkKXttZWRpYS52aWRlbz1udWxsO21lZGlhLmRvbS5yZW1vdmVDaGlsZChtZWRpYS5kb20uZmlyc3RDaGlsZCk7fX07cHJvdG8ucmVtb3ZlTWVkaWE9ZnVuY3Rpb24oaG9sZGVyKXt2YXIgY29udGVudD1ob2xkZXIuZmlyc3RDaGlsZDtpZighY29udGVudCl7cmV0dXJuO31cbndoaWxlKGNvbnRlbnQuZmlyc3RDaGlsZCl7Y29udGVudC5yZW1vdmVDaGlsZChjb250ZW50LmZpcnN0Q2hpbGQpO319O3Byb3RvLnNob3dNZWRpYT1mdW5jdGlvbihtZWRpYSxzbGlkZV9pbmRleCl7dmFyIHNsaWRlcj10aGlzLnNsaWRlcjtpZih0aGlzLm9wdGlvbnMuZmFkZUlmU2V0dGxlJiYhc2xpZGVyLnNldHRsZSYmIW1lZGlhLmRvbS5yZXZlYWxlZCl7cmV0dXJuO31cbnZhciBzbGlkZT10aGlzLnNsaWRlc1tzbGlkZV9pbmRleF0sZ2FsbGVyeT10aGlzLmdhbGxlcnksaG9sZGVyPXNsaWRlLmZpcnN0Q2hpbGQsbG9hZGVyPWhvbGRlci5maXJzdENoaWxkLHByZWxvYWQ9dGhpcy5vcHRpb25zLnByZWxvYWQ7dGhpcy5zZXRNZWRpYVNpemUobWVkaWEsc2xpZGUpO2lmKG1lZGlhLmluZGV4PT09Z2FsbGVyeS5pbmRleCl7dGhpcy5pc1pvb21hYmxlKCk7fVxudXRpbHMuYWRkQ2xhc3MobWVkaWEuZG9tLHRoaXMucHJlKyctbWVkaWEtbG9hZGVkJyk7bWVkaWEuZG9tLnJldmVhbGVkPXRydWU7aWYoc2xpZGUubWVkaWE9PT1tZWRpYS5pbmRleCl7bG9hZGVyLnN0eWxlLnZpc2liaWxpdHk9J2hpZGRlbic7Z2FsbGVyeS5sb2FkZWQrPTE7aWYoZ2FsbGVyeS5sb2FkZWQ9PT1wcmVsb2FkJiZwcmVsb2FkPDQpe3RoaXMuc2V0TWVkaWEocHJlbG9hZCsyKTt9fVxuaWYobWVkaWEudHlwZT09PSdpZnJhbWUnKXttZWRpYS5kb20ubG9hZGVkPWZhbHNlO319O3Byb3RvLnNldE1lZGlhU2l6ZT1mdW5jdGlvbihtZWRpYSxzbGlkZSl7dmFyIG9iamVjdD1tZWRpYS5kb20sc2xpZGVyPXRoaXMuc2xpZGVyLHZpZXdwb3J0PW9iamVjdC52aWV3cG9ydCx0aHVtYnM9dGhpcy5nZXRUaHVtYkhlaWdodCgpO2lmKG9iamVjdC5lcnJvcil7cmV0dXJuO31cbmlmKCF2aWV3cG9ydHx8dmlld3BvcnQud2lkdGghPT1zbGlkZXIud2lkdGh8fHZpZXdwb3J0LmhlaWdodCE9PXNsaWRlci5oZWlnaHQtdGh1bWJzKXt0aGlzLmdldENhcHRpb25IZWlnaHQobWVkaWEsc2xpZGUpO3RoaXMuZ2V0TWVkaWFTaXplKG1lZGlhLHNsaWRlKTt0aGlzLmZpdE1lZGlhU2l6ZShtZWRpYSxzbGlkZSk7dGhpcy5zZXRNZWRpYU9mZnNldChtZWRpYSxzbGlkZSk7fVxudmFyIHN0eWxlPW9iamVjdC5zdHlsZTtzdHlsZS53aWR0aD1vYmplY3Quc2l6ZS53aWR0aCsncHgnO3N0eWxlLmhlaWdodD1vYmplY3Quc2l6ZS5oZWlnaHQrJ3B4JztzdHlsZS5sZWZ0PW9iamVjdC5vZmZzZXQubGVmdCsncHgnO3N0eWxlLnRvcD1vYmplY3Qub2Zmc2V0LnRvcCsncHgnO307cHJvdG8uZ2V0Q2FwdGlvbkhlaWdodD1mdW5jdGlvbihtZWRpYSxzbGlkZSl7dmFyIGNhcHRpb249dGhpcy5ET00uY2FwdGlvbklubmVyLHRvcEJhcj10aGlzLkRPTS50b3BCYXIuaGVpZ2h0LGNvbnRlbnQ9Y2FwdGlvbi5pbm5lckhUTUwsdGh1bWJzPXRoaXMuZ2V0VGh1bWJIZWlnaHQoKTtpZih0aGlzLm9wdGlvbnMuY2FwdGlvbiYmdGhpcy5zdGF0ZXMuY2FwdGlvbiYmbWVkaWEuY2FwdGlvbil7Y2FwdGlvbi5pbm5lckhUTUw9bWVkaWEuY2FwdGlvbjtjYXB0aW9uLmhlaWdodD1NYXRoLm1heCh0b3BCYXIscGFyc2VJbnQoY2FwdGlvbi5jbGllbnRIZWlnaHQsMTApKXx8dG9wQmFyO2NhcHRpb24uaW5uZXJIVE1MPWNvbnRlbnQ7fWVsc2V7Y2FwdGlvbi5oZWlnaHQ9dGh1bWJzPzA6dG9wQmFyO31cbnNsaWRlLndpZHRoPXRoaXMuc2xpZGVyLndpZHRoO3NsaWRlLmhlaWdodD10aGlzLnNsaWRlci5oZWlnaHQtdG9wQmFyLWNhcHRpb24uaGVpZ2h0LXRodW1iczt9O3Byb3RvLmdldE1lZGlhU2l6ZT1mdW5jdGlvbihtZWRpYSxzbGlkZSl7dmFyIHNpemU9bWVkaWEuZG9tLnNpemU9e307c3dpdGNoKG1lZGlhLnR5cGUpe2Nhc2UnaW1hZ2UnOnNpemUud2lkdGg9bWVkaWEuZG9tLm5hdHVyYWxXaWR0aDtzaXplLmhlaWdodD1tZWRpYS5kb20ubmF0dXJhbEhlaWdodDticmVhaztjYXNlJ3ZpZGVvJzpzaXplLndpZHRoPXRoaXMub3B0aW9ucy52aWRlb01heFdpZHRoO3NpemUuaGVpZ2h0PXNpemUud2lkdGgvdGhpcy5vcHRpb25zLnZpZGVvUmF0aW87YnJlYWs7Y2FzZSdpZnJhbWUnOnNpemUud2lkdGg9bWVkaWEud2lkdGg/bWVkaWEud2lkdGg6c2xpZGUud2lkdGg+NjgwP3NsaWRlLndpZHRoKjAuODpzbGlkZS53aWR0aDtzaXplLmhlaWdodD1tZWRpYS5oZWlnaHQ/bWVkaWEuaGVpZ2h0OnNsaWRlLmhlaWdodDticmVhaztjYXNlJ0hUTUwnOnNpemUud2lkdGg9bWVkaWEud2lkdGg/bWVkaWEud2lkdGg6c2xpZGUud2lkdGg7c2l6ZS5oZWlnaHQ9bWVkaWEuaGVpZ2h0P21lZGlhLmhlaWdodDpzbGlkZS5oZWlnaHQ7YnJlYWs7fX07cHJvdG8uZml0TWVkaWFTaXplPWZ1bmN0aW9uKG1lZGlhLHNsaWRlKXt2YXIgc2xpZGVyPXRoaXMuc2xpZGVyLG9wdGlvbnM9dGhpcy5vcHRpb25zLHpvb209b3B0aW9ucy56b29tVG8sc2l6ZT1tZWRpYS5kb20uc2l6ZSxyYXRpbz1zaXplLndpZHRoL3NpemUuaGVpZ2h0LHRodW1icz10aGlzLmdldFRodW1iSGVpZ2h0KCksc21hbGxEZXZpY2U9c2xpZGVyLndpZHRoPD00ODB8fHNsaWRlci5oZWlnaHQ8PTY4MCxjYW5PdmVyZmxvdz1bJ3ZpZGVvJywnaWZyYW1lJywnSFRNTCddLmluZGV4T2YobWVkaWEudHlwZSk8MCxzbWFydFJlc2l6ZT1vcHRpb25zLnNtYXJ0UmVzaXplJiZzbWFsbERldmljZSx3aWR0aCxoZWlnaHQ7dmFyIHZpZXdwb3J0cz1bc2xpZGUuaGVpZ2h0XTtpZigoc21hcnRSZXNpemV8fG9wdGlvbnMub3ZlcmZsb3cpJiZjYW5PdmVyZmxvdyl7dmlld3BvcnRzLnVuc2hpZnQoc2xpZGVyLmhlaWdodC10aHVtYnMpO31cbnZpZXdwb3J0cy5mb3JFYWNoKGZ1bmN0aW9uKHZpZXdwb3J0KXtpZighaGVpZ2h0fHxoZWlnaHQ8c2xpZGVyLmhlaWdodC10aHVtYnMpe3dpZHRoPU1hdGgubWluKHNpemUud2lkdGgscmF0aW8qdmlld3BvcnQpO3dpZHRoPXdpZHRoPnNsaWRlLndpZHRoP3NsaWRlLndpZHRoOk1hdGgucm91bmQod2lkdGgpO2hlaWdodD1NYXRoLmNlaWwoMS9yYXRpbyp3aWR0aCk7aGVpZ2h0PWhlaWdodCV2aWV3cG9ydDwyP3ZpZXdwb3J0OmhlaWdodDt9fSk7dmFyIHNjYWxlPU51bWJlcigoc2l6ZS53aWR0aC93aWR0aCkudG9GaXhlZCgzKSk7em9vbT16b29tPT09J2F1dG8nP3NjYWxlOnpvb207bWVkaWEuZG9tLnNpemU9e3dpZHRoOndpZHRoLGhlaWdodDpoZWlnaHQsc2NhbGU6c2NhbGU+PW9wdGlvbnMubWluWm9vbT9NYXRoLm1pbihvcHRpb25zLm1heFpvb20sem9vbSk6MX07fTtwcm90by5zZXRNZWRpYU9mZnNldD1mdW5jdGlvbihtZWRpYSxzbGlkZSl7dmFyIHNpemU9bWVkaWEuZG9tLnNpemUsc2xpZGVyPXRoaXMuc2xpZGVyLHRvcEJhcj10aGlzLkRPTS50b3BCYXIuaGVpZ2h0LHRodW1icz10aGlzLmdldFRodW1iSGVpZ2h0KCksZnJvbVRvcD0wO2lmKHNpemUuaGVpZ2h0PD1zbGlkZS5oZWlnaHQpe2Zyb21Ub3A9dG9wQmFyKyhzbGlkZS5oZWlnaHQtc2l6ZS5oZWlnaHQpKjAuNTt9XG5tZWRpYS5kb20ub2Zmc2V0PXt0b3A6ZnJvbVRvcDwwPzA6TWF0aC5yb3VuZChmcm9tVG9wKSxsZWZ0Ok1hdGgucm91bmQoKHNsaWRlLndpZHRoLXNpemUud2lkdGgpKjAuNSl9O21lZGlhLmRvbS52aWV3cG9ydD17d2lkdGg6c2xpZGVyLndpZHRoLGhlaWdodDpzbGlkZXIuaGVpZ2h0LXRodW1ic307fTtwcm90by5tZWRpYVZpZXdwb3J0PWZ1bmN0aW9uKHNjYWxlKXt2YXIgbWVkaWE9dGhpcy5nZXRNZWRpYSgpO2lmKCFtZWRpYS5kb218fCFtZWRpYS5kb20uc2l6ZSl7cmV0dXJue3RvcDowLGJvdHRvbTowLGxlZnQ6MCxyaWdodDowfTt9XG52YXIgc2l6ZT1tZWRpYS5kb20uc2l6ZSxvZmZzZXQ9bWVkaWEuZG9tLm9mZnNldCxoZWlnaHQ9dGhpcy5zbGlkZXIuaGVpZ2h0LHdpZHRoPXRoaXMuc2xpZGVyLndpZHRoLGhHYXA9KGhlaWdodC1zaXplLmhlaWdodCkqMC41LHZHYXA9b2Zmc2V0LnRvcCoyLWhHYXAsY0dhcD0oaEdhcC12R2FwKSowLjUsc0dhcD1jR2FwKnNjYWxlLWNHYXAqMi12R2FwLGxlZnQ9c2l6ZS53aWR0aC8yKihzY2FsZS0xKS1vZmZzZXQubGVmdCx0b3A9c2l6ZS5oZWlnaHQqc2NhbGU8PWhlaWdodD9jR2FwKnNjYWxlOi1zaXplLmhlaWdodC8yKihzY2FsZS0xKStoZWlnaHQtc2l6ZS5oZWlnaHQrc0dhcCxib3R0b209c2l6ZS5oZWlnaHQqc2NhbGU8PWhlaWdodD9jR2FwKnNjYWxlOnNpemUuaGVpZ2h0LzIqKHNjYWxlLTEpK3NHYXA7cmV0dXJue3RvcDpzY2FsZTw9MT8wOk1hdGgucm91bmQodG9wKSxib3R0b206c2NhbGU8PTE/MDpNYXRoLnJvdW5kKGJvdHRvbSksbGVmdDpzaXplLndpZHRoKnNjYWxlPHdpZHRoPzA6TWF0aC5yb3VuZChsZWZ0KSxyaWdodDpzaXplLndpZHRoKnNjYWxlPHdpZHRoPzA6TWF0aC5yb3VuZCgtbGVmdCl9O307cHJvdG8uc2V0TWVkaWE9ZnVuY3Rpb24obnVtYmVyKXt2YXIgZ2FsbGVyeT10aGlzLmdhbGxlcnksc2xpZGVzPXRoaXMuc2xpZGVzLGxvb3A9dGhpcy5zdGF0ZXMubG9vcCxSVEw9dGhpcy5pc1JUTCgpLGluZGV4PU1hdGgucm91bmQoLVJUTCp0aGlzLnNsaWRlci5wb3NpdGlvbi54L3NsaWRlcy53aWR0aCksbGVuZ3RoPWdhbGxlcnkuaW5pdGlhbExlbmd0aC0xLGFkanVzdD0wLHRvTG9hZD1bXSxpO2lmKCFudW1iZXImJiFnYWxsZXJ5LmxvYWRlZCl7bnVtYmVyPTA7Zm9yKGk9MDtpPHNsaWRlcy5sZW5ndGg7aSsrKXtpZihzbGlkZXNbaV0uZmlyc3RDaGlsZC5jaGlsZEVsZW1lbnRDb3VudCl7bnVtYmVyKys7fX1cbm51bWJlcis9MjtnYWxsZXJ5LmxvYWRlZD10aGlzLm9wdGlvbnMucHJlbG9hZDt9XG5zd2l0Y2gobnVtYmVyKXtjYXNlIDA6Y2FzZSAxOnRvTG9hZD1bMF07YnJlYWs7Y2FzZSAyOmNhc2UgMzp0b0xvYWQ9Wy0xLDAsMV07YnJlYWs7ZGVmYXVsdDpudW1iZXI9NTt0b0xvYWQ9Wy0yLC0xLDAsMSwyXTt9XG5pZighbG9vcCl7dmFyIG1heE1lZGlhPWluZGV4K3RvTG9hZFtudW1iZXItMV0sbWluTWVkaWE9aW5kZXgrdG9Mb2FkWzBdO2FkanVzdD0obWluTWVkaWE8MCk/LW1pbk1lZGlhOjA7YWRqdXN0PShtYXhNZWRpYT5sZW5ndGgpP2xlbmd0aC1tYXhNZWRpYTphZGp1c3Q7fVxudG9Mb2FkPXRvTG9hZC5tYXAoZnVuY3Rpb24oaSl7cmV0dXJuIHV0aWxzLm1vZHVsbyhnYWxsZXJ5Lmxlbmd0aCxpK2FkanVzdCtpbmRleCk7fSk7Zm9yKGk9MDtpPHNsaWRlcy5sZW5ndGg7aSsrKXt2YXIgc2xpZGU9c2xpZGVzW2ldLG1lZGlhX2luZGV4PXV0aWxzLm1vZHVsbyhnYWxsZXJ5Lmxlbmd0aCxzbGlkZS5pbmRleCk7aWYoIWxvb3AmJnNsaWRlLmluZGV4Pm1lZGlhX2luZGV4KXtjb250aW51ZTt9XG5pZih0b0xvYWQuaW5kZXhPZihtZWRpYV9pbmRleCk+LTEmJnNsaWRlLm1lZGlhIT09bWVkaWFfaW5kZXgpe3RoaXMudW5sb2FkTWVkaWEoc2xpZGUpO3RoaXMuaW5zZXJ0TWVkaWEobWVkaWFfaW5kZXgsaSk7fX19O3Byb3RvLnVwZGF0ZU1lZGlhSW5mbz1mdW5jdGlvbigpe3ZhciBzbGlkZXM9dGhpcy5zbGlkZXMsZ2FsbGVyeT10aGlzLmdhbGxlcnk7Z2FsbGVyeS5pbmRleD11dGlscy5tb2R1bG8oZ2FsbGVyeS5sZW5ndGgsc2xpZGVzLmluZGV4KTt0aGlzLmlzWm9vbWFibGUoKTt0aGlzLmlzRG93bmxvYWRhYmxlKCk7dGhpcy51cGRhdGVDb3VudGVyKCk7dGhpcy51cGRhdGVDYXB0aW9uKCk7dGhpcy51cGRhdGVUaHVtYnMoKTt1dGlscy5kaXNwYXRjaEV2ZW50KHRoaXMsJ21vZHVsb2JveCcsJ3VwZGF0ZU1lZGlhJyx0aGlzLmdldE1lZGlhKCkpO307cHJvdG8uc2V0VGh1bWJzPWZ1bmN0aW9uKCl7dmFyIGdhbGxlcnk9dGhpcy5nYWxsZXJ5LHRodW1icz10aGlzLnRodW1icyxsZW5ndGg9Z2FsbGVyeS5pbml0aWFsTGVuZ3RoLHRodW1ibmFpbHM9dGhpcy5vcHRpb25zLnRodW1ibmFpbHMsdGh1bWJzSG9sZGVyPXRoaXMuRE9NLnRodW1ic0hvbGRlcjtpZighdGh1bWJuYWlsc3x8bGVuZ3RoPDIpe3RoaXMuRE9NLmNhcHRpb24uc3R5bGUuYm90dG9tPTA7dGh1bWJzSG9sZGVyLnN0eWxlLnZpc2liaWxpdHk9J2hpZGRlbic7dGh1bWJzSG9sZGVyLnN0eWxlLmhlaWdodD0wO3RodW1icy5oZWlnaHQ9dGh1bWJzLmd1dHRlcj0wO3JldHVybjt9XG52YXIgc2l6ZXM9dGhpcy5vcHRpb25zLnRodW1ibmFpbFNpemVzLHNjcmVlblc9TWF0aC5tYXgod2luZG93LmlubmVyV2lkdGgsTWF0aC5tYXgoc2NyZWVuLndpZHRoLHNjcmVlbi5oZWlnaHQpKSx0aHVtYk5iPTAsaTt2YXIgd2lkdGhzPU9iamVjdC5rZXlzKHNpemVzKS5zb3J0KGZ1bmN0aW9uKGEsYil7cmV0dXJuIGEtYjt9KTtmb3IoaT0wO2k8d2lkdGhzLmxlbmd0aDtpKyspe3ZhciBzaXplPXdpZHRoc1tpXSx3aWR0aD1pPT09d2lkdGhzLmxlbmd0aC0xP3NjcmVlblc6TWF0aC5taW4oc2NyZWVuVyxzaXplKSxudW1iZXI9TWF0aC5jZWlsKHdpZHRoLyhzaXplc1tzaXplXS53aWR0aCtzaXplc1tzaXplXS5ndXR0ZXIpKjIpO2lmKGlzRmluaXRlKG51bWJlcikmJm51bWJlcj50aHVtYk5iKXt0aHVtYk5iPW51bWJlcjt9XG5pZihzaXplPj1zY3JlZW5XKXticmVhazt9fVxudmFyIGZyYWdtZW50PWRvY3VtZW50LmNyZWF0ZURvY3VtZW50RnJhZ21lbnQoKTtsZW5ndGg9bGVuZ3RoPjUwP01hdGgubWluKHRodW1iTmIsbGVuZ3RoKTpsZW5ndGg7Zm9yKGk9MDtpPGxlbmd0aDtpKyspe3ZhciB0aHVtYj11dGlscy5jcmVhdGVFbCgnZGl2Jyx0aGlzLnByZSsnLXRodW1iJyk7ZnJhZ21lbnQuYXBwZW5kQ2hpbGQodGh1bWIpO31cbnRoaXMuRE9NLnRodW1ic0lubmVyLmFwcGVuZENoaWxkKGZyYWdtZW50KTt0aGlzLnNldFRodW1ic1Bvc2l0aW9uKCk7fTtwcm90by50aHVtYkNsaWNrPWZ1bmN0aW9uKGV2ZW50KXt2YXIgdGFyZ2V0PWV2ZW50LnRhcmdldDtpZighdXRpbHMuaGFzQ2xhc3ModGFyZ2V0LHRoaXMucHJlKyctdGh1bWInKSl7dGFyZ2V0PXRhcmdldC5wYXJlbnROb2RlO31cbmlmKHBhcnNlSW50KHRhcmdldC5pbmRleCwxMCk+PTApe3RoaXMuc2xpZGVUbyh0YXJnZXQuaW5kZXgpO319O3Byb3RvLmxvYWRUaHVtYj1mdW5jdGlvbih0aHVtYixpbmRleCl7dmFyIG1lZGlhPXRoaXMuZ2FsbGVyeVtpbmRleF0sc3JjO2lmKCFtZWRpYS50aHVtYnx8dHlwZW9mIG1lZGlhLnRodW1iIT09J29iamVjdCcpe3NyYz1tZWRpYS50aHVtYjttZWRpYS50aHVtYj11dGlscy5jcmVhdGVFbCgnZGl2Jyx0aGlzLnByZSsnLXRodW1iLWJnJyk7bWVkaWEudGh1bWIuc3R5bGUuYmFja2dyb3VuZEltYWdlPXNyYyYmc3JjLmluZGV4T2YoJy5qc29uJyk8MD8ndXJsKCcrc3JjKycpJzpudWxsO2lmKG1lZGlhLnR5cGU9PT0ndmlkZW8nKXt1dGlscy5hZGRDbGFzcyhtZWRpYS50aHVtYix0aGlzLnByZSsnLXRodW1iLXZpZGVvJyk7dXRpbHMuYWRkQ2xhc3MobWVkaWEudGh1bWIsdGhpcy5wcmUrJy10aHVtYi1sb2FkZWQnKTt9fVxudmFyIG1ldGhvZD10aHVtYi5maXJzdENoaWxkPydyZXBsYWNlQ2hpbGQnOidhcHBlbmRDaGlsZCc7dGh1bWJbbWV0aG9kXShtZWRpYS50aHVtYix0aHVtYi5maXJzdENoaWxkKTt0aHVtYi5tZWRpYT1pbmRleDtpZihzcmMpe3ZhciBkb209bmV3IEltYWdlKCk7ZG9tLm9ubG9hZD1mdW5jdGlvbigpe3V0aWxzLmFkZENsYXNzKG1lZGlhLnRodW1iLHRoaXMucHJlKyctdGh1bWItbG9hZGVkJyk7fS5iaW5kKHRoaXMpO2RvbS5zcmM9c3JjO319O3Byb3RvLnVwZGF0ZVRodW1icz1mdW5jdGlvbigpe3ZhciBnYWxsZXJ5PXRoaXMuZ2FsbGVyeTtpZighdGhpcy5vcHRpb25zLnRodW1ibmFpbHN8fGdhbGxlcnkuaW5pdGlhbExlbmd0aDwyKXtyZXR1cm47fVxudmFyIHRodW1icz10aGlzLnRodW1icyxwb3NpdGlvbj10aGlzLmdldFRodW1iUG9zaXRpb24odGh1bWJzKTt0aHVtYnMuc3RvcEFuaW1hdGUoKTtpZihwb3NpdGlvbj09PXRodW1icy5wb3NpdGlvbi54KXt0aGlzLnNoaWZ0VGh1bWJzKHRodW1icyk7cmV0dXJuO31cbmlmKE1hdGguYWJzKHBvc2l0aW9uLXRodW1icy5wb3NpdGlvbi54KT41MCp0aHVtYnMuc2l6ZSl7dGhpcy5ET00udGh1bWJzSG9sZGVyLnN0eWxlLnZpc2liaWxpdHk9J2hpZGRlbic7dGh1bWJzLnBvc2l0aW9uLng9cG9zaXRpb247dXRpbHMudHJhbnNsYXRlKHRoaXMuRE9NLnRodW1ic0lubmVyLHBvc2l0aW9uLDApO3RoaXMucmVuZGVyVGh1bWJzKHRodW1icyk7dGhpcy5ET00udGh1bWJzSG9sZGVyLnN0eWxlLnZpc2liaWxpdHk9Jyc7fWVsc2V7dGh1bWJzLnN0YXJ0QW5pbWF0ZSgpO3RodW1icy5yZWxlYXNlRHJhZygpO3RodW1icy5hbmltYXRlVG8oe3g6cG9zaXRpb259KTt9fTtwcm90by51cGRhdGVDYXB0aW9uPWZ1bmN0aW9uKCl7aWYodGhpcy5vcHRpb25zLmNhcHRpb24pe3ZhciBtZWRpYT10aGlzLmdldE1lZGlhKCksY29udGVudD1tZWRpYS5jYXB0aW9uP21lZGlhLmNhcHRpb246JycsY2FwdGlvbj10aGlzLkRPTS5jYXB0aW9uSW5uZXI7aWYoY2FwdGlvbi5pbm5lckhUTUwhPT1jb250ZW50KXtjYXB0aW9uLmlubmVySFRNTD1jb250ZW50O319fTtwcm90by51cGRhdGVDb3VudGVyPWZ1bmN0aW9uKCl7aWYodGhpcy5vcHRpb25zLmNvdW50ZXJNZXNzYWdlKXt2YXIgZ2FsbGVyeT10aGlzLmdhbGxlcnksbGVuZ3RoPWdhbGxlcnkuaW5pdGlhbExlbmd0aCxpbmRleD11dGlscy5tb2R1bG8obGVuZ3RoLGdhbGxlcnkuaW5kZXgpLG1lc3NhZ2U9dGhpcy5vcHRpb25zLmNvdW50ZXJNZXNzYWdlLGNvbnRlbnQ9bWVzc2FnZS5yZXBsYWNlKCdbaW5kZXhdJyxpbmRleCsxKS5yZXBsYWNlKCdbdG90YWxdJyxsZW5ndGgpLGNvdW50ZXI9dGhpcy5ET00uY291bnRlcjtpZihjb3VudGVyLnRleHRDb250ZW50IT09Y29udGVudCl7Y291bnRlci50ZXh0Q29udGVudD1jb250ZW50O319fTtwcm90by53cmFwQXJvdW5kPWZ1bmN0aW9uKCl7dmFyIGxvb3A9dGhpcy5vcHRpb25zLmxvb3AsZ2FsbGVyeT10aGlzLmdhbGxlcnksbGVuZ3RoPWdhbGxlcnkubGVuZ3RoO2lmKCFnYWxsZXJ5LmluaXRpYWxMZW5ndGgpe2dhbGxlcnkuaW5pdGlhbExlbmd0aD1sZW5ndGg7fVxudGhpcy5zdGF0ZXMubG9vcD1sb29wJiZsb29wPD1sZW5ndGg/dHJ1ZTpmYWxzZTtpZih0aGlzLnN0YXRlcy5sb29wJiZsZW5ndGg8dGhpcy5zbGlkZXMubGVuZ3RoKXt2YXIgYWRkPU1hdGguY2VpbCh0aGlzLnNsaWRlcy5sZW5ndGgvbGVuZ3RoKSpsZW5ndGgtbGVuZ3RoO2Zvcih2YXIgaT0wO2k8YWRkO2krKyl7dmFyIGluZGV4PWxlbmd0aCtpO2dhbGxlcnlbaW5kZXhdPXV0aWxzLmNsb25lT2JqZWN0KGdhbGxlcnlbdXRpbHMubW9kdWxvKGxlbmd0aCxpKV0pO2dhbGxlcnlbaW5kZXhdLmluZGV4PWluZGV4O319fTtwcm90by5zZXRTbGlkZXI9ZnVuY3Rpb24oKXt2YXIgc2xpZGVyPXRoaXMuc2xpZGVyLHNsaWRlcz10aGlzLnNsaWRlczt0aGlzLnNldFNpemVzKHNsaWRlcixzbGlkZXMpO3RoaXMuc2V0U2xpZGVyUG9zaXRpb24oc2xpZGVyLHNsaWRlcyk7dGhpcy5zZXRTbGlkZXNQb3NpdGlvbnMoc2xpZGVzKTt0aGlzLkRPTS5vdmVybGF5LnN0eWxlLm9wYWNpdHk9MTt9O3Byb3RvLnNldFNpemVzPWZ1bmN0aW9uKHNsaWRlcixzbGlkZXMpe3NsaWRlci53aWR0aD1kb2N1bWVudC5ib2R5LmNsaWVudFdpZHRoO3NsaWRlci5oZWlnaHQ9d2luZG93LmlubmVySGVpZ2h0O3NsaWRlcy53aWR0aD1zbGlkZXIud2lkdGgrTWF0aC5yb3VuZChzbGlkZXIud2lkdGgqdGhpcy5vcHRpb25zLnNwYWNpbmcpO307cHJvdG8uc2V0U2xpZGVzUG9zaXRpb25zPWZ1bmN0aW9uKHNsaWRlcyl7Zm9yKHZhciBpPTA7aTxzbGlkZXMubGVuZ3RoO2krKyl7c2xpZGVzW2ldLnBvc2l0aW9uPW51bGw7dGhpcy5zZXRDZWxsUG9zaXRpb24oaSk7fVxudGhpcy5zaGlmdFNsaWRlcygpO307cHJvdG8uc2V0VGh1bWJzUG9zaXRpb249ZnVuY3Rpb24oKXtpZighdGhpcy5vcHRpb25zLnRodW1ibmFpbHN8fHRoaXMuZ2FsbGVyeS5pbml0aWFsTGVuZ3RoPDIpe3JldHVybjt9XG52YXIgdGh1bWJzPXRoaXMudGh1bWJzLHNsaWRlcj10aGlzLnNsaWRlcixob2xkZXI9dGhpcy5ET00udGh1bWJzSG9sZGVyLGlubmVyPXRoaXMuRE9NLnRodW1ic0lubmVyLHNpemVzPXRoaXMub3B0aW9ucy50aHVtYm5haWxTaXplcyxSVEw9dGhpcy5vcHRpb25zLnJpZ2h0VG9MZWZ0LHdpZHRocz1PYmplY3Qua2V5cyhzaXplcykuc29ydChmdW5jdGlvbihhLGIpe3JldHVybiBiLWE7fSksd2lkdGg9TWF0aC5tYXguYXBwbHkobnVsbCx3aWR0aHMpLGJyb3dzZXI9d2luZG93LmlubmVyV2lkdGgsc2l6ZTtmb3IodmFyIGk9MDtpPHdpZHRocy5sZW5ndGg7aSsrKXtzaXplPU51bWJlcih3aWR0aHNbaV0pO2lmKGJyb3dzZXI8PXNpemUpe3dpZHRoPXNpemU7fX1cbnRodW1icy53aWR0aD1OdW1iZXIoc2l6ZXNbd2lkdGhdLndpZHRoKTt0aHVtYnMuZ3V0dGVyPU51bWJlcihzaXplc1t3aWR0aF0uZ3V0dGVyKTt0aHVtYnMuaGVpZ2h0PU51bWJlcihzaXplc1t3aWR0aF0uaGVpZ2h0KTt0aHVtYnMuc2l6ZT10aHVtYnMud2lkdGgrdGh1bWJzLmd1dHRlcjt0aHVtYnMubGVuZ3RoPXRoaXMuZ2FsbGVyeS5pbml0aWFsTGVuZ3RoO3ZhciB0b3RhbFdpZHRoPXRodW1icy5sZW5ndGgqdGh1bWJzLnNpemU7dGh1bWJzLmJvdW5kPXtsZWZ0OjAscmlnaHQ6dG90YWxXaWR0aD5zbGlkZXIud2lkdGg/c2xpZGVyLndpZHRoLXRvdGFsV2lkdGg6MH07aWYoUlRMKXt0aHVtYnMuYm91bmQucmlnaHQ9dG90YWxXaWR0aD5zbGlkZXIud2lkdGg/c2xpZGVyLndpZHRoLXRodW1icy5zaXplOnRvdGFsV2lkdGgtdGh1bWJzLnNpemU7dGh1bWJzLmJvdW5kLmxlZnQ9dG90YWxXaWR0aC10aHVtYnMuc2l6ZTt9XG5pZih0aGlzLm9wdGlvbnMudGh1bWJuYWlsc05hdj09PSdjZW50ZXJlZCcpe3RodW1icy5ib3VuZD17bGVmdDp0b3RhbFdpZHRoPnNsaWRlci53aWR0aD9NYXRoLmZsb29yKHNsaWRlci53aWR0aCowLjUtdGh1bWJzLnNpemUqMC41KTpNYXRoLmZsb29yKHRvdGFsV2lkdGgqMC41LXRodW1icy5zaXplKjAuNSkscmlnaHQ6dG90YWxXaWR0aD5zbGlkZXIud2lkdGg/TWF0aC5jZWlsKHNsaWRlci53aWR0aCowLjUtdG90YWxXaWR0aCt0aHVtYnMuc2l6ZSowLjUpOi1NYXRoLmNlaWwodG90YWxXaWR0aCowLjUtdGh1bWJzLnNpemUqMC41KX07aWYoUlRMKXt0aHVtYnMuYm91bmQucmlnaHQ9dGh1bWJzLmJvdW5kLmxlZnQ7dGh1bWJzLmJvdW5kLmxlZnQ9dGh1bWJzLmJvdW5kLmxlZnQrdG90YWxXaWR0aC10aHVtYnMuc2l6ZTt9fVxudGh1bWJzLnJlc2V0QW5pbWF0ZSgpO3ZhciBwb3NpdGlvbj10aGlzLmdldFRodW1iUG9zaXRpb24odGh1bWJzKTt0aHVtYnMucG9zaXRpb24ueD1wb3NpdGlvbjt1dGlscy50cmFuc2xhdGUoaW5uZXIscG9zaXRpb24sMCk7dmFyIGhhc0hlaWdodD10aGlzLmdldFRodW1iSGVpZ2h0KCk7aG9sZGVyLnN0eWxlLnZpc2liaWxpdHk9aGFzSGVpZ2h0PycnOidoaWRkZW4nO2hvbGRlci5zdHlsZS5oZWlnaHQ9aGFzSGVpZ2h0P2hhc0hlaWdodCsncHgnOicnO2lubmVyLnN0eWxlLmhlaWdodD1oYXNIZWlnaHQ/dGh1bWJzLmhlaWdodCtNYXRoLm1pbigxMCx0aHVtYnMuZ3V0dGVyKSsncHgnOicnO2lubmVyLnN0eWxlLndpZHRoPXRodW1icy5sZW5ndGgqdGh1bWJzLnNpemUrJ3B4Jztpbm5lci5zdHlsZS5yaWdodD10b3RhbFdpZHRoPnNsaWRlci53aWR0aCYmUlRMPydhdXRvJzonJzt9O3Byb3RvLmdldFRodW1iUG9zaXRpb249ZnVuY3Rpb24odGh1bWJzKXt2YXIgc2xpZGVyPXRoaXMuc2xpZGVyLGdhbGxlcnk9dGhpcy5nYWxsZXJ5LHRodW1iTmF2PXRoaXMub3B0aW9ucy50aHVtYm5haWxzTmF2LFJUTD10aGlzLmlzUlRMKCksbGVmdD1SVEw8MD8ncmlnaHQnOidsZWZ0JyxpbmRleD11dGlscy5tb2R1bG8oZ2FsbGVyeS5pbml0aWFsTGVuZ3RoLGdhbGxlcnkuaW5kZXgpLGNlbnRlcmVkPXNsaWRlci53aWR0aCowLjUtdGh1bWJzLnNpemUqMC41LHBvc2l0aW9uPXRodW1icy5ib3VuZFtsZWZ0XS1pbmRleCp0aHVtYnMuc2l6ZSpSVEw7cG9zaXRpb249IXRodW1icy5ib3VuZFtsZWZ0XT9wb3NpdGlvbitjZW50ZXJlZDpwb3NpdGlvbisoUlRMPDAmJnRodW1iTmF2IT09J2NlbnRlcmVkJz8tY2VudGVyZWQ6MCk7cmV0dXJuIE1hdGgubWF4KHRodW1icy5ib3VuZC5yaWdodCxNYXRoLm1pbih0aHVtYnMuYm91bmQubGVmdCxwb3NpdGlvbikpO307cHJvdG8uc2V0Q2VsbFBvc2l0aW9uPWZ1bmN0aW9uKGluZGV4KXt2YXIgY2VsbD10aGlzLmNlbGxzW2luZGV4XTtjZWxsLnJlc2V0QW5pbWF0ZSgpO3V0aWxzLnRyYW5zbGF0ZSh0aGlzLnNsaWRlc1tpbmRleF0uY2hpbGRyZW5bMF0sMCwwLDEpO307cHJvdG8uc2V0U2xpZGVyUG9zaXRpb249ZnVuY3Rpb24oc2xpZGVyLHNsaWRlcyl7dmFyIFJUTD10aGlzLm9wdGlvbnMucmlnaHRUb0xlZnQscG9zWD0tc2xpZGVzLmluZGV4KnNsaWRlcy53aWR0aDtwb3NYPVJUTD8tcG9zWDpwb3NYO3NsaWRlci5yZXNldEFuaW1hdGUoKTtzbGlkZXIucG9zaXRpb24ueD1zbGlkZXIuYXR0cmFjdGlvbi54PXBvc1g7c2xpZGVyLmJvdW5kPXtsZWZ0OjAscmlnaHQ6LSh0aGlzLmdhbGxlcnkubGVuZ3RoLTEpKnNsaWRlcy53aWR0aH07aWYoUlRMKXtzbGlkZXIuYm91bmQubGVmdD0tc2xpZGVyLmJvdW5kLnJpZ2h0O3NsaWRlci5ib3VuZC5yaWdodD0wO31cbnV0aWxzLnRyYW5zbGF0ZSh0aGlzLkRPTS5zbGlkZXIscG9zWCwwKTt9O3Byb3RvLnNldEFuaW1hdGlvbj1mdW5jdGlvbigpe3ZhciBzbGlkZXI9dGhpcy5ET00uc2xpZGVyLGZyaWN0aW9uPXRoaXMub3B0aW9ucy5mcmljdGlvbixhdHRyYWN0aW9uPXRoaXMub3B0aW9ucy5hdHRyYWN0aW9uO3RoaXMuc2xpZGVyPW5ldyBBbmltYXRlKHNsaWRlcix7eDowLHk6MH0sTWF0aC5taW4oTWF0aC5tYXgoZnJpY3Rpb24uc2xpZGVyLDApLDEpLE1hdGgubWluKE1hdGgubWF4KGF0dHJhY3Rpb24uc2xpZGVyLDApLDEpKTt0aGlzLnNsaWRlci5vbignc2V0dGxlLnRvYW5pbWF0ZScsdGhpcy5zZXR0bGVTaWRlci5iaW5kKHRoaXMpKTt0aGlzLnNsaWRlci5vbigncmVuZGVyLnRvYW5pbWF0ZScsdGhpcy5yZW5kZXJTbGlkZXIuYmluZCh0aGlzKSk7dmFyIHNsaWRlcz1zbGlkZXIuY2hpbGRyZW4sbGVuZ3RoPXNsaWRlcy5sZW5ndGg7Zm9yKHZhciBpPTA7aTxsZW5ndGg7aSsrKXt0aGlzLmNlbGxzW2ldPW5ldyBBbmltYXRlKHNsaWRlc1tpXS5jaGlsZHJlblswXSx7eDowLHk6MCxzOjF9LE1hdGgubWluKE1hdGgubWF4KGZyaWN0aW9uLnNsaWRlLDApLDEpLE1hdGgubWluKE1hdGgubWF4KGF0dHJhY3Rpb24uc2xpZGUsMCksMSkpO3RoaXMuY2VsbHNbaV0ub24oJ3NldHRsZS50b2FuaW1hdGUnLHRoaXMuc2V0dGxlQ2VsbC5iaW5kKHRoaXMpKTt0aGlzLmNlbGxzW2ldLm9uKCdyZW5kZXIudG9hbmltYXRlJyx0aGlzLnJlbmRlckNlbGwuYmluZCh0aGlzKSk7fVxudGhpcy50aHVtYnM9bmV3IEFuaW1hdGUodGhpcy5ET00udGh1bWJzSW5uZXIse3g6MH0sTWF0aC5taW4oTWF0aC5tYXgoZnJpY3Rpb24udGh1bWJzLDApLDEpLE1hdGgubWluKE1hdGgubWF4KGF0dHJhY3Rpb24udGh1bWJzLDApLDEpKTt0aGlzLnRodW1icy5vbignc2V0dGxlLnRvYW5pbWF0ZScsdGhpcy5zZXR0bGVUaHVtYnMuYmluZCh0aGlzKSk7dGhpcy50aHVtYnMub24oJ3JlbmRlci50b2FuaW1hdGUnLHRoaXMucmVuZGVyVGh1bWJzLmJpbmQodGhpcykpO307cHJvdG8uc2V0dGxlU2lkZXI9ZnVuY3Rpb24oc2xpZGVyKXt2YXIgbWVkaWE7dXRpbHMuZGlzcGF0Y2hFdmVudCh0aGlzLCdtb2R1bG9ib3gnLCdzbGlkZXJTZXR0bGVkJyxzbGlkZXIucG9zaXRpb24pO2lmKHRoaXMuc3RhdGVzLm9wZW4pe3RoaXMuc2V0TWVkaWEoKTt0aGlzLnJlcGxhY2VTdGF0ZSgpO31cbmlmKHRoaXMub3B0aW9ucy5mYWRlSWZTZXR0bGUpe3ZhciBzbGlkZXM9dGhpcy5zbGlkZXM7Zm9yKHZhciBpPTA7aTxzbGlkZXMubGVuZ3RoO2krKyl7dmFyIGluZGV4PXNsaWRlc1tpXS5tZWRpYTttZWRpYT10aGlzLmdhbGxlcnlbaW5kZXhdO2lmKG1lZGlhLmRvbS5sb2FkZWQpe3RoaXMuc2hvd01lZGlhKG1lZGlhLGkpO319fX07cHJvdG8uc2V0dGxlQ2VsbD1mdW5jdGlvbihjZWxsKXt2YXIgZ2VzdHVyZT10aGlzLmdlc3R1cmU7aWYoZ2VzdHVyZS5jbG9zZUJ5KXt1dGlscy5kaXNwYXRjaEV2ZW50KHRoaXMsJ21vZHVsb2JveCcsJ3BhbllTZXR0bGVkJyxudWxsLGNlbGwucG9zaXRpb24pO31cbmlmKChnZXN0dXJlLmNsb3NlQnkmJmdlc3R1cmUuY2FuQ2xvc2U9PT1mYWxzZSl8fCFnZXN0dXJlLmNsb3NlQnkpe3V0aWxzLmRpc3BhdGNoRXZlbnQodGhpcywnbW9kdWxvYm94JywncGFuWm9vbVNldHRsZWQnLG51bGwsY2VsbC5wb3NpdGlvbik7fX07cHJvdG8uc2V0dGxlVGh1bWJzPWZ1bmN0aW9uKHRodW1icyl7dXRpbHMuZGlzcGF0Y2hFdmVudCh0aGlzLCdtb2R1bG9ib3gnLCd0aHVtYnNTZXR0bGVkJyxudWxsLHRodW1icy5wb3NpdGlvbik7fTtwcm90by5yZW5kZXJTbGlkZXI9ZnVuY3Rpb24oc2xpZGVyKXt0aGlzLnNoaWZ0U2xpZGVzKCk7dmFyIFJUTD10aGlzLmlzUlRMKCksbGVuZ3RoPXRoaXMuZ2FsbGVyeS5pbml0aWFsTGVuZ3RoLGluZGV4UG9zPS1SVEwqc2xpZGVyLnBvc2l0aW9uLngvdGhpcy5zbGlkZXMud2lkdGgsbW9kdWxvUG9zPXV0aWxzLm1vZHVsbyhsZW5ndGgsaW5kZXhQb3MpLHByb2dyZXNzPShtb2R1bG9Qb3M+bGVuZ3RoLTAuNT8wOm1vZHVsb1BvcykvKGxlbmd0aC0xKTt1dGlscy5kaXNwYXRjaEV2ZW50KHRoaXMsJ21vZHVsb2JveCcsJ3NsaWRlclByb2dyZXNzJyxudWxsLE1hdGgubWluKDEsTWF0aC5tYXgoMCxwcm9ncmVzcykpKTt9O3Byb3RvLnJlbmRlckNlbGw9ZnVuY3Rpb24oY2VsbCl7dGhpcy53aWxsQ2xvc2UoY2VsbCk7dmFyIHByb2dyZXNzO2lmKHRoaXMuZ2VzdHVyZS50eXBlPT09J3BhblknfHx0aGlzLmdlc3R1cmUuY2xvc2VCeXx8KHRoaXMuZ2VzdHVyZS50eXBlPT09J2RyYWdTbGlkZXInJiZjZWxsLnBvc2l0aW9uLnkhPT0wKSl7cHJvZ3Jlc3M9MS1NYXRoLmFicyhjZWxsLnBvc2l0aW9uLnkpLyh0aGlzLnNsaWRlci5oZWlnaHQqMC41KTt1dGlscy5kaXNwYXRjaEV2ZW50KHRoaXMsJ21vZHVsb2JveCcsJ3BhbllQcm9ncmVzcycsbnVsbCxwcm9ncmVzcyk7fVxuaWYodGhpcy5nZXN0dXJlLnR5cGUhPT0ncGFuWScmJmNlbGwucG9zaXRpb24ucyE9PTEpe3Byb2dyZXNzPWNlbGwucG9zaXRpb24uczt1dGlscy5kaXNwYXRjaEV2ZW50KHRoaXMsJ21vZHVsb2JveCcsJ3Bhblpvb21Qcm9ncmVzcycsbnVsbCxwcm9ncmVzcyk7fX07cHJvdG8ucmVuZGVyVGh1bWJzPWZ1bmN0aW9uKHRodW1icyl7dGhpcy5zaGlmdFRodW1icyh0aHVtYnMpO3ZhciBwcm9ncmVzcz10aHVtYnMuYm91bmQubGVmdCE9PXRodW1icy5ib3VuZC5yaWdodD8odGh1bWJzLmJvdW5kLmxlZnQtdGh1bWJzLnBvc2l0aW9uLngpLyh0aHVtYnMuYm91bmQubGVmdC10aHVtYnMuYm91bmQucmlnaHQpOjA7dXRpbHMuZGlzcGF0Y2hFdmVudCh0aGlzLCdtb2R1bG9ib3gnLCd0aHVtYnNQcm9ncmVzcycsbnVsbCxwcm9ncmVzcyk7fTtwcm90by50b3VjaFN0YXJ0PWZ1bmN0aW9uKGV2ZW50KXt2YXIgZWxlbWVudD1ldmVudC50YXJnZXQsdGFnTmFtZT1lbGVtZW50LnRhZ05hbWUsY2xhc3NOYW1lPWVsZW1lbnQuY2xhc3NOYW1lO2lmKGV2ZW50LndoaWNoIT09MyYmZWxlbWVudCE9PXRoaXMuYnV0dG9ucy5wbGF5KXt0aGlzLnN0b3BTbGlkZVNob3coKTt9XG5pZihldmVudC53aGljaD09PTN8fCF0aGlzLmlzRWwoZXZlbnQpfHxbJ0JVVFRPTicsJ1ZJREVPJywnSU5QVVQnLCdBJ10uaW5kZXhPZih0YWdOYW1lKT4tMSl7cmV0dXJuO31cbmlmKHRhZ05hbWU9PT0nSU1HJyYmdGhpcy5nYWxsZXJ5Lmxlbmd0aD4xKXt1dGlscy5hZGRDbGFzcyh0aGlzLkRPTS5ob2xkZXIsdGhpcy5wcmUrJy1kcmFnZ2luZycpO31cbmV2ZW50LnByZXZlbnREZWZhdWx0KCk7aWYodXRpbHMuaGFzQ2xhc3ModGhpcy5ET00uaG9sZGVyLHRoaXMucHJlKyctb3Blbi10b29sdGlwJykpe3JldHVybjt9XG5pZighdGhpcy5wb2ludGVycy5sZW5ndGgpe3RoaXMuZ2VzdHVyZS5jYW5DbG9zZT11bmRlZmluZWQ7dXRpbHMuaGFuZGxlRXZlbnRzKHRoaXMsd2luZG93LHRoaXMuZHJhZ0V2ZW50cy5tb3ZlLCd0b3VjaE1vdmUnKTt1dGlscy5oYW5kbGVFdmVudHModGhpcyx3aW5kb3csdGhpcy5kcmFnRXZlbnRzLmVuZCwndG91Y2hFbmQnKTt9XG50aGlzLmFkZFBvaW50ZXIoZXZlbnQpO2lmKGNsYXNzTmFtZS5pbmRleE9mKCctdGh1bWInKTwwKXt0aGlzLnNsaWRlci5zdG9wQW5pbWF0ZSgpO3ZhciBjZWxsPXRoaXMuZ2V0Q2VsbCgpO2lmKE1hdGgucm91bmQoY2VsbC5wb3NpdGlvbi5zKjEwMCkvMTAwIT09MXx8dGhpcy5wb2ludGVycy5sZW5ndGg9PT0yfHx0aGlzLmdlc3R1cmUuY2xvc2VCeSl7Y2VsbC5zdG9wQW5pbWF0ZSgpO319ZWxzZXt0aGlzLnRodW1icy5zdG9wQW5pbWF0ZSgpO31cbnRoaXMuZ2VzdHVyZXMoJ3N0YXJ0Jyk7fTtwcm90by50b3VjaE1vdmU9ZnVuY3Rpb24oZXZlbnQpe3RoaXMudXBkYXRlUG9pbnRlcihldmVudCk7dmFyIGdlc3R1cmU9dGhpcy5nZXN0dXJlO3ZhciBwb2ludGVyTmI9dGhpcy5wb2ludGVycy5sZW5ndGg7dmFyIGlzU2V0dGxlPXRoaXMuaXNTbGlkZXJTZXR0bGUoKTt0aGlzLnN3aXRjaFBvaW50ZXJzKCk7dGhpcy5nZXN0dXJlcygnbW92ZScpO2lmKGdlc3R1cmUudHlwZSl7dGhpc1tnZXN0dXJlLnR5cGVdKGV2ZW50KTt1dGlscy5kaXNwYXRjaEV2ZW50KHRoaXMsJ21vZHVsb2JveCcsZ2VzdHVyZS50eXBlKydNb3ZlJyxldmVudCxnZXN0dXJlKTtnZXN0dXJlLm1vdmU9dHJ1ZTt9XG5lbHNlIGlmKChwb2ludGVyTmI9PT0yJiZpc1NldHRsZSl8fE1hdGguYWJzKGdlc3R1cmUuZHgpPnRoaXMub3B0aW9ucy50aHJlc2hvbGR8fE1hdGguYWJzKGdlc3R1cmUuZHkpPnRoaXMub3B0aW9ucy50aHJlc2hvbGQpe2dlc3R1cmUuc3grPWdlc3R1cmUuZHg7Z2VzdHVyZS5zeSs9Z2VzdHVyZS5keTtnZXN0dXJlLmNhblpvb209dGhpcy5pc1pvb21hYmxlKCk7Z2VzdHVyZS5jbG9zZUJ5PWZhbHNlO2dlc3R1cmUudHlwZT1NYXRoLmFicyhnZXN0dXJlLmR4KTxNYXRoLmFicyhnZXN0dXJlLmR5KS8yP2ZhbHNlOidkcmFnU2xpZGVyJztnZXN0dXJlLnR5cGU9dGhpcy5vcHRpb25zLmRyYWdUb0Nsb3NlJiYhZ2VzdHVyZS50eXBlJiZpc1NldHRsZT8ncGFuWSc6Z2VzdHVyZS50eXBlO2dlc3R1cmUudHlwZT0odGhpcy5vcHRpb25zLnBpbmNoVG9ab29tfHx0aGlzLnN0YXRlcy56b29tKSYmZ2VzdHVyZS5jYW5ab29tJiZpc1NldHRsZSYmKHBvaW50ZXJOYj09PTJ8fHRoaXMuc3RhdGVzLnpvb20pPydwYW5ab29tJzpnZXN0dXJlLnR5cGU7Z2VzdHVyZS50eXBlPXRoaXMub3B0aW9ucy5waW5jaFRvQ2xvc2UmJmdlc3R1cmUuc2NhbGU8MSYmaXNTZXR0bGUmJnBvaW50ZXJOYj09PTI/J3Bhblpvb20nOmdlc3R1cmUudHlwZTtnZXN0dXJlLnR5cGU9ZXZlbnQudGFyZ2V0LmNsYXNzTmFtZS5pbmRleE9mKCctdGh1bWInKT4tMT8nZHJhZ1RodW1icyc6Z2VzdHVyZS50eXBlO2lmKGdlc3R1cmUudHlwZT09PSdkcmFnU2xpZGVyJyl7dGhpcy5zZXRNZWRpYSgpO31cbmlmKFsnZHJhZ1NsaWRlcicsJ2RyYWdUaHVtYnMnXS5pbmRleE9mKGdlc3R1cmUudHlwZSk+LTEpe3ZhciBjZWxsPXRoaXMuZ2V0Q2VsbCgpO2NlbGwuc3RhcnRBbmltYXRlKCk7Y2VsbC5yZWxlYXNlRHJhZygpO2NlbGwuYW5pbWF0ZVRvKHt4OjAseTowLHM6MX0pO31cbmlmKGdlc3R1cmUudHlwZSE9PSdkcmFnU2xpZGVyJyl7dmFyIHNsaWRlcj10aGlzLnNsaWRlcixzbGlkZXM9dGhpcy5zbGlkZXM7dmFyIFJUTD10aGlzLmlzUlRMKCk7aWYoLVJUTCpzbGlkZXIucG9zaXRpb24ueCE9PXNsaWRlcy5pbmRleCpzbGlkZXMud2lkdGgpe3NsaWRlci5zdGFydEFuaW1hdGUoKTtzbGlkZXIucmVsZWFzZURyYWcoKTt9fVxuaWYoZ2VzdHVyZS50eXBlKXt0aGlzLnBhdXNlVmlkZW8oKTt1dGlscy5kaXNwYXRjaEV2ZW50KHRoaXMsJ21vZHVsb2JveCcsZ2VzdHVyZS50eXBlKydTdGFydCcsZXZlbnQsZ2VzdHVyZSk7aWYodGhpcy5nYWxsZXJ5Lmxlbmd0aD4xfHxnZXN0dXJlLnR5cGUhPT0nZHJhZ1NsaWRlcicpe3V0aWxzLmFkZENsYXNzKHRoaXMuRE9NLmhvbGRlcix0aGlzLnByZSsnLWRyYWdnaW5nJyk7fX19fTtwcm90by50b3VjaEVuZD1mdW5jdGlvbihldmVudCl7dGhpcy5kZWxldGVQb2ludGVyKGV2ZW50KTtpZighdGhpcy5wb2ludGVycy5sZW5ndGgpe3V0aWxzLnJlbW92ZUNsYXNzKHRoaXMuRE9NLmhvbGRlcix0aGlzLnByZSsnLWRyYWdnaW5nJyk7dXRpbHMuaGFuZGxlRXZlbnRzKHRoaXMsd2luZG93LHRoaXMuZHJhZ0V2ZW50cy5tb3ZlLCd0b3VjaE1vdmUnLGZhbHNlKTt1dGlscy5oYW5kbGVFdmVudHModGhpcyx3aW5kb3csdGhpcy5kcmFnRXZlbnRzLmVuZCwndG91Y2hFbmQnLGZhbHNlKTtpZih0aGlzLmlzU2xpZGVyU2V0dGxlKCkpe3ZhciBjbGFzc05hbWU9ZXZlbnQudGFyZ2V0LmNsYXNzTmFtZTtpZih1dGlscy5oYXNDbGFzcyhldmVudC50YXJnZXQsdGhpcy5wcmUrJy12aWRlbycpKXt0aGlzLmFwcGVuZFZpZGVvKCk7fWVsc2UgaWYodGhpcy5vcHRpb25zLnRhcFRvQ2xvc2UmJiF0aGlzLnN0YXRlcy56b29tJiYoY2xhc3NOYW1lPT09dGhpcy5wcmUrJy1pdGVtLWlubmVyJ3x8Y2xhc3NOYW1lPT09dGhpcy5wcmUrJy10b3AtYmFyJykmJk1hdGguYWJzKHRoaXMuZ2VzdHVyZS5keCk8dGhpcy5vcHRpb25zLnRocmVzaG9sZCl7dGhpcy5jbG9zZSgpO3JldHVybjt9XG5pZihldmVudC50YXJnZXQudGFnTmFtZT09PSdJTUcnKXt0aGlzLmRvdWJsZVRhcChldmVudCk7fX1cbmlmKHRoaXMub3B0aW9ucy50aHVtYm5haWxzJiYhdGhpcy5nZXN0dXJlLm1vdmUpe3RoaXMudGh1bWJDbGljayhldmVudCk7fVxudmFyIGdlc3R1cmVFbmQ9dGhpcy5nZXN0dXJlLnR5cGUrJ0VuZCc7aWYodGhpcy5nZXN0dXJlLnR5cGUmJnR5cGVvZiB0aGlzW2dlc3R1cmVFbmRdPT09J2Z1bmN0aW9uJyl7dGhpc1tnZXN0dXJlRW5kXShldmVudCk7dXRpbHMuZGlzcGF0Y2hFdmVudCh0aGlzLCdtb2R1bG9ib3gnLGdlc3R1cmVFbmQsZXZlbnQsdGhpcy5nZXN0dXJlKTt9XG50aGlzLmdlc3R1cmUudHlwZT10aGlzLmdlc3R1cmUubW92ZT1mYWxzZTtpZighdGhpcy5zdGF0ZXMub3Blbil7cmV0dXJuO31cbnZhciBjZWxsPXRoaXMuZ2V0Q2VsbCgpO2lmKCFjZWxsLnNldHRsZSl7Y2VsbC5zdGFydEFuaW1hdGUoKTtjZWxsLnJlbGVhc2VEcmFnKCk7fVxudmFyIHNsaWRlcj10aGlzLnNsaWRlcjtpZighc2xpZGVyLnNldHRsZSl7c2xpZGVyLnN0YXJ0QW5pbWF0ZSgpO3NsaWRlci5yZWxlYXNlRHJhZygpO319fTtwcm90by5zd2l0Y2hQb2ludGVycz1mdW5jdGlvbigpe2lmKHRoaXMuZ2VzdHVyZS50eXBlPT09J3Bhblpvb20nJiZ0aGlzLnBvaW50ZXJzLmxlbmd0aD09PTEmJnRoaXMuZ2VzdHVyZS5kaXN0YW5jZSE9PTApe3ZhciBtb3Zpbmc9dGhpcy5nZXRDZWxsKCk7bW92aW5nLnN0b3BBbmltYXRlKCk7bW92aW5nLnN0YXJ0QW5pbWF0ZSgpO3RoaXMuZ2VzdHVyZS5tb3ZlPWZhbHNlO3RoaXMuZ2VzdHVyZXMoJ3N0YXJ0Jyk7dGhpcy5nZXN0dXJlcygnbW92ZScpO319O3Byb3RvLmRvdWJsZVRhcD1mdW5jdGlvbihldmVudCl7ZXZlbnQucHJldmVudERlZmF1bHQoKTt2YXIgdG91Y2hlcz10aGlzLm1hcFBvaW50ZXIoZXZlbnQpLGNsaWVudFg9dG91Y2hlc1swXS5jbGllbnRYLGNsaWVudFk9dG91Y2hlc1swXS5jbGllbnRZO2lmKHR5cGVvZiB0aGlzLnRhcCE9PSd1bmRlZmluZWQnJiYrbmV3IERhdGUoKS10aGlzLnRhcC5kZWxheTwzNTAmJk1hdGguYWJzKHRoaXMudGFwLmRlbHRhWC1jbGllbnRYKTwzMCYmTWF0aC5hYnModGhpcy50YXAuZGVsdGFZLWNsaWVudFkpPDMwKXtpZih0aGlzLnN0YXRlcy50YXBJZGxlKXtjbGVhclRpbWVvdXQodGhpcy5zdGF0ZXMudGFwSWRsZSk7fVxuaWYodGhpcy5vcHRpb25zLmRvdWJsZVRhcFRvWm9vbSl7dGhpcy56b29tVG8oY2xpZW50WCxjbGllbnRZKTt9XG50aGlzLnRhcD11bmRlZmluZWQ7fWVsc2V7aWYodGhpcy5icm93c2VyLnRvdWNoRGV2aWNlJiZ0aGlzLm9wdGlvbnMudGltZVRvSWRsZSYmIXRoaXMuc3RhdGVzLmlkbGUpe3RoaXMuc3RhdGVzLnRhcElkbGU9c2V0VGltZW91dChmdW5jdGlvbigpe3ZhciBtZXRob2Q9IXV0aWxzLmhhc0NsYXNzKHRoaXMuRE9NLmhvbGRlcix0aGlzLnByZSsnLWlkbGUnKT8nYWRkJzoncmVtb3ZlJzt1dGlsc1ttZXRob2QrJ0NsYXNzJ10odGhpcy5ET00uaG9sZGVyLHRoaXMucHJlKyctaWRsZScpO30uYmluZCh0aGlzKSwzNTApO31cbnRoaXMudGFwPXtkZWxheTorbmV3IERhdGUoKSxkZWx0YVg6dG91Y2hlc1swXS5jbGllbnRYLGRlbHRhWTp0b3VjaGVzWzBdLmNsaWVudFl9O319O3Byb3RvLmlzU2xpZGVyU2V0dGxlPWZ1bmN0aW9uKCl7aWYodGhpcy5nZXN0dXJlLnR5cGUpe3JldHVybiBmYWxzZTt9XG52YXIgUlRMPXRoaXMuaXNSVEwoKSxzbGlkZXM9dGhpcy5zbGlkZXMsd2lkdGg9c2xpZGVzLndpZHRoLHRvU2V0dGxlPU1hdGguYWJzKFJUTCp0aGlzLnNsaWRlci5wb3NpdGlvbi54K3NsaWRlcy5pbmRleCp3aWR0aCkvd2lkdGgqMTAwO3JldHVybiB0b1NldHRsZTw9Mz90cnVlOmZhbHNlO307cHJvdG8ubWFwUG9pbnRlcj1mdW5jdGlvbihldmVudCl7cmV0dXJuIGV2ZW50LnRvdWNoZXM/ZXZlbnQuY2hhbmdlZFRvdWNoZXM6W2V2ZW50XTt9O3Byb3RvLmFkZFBvaW50ZXI9ZnVuY3Rpb24oZXZlbnQpe3ZhciBwb2ludGVycz10aGlzLm1hcFBvaW50ZXIoZXZlbnQpO2Zvcih2YXIgaT0wO2k8cG9pbnRlcnMubGVuZ3RoO2krKyl7aWYodGhpcy5wb2ludGVycy5sZW5ndGg8MiYmWydkcmFnU2xpZGVyJywncGFuWScsJ2RyYWdUaHVtYnMnXS5pbmRleE9mKHRoaXMuZ2VzdHVyZS50eXBlKT09PS0xKXt2YXIgZXY9cG9pbnRlcnNbaV0saWQ9ZXYucG9pbnRlcklkIT09dW5kZWZpbmVkP2V2LnBvaW50ZXJJZDpldi5pZGVudGlmaWVyO2lmKCF0aGlzLmdldFBvaW50ZXIoaWQpKXt0aGlzLnBvaW50ZXJzW3RoaXMucG9pbnRlcnMubGVuZ3RoXT17aWQ6aWQseDpNYXRoLnJvdW5kKGV2LmNsaWVudFgpLHk6TWF0aC5yb3VuZChldi5jbGllbnRZKX07fX19fTtwcm90by51cGRhdGVQb2ludGVyPWZ1bmN0aW9uKGV2ZW50KXt2YXIgcG9pbnRlcnM9dGhpcy5tYXBQb2ludGVyKGV2ZW50KTtmb3IodmFyIGk9MDtpPHBvaW50ZXJzLmxlbmd0aDtpKyspe3ZhciBldj1wb2ludGVyc1tpXSxpZD1ldi5wb2ludGVySWQhPT11bmRlZmluZWQ/ZXYucG9pbnRlcklkOmV2LmlkZW50aWZpZXIscHQ9dGhpcy5nZXRQb2ludGVyKGlkKTtpZihwdCl7cHQueD1NYXRoLnJvdW5kKGV2LmNsaWVudFgpO3B0Lnk9TWF0aC5yb3VuZChldi5jbGllbnRZKTt9fX07cHJvdG8uZGVsZXRlUG9pbnRlcj1mdW5jdGlvbihldmVudCl7dmFyIHBvaW50ZXJzPXRoaXMubWFwUG9pbnRlcihldmVudCk7Zm9yKHZhciBpPTA7aTxwb2ludGVycy5sZW5ndGg7aSsrKXt2YXIgZXY9cG9pbnRlcnNbaV0saWQ9ZXYucG9pbnRlcklkIT09dW5kZWZpbmVkP2V2LnBvaW50ZXJJZDpldi5pZGVudGlmaWVyO2Zvcih2YXIgcD0wO3A8dGhpcy5wb2ludGVycy5sZW5ndGg7cCsrKXtpZih0aGlzLnBvaW50ZXJzW3BdLmlkPT09aWQpe3RoaXMucG9pbnRlcnMuc3BsaWNlKHAsMSk7fX19fTtwcm90by5nZXRQb2ludGVyPWZ1bmN0aW9uKGlkKXtmb3IodmFyIGsgaW4gdGhpcy5wb2ludGVycyl7aWYodGhpcy5wb2ludGVyc1trXS5pZD09PWlkKXtyZXR1cm4gdGhpcy5wb2ludGVyc1trXTt9fVxucmV0dXJuIG51bGw7fTtwcm90by5nZXN0dXJlcz1mdW5jdGlvbih0eXBlKXt2YXIgZz10aGlzLmdlc3R1cmUscG9pbnRlcnM9dGhpcy5wb2ludGVycyxkaXN0YW5jZTtpZighcG9pbnRlcnMubGVuZ3RoKXtyZXR1cm47fVxuZy5kaXJlY3Rpb249Zy54P3BvaW50ZXJzWzBdLng+Zy54PzE6LTE6MDtnLng9cG9pbnRlcnNbMF0ueDtnLnk9cG9pbnRlcnNbMF0ueTtpZihwb2ludGVycy5sZW5ndGg9PT0yKXt2YXIgeDI9cG9pbnRlcnNbMV0ueCx5Mj1wb2ludGVyc1sxXS55O2Rpc3RhbmNlPXRoaXMuZ2V0RGlzdGFuY2UoW2cueCxnLnldLFt4Mix5Ml0pO2cueD1nLngtKGcueC14MikvMjtnLnk9Zy55LShnLnkteTIpLzI7fVxuaWYodHlwZT09PSdzdGFydCcpe2cuZHg9MDtnLmR5PTA7Zy5zeD1nLng7Zy5zeT1nLnk7Zy5kaXN0YW5jZT1kaXN0YW5jZT9kaXN0YW5jZTowO31lbHNle2cuZHg9Zy54LWcuc3g7Zy5keT1nLnktZy5zeTtnLnNjYWxlPWRpc3RhbmNlJiZnLmRpc3RhbmNlP2Rpc3RhbmNlL2cuZGlzdGFuY2U6MTt9fTtwcm90by5nZXREaXN0YW5jZT1mdW5jdGlvbihwMSxwMil7dmFyIHg9cDJbMF0tcDFbMF0seT1wMlsxXS1wMVsxXTtyZXR1cm4gTWF0aC5zcXJ0KCh4KngpKyh5KnkpKTt9O3Byb3RvLnBhblk9ZnVuY3Rpb24oKXt2YXIgbW92aW5nPXRoaXMuZ2V0Q2VsbCgpO21vdmluZy5zdGFydEFuaW1hdGUoKTttb3ZpbmcudXBkYXRlRHJhZyh7eDptb3ZpbmcucG9zaXRpb24ueCx5Om1vdmluZy5zdGFydC55K3RoaXMuZ2VzdHVyZS5keSxzOm1vdmluZy5wb3NpdGlvbi5zfSk7fTtwcm90by5wYW5ZRW5kPWZ1bmN0aW9uKCl7dmFyIHBvc1k9MCxtb3Zpbmc9dGhpcy5nZXRDZWxsKCksaGVpZ2h0PXRoaXMuc2xpZGVyLmhlaWdodCxyZXN0aW5nPW1vdmluZy5yZXN0aW5nLnk7aWYoMS1NYXRoLmFicyhyZXN0aW5nKS8oaGVpZ2h0KjAuNSk8MC44KXtwb3NZPU1hdGguYWJzKHJlc3RpbmcpPGhlaWdodCowLjU/TWF0aC5hYnMocmVzdGluZykvcmVzdGluZypoZWlnaHQqMC41OnJlc3Rpbmc7dGhpcy5jbG9zZSgpO21vdmluZy5hbmltYXRlVG8oe3g6MCx5OnBvc1ksczpwb3NZP21vdmluZy5yZXN0aW5nLnM6MX0pO21vdmluZy5zdGFydEFuaW1hdGUoKTttb3ZpbmcucmVsZWFzZURyYWcoKTt9fTtwcm90by5wYW5ab29tPWZ1bmN0aW9uKCl7dmFyIG1vdmluZz10aGlzLmdldENlbGwoKSxnZXN0dXJlPXRoaXMuZ2VzdHVyZSxib3VuZD10aGlzLm1lZGlhVmlld3BvcnQobW92aW5nLnBvc2l0aW9uLnMpLG1pblpvb209dGhpcy5vcHRpb25zLnBpbmNoVG9DbG9zZSYmZ2VzdHVyZS5jYW5DbG9zZT8wLjE6MC42LHNjYWxlPU1hdGgubWluKHRoaXMub3B0aW9ucy5tYXhab29tKjEuNSxNYXRoLm1heChtaW5ab29tLG1vdmluZy5zdGFydC5zKmdlc3R1cmUuc2NhbGUpKSxwb3NYPW1vdmluZy5zdGFydC54K2dlc3R1cmUuZHgscG9zWT1tb3Zpbmcuc3RhcnQueStnZXN0dXJlLmR5LGNlbnRlclg9Z2VzdHVyZS5zeC10aGlzLnNsaWRlci53aWR0aCowLjUsY2VudGVyWT1nZXN0dXJlLnN5LXRoaXMuc2xpZGVyLmhlaWdodCowLjU7aWYoIWdlc3R1cmUuY2FuWm9vbXx8KCF0aGlzLm9wdGlvbnMucGluY2hUb1pvb20mJiF0aGlzLnN0YXRlcy56b29tKSl7c2NhbGU9TWF0aC5taW4oMSxzY2FsZSk7fVxuaWYoIXRoaXMub3B0aW9ucy5waW5jaFRvWm9vbSYmdGhpcy5zdGF0ZXMuem9vbSl7c2NhbGU9bW92aW5nLnBvc2l0aW9uLnM7fVxuaWYoIWdlc3R1cmUubW92ZSYmdGhpcy5wb2ludGVycy5sZW5ndGg9PT0xKXttb3Zpbmcuc3RhcnQueCs9cG9zWD5ib3VuZC5sZWZ0P3Bvc1gtYm91bmQubGVmdDpwb3NYPGJvdW5kLnJpZ2h0P3Bvc1gtYm91bmQucmlnaHQ6MDttb3Zpbmcuc3RhcnQueSs9cG9zWT5ib3VuZC5ib3R0b20/cG9zWS1ib3VuZC5ib3R0b206cG9zWTxib3VuZC50b3A/cG9zWS1ib3VuZC50b3A6MDt9XG5wb3NYPWdlc3R1cmUuZHgrY2VudGVyWCsobW92aW5nLnN0YXJ0LngtY2VudGVyWCkqKHNjYWxlL21vdmluZy5zdGFydC5zKTtwb3NZPWdlc3R1cmUuZHkrY2VudGVyWSsobW92aW5nLnN0YXJ0LnktY2VudGVyWSkqKHNjYWxlL21vdmluZy5zdGFydC5zKTtpZih0aGlzLnBvaW50ZXJzLmxlbmd0aD09PTEpe3Bvc1g9cG9zWD5ib3VuZC5sZWZ0Pyhwb3NYK2JvdW5kLmxlZnQpKjAuNTpwb3NYPGJvdW5kLnJpZ2h0Pyhwb3NYK2JvdW5kLnJpZ2h0KSowLjU6cG9zWDtwb3NZPXBvc1k+Ym91bmQuYm90dG9tPyhwb3NZK2JvdW5kLmJvdHRvbSkqMC41OnBvc1k8Ym91bmQudG9wPyhwb3NZK2JvdW5kLnRvcCkqMC41OnBvc1k7fVxubW92aW5nLnN0YXJ0QW5pbWF0ZSgpO21vdmluZy51cGRhdGVEcmFnKHt4OnBvc1gseTpwb3NZLHM6c2NhbGV9KTt0aGlzLnVwZGF0ZVpvb20oc2NhbGUpO307cHJvdG8ucGFuWm9vbUVuZD1mdW5jdGlvbigpe3ZhciBtb3Zpbmc9dGhpcy5nZXRDZWxsKCksZ2VzdHVyZT10aGlzLmdlc3R1cmUsc2NhbGU9bW92aW5nLnJlc3Rpbmcucz50aGlzLm9wdGlvbnMubWF4Wm9vbT90aGlzLm9wdGlvbnMubWF4Wm9vbTptb3ZpbmcucmVzdGluZy5zPDE/MTptb3ZpbmcucmVzdGluZy5zLGJvdW5kPXRoaXMubWVkaWFWaWV3cG9ydChzY2FsZSkscG9zWCxwb3NZO2lmKE1hdGgucm91bmQobW92aW5nLnJlc3RpbmcucyoxMCkvMTA+dGhpcy5vcHRpb25zLm1heFpvb20pe3ZhciBjZW50ZXJYPWdlc3R1cmUuZGlzdGFuY2U/Z2VzdHVyZS5zeC10aGlzLnNsaWRlci53aWR0aCowLjU6MDt2YXIgY2VudGVyWT1nZXN0dXJlLmRpc3RhbmNlP2dlc3R1cmUuc3ktdGhpcy5zbGlkZXIuaGVpZ2h0KjAuNTowO3Bvc1g9Z2VzdHVyZS5keCtjZW50ZXJYKyhtb3Zpbmcuc3RhcnQueC1jZW50ZXJYKSooc2NhbGUvbW92aW5nLnN0YXJ0LnMpO3Bvc1k9Z2VzdHVyZS5keStjZW50ZXJZKyhtb3Zpbmcuc3RhcnQueS1jZW50ZXJZKSooc2NhbGUvbW92aW5nLnN0YXJ0LnMpO3Bvc1g9cG9zWD5ib3VuZC5sZWZ0P2JvdW5kLmxlZnQ6cG9zWDxib3VuZC5yaWdodD9ib3VuZC5yaWdodDpwb3NYO3Bvc1k9cG9zWT5ib3VuZC5ib3R0b20/Ym91bmQuYm90dG9tOnBvc1k8Ym91bmQudG9wP2JvdW5kLnRvcDpwb3NZO31lbHNle3Bvc1g9bW92aW5nLnJlc3RpbmcueD5ib3VuZC5sZWZ0P2JvdW5kLmxlZnQ6bW92aW5nLnJlc3RpbmcueDxib3VuZC5yaWdodD9ib3VuZC5yaWdodDp1bmRlZmluZWQ7cG9zWT1tb3ZpbmcucmVzdGluZy55PmJvdW5kLmJvdHRvbT9ib3VuZC5ib3R0b206bW92aW5nLnJlc3RpbmcueTxib3VuZC50b3A/Ym91bmQudG9wOnVuZGVmaW5lZDt9XG5pZih0aGlzLm9wdGlvbnMucGluY2hUb0Nsb3NlJiZtb3ZpbmcucmVzdGluZy5zPDAuOCYmZ2VzdHVyZS5jYW5DbG9zZSl7c2NhbGU9bW92aW5nLnJlc3RpbmcuczwwLjM/bW92aW5nLnJlc3RpbmcuczowLjE1O3Bvc1g9bW92aW5nLnJlc3RpbmcueDtwb3NZPW1vdmluZy5yZXN0aW5nLnk7dGhpcy5jbG9zZSgpO31cbm1vdmluZy5hbmltYXRlVG8oe3g6cG9zWCx5OnBvc1ksczpzY2FsZSE9PW1vdmluZy5yZXN0aW5nLnM/c2NhbGU6dW5kZWZpbmVkfSk7bW92aW5nLnN0YXJ0QW5pbWF0ZSgpO21vdmluZy5yZWxlYXNlRHJhZygpO3RoaXMudXBkYXRlWm9vbShtb3ZpbmcucmVzdGluZy5zKTt9O3Byb3RvLmRyYWdUaHVtYnM9ZnVuY3Rpb24oKXt2YXIgbW92aW5nPXRoaXMudGh1bWJzLGJvdW5kPW1vdmluZy5ib3VuZCxwb3NYPW1vdmluZy5zdGFydC54K3RoaXMuZ2VzdHVyZS5keDtpZighdGhpcy5nZXN0dXJlLm1vdmUpe21vdmluZy5zdGFydC54Kz1wb3NYPmJvdW5kLmxlZnQ/cG9zWC1ib3VuZC5sZWZ0OnBvc1g8Ym91bmQucmlnaHQ/cG9zWC1ib3VuZC5yaWdodDowO3Bvc1g9bW92aW5nLnN0YXJ0LngrdGhpcy5nZXN0dXJlLmR4O31cbnBvc1g9cG9zWD5ib3VuZC5sZWZ0Pyhwb3NYK2JvdW5kLmxlZnQpKjAuNTpwb3NYPGJvdW5kLnJpZ2h0Pyhwb3NYK2JvdW5kLnJpZ2h0KSowLjU6cG9zWDttb3Zpbmcuc3RhcnRBbmltYXRlKCk7bW92aW5nLmF0dHJhY3Rpb24ueD11bmRlZmluZWQ7bW92aW5nLnVwZGF0ZURyYWcoe3g6cG9zWH0pO307cHJvdG8uZHJhZ1RodW1ic0VuZD1mdW5jdGlvbigpe3ZhciBtb3Zpbmc9dGhpcy50aHVtYnMsYm91bmQ9bW92aW5nLmJvdW5kLHBvc1g9bW92aW5nLnJlc3RpbmcueDtwb3NYPXBvc1g+Ym91bmQubGVmdD9ib3VuZC5sZWZ0OnBvc1g8Ym91bmQucmlnaHQ/Ym91bmQucmlnaHQ6cG9zWDtpZihwb3NYIT09bW92aW5nLnJlc3RpbmcueCl7bW92aW5nLmFuaW1hdGVUbyh7eDpwb3NYfSk7fVxubW92aW5nLnN0YXJ0QW5pbWF0ZSgpO21vdmluZy5yZWxlYXNlRHJhZygpO307cHJvdG8uZHJhZ1NsaWRlcj1mdW5jdGlvbigpe2lmKHRoaXMuZ2FsbGVyeS5sZW5ndGg9PT0xKXtyZXR1cm47fVxudmFyIG1vdmluZz10aGlzLnNsaWRlcixwb3NYPW1vdmluZy5zdGFydC54K3RoaXMuZ2VzdHVyZS5keDtpZighdGhpcy5zdGF0ZXMubG9vcCl7dmFyIGJvdW5kPW1vdmluZy5ib3VuZDtpZighdGhpcy5nZXN0dXJlLm1vdmUpe21vdmluZy5zdGFydC54Kz1wb3NYPmJvdW5kLmxlZnQ/cG9zWC1ib3VuZC5sZWZ0OnBvc1g8Ym91bmQucmlnaHQ/cG9zWC1ib3VuZC5yaWdodDowO3Bvc1g9bW92aW5nLnN0YXJ0LngrdGhpcy5nZXN0dXJlLmR4O31cbnBvc1g9cG9zWD5ib3VuZC5sZWZ0Pyhwb3NYK2JvdW5kLmxlZnQpKjAuNTpwb3NYPGJvdW5kLnJpZ2h0Pyhwb3NYK2JvdW5kLnJpZ2h0KSowLjU6cG9zWDt9XG5tb3Zpbmcuc3RhcnRBbmltYXRlKCk7bW92aW5nLnVwZGF0ZURyYWcoe3g6cG9zWH0pO307cHJvdG8uZHJhZ1NsaWRlckVuZD1mdW5jdGlvbigpe2lmKHRoaXMuZ2FsbGVyeS5sZW5ndGg9PT0xKXtyZXR1cm47fVxudmFyIG1vdmluZz10aGlzLnNsaWRlcixzbGlkZXM9dGhpcy5zbGlkZXMsb2xkSW5kZXg9c2xpZGVzLmluZGV4LFJUTD10aGlzLmlzUlRMKCkscmVzdGluZ1g9bW92aW5nLnJlc3RpbmcueCxwb3NpdGlvblg9bW92aW5nLnBvc2l0aW9uLng7dGhpcy5nZXRSZXN0aW5nSW5kZXgocG9zaXRpb25YLHJlc3RpbmdYKTtpZihvbGRJbmRleCE9PXNsaWRlcy5pbmRleCl7dGhpcy51cGRhdGVNZWRpYUluZm8oKTt9XG50aGlzLnNsaWRlci5hbmltYXRlVG8oe3g6LVJUTCpzbGlkZXMuaW5kZXgqc2xpZGVzLndpZHRoLHk6dW5kZWZpbmVkLHM6dW5kZWZpbmVkfSk7bW92aW5nLnN0YXJ0QW5pbWF0ZSgpO21vdmluZy5yZWxlYXNlRHJhZygpO307cHJvdG8uZ2V0UmVzdGluZ0luZGV4PWZ1bmN0aW9uKHBvc2l0aW9uWCxyZXN0aW5nWCl7dmFyIGRpcmVjdGlvbj10aGlzLmdlc3R1cmUuZGlyZWN0aW9uLGdhbGxlcnk9dGhpcy5nYWxsZXJ5LHNsaWRlcz10aGlzLnNsaWRlcyxkZWx0YVg9dGhpcy5nZXN0dXJlLmR4LFJUTD10aGlzLmlzUlRMKCksaW5kZXg9TWF0aC5yb3VuZCgtUlRMKnBvc2l0aW9uWC9zbGlkZXMud2lkdGgpLG1vdmVkPU1hdGguYWJzKHJlc3RpbmdYLXBvc2l0aW9uWCk7aWYoTWF0aC5hYnMoZGVsdGFYKTxzbGlkZXMud2lkdGgqMC41JiZtb3ZlZCl7aWYoZGVsdGFYPjAmJmRpcmVjdGlvbj4wKXtpbmRleC09MSpSVEw7fWVsc2UgaWYoZGVsdGFYPDAmJmRpcmVjdGlvbjwwKXtpbmRleCs9MSpSVEw7fX1cbnZhciBnYXA9TWF0aC5tYXgoLTEsTWF0aC5taW4oMSxpbmRleC1zbGlkZXMuaW5kZXgpKTtpZighdGhpcy5zdGF0ZXMubG9vcCYmKChnYWxsZXJ5LmluZGV4K2dhcDwwKXx8KGdhbGxlcnkuaW5kZXgrZ2FwPmdhbGxlcnkubGVuZ3RoLTEpKSl7cmV0dXJuO31cbnNsaWRlcy5pbmRleCs9Z2FwO307cHJvdG8uc2hpZnRTbGlkZXM9ZnVuY3Rpb24oKXt2YXIgc2xpZGVzPXRoaXMuc2xpZGVzLGdhbGxlcnk9dGhpcy5nYWxsZXJ5LGxvb3A9dGhpcy5zdGF0ZXMubG9vcCxSVEw9dGhpcy5pc1JUTCgpLGZyb209UlRMKk1hdGgucm91bmQoLXRoaXMuc2xpZGVyLnBvc2l0aW9uLngvc2xpZGVzLndpZHRoKS0yLHRvPWZyb20rNTtpZighbG9vcCYmdG8+Z2FsbGVyeS5pbml0aWFsTGVuZ3RoLTEpe2Zyb209Z2FsbGVyeS5pbml0aWFsTGVuZ3RoLTU7dG89ZnJvbSs1O31cbmlmKCFsb29wJiZmcm9tPDApe2Zyb209MDt0bz01O31cbmZvcih2YXIgaT1mcm9tO2k8dG87aSsrKXt2YXIgcG9zaXRpb249UlRMKmkqc2xpZGVzLndpZHRoLHNsaWRlX2luZGV4PXV0aWxzLm1vZHVsbyhzbGlkZXMubGVuZ3RoLGkpLHNsaWRlPXNsaWRlc1tzbGlkZV9pbmRleF07aWYoc2xpZGUuaW5kZXghPT1pfHxzbGlkZS5wb3NpdGlvbiE9PXBvc2l0aW9uKXtzbGlkZS5pbmRleD1pO3NsaWRlLnBvc2l0aW9uPXBvc2l0aW9uO3NsaWRlLnN0eWxlLmxlZnQ9cG9zaXRpb24rJ3B4Jzt9fVxuaWYodGhpcy5zdGF0ZXMub3Blbil7dGhpcy5zZXRNZWRpYSgzKTt9fTtwcm90by5zaGlmdFRodW1icz1mdW5jdGlvbih0aHVtYnMpe3ZhciBjaGlsZD10aGlzLkRPTS50aHVtYnNJbm5lci5jaGlsZHJlbixzbGlkZXI9dGhpcy5zbGlkZXIsZ2FsbGVyeT10aGlzLmdhbGxlcnksUlRMPXRoaXMuaXNSVEwoKSxsZW5ndGg9Y2hpbGQubGVuZ3RoLGluZGV4PXV0aWxzLm1vZHVsbyhnYWxsZXJ5LmluaXRpYWxMZW5ndGgsZ2FsbGVyeS5pbmRleCksd2lkdGg9dGh1bWJzLnNpemUqKGxlbmd0aCkqMC41LGNlbnRlcj1NYXRoLnJvdW5kKCgtUlRMKnRodW1icy5wb3NpdGlvbi54K1JUTCp3aWR0aCowLjUpL3RodW1icy5zaXplKSxmcm9tPU1hdGgubWF4KDAsY2VudGVyLU1hdGguZmxvb3IobGVuZ3RoLzIpKSx0bz1mcm9tK2xlbmd0aDt2YXIgdG9sZXJhbmNlPXNsaWRlci53aWR0aCowLjUsYm91bmRMZWZ0PXRodW1icy5wb3NpdGlvbi54K3RvbGVyYW5jZSxib3VuZFJpZ2h0PXRodW1icy5wb3NpdGlvbi54LXNsaWRlci53aWR0aC10b2xlcmFuY2U7aWYodG8+Z2FsbGVyeS5pbml0aWFsTGVuZ3RoKXt0bz1nYWxsZXJ5LmluaXRpYWxMZW5ndGg7ZnJvbT10by1sZW5ndGg7fVxuaWYodG89PT1nYWxsZXJ5LmluaXRpYWxMZW5ndGgtMSYmZnJvbS10bzxsZW5ndGgpe2Zyb209Z2FsbGVyeS5pbml0aWFsTGVuZ3RoLWxlbmd0aDt9XG5mb3IodmFyIGk9ZnJvbTtpPHRvO2krKyl7dmFyIHRodW1iPWNoaWxkW3V0aWxzLm1vZHVsbyhsZW5ndGgsaSldLHBvc2l0aW9uPVJUTCppKnRodW1icy5zaXplK3RodW1icy5ndXR0ZXIqMC41LGNsYXNzTmFtZT10aGlzLnByZSsnLWFjdGl2ZS10aHVtYicsaXNBY3RpdmU9dXRpbHMuaGFzQ2xhc3ModGh1bWIsY2xhc3NOYW1lKTtpZih0aHVtYi5pbmRleCE9PWl8fHRodW1iLnBvc2l0aW9uIT09cG9zaXRpb24pe3RodW1iLmluZGV4PWk7dGh1bWIucG9zaXRpb249cG9zaXRpb247dGh1bWIuc3R5bGUubGVmdD1wb3NpdGlvbisncHgnO31cbnRoaXMuc2V0VGh1bWJTaXplKHRodW1iLHRodW1icyk7aWYoLXRodW1iLnBvc2l0aW9uPD1ib3VuZExlZnQmJi10aHVtYi5wb3NpdGlvbj49Ym91bmRSaWdodCYmdGh1bWIubWVkaWEhPT1pKXt0aGlzLmxvYWRUaHVtYih0aHVtYixpKTt9XG5pZihpc0FjdGl2ZSYmaW5kZXghPT1pKXt1dGlscy5yZW1vdmVDbGFzcyh0aHVtYixjbGFzc05hbWUpO31lbHNlIGlmKCFpc0FjdGl2ZSYmaW5kZXg9PT1pKXt1dGlscy5hZGRDbGFzcyh0aHVtYixjbGFzc05hbWUpO319fTtwcm90by5zZXRUaHVtYlNpemU9ZnVuY3Rpb24odGh1bWIsdGh1bWJzKXtpZih0aHVtYi53aWR0aCE9PXRodW1icy53aWR0aHx8dGh1bWIuaGVpZ2h0IT09dGh1bWJzLmhlaWdodHx8dGh1bWIuZ3V0dGVyIT09dGh1bWJzLmd1dHRlcil7dGh1bWIud2lkdGg9dGh1bWJzLndpZHRoO3RodW1iLmhlaWdodD10aHVtYnMuaGVpZ2h0O3RodW1iLmd1dHRlcj10aHVtYnMuZ3V0dGVyO3RodW1iLnN0eWxlLndpZHRoPXRodW1icy53aWR0aCsncHgnO3RodW1iLnN0eWxlLmhlaWdodD10aHVtYnMuaGVpZ2h0KydweCc7fX07cHJvdG8ud2lsbENsb3NlPWZ1bmN0aW9uKGNlbGwpe3ZhciBvcGFjaXR5PXRoaXMuRE9NLm92ZXJsYXkuc3R5bGUub3BhY2l0eSxjYW5DbG9zZT10aGlzLmdlc3R1cmUuY2FuQ2xvc2UsZ2VzdHVyZVR5cGU9dGhpcy5nZXN0dXJlLnR5cGUsZ2VzdHVyZUNsb3NlPXRoaXMuZ2VzdHVyZS5jbG9zZUJ5LHBpbmNoVG9DbG9zZT1nZXN0dXJlVHlwZT09PSdwYW5ab29tJ3x8Z2VzdHVyZUNsb3NlPT09J3Bhblpvb20nLGRyYWdZVG9DbG9zZT1nZXN0dXJlVHlwZT09PSdwYW5ZJ3x8Z2VzdHVyZUNsb3NlPT09J3BhblknO2lmKGNlbGwucG9zaXRpb24ucz4xLjEmJnR5cGVvZiBjYW5DbG9zZT09PSd1bmRlZmluZWQnKXt0aGlzLmdlc3R1cmUuY2FuQ2xvc2U9ZmFsc2U7fWVsc2UgaWYoY2VsbC5wb3NpdGlvbi5zPDEmJnR5cGVvZiBjYW5DbG9zZT09PSd1bmRlZmluZWQnKXt0aGlzLmdlc3R1cmUuY2FuQ2xvc2U9dHJ1ZTt9XG5pZih0aGlzLm9wdGlvbnMucGluY2hUb0Nsb3NlJiZwaW5jaFRvQ2xvc2UmJnRoaXMuZ2VzdHVyZS5jYW5DbG9zZSl7b3BhY2l0eT1jZWxsLnBvc2l0aW9uLnM7dGhpcy5nZXN0dXJlLmNsb3NlQnk9J3Bhblpvb20nO31lbHNlIGlmKGRyYWdZVG9DbG9zZSl7b3BhY2l0eT0xLU1hdGguYWJzKGNlbGwucG9zaXRpb24ueSkvKHRoaXMuc2xpZGVyLmhlaWdodCowLjUpO3RoaXMuZ2VzdHVyZS5jbG9zZUJ5PSdwYW5ZJzt9ZWxzZSBpZihvcGFjaXR5JiZvcGFjaXR5PDEpe29wYWNpdHk9MTt0aGlzLmdlc3R1cmUuY2xvc2VCeT1mYWxzZTt9XG5vcGFjaXR5PSFvcGFjaXR5PzE6TWF0aC5tYXgoMCxNYXRoLm1pbigxLG9wYWNpdHkpKTt2YXIgbWV0aG9kPW9wYWNpdHk8PTAuOHx8IW9wYWNpdHk/J2FkZCc6J3JlbW92ZSc7dXRpbHNbbWV0aG9kKydDbGFzcyddKHRoaXMuRE9NLmhvbGRlcix0aGlzLnByZSsnLXdpbGwtY2xvc2UnKTt0aGlzLkRPTS5vdmVybGF5LnN0eWxlLm9wYWNpdHk9b3BhY2l0eTt9O3Byb3RvLnByZXY9dXRpbHMudGhyb3R0bGUoZnVuY3Rpb24oKXtpZighdGhpcy5nZXN0dXJlLm1vdmUpe3RoaXMuc2xpZGVUbyh0aGlzLnNsaWRlcy5pbmRleC0xKnRoaXMuaXNSVEwoKSk7fX0sMTIwKTtwcm90by5uZXh0PXV0aWxzLnRocm90dGxlKGZ1bmN0aW9uKCl7aWYoIXRoaXMuZ2VzdHVyZS5tb3ZlKXt0aGlzLnNsaWRlVG8odGhpcy5zbGlkZXMuaW5kZXgrMSp0aGlzLmlzUlRMKCkpO319LDEyMCk7cHJvdG8uc2xpZGVUbz1mdW5jdGlvbih0byxzbGlkZVNob3cpe3ZhciBzbGlkZXM9dGhpcy5zbGlkZXMsZ2FsbGVyeT10aGlzLmdhbGxlcnksaG9sZGVyPXRoaXMuRE9NLnNsaWRlcixSVEw9dGhpcy5pc1JUTCgpLGxlbmd0aD1nYWxsZXJ5LmluaXRpYWxMZW5ndGgsbW9kdWxvVG89dXRpbHMubW9kdWxvKGxlbmd0aCx0byksbW9kdWxvRnJvbT11dGlscy5tb2R1bG8obGVuZ3RoLGdhbGxlcnkuaW5kZXgpLHNsaWRlQnk9bW9kdWxvVG8tbW9kdWxvRnJvbSxmcm9tRW5kcz1sZW5ndGgtTWF0aC5hYnMoc2xpZGVCeSk7aWYoIXRoaXMuc3RhdGVzLmxvb3AmJih0bzwwfHx0bz50aGlzLmdhbGxlcnkuaW5pdGlhbExlbmd0aC0xKSl7cmV0dXJuO31cbmlmKHRoaXMuc3RhdGVzLmxvb3AmJmZyb21FbmRzPDMmJmZyb21FbmRzKjI8bGVuZ3RoKXtzbGlkZUJ5PXNsaWRlQnk8MD9mcm9tRW5kczotZnJvbUVuZHM7fVxuaWYobW9kdWxvVG89PT10byl7dG89c2xpZGVzLmluZGV4K3NsaWRlQnk7fVxuc2xpZGVCeT10by1zbGlkZXMuaW5kZXg7aWYoIXNsaWRlQnkpe3JldHVybjt9XG5pZih0aGlzLnN0YXRlcy56b29tKXt0aGlzLnpvb20oKTt9XG50aGlzLnBhdXNlVmlkZW8oKTt0aGlzLnNoYXJlKCk7aWYoIXNsaWRlU2hvdyl7dGhpcy5zdG9wU2xpZGVTaG93KCk7fVxuc2xpZGVzLmluZGV4PXRvO3ZhciBzbGlkZXI9dGhpcy5zbGlkZXI7aWYoTWF0aC5hYnMoc2xpZGVCeSk+Mil7dXRpbHMuYWRkQ2xhc3MoaG9sZGVyLHRoaXMucHJlKyctaGlkZScpO3RoaXMuc2V0U2xpZGVyUG9zaXRpb24oc2xpZGVyLHNsaWRlcyk7dGhpcy5zZXRTbGlkZXNQb3NpdGlvbnMoc2xpZGVzKTt2YXIgbW92ZUJ5PVJUTCpzbGlkZXMud2lkdGgqTWF0aC5taW4oMixNYXRoLmFicyhzbGlkZUJ5KSkqTWF0aC5hYnMoc2xpZGVCeSkvc2xpZGVCeTtzbGlkZXIucG9zaXRpb24ueD1zbGlkZXIuYXR0cmFjdGlvbi54PXNsaWRlci5wb3NpdGlvbi54K21vdmVCeTt1dGlscy50cmFuc2xhdGUoaG9sZGVyLHNsaWRlci5wb3NpdGlvbi54LDApO2hvbGRlci5nZXRDbGllbnRSZWN0cygpO31cbnRoaXMudXBkYXRlTWVkaWFJbmZvKCk7dXRpbHMucmVtb3ZlQ2xhc3MoaG9sZGVyLHRoaXMucHJlKyctaGlkZScpO3NsaWRlci5zdGFydEFuaW1hdGUoKTtzbGlkZXIucmVsZWFzZURyYWcoKTtzbGlkZXIuYW5pbWF0ZVRvKHt4Oi1SVEwqdG8qc2xpZGVzLndpZHRoLHk6MCxzOnVuZGVmaW5lZH0pO307cHJvdG8ua2V5RG93bj1mdW5jdGlvbihldmVudCl7dmFyIGtleT1ldmVudC5rZXlDb2RlLG9wdD10aGlzLm9wdGlvbnM7aWYob3B0LnByZXZOZXh0S2V5KXtpZihrZXk9PT0zNyl7dGhpcy5wcmV2KGV2ZW50KTt9ZWxzZSBpZihrZXk9PT0zOSl7dGhpcy5uZXh0KGV2ZW50KTt9fVxuaWYoa2V5PT09MjcmJm9wdC5lc2NhcGVUb0Nsb3NlKXt0aGlzLmNsb3NlKCk7fVxuaWYoIW9wdC5tb3VzZVdoZWVsJiZbMzIsMzMsMzQsMzUsMzYsMzgsNDBdLmluZGV4T2Yoa2V5KT4tMSl7ZXZlbnQucHJldmVudERlZmF1bHQoKTtyZXR1cm4gZmFsc2U7fX07cHJvdG8uem9vbT1mdW5jdGlvbigpe3RoaXMuem9vbVRvKCk7fTtwcm90by56b29tVG89ZnVuY3Rpb24oeCx5LHNjYWxlKXtpZighdGhpcy5pc1NsaWRlclNldHRsZSgpfHwoIXRoaXMuaXNab29tYWJsZSgpJiZzY2FsZT4xKSl7cmV0dXJuO31cbnRoaXMuZ2VzdHVyZS5jbG9zZUJ5PWZhbHNlO3ZhciBtZWRpYT10aGlzLmdldE1lZGlhKCk7c2NhbGU9c2NhbGU/c2NhbGU6dGhpcy5zdGF0ZXMuem9vbT8xOm1lZGlhLmRvbS5zaXplLnNjYWxlO3ZhciBjZWxsPXRoaXMuZ2V0Q2VsbCgpLGJvdW5kPXRoaXMubWVkaWFWaWV3cG9ydChzY2FsZSksY2VudFg9eD94LXRoaXMuc2xpZGVyLndpZHRoKjAuNTowLGNlbnRZPXk/eS10aGlzLnNsaWRlci5oZWlnaHQqMC41OjAsUG9zWD1zY2FsZT4xP01hdGguY2VpbChjZW50WCsoY2VsbC5wb3NpdGlvbi54LWNlbnRYKSooc2NhbGUvY2VsbC5wb3NpdGlvbi5zKSk6MCxQb3NZPXNjYWxlPjE/TWF0aC5jZWlsKGNlbnRZKyhjZWxsLnBvc2l0aW9uLnktY2VudFkpKihzY2FsZS9jZWxsLnBvc2l0aW9uLnMpKTowO2NlbGwuc3RhcnRBbmltYXRlKCk7Y2VsbC5yZWxlYXNlRHJhZygpO2NlbGwuYW5pbWF0ZVRvKHt4OlBvc1g+Ym91bmQubGVmdD9ib3VuZC5sZWZ0OlBvc1g8Ym91bmQucmlnaHQ/Ym91bmQucmlnaHQ6UG9zWCx5OlBvc1k+Ym91bmQuYm90dG9tP2JvdW5kLmJvdHRvbTpQb3NZPGJvdW5kLnRvcD9ib3VuZC50b3A6UG9zWSxzOnNjYWxlfSk7dGhpcy51cGRhdGVab29tKHNjYWxlKTt9O3Byb3RvLnVwZGF0ZVpvb209ZnVuY3Rpb24oc2NhbGUpe3RoaXMuc3RhdGVzLnpvb209c2NhbGU+MT90cnVlOmZhbHNlO3V0aWxzW3RoaXMuc3RhdGVzLnpvb20/J2FkZENsYXNzJzoncmVtb3ZlQ2xhc3MnXSh0aGlzLkRPTS5ob2xkZXIsdGhpcy5wcmUrJy1wYW56b29tJyk7fTtwcm90by5kZXN0cm95PWZ1bmN0aW9uKCl7aWYoIXRoaXMuR1VJRCl7cmV0dXJuO31cbmlmKHRoaXMuc3RhdGVzLm9wZW4pe3RoaXMuY2xvc2UoKTt9XG52YXIgc2VsZWN0b3JzPXRoaXMub3B0aW9ucy5tZWRpYVNlbGVjdG9yLHNvdXJjZXM9Jyc7dHJ5e3NvdXJjZXM9ZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChzZWxlY3RvcnMpO31jYXRjaChlcnJvcil7fVxuZm9yKHZhciBpPTAsbD1zb3VyY2VzLmxlbmd0aDtpPGw7aSsrKXt2YXIgc291cmNlPXNvdXJjZXNbaV07aWYoc291cmNlLm1vYnhMaXN0ZW5lcil7c291cmNlLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2NsaWNrJyxzb3VyY2UubW9ieExpc3RlbmVyLGZhbHNlKTt9fVxudGhpcy5iaW5kRXZlbnRzKGZhbHNlKTt0aGlzLnNsaWRlci5yZXNldEFuaW1hdGUoKTtmb3IoaT0wO2k8dGhpcy5zbGlkZXMubGVuZ3RoO2krKyl7dGhpcy5jZWxsc1tpXS5yZXNldEFuaW1hdGUoKTt9XG5pZih0aGlzLnRodW1icyl7dGhpcy50aHVtYnMucmVzZXRBbmltYXRlKCk7fVxudGhpcy5ET00uaG9sZGVyLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5ET00uaG9sZGVyKTt0aGlzLkRPTS5jb21tZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5ET00uY29tbWVudCk7ZGVsZXRlIGluc3RhbmNlc1t0aGlzLkdVSURdO2RlbGV0ZSB0aGlzLkRPTS5ob2xkZXIuR1VJRDtkZWxldGUgdGhpcy5HVUlEO307aWYodHlwZW9mIGpRdWVyeSE9PSd1bmRlZmluZWQnKXsoZnVuY3Rpb24oJCl7JC5Nb2R1bG9Cb3g9ZnVuY3Rpb24ob3B0aW9ucyl7cmV0dXJuIG5ldyBNb2R1bG9Cb3gob3B0aW9ucyk7fTt9KShqUXVlcnkpO31cbnJldHVybiBNb2R1bG9Cb3g7fSkpOztqUXVlcnkoZnVuY3Rpb24oKXtQYXJhbGxheFNjcm9sbC5pbml0KCk7fSk7dmFyIFBhcmFsbGF4U2Nyb2xsPXtzaG93TG9nczpmYWxzZSxyb3VuZDoxMDAwLGluaXQ6ZnVuY3Rpb24oKXt0aGlzLl9sb2coXCJpbml0XCIpO2lmKHRoaXMuX2luaXRlZCl7dGhpcy5fbG9nKFwiQWxyZWFkeSBJbml0ZWRcIik7dGhpcy5faW5pdGVkPXRydWU7cmV0dXJuO31cbnRoaXMuX3JlcXVlc3RBbmltYXRpb25GcmFtZT0oZnVuY3Rpb24oKXtyZXR1cm4gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZXx8d2luZG93LndlYmtpdFJlcXVlc3RBbmltYXRpb25GcmFtZXx8d2luZG93Lm1velJlcXVlc3RBbmltYXRpb25GcmFtZXx8d2luZG93Lm9SZXF1ZXN0QW5pbWF0aW9uRnJhbWV8fHdpbmRvdy5tc1JlcXVlc3RBbmltYXRpb25GcmFtZXx8ZnVuY3Rpb24oY2FsbGJhY2ssZWxlbWVudCl7d2luZG93LnNldFRpbWVvdXQoY2FsbGJhY2ssMTAwMC82MCk7fTt9KSgpO3RoaXMuX29uU2Nyb2xsKHRydWUpO30sX2luaXRlZDpmYWxzZSxfcHJvcGVydGllczpbJ3gnLCd5JywneicsJ3JvdGF0ZVgnLCdyb3RhdGVZJywncm90YXRlWicsJ3NjYWxlWCcsJ3NjYWxlWScsJ3NjYWxlWicsJ3NjYWxlJ10sX3JlcXVlc3RBbmltYXRpb25GcmFtZTpudWxsLF9sb2c6ZnVuY3Rpb24obWVzc2FnZSl7aWYodGhpcy5zaG93TG9ncyljb25zb2xlLmxvZyhcIlBhcmFsbGF4IFNjcm9sbCAvIFwiK21lc3NhZ2UpO30sX29uU2Nyb2xsOmZ1bmN0aW9uKG5vU21vb3RoKXt2YXIgc2Nyb2xsPWpRdWVyeShkb2N1bWVudCkuc2Nyb2xsVG9wKCk7dmFyIHdpbmRvd0hlaWdodD1qUXVlcnkod2luZG93KS5oZWlnaHQoKTt0aGlzLl9sb2coXCJvblNjcm9sbCBcIitzY3JvbGwpO2pRdWVyeShcIltkYXRhLXBhcmFsbGF4XVwiKS5lYWNoKGpRdWVyeS5wcm94eShmdW5jdGlvbihpbmRleCxlbCl7dmFyICRlbD1qUXVlcnkoZWwpO3ZhciBwcm9wZXJ0aWVzPVtdO3ZhciBhcHBseVByb3BlcnRpZXM9ZmFsc2U7dmFyIHN0eWxlPSRlbC5kYXRhKFwic3R5bGVcIik7aWYoc3R5bGU9PXVuZGVmaW5lZCl7c3R5bGU9JGVsLmF0dHIoXCJzdHlsZVwiKXx8XCJcIjskZWwuZGF0YShcInN0eWxlXCIsc3R5bGUpO31cbnZhciBkYXRhcz1bJGVsLmRhdGEoXCJwYXJhbGxheFwiKV07dmFyIGlEYXRhO2ZvcihpRGF0YT0yOztpRGF0YSsrKXtpZigkZWwuZGF0YShcInBhcmFsbGF4XCIraURhdGEpKXtkYXRhcy5wdXNoKCRlbC5kYXRhKFwicGFyYWxsYXgtXCIraURhdGEpKTt9XG5lbHNle2JyZWFrO319XG52YXIgZGF0YXNMZW5ndGg9ZGF0YXMubGVuZ3RoO2ZvcihpRGF0YT0wO2lEYXRhPGRhdGFzTGVuZ3RoO2lEYXRhKyspe3ZhciBkYXRhPWRhdGFzW2lEYXRhXTt2YXIgc2Nyb2xsRnJvbT1kYXRhW1wiZnJvbS1zY3JvbGxcIl07aWYoc2Nyb2xsRnJvbT09dW5kZWZpbmVkKXNjcm9sbEZyb209TWF0aC5tYXgoMCxqUXVlcnkoZWwpLm9mZnNldCgpLnRvcC13aW5kb3dIZWlnaHQpO3Njcm9sbEZyb209c2Nyb2xsRnJvbXwwO3ZhciBzY3JvbGxEaXN0YW5jZT1kYXRhW1wiZGlzdGFuY2VcIl07dmFyIHNjcm9sbFRvPWRhdGFbXCJ0by1zY3JvbGxcIl07aWYoc2Nyb2xsRGlzdGFuY2U9PXVuZGVmaW5lZCYmc2Nyb2xsVG89PXVuZGVmaW5lZClzY3JvbGxEaXN0YW5jZT13aW5kb3dIZWlnaHQ7c2Nyb2xsRGlzdGFuY2U9TWF0aC5tYXgoc2Nyb2xsRGlzdGFuY2V8MCwxKTt2YXIgZWFzaW5nPWRhdGFbXCJlYXNpbmdcIl07dmFyIGVhc2luZ1JldHVybj1kYXRhW1wiZWFzaW5nLXJldHVyblwiXTtpZihlYXNpbmc9PXVuZGVmaW5lZHx8IWpRdWVyeS5lYXNpbmd8fCFqUXVlcnkuZWFzaW5nW2Vhc2luZ10pZWFzaW5nPW51bGw7aWYoZWFzaW5nUmV0dXJuPT11bmRlZmluZWR8fCFqUXVlcnkuZWFzaW5nfHwhalF1ZXJ5LmVhc2luZ1tlYXNpbmdSZXR1cm5dKWVhc2luZ1JldHVybj1lYXNpbmc7aWYoZWFzaW5nKXt2YXIgdG90YWxUaW1lPWRhdGFbXCJkdXJhdGlvblwiXTtpZih0b3RhbFRpbWU9PXVuZGVmaW5lZCl0b3RhbFRpbWU9c2Nyb2xsRGlzdGFuY2U7dG90YWxUaW1lPU1hdGgubWF4KHRvdGFsVGltZXwwLDEpO3ZhciB0b3RhbFRpbWVSZXR1cm49ZGF0YVtcImR1cmF0aW9uLXJldHVyblwiXTtpZih0b3RhbFRpbWVSZXR1cm49PXVuZGVmaW5lZCl0b3RhbFRpbWVSZXR1cm49dG90YWxUaW1lO3Njcm9sbERpc3RhbmNlPTE7dmFyIGN1cnJlbnRUaW1lPSRlbC5kYXRhKFwiY3VycmVudC10aW1lXCIpO2lmKGN1cnJlbnRUaW1lPT11bmRlZmluZWQpY3VycmVudFRpbWU9MDt9XG5pZihzY3JvbGxUbz09dW5kZWZpbmVkKXNjcm9sbFRvPXNjcm9sbEZyb20rc2Nyb2xsRGlzdGFuY2U7c2Nyb2xsVG89c2Nyb2xsVG98MDt2YXIgc21vb3RobmVzcz1kYXRhW1wic21vb3RobmVzc1wiXTtpZihzbW9vdGhuZXNzPT11bmRlZmluZWQpc21vb3RobmVzcz0zMDtzbW9vdGhuZXNzPXNtb290aG5lc3N8MDtpZihub1Ntb290aHx8c21vb3RobmVzcz09MClzbW9vdGhuZXNzPTE7c21vb3RobmVzcz1zbW9vdGhuZXNzfDA7dmFyIHNjcm9sbEN1cnJlbnQ9c2Nyb2xsO3Njcm9sbEN1cnJlbnQ9TWF0aC5tYXgoc2Nyb2xsQ3VycmVudCxzY3JvbGxGcm9tKTtzY3JvbGxDdXJyZW50PU1hdGgubWluKHNjcm9sbEN1cnJlbnQsc2Nyb2xsVG8pO2lmKGVhc2luZyl7aWYoJGVsLmRhdGEoXCJzZW5zXCIpPT11bmRlZmluZWQpJGVsLmRhdGEoXCJzZW5zXCIsXCJiYWNrXCIpO2lmKHNjcm9sbEN1cnJlbnQ+c2Nyb2xsRnJvbSl7aWYoJGVsLmRhdGEoXCJzZW5zXCIpPT1cImJhY2tcIil7Y3VycmVudFRpbWU9MTskZWwuZGF0YShcInNlbnNcIixcImdvXCIpO31cbmVsc2V7Y3VycmVudFRpbWUrKzt9fVxuaWYoc2Nyb2xsQ3VycmVudDxzY3JvbGxUbyl7aWYoJGVsLmRhdGEoXCJzZW5zXCIpPT1cImdvXCIpe2N1cnJlbnRUaW1lPTE7JGVsLmRhdGEoXCJzZW5zXCIsXCJiYWNrXCIpO31cbmVsc2V7Y3VycmVudFRpbWUrKzt9fVxuaWYobm9TbW9vdGgpY3VycmVudFRpbWU9dG90YWxUaW1lOyRlbC5kYXRhKFwiY3VycmVudC10aW1lXCIsY3VycmVudFRpbWUpO31cbnRoaXMuX3Byb3BlcnRpZXMubWFwKGpRdWVyeS5wcm94eShmdW5jdGlvbihwcm9wKXt2YXIgZGVmYXVsdFByb3A9MDt2YXIgdG89ZGF0YVtwcm9wXTtpZih0bz09dW5kZWZpbmVkKXJldHVybjtpZihwcm9wPT1cInNjYWxlXCJ8fHByb3A9PVwic2NhbGVYXCJ8fHByb3A9PVwic2NhbGVZXCJ8fHByb3A9PVwic2NhbGVaXCIpe2RlZmF1bHRQcm9wPTE7fVxuZWxzZXt0bz10b3wwO31cbnZhciBwcmV2PSRlbC5kYXRhKFwiX1wiK3Byb3ApO2lmKHByZXY9PXVuZGVmaW5lZClwcmV2PWRlZmF1bHRQcm9wO3ZhciBuZXh0PSgodG8tZGVmYXVsdFByb3ApKigoc2Nyb2xsQ3VycmVudC1zY3JvbGxGcm9tKS8oc2Nyb2xsVG8tc2Nyb2xsRnJvbSkpKStkZWZhdWx0UHJvcDt2YXIgdmFsPXByZXYrKG5leHQtcHJldikvc21vb3RobmVzcztpZihlYXNpbmcmJmN1cnJlbnRUaW1lPjAmJmN1cnJlbnRUaW1lPD10b3RhbFRpbWUpe3ZhciBmcm9tPWRlZmF1bHRQcm9wO2lmKCRlbC5kYXRhKFwic2Vuc1wiKT09XCJiYWNrXCIpe2Zyb209dG87dG89LXRvO2Vhc2luZz1lYXNpbmdSZXR1cm47dG90YWxUaW1lPXRvdGFsVGltZVJldHVybjt9XG52YWw9alF1ZXJ5LmVhc2luZ1tlYXNpbmddKG51bGwsY3VycmVudFRpbWUsZnJvbSx0byx0b3RhbFRpbWUpO31cbnZhbD1NYXRoLmNlaWwodmFsKnRoaXMucm91bmQpL3RoaXMucm91bmQ7aWYodmFsPT1wcmV2JiZuZXh0PT10byl2YWw9dG87aWYoIXByb3BlcnRpZXNbcHJvcF0pcHJvcGVydGllc1twcm9wXT0wO3Byb3BlcnRpZXNbcHJvcF0rPXZhbDtpZihwcmV2IT1wcm9wZXJ0aWVzW3Byb3BdKXskZWwuZGF0YShcIl9cIitwcm9wLHByb3BlcnRpZXNbcHJvcF0pO2FwcGx5UHJvcGVydGllcz10cnVlO319LHRoaXMpKTt9XG5pZihhcHBseVByb3BlcnRpZXMpe2lmKHByb3BlcnRpZXNbXCJ6XCJdIT11bmRlZmluZWQpe3ZhciBwZXJzcGVjdGl2ZT1kYXRhW1wicGVyc3BlY3RpdmVcIl07aWYocGVyc3BlY3RpdmU9PXVuZGVmaW5lZClwZXJzcGVjdGl2ZT04MDA7dmFyICRwYXJlbnQ9JGVsLnBhcmVudCgpO2lmKCEkcGFyZW50LmRhdGEoXCJzdHlsZVwiKSkkcGFyZW50LmRhdGEoXCJzdHlsZVwiLCRwYXJlbnQuYXR0cihcInN0eWxlXCIpfHxcIlwiKTskcGFyZW50LmF0dHIoXCJzdHlsZVwiLFwicGVyc3BlY3RpdmU6XCIrcGVyc3BlY3RpdmUrXCJweDsgLXdlYmtpdC1wZXJzcGVjdGl2ZTpcIitwZXJzcGVjdGl2ZStcInB4OyBcIiskcGFyZW50LmRhdGEoXCJzdHlsZVwiKSk7fVxuaWYocHJvcGVydGllc1tcInNjYWxlWFwiXT09dW5kZWZpbmVkKXByb3BlcnRpZXNbXCJzY2FsZVhcIl09MTtpZihwcm9wZXJ0aWVzW1wic2NhbGVZXCJdPT11bmRlZmluZWQpcHJvcGVydGllc1tcInNjYWxlWVwiXT0xO2lmKHByb3BlcnRpZXNbXCJzY2FsZVpcIl09PXVuZGVmaW5lZClwcm9wZXJ0aWVzW1wic2NhbGVaXCJdPTE7aWYocHJvcGVydGllc1tcInNjYWxlXCJdIT11bmRlZmluZWQpe3Byb3BlcnRpZXNbXCJzY2FsZVhcIl0qPXByb3BlcnRpZXNbXCJzY2FsZVwiXTtwcm9wZXJ0aWVzW1wic2NhbGVZXCJdKj1wcm9wZXJ0aWVzW1wic2NhbGVcIl07cHJvcGVydGllc1tcInNjYWxlWlwiXSo9cHJvcGVydGllc1tcInNjYWxlXCJdO31cbnZhciB0cmFuc2xhdGUzZD1cInRyYW5zbGF0ZTNkKFwiKyhwcm9wZXJ0aWVzW1wieFwiXT9wcm9wZXJ0aWVzW1wieFwiXTowKStcInB4LCBcIisocHJvcGVydGllc1tcInlcIl0/cHJvcGVydGllc1tcInlcIl06MCkrXCJweCwgXCIrKHByb3BlcnRpZXNbXCJ6XCJdP3Byb3BlcnRpZXNbXCJ6XCJdOjApK1wicHgpXCI7dmFyIHJvdGF0ZTNkPVwicm90YXRlWChcIisocHJvcGVydGllc1tcInJvdGF0ZVhcIl0/cHJvcGVydGllc1tcInJvdGF0ZVhcIl06MCkrXCJkZWcpIHJvdGF0ZVkoXCIrKHByb3BlcnRpZXNbXCJyb3RhdGVZXCJdP3Byb3BlcnRpZXNbXCJyb3RhdGVZXCJdOjApK1wiZGVnKSByb3RhdGVaKFwiKyhwcm9wZXJ0aWVzW1wicm90YXRlWlwiXT9wcm9wZXJ0aWVzW1wicm90YXRlWlwiXTowKStcImRlZylcIjt2YXIgc2NhbGUzZD1cInNjYWxlWChcIitwcm9wZXJ0aWVzW1wic2NhbGVYXCJdK1wiKSBzY2FsZVkoXCIrcHJvcGVydGllc1tcInNjYWxlWVwiXStcIikgc2NhbGVaKFwiK3Byb3BlcnRpZXNbXCJzY2FsZVpcIl0rXCIpXCI7dmFyIGNzc1RyYW5zZm9ybT10cmFuc2xhdGUzZCtcIiBcIityb3RhdGUzZCtcIiBcIitzY2FsZTNkK1wiO1wiO3RoaXMuX2xvZyhjc3NUcmFuc2Zvcm0pOyRlbC5hdHRyKFwic3R5bGVcIixcInRyYW5zZm9ybTpcIitjc3NUcmFuc2Zvcm0rXCIgLXdlYmtpdC10cmFuc2Zvcm06XCIrY3NzVHJhbnNmb3JtK1wiIFwiK3N0eWxlKTt9fSx0aGlzKSk7aWYod2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSl7d2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZShqUXVlcnkucHJveHkodGhpcy5fb25TY3JvbGwsdGhpcyxmYWxzZSkpO31cbmVsc2V7dGhpcy5fcmVxdWVzdEFuaW1hdGlvbkZyYW1lKGpRdWVyeS5wcm94eSh0aGlzLl9vblNjcm9sbCx0aGlzLGZhbHNlKSk7fX19Oztcbi8qIVxuKiBqUXVlcnkgU21vb3ZlIHYwLjIuMTEgKGh0dHA6Ly9zbW9vdmUuanMub3JnLylcbiogQ29weXJpZ2h0IChjKSAyMDE3IEFkYW0gQm91cWRpYlxuKiBMaWNlbnNlZCB1bmRlciBHUEwtMi4wIChodHRwOi8vYWJlbWVkaWEuY28udWsvbGljZW5zZSkgXG4qL1xuKGZ1bmN0aW9uKCQsd2luZG93LGRvY3VtZW50KXtmdW5jdGlvbiBjcm9zc0Jyb3dzZXIocHJvcGVydHksdmFsdWUscHJlZml4KXtmdW5jdGlvbiB1Y2FzZShzdHJpbmcpe3JldHVybiBzdHJpbmcuY2hhckF0KDApLnRvVXBwZXJDYXNlKCkrc3RyaW5nLnNsaWNlKDEpO31cbnZhciB2ZW5kb3I9Wyd3ZWJraXQnLCdtb3onLCdtcycsJ28nXSxwcm9wZXJ0aWVzPXt9O2Zvcih2YXIgaT0wO2k8dmVuZG9yLmxlbmd0aDtpKyspe2lmKHByZWZpeCl7dmFsdWU9dmFsdWUucmVwbGFjZShwcmVmaXgsJy0nK3ZlbmRvcltpXSsnLScrcHJlZml4KTt9XG5wcm9wZXJ0aWVzW3VjYXNlKHZlbmRvcltpXSkrdWNhc2UocHJvcGVydHkpXT12YWx1ZTt9XG5wcm9wZXJ0aWVzW3Byb3BlcnR5XT12YWx1ZTtyZXR1cm4gcHJvcGVydGllczt9XG5mdW5jdGlvbiBzbW9vdmVJdChkaXJlY3Rpb24pe3ZhciBoZWlnaHQ9JCh3aW5kb3cpLmhlaWdodCgpLHdpZHRoPSQod2luZG93KS53aWR0aCgpO2Zvcih2YXIgaT0wO2k8JC5mbi5zbW9vdmUuaXRlbXMubGVuZ3RoO2krKyl7dmFyICRpdGVtPSQuZm4uc21vb3ZlLml0ZW1zW2ldLHBhcmFtcz0kaXRlbS5wYXJhbXM7aWYoISRpdGVtLmhhc0NsYXNzKCdzbW9vdmVkJykpXG57dmFyIG9mZnNldD0oIWRpcmVjdGlvbnx8ZGlyZWN0aW9uPT09J2Rvd24nJiYkaXRlbS5jc3MoJ29wYWNpdHknKT09PScxJyk/MDpwYXJhbXMub2Zmc2V0LGl0ZW10b3A9JCh3aW5kb3cpLnNjcm9sbFRvcCgpK2hlaWdodC0kaXRlbS5vZmZzZXQoKS50b3A7aWYodHlwZW9mIG9mZnNldD09PSdzdHJpbmcnJiZvZmZzZXQuaW5kZXhPZignJScpKXtvZmZzZXQ9cGFyc2VJbnQob2Zmc2V0KS8xMDAqaGVpZ2h0O31cbmlmKGl0ZW10b3A8b2Zmc2V0fHxkaXJlY3Rpb249PSdmaXJzdCcpe2lmKCFpc05hTihwYXJhbXMub3BhY2l0eSkpeyRpdGVtLmNzcyh7b3BhY2l0eTpwYXJhbXMub3BhY2l0eX0pO31cbnZhciB0cmFuc2Zvcm1zPVtdLHByb3BlcnRpZXM9Wydtb3ZlJywnbW92ZTNEJywnbW92ZVgnLCdtb3ZlWScsJ21vdmVaJywncm90YXRlJywncm90YXRlM2QnLCdyb3RhdGVYJywncm90YXRlWScsJ3JvdGF0ZVonLCdzY2FsZScsJ3NjYWxlM2QnLCdzY2FsZVgnLCdzY2FsZVknLCdza2V3Jywnc2tld1gnLCdza2V3WSddO2Zvcih2YXIgcD0wO3A8cHJvcGVydGllcy5sZW5ndGg7cCsrKXtpZih0eXBlb2YgcGFyYW1zW3Byb3BlcnRpZXNbcF1dIT09XCJ1bmRlZmluZWRcIil7dHJhbnNmb3Jtc1twcm9wZXJ0aWVzW3BdXT1wYXJhbXNbcHJvcGVydGllc1twXV07fX1cbnZhciB0cmFuc2Zvcm09Jyc7Zm9yKHZhciB0IGluIHRyYW5zZm9ybXMpe3RyYW5zZm9ybSs9dC5yZXBsYWNlKCdtb3ZlJywndHJhbnNsYXRlJykrJygnK3RyYW5zZm9ybXNbdF0rJykgJzt9XG5pZih0cmFuc2Zvcm0peyRpdGVtLmNzcyhjcm9zc0Jyb3dzZXIoJ3RyYW5zZm9ybScsdHJhbnNmb3JtKSk7JGl0ZW0ucGFyZW50KCkuY3NzKGNyb3NzQnJvd3NlcigncGVyc3BlY3RpdmUnLHBhcmFtcy5wZXJzcGVjdGl2ZSkpO2lmKHBhcmFtcy50cmFuc2Zvcm1PcmlnaW4peyRpdGVtLmNzcyhjcm9zc0Jyb3dzZXIoJ3RyYW5zZm9ybU9yaWdpbicscGFyYW1zLnRyYW5zZm9ybU9yaWdpbikpO319XG5pZih0eXBlb2YgcGFyYW1zLmRlbGF5IT09XCJ1bmRlZmluZWRcIiYmcGFyYW1zLmRlbGF5PjApXG57JGl0ZW0uY3NzKCd0cmFuc2l0aW9uLWRlbGF5JyxwYXJzZUludChwYXJhbXMuZGVsYXkpKydtcycpO31cbiRpdGVtLmFkZENsYXNzKCdmaXJzdF9zbW9vdmVkJyk7fWVsc2V7aWYoISRpdGVtLmhhc0NsYXNzKCdmaXJzdF9zbW9vdmVkJyk9PT10cnVlKVxueyRpdGVtLnN0b3AoKS5jc3MoJ29wYWNpdHknLDEpLmNzcyhjcm9zc0Jyb3dzZXIoJ3RyYW5zZm9ybScsJ3RyYW5zbGF0ZSgwcHgsIDBweCknKSkuY3NzKCd0cmFuc2Zvcm0nLCcnKTskaXRlbS5hZGRDbGFzcygnc21vb3ZlZCcpOyRpdGVtLnBhcmVudCgpLmFkZENsYXNzKCdzbW9vdmVkJyk7fVxuZWxzZVxue2pRdWVyeSgnYm9keScpLmFkZENsYXNzKCdoYXMtc21vb3ZlJyk7aWYoaXRlbXRvcDxvZmZzZXR8fGpRdWVyeSh3aW5kb3cpLmhlaWdodCgpPj1qUXVlcnkoZG9jdW1lbnQpLmhlaWdodCgpKVxueyRpdGVtLnN0b3AoKS5xdWV1ZShmdW5jdGlvbihuZXh0KXtqUXVlcnkodGhpcykuc3RvcCgpLmNzcygnb3BhY2l0eScsMSkuY3NzKGNyb3NzQnJvd3NlcigndHJhbnNmb3JtJywndHJhbnNsYXRlKDBweCwgMHB4KScpKS5jc3MoJ3RyYW5zZm9ybScsJycpOyRpdGVtLmFkZENsYXNzKCdzbW9vdmVkJyk7JGl0ZW0ucGFyZW50KCkuYWRkQ2xhc3MoJ3Ntb292ZWQnKTskaXRlbS5yZW1vdmVDbGFzcygnZmlyc3Rfc21vb3ZlZCcpO25leHQoKTt9KTt9XG5lbHNlXG57JGl0ZW0uc3RvcCgpLmRlbGF5KDEwMDApLnF1ZXVlKGZ1bmN0aW9uKG5leHQpe3dpbmRvdy5zY3JvbGxUbyh3aW5kb3cuc2Nyb2xsWCx3aW5kb3cuc2Nyb2xsWSsxKTt9KTskaXRlbS5yZW1vdmVDbGFzcygnZmlyc3Rfc21vb3ZlZCcpO319fX19fVxuZnVuY3Rpb24gdGhyb3R0bGUoZm4sdGhyZXNoaG9sZCxzY29wZSl7dGhyZXNoaG9sZD10aHJlc2hob2xkfHwyNTA7dmFyIGxhc3QsZGVmZXJUaW1lcjtyZXR1cm4gZnVuY3Rpb24oKXt2YXIgY29udGV4dD1zY29wZXx8dGhpcyxub3c9K25ldyBEYXRlKCksYXJncz1hcmd1bWVudHM7aWYobGFzdCYmbm93PGxhc3QrdGhyZXNoaG9sZCl7Y2xlYXJUaW1lb3V0KGRlZmVyVGltZXIpO2RlZmVyVGltZXI9c2V0VGltZW91dChmdW5jdGlvbigpe2xhc3Q9bm93O2ZuLmFwcGx5KGNvbnRleHQsYXJncyk7fSx0aHJlc2hob2xkKTt9ZWxzZXtsYXN0PW5vdztmbi5hcHBseShjb250ZXh0LGFyZ3MpO319O31cbiQuZm4uc21vb3ZlPWZ1bmN0aW9uKG9wdGlvbnMpeyQuZm4uc21vb3ZlLmluaXQodGhpcywkLmV4dGVuZCh7fSwkLmZuLnNtb292ZS5kZWZhdWx0cyxvcHRpb25zKSk7cmV0dXJuIHRoaXM7fTskLmZuLnNtb292ZS5pdGVtcz1bXTskLmZuLnNtb292ZS5sb2FkZWQ9ZmFsc2U7JC5mbi5zbW9vdmUuZGVmYXVsdHM9e29mZnNldDonNTAlJyxvcGFjaXR5OjAsZGVsYXk6JzBtcycsZHVyYXRpb246JzUwMG1zJyx0cmFuc2l0aW9uOlwiXCIsdHJhbnNmb3JtU3R5bGU6J3ByZXNlcnZlLTNkJyx0cmFuc2Zvcm1PcmlnaW46ZmFsc2UscGVyc3BlY3RpdmU6MTAwMCxtaW5fd2lkdGg6NzY4LG1pbl9oZWlnaHQ6ZmFsc2V9OyQuZm4uc21vb3ZlLmluaXQ9ZnVuY3Rpb24oaXRlbXMsc2V0dGluZ3Mpe2l0ZW1zLmVhY2goZnVuY3Rpb24oKXt2YXIgJGl0ZW09JCh0aGlzKSxwYXJhbXM9JGl0ZW0ucGFyYW1zPSQuZXh0ZW5kKHt9LHNldHRpbmdzLCRpdGVtLmRhdGEoKSk7JGl0ZW0uZGF0YSgndG9wJywkaXRlbS5vZmZzZXQoKS50b3ApO3BhcmFtcy50cmFuc2l0aW9uPWNyb3NzQnJvd3NlcigndHJhbnNpdGlvbicscGFyYW1zLnRyYW5zaXRpb24sJ3RyYW5zZm9ybScpOyRpdGVtLmNzcyhwYXJhbXMudHJhbnNpdGlvbik7JC5mbi5zbW9vdmUuaXRlbXMucHVzaCgkaXRlbSk7fSk7aWYoISQuZm4uc21vb3ZlLmxvYWRlZCl7JC5mbi5zbW9vdmUubG9hZGVkPXRydWU7dmFyIG9sZFNjcm9sbD0wLG9sZEhlaWdodD0kKHdpbmRvdykuaGVpZ2h0KCksb2xkV2lkdGg9JCh3aW5kb3cpLndpZHRoKCksb2xkRG9jSGVpZ2h0PSQoZG9jdW1lbnQpLmhlaWdodCgpLHJlc2l6aW5nO2lmKCQoJ2JvZHknKS53aWR0aCgpPT09JCh3aW5kb3cpLndpZHRoKCkpeyQoJ2JvZHknKS5hZGRDbGFzcygnc21vb3ZlLW92ZXJmbG93Jyk7fVxuaWYoJCgnYm9keScpLmhhc0NsYXNzKCdlbGVtZW50b3ItZWRpdG9yLWFjdGl2ZScpKVxueyQoJ2lmcmFtZSNlbGVtZW50b3ItcHJldmlldy1pZnJhbWUnKS5yZWFkeShmdW5jdGlvbigpe3Ntb292ZUl0KCdmaXJzdCcpOyQod2luZG93KS5vbignc2Nyb2xsJyx0aHJvdHRsZShmdW5jdGlvbigpe3ZhciBzY3JvbGx0b3A9JCh3aW5kb3cpLnNjcm9sbFRvcCgpLGRpcmVjdGlvbj0oc2Nyb2xsdG9wPG9sZFNjcm9sbCk/ZGlyZWN0aW9uPSd1cCc6J2Rvd24nO29sZFNjcm9sbD1zY3JvbGx0b3A7c21vb3ZlSXQoZGlyZWN0aW9uKTt9LDI1MCkpO3dpbmRvdy5zY3JvbGxUbyh3aW5kb3cuc2Nyb2xsWCx3aW5kb3cuc2Nyb2xsWSsxKTt9KTt9XG5lbHNlXG57alF1ZXJ5KGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpe3Ntb292ZUl0KCdkb3duJyk7JCh3aW5kb3cpLm9uKCdzY3JvbGwnLHRocm90dGxlKGZ1bmN0aW9uKCl7dmFyIHNjcm9sbHRvcD0kKHdpbmRvdykuc2Nyb2xsVG9wKCksZGlyZWN0aW9uPShzY3JvbGx0b3A8b2xkU2Nyb2xsKT9kaXJlY3Rpb249J3VwJzonZG93bic7b2xkU2Nyb2xsPXNjcm9sbHRvcDtzbW9vdmVJdChkaXJlY3Rpb24pO30sMjUwKSk7aWYoalF1ZXJ5KHdpbmRvdykuaGVpZ2h0KCk+PWpRdWVyeShkb2N1bWVudCkuaGVpZ2h0KCkpXG57c21vb3ZlSXQoJ2Rvd24nKTt9XG52YXIgaXNGaXJlZm94PW5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKS5pbmRleE9mKCdmaXJlZm94Jyk+LTE7aWYoaXNGaXJlZm94KXt3aW5kb3cuc2Nyb2xsVG8od2luZG93LnNjcm9sbFgsd2luZG93LnNjcm9sbFkrMSk7fVxudmFyIGlzVG91Y2g9KCdvbnRvdWNoc3RhcnQnaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KTtpZihpc1RvdWNoKXt3aW5kb3cuc2Nyb2xsVG8od2luZG93LnNjcm9sbFgsd2luZG93LnNjcm9sbFkrMSk7fX0pO319fTt9KGpRdWVyeSx3aW5kb3csZG9jdW1lbnQpKTs7KGZ1bmN0aW9uKGYpe2lmKHR5cGVvZiBleHBvcnRzPT09XCJvYmplY3RcIiYmdHlwZW9mIG1vZHVsZSE9PVwidW5kZWZpbmVkXCIpe21vZHVsZS5leHBvcnRzPWYoKX1lbHNlIGlmKHR5cGVvZiBkZWZpbmU9PT1cImZ1bmN0aW9uXCImJmRlZmluZS5hbWQpe2RlZmluZShbXSxmKX1lbHNle3ZhciBnO2lmKHR5cGVvZiB3aW5kb3chPT1cInVuZGVmaW5lZFwiKXtnPXdpbmRvd31lbHNlIGlmKHR5cGVvZiBnbG9iYWwhPT1cInVuZGVmaW5lZFwiKXtnPWdsb2JhbH1lbHNlIGlmKHR5cGVvZiBzZWxmIT09XCJ1bmRlZmluZWRcIil7Zz1zZWxmfWVsc2V7Zz10aGlzfWcuUGFyYWxsYXg9ZigpfX0pKGZ1bmN0aW9uKCl7dmFyIGRlZmluZSxtb2R1bGUsZXhwb3J0cztyZXR1cm4oZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSh7MTpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7J3VzZSBzdHJpY3QnO3ZhciBnZXRPd25Qcm9wZXJ0eVN5bWJvbHM9T2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9sczt2YXIgaGFzT3duUHJvcGVydHk9T2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eTt2YXIgcHJvcElzRW51bWVyYWJsZT1PYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlO2Z1bmN0aW9uIHRvT2JqZWN0KHZhbCl7aWYodmFsPT09bnVsbHx8dmFsPT09dW5kZWZpbmVkKXt0aHJvdyBuZXcgVHlwZUVycm9yKCdPYmplY3QuYXNzaWduIGNhbm5vdCBiZSBjYWxsZWQgd2l0aCBudWxsIG9yIHVuZGVmaW5lZCcpO31cbnJldHVybiBPYmplY3QodmFsKTt9XG5mdW5jdGlvbiBzaG91bGRVc2VOYXRpdmUoKXt0cnl7aWYoIU9iamVjdC5hc3NpZ24pe3JldHVybiBmYWxzZTt9XG52YXIgdGVzdDE9bmV3IFN0cmluZygnYWJjJyk7dGVzdDFbNV09J2RlJztpZihPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyh0ZXN0MSlbMF09PT0nNScpe3JldHVybiBmYWxzZTt9XG52YXIgdGVzdDI9e307Zm9yKHZhciBpPTA7aTwxMDtpKyspe3Rlc3QyWydfJytTdHJpbmcuZnJvbUNoYXJDb2RlKGkpXT1pO31cbnZhciBvcmRlcjI9T2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModGVzdDIpLm1hcChmdW5jdGlvbihuKXtyZXR1cm4gdGVzdDJbbl07fSk7aWYob3JkZXIyLmpvaW4oJycpIT09JzAxMjM0NTY3ODknKXtyZXR1cm4gZmFsc2U7fVxudmFyIHRlc3QzPXt9OydhYmNkZWZnaGlqa2xtbm9wcXJzdCcuc3BsaXQoJycpLmZvckVhY2goZnVuY3Rpb24obGV0dGVyKXt0ZXN0M1tsZXR0ZXJdPWxldHRlcjt9KTtpZihPYmplY3Qua2V5cyhPYmplY3QuYXNzaWduKHt9LHRlc3QzKSkuam9pbignJykhPT0nYWJjZGVmZ2hpamtsbW5vcHFyc3QnKXtyZXR1cm4gZmFsc2U7fVxucmV0dXJuIHRydWU7fWNhdGNoKGVycil7cmV0dXJuIGZhbHNlO319XG5tb2R1bGUuZXhwb3J0cz1zaG91bGRVc2VOYXRpdmUoKT9PYmplY3QuYXNzaWduOmZ1bmN0aW9uKHRhcmdldCxzb3VyY2Upe3ZhciBmcm9tO3ZhciB0bz10b09iamVjdCh0YXJnZXQpO3ZhciBzeW1ib2xzO2Zvcih2YXIgcz0xO3M8YXJndW1lbnRzLmxlbmd0aDtzKyspe2Zyb209T2JqZWN0KGFyZ3VtZW50c1tzXSk7Zm9yKHZhciBrZXkgaW4gZnJvbSl7aWYoaGFzT3duUHJvcGVydHkuY2FsbChmcm9tLGtleSkpe3RvW2tleV09ZnJvbVtrZXldO319XG5pZihnZXRPd25Qcm9wZXJ0eVN5bWJvbHMpe3N5bWJvbHM9Z2V0T3duUHJvcGVydHlTeW1ib2xzKGZyb20pO2Zvcih2YXIgaT0wO2k8c3ltYm9scy5sZW5ndGg7aSsrKXtpZihwcm9wSXNFbnVtZXJhYmxlLmNhbGwoZnJvbSxzeW1ib2xzW2ldKSl7dG9bc3ltYm9sc1tpXV09ZnJvbVtzeW1ib2xzW2ldXTt9fX19XG5yZXR1cm4gdG87fTt9LHt9XSwyOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXsoZnVuY3Rpb24ocHJvY2Vzcyl7KGZ1bmN0aW9uKCl7dmFyIGdldE5hbm9TZWNvbmRzLGhydGltZSxsb2FkVGltZSxtb2R1bGVMb2FkVGltZSxub2RlTG9hZFRpbWUsdXBUaW1lO2lmKCh0eXBlb2YgcGVyZm9ybWFuY2UhPT1cInVuZGVmaW5lZFwiJiZwZXJmb3JtYW5jZSE9PW51bGwpJiZwZXJmb3JtYW5jZS5ub3cpe21vZHVsZS5leHBvcnRzPWZ1bmN0aW9uKCl7cmV0dXJuIHBlcmZvcm1hbmNlLm5vdygpO307fWVsc2UgaWYoKHR5cGVvZiBwcm9jZXNzIT09XCJ1bmRlZmluZWRcIiYmcHJvY2VzcyE9PW51bGwpJiZwcm9jZXNzLmhydGltZSl7bW9kdWxlLmV4cG9ydHM9ZnVuY3Rpb24oKXtyZXR1cm4oZ2V0TmFub1NlY29uZHMoKS1ub2RlTG9hZFRpbWUpLzFlNjt9O2hydGltZT1wcm9jZXNzLmhydGltZTtnZXROYW5vU2Vjb25kcz1mdW5jdGlvbigpe3ZhciBocjtocj1ocnRpbWUoKTtyZXR1cm4gaHJbMF0qMWU5K2hyWzFdO307bW9kdWxlTG9hZFRpbWU9Z2V0TmFub1NlY29uZHMoKTt1cFRpbWU9cHJvY2Vzcy51cHRpbWUoKSoxZTk7bm9kZUxvYWRUaW1lPW1vZHVsZUxvYWRUaW1lLXVwVGltZTt9ZWxzZSBpZihEYXRlLm5vdyl7bW9kdWxlLmV4cG9ydHM9ZnVuY3Rpb24oKXtyZXR1cm4gRGF0ZS5ub3coKS1sb2FkVGltZTt9O2xvYWRUaW1lPURhdGUubm93KCk7fWVsc2V7bW9kdWxlLmV4cG9ydHM9ZnVuY3Rpb24oKXtyZXR1cm4gbmV3IERhdGUoKS5nZXRUaW1lKCktbG9hZFRpbWU7fTtsb2FkVGltZT1uZXcgRGF0ZSgpLmdldFRpbWUoKTt9fSkuY2FsbCh0aGlzKTt9KS5jYWxsKHRoaXMscmVxdWlyZSgnX3Byb2Nlc3MnKSl9LHtcIl9wcm9jZXNzXCI6M31dLDM6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe3ZhciBwcm9jZXNzPW1vZHVsZS5leHBvcnRzPXt9O3ZhciBjYWNoZWRTZXRUaW1lb3V0O3ZhciBjYWNoZWRDbGVhclRpbWVvdXQ7ZnVuY3Rpb24gZGVmYXVsdFNldFRpbW91dCgpe3Rocm93IG5ldyBFcnJvcignc2V0VGltZW91dCBoYXMgbm90IGJlZW4gZGVmaW5lZCcpO31cbmZ1bmN0aW9uIGRlZmF1bHRDbGVhclRpbWVvdXQoKXt0aHJvdyBuZXcgRXJyb3IoJ2NsZWFyVGltZW91dCBoYXMgbm90IGJlZW4gZGVmaW5lZCcpO31cbihmdW5jdGlvbigpe3RyeXtpZih0eXBlb2Ygc2V0VGltZW91dD09PSdmdW5jdGlvbicpe2NhY2hlZFNldFRpbWVvdXQ9c2V0VGltZW91dDt9ZWxzZXtjYWNoZWRTZXRUaW1lb3V0PWRlZmF1bHRTZXRUaW1vdXQ7fX1jYXRjaChlKXtjYWNoZWRTZXRUaW1lb3V0PWRlZmF1bHRTZXRUaW1vdXQ7fVxudHJ5e2lmKHR5cGVvZiBjbGVhclRpbWVvdXQ9PT0nZnVuY3Rpb24nKXtjYWNoZWRDbGVhclRpbWVvdXQ9Y2xlYXJUaW1lb3V0O31lbHNle2NhY2hlZENsZWFyVGltZW91dD1kZWZhdWx0Q2xlYXJUaW1lb3V0O319Y2F0Y2goZSl7Y2FjaGVkQ2xlYXJUaW1lb3V0PWRlZmF1bHRDbGVhclRpbWVvdXQ7fX0oKSlcbmZ1bmN0aW9uIHJ1blRpbWVvdXQoZnVuKXtpZihjYWNoZWRTZXRUaW1lb3V0PT09c2V0VGltZW91dCl7cmV0dXJuIHNldFRpbWVvdXQoZnVuLDApO31cbmlmKChjYWNoZWRTZXRUaW1lb3V0PT09ZGVmYXVsdFNldFRpbW91dHx8IWNhY2hlZFNldFRpbWVvdXQpJiZzZXRUaW1lb3V0KXtjYWNoZWRTZXRUaW1lb3V0PXNldFRpbWVvdXQ7cmV0dXJuIHNldFRpbWVvdXQoZnVuLDApO31cbnRyeXtyZXR1cm4gY2FjaGVkU2V0VGltZW91dChmdW4sMCk7fWNhdGNoKGUpe3RyeXtyZXR1cm4gY2FjaGVkU2V0VGltZW91dC5jYWxsKG51bGwsZnVuLDApO31jYXRjaChlKXtyZXR1cm4gY2FjaGVkU2V0VGltZW91dC5jYWxsKHRoaXMsZnVuLDApO319fVxuZnVuY3Rpb24gcnVuQ2xlYXJUaW1lb3V0KG1hcmtlcil7aWYoY2FjaGVkQ2xlYXJUaW1lb3V0PT09Y2xlYXJUaW1lb3V0KXtyZXR1cm4gY2xlYXJUaW1lb3V0KG1hcmtlcik7fVxuaWYoKGNhY2hlZENsZWFyVGltZW91dD09PWRlZmF1bHRDbGVhclRpbWVvdXR8fCFjYWNoZWRDbGVhclRpbWVvdXQpJiZjbGVhclRpbWVvdXQpe2NhY2hlZENsZWFyVGltZW91dD1jbGVhclRpbWVvdXQ7cmV0dXJuIGNsZWFyVGltZW91dChtYXJrZXIpO31cbnRyeXtyZXR1cm4gY2FjaGVkQ2xlYXJUaW1lb3V0KG1hcmtlcik7fWNhdGNoKGUpe3RyeXtyZXR1cm4gY2FjaGVkQ2xlYXJUaW1lb3V0LmNhbGwobnVsbCxtYXJrZXIpO31jYXRjaChlKXtyZXR1cm4gY2FjaGVkQ2xlYXJUaW1lb3V0LmNhbGwodGhpcyxtYXJrZXIpO319fVxudmFyIHF1ZXVlPVtdO3ZhciBkcmFpbmluZz1mYWxzZTt2YXIgY3VycmVudFF1ZXVlO3ZhciBxdWV1ZUluZGV4PS0xO2Z1bmN0aW9uIGNsZWFuVXBOZXh0VGljaygpe2lmKCFkcmFpbmluZ3x8IWN1cnJlbnRRdWV1ZSl7cmV0dXJuO31cbmRyYWluaW5nPWZhbHNlO2lmKGN1cnJlbnRRdWV1ZS5sZW5ndGgpe3F1ZXVlPWN1cnJlbnRRdWV1ZS5jb25jYXQocXVldWUpO31lbHNle3F1ZXVlSW5kZXg9LTE7fVxuaWYocXVldWUubGVuZ3RoKXtkcmFpblF1ZXVlKCk7fX1cbmZ1bmN0aW9uIGRyYWluUXVldWUoKXtpZihkcmFpbmluZyl7cmV0dXJuO31cbnZhciB0aW1lb3V0PXJ1blRpbWVvdXQoY2xlYW5VcE5leHRUaWNrKTtkcmFpbmluZz10cnVlO3ZhciBsZW49cXVldWUubGVuZ3RoO3doaWxlKGxlbil7Y3VycmVudFF1ZXVlPXF1ZXVlO3F1ZXVlPVtdO3doaWxlKCsrcXVldWVJbmRleDxsZW4pe2lmKGN1cnJlbnRRdWV1ZSl7Y3VycmVudFF1ZXVlW3F1ZXVlSW5kZXhdLnJ1bigpO319XG5xdWV1ZUluZGV4PS0xO2xlbj1xdWV1ZS5sZW5ndGg7fVxuY3VycmVudFF1ZXVlPW51bGw7ZHJhaW5pbmc9ZmFsc2U7cnVuQ2xlYXJUaW1lb3V0KHRpbWVvdXQpO31cbnByb2Nlc3MubmV4dFRpY2s9ZnVuY3Rpb24oZnVuKXt2YXIgYXJncz1uZXcgQXJyYXkoYXJndW1lbnRzLmxlbmd0aC0xKTtpZihhcmd1bWVudHMubGVuZ3RoPjEpe2Zvcih2YXIgaT0xO2k8YXJndW1lbnRzLmxlbmd0aDtpKyspe2FyZ3NbaS0xXT1hcmd1bWVudHNbaV07fX1cbnF1ZXVlLnB1c2gobmV3IEl0ZW0oZnVuLGFyZ3MpKTtpZihxdWV1ZS5sZW5ndGg9PT0xJiYhZHJhaW5pbmcpe3J1blRpbWVvdXQoZHJhaW5RdWV1ZSk7fX07ZnVuY3Rpb24gSXRlbShmdW4sYXJyYXkpe3RoaXMuZnVuPWZ1bjt0aGlzLmFycmF5PWFycmF5O31cbkl0ZW0ucHJvdG90eXBlLnJ1bj1mdW5jdGlvbigpe3RoaXMuZnVuLmFwcGx5KG51bGwsdGhpcy5hcnJheSk7fTtwcm9jZXNzLnRpdGxlPSdicm93c2VyJztwcm9jZXNzLmJyb3dzZXI9dHJ1ZTtwcm9jZXNzLmVudj17fTtwcm9jZXNzLmFyZ3Y9W107cHJvY2Vzcy52ZXJzaW9uPScnO3Byb2Nlc3MudmVyc2lvbnM9e307ZnVuY3Rpb24gbm9vcCgpe31cbnByb2Nlc3Mub249bm9vcDtwcm9jZXNzLmFkZExpc3RlbmVyPW5vb3A7cHJvY2Vzcy5vbmNlPW5vb3A7cHJvY2Vzcy5vZmY9bm9vcDtwcm9jZXNzLnJlbW92ZUxpc3RlbmVyPW5vb3A7cHJvY2Vzcy5yZW1vdmVBbGxMaXN0ZW5lcnM9bm9vcDtwcm9jZXNzLmVtaXQ9bm9vcDtwcm9jZXNzLnByZXBlbmRMaXN0ZW5lcj1ub29wO3Byb2Nlc3MucHJlcGVuZE9uY2VMaXN0ZW5lcj1ub29wO3Byb2Nlc3MubGlzdGVuZXJzPWZ1bmN0aW9uKG5hbWUpe3JldHVybltdfVxucHJvY2Vzcy5iaW5kaW5nPWZ1bmN0aW9uKG5hbWUpe3Rocm93IG5ldyBFcnJvcigncHJvY2Vzcy5iaW5kaW5nIGlzIG5vdCBzdXBwb3J0ZWQnKTt9O3Byb2Nlc3MuY3dkPWZ1bmN0aW9uKCl7cmV0dXJuJy8nfTtwcm9jZXNzLmNoZGlyPWZ1bmN0aW9uKGRpcil7dGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmNoZGlyIGlzIG5vdCBzdXBwb3J0ZWQnKTt9O3Byb2Nlc3MudW1hc2s9ZnVuY3Rpb24oKXtyZXR1cm4gMDt9O30se31dLDQ6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpeyhmdW5jdGlvbihnbG9iYWwpe3ZhciBub3c9cmVxdWlyZSgncGVyZm9ybWFuY2Utbm93Jykscm9vdD10eXBlb2Ygd2luZG93PT09J3VuZGVmaW5lZCc/Z2xvYmFsOndpbmRvdyx2ZW5kb3JzPVsnbW96Jywnd2Via2l0J10sc3VmZml4PSdBbmltYXRpb25GcmFtZScscmFmPXJvb3RbJ3JlcXVlc3QnK3N1ZmZpeF0sY2FmPXJvb3RbJ2NhbmNlbCcrc3VmZml4XXx8cm9vdFsnY2FuY2VsUmVxdWVzdCcrc3VmZml4XVxuZm9yKHZhciBpPTA7IXJhZiYmaTx2ZW5kb3JzLmxlbmd0aDtpKyspe3JhZj1yb290W3ZlbmRvcnNbaV0rJ1JlcXVlc3QnK3N1ZmZpeF1cbmNhZj1yb290W3ZlbmRvcnNbaV0rJ0NhbmNlbCcrc3VmZml4XXx8cm9vdFt2ZW5kb3JzW2ldKydDYW5jZWxSZXF1ZXN0JytzdWZmaXhdfVxuaWYoIXJhZnx8IWNhZil7dmFyIGxhc3Q9MCxpZD0wLHF1ZXVlPVtdLGZyYW1lRHVyYXRpb249MTAwMC82MFxucmFmPWZ1bmN0aW9uKGNhbGxiYWNrKXtpZihxdWV1ZS5sZW5ndGg9PT0wKXt2YXIgX25vdz1ub3coKSxuZXh0PU1hdGgubWF4KDAsZnJhbWVEdXJhdGlvbi0oX25vdy1sYXN0KSlcbmxhc3Q9bmV4dCtfbm93XG5zZXRUaW1lb3V0KGZ1bmN0aW9uKCl7dmFyIGNwPXF1ZXVlLnNsaWNlKDApXG5xdWV1ZS5sZW5ndGg9MFxuZm9yKHZhciBpPTA7aTxjcC5sZW5ndGg7aSsrKXtpZighY3BbaV0uY2FuY2VsbGVkKXt0cnl7Y3BbaV0uY2FsbGJhY2sobGFzdCl9Y2F0Y2goZSl7c2V0VGltZW91dChmdW5jdGlvbigpe3Rocm93IGV9LDApfX19fSxNYXRoLnJvdW5kKG5leHQpKX1cbnF1ZXVlLnB1c2goe2hhbmRsZTorK2lkLGNhbGxiYWNrOmNhbGxiYWNrLGNhbmNlbGxlZDpmYWxzZX0pXG5yZXR1cm4gaWR9XG5jYWY9ZnVuY3Rpb24oaGFuZGxlKXtmb3IodmFyIGk9MDtpPHF1ZXVlLmxlbmd0aDtpKyspe2lmKHF1ZXVlW2ldLmhhbmRsZT09PWhhbmRsZSl7cXVldWVbaV0uY2FuY2VsbGVkPXRydWV9fX19XG5tb2R1bGUuZXhwb3J0cz1mdW5jdGlvbihmbil7cmV0dXJuIHJhZi5jYWxsKHJvb3QsZm4pfVxubW9kdWxlLmV4cG9ydHMuY2FuY2VsPWZ1bmN0aW9uKCl7Y2FmLmFwcGx5KHJvb3QsYXJndW1lbnRzKX1cbm1vZHVsZS5leHBvcnRzLnBvbHlmaWxsPWZ1bmN0aW9uKCl7cm9vdC5yZXF1ZXN0QW5pbWF0aW9uRnJhbWU9cmFmXG5yb290LmNhbmNlbEFuaW1hdGlvbkZyYW1lPWNhZn19KS5jYWxsKHRoaXMsdHlwZW9mIGdsb2JhbCE9PVwidW5kZWZpbmVkXCI/Z2xvYmFsOnR5cGVvZiBzZWxmIT09XCJ1bmRlZmluZWRcIj9zZWxmOnR5cGVvZiB3aW5kb3chPT1cInVuZGVmaW5lZFwiP3dpbmRvdzp7fSl9LHtcInBlcmZvcm1hbmNlLW5vd1wiOjJ9XSw1OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXsndXNlIHN0cmljdCc7dmFyIF9jcmVhdGVDbGFzcz1mdW5jdGlvbigpe2Z1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LHByb3BzKXtmb3IodmFyIGk9MDtpPHByb3BzLmxlbmd0aDtpKyspe3ZhciBkZXNjcmlwdG9yPXByb3BzW2ldO2Rlc2NyaXB0b3IuZW51bWVyYWJsZT1kZXNjcmlwdG9yLmVudW1lcmFibGV8fGZhbHNlO2Rlc2NyaXB0b3IuY29uZmlndXJhYmxlPXRydWU7aWYoXCJ2YWx1ZVwiaW4gZGVzY3JpcHRvcilkZXNjcmlwdG9yLndyaXRhYmxlPXRydWU7T2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCxkZXNjcmlwdG9yLmtleSxkZXNjcmlwdG9yKTt9fXJldHVybiBmdW5jdGlvbihDb25zdHJ1Y3Rvcixwcm90b1Byb3BzLHN0YXRpY1Byb3BzKXtpZihwcm90b1Byb3BzKWRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLHByb3RvUHJvcHMpO2lmKHN0YXRpY1Byb3BzKWRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3Isc3RhdGljUHJvcHMpO3JldHVybiBDb25zdHJ1Y3Rvcjt9O30oKTtmdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsQ29uc3RydWN0b3Ipe2lmKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3Rvcikpe3Rocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7fX1cbnZhciBycUFuRnI9cmVxdWlyZSgncmFmJyk7dmFyIG9iamVjdEFzc2lnbj1yZXF1aXJlKCdvYmplY3QtYXNzaWduJyk7dmFyIGhlbHBlcnM9e3Byb3BlcnR5Q2FjaGU6e30sdmVuZG9yczpbbnVsbCxbJy13ZWJraXQtJywnd2Via2l0J10sWyctbW96LScsJ01veiddLFsnLW8tJywnTyddLFsnLW1zLScsJ21zJ11dLGNsYW1wOmZ1bmN0aW9uIGNsYW1wKHZhbHVlLG1pbixtYXgpe3JldHVybiBtaW48bWF4P3ZhbHVlPG1pbj9taW46dmFsdWU+bWF4P21heDp2YWx1ZTp2YWx1ZTxtYXg/bWF4OnZhbHVlPm1pbj9taW46dmFsdWU7fSxkYXRhOmZ1bmN0aW9uIGRhdGEoZWxlbWVudCxuYW1lKXtyZXR1cm4gaGVscGVycy5kZXNlcmlhbGl6ZShlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS0nK25hbWUpKTt9LGRlc2VyaWFsaXplOmZ1bmN0aW9uIGRlc2VyaWFsaXplKHZhbHVlKXtpZih2YWx1ZT09PSd0cnVlJyl7cmV0dXJuIHRydWU7fWVsc2UgaWYodmFsdWU9PT0nZmFsc2UnKXtyZXR1cm4gZmFsc2U7fWVsc2UgaWYodmFsdWU9PT0nbnVsbCcpe3JldHVybiBudWxsO31lbHNlIGlmKCFpc05hTihwYXJzZUZsb2F0KHZhbHVlKSkmJmlzRmluaXRlKHZhbHVlKSl7cmV0dXJuIHBhcnNlRmxvYXQodmFsdWUpO31lbHNle3JldHVybiB2YWx1ZTt9fSxjYW1lbENhc2U6ZnVuY3Rpb24gY2FtZWxDYXNlKHZhbHVlKXtyZXR1cm4gdmFsdWUucmVwbGFjZSgvLSsoLik/L2csZnVuY3Rpb24obWF0Y2gsY2hhcmFjdGVyKXtyZXR1cm4gY2hhcmFjdGVyP2NoYXJhY3Rlci50b1VwcGVyQ2FzZSgpOicnO30pO30sYWNjZWxlcmF0ZTpmdW5jdGlvbiBhY2NlbGVyYXRlKGVsZW1lbnQpe2hlbHBlcnMuY3NzKGVsZW1lbnQsJ3RyYW5zZm9ybScsJ3RyYW5zbGF0ZTNkKDAsMCwwKSByb3RhdGUoMC4wMDAxZGVnKScpO2hlbHBlcnMuY3NzKGVsZW1lbnQsJ3RyYW5zZm9ybS1zdHlsZScsJ3ByZXNlcnZlLTNkJyk7aGVscGVycy5jc3MoZWxlbWVudCwnYmFja2ZhY2UtdmlzaWJpbGl0eScsJ2hpZGRlbicpO30sdHJhbnNmb3JtU3VwcG9ydDpmdW5jdGlvbiB0cmFuc2Zvcm1TdXBwb3J0KHZhbHVlKXt2YXIgZWxlbWVudD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKSxwcm9wZXJ0eVN1cHBvcnQ9ZmFsc2UscHJvcGVydHlWYWx1ZT1udWxsLGZlYXR1cmVTdXBwb3J0PWZhbHNlLGNzc1Byb3BlcnR5PW51bGwsanNQcm9wZXJ0eT1udWxsO2Zvcih2YXIgaT0wLGw9aGVscGVycy52ZW5kb3JzLmxlbmd0aDtpPGw7aSsrKXtpZihoZWxwZXJzLnZlbmRvcnNbaV0hPT1udWxsKXtjc3NQcm9wZXJ0eT1oZWxwZXJzLnZlbmRvcnNbaV1bMF0rJ3RyYW5zZm9ybSc7anNQcm9wZXJ0eT1oZWxwZXJzLnZlbmRvcnNbaV1bMV0rJ1RyYW5zZm9ybSc7fWVsc2V7Y3NzUHJvcGVydHk9J3RyYW5zZm9ybSc7anNQcm9wZXJ0eT0ndHJhbnNmb3JtJzt9XG5pZihlbGVtZW50LnN0eWxlW2pzUHJvcGVydHldIT09dW5kZWZpbmVkKXtwcm9wZXJ0eVN1cHBvcnQ9dHJ1ZTticmVhazt9fVxuc3dpdGNoKHZhbHVlKXtjYXNlJzJEJzpmZWF0dXJlU3VwcG9ydD1wcm9wZXJ0eVN1cHBvcnQ7YnJlYWs7Y2FzZSczRCc6aWYocHJvcGVydHlTdXBwb3J0KXt2YXIgYm9keT1kb2N1bWVudC5ib2R5fHxkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdib2R5JyksZG9jdW1lbnRFbGVtZW50PWRvY3VtZW50LmRvY3VtZW50RWxlbWVudCxkb2N1bWVudE92ZXJmbG93PWRvY3VtZW50RWxlbWVudC5zdHlsZS5vdmVyZmxvdyxpc0NyZWF0ZWRCb2R5PWZhbHNlO2lmKCFkb2N1bWVudC5ib2R5KXtpc0NyZWF0ZWRCb2R5PXRydWU7ZG9jdW1lbnRFbGVtZW50LnN0eWxlLm92ZXJmbG93PSdoaWRkZW4nO2RvY3VtZW50RWxlbWVudC5hcHBlbmRDaGlsZChib2R5KTtib2R5LnN0eWxlLm92ZXJmbG93PSdoaWRkZW4nO2JvZHkuc3R5bGUuYmFja2dyb3VuZD0nJzt9XG5ib2R5LmFwcGVuZENoaWxkKGVsZW1lbnQpO2VsZW1lbnQuc3R5bGVbanNQcm9wZXJ0eV09J3RyYW5zbGF0ZTNkKDFweCwxcHgsMXB4KSc7cHJvcGVydHlWYWx1ZT13aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShlbGVtZW50KS5nZXRQcm9wZXJ0eVZhbHVlKGNzc1Byb3BlcnR5KTtmZWF0dXJlU3VwcG9ydD1wcm9wZXJ0eVZhbHVlIT09dW5kZWZpbmVkJiZwcm9wZXJ0eVZhbHVlLmxlbmd0aD4wJiZwcm9wZXJ0eVZhbHVlIT09J25vbmUnO2RvY3VtZW50RWxlbWVudC5zdHlsZS5vdmVyZmxvdz1kb2N1bWVudE92ZXJmbG93O2JvZHkucmVtb3ZlQ2hpbGQoZWxlbWVudCk7aWYoaXNDcmVhdGVkQm9keSl7Ym9keS5yZW1vdmVBdHRyaWJ1dGUoJ3N0eWxlJyk7Ym9keS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGJvZHkpO319XG5icmVhazt9XG5yZXR1cm4gZmVhdHVyZVN1cHBvcnQ7fSxjc3M6ZnVuY3Rpb24gY3NzKGVsZW1lbnQscHJvcGVydHksdmFsdWUpe3ZhciBqc1Byb3BlcnR5PWhlbHBlcnMucHJvcGVydHlDYWNoZVtwcm9wZXJ0eV07aWYoIWpzUHJvcGVydHkpe2Zvcih2YXIgaT0wLGw9aGVscGVycy52ZW5kb3JzLmxlbmd0aDtpPGw7aSsrKXtpZihoZWxwZXJzLnZlbmRvcnNbaV0hPT1udWxsKXtqc1Byb3BlcnR5PWhlbHBlcnMuY2FtZWxDYXNlKGhlbHBlcnMudmVuZG9yc1tpXVsxXSsnLScrcHJvcGVydHkpO31lbHNle2pzUHJvcGVydHk9cHJvcGVydHk7fVxuaWYoZWxlbWVudC5zdHlsZVtqc1Byb3BlcnR5XSE9PXVuZGVmaW5lZCl7aGVscGVycy5wcm9wZXJ0eUNhY2hlW3Byb3BlcnR5XT1qc1Byb3BlcnR5O2JyZWFrO319fVxuZWxlbWVudC5zdHlsZVtqc1Byb3BlcnR5XT12YWx1ZTt9fTt2YXIgTUFHSUNfTlVNQkVSPTMwLERFRkFVTFRTPXtyZWxhdGl2ZUlucHV0OmZhbHNlLGNsaXBSZWxhdGl2ZUlucHV0OmZhbHNlLGlucHV0RWxlbWVudDpudWxsLGhvdmVyT25seTpmYWxzZSxjYWxpYnJhdGlvblRocmVzaG9sZDoxMDAsY2FsaWJyYXRpb25EZWxheTo1MDAsc3VwcG9ydERlbGF5OjUwMCxjYWxpYnJhdGVYOmZhbHNlLGNhbGlicmF0ZVk6dHJ1ZSxpbnZlcnRYOnRydWUsaW52ZXJ0WTp0cnVlLGxpbWl0WDpmYWxzZSxsaW1pdFk6ZmFsc2Usc2NhbGFyWDoxMC4wLHNjYWxhclk6MTAuMCxmcmljdGlvblg6MC4xLGZyaWN0aW9uWTowLjEsb3JpZ2luWDowLjUsb3JpZ2luWTowLjUscG9pbnRlckV2ZW50czpmYWxzZSxwcmVjaXNpb246MSxvblJlYWR5Om51bGwsc2VsZWN0b3I6bnVsbH07dmFyIFBhcmFsbGF4PWZ1bmN0aW9uKCl7ZnVuY3Rpb24gUGFyYWxsYXgoZWxlbWVudCxvcHRpb25zKXtfY2xhc3NDYWxsQ2hlY2sodGhpcyxQYXJhbGxheCk7dGhpcy5lbGVtZW50PWVsZW1lbnQ7dmFyIGRhdGE9e2NhbGlicmF0ZVg6aGVscGVycy5kYXRhKHRoaXMuZWxlbWVudCwnY2FsaWJyYXRlLXgnKSxjYWxpYnJhdGVZOmhlbHBlcnMuZGF0YSh0aGlzLmVsZW1lbnQsJ2NhbGlicmF0ZS15JyksaW52ZXJ0WDpoZWxwZXJzLmRhdGEodGhpcy5lbGVtZW50LCdpbnZlcnQteCcpLGludmVydFk6aGVscGVycy5kYXRhKHRoaXMuZWxlbWVudCwnaW52ZXJ0LXknKSxsaW1pdFg6aGVscGVycy5kYXRhKHRoaXMuZWxlbWVudCwnbGltaXQteCcpLGxpbWl0WTpoZWxwZXJzLmRhdGEodGhpcy5lbGVtZW50LCdsaW1pdC15Jyksc2NhbGFyWDpoZWxwZXJzLmRhdGEodGhpcy5lbGVtZW50LCdzY2FsYXIteCcpLHNjYWxhclk6aGVscGVycy5kYXRhKHRoaXMuZWxlbWVudCwnc2NhbGFyLXknKSxmcmljdGlvblg6aGVscGVycy5kYXRhKHRoaXMuZWxlbWVudCwnZnJpY3Rpb24teCcpLGZyaWN0aW9uWTpoZWxwZXJzLmRhdGEodGhpcy5lbGVtZW50LCdmcmljdGlvbi15Jyksb3JpZ2luWDpoZWxwZXJzLmRhdGEodGhpcy5lbGVtZW50LCdvcmlnaW4teCcpLG9yaWdpblk6aGVscGVycy5kYXRhKHRoaXMuZWxlbWVudCwnb3JpZ2luLXknKSxwb2ludGVyRXZlbnRzOmhlbHBlcnMuZGF0YSh0aGlzLmVsZW1lbnQsJ3BvaW50ZXItZXZlbnRzJykscHJlY2lzaW9uOmhlbHBlcnMuZGF0YSh0aGlzLmVsZW1lbnQsJ3ByZWNpc2lvbicpLHJlbGF0aXZlSW5wdXQ6aGVscGVycy5kYXRhKHRoaXMuZWxlbWVudCwncmVsYXRpdmUtaW5wdXQnKSxjbGlwUmVsYXRpdmVJbnB1dDpoZWxwZXJzLmRhdGEodGhpcy5lbGVtZW50LCdjbGlwLXJlbGF0aXZlLWlucHV0JyksaG92ZXJPbmx5OmhlbHBlcnMuZGF0YSh0aGlzLmVsZW1lbnQsJ2hvdmVyLW9ubHknKSxpbnB1dEVsZW1lbnQ6ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihoZWxwZXJzLmRhdGEodGhpcy5lbGVtZW50LCdpbnB1dC1lbGVtZW50JykpLHNlbGVjdG9yOmhlbHBlcnMuZGF0YSh0aGlzLmVsZW1lbnQsJ3NlbGVjdG9yJyl9O2Zvcih2YXIga2V5IGluIGRhdGEpe2lmKGRhdGFba2V5XT09PW51bGwpe2RlbGV0ZSBkYXRhW2tleV07fX1cbm9iamVjdEFzc2lnbih0aGlzLERFRkFVTFRTLGRhdGEsb3B0aW9ucyk7aWYoIXRoaXMuaW5wdXRFbGVtZW50KXt0aGlzLmlucHV0RWxlbWVudD10aGlzLmVsZW1lbnQ7fVxudGhpcy5jYWxpYnJhdGlvblRpbWVyPW51bGw7dGhpcy5jYWxpYnJhdGlvbkZsYWc9dHJ1ZTt0aGlzLmVuYWJsZWQ9ZmFsc2U7dGhpcy5kZXB0aHNYPVtdO3RoaXMuZGVwdGhzWT1bXTt0aGlzLnJhZj1udWxsO3RoaXMuYm91bmRzPW51bGw7dGhpcy5lbGVtZW50UG9zaXRpb25YPTA7dGhpcy5lbGVtZW50UG9zaXRpb25ZPTA7dGhpcy5lbGVtZW50V2lkdGg9MDt0aGlzLmVsZW1lbnRIZWlnaHQ9MDt0aGlzLmVsZW1lbnRDZW50ZXJYPTA7dGhpcy5lbGVtZW50Q2VudGVyWT0wO3RoaXMuZWxlbWVudFJhbmdlWD0wO3RoaXMuZWxlbWVudFJhbmdlWT0wO3RoaXMuY2FsaWJyYXRpb25YPTA7dGhpcy5jYWxpYnJhdGlvblk9MDt0aGlzLmlucHV0WD0wO3RoaXMuaW5wdXRZPTA7dGhpcy5tb3Rpb25YPTA7dGhpcy5tb3Rpb25ZPTA7dGhpcy52ZWxvY2l0eVg9MDt0aGlzLnZlbG9jaXR5WT0wO3RoaXMub25Nb3VzZU1vdmU9dGhpcy5vbk1vdXNlTW92ZS5iaW5kKHRoaXMpO3RoaXMub25EZXZpY2VPcmllbnRhdGlvbj10aGlzLm9uRGV2aWNlT3JpZW50YXRpb24uYmluZCh0aGlzKTt0aGlzLm9uRGV2aWNlTW90aW9uPXRoaXMub25EZXZpY2VNb3Rpb24uYmluZCh0aGlzKTt0aGlzLm9uT3JpZW50YXRpb25UaW1lcj10aGlzLm9uT3JpZW50YXRpb25UaW1lci5iaW5kKHRoaXMpO3RoaXMub25Nb3Rpb25UaW1lcj10aGlzLm9uTW90aW9uVGltZXIuYmluZCh0aGlzKTt0aGlzLm9uQ2FsaWJyYXRpb25UaW1lcj10aGlzLm9uQ2FsaWJyYXRpb25UaW1lci5iaW5kKHRoaXMpO3RoaXMub25BbmltYXRpb25GcmFtZT10aGlzLm9uQW5pbWF0aW9uRnJhbWUuYmluZCh0aGlzKTt0aGlzLm9uV2luZG93UmVzaXplPXRoaXMub25XaW5kb3dSZXNpemUuYmluZCh0aGlzKTt0aGlzLndpbmRvd1dpZHRoPW51bGw7dGhpcy53aW5kb3dIZWlnaHQ9bnVsbDt0aGlzLndpbmRvd0NlbnRlclg9bnVsbDt0aGlzLndpbmRvd0NlbnRlclk9bnVsbDt0aGlzLndpbmRvd1JhZGl1c1g9bnVsbDt0aGlzLndpbmRvd1JhZGl1c1k9bnVsbDt0aGlzLnBvcnRyYWl0PWZhbHNlO3RoaXMuZGVza3RvcD0hbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvKGlQaG9uZXxpUG9kfGlQYWR8QW5kcm9pZHxCbGFja0JlcnJ5fEJCMTB8bW9iaXx0YWJsZXR8b3BlcmEgbWluaXxuZXh1cyA3KS9pKTt0aGlzLm1vdGlvblN1cHBvcnQ9ISF3aW5kb3cuRGV2aWNlTW90aW9uRXZlbnQmJiF0aGlzLmRlc2t0b3A7dGhpcy5vcmllbnRhdGlvblN1cHBvcnQ9ISF3aW5kb3cuRGV2aWNlT3JpZW50YXRpb25FdmVudCYmIXRoaXMuZGVza3RvcDt0aGlzLm9yaWVudGF0aW9uU3RhdHVzPTA7dGhpcy5tb3Rpb25TdGF0dXM9MDt0aGlzLmluaXRpYWxpc2UoKTt9XG5fY3JlYXRlQ2xhc3MoUGFyYWxsYXgsW3trZXk6J2luaXRpYWxpc2UnLHZhbHVlOmZ1bmN0aW9uIGluaXRpYWxpc2UoKXtpZih0aGlzLnRyYW5zZm9ybTJEU3VwcG9ydD09PXVuZGVmaW5lZCl7dGhpcy50cmFuc2Zvcm0yRFN1cHBvcnQ9aGVscGVycy50cmFuc2Zvcm1TdXBwb3J0KCcyRCcpO3RoaXMudHJhbnNmb3JtM0RTdXBwb3J0PWhlbHBlcnMudHJhbnNmb3JtU3VwcG9ydCgnM0QnKTt9XG5pZih0aGlzLnRyYW5zZm9ybTNEU3VwcG9ydCl7aGVscGVycy5hY2NlbGVyYXRlKHRoaXMuZWxlbWVudCk7fVxudmFyIHN0eWxlPXdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKHRoaXMuZWxlbWVudCk7aWYoc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgncG9zaXRpb24nKT09PSdzdGF0aWMnKXt0aGlzLmVsZW1lbnQuc3R5bGUucG9zaXRpb249J3JlbGF0aXZlJzt9XG5pZighdGhpcy5wb2ludGVyRXZlbnRzKXt0aGlzLmVsZW1lbnQuc3R5bGUucG9pbnRlckV2ZW50cz0nbm9uZSc7fVxudGhpcy51cGRhdGVMYXllcnMoKTt0aGlzLnVwZGF0ZURpbWVuc2lvbnMoKTt0aGlzLmVuYWJsZSgpO3RoaXMucXVldWVDYWxpYnJhdGlvbih0aGlzLmNhbGlicmF0aW9uRGVsYXkpO319LHtrZXk6J2RvUmVhZHlDYWxsYmFjaycsdmFsdWU6ZnVuY3Rpb24gZG9SZWFkeUNhbGxiYWNrKCl7aWYodGhpcy5vblJlYWR5KXt0aGlzLm9uUmVhZHkoKTt9fX0se2tleTondXBkYXRlTGF5ZXJzJyx2YWx1ZTpmdW5jdGlvbiB1cGRhdGVMYXllcnMoKXtpZih0aGlzLnNlbGVjdG9yKXt0aGlzLmxheWVycz10aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvckFsbCh0aGlzLnNlbGVjdG9yKTt9ZWxzZXt0aGlzLmxheWVycz10aGlzLmVsZW1lbnQuY2hpbGRyZW47fVxuaWYoIXRoaXMubGF5ZXJzLmxlbmd0aCl7Y29uc29sZS53YXJuKCdQYXJhbGxheEpTOiBZb3VyIHNjZW5lIGRvZXMgbm90IGhhdmUgYW55IGxheWVycy4nKTt9XG50aGlzLmRlcHRoc1g9W107dGhpcy5kZXB0aHNZPVtdO2Zvcih2YXIgaW5kZXg9MDtpbmRleDx0aGlzLmxheWVycy5sZW5ndGg7aW5kZXgrKyl7dmFyIGxheWVyPXRoaXMubGF5ZXJzW2luZGV4XTtpZih0aGlzLnRyYW5zZm9ybTNEU3VwcG9ydCl7aGVscGVycy5hY2NlbGVyYXRlKGxheWVyKTt9XG5sYXllci5zdHlsZS5wb3NpdGlvbj1pbmRleD8nYWJzb2x1dGUnOidyZWxhdGl2ZSc7bGF5ZXIuc3R5bGUuZGlzcGxheT0nYmxvY2snO2xheWVyLnN0eWxlLmxlZnQ9MDtsYXllci5zdHlsZS50b3A9MDt2YXIgZGVwdGg9aGVscGVycy5kYXRhKGxheWVyLCdkZXB0aCcpfHwwO3RoaXMuZGVwdGhzWC5wdXNoKGhlbHBlcnMuZGF0YShsYXllciwnZGVwdGgteCcpfHxkZXB0aCk7dGhpcy5kZXB0aHNZLnB1c2goaGVscGVycy5kYXRhKGxheWVyLCdkZXB0aC15Jyl8fGRlcHRoKTt9fX0se2tleTondXBkYXRlRGltZW5zaW9ucycsdmFsdWU6ZnVuY3Rpb24gdXBkYXRlRGltZW5zaW9ucygpe3RoaXMud2luZG93V2lkdGg9d2luZG93LmlubmVyV2lkdGg7dGhpcy53aW5kb3dIZWlnaHQ9d2luZG93LmlubmVySGVpZ2h0O3RoaXMud2luZG93Q2VudGVyWD10aGlzLndpbmRvd1dpZHRoKnRoaXMub3JpZ2luWDt0aGlzLndpbmRvd0NlbnRlclk9dGhpcy53aW5kb3dIZWlnaHQqdGhpcy5vcmlnaW5ZO3RoaXMud2luZG93UmFkaXVzWD1NYXRoLm1heCh0aGlzLndpbmRvd0NlbnRlclgsdGhpcy53aW5kb3dXaWR0aC10aGlzLndpbmRvd0NlbnRlclgpO3RoaXMud2luZG93UmFkaXVzWT1NYXRoLm1heCh0aGlzLndpbmRvd0NlbnRlclksdGhpcy53aW5kb3dIZWlnaHQtdGhpcy53aW5kb3dDZW50ZXJZKTt9fSx7a2V5Oid1cGRhdGVCb3VuZHMnLHZhbHVlOmZ1bmN0aW9uIHVwZGF0ZUJvdW5kcygpe3RoaXMuYm91bmRzPXRoaXMuaW5wdXRFbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO3RoaXMuZWxlbWVudFBvc2l0aW9uWD10aGlzLmJvdW5kcy5sZWZ0O3RoaXMuZWxlbWVudFBvc2l0aW9uWT10aGlzLmJvdW5kcy50b3A7dGhpcy5lbGVtZW50V2lkdGg9dGhpcy5ib3VuZHMud2lkdGg7dGhpcy5lbGVtZW50SGVpZ2h0PXRoaXMuYm91bmRzLmhlaWdodDt0aGlzLmVsZW1lbnRDZW50ZXJYPXRoaXMuZWxlbWVudFdpZHRoKnRoaXMub3JpZ2luWDt0aGlzLmVsZW1lbnRDZW50ZXJZPXRoaXMuZWxlbWVudEhlaWdodCp0aGlzLm9yaWdpblk7dGhpcy5lbGVtZW50UmFuZ2VYPU1hdGgubWF4KHRoaXMuZWxlbWVudENlbnRlclgsdGhpcy5lbGVtZW50V2lkdGgtdGhpcy5lbGVtZW50Q2VudGVyWCk7dGhpcy5lbGVtZW50UmFuZ2VZPU1hdGgubWF4KHRoaXMuZWxlbWVudENlbnRlclksdGhpcy5lbGVtZW50SGVpZ2h0LXRoaXMuZWxlbWVudENlbnRlclkpO319LHtrZXk6J3F1ZXVlQ2FsaWJyYXRpb24nLHZhbHVlOmZ1bmN0aW9uIHF1ZXVlQ2FsaWJyYXRpb24oZGVsYXkpe2NsZWFyVGltZW91dCh0aGlzLmNhbGlicmF0aW9uVGltZXIpO3RoaXMuY2FsaWJyYXRpb25UaW1lcj1zZXRUaW1lb3V0KHRoaXMub25DYWxpYnJhdGlvblRpbWVyLGRlbGF5KTt9fSx7a2V5OidlbmFibGUnLHZhbHVlOmZ1bmN0aW9uIGVuYWJsZSgpe2lmKHRoaXMuZW5hYmxlZCl7cmV0dXJuO31cbnRoaXMuZW5hYmxlZD10cnVlO2lmKHRoaXMub3JpZW50YXRpb25TdXBwb3J0KXt0aGlzLnBvcnRyYWl0PWZhbHNlO3dpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdkZXZpY2VvcmllbnRhdGlvbicsdGhpcy5vbkRldmljZU9yaWVudGF0aW9uKTt0aGlzLmRldGVjdGlvblRpbWVyPXNldFRpbWVvdXQodGhpcy5vbk9yaWVudGF0aW9uVGltZXIsdGhpcy5zdXBwb3J0RGVsYXkpO31lbHNlIGlmKHRoaXMubW90aW9uU3VwcG9ydCl7dGhpcy5wb3J0cmFpdD1mYWxzZTt3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignZGV2aWNlbW90aW9uJyx0aGlzLm9uRGV2aWNlTW90aW9uKTt0aGlzLmRldGVjdGlvblRpbWVyPXNldFRpbWVvdXQodGhpcy5vbk1vdGlvblRpbWVyLHRoaXMuc3VwcG9ydERlbGF5KTt9ZWxzZXt0aGlzLmNhbGlicmF0aW9uWD0wO3RoaXMuY2FsaWJyYXRpb25ZPTA7dGhpcy5wb3J0cmFpdD1mYWxzZTt3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJyx0aGlzLm9uTW91c2VNb3ZlKTt0aGlzLmRvUmVhZHlDYWxsYmFjaygpO31cbndpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLHRoaXMub25XaW5kb3dSZXNpemUpO3RoaXMucmFmPXJxQW5Gcih0aGlzLm9uQW5pbWF0aW9uRnJhbWUpO319LHtrZXk6J2Rpc2FibGUnLHZhbHVlOmZ1bmN0aW9uIGRpc2FibGUoKXtpZighdGhpcy5lbmFibGVkKXtyZXR1cm47fVxudGhpcy5lbmFibGVkPWZhbHNlO2lmKHRoaXMub3JpZW50YXRpb25TdXBwb3J0KXt3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignZGV2aWNlb3JpZW50YXRpb24nLHRoaXMub25EZXZpY2VPcmllbnRhdGlvbik7fWVsc2UgaWYodGhpcy5tb3Rpb25TdXBwb3J0KXt3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignZGV2aWNlbW90aW9uJyx0aGlzLm9uRGV2aWNlTW90aW9uKTt9ZWxzZXt3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJyx0aGlzLm9uTW91c2VNb3ZlKTt9XG53aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcigncmVzaXplJyx0aGlzLm9uV2luZG93UmVzaXplKTtycUFuRnIuY2FuY2VsKHRoaXMucmFmKTt9fSx7a2V5OidjYWxpYnJhdGUnLHZhbHVlOmZ1bmN0aW9uIGNhbGlicmF0ZSh4LHkpe3RoaXMuY2FsaWJyYXRlWD14PT09dW5kZWZpbmVkP3RoaXMuY2FsaWJyYXRlWDp4O3RoaXMuY2FsaWJyYXRlWT15PT09dW5kZWZpbmVkP3RoaXMuY2FsaWJyYXRlWTp5O319LHtrZXk6J2ludmVydCcsdmFsdWU6ZnVuY3Rpb24gaW52ZXJ0KHgseSl7dGhpcy5pbnZlcnRYPXg9PT11bmRlZmluZWQ/dGhpcy5pbnZlcnRYOng7dGhpcy5pbnZlcnRZPXk9PT11bmRlZmluZWQ/dGhpcy5pbnZlcnRZOnk7fX0se2tleTonZnJpY3Rpb24nLHZhbHVlOmZ1bmN0aW9uIGZyaWN0aW9uKHgseSl7dGhpcy5mcmljdGlvblg9eD09PXVuZGVmaW5lZD90aGlzLmZyaWN0aW9uWDp4O3RoaXMuZnJpY3Rpb25ZPXk9PT11bmRlZmluZWQ/dGhpcy5mcmljdGlvblk6eTt9fSx7a2V5OidzY2FsYXInLHZhbHVlOmZ1bmN0aW9uIHNjYWxhcih4LHkpe3RoaXMuc2NhbGFyWD14PT09dW5kZWZpbmVkP3RoaXMuc2NhbGFyWDp4O3RoaXMuc2NhbGFyWT15PT09dW5kZWZpbmVkP3RoaXMuc2NhbGFyWTp5O319LHtrZXk6J2xpbWl0Jyx2YWx1ZTpmdW5jdGlvbiBsaW1pdCh4LHkpe3RoaXMubGltaXRYPXg9PT11bmRlZmluZWQ/dGhpcy5saW1pdFg6eDt0aGlzLmxpbWl0WT15PT09dW5kZWZpbmVkP3RoaXMubGltaXRZOnk7fX0se2tleTonb3JpZ2luJyx2YWx1ZTpmdW5jdGlvbiBvcmlnaW4oeCx5KXt0aGlzLm9yaWdpblg9eD09PXVuZGVmaW5lZD90aGlzLm9yaWdpblg6eDt0aGlzLm9yaWdpblk9eT09PXVuZGVmaW5lZD90aGlzLm9yaWdpblk6eTt9fSx7a2V5OidzZXRJbnB1dEVsZW1lbnQnLHZhbHVlOmZ1bmN0aW9uIHNldElucHV0RWxlbWVudChlbGVtZW50KXt0aGlzLmlucHV0RWxlbWVudD1lbGVtZW50O3RoaXMudXBkYXRlRGltZW5zaW9ucygpO319LHtrZXk6J3NldFBvc2l0aW9uJyx2YWx1ZTpmdW5jdGlvbiBzZXRQb3NpdGlvbihlbGVtZW50LHgseSl7eD14LnRvRml4ZWQodGhpcy5wcmVjaXNpb24pKydweCc7eT15LnRvRml4ZWQodGhpcy5wcmVjaXNpb24pKydweCc7aWYodGhpcy50cmFuc2Zvcm0zRFN1cHBvcnQpe2hlbHBlcnMuY3NzKGVsZW1lbnQsJ3RyYW5zZm9ybScsJ3RyYW5zbGF0ZTNkKCcreCsnLCcreSsnLDApJyk7fWVsc2UgaWYodGhpcy50cmFuc2Zvcm0yRFN1cHBvcnQpe2hlbHBlcnMuY3NzKGVsZW1lbnQsJ3RyYW5zZm9ybScsJ3RyYW5zbGF0ZSgnK3grJywnK3krJyknKTt9ZWxzZXtlbGVtZW50LnN0eWxlLmxlZnQ9eDtlbGVtZW50LnN0eWxlLnRvcD15O319fSx7a2V5Oidvbk9yaWVudGF0aW9uVGltZXInLHZhbHVlOmZ1bmN0aW9uIG9uT3JpZW50YXRpb25UaW1lcigpe2lmKHRoaXMub3JpZW50YXRpb25TdXBwb3J0JiZ0aGlzLm9yaWVudGF0aW9uU3RhdHVzPT09MCl7dGhpcy5kaXNhYmxlKCk7dGhpcy5vcmllbnRhdGlvblN1cHBvcnQ9ZmFsc2U7dGhpcy5lbmFibGUoKTt9ZWxzZXt0aGlzLmRvUmVhZHlDYWxsYmFjaygpO319fSx7a2V5Oidvbk1vdGlvblRpbWVyJyx2YWx1ZTpmdW5jdGlvbiBvbk1vdGlvblRpbWVyKCl7aWYodGhpcy5tb3Rpb25TdXBwb3J0JiZ0aGlzLm1vdGlvblN0YXR1cz09PTApe3RoaXMuZGlzYWJsZSgpO3RoaXMubW90aW9uU3VwcG9ydD1mYWxzZTt0aGlzLmVuYWJsZSgpO31lbHNle3RoaXMuZG9SZWFkeUNhbGxiYWNrKCk7fX19LHtrZXk6J29uQ2FsaWJyYXRpb25UaW1lcicsdmFsdWU6ZnVuY3Rpb24gb25DYWxpYnJhdGlvblRpbWVyKCl7dGhpcy5jYWxpYnJhdGlvbkZsYWc9dHJ1ZTt9fSx7a2V5OidvbldpbmRvd1Jlc2l6ZScsdmFsdWU6ZnVuY3Rpb24gb25XaW5kb3dSZXNpemUoKXt0aGlzLnVwZGF0ZURpbWVuc2lvbnMoKTt9fSx7a2V5OidvbkFuaW1hdGlvbkZyYW1lJyx2YWx1ZTpmdW5jdGlvbiBvbkFuaW1hdGlvbkZyYW1lKCl7dGhpcy51cGRhdGVCb3VuZHMoKTt2YXIgY2FsaWJyYXRlZElucHV0WD10aGlzLmlucHV0WC10aGlzLmNhbGlicmF0aW9uWCxjYWxpYnJhdGVkSW5wdXRZPXRoaXMuaW5wdXRZLXRoaXMuY2FsaWJyYXRpb25ZO2lmKE1hdGguYWJzKGNhbGlicmF0ZWRJbnB1dFgpPnRoaXMuY2FsaWJyYXRpb25UaHJlc2hvbGR8fE1hdGguYWJzKGNhbGlicmF0ZWRJbnB1dFkpPnRoaXMuY2FsaWJyYXRpb25UaHJlc2hvbGQpe3RoaXMucXVldWVDYWxpYnJhdGlvbigwKTt9XG5pZih0aGlzLnBvcnRyYWl0KXt0aGlzLm1vdGlvblg9dGhpcy5jYWxpYnJhdGVYP2NhbGlicmF0ZWRJbnB1dFk6dGhpcy5pbnB1dFk7dGhpcy5tb3Rpb25ZPXRoaXMuY2FsaWJyYXRlWT9jYWxpYnJhdGVkSW5wdXRYOnRoaXMuaW5wdXRYO31lbHNle3RoaXMubW90aW9uWD10aGlzLmNhbGlicmF0ZVg/Y2FsaWJyYXRlZElucHV0WDp0aGlzLmlucHV0WDt0aGlzLm1vdGlvblk9dGhpcy5jYWxpYnJhdGVZP2NhbGlicmF0ZWRJbnB1dFk6dGhpcy5pbnB1dFk7fVxudGhpcy5tb3Rpb25YKj10aGlzLmVsZW1lbnRXaWR0aCoodGhpcy5zY2FsYXJYLzEwMCk7dGhpcy5tb3Rpb25ZKj10aGlzLmVsZW1lbnRIZWlnaHQqKHRoaXMuc2NhbGFyWS8xMDApO2lmKCFpc05hTihwYXJzZUZsb2F0KHRoaXMubGltaXRYKSkpe3RoaXMubW90aW9uWD1oZWxwZXJzLmNsYW1wKHRoaXMubW90aW9uWCwtdGhpcy5saW1pdFgsdGhpcy5saW1pdFgpO31cbmlmKCFpc05hTihwYXJzZUZsb2F0KHRoaXMubGltaXRZKSkpe3RoaXMubW90aW9uWT1oZWxwZXJzLmNsYW1wKHRoaXMubW90aW9uWSwtdGhpcy5saW1pdFksdGhpcy5saW1pdFkpO31cbnRoaXMudmVsb2NpdHlYKz0odGhpcy5tb3Rpb25YLXRoaXMudmVsb2NpdHlYKSp0aGlzLmZyaWN0aW9uWDt0aGlzLnZlbG9jaXR5WSs9KHRoaXMubW90aW9uWS10aGlzLnZlbG9jaXR5WSkqdGhpcy5mcmljdGlvblk7Zm9yKHZhciBpbmRleD0wO2luZGV4PHRoaXMubGF5ZXJzLmxlbmd0aDtpbmRleCsrKXt2YXIgbGF5ZXI9dGhpcy5sYXllcnNbaW5kZXhdLGRlcHRoWD10aGlzLmRlcHRoc1hbaW5kZXhdLGRlcHRoWT10aGlzLmRlcHRoc1lbaW5kZXhdLHhPZmZzZXQ9dGhpcy52ZWxvY2l0eVgqKGRlcHRoWCoodGhpcy5pbnZlcnRYPy0xOjEpKSx5T2Zmc2V0PXRoaXMudmVsb2NpdHlZKihkZXB0aFkqKHRoaXMuaW52ZXJ0WT8tMToxKSk7dGhpcy5zZXRQb3NpdGlvbihsYXllcix4T2Zmc2V0LHlPZmZzZXQpO31cbnRoaXMucmFmPXJxQW5Gcih0aGlzLm9uQW5pbWF0aW9uRnJhbWUpO319LHtrZXk6J3JvdGF0ZScsdmFsdWU6ZnVuY3Rpb24gcm90YXRlKGJldGEsZ2FtbWEpe3ZhciB4PShiZXRhfHwwKS9NQUdJQ19OVU1CRVIseT0oZ2FtbWF8fDApL01BR0lDX05VTUJFUjt2YXIgcG9ydHJhaXQ9dGhpcy53aW5kb3dIZWlnaHQ+dGhpcy53aW5kb3dXaWR0aDtpZih0aGlzLnBvcnRyYWl0IT09cG9ydHJhaXQpe3RoaXMucG9ydHJhaXQ9cG9ydHJhaXQ7dGhpcy5jYWxpYnJhdGlvbkZsYWc9dHJ1ZTt9XG5pZih0aGlzLmNhbGlicmF0aW9uRmxhZyl7dGhpcy5jYWxpYnJhdGlvbkZsYWc9ZmFsc2U7dGhpcy5jYWxpYnJhdGlvblg9eDt0aGlzLmNhbGlicmF0aW9uWT15O31cbnRoaXMuaW5wdXRYPXg7dGhpcy5pbnB1dFk9eTt9fSx7a2V5OidvbkRldmljZU9yaWVudGF0aW9uJyx2YWx1ZTpmdW5jdGlvbiBvbkRldmljZU9yaWVudGF0aW9uKGV2ZW50KXt2YXIgYmV0YT1ldmVudC5iZXRhO3ZhciBnYW1tYT1ldmVudC5nYW1tYTtpZihiZXRhIT09bnVsbCYmZ2FtbWEhPT1udWxsKXt0aGlzLm9yaWVudGF0aW9uU3RhdHVzPTE7dGhpcy5yb3RhdGUoYmV0YSxnYW1tYSk7fX19LHtrZXk6J29uRGV2aWNlTW90aW9uJyx2YWx1ZTpmdW5jdGlvbiBvbkRldmljZU1vdGlvbihldmVudCl7dmFyIGJldGE9ZXZlbnQucm90YXRpb25SYXRlLmJldGE7dmFyIGdhbW1hPWV2ZW50LnJvdGF0aW9uUmF0ZS5nYW1tYTtpZihiZXRhIT09bnVsbCYmZ2FtbWEhPT1udWxsKXt0aGlzLm1vdGlvblN0YXR1cz0xO3RoaXMucm90YXRlKGJldGEsZ2FtbWEpO319fSx7a2V5Oidvbk1vdXNlTW92ZScsdmFsdWU6ZnVuY3Rpb24gb25Nb3VzZU1vdmUoZXZlbnQpe3ZhciBjbGllbnRYPWV2ZW50LmNsaWVudFgsY2xpZW50WT1ldmVudC5jbGllbnRZO2lmKHRoaXMuaG92ZXJPbmx5JiYoY2xpZW50WDx0aGlzLmVsZW1lbnRQb3NpdGlvblh8fGNsaWVudFg+dGhpcy5lbGVtZW50UG9zaXRpb25YK3RoaXMuZWxlbWVudFdpZHRofHxjbGllbnRZPHRoaXMuZWxlbWVudFBvc2l0aW9uWXx8Y2xpZW50WT50aGlzLmVsZW1lbnRQb3NpdGlvblkrdGhpcy5lbGVtZW50SGVpZ2h0KSl7dGhpcy5pbnB1dFg9MDt0aGlzLmlucHV0WT0wO3JldHVybjt9XG5pZih0aGlzLnJlbGF0aXZlSW5wdXQpe2lmKHRoaXMuY2xpcFJlbGF0aXZlSW5wdXQpe2NsaWVudFg9TWF0aC5tYXgoY2xpZW50WCx0aGlzLmVsZW1lbnRQb3NpdGlvblgpO2NsaWVudFg9TWF0aC5taW4oY2xpZW50WCx0aGlzLmVsZW1lbnRQb3NpdGlvblgrdGhpcy5lbGVtZW50V2lkdGgpO2NsaWVudFk9TWF0aC5tYXgoY2xpZW50WSx0aGlzLmVsZW1lbnRQb3NpdGlvblkpO2NsaWVudFk9TWF0aC5taW4oY2xpZW50WSx0aGlzLmVsZW1lbnRQb3NpdGlvblkrdGhpcy5lbGVtZW50SGVpZ2h0KTt9XG5pZih0aGlzLmVsZW1lbnRSYW5nZVgmJnRoaXMuZWxlbWVudFJhbmdlWSl7dGhpcy5pbnB1dFg9KGNsaWVudFgtdGhpcy5lbGVtZW50UG9zaXRpb25YLXRoaXMuZWxlbWVudENlbnRlclgpL3RoaXMuZWxlbWVudFJhbmdlWDt0aGlzLmlucHV0WT0oY2xpZW50WS10aGlzLmVsZW1lbnRQb3NpdGlvblktdGhpcy5lbGVtZW50Q2VudGVyWSkvdGhpcy5lbGVtZW50UmFuZ2VZO319ZWxzZXtpZih0aGlzLndpbmRvd1JhZGl1c1gmJnRoaXMud2luZG93UmFkaXVzWSl7dGhpcy5pbnB1dFg9KGNsaWVudFgtdGhpcy53aW5kb3dDZW50ZXJYKS90aGlzLndpbmRvd1JhZGl1c1g7dGhpcy5pbnB1dFk9KGNsaWVudFktdGhpcy53aW5kb3dDZW50ZXJZKS90aGlzLndpbmRvd1JhZGl1c1k7fX19fSx7a2V5OidkZXN0cm95Jyx2YWx1ZTpmdW5jdGlvbiBkZXN0cm95KCl7dGhpcy5kaXNhYmxlKCk7Y2xlYXJUaW1lb3V0KHRoaXMuY2FsaWJyYXRpb25UaW1lcik7Y2xlYXJUaW1lb3V0KHRoaXMuZGV0ZWN0aW9uVGltZXIpO3RoaXMuZWxlbWVudC5yZW1vdmVBdHRyaWJ1dGUoJ3N0eWxlJyk7Zm9yKHZhciBpbmRleD0wO2luZGV4PHRoaXMubGF5ZXJzLmxlbmd0aDtpbmRleCsrKXt0aGlzLmxheWVyc1tpbmRleF0ucmVtb3ZlQXR0cmlidXRlKCdzdHlsZScpO31cbmRlbGV0ZSB0aGlzLmVsZW1lbnQ7ZGVsZXRlIHRoaXMubGF5ZXJzO319LHtrZXk6J3ZlcnNpb24nLHZhbHVlOmZ1bmN0aW9uIHZlcnNpb24oKXtyZXR1cm4nMy4xLjAnO319XSk7cmV0dXJuIFBhcmFsbGF4O30oKTttb2R1bGUuZXhwb3J0cz1QYXJhbGxheDt9LHtcIm9iamVjdC1hc3NpZ25cIjoxLFwicmFmXCI6NH1dfSx7fSxbNV0pKDUpfSk7O1xuLyohIEJsYXN0LmpzICgyLjAuMCk6IGp1bGlhbi5jb20vcmVzZWFyY2gvYmxhc3QgKEMpIDIwMTUgSnVsaWFuIFNoYXBpcm8uIE1JVCBAbGljZW5zZTogZW4ud2lraXBlZGlhLm9yZy93aWtpL01JVF9MaWNlbnNlICovXG47KGZ1bmN0aW9uKCQsd2luZG93LGRvY3VtZW50LHVuZGVmaW5lZCl7dmFyIElFPShmdW5jdGlvbigpe2lmKGRvY3VtZW50LmRvY3VtZW50TW9kZSl7cmV0dXJuIGRvY3VtZW50LmRvY3VtZW50TW9kZTt9ZWxzZXtmb3IodmFyIGk9NztpPjA7aS0tKXt2YXIgZGl2PWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7ZGl2LmlubmVySFRNTD1cIjwhLS1baWYgSUUgXCIraStcIl0+PHNwYW4+PC9zcGFuPjwhW2VuZGlmXS0tPlwiO2lmKGRpdi5nZXRFbGVtZW50c0J5VGFnTmFtZShcInNwYW5cIikubGVuZ3RoKXtkaXY9bnVsbDtyZXR1cm4gaTt9XG5kaXY9bnVsbDt9fVxucmV0dXJuIHVuZGVmaW5lZDt9KSgpO3ZhciBjb25zb2xlPXdpbmRvdy5jb25zb2xlfHx7bG9nOmZ1bmN0aW9uKCl7fSx0aW1lOmZ1bmN0aW9uKCl7fX07dmFyIE5BTUU9XCJibGFzdFwiLGNoYXJhY3RlclJhbmdlcz17bGF0aW5QdW5jdHVhdGlvbjpcIuKAk+KAlOKAsuKAmSfigJzigLPigJ5cXFwiKMKrLuKApsKhwr/igLLigJkn4oCd4oCz4oCcXFxcIinCuy7igKYhP1wiLGxhdGluTGV0dGVyczpcIlxcXFx1MDA0MS1cXFxcdTAwNUFcXFxcdTAwNjEtXFxcXHUwMDdBXFxcXHUwMEMwLVxcXFx1MDE3RlxcXFx1MDEwMC1cXFxcdTAxRkZcXFxcdTAxODAtXFxcXHUwMjdGXCJ9LFJlZz17YWJicmV2aWF0aW9uczpuZXcgUmVnRXhwKFwiW15cIitjaGFyYWN0ZXJSYW5nZXMubGF0aW5MZXR0ZXJzK1wiXShlXFxcXC5nXFxcXC4pfChpXFxcXC5lXFxcXC4pfChtclxcXFwuKXwobXJzXFxcXC4pfChtc1xcXFwuKXwoZHJcXFxcLil8KHByb2ZcXFxcLil8KGVzcVxcXFwuKXwoc3JcXFxcLil8KGpyXFxcXC4pW15cIitjaGFyYWN0ZXJSYW5nZXMubGF0aW5MZXR0ZXJzK1wiXVwiLFwiaWdcIiksaW5uZXJXb3JkUGVyaW9kOm5ldyBSZWdFeHAoXCJbXCIrY2hhcmFjdGVyUmFuZ2VzLmxhdGluTGV0dGVycytcIl1cXC5bXCIrY2hhcmFjdGVyUmFuZ2VzLmxhdGluTGV0dGVycytcIl1cIixcImlnXCIpLG9ubHlDb250YWluc1B1bmN0dWF0aW9uOm5ldyBSZWdFeHAoXCJbXlwiK2NoYXJhY3RlclJhbmdlcy5sYXRpblB1bmN0dWF0aW9uK1wiXVwiKSxhZGpvaW5lZFB1bmN0dWF0aW9uOm5ldyBSZWdFeHAoXCJeW1wiK2NoYXJhY3RlclJhbmdlcy5sYXRpblB1bmN0dWF0aW9uK1wiXSt8W1wiK2NoYXJhY3RlclJhbmdlcy5sYXRpblB1bmN0dWF0aW9uK1wiXSskXCIsXCJnXCIpLHNraXBwZWRFbGVtZW50czovKHNjcmlwdHxzdHlsZXxzZWxlY3R8dGV4dGFyZWEpL2ksaGFzUGx1Z2luQ2xhc3M6bmV3IFJlZ0V4cChcIihefCApXCIrTkFNRStcIiggfCQpXCIsXCJnaVwiKX07JC5mbltOQU1FXT1mdW5jdGlvbihvcHRpb25zKXtmdW5jdGlvbiBlbmNvZGVQdW5jdHVhdGlvbih0ZXh0KXtyZXR1cm4gdGV4dC5yZXBsYWNlKFJlZy5hYmJyZXZpYXRpb25zLGZ1bmN0aW9uKG1hdGNoKXtyZXR1cm4gbWF0Y2gucmVwbGFjZSgvXFwuL2csXCJ7ezQ2fX1cIik7fSkucmVwbGFjZShSZWcuaW5uZXJXb3JkUGVyaW9kLGZ1bmN0aW9uKG1hdGNoKXtyZXR1cm4gbWF0Y2gucmVwbGFjZSgvXFwuL2csXCJ7ezQ2fX1cIik7fSk7fVxuZnVuY3Rpb24gZGVjb2RlUHVuY3R1YXRpb24odGV4dCl7cmV0dXJuIHRleHQucmVwbGFjZSgve3soXFxkezEsM30pfX0vZyxmdW5jdGlvbihmdWxsTWF0Y2gsc3ViTWF0Y2gpe3JldHVybiBTdHJpbmcuZnJvbUNoYXJDb2RlKHN1Yk1hdGNoKTt9KTt9XG5mdW5jdGlvbiB3cmFwTm9kZShub2RlLG9wdHMpe3ZhciB3cmFwcGVyPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQob3B0cy50YWcpO3dyYXBwZXIuY2xhc3NOYW1lPU5BTUU7aWYob3B0cy5jdXN0b21DbGFzcyl7d3JhcHBlci5jbGFzc05hbWUrPVwiIFwiK29wdHMuY3VzdG9tQ2xhc3M7aWYob3B0cy5nZW5lcmF0ZUluZGV4SUQpe3dyYXBwZXIuaWQ9b3B0cy5jdXN0b21DbGFzcytcIi1cIitFbGVtZW50LmJsYXN0ZWRJbmRleDt9fVxuaWYob3B0cy5kZWxpbWl0ZXI9PT1cImFsbFwiJiYvXFxzLy50ZXN0KG5vZGUuZGF0YSkpe3dyYXBwZXIuc3R5bGUud2hpdGVTcGFjZT1cInByZS1saW5lXCI7fVxuaWYob3B0cy5nZW5lcmF0ZVZhbHVlQ2xhc3M9PT10cnVlJiYhb3B0cy5zZWFyY2gmJihvcHRzLmRlbGltaXRlcj09PVwiY2hhcmFjdGVyXCJ8fG9wdHMuZGVsaW1pdGVyPT09XCJ3b3JkXCIpKXt2YXIgdmFsdWVDbGFzcyx0ZXh0PW5vZGUuZGF0YTtpZihvcHRzLmRlbGltaXRlcj09PVwid29yZFwiJiZSZWcub25seUNvbnRhaW5zUHVuY3R1YXRpb24udGVzdCh0ZXh0KSl7dGV4dD10ZXh0LnJlcGxhY2UoUmVnLmFkam9pbmVkUHVuY3R1YXRpb24sXCJcIik7fVxudmFsdWVDbGFzcz1OQU1FK1wiLVwiK29wdHMuZGVsaW1pdGVyLnRvTG93ZXJDYXNlKCkrXCItXCIrdGV4dC50b0xvd2VyQ2FzZSgpO3dyYXBwZXIuY2xhc3NOYW1lKz1cIiBcIit2YWx1ZUNsYXNzO31cbmlmKG9wdHMuYXJpYSl7d3JhcHBlci5zZXRBdHRyaWJ1dGUoXCJhcmlhLWhpZGRlblwiLFwidHJ1ZVwiKTt9XG53cmFwcGVyLmFwcGVuZENoaWxkKG5vZGUuY2xvbmVOb2RlKGZhbHNlKSk7cmV0dXJuIHdyYXBwZXI7fVxuZnVuY3Rpb24gdHJhdmVyc2VET00obm9kZSxvcHRzKXt2YXIgbWF0Y2hQb3NpdGlvbj0tMSxza2lwTm9kZUJpdD0wO2lmKG5vZGUubm9kZVR5cGU9PT0zJiZub2RlLmRhdGEubGVuZ3RoKXtpZihFbGVtZW50Lm5vZGVCZWdpbm5pbmcpe25vZGUuZGF0YT0oIW9wdHMuc2VhcmNoJiZvcHRzLmRlbGltaXRlcj09PVwic2VudGVuY2VcIik/ZW5jb2RlUHVuY3R1YXRpb24obm9kZS5kYXRhKTpkZWNvZGVQdW5jdHVhdGlvbihub2RlLmRhdGEpO0VsZW1lbnQubm9kZUJlZ2lubmluZz1mYWxzZTt9XG5tYXRjaFBvc2l0aW9uPW5vZGUuZGF0YS5zZWFyY2goZGVsaW1pdGVyUmVnZXgpO2lmKG1hdGNoUG9zaXRpb24hPT0tMSl7dmFyIG1hdGNoPW5vZGUuZGF0YS5tYXRjaChkZWxpbWl0ZXJSZWdleCksbWF0Y2hUZXh0PW1hdGNoWzBdLHN1Yk1hdGNoVGV4dD1tYXRjaFsxXXx8ZmFsc2U7aWYobWF0Y2hUZXh0PT09XCJcIil7bWF0Y2hQb3NpdGlvbisrO31lbHNlIGlmKHN1Yk1hdGNoVGV4dCYmc3ViTWF0Y2hUZXh0IT09bWF0Y2hUZXh0KXttYXRjaFBvc2l0aW9uKz1tYXRjaFRleHQuaW5kZXhPZihzdWJNYXRjaFRleHQpO21hdGNoVGV4dD1zdWJNYXRjaFRleHQ7fVxudmFyIG1pZGRsZUJpdD1ub2RlLnNwbGl0VGV4dChtYXRjaFBvc2l0aW9uKTttaWRkbGVCaXQuc3BsaXRUZXh0KG1hdGNoVGV4dC5sZW5ndGgpO3NraXBOb2RlQml0PTE7aWYoIW9wdHMuc2VhcmNoJiZvcHRzLmRlbGltaXRlcj09PVwic2VudGVuY2VcIil7bWlkZGxlQml0LmRhdGE9ZGVjb2RlUHVuY3R1YXRpb24obWlkZGxlQml0LmRhdGEpO31cbnZhciB3cmFwcGVkTm9kZT13cmFwTm9kZShtaWRkbGVCaXQsb3B0cyxFbGVtZW50LmJsYXN0ZWRJbmRleCk7bWlkZGxlQml0LnBhcmVudE5vZGUucmVwbGFjZUNoaWxkKHdyYXBwZWROb2RlLG1pZGRsZUJpdCk7RWxlbWVudC53cmFwcGVycy5wdXNoKHdyYXBwZWROb2RlKTtFbGVtZW50LmJsYXN0ZWRJbmRleCsrO319ZWxzZSBpZihub2RlLm5vZGVUeXBlPT09MSYmbm9kZS5oYXNDaGlsZE5vZGVzKCkmJiFSZWcuc2tpcHBlZEVsZW1lbnRzLnRlc3Qobm9kZS50YWdOYW1lKSYmIVJlZy5oYXNQbHVnaW5DbGFzcy50ZXN0KG5vZGUuY2xhc3NOYW1lKSl7Zm9yKHZhciBpPTA7aTxub2RlLmNoaWxkTm9kZXMubGVuZ3RoO2krKyl7RWxlbWVudC5ub2RlQmVnaW5uaW5nPXRydWU7aSs9dHJhdmVyc2VET00obm9kZS5jaGlsZE5vZGVzW2ldLG9wdHMpO319XG5yZXR1cm4gc2tpcE5vZGVCaXQ7fVxudmFyIG9wdHM9JC5leHRlbmQoe30sJC5mbltOQU1FXS5kZWZhdWx0cyxvcHRpb25zKSxkZWxpbWl0ZXJSZWdleCxFbGVtZW50PXt9O2lmKG9wdHMuc2VhcmNoLmxlbmd0aCYmKHR5cGVvZiBvcHRzLnNlYXJjaD09PVwic3RyaW5nXCJ8fC9eXFxkLy50ZXN0KHBhcnNlRmxvYXQob3B0cy5zZWFyY2gpKSkpe29wdHMuZGVsaW1pdGVyPW9wdHMuc2VhcmNoLnRvU3RyaW5nKCkucmVwbGFjZSgvWy1bXFxdeyx9KC4pKis/fF4kXFxcXFxcL10vZyxcIlxcXFwkJlwiKTtkZWxpbWl0ZXJSZWdleD1uZXcgUmVnRXhwKFwiKD86XnxbXi1cIitjaGFyYWN0ZXJSYW5nZXMubGF0aW5MZXR0ZXJzK1wiXSkoXCIrb3B0cy5kZWxpbWl0ZXIrXCIoJ3MpPykoPyFbLVwiK2NoYXJhY3RlclJhbmdlcy5sYXRpbkxldHRlcnMrXCJdKVwiLFwiaVwiKTt9ZWxzZXtpZih0eXBlb2Ygb3B0cy5kZWxpbWl0ZXI9PT1cInN0cmluZ1wiKXtvcHRzLmRlbGltaXRlcj1vcHRzLmRlbGltaXRlci50b0xvd2VyQ2FzZSgpO31cbnN3aXRjaChvcHRzLmRlbGltaXRlcil7Y2FzZVwiYWxsXCI6ZGVsaW1pdGVyUmVnZXg9LyguKS87YnJlYWs7Y2FzZVwibGV0dGVyXCI6Y2FzZVwiY2hhclwiOmNhc2VcImNoYXJhY3RlclwiOmRlbGltaXRlclJlZ2V4PS8oXFxTKS87YnJlYWs7Y2FzZVwid29yZFwiOmRlbGltaXRlclJlZ2V4PS9cXHMqKFxcUyspXFxzKi87YnJlYWs7Y2FzZVwic2VudGVuY2VcIjpkZWxpbWl0ZXJSZWdleD0vKD89XFxTKSgoWy5dezIsfSk/W14hP10rPyhbLuKApiE/XSt8KD89XFxzKyQpfCQpKFxccypb4oCy4oCZJ+KAneKAs+KAnFwiKcK7XSspKikvO2JyZWFrO2Nhc2VcImVsZW1lbnRcIjpkZWxpbWl0ZXJSZWdleD0vKD89XFxTKShbXFxTXFxzXSpcXFMpLzticmVhaztkZWZhdWx0OmlmKG9wdHMuZGVsaW1pdGVyIGluc3RhbmNlb2YgUmVnRXhwKXtkZWxpbWl0ZXJSZWdleD1vcHRzLmRlbGltaXRlcjt9ZWxzZXtjb25zb2xlLmxvZyhOQU1FK1wiOiBVbnJlY29nbml6ZWQgZGVsaW1pdGVyLCBlbXB0eSBzZWFyY2ggc3RyaW5nLCBvciBpbnZhbGlkIGN1c3RvbSBSZWdleC4gQWJvcnRpbmcuXCIpO3JldHVybiB0cnVlO319fVxudGhpcy5lYWNoKGZ1bmN0aW9uKCl7dmFyICR0aGlzPSQodGhpcyksdGV4dD0kdGhpcy50ZXh0KCk7aWYob3B0aW9ucyE9PWZhbHNlKXtFbGVtZW50PXtibGFzdGVkSW5kZXg6MCxub2RlQmVnaW5uaW5nOmZhbHNlLHdyYXBwZXJzOkVsZW1lbnQud3JhcHBlcnN8fFtdfTtpZigkdGhpcy5kYXRhKE5BTUUpIT09dW5kZWZpbmVkJiYoJHRoaXMuZGF0YShOQU1FKSE9PVwic2VhcmNoXCJ8fG9wdHMuc2VhcmNoPT09ZmFsc2UpKXtyZXZlcnNlKCR0aGlzLG9wdHMpO2lmKG9wdHMuZGVidWcpY29uc29sZS5sb2coTkFNRStcIjogUmVtb3ZlZCBlbGVtZW50J3MgZXhpc3RpbmcgQmxhc3QgY2FsbC5cIik7fVxuJHRoaXMuZGF0YShOQU1FLG9wdHMuc2VhcmNoIT09ZmFsc2U/XCJzZWFyY2hcIjpvcHRzLmRlbGltaXRlcik7aWYob3B0cy5hcmlhKXskdGhpcy5hdHRyKFwiYXJpYS1sYWJlbFwiLHRleHQpO31cbmlmKG9wdHMuc3RyaXBIVE1MVGFncyl7JHRoaXMuaHRtbCh0ZXh0KTt9XG50cnl7ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChvcHRzLnRhZyk7fWNhdGNoKGVycm9yKXtvcHRzLnRhZz1cInNwYW5cIjtpZihvcHRzLmRlYnVnKWNvbnNvbGUubG9nKE5BTUUrXCI6IEludmFsaWQgdGFnIHN1cHBsaWVkLiBEZWZhdWx0aW5nIHRvIHNwYW4uXCIpO31cbiR0aGlzLmFkZENsYXNzKE5BTUUrXCItcm9vdFwiKTtpZihvcHRzLmRlYnVnKWNvbnNvbGUudGltZShOQU1FKTt0cmF2ZXJzZURPTSh0aGlzLG9wdHMpO2lmKG9wdHMuZGVidWcpY29uc29sZS50aW1lRW5kKE5BTUUpO31lbHNlIGlmKG9wdGlvbnM9PT1mYWxzZSYmJHRoaXMuZGF0YShOQU1FKSE9PXVuZGVmaW5lZCl7cmV2ZXJzZSgkdGhpcyxvcHRzKTt9XG5pZihvcHRzLmRlYnVnKXskLmVhY2goRWxlbWVudC53cmFwcGVycyxmdW5jdGlvbihpbmRleCxlbGVtZW50KXtjb25zb2xlLmxvZyhOQU1FK1wiIFtcIitvcHRzLmRlbGltaXRlcitcIl0gXCIrdGhpcy5vdXRlckhUTUwpO3RoaXMuc3R5bGUuYmFja2dyb3VuZENvbG9yPWluZGV4JTI/XCIjZjEyMTg1XCI6XCIjMDc1ZDlhXCI7fSk7fX0pO2Z1bmN0aW9uIHJldmVyc2UoJHRoaXMsb3B0cyl7aWYob3B0cy5kZWJ1Zyljb25zb2xlLnRpbWUoXCJibGFzdCByZXZlcnNhbFwiKTt2YXIgc2tpcHBlZERlc2NlbmRhbnRSb290PWZhbHNlOyR0aGlzLnJlbW92ZUNsYXNzKE5BTUUrXCItcm9vdFwiKS5yZW1vdmVBdHRyKFwiYXJpYS1sYWJlbFwiKS5maW5kKFwiLlwiK05BTUUpLmVhY2goZnVuY3Rpb24oKXt2YXIgJHRoaXM9JCh0aGlzKTtpZighJHRoaXMuY2xvc2VzdChcIi5cIitOQU1FK1wiLXJvb3RcIikubGVuZ3RoKXt2YXIgdGhpc1BhcmVudE5vZGU9dGhpcy5wYXJlbnROb2RlO2lmKElFPD03KSh0aGlzUGFyZW50Tm9kZS5maXJzdENoaWxkLm5vZGVOYW1lKTt0aGlzUGFyZW50Tm9kZS5yZXBsYWNlQ2hpbGQodGhpcy5maXJzdENoaWxkLHRoaXMpO3RoaXNQYXJlbnROb2RlLm5vcm1hbGl6ZSgpO31lbHNle3NraXBwZWREZXNjZW5kYW50Um9vdD10cnVlO319KTtpZih3aW5kb3cuWmVwdG8peyR0aGlzLmRhdGEoTkFNRSx1bmRlZmluZWQpO31lbHNleyR0aGlzLnJlbW92ZURhdGEoTkFNRSk7fVxuaWYob3B0cy5kZWJ1Zyl7Y29uc29sZS5sb2coTkFNRStcIjogUmV2ZXJzZWQgQmxhc3RcIisoJHRoaXMuYXR0cihcImlkXCIpP1wiIG9uICNcIiskdGhpcy5hdHRyKFwiaWRcIikrXCIuXCI6XCIuXCIpKyhza2lwcGVkRGVzY2VuZGFudFJvb3Q/XCIgU2tpcHBlZCByZXZlcnNhbCBvbiB0aGUgY2hpbGRyZW4gb2Ygb25lIG9yIG1vcmUgZGVzY2VuZGFudCByb290IGVsZW1lbnRzLlwiOlwiXCIpKTtjb25zb2xlLnRpbWVFbmQoXCJibGFzdCByZXZlcnNhbFwiKTt9fVxuaWYob3B0aW9ucyE9PWZhbHNlJiZvcHRzLnJldHVybkdlbmVyYXRlZD09PXRydWUpe3ZhciBuZXdTdGFjaz0kKCkuYWRkKEVsZW1lbnQud3JhcHBlcnMpO25ld1N0YWNrLnByZXZPYmplY3Q9dGhpcztuZXdTdGFjay5jb250ZXh0PXRoaXMuY29udGV4dDtyZXR1cm4gbmV3U3RhY2s7fWVsc2V7cmV0dXJuIHRoaXM7fX07JC5mbi5ibGFzdC5kZWZhdWx0cz17cmV0dXJuR2VuZXJhdGVkOnRydWUsZGVsaW1pdGVyOlwid29yZFwiLHRhZzpcInNwYW5cIixzZWFyY2g6ZmFsc2UsY3VzdG9tQ2xhc3M6XCJcIixnZW5lcmF0ZUluZGV4SUQ6ZmFsc2UsZ2VuZXJhdGVWYWx1ZUNsYXNzOmZhbHNlLHN0cmlwSFRNTFRhZ3M6ZmFsc2UsYXJpYTp0cnVlLGRlYnVnOmZhbHNlfTt9KSh3aW5kb3cualF1ZXJ5fHx3aW5kb3cuWmVwdG8sd2luZG93LGRvY3VtZW50KTs7XG4vKiFcbiAqIE5hbWUgICAgOiBKdXN0IEFub3RoZXIgUGFyYWxsYXggW0phcmFsbGF4XVxuICogVmVyc2lvbiA6IDEuMTIuNVxuICogQXV0aG9yICA6IG5LIDxodHRwczovL25rZGV2LmluZm8+XG4gKiBHaXRIdWIgIDogaHR0cHM6Ly9naXRodWIuY29tL25rLW8vamFyYWxsYXhcbiAqL1xuKGZ1bmN0aW9uKG1vZHVsZXMpe3ZhciBpbnN0YWxsZWRNb2R1bGVzPXt9O2Z1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpe2lmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKXtyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0czt9XG52YXIgbW9kdWxlPWluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdPXtpOm1vZHVsZUlkLGw6ZmFsc2UsZXhwb3J0czp7fX07bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cyxtb2R1bGUsbW9kdWxlLmV4cG9ydHMsX193ZWJwYWNrX3JlcXVpcmVfXyk7bW9kdWxlLmw9dHJ1ZTtyZXR1cm4gbW9kdWxlLmV4cG9ydHM7fVxuX193ZWJwYWNrX3JlcXVpcmVfXy5tPW1vZHVsZXM7X193ZWJwYWNrX3JlcXVpcmVfXy5jPWluc3RhbGxlZE1vZHVsZXM7X193ZWJwYWNrX3JlcXVpcmVfXy5kPWZ1bmN0aW9uKGV4cG9ydHMsbmFtZSxnZXR0ZXIpe2lmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cyxuYW1lKSl7T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsbmFtZSx7ZW51bWVyYWJsZTp0cnVlLGdldDpnZXR0ZXJ9KTt9fTtfX3dlYnBhY2tfcmVxdWlyZV9fLnI9ZnVuY3Rpb24oZXhwb3J0cyl7aWYodHlwZW9mIFN5bWJvbCE9PSd1bmRlZmluZWQnJiZTeW1ib2wudG9TdHJpbmdUYWcpe09iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLFN5bWJvbC50b1N0cmluZ1RhZyx7dmFsdWU6J01vZHVsZSd9KTt9XG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywnX19lc01vZHVsZScse3ZhbHVlOnRydWV9KTt9O19fd2VicGFja19yZXF1aXJlX18udD1mdW5jdGlvbih2YWx1ZSxtb2RlKXtpZihtb2RlJjEpdmFsdWU9X193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7aWYobW9kZSY4KXJldHVybiB2YWx1ZTtpZigobW9kZSY0KSYmdHlwZW9mIHZhbHVlPT09J29iamVjdCcmJnZhbHVlJiZ2YWx1ZS5fX2VzTW9kdWxlKXJldHVybiB2YWx1ZTt2YXIgbnM9T2JqZWN0LmNyZWF0ZShudWxsKTtfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO09iamVjdC5kZWZpbmVQcm9wZXJ0eShucywnZGVmYXVsdCcse2VudW1lcmFibGU6dHJ1ZSx2YWx1ZTp2YWx1ZX0pO2lmKG1vZGUmMiYmdHlwZW9mIHZhbHVlIT0nc3RyaW5nJylmb3IodmFyIGtleSBpbiB2YWx1ZSlfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsa2V5LGZ1bmN0aW9uKGtleSl7cmV0dXJuIHZhbHVlW2tleV07fS5iaW5kKG51bGwsa2V5KSk7cmV0dXJuIG5zO307X193ZWJwYWNrX3JlcXVpcmVfXy5uPWZ1bmN0aW9uKG1vZHVsZSl7dmFyIGdldHRlcj1tb2R1bGUmJm1vZHVsZS5fX2VzTW9kdWxlP2Z1bmN0aW9uIGdldERlZmF1bHQoKXtyZXR1cm4gbW9kdWxlWydkZWZhdWx0J107fTpmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCl7cmV0dXJuIG1vZHVsZTt9O19fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsJ2EnLGdldHRlcik7cmV0dXJuIGdldHRlcjt9O19fd2VicGFja19yZXF1aXJlX18ubz1mdW5jdGlvbihvYmplY3QscHJvcGVydHkpe3JldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LHByb3BlcnR5KTt9O19fd2VicGFja19yZXF1aXJlX18ucD1cIlwiO3JldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucz0xMCk7fSlcbihbLCwoZnVuY3Rpb24obW9kdWxlLGV4cG9ydHMpe21vZHVsZS5leHBvcnRzPWZ1bmN0aW9uKGNhbGxiYWNrKXtpZihkb2N1bWVudC5yZWFkeVN0YXRlPT09J2NvbXBsZXRlJ3x8ZG9jdW1lbnQucmVhZHlTdGF0ZT09PSdpbnRlcmFjdGl2ZScpe2NhbGxiYWNrLmNhbGwoKTt9ZWxzZSBpZihkb2N1bWVudC5hdHRhY2hFdmVudCl7ZG9jdW1lbnQuYXR0YWNoRXZlbnQoJ29ucmVhZHlzdGF0ZWNoYW5nZScsZnVuY3Rpb24oKXtpZihkb2N1bWVudC5yZWFkeVN0YXRlPT09J2ludGVyYWN0aXZlJyljYWxsYmFjay5jYWxsKCk7fSk7fWVsc2UgaWYoZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcil7ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsY2FsbGJhY2spO319O30pLChmdW5jdGlvbihtb2R1bGUsZXhwb3J0cyxfX3dlYnBhY2tfcmVxdWlyZV9fKXsoZnVuY3Rpb24oZ2xvYmFsKXt2YXIgd2luO2lmKHR5cGVvZiB3aW5kb3chPT1cInVuZGVmaW5lZFwiKXt3aW49d2luZG93O31lbHNlIGlmKHR5cGVvZiBnbG9iYWwhPT1cInVuZGVmaW5lZFwiKXt3aW49Z2xvYmFsO31lbHNlIGlmKHR5cGVvZiBzZWxmIT09XCJ1bmRlZmluZWRcIil7d2luPXNlbGY7fWVsc2V7d2luPXt9O31cbm1vZHVsZS5leHBvcnRzPXdpbjt9LmNhbGwodGhpcyxfX3dlYnBhY2tfcmVxdWlyZV9fKDQpKSl9KSwoZnVuY3Rpb24obW9kdWxlLGV4cG9ydHMpe2Z1bmN0aW9uIF90eXBlb2Yob2JqKXtcIkBiYWJlbC9oZWxwZXJzIC0gdHlwZW9mXCI7aWYodHlwZW9mIFN5bWJvbD09PVwiZnVuY3Rpb25cIiYmdHlwZW9mIFN5bWJvbC5pdGVyYXRvcj09PVwic3ltYm9sXCIpe190eXBlb2Y9ZnVuY3Rpb24gX3R5cGVvZihvYmope3JldHVybiB0eXBlb2Ygb2JqO307fWVsc2V7X3R5cGVvZj1mdW5jdGlvbiBfdHlwZW9mKG9iail7cmV0dXJuIG9iaiYmdHlwZW9mIFN5bWJvbD09PVwiZnVuY3Rpb25cIiYmb2JqLmNvbnN0cnVjdG9yPT09U3ltYm9sJiZvYmohPT1TeW1ib2wucHJvdG90eXBlP1wic3ltYm9sXCI6dHlwZW9mIG9iajt9O31yZXR1cm4gX3R5cGVvZihvYmopO31cbnZhciBnO2c9ZnVuY3Rpb24oKXtyZXR1cm4gdGhpczt9KCk7dHJ5e2c9Z3x8bmV3IEZ1bmN0aW9uKFwicmV0dXJuIHRoaXNcIikoKTt9Y2F0Y2goZSl7aWYoKHR5cGVvZiB3aW5kb3c9PT1cInVuZGVmaW5lZFwiP1widW5kZWZpbmVkXCI6X3R5cGVvZih3aW5kb3cpKT09PVwib2JqZWN0XCIpZz13aW5kb3c7fVxubW9kdWxlLmV4cG9ydHM9Zzt9KSwsLCwsLChmdW5jdGlvbihtb2R1bGUsZXhwb3J0cyxfX3dlYnBhY2tfcmVxdWlyZV9fKXttb2R1bGUuZXhwb3J0cz1fX3dlYnBhY2tfcmVxdWlyZV9fKDExKTt9KSwoZnVuY3Rpb24obW9kdWxlLF9fd2VicGFja19leHBvcnRzX18sX193ZWJwYWNrX3JlcXVpcmVfXyl7XCJ1c2Ugc3RyaWN0XCI7X193ZWJwYWNrX3JlcXVpcmVfXy5yKF9fd2VicGFja19leHBvcnRzX18pO3ZhciBsaXRlX3JlYWR5X19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8wX189X193ZWJwYWNrX3JlcXVpcmVfXygyKTt2YXIgbGl0ZV9yZWFkeV9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMF9fX2RlZmF1bHQ9X193ZWJwYWNrX3JlcXVpcmVfXy5uKGxpdGVfcmVhZHlfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzBfXyk7dmFyIGdsb2JhbF9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMV9fPV9fd2VicGFja19yZXF1aXJlX18oMyk7dmFyIGdsb2JhbF9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMV9fX2RlZmF1bHQ9X193ZWJwYWNrX3JlcXVpcmVfXy5uKGdsb2JhbF9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMV9fKTt2YXIgX2phcmFsbGF4X2VzbV9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMl9fPV9fd2VicGFja19yZXF1aXJlX18oMTIpO2Z1bmN0aW9uIF90eXBlb2Yob2JqKXtcIkBiYWJlbC9oZWxwZXJzIC0gdHlwZW9mXCI7aWYodHlwZW9mIFN5bWJvbD09PVwiZnVuY3Rpb25cIiYmdHlwZW9mIFN5bWJvbC5pdGVyYXRvcj09PVwic3ltYm9sXCIpe190eXBlb2Y9ZnVuY3Rpb24gX3R5cGVvZihvYmope3JldHVybiB0eXBlb2Ygb2JqO307fWVsc2V7X3R5cGVvZj1mdW5jdGlvbiBfdHlwZW9mKG9iail7cmV0dXJuIG9iaiYmdHlwZW9mIFN5bWJvbD09PVwiZnVuY3Rpb25cIiYmb2JqLmNvbnN0cnVjdG9yPT09U3ltYm9sJiZvYmohPT1TeW1ib2wucHJvdG90eXBlP1wic3ltYm9sXCI6dHlwZW9mIG9iajt9O31yZXR1cm4gX3R5cGVvZihvYmopO31cbnZhciBvbGRQbHVnaW49Z2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19bXCJ3aW5kb3dcIl0uamFyYWxsYXg7Z2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19bXCJ3aW5kb3dcIl0uamFyYWxsYXg9X2phcmFsbGF4X2VzbV9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMl9fW1wiZGVmYXVsdFwiXTtnbG9iYWxfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzFfX1tcIndpbmRvd1wiXS5qYXJhbGxheC5ub0NvbmZsaWN0PWZ1bmN0aW9uKCl7Z2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19bXCJ3aW5kb3dcIl0uamFyYWxsYXg9b2xkUGx1Z2luO3JldHVybiB0aGlzO307aWYoJ3VuZGVmaW5lZCchPT10eXBlb2YgZ2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19bXCJqUXVlcnlcIl0pe3ZhciBqUXVlcnlQbHVnaW49ZnVuY3Rpb24galF1ZXJ5UGx1Z2luKCl7Zm9yKHZhciBfbGVuPWFyZ3VtZW50cy5sZW5ndGgsYXJncz1uZXcgQXJyYXkoX2xlbiksX2tleT0wO19rZXk8X2xlbjtfa2V5Kyspe2FyZ3NbX2tleV09YXJndW1lbnRzW19rZXldO31cbkFycmF5LnByb3RvdHlwZS51bnNoaWZ0LmNhbGwoYXJncyx0aGlzKTt2YXIgcmVzPV9qYXJhbGxheF9lc21fX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzJfX1tcImRlZmF1bHRcIl0uYXBwbHkoZ2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19bXCJ3aW5kb3dcIl0sYXJncyk7cmV0dXJuJ29iamVjdCchPT1fdHlwZW9mKHJlcyk/cmVzOnRoaXM7fTtqUXVlcnlQbHVnaW4uY29uc3RydWN0b3I9X2phcmFsbGF4X2VzbV9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMl9fW1wiZGVmYXVsdFwiXS5jb25zdHJ1Y3Rvcjt2YXIgb2xkSnFQbHVnaW49Z2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19bXCJqUXVlcnlcIl0uZm4uamFyYWxsYXg7Z2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19bXCJqUXVlcnlcIl0uZm4uamFyYWxsYXg9alF1ZXJ5UGx1Z2luO2dsb2JhbF9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMV9fW1wialF1ZXJ5XCJdLmZuLmphcmFsbGF4Lm5vQ29uZmxpY3Q9ZnVuY3Rpb24oKXtnbG9iYWxfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzFfX1tcImpRdWVyeVwiXS5mbi5qYXJhbGxheD1vbGRKcVBsdWdpbjtyZXR1cm4gdGhpczt9O31cbmxpdGVfcmVhZHlfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzBfX19kZWZhdWx0KCkoZnVuY3Rpb24oKXtPYmplY3QoX2phcmFsbGF4X2VzbV9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMl9fW1wiZGVmYXVsdFwiXSkoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtamFyYWxsYXhdJykpO30pO30pLChmdW5jdGlvbihtb2R1bGUsX193ZWJwYWNrX2V4cG9ydHNfXyxfX3dlYnBhY2tfcmVxdWlyZV9fKXtcInVzZSBzdHJpY3RcIjtfX3dlYnBhY2tfcmVxdWlyZV9fLnIoX193ZWJwYWNrX2V4cG9ydHNfXyk7dmFyIGxpdGVfcmVhZHlfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzBfXz1fX3dlYnBhY2tfcmVxdWlyZV9fKDIpO3ZhciBsaXRlX3JlYWR5X19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8wX19fZGVmYXVsdD1fX3dlYnBhY2tfcmVxdWlyZV9fLm4obGl0ZV9yZWFkeV9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMF9fKTt2YXIgZ2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX189X193ZWJwYWNrX3JlcXVpcmVfXygzKTt2YXIgZ2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19fZGVmYXVsdD1fX3dlYnBhY2tfcmVxdWlyZV9fLm4oZ2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX18pO2Z1bmN0aW9uIF9zbGljZWRUb0FycmF5KGFycixpKXtyZXR1cm4gX2FycmF5V2l0aEhvbGVzKGFycil8fF9pdGVyYWJsZVRvQXJyYXlMaW1pdChhcnIsaSl8fF91bnN1cHBvcnRlZEl0ZXJhYmxlVG9BcnJheShhcnIsaSl8fF9ub25JdGVyYWJsZVJlc3QoKTt9XG5mdW5jdGlvbiBfbm9uSXRlcmFibGVSZXN0KCl7dGhyb3cgbmV3IFR5cGVFcnJvcihcIkludmFsaWQgYXR0ZW1wdCB0byBkZXN0cnVjdHVyZSBub24taXRlcmFibGUgaW5zdGFuY2UuXFxuSW4gb3JkZXIgdG8gYmUgaXRlcmFibGUsIG5vbi1hcnJheSBvYmplY3RzIG11c3QgaGF2ZSBhIFtTeW1ib2wuaXRlcmF0b3JdKCkgbWV0aG9kLlwiKTt9XG5mdW5jdGlvbiBfdW5zdXBwb3J0ZWRJdGVyYWJsZVRvQXJyYXkobyxtaW5MZW4pe2lmKCFvKXJldHVybjtpZih0eXBlb2Ygbz09PVwic3RyaW5nXCIpcmV0dXJuIF9hcnJheUxpa2VUb0FycmF5KG8sbWluTGVuKTt2YXIgbj1PYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobykuc2xpY2UoOCwtMSk7aWYobj09PVwiT2JqZWN0XCImJm8uY29uc3RydWN0b3Ipbj1vLmNvbnN0cnVjdG9yLm5hbWU7aWYobj09PVwiTWFwXCJ8fG49PT1cIlNldFwiKXJldHVybiBBcnJheS5mcm9tKG8pO2lmKG49PT1cIkFyZ3VtZW50c1wifHwvXig/OlVpfEkpbnQoPzo4fDE2fDMyKSg/OkNsYW1wZWQpP0FycmF5JC8udGVzdChuKSlyZXR1cm4gX2FycmF5TGlrZVRvQXJyYXkobyxtaW5MZW4pO31cbmZ1bmN0aW9uIF9hcnJheUxpa2VUb0FycmF5KGFycixsZW4pe2lmKGxlbj09bnVsbHx8bGVuPmFyci5sZW5ndGgpbGVuPWFyci5sZW5ndGg7Zm9yKHZhciBpPTAsYXJyMj1uZXcgQXJyYXkobGVuKTtpPGxlbjtpKyspe2FycjJbaV09YXJyW2ldO31yZXR1cm4gYXJyMjt9XG5mdW5jdGlvbiBfaXRlcmFibGVUb0FycmF5TGltaXQoYXJyLGkpe2lmKHR5cGVvZiBTeW1ib2w9PT1cInVuZGVmaW5lZFwifHwhKFN5bWJvbC5pdGVyYXRvciBpbiBPYmplY3QoYXJyKSkpcmV0dXJuO3ZhciBfYXJyPVtdO3ZhciBfbj10cnVlO3ZhciBfZD1mYWxzZTt2YXIgX2U9dW5kZWZpbmVkO3RyeXtmb3IodmFyIF9pPWFycltTeW1ib2wuaXRlcmF0b3JdKCksX3M7IShfbj0oX3M9X2kubmV4dCgpKS5kb25lKTtfbj10cnVlKXtfYXJyLnB1c2goX3MudmFsdWUpO2lmKGkmJl9hcnIubGVuZ3RoPT09aSlicmVhazt9fWNhdGNoKGVycil7X2Q9dHJ1ZTtfZT1lcnI7fWZpbmFsbHl7dHJ5e2lmKCFfbiYmX2lbXCJyZXR1cm5cIl0hPW51bGwpX2lbXCJyZXR1cm5cIl0oKTt9ZmluYWxseXtpZihfZCl0aHJvdyBfZTt9fXJldHVybiBfYXJyO31cbmZ1bmN0aW9uIF9hcnJheVdpdGhIb2xlcyhhcnIpe2lmKEFycmF5LmlzQXJyYXkoYXJyKSlyZXR1cm4gYXJyO31cbmZ1bmN0aW9uIF90eXBlb2Yob2JqKXtcIkBiYWJlbC9oZWxwZXJzIC0gdHlwZW9mXCI7aWYodHlwZW9mIFN5bWJvbD09PVwiZnVuY3Rpb25cIiYmdHlwZW9mIFN5bWJvbC5pdGVyYXRvcj09PVwic3ltYm9sXCIpe190eXBlb2Y9ZnVuY3Rpb24gX3R5cGVvZihvYmope3JldHVybiB0eXBlb2Ygb2JqO307fWVsc2V7X3R5cGVvZj1mdW5jdGlvbiBfdHlwZW9mKG9iail7cmV0dXJuIG9iaiYmdHlwZW9mIFN5bWJvbD09PVwiZnVuY3Rpb25cIiYmb2JqLmNvbnN0cnVjdG9yPT09U3ltYm9sJiZvYmohPT1TeW1ib2wucHJvdG90eXBlP1wic3ltYm9sXCI6dHlwZW9mIG9iajt9O31yZXR1cm4gX3R5cGVvZihvYmopO31cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSxDb25zdHJ1Y3Rvcil7aWYoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSl7dGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTt9fVxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnRpZXModGFyZ2V0LHByb3BzKXtmb3IodmFyIGk9MDtpPHByb3BzLmxlbmd0aDtpKyspe3ZhciBkZXNjcmlwdG9yPXByb3BzW2ldO2Rlc2NyaXB0b3IuZW51bWVyYWJsZT1kZXNjcmlwdG9yLmVudW1lcmFibGV8fGZhbHNlO2Rlc2NyaXB0b3IuY29uZmlndXJhYmxlPXRydWU7aWYoXCJ2YWx1ZVwiaW4gZGVzY3JpcHRvcilkZXNjcmlwdG9yLndyaXRhYmxlPXRydWU7T2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCxkZXNjcmlwdG9yLmtleSxkZXNjcmlwdG9yKTt9fVxuZnVuY3Rpb24gX2NyZWF0ZUNsYXNzKENvbnN0cnVjdG9yLHByb3RvUHJvcHMsc3RhdGljUHJvcHMpe2lmKHByb3RvUHJvcHMpX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLHByb3RvUHJvcHMpO2lmKHN0YXRpY1Byb3BzKV9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLHN0YXRpY1Byb3BzKTtyZXR1cm4gQ29uc3RydWN0b3I7fVxudmFyIG5hdmlnYXRvcj1nbG9iYWxfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzFfX1tcIndpbmRvd1wiXS5uYXZpZ2F0b3I7dmFyIGlzSUU9LTE8bmF2aWdhdG9yLnVzZXJBZ2VudC5pbmRleE9mKCdNU0lFICcpfHwtMTxuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoJ1RyaWRlbnQvJyl8fC0xPG5hdmlnYXRvci51c2VyQWdlbnQuaW5kZXhPZignRWRnZS8nKTt2YXIgaXNNb2JpbGU9L0FuZHJvaWR8d2ViT1N8aVBob25lfGlQYWR8aVBvZHxCbGFja0JlcnJ5fElFTW9iaWxlfE9wZXJhIE1pbmkvaS50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpO3ZhciBzdXBwb3J0VHJhbnNmb3JtPWZ1bmN0aW9uKCl7dmFyIHByZWZpeGVzPSd0cmFuc2Zvcm0gV2Via2l0VHJhbnNmb3JtIE1velRyYW5zZm9ybScuc3BsaXQoJyAnKTt2YXIgZGl2PWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO2Zvcih2YXIgaT0wO2k8cHJlZml4ZXMubGVuZ3RoO2krPTEpe2lmKGRpdiYmZGl2LnN0eWxlW3ByZWZpeGVzW2ldXSE9PXVuZGVmaW5lZCl7cmV0dXJuIHByZWZpeGVzW2ldO319XG5yZXR1cm4gZmFsc2U7fSgpO3ZhciAkZGV2aWNlSGVscGVyO2Z1bmN0aW9uIGdldERldmljZUhlaWdodCgpe2lmKCEkZGV2aWNlSGVscGVyJiZkb2N1bWVudC5ib2R5KXskZGV2aWNlSGVscGVyPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpOyRkZXZpY2VIZWxwZXIuc3R5bGUuY3NzVGV4dD0ncG9zaXRpb246IGZpeGVkOyB0b3A6IC05OTk5cHg7IGxlZnQ6IDA7IGhlaWdodDogMTAwdmg7IHdpZHRoOiAwOyc7ZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCgkZGV2aWNlSGVscGVyKTt9XG5yZXR1cm4oJGRldmljZUhlbHBlcj8kZGV2aWNlSGVscGVyLmNsaWVudEhlaWdodDowKXx8Z2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19bXCJ3aW5kb3dcIl0uaW5uZXJIZWlnaHR8fGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRIZWlnaHQ7fVxudmFyIHduZEg7ZnVuY3Rpb24gdXBkYXRlV25kVmFycygpe2lmKGlzTW9iaWxlKXt3bmRIPWdldERldmljZUhlaWdodCgpO31lbHNle3duZEg9Z2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19bXCJ3aW5kb3dcIl0uaW5uZXJIZWlnaHR8fGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRIZWlnaHQ7fX1cbnVwZGF0ZVduZFZhcnMoKTtnbG9iYWxfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzFfX1tcIndpbmRvd1wiXS5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLHVwZGF0ZVduZFZhcnMpO2dsb2JhbF9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMV9fW1wid2luZG93XCJdLmFkZEV2ZW50TGlzdGVuZXIoJ29yaWVudGF0aW9uY2hhbmdlJyx1cGRhdGVXbmRWYXJzKTtnbG9iYWxfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzFfX1tcIndpbmRvd1wiXS5hZGRFdmVudExpc3RlbmVyKCdsb2FkJyx1cGRhdGVXbmRWYXJzKTtsaXRlX3JlYWR5X19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8wX19fZGVmYXVsdCgpKGZ1bmN0aW9uKCl7dXBkYXRlV25kVmFycyh7dHlwZTonZG9tLWxvYWRlZCd9KTt9KTt2YXIgamFyYWxsYXhMaXN0PVtdO2Z1bmN0aW9uIGdldFBhcmVudHMoZWxlbSl7dmFyIHBhcmVudHM9W107d2hpbGUobnVsbCE9PWVsZW0ucGFyZW50RWxlbWVudCl7ZWxlbT1lbGVtLnBhcmVudEVsZW1lbnQ7aWYoMT09PWVsZW0ubm9kZVR5cGUpe3BhcmVudHMucHVzaChlbGVtKTt9fVxucmV0dXJuIHBhcmVudHM7fVxuZnVuY3Rpb24gdXBkYXRlUGFyYWxsYXgoKXtpZighamFyYWxsYXhMaXN0Lmxlbmd0aCl7cmV0dXJuO31cbmphcmFsbGF4TGlzdC5mb3JFYWNoKGZ1bmN0aW9uKGRhdGEsayl7dmFyIGluc3RhbmNlPWRhdGEuaW5zdGFuY2Usb2xkRGF0YT1kYXRhLm9sZERhdGE7dmFyIGNsaWVudFJlY3Q9aW5zdGFuY2UuJGl0ZW0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7dmFyIG5ld0RhdGE9e3dpZHRoOmNsaWVudFJlY3Qud2lkdGgsaGVpZ2h0OmNsaWVudFJlY3QuaGVpZ2h0LHRvcDpjbGllbnRSZWN0LnRvcCxib3R0b206Y2xpZW50UmVjdC5ib3R0b20sd25kVzpnbG9iYWxfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzFfX1tcIndpbmRvd1wiXS5pbm5lcldpZHRoLHduZEg6d25kSH07dmFyIGlzUmVzaXplZD0hb2xkRGF0YXx8b2xkRGF0YS53bmRXIT09bmV3RGF0YS53bmRXfHxvbGREYXRhLnduZEghPT1uZXdEYXRhLnduZEh8fG9sZERhdGEud2lkdGghPT1uZXdEYXRhLndpZHRofHxvbGREYXRhLmhlaWdodCE9PW5ld0RhdGEuaGVpZ2h0O3ZhciBpc1Njcm9sbGVkPWlzUmVzaXplZHx8IW9sZERhdGF8fG9sZERhdGEudG9wIT09bmV3RGF0YS50b3B8fG9sZERhdGEuYm90dG9tIT09bmV3RGF0YS5ib3R0b207amFyYWxsYXhMaXN0W2tdLm9sZERhdGE9bmV3RGF0YTtpZihpc1Jlc2l6ZWQpe2luc3RhbmNlLm9uUmVzaXplKCk7fVxuaWYoaXNTY3JvbGxlZCl7aW5zdGFuY2Uub25TY3JvbGwoKTt9fSk7Z2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19bXCJ3aW5kb3dcIl0ucmVxdWVzdEFuaW1hdGlvbkZyYW1lKHVwZGF0ZVBhcmFsbGF4KTt9XG52YXIgaW5zdGFuY2VJRD0wO3ZhciBKYXJhbGxheD1mdW5jdGlvbigpe2Z1bmN0aW9uIEphcmFsbGF4KGl0ZW0sdXNlck9wdGlvbnMpe19jbGFzc0NhbGxDaGVjayh0aGlzLEphcmFsbGF4KTt2YXIgc2VsZj10aGlzO3NlbGYuaW5zdGFuY2VJRD1pbnN0YW5jZUlEO2luc3RhbmNlSUQrPTE7c2VsZi4kaXRlbT1pdGVtO3NlbGYuZGVmYXVsdHM9e3R5cGU6J3Njcm9sbCcsc3BlZWQ6MC41LGltZ1NyYzpudWxsLGltZ0VsZW1lbnQ6Jy5qYXJhbGxheC1pbWcnLGltZ1NpemU6J2NvdmVyJyxpbWdQb3NpdGlvbjonNTAlIDUwJScsaW1nUmVwZWF0Oiduby1yZXBlYXQnLGtlZXBJbWc6ZmFsc2UsZWxlbWVudEluVmlld3BvcnQ6bnVsbCx6SW5kZXg6LTEwMCxkaXNhYmxlUGFyYWxsYXg6ZmFsc2UsZGlzYWJsZVZpZGVvOmZhbHNlLHZpZGVvU3JjOm51bGwsdmlkZW9TdGFydFRpbWU6MCx2aWRlb0VuZFRpbWU6MCx2aWRlb1ZvbHVtZTowLHZpZGVvTG9vcDp0cnVlLHZpZGVvUGxheU9ubHlWaXNpYmxlOnRydWUsdmlkZW9MYXp5TG9hZGluZzp0cnVlLG9uU2Nyb2xsOm51bGwsb25Jbml0Om51bGwsb25EZXN0cm95Om51bGwsb25Db3ZlckltYWdlOm51bGx9O3ZhciBkYXRhT3B0aW9ucz1zZWxmLiRpdGVtLmRhdGFzZXR8fHt9O3ZhciBwdXJlRGF0YU9wdGlvbnM9e307T2JqZWN0LmtleXMoZGF0YU9wdGlvbnMpLmZvckVhY2goZnVuY3Rpb24oa2V5KXt2YXIgbG93ZUNhc2VPcHRpb249a2V5LnN1YnN0cigwLDEpLnRvTG93ZXJDYXNlKCkra2V5LnN1YnN0cigxKTtpZihsb3dlQ2FzZU9wdGlvbiYmJ3VuZGVmaW5lZCchPT10eXBlb2Ygc2VsZi5kZWZhdWx0c1tsb3dlQ2FzZU9wdGlvbl0pe3B1cmVEYXRhT3B0aW9uc1tsb3dlQ2FzZU9wdGlvbl09ZGF0YU9wdGlvbnNba2V5XTt9fSk7c2VsZi5vcHRpb25zPXNlbGYuZXh0ZW5kKHt9LHNlbGYuZGVmYXVsdHMscHVyZURhdGFPcHRpb25zLHVzZXJPcHRpb25zKTtzZWxmLnB1cmVPcHRpb25zPXNlbGYuZXh0ZW5kKHt9LHNlbGYub3B0aW9ucyk7T2JqZWN0LmtleXMoc2VsZi5vcHRpb25zKS5mb3JFYWNoKGZ1bmN0aW9uKGtleSl7aWYoJ3RydWUnPT09c2VsZi5vcHRpb25zW2tleV0pe3NlbGYub3B0aW9uc1trZXldPXRydWU7fWVsc2UgaWYoJ2ZhbHNlJz09PXNlbGYub3B0aW9uc1trZXldKXtzZWxmLm9wdGlvbnNba2V5XT1mYWxzZTt9fSk7c2VsZi5vcHRpb25zLnNwZWVkPU1hdGgubWluKDIsTWF0aC5tYXgoLTEscGFyc2VGbG9hdChzZWxmLm9wdGlvbnMuc3BlZWQpKSk7aWYoJ3N0cmluZyc9PT10eXBlb2Ygc2VsZi5vcHRpb25zLmRpc2FibGVQYXJhbGxheCl7c2VsZi5vcHRpb25zLmRpc2FibGVQYXJhbGxheD1uZXcgUmVnRXhwKHNlbGYub3B0aW9ucy5kaXNhYmxlUGFyYWxsYXgpO31cbmlmKHNlbGYub3B0aW9ucy5kaXNhYmxlUGFyYWxsYXggaW5zdGFuY2VvZiBSZWdFeHApe3ZhciBkaXNhYmxlUGFyYWxsYXhSZWdleHA9c2VsZi5vcHRpb25zLmRpc2FibGVQYXJhbGxheDtzZWxmLm9wdGlvbnMuZGlzYWJsZVBhcmFsbGF4PWZ1bmN0aW9uKCl7cmV0dXJuIGRpc2FibGVQYXJhbGxheFJlZ2V4cC50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpO307fVxuaWYoJ2Z1bmN0aW9uJyE9PXR5cGVvZiBzZWxmLm9wdGlvbnMuZGlzYWJsZVBhcmFsbGF4KXtzZWxmLm9wdGlvbnMuZGlzYWJsZVBhcmFsbGF4PWZ1bmN0aW9uKCl7cmV0dXJuIGZhbHNlO307fVxuaWYoJ3N0cmluZyc9PT10eXBlb2Ygc2VsZi5vcHRpb25zLmRpc2FibGVWaWRlbyl7c2VsZi5vcHRpb25zLmRpc2FibGVWaWRlbz1uZXcgUmVnRXhwKHNlbGYub3B0aW9ucy5kaXNhYmxlVmlkZW8pO31cbmlmKHNlbGYub3B0aW9ucy5kaXNhYmxlVmlkZW8gaW5zdGFuY2VvZiBSZWdFeHApe3ZhciBkaXNhYmxlVmlkZW9SZWdleHA9c2VsZi5vcHRpb25zLmRpc2FibGVWaWRlbztzZWxmLm9wdGlvbnMuZGlzYWJsZVZpZGVvPWZ1bmN0aW9uKCl7cmV0dXJuIGRpc2FibGVWaWRlb1JlZ2V4cC50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpO307fVxuaWYoJ2Z1bmN0aW9uJyE9PXR5cGVvZiBzZWxmLm9wdGlvbnMuZGlzYWJsZVZpZGVvKXtzZWxmLm9wdGlvbnMuZGlzYWJsZVZpZGVvPWZ1bmN0aW9uKCl7cmV0dXJuIGZhbHNlO307fVxudmFyIGVsZW1lbnRJblZQPXNlbGYub3B0aW9ucy5lbGVtZW50SW5WaWV3cG9ydDtpZihlbGVtZW50SW5WUCYmJ29iamVjdCc9PT1fdHlwZW9mKGVsZW1lbnRJblZQKSYmJ3VuZGVmaW5lZCchPT10eXBlb2YgZWxlbWVudEluVlAubGVuZ3RoKXt2YXIgX2VsZW1lbnRJblZQPWVsZW1lbnRJblZQO3ZhciBfZWxlbWVudEluVlAyPV9zbGljZWRUb0FycmF5KF9lbGVtZW50SW5WUCwxKTtlbGVtZW50SW5WUD1fZWxlbWVudEluVlAyWzBdO31cbmlmKCEoZWxlbWVudEluVlAgaW5zdGFuY2VvZiBFbGVtZW50KSl7ZWxlbWVudEluVlA9bnVsbDt9XG5zZWxmLm9wdGlvbnMuZWxlbWVudEluVmlld3BvcnQ9ZWxlbWVudEluVlA7c2VsZi5pbWFnZT17c3JjOnNlbGYub3B0aW9ucy5pbWdTcmN8fG51bGwsJGNvbnRhaW5lcjpudWxsLHVzZUltZ1RhZzpmYWxzZSxwb3NpdGlvbjovaVBhZHxpUGhvbmV8aVBvZHxBbmRyb2lkLy50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpPydhYnNvbHV0ZSc6J2ZpeGVkJ307aWYoc2VsZi5pbml0SW1nKCkmJnNlbGYuY2FuSW5pdFBhcmFsbGF4KCkpe3NlbGYuaW5pdCgpO319XG5fY3JlYXRlQ2xhc3MoSmFyYWxsYXgsW3trZXk6XCJjc3NcIix2YWx1ZTpmdW5jdGlvbiBjc3MoZWwsc3R5bGVzKXtpZignc3RyaW5nJz09PXR5cGVvZiBzdHlsZXMpe3JldHVybiBnbG9iYWxfX1dFQlBBQ0tfSU1QT1JURURfTU9EVUxFXzFfX1tcIndpbmRvd1wiXS5nZXRDb21wdXRlZFN0eWxlKGVsKS5nZXRQcm9wZXJ0eVZhbHVlKHN0eWxlcyk7fVxuaWYoc3R5bGVzLnRyYW5zZm9ybSYmc3VwcG9ydFRyYW5zZm9ybSl7c3R5bGVzW3N1cHBvcnRUcmFuc2Zvcm1dPXN0eWxlcy50cmFuc2Zvcm07fVxuT2JqZWN0LmtleXMoc3R5bGVzKS5mb3JFYWNoKGZ1bmN0aW9uKGtleSl7ZWwuc3R5bGVba2V5XT1zdHlsZXNba2V5XTt9KTtyZXR1cm4gZWw7fX0se2tleTpcImV4dGVuZFwiLHZhbHVlOmZ1bmN0aW9uIGV4dGVuZChvdXQpe2Zvcih2YXIgX2xlbj1hcmd1bWVudHMubGVuZ3RoLGFyZ3M9bmV3IEFycmF5KF9sZW4+MT9fbGVuLTE6MCksX2tleT0xO19rZXk8X2xlbjtfa2V5Kyspe2FyZ3NbX2tleS0xXT1hcmd1bWVudHNbX2tleV07fVxub3V0PW91dHx8e307T2JqZWN0LmtleXMoYXJncykuZm9yRWFjaChmdW5jdGlvbihpKXtpZighYXJnc1tpXSl7cmV0dXJuO31cbk9iamVjdC5rZXlzKGFyZ3NbaV0pLmZvckVhY2goZnVuY3Rpb24oa2V5KXtvdXRba2V5XT1hcmdzW2ldW2tleV07fSk7fSk7cmV0dXJuIG91dDt9fSx7a2V5OlwiZ2V0V2luZG93RGF0YVwiLHZhbHVlOmZ1bmN0aW9uIGdldFdpbmRvd0RhdGEoKXtyZXR1cm57d2lkdGg6Z2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19bXCJ3aW5kb3dcIl0uaW5uZXJXaWR0aHx8ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudFdpZHRoLGhlaWdodDp3bmRILHk6ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcH07fX0se2tleTpcImluaXRJbWdcIix2YWx1ZTpmdW5jdGlvbiBpbml0SW1nKCl7dmFyIHNlbGY9dGhpczt2YXIgJGltZ0VsZW1lbnQ9c2VsZi5vcHRpb25zLmltZ0VsZW1lbnQ7aWYoJGltZ0VsZW1lbnQmJidzdHJpbmcnPT09dHlwZW9mICRpbWdFbGVtZW50KXskaW1nRWxlbWVudD1zZWxmLiRpdGVtLnF1ZXJ5U2VsZWN0b3IoJGltZ0VsZW1lbnQpO31cbmlmKCEoJGltZ0VsZW1lbnQgaW5zdGFuY2VvZiBFbGVtZW50KSl7aWYoc2VsZi5vcHRpb25zLmltZ1NyYyl7JGltZ0VsZW1lbnQ9bmV3IEltYWdlKCk7JGltZ0VsZW1lbnQuc3JjPXNlbGYub3B0aW9ucy5pbWdTcmM7fWVsc2V7JGltZ0VsZW1lbnQ9bnVsbDt9fVxuaWYoJGltZ0VsZW1lbnQpe2lmKHNlbGYub3B0aW9ucy5rZWVwSW1nKXtzZWxmLmltYWdlLiRpdGVtPSRpbWdFbGVtZW50LmNsb25lTm9kZSh0cnVlKTt9ZWxzZXtzZWxmLmltYWdlLiRpdGVtPSRpbWdFbGVtZW50O3NlbGYuaW1hZ2UuJGl0ZW1QYXJlbnQ9JGltZ0VsZW1lbnQucGFyZW50Tm9kZTt9XG5zZWxmLmltYWdlLnVzZUltZ1RhZz10cnVlO31cbmlmKHNlbGYuaW1hZ2UuJGl0ZW0pe3JldHVybiB0cnVlO31cbmlmKG51bGw9PT1zZWxmLmltYWdlLnNyYyl7c2VsZi5pbWFnZS5zcmM9J2RhdGE6aW1hZ2UvZ2lmO2Jhc2U2NCxSMGxHT0RsaEFRQUJBSUFBQUFBQUFQLy8veUg1QkFFQUFBQUFMQUFBQUFBQkFBRUFBQUlCUkFBNyc7c2VsZi5pbWFnZS5iZ0ltYWdlPXNlbGYuY3NzKHNlbGYuJGl0ZW0sJ2JhY2tncm91bmQtaW1hZ2UnKTt9XG5yZXR1cm4hKCFzZWxmLmltYWdlLmJnSW1hZ2V8fCdub25lJz09PXNlbGYuaW1hZ2UuYmdJbWFnZSk7fX0se2tleTpcImNhbkluaXRQYXJhbGxheFwiLHZhbHVlOmZ1bmN0aW9uIGNhbkluaXRQYXJhbGxheCgpe3JldHVybiBzdXBwb3J0VHJhbnNmb3JtJiYhdGhpcy5vcHRpb25zLmRpc2FibGVQYXJhbGxheCgpO319LHtrZXk6XCJpbml0XCIsdmFsdWU6ZnVuY3Rpb24gaW5pdCgpe3ZhciBzZWxmPXRoaXM7dmFyIGNvbnRhaW5lclN0eWxlcz17cG9zaXRpb246J2Fic29sdXRlJyx0b3A6MCxsZWZ0OjAsd2lkdGg6JzEwMCUnLGhlaWdodDonMTAwJScsb3ZlcmZsb3c6J2hpZGRlbid9O3ZhciBpbWFnZVN0eWxlcz17cG9pbnRlckV2ZW50czonbm9uZScsdHJhbnNmb3JtU3R5bGU6J3ByZXNlcnZlLTNkJyxiYWNrZmFjZVZpc2liaWxpdHk6J2hpZGRlbicsd2lsbENoYW5nZTondHJhbnNmb3JtLG9wYWNpdHknfTtpZighc2VsZi5vcHRpb25zLmtlZXBJbWcpe3ZhciBjdXJTdHlsZT1zZWxmLiRpdGVtLmdldEF0dHJpYnV0ZSgnc3R5bGUnKTtpZihjdXJTdHlsZSl7c2VsZi4kaXRlbS5zZXRBdHRyaWJ1dGUoJ2RhdGEtamFyYWxsYXgtb3JpZ2luYWwtc3R5bGVzJyxjdXJTdHlsZSk7fVxuaWYoc2VsZi5pbWFnZS51c2VJbWdUYWcpe3ZhciBjdXJJbWdTdHlsZT1zZWxmLmltYWdlLiRpdGVtLmdldEF0dHJpYnV0ZSgnc3R5bGUnKTtpZihjdXJJbWdTdHlsZSl7c2VsZi5pbWFnZS4kaXRlbS5zZXRBdHRyaWJ1dGUoJ2RhdGEtamFyYWxsYXgtb3JpZ2luYWwtc3R5bGVzJyxjdXJJbWdTdHlsZSk7fX19XG5pZignc3RhdGljJz09PXNlbGYuY3NzKHNlbGYuJGl0ZW0sJ3Bvc2l0aW9uJykpe3NlbGYuY3NzKHNlbGYuJGl0ZW0se3Bvc2l0aW9uOidyZWxhdGl2ZSd9KTt9XG5pZignYXV0byc9PT1zZWxmLmNzcyhzZWxmLiRpdGVtLCd6LWluZGV4Jykpe3NlbGYuY3NzKHNlbGYuJGl0ZW0se3pJbmRleDowfSk7fVxuc2VsZi5pbWFnZS4kY29udGFpbmVyPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO3NlbGYuY3NzKHNlbGYuaW1hZ2UuJGNvbnRhaW5lcixjb250YWluZXJTdHlsZXMpO3NlbGYuY3NzKHNlbGYuaW1hZ2UuJGNvbnRhaW5lcix7J3otaW5kZXgnOnNlbGYub3B0aW9ucy56SW5kZXh9KTtpZihpc0lFKXtzZWxmLmNzcyhzZWxmLmltYWdlLiRjb250YWluZXIse29wYWNpdHk6MC45OTk5fSk7fVxuc2VsZi5pbWFnZS4kY29udGFpbmVyLnNldEF0dHJpYnV0ZSgnaWQnLFwiamFyYWxsYXgtY29udGFpbmVyLVwiLmNvbmNhdChzZWxmLmluc3RhbmNlSUQpKTtzZWxmLiRpdGVtLmFwcGVuZENoaWxkKHNlbGYuaW1hZ2UuJGNvbnRhaW5lcik7aWYoc2VsZi5pbWFnZS51c2VJbWdUYWcpe2ltYWdlU3R5bGVzPXNlbGYuZXh0ZW5kKHsnb2JqZWN0LWZpdCc6c2VsZi5vcHRpb25zLmltZ1NpemUsJ29iamVjdC1wb3NpdGlvbic6c2VsZi5vcHRpb25zLmltZ1Bvc2l0aW9uLCdmb250LWZhbWlseSc6XCJvYmplY3QtZml0OiBcIi5jb25jYXQoc2VsZi5vcHRpb25zLmltZ1NpemUsXCI7IG9iamVjdC1wb3NpdGlvbjogXCIpLmNvbmNhdChzZWxmLm9wdGlvbnMuaW1nUG9zaXRpb24sXCI7XCIpLCdtYXgtd2lkdGgnOidub25lJ30sY29udGFpbmVyU3R5bGVzLGltYWdlU3R5bGVzKTt9ZWxzZXtzZWxmLmltYWdlLiRpdGVtPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO2lmKHNlbGYuaW1hZ2Uuc3JjKXtpbWFnZVN0eWxlcz1zZWxmLmV4dGVuZCh7J2JhY2tncm91bmQtcG9zaXRpb24nOnNlbGYub3B0aW9ucy5pbWdQb3NpdGlvbiwnYmFja2dyb3VuZC1zaXplJzpzZWxmLm9wdGlvbnMuaW1nU2l6ZSwnYmFja2dyb3VuZC1yZXBlYXQnOnNlbGYub3B0aW9ucy5pbWdSZXBlYXQsJ2JhY2tncm91bmQtaW1hZ2UnOnNlbGYuaW1hZ2UuYmdJbWFnZXx8XCJ1cmwoXFxcIlwiLmNvbmNhdChzZWxmLmltYWdlLnNyYyxcIlxcXCIpXCIpfSxjb250YWluZXJTdHlsZXMsaW1hZ2VTdHlsZXMpO319XG5pZignb3BhY2l0eSc9PT1zZWxmLm9wdGlvbnMudHlwZXx8J3NjYWxlJz09PXNlbGYub3B0aW9ucy50eXBlfHwnc2NhbGUtb3BhY2l0eSc9PT1zZWxmLm9wdGlvbnMudHlwZXx8MT09PXNlbGYub3B0aW9ucy5zcGVlZCl7c2VsZi5pbWFnZS5wb3NpdGlvbj0nYWJzb2x1dGUnO31cbmlmKCdmaXhlZCc9PT1zZWxmLmltYWdlLnBvc2l0aW9uKXt2YXIgJHBhcmVudHM9Z2V0UGFyZW50cyhzZWxmLiRpdGVtKS5maWx0ZXIoZnVuY3Rpb24oZWwpe3ZhciBzdHlsZXM9Z2xvYmFsX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8xX19bXCJ3aW5kb3dcIl0uZ2V0Q29tcHV0ZWRTdHlsZShlbCk7dmFyIHBhcmVudFRyYW5zZm9ybT1zdHlsZXNbJy13ZWJraXQtdHJhbnNmb3JtJ118fHN0eWxlc1snLW1vei10cmFuc2Zvcm0nXXx8c3R5bGVzLnRyYW5zZm9ybTt2YXIgb3ZlcmZsb3dSZWdleD0vKGF1dG98c2Nyb2xsKS87cmV0dXJuIHBhcmVudFRyYW5zZm9ybSYmJ25vbmUnIT09cGFyZW50VHJhbnNmb3JtfHxvdmVyZmxvd1JlZ2V4LnRlc3Qoc3R5bGVzLm92ZXJmbG93K3N0eWxlc1snb3ZlcmZsb3cteSddK3N0eWxlc1snb3ZlcmZsb3cteCddKTt9KTtzZWxmLmltYWdlLnBvc2l0aW9uPSRwYXJlbnRzLmxlbmd0aD8nYWJzb2x1dGUnOidmaXhlZCc7fVxuaW1hZ2VTdHlsZXMucG9zaXRpb249c2VsZi5pbWFnZS5wb3NpdGlvbjtzZWxmLmNzcyhzZWxmLmltYWdlLiRpdGVtLGltYWdlU3R5bGVzKTtzZWxmLmltYWdlLiRjb250YWluZXIuYXBwZW5kQ2hpbGQoc2VsZi5pbWFnZS4kaXRlbSk7c2VsZi5vblJlc2l6ZSgpO3NlbGYub25TY3JvbGwodHJ1ZSk7aWYoc2VsZi5vcHRpb25zLm9uSW5pdCl7c2VsZi5vcHRpb25zLm9uSW5pdC5jYWxsKHNlbGYpO31cbmlmKCdub25lJyE9PXNlbGYuY3NzKHNlbGYuJGl0ZW0sJ2JhY2tncm91bmQtaW1hZ2UnKSl7c2VsZi5jc3Moc2VsZi4kaXRlbSx7J2JhY2tncm91bmQtaW1hZ2UnOidub25lJ30pO31cbnNlbGYuYWRkVG9QYXJhbGxheExpc3QoKTt9fSx7a2V5OlwiYWRkVG9QYXJhbGxheExpc3RcIix2YWx1ZTpmdW5jdGlvbiBhZGRUb1BhcmFsbGF4TGlzdCgpe2phcmFsbGF4TGlzdC5wdXNoKHtpbnN0YW5jZTp0aGlzfSk7aWYoMT09PWphcmFsbGF4TGlzdC5sZW5ndGgpe2dsb2JhbF9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMV9fW1wid2luZG93XCJdLnJlcXVlc3RBbmltYXRpb25GcmFtZSh1cGRhdGVQYXJhbGxheCk7fX19LHtrZXk6XCJyZW1vdmVGcm9tUGFyYWxsYXhMaXN0XCIsdmFsdWU6ZnVuY3Rpb24gcmVtb3ZlRnJvbVBhcmFsbGF4TGlzdCgpe3ZhciBzZWxmPXRoaXM7amFyYWxsYXhMaXN0LmZvckVhY2goZnVuY3Rpb24oZGF0YSxrZXkpe2lmKGRhdGEuaW5zdGFuY2UuaW5zdGFuY2VJRD09PXNlbGYuaW5zdGFuY2VJRCl7amFyYWxsYXhMaXN0LnNwbGljZShrZXksMSk7fX0pO319LHtrZXk6XCJkZXN0cm95XCIsdmFsdWU6ZnVuY3Rpb24gZGVzdHJveSgpe3ZhciBzZWxmPXRoaXM7c2VsZi5yZW1vdmVGcm9tUGFyYWxsYXhMaXN0KCk7dmFyIG9yaWdpbmFsU3R5bGVzVGFnPXNlbGYuJGl0ZW0uZ2V0QXR0cmlidXRlKCdkYXRhLWphcmFsbGF4LW9yaWdpbmFsLXN0eWxlcycpO3NlbGYuJGl0ZW0ucmVtb3ZlQXR0cmlidXRlKCdkYXRhLWphcmFsbGF4LW9yaWdpbmFsLXN0eWxlcycpO2lmKCFvcmlnaW5hbFN0eWxlc1RhZyl7c2VsZi4kaXRlbS5yZW1vdmVBdHRyaWJ1dGUoJ3N0eWxlJyk7fWVsc2V7c2VsZi4kaXRlbS5zZXRBdHRyaWJ1dGUoJ3N0eWxlJyxvcmlnaW5hbFN0eWxlc1RhZyk7fVxuaWYoc2VsZi5pbWFnZS51c2VJbWdUYWcpe3ZhciBvcmlnaW5hbFN0eWxlc0ltZ1RhZz1zZWxmLmltYWdlLiRpdGVtLmdldEF0dHJpYnV0ZSgnZGF0YS1qYXJhbGxheC1vcmlnaW5hbC1zdHlsZXMnKTtzZWxmLmltYWdlLiRpdGVtLnJlbW92ZUF0dHJpYnV0ZSgnZGF0YS1qYXJhbGxheC1vcmlnaW5hbC1zdHlsZXMnKTtpZighb3JpZ2luYWxTdHlsZXNJbWdUYWcpe3NlbGYuaW1hZ2UuJGl0ZW0ucmVtb3ZlQXR0cmlidXRlKCdzdHlsZScpO31lbHNle3NlbGYuaW1hZ2UuJGl0ZW0uc2V0QXR0cmlidXRlKCdzdHlsZScsb3JpZ2luYWxTdHlsZXNUYWcpO31cbmlmKHNlbGYuaW1hZ2UuJGl0ZW1QYXJlbnQpe3NlbGYuaW1hZ2UuJGl0ZW1QYXJlbnQuYXBwZW5kQ2hpbGQoc2VsZi5pbWFnZS4kaXRlbSk7fX1cbmlmKHNlbGYuJGNsaXBTdHlsZXMpe3NlbGYuJGNsaXBTdHlsZXMucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzZWxmLiRjbGlwU3R5bGVzKTt9XG5pZihzZWxmLmltYWdlLiRjb250YWluZXIpe3NlbGYuaW1hZ2UuJGNvbnRhaW5lci5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHNlbGYuaW1hZ2UuJGNvbnRhaW5lcik7fVxuaWYoc2VsZi5vcHRpb25zLm9uRGVzdHJveSl7c2VsZi5vcHRpb25zLm9uRGVzdHJveS5jYWxsKHNlbGYpO31cbmRlbGV0ZSBzZWxmLiRpdGVtLmphcmFsbGF4O319LHtrZXk6XCJjbGlwQ29udGFpbmVyXCIsdmFsdWU6ZnVuY3Rpb24gY2xpcENvbnRhaW5lcigpe2lmKCdmaXhlZCchPT10aGlzLmltYWdlLnBvc2l0aW9uKXtyZXR1cm47fVxudmFyIHNlbGY9dGhpczt2YXIgcmVjdD1zZWxmLmltYWdlLiRjb250YWluZXIuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7dmFyIHdpZHRoPXJlY3Qud2lkdGgsaGVpZ2h0PXJlY3QuaGVpZ2h0O2lmKCFzZWxmLiRjbGlwU3R5bGVzKXtzZWxmLiRjbGlwU3R5bGVzPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3N0eWxlJyk7c2VsZi4kY2xpcFN0eWxlcy5zZXRBdHRyaWJ1dGUoJ3R5cGUnLCd0ZXh0L2NzcycpO3NlbGYuJGNsaXBTdHlsZXMuc2V0QXR0cmlidXRlKCdpZCcsXCJqYXJhbGxheC1jbGlwLVwiLmNvbmNhdChzZWxmLmluc3RhbmNlSUQpKTt2YXIgaGVhZD1kb2N1bWVudC5oZWFkfHxkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnaGVhZCcpWzBdO2hlYWQuYXBwZW5kQ2hpbGQoc2VsZi4kY2xpcFN0eWxlcyk7fVxudmFyIHN0eWxlcz1cIiNqYXJhbGxheC1jb250YWluZXItXCIuY29uY2F0KHNlbGYuaW5zdGFuY2VJRCxcIiB7XFxuICAgICAgICAgICAgY2xpcDogcmVjdCgwIFwiKS5jb25jYXQod2lkdGgsXCJweCBcIikuY29uY2F0KGhlaWdodCxcInB4IDApO1xcbiAgICAgICAgICAgIGNsaXA6IHJlY3QoMCwgXCIpLmNvbmNhdCh3aWR0aCxcInB4LCBcIikuY29uY2F0KGhlaWdodCxcInB4LCAwKTtcXG4gICAgICAgICAgICAtd2Via2l0LWNsaXAtcGF0aDogcG9seWdvbigwIDAsIDEwMCUgMCwgMTAwJSAxMDAlLCAwIDEwMCUpO1xcbiAgICAgICAgICAgIGNsaXAtcGF0aDogcG9seWdvbigwIDAsIDEwMCUgMCwgMTAwJSAxMDAlLCAwIDEwMCUpO1xcbiAgICAgICAgfVwiKTtpZihzZWxmLiRjbGlwU3R5bGVzLnN0eWxlU2hlZXQpe3NlbGYuJGNsaXBTdHlsZXMuc3R5bGVTaGVldC5jc3NUZXh0PXN0eWxlczt9ZWxzZXtzZWxmLiRjbGlwU3R5bGVzLmlubmVySFRNTD1zdHlsZXM7fX19LHtrZXk6XCJjb3ZlckltYWdlXCIsdmFsdWU6ZnVuY3Rpb24gY292ZXJJbWFnZSgpe3ZhciBzZWxmPXRoaXM7dmFyIHJlY3Q9c2VsZi5pbWFnZS4kY29udGFpbmVyLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO3ZhciBjb250SD1yZWN0LmhlaWdodDt2YXIgc3BlZWQ9c2VsZi5vcHRpb25zLnNwZWVkO3ZhciBpc1Njcm9sbD0nc2Nyb2xsJz09PXNlbGYub3B0aW9ucy50eXBlfHwnc2Nyb2xsLW9wYWNpdHknPT09c2VsZi5vcHRpb25zLnR5cGU7dmFyIHNjcm9sbERpc3Q9MDt2YXIgcmVzdWx0SD1jb250SDt2YXIgcmVzdWx0TVQ9MDtpZihpc1Njcm9sbCl7aWYoMD5zcGVlZCl7c2Nyb2xsRGlzdD1zcGVlZCpNYXRoLm1heChjb250SCx3bmRIKTtpZih3bmRIPGNvbnRIKXtzY3JvbGxEaXN0LT1zcGVlZCooY29udEgtd25kSCk7fX1lbHNle3Njcm9sbERpc3Q9c3BlZWQqKGNvbnRIK3duZEgpO31cbmlmKDE8c3BlZWQpe3Jlc3VsdEg9TWF0aC5hYnMoc2Nyb2xsRGlzdC13bmRIKTt9ZWxzZSBpZigwPnNwZWVkKXtyZXN1bHRIPXNjcm9sbERpc3Qvc3BlZWQrTWF0aC5hYnMoc2Nyb2xsRGlzdCk7fWVsc2V7cmVzdWx0SCs9KHduZEgtY29udEgpKigxLXNwZWVkKTt9XG5zY3JvbGxEaXN0Lz0yO31cbnNlbGYucGFyYWxsYXhTY3JvbGxEaXN0YW5jZT1zY3JvbGxEaXN0O2lmKGlzU2Nyb2xsKXtyZXN1bHRNVD0od25kSC1yZXN1bHRIKS8yO31lbHNle3Jlc3VsdE1UPShjb250SC1yZXN1bHRIKS8yO31cbnNlbGYuY3NzKHNlbGYuaW1hZ2UuJGl0ZW0se2hlaWdodDpcIlwiLmNvbmNhdChyZXN1bHRILFwicHhcIiksbWFyZ2luVG9wOlwiXCIuY29uY2F0KHJlc3VsdE1ULFwicHhcIiksbGVmdDonZml4ZWQnPT09c2VsZi5pbWFnZS5wb3NpdGlvbj9cIlwiLmNvbmNhdChyZWN0LmxlZnQsXCJweFwiKTonMCcsd2lkdGg6XCJcIi5jb25jYXQocmVjdC53aWR0aCxcInB4XCIpfSk7aWYoc2VsZi5vcHRpb25zLm9uQ292ZXJJbWFnZSl7c2VsZi5vcHRpb25zLm9uQ292ZXJJbWFnZS5jYWxsKHNlbGYpO31cbnJldHVybntpbWFnZTp7aGVpZ2h0OnJlc3VsdEgsbWFyZ2luVG9wOnJlc3VsdE1UfSxjb250YWluZXI6cmVjdH07fX0se2tleTpcImlzVmlzaWJsZVwiLHZhbHVlOmZ1bmN0aW9uIGlzVmlzaWJsZSgpe3JldHVybiB0aGlzLmlzRWxlbWVudEluVmlld3BvcnR8fGZhbHNlO319LHtrZXk6XCJvblNjcm9sbFwiLHZhbHVlOmZ1bmN0aW9uIG9uU2Nyb2xsKGZvcmNlKXt2YXIgc2VsZj10aGlzO3ZhciByZWN0PXNlbGYuJGl0ZW0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7dmFyIGNvbnRUPXJlY3QudG9wO3ZhciBjb250SD1yZWN0LmhlaWdodDt2YXIgc3R5bGVzPXt9O3ZhciB2aWV3cG9ydFJlY3Q9cmVjdDtpZihzZWxmLm9wdGlvbnMuZWxlbWVudEluVmlld3BvcnQpe3ZpZXdwb3J0UmVjdD1zZWxmLm9wdGlvbnMuZWxlbWVudEluVmlld3BvcnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7fVxuc2VsZi5pc0VsZW1lbnRJblZpZXdwb3J0PTA8PXZpZXdwb3J0UmVjdC5ib3R0b20mJjA8PXZpZXdwb3J0UmVjdC5yaWdodCYmdmlld3BvcnRSZWN0LnRvcDw9d25kSCYmdmlld3BvcnRSZWN0LmxlZnQ8PWdsb2JhbF9fV0VCUEFDS19JTVBPUlRFRF9NT0RVTEVfMV9fW1wid2luZG93XCJdLmlubmVyV2lkdGg7aWYoZm9yY2U/ZmFsc2U6IXNlbGYuaXNFbGVtZW50SW5WaWV3cG9ydCl7cmV0dXJuO31cbnZhciBiZWZvcmVUb3A9TWF0aC5tYXgoMCxjb250VCk7dmFyIGJlZm9yZVRvcEVuZD1NYXRoLm1heCgwLGNvbnRIK2NvbnRUKTt2YXIgYWZ0ZXJUb3A9TWF0aC5tYXgoMCwtY29udFQpO3ZhciBiZWZvcmVCb3R0b209TWF0aC5tYXgoMCxjb250VCtjb250SC13bmRIKTt2YXIgYmVmb3JlQm90dG9tRW5kPU1hdGgubWF4KDAsY29udEgtKGNvbnRUK2NvbnRILXduZEgpKTt2YXIgYWZ0ZXJCb3R0b209TWF0aC5tYXgoMCwtY29udFQrd25kSC1jb250SCk7dmFyIGZyb21WaWV3cG9ydENlbnRlcj0xLTIqKCh3bmRILWNvbnRUKS8od25kSCtjb250SCkpO3ZhciB2aXNpYmxlUGVyY2VudD0xO2lmKGNvbnRIPHduZEgpe3Zpc2libGVQZXJjZW50PTEtKGFmdGVyVG9wfHxiZWZvcmVCb3R0b20pL2NvbnRIO31lbHNlIGlmKGJlZm9yZVRvcEVuZDw9d25kSCl7dmlzaWJsZVBlcmNlbnQ9YmVmb3JlVG9wRW5kL3duZEg7fWVsc2UgaWYoYmVmb3JlQm90dG9tRW5kPD13bmRIKXt2aXNpYmxlUGVyY2VudD1iZWZvcmVCb3R0b21FbmQvd25kSDt9XG5pZignb3BhY2l0eSc9PT1zZWxmLm9wdGlvbnMudHlwZXx8J3NjYWxlLW9wYWNpdHknPT09c2VsZi5vcHRpb25zLnR5cGV8fCdzY3JvbGwtb3BhY2l0eSc9PT1zZWxmLm9wdGlvbnMudHlwZSl7c3R5bGVzLnRyYW5zZm9ybT0ndHJhbnNsYXRlM2QoMCwwLDApJztzdHlsZXMub3BhY2l0eT12aXNpYmxlUGVyY2VudDt9XG5pZignc2NhbGUnPT09c2VsZi5vcHRpb25zLnR5cGV8fCdzY2FsZS1vcGFjaXR5Jz09PXNlbGYub3B0aW9ucy50eXBlKXt2YXIgc2NhbGU9MTtpZigwPnNlbGYub3B0aW9ucy5zcGVlZCl7c2NhbGUtPXNlbGYub3B0aW9ucy5zcGVlZCp2aXNpYmxlUGVyY2VudDt9ZWxzZXtzY2FsZSs9c2VsZi5vcHRpb25zLnNwZWVkKigxLXZpc2libGVQZXJjZW50KTt9XG5zdHlsZXMudHJhbnNmb3JtPVwic2NhbGUoXCIuY29uY2F0KHNjYWxlLFwiKSB0cmFuc2xhdGUzZCgwLDAsMClcIik7fVxuaWYoJ3Njcm9sbCc9PT1zZWxmLm9wdGlvbnMudHlwZXx8J3Njcm9sbC1vcGFjaXR5Jz09PXNlbGYub3B0aW9ucy50eXBlKXt2YXIgcG9zaXRpb25ZPXNlbGYucGFyYWxsYXhTY3JvbGxEaXN0YW5jZSpmcm9tVmlld3BvcnRDZW50ZXI7aWYoJ2Fic29sdXRlJz09PXNlbGYuaW1hZ2UucG9zaXRpb24pe3Bvc2l0aW9uWS09Y29udFQ7fVxuc3R5bGVzLnRyYW5zZm9ybT1cInRyYW5zbGF0ZTNkKDAsXCIuY29uY2F0KHBvc2l0aW9uWSxcInB4LDApXCIpO31cbnNlbGYuY3NzKHNlbGYuaW1hZ2UuJGl0ZW0sc3R5bGVzKTtpZihzZWxmLm9wdGlvbnMub25TY3JvbGwpe3NlbGYub3B0aW9ucy5vblNjcm9sbC5jYWxsKHNlbGYse3NlY3Rpb246cmVjdCxiZWZvcmVUb3A6YmVmb3JlVG9wLGJlZm9yZVRvcEVuZDpiZWZvcmVUb3BFbmQsYWZ0ZXJUb3A6YWZ0ZXJUb3AsYmVmb3JlQm90dG9tOmJlZm9yZUJvdHRvbSxiZWZvcmVCb3R0b21FbmQ6YmVmb3JlQm90dG9tRW5kLGFmdGVyQm90dG9tOmFmdGVyQm90dG9tLHZpc2libGVQZXJjZW50OnZpc2libGVQZXJjZW50LGZyb21WaWV3cG9ydENlbnRlcjpmcm9tVmlld3BvcnRDZW50ZXJ9KTt9fX0se2tleTpcIm9uUmVzaXplXCIsdmFsdWU6ZnVuY3Rpb24gb25SZXNpemUoKXt0aGlzLmNvdmVySW1hZ2UoKTt0aGlzLmNsaXBDb250YWluZXIoKTt9fV0pO3JldHVybiBKYXJhbGxheDt9KCk7dmFyIHBsdWdpbj1mdW5jdGlvbiBwbHVnaW4oaXRlbXMsb3B0aW9ucyl7aWYoJ29iamVjdCc9PT0odHlwZW9mIEhUTUxFbGVtZW50PT09XCJ1bmRlZmluZWRcIj9cInVuZGVmaW5lZFwiOl90eXBlb2YoSFRNTEVsZW1lbnQpKT9pdGVtcyBpbnN0YW5jZW9mIEhUTUxFbGVtZW50Oml0ZW1zJiYnb2JqZWN0Jz09PV90eXBlb2YoaXRlbXMpJiZudWxsIT09aXRlbXMmJjE9PT1pdGVtcy5ub2RlVHlwZSYmJ3N0cmluZyc9PT10eXBlb2YgaXRlbXMubm9kZU5hbWUpe2l0ZW1zPVtpdGVtc107fVxudmFyIGxlbj1pdGVtcy5sZW5ndGg7dmFyIGs9MDt2YXIgcmV0O2Zvcih2YXIgX2xlbjI9YXJndW1lbnRzLmxlbmd0aCxhcmdzPW5ldyBBcnJheShfbGVuMj4yP19sZW4yLTI6MCksX2tleTI9Mjtfa2V5MjxfbGVuMjtfa2V5MisrKXthcmdzW19rZXkyLTJdPWFyZ3VtZW50c1tfa2V5Ml07fVxuZm9yKGs7azxsZW47ays9MSl7aWYoJ29iamVjdCc9PT1fdHlwZW9mKG9wdGlvbnMpfHwndW5kZWZpbmVkJz09PXR5cGVvZiBvcHRpb25zKXtpZighaXRlbXNba10uamFyYWxsYXgpe2l0ZW1zW2tdLmphcmFsbGF4PW5ldyBKYXJhbGxheChpdGVtc1trXSxvcHRpb25zKTt9fWVsc2UgaWYoaXRlbXNba10uamFyYWxsYXgpe3JldD1pdGVtc1trXS5qYXJhbGxheFtvcHRpb25zXS5hcHBseShpdGVtc1trXS5qYXJhbGxheCxhcmdzKTt9XG5pZigndW5kZWZpbmVkJyE9PXR5cGVvZiByZXQpe3JldHVybiByZXQ7fX1cbnJldHVybiBpdGVtczt9O3BsdWdpbi5jb25zdHJ1Y3Rvcj1KYXJhbGxheDtfX3dlYnBhY2tfZXhwb3J0c19fW1wiZGVmYXVsdFwiXT0ocGx1Z2luKTt9KV0pO1xuOy8qXG4gU3RpY2t5LWtpdCB2MS4xLjIgfCBXVEZQTCB8IExlYWYgQ29yY29yYW4gMjAxNSB8IGh0dHA6Ly9sZWFmby5uZXRcbiovXG4oZnVuY3Rpb24oKXt2YXIgYixmO2I9dGhpcy5qUXVlcnl8fHdpbmRvdy5qUXVlcnk7Zj1iKHdpbmRvdyk7Yi5mbi5zdGlja19pbl9wYXJlbnQ9ZnVuY3Rpb24oZCl7dmFyIEEsdyxKLG4sQixLLHAscSxrLEUsdDtudWxsPT1kJiYoZD17fSk7dD1kLnN0aWNreV9jbGFzcztCPWQuaW5uZXJfc2Nyb2xsaW5nO0U9ZC5yZWNhbGNfZXZlcnk7az1kLnBhcmVudDtxPWQub2Zmc2V0X3RvcDtwPWQuc3BhY2VyO3c9ZC5ib3R0b21pbmc7bnVsbD09cSYmKHE9MCk7bnVsbD09ayYmKGs9dm9pZCAwKTtudWxsPT1CJiYoQj0hMCk7bnVsbD09dCYmKHQ9XCJpc19zdHVja1wiKTtBPWIoZG9jdW1lbnQpO251bGw9PXcmJih3PSEwKTtKPWZ1bmN0aW9uKGEsZCxuLEMsRix1LHIsRyl7dmFyIHYsSCxtLEQsSSxjLGcseCx5LHosaCxsO2lmKCFhLmRhdGEoXCJzdGlja3lfa2l0XCIpKXthLmRhdGEoXCJzdGlja3lfa2l0XCIsITApO0k9QS5oZWlnaHQoKTtnPWEucGFyZW50KCk7bnVsbCE9ayYmKGc9Zy5jbG9zZXN0KGspKTtcbmlmKCFnLmxlbmd0aCl0aHJvd1wiZmFpbGVkIHRvIGZpbmQgc3RpY2sgcGFyZW50XCI7dj1tPSExOyhoPW51bGwhPXA/cCYmYS5jbG9zZXN0KHApOmIoXCI8ZGl2IC8+XCIpKSYmaC5jc3MoXCJwb3NpdGlvblwiLGEuY3NzKFwicG9zaXRpb25cIikpO3g9ZnVuY3Rpb24oKXt2YXIgYyxmLGU7aWYoIUcmJihJPUEuaGVpZ2h0KCksYz1wYXJzZUludChnLmNzcyhcImJvcmRlci10b3Atd2lkdGhcIiksMTApLGY9cGFyc2VJbnQoZy5jc3MoXCJwYWRkaW5nLXRvcFwiKSwxMCksZD1wYXJzZUludChnLmNzcyhcInBhZGRpbmctYm90dG9tXCIpLDEwKSxuPWcub2Zmc2V0KCkudG9wK2MrZixDPWcuaGVpZ2h0KCksbSYmKHY9bT0hMSxudWxsPT1wJiYoYS5pbnNlcnRBZnRlcihoKSxoLmRldGFjaCgpKSxhLmNzcyh7cG9zaXRpb246XCJcIix0b3A6XCJcIix3aWR0aDpcIlwiLGJvdHRvbTpcIlwifSkucmVtb3ZlQ2xhc3ModCksZT0hMCksRj1hLm9mZnNldCgpLnRvcC0ocGFyc2VJbnQoYS5jc3MoXCJtYXJnaW4tdG9wXCIpLDEwKXx8MCktcSxcbnU9YS5vdXRlckhlaWdodCghMCkscj1hLmNzcyhcImZsb2F0XCIpLGgmJmguY3NzKHt3aWR0aDphLm91dGVyV2lkdGgoITApLGhlaWdodDp1LGRpc3BsYXk6YS5jc3MoXCJkaXNwbGF5XCIpLFwidmVydGljYWwtYWxpZ25cIjphLmNzcyhcInZlcnRpY2FsLWFsaWduXCIpLFwiZmxvYXRcIjpyfSksZSkpcmV0dXJuIGwoKX07eCgpO2lmKHUhPT1DKXJldHVybiBEPXZvaWQgMCxjPXEsej1FLGw9ZnVuY3Rpb24oKXt2YXIgYixsLGUsaztpZighRyYmKGU9ITEsbnVsbCE9eiYmKC0teiwwPj16JiYoej1FLHgoKSxlPSEwKSksZXx8QS5oZWlnaHQoKT09PUl8fHgoKSxlPWYuc2Nyb2xsVG9wKCksbnVsbCE9RCYmKGw9ZS1EKSxEPWUsbT8odyYmKGs9ZSt1K2M+QytuLHYmJiFrJiYodj0hMSxhLmNzcyh7cG9zaXRpb246XCJmaXhlZFwiLGJvdHRvbTpcIlwiLHRvcDpjfSkudHJpZ2dlcihcInN0aWNreV9raXQ6dW5ib3R0b21cIikpKSxlPEYmJihtPSExLGM9cSxudWxsPT1wJiYoXCJsZWZ0XCIhPT1yJiZcInJpZ2h0XCIhPT1yfHxhLmluc2VydEFmdGVyKGgpLFxuaC5kZXRhY2goKSksYj17cG9zaXRpb246XCJcIix3aWR0aDpcIlwiLHRvcDpcIlwifSxhLmNzcyhiKS5yZW1vdmVDbGFzcyh0KS50cmlnZ2VyKFwic3RpY2t5X2tpdDp1bnN0aWNrXCIpKSxCJiYoYj1mLmhlaWdodCgpLHUrcT5iJiYhdiYmKGMtPWwsYz1NYXRoLm1heChiLXUsYyksYz1NYXRoLm1pbihxLGMpLG0mJmEuY3NzKHt0b3A6YytcInB4XCJ9KSkpKTplPkYmJihtPSEwLGI9e3Bvc2l0aW9uOlwiZml4ZWRcIix0b3A6Y30sYi53aWR0aD1cImJvcmRlci1ib3hcIj09PWEuY3NzKFwiYm94LXNpemluZ1wiKT9hLm91dGVyV2lkdGgoKStcInB4XCI6YS53aWR0aCgpK1wicHhcIixhLmNzcyhiKS5hZGRDbGFzcyh0KSxudWxsPT1wJiYoYS5hZnRlcihoKSxcImxlZnRcIiE9PXImJlwicmlnaHRcIiE9PXJ8fGguYXBwZW5kKGEpKSxhLnRyaWdnZXIoXCJzdGlja3lfa2l0OnN0aWNrXCIpKSxtJiZ3JiYobnVsbD09ayYmKGs9ZSt1K2M+QytuKSwhdiYmaykpKXJldHVybiB2PSEwLFwic3RhdGljXCI9PT1nLmNzcyhcInBvc2l0aW9uXCIpJiZnLmNzcyh7cG9zaXRpb246XCJyZWxhdGl2ZVwifSksXG5hLmNzcyh7cG9zaXRpb246XCJhYnNvbHV0ZVwiLGJvdHRvbTpkLHRvcDpcImF1dG9cIn0pLnRyaWdnZXIoXCJzdGlja3lfa2l0OmJvdHRvbVwiKX0seT1mdW5jdGlvbigpe3goKTtyZXR1cm4gbCgpfSxIPWZ1bmN0aW9uKCl7Rz0hMDtmLm9mZihcInRvdWNobW92ZVwiLGwpO2Yub2ZmKFwic2Nyb2xsXCIsbCk7Zi5vZmYoXCJyZXNpemVcIix5KTtiKGRvY3VtZW50LmJvZHkpLm9mZihcInN0aWNreV9raXQ6cmVjYWxjXCIseSk7YS5vZmYoXCJzdGlja3lfa2l0OmRldGFjaFwiLEgpO2EucmVtb3ZlRGF0YShcInN0aWNreV9raXRcIik7YS5jc3Moe3Bvc2l0aW9uOlwiXCIsYm90dG9tOlwiXCIsdG9wOlwiXCIsd2lkdGg6XCJcIn0pO2cucG9zaXRpb24oXCJwb3NpdGlvblwiLFwiXCIpO2lmKG0pcmV0dXJuIG51bGw9PXAmJihcImxlZnRcIiE9PXImJlwicmlnaHRcIiE9PXJ8fGEuaW5zZXJ0QWZ0ZXIoaCksaC5yZW1vdmUoKSksYS5yZW1vdmVDbGFzcyh0KX0sZi5vbihcInRvdWNobW92ZVwiLGwpLGYub24oXCJzY3JvbGxcIixsKSxmLm9uKFwicmVzaXplXCIsXG55KSxiKGRvY3VtZW50LmJvZHkpLm9uKFwic3RpY2t5X2tpdDpyZWNhbGNcIix5KSxhLm9uKFwic3RpY2t5X2tpdDpkZXRhY2hcIixIKSxzZXRUaW1lb3V0KGwsMCl9fTtuPTA7Zm9yKEs9dGhpcy5sZW5ndGg7bjxLO24rKylkPXRoaXNbbl0sSihiKGQpKTtyZXR1cm4gdGhpc319KS5jYWxsKHRoaXMpO1xuIl0sInNvdXJjZVJvb3QiOiIifQ==