<?php

namespace App\Http\Livewire\Backend;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\TableComponent;
use Rappasoft\LaravelLivewireTables\Traits\HtmlComponents;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class ContactsTable.
 */
class ContactsTable extends TableComponent
{
    use HtmlComponents;

    /**
     * @var string
     */
    public $sortField = 'title';

    /**
     * @var array
     */
    protected $options = [
        'bootstrap.container' => false,
        'bootstrap.classes.table' => 'table table-striped',
    ];

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Contact::query()->orderBy('created_at', 'asc');
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Company'), 'company')
                ->sortable()
                ->searchable(function ($builder, $term) {
                    return $builder->where('company', '%'.$term.'%');
                })
                ->format(function (Contact $model) {
                    return view('backend.contact.includes.company', ['contact' => $model]);
                }),
            Column::make(__('Name'), 'name')
                ->sortable()
                ->searchable(function ($builder, $term) {
                    return $builder->orWhere('name', '%'.$term.'%');
                })
                ->format(function (Contact $model) {
                    return view('backend.contact.includes.name', ['contact' => $model]);
                }),
            Column::make(__('Email'), 'email')
                ->sortable()
                ->searchable(function ($builder, $term) {
                    return $builder->orWhere('email', '%'.$term.'%');
                })
                ->format(function (Contact $model) {
                    return view('backend.contact.includes.email', ['contact' => $model]);
                }),
            Column::make(__('Phone'), 'phone')
                ->sortable()
                ->searchable(function ($builder, $term) {
                    return $builder->orWhere('phone', '%'.$term.'%');
                })
                ->format(function (Contact $model) {
                    return view('backend.contact.includes.phone', ['contact' => $model]);
                }),
            Column::make(__('CreatedAt'), 'created_at')
                ->sortable()
                ->format(function (Contact $model) {
                    return date('d/m/Y', strtotime($model->created_at));
                }),
            Column::make(__('Actions'))
                ->format(function (Contact $model) {
                    return view('backend.contact.includes.actions', ['contact' => $model]);
                }),
        ];
    }
}
