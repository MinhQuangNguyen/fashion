<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\PostService;
use App\Models\Category;
use Illuminate\Http\Request;

/**
 * Class NewsController.
 */
class DetailblogController extends Controller
{
    /**
     * NewsController constructor.
     *
     * @param  PostService  $postService
     * @param  CategoryService  $categoryService
     */
    public function __construct(PostService $postService, CategoryService $categoryService)
    {
        $this->postService = $postService;
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $slug = null)
    {
        $categories = $this->categoryService->getNewCategories();
        $category_id = null;
        $category = null;

        if($slug) {
            $category = $this->categoryService->getCategoryBySlug($slug);
            $category_id = $category->id;
        }
        $slides = $this->postService->getPostHighLight($category_id);
        $keyword = $request->get('keyword');
        $posts = $this->postService->getPostByCategory($category_id, $keyword);
        $libraries = $this->categoryService->getPostByCategoryType(2);
        $res = [];
        foreach($slides as $slide) {
            $title = $slide->category->type === Category::TYPE_LIBRARY ? $slide->type === 0 ? 'Hình ảnh' : 'Video' : $slide->category->name;
            array_push($res, [
                'title' => $title,
                'content' => $slide->title,
                'image' => $slide->banner,
            ]);
        }
        return view('frontend.detailBlog.index', [
            'categories' => $categories,
            'slides' => json_decode(json_encode($res)),
            'posts' => $posts,
            'libraries' => $libraries
        ]);
    }

    public function detail(Request $request, $categorySlug, $slug)
    {
        $post = $this->postService->getPostBySlug($slug);
        $category = $this->categoryService->where('id', $post->category_id)->where('active', 1)->first();
        $title = $category->type === Category::TYPE_LIBRARY ? $post->type === 0 ? 'Hình ảnh' : 'Video' : $category->name;
        $slides = [
            [
                'title' => $title,
                'content' => $post->title,
                'image' => $post->banner,
            ]
        ];

        $news_relates = $this->postService->getPostRelate($post->category_id, $post->id, $post->type);
        return view('frontend.detailBlog.detail', [
            'slides' => json_decode(json_encode($slides)),
            'post' => $post,
            'news_relates' => $news_relates
        ]);
    }

    public function detailBlog()
    {
        return view('frontend.detailBlog.detailBlog');
    }

    public function detailLibraryImg()
    {
        return view('frontend.detailBlog.detailLibraryImg');
    }
}
