<?php

namespace App\Services;

use App\Models\Department;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class DepartmentService.
 */
class DepartmentService extends BaseService
{
    /**
     * DepartmentService constructor.
     *
     * @param  Department  $department
     */
    public function __construct(Department $department)
    {
        $this->model = $department;
    }

    /**
     * @return mixed
     */
    public function getDepartments($id = null)
    {
        $query = $this->model::where('active', 1)->orderBy('sort', 'asc');
        return $query->get();
    }
}
