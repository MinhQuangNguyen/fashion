<?php

namespace App\Services;

use App\Models\Career;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class CareerService.
 */
class CareerService extends BaseService
{
    /**
     * CareerService constructor.
     *
     * @param  Career  $career
     */
    public function __construct(Career $career)
    {
        $this->model = $career;
    }

    /**
     * @return mixed
     */
    public function getCareers($id = null)
    {
        $query = $this->model::where('active', 1)->orderBy('sort', 'asc');
        return $query->get();
    }
}
