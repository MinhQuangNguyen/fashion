<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use App\Services\MenuService;
use App\Services\SettingService;
use App\Services\CategoryService;

/**
 * Class AppServiceProvider.
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
        Paginator::defaultView('pagination::iconic');
        //get menu
        $menuService = \App::make("App\Services\MenuService");
        $menu = $menuService->getForHome();
        //get setting
        $settingService = \App::make("App\Services\SettingService");
        $setting = $settingService->getSettingForHome();
        //slide gia tri cot loi
        $categoryService = \App::make("App\Services\CategoryService");
        $slidesT = $categoryService->getSlideByKey('homepage-new');

        view()->share(['menu' => $menu, 'setting' => $setting, 'slidesT' => $slidesT]);
    }
}
