@inject('model', '\App\Models\Category')

@extends('backend.layouts.app')

@section('title', __('Update Banner'))

@section('content')
    <x-forms.patch :action="route('admin.banner.update', $banner)" enctype="multipart/form-data">
        <x-backend.card>
            <x-slot name="header">
                @lang('Update Banner')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.banner.index')" :text="__('Back')" />
            </x-slot>

            <x-slot name="body">
                <div x-data="{key: '{{ $banner->key }}'}">
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

                        <div class="col-md-10">
                            <input type="text"  name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name') ?? $banner->name }}" maxlength="100" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="description" class="col-md-2 col-form-label">@lang('Description')</label>

                        <div class="col-md-10">
                            <textarea type="text" name="description" class="form-control" placeholder="{{ __('Description') }}">{{ old('description') ?? $banner->description }}</textarea>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="key" class="col-md-2 col-form-label">@lang('Position')</label>

                        <div class="col-md-10">
                            <select name="key" class="form-control" x-on:change="key = $event.target.value">
                                @foreach($model::KEYS as $item)
                                    <option value="{{ $item['key'] }}" {{ $banner->key === $item['key'] ? 'selected' : '' }}>{{ __($item['value']) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="sort" class="col-md-2 col-form-label">@lang('Active')</label>

                        <div class="col-md-10" style="line-height: 32px;">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="active" id="active1" value="{{ $model::ACTIVE }}" {{ $banner->active === $model::ACTIVE ? 'checked' : '' }}>
                                <label class="form-check-label" for="active1">@lang('Active')</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="active" id="active0" value="{{ $model::INACTIVE }}" {{ $banner->active === $model::INACTIVE ? 'checked' : '' }}>
                                <label class="form-check-label" for="active0">@lang('Inactive')</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="key" class="col-md-12 col-form-label">@lang('Images')</label>

                        <div class="col-md-12">
                            @include('backend.banner.includes.list-image')
                        </div>
                    </div><!--form-group-->
                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Update')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.patch>
@endsection
