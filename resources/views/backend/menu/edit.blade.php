@extends('backend.layouts.app')

@section('title', __('Update Menu'))

@section('content')
    <x-forms.patch :action="route('admin.menu.update', $menu)">
        <x-backend.card>
            <x-slot name="header">
                @lang('Update Menu')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.menu.index')" :text="__('Back')" />
            </x-slot>

            <x-slot name="body">
                <div x-data="{parentId : '{{ $menu->parent_id }}', categoryId: '{{ $menu->category_id }}'}">
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

                        <div class="col-md-10">
                            <input type="text"  name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name') ?? $menu->name }}" maxlength="100" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="category_id" class="col-md-2 col-form-label">@lang('Category')</label>

                        <div class="col-md-10">
                            <select name="category_id" class="form-control" x-on:change="categoryId = $event.target.value">
                                <option value=""></option>
                                @foreach($categories as $item)
                                    <option value="{{ $item->id }}" {{ $menu->category_id === $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="link" class="col-md-2 col-form-label">@lang('Link')</label>

                        <div class="col-md-10">
                            <input type="text"  name="link" class="form-control" placeholder="{{ __('Link') }}" value="{{ old('link') ?? $menu->link }}" maxlength="255" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="parent_id" class="col-md-2 col-form-label">@lang('Parent')</label>

                        <div class="col-md-10">
                            <select name="parent_id" class="form-control" x-on:change="parentId = $event.target.value">
                                <option value=""></option>
                                @foreach($parents as $item)
                                    <option value="{{ $item->id }}" {{ $menu->parent_id === $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="sort" class="col-md-2 col-form-label">@lang('Sort')</label>

                        <div class="col-md-10">
                            <input type="number" name="sort" class="form-control" placeholder="{{ __('Sort') }}" value="{{ old('sort') ?? $menu->sort }}" required />
                        </div>
                    </div>

                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Update')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.patch>

    @push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("select").select2({
                placeholder: "{{ __('Select a option') }}",
                allowClear: true
            });
        })
    </script>
    @endpush('after-scripts')
@endsection
