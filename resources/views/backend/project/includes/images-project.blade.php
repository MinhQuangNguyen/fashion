<div class="card">
    <h5 class="card-header">
        <a data-toggle="collapse" href="#collapse-image" aria-expanded="true" aria-controls="collapse-image" id="heading-image" class="d-block">
            <i class="fa fa-chevron-down float-right"></i>
            @lang('Images Project')
        </a>
    </h5>
    <div id="collapse-image" class="collapse show" aria-labelledby="heading-image">
        <div class="card-body">
            <div class="form-group row">
                <label for="title" class="col-md-2 col-form-label">@lang('Title')</label>

                <div class="col-md-10">
                    <input type="text" name="images[title]" class="form-control" placeholder="{{ __('Title') }}" value="{{ old('images.title') ?? $project->images['title'] ?? __('Images Project') }}" maxlength="100" required />
                </div>
            </div>

            @if(!isset($project))
            <div class="form-group row">
                <label for="title_en" class="col-md-2 col-form-label">@lang('Title (En)')</label>

                <div class="col-md-10">
                    <input type="text" name="images[title_en]" class="form-control" placeholder="{{ __('Title (En)') }}" value="{{ old('images.title') ?? 'Images' }}" maxlength="100" required />
                </div>
            </div>
            @endif

            <div class="form-group row">
                <label for="content" class="col-md-2 col-form-label">@lang('Content')</label>

                <div class="col-md-10">
                    <textarea type="text" name="images[content]" id="content_p" class="form-control" placeholder="{{ __('Content') }}">{{ old('images.content') ?? (isset($project) && isset($project->images['content']) ? $project->images['content'] : null) }}</textarea>
                </div>
            </div>

            @if(!isset($project))
            <div class="form-group row">
                <label for="content_en" class="col-md-2 col-form-label">@lang('Content (En)')</label>

                <div class="col-md-10">
                    <textarea type="text" name="images[content_en]" id="content_p_en" class="form-control" placeholder="{{ __('Content (En)') }}">{{ old('images.content_en') }}</textarea>
                </div>
            </div>
            @endif

            @include('backend.project.includes.images', ['type' => 'images', 'project' => isset($project) ? $project : null])
        </div>
    </div>
</div>
