<x-utils.edit-button :href="route('admin.project.edit', $project)" />
<x-utils.delete-button :href="route('admin.project.destroy', $project)" />
