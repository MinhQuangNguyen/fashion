<div class="about" data-scroll-section="" data-scroll-section-id="section1"
    style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
   <div class="container">
        <div class="row">
            <div class="offset-lg-3 offset-1"></div>
            <div class="col-lg-6 col-10">
                <div class="contact--text content-contact-mqn">
                    <p class="reveal fade-anim-default" data-scroll="" data-scroll-call="reveal" data-delay="300">
                      @lang("The Dark House is a minimalist, futuristic fashion brand that wants to bring a luxurious and modern look to customers.")
                    </p>
                    <p class="reveal fade-anim-default" data-scroll="" data-scroll-call="reveal" data-delay="400">
                      @lang("We aim to bring high quality apparel products with premium materials for long-term use. We strive to grow our business with the honesty and integrity we use to create our products.")
                    </p>
                </div>
                <div id="contact-scroll" class="contact--form">
                    <div role="form" class="wpcf7" id="wpcf7-f135-o1" lang="en-US" dir="ltr">
                        <div class="screen-reader-response">
                            <p role="status" aria-live="polite" aria-atomic="true"></p>
                            <ul></ul>
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <x-forms.post :action="route('frontend.contact.store')">
                            <div class="row">
                                <div class="col-12 col-lg-6 reveal fade-anim-default" data-scroll="" data-scroll-call="reveal"
                                    data-delay="300">
                                    <label>@lang("Username *")</label><br>
                                    <span class="wpcf7-form-control-wrap firstname">
                                        <input type="text" name="name" value="" required>
                                    </span>
                                </div>
                                <div class="col-12 col-lg-6 reveal fade-anim-default" data-scroll="" data-scroll-call="reveal"
                                    data-delay="300">
                                    <label>@lang("Email *")</label><br>
                                    <span class="wpcf7-form-control-wrap firstname">
                                        <input type="email" name="email" value="" required>
                                    </span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 reveal fade-anim-default" data-scroll="" data-scroll-call="reveal"
                                    data-delay="400">
                                    <label>@lang("Phone number *")</label><br>
                                    <span class="wpcf7-form-control-wrap firstname">
                                        <input type="text" name="phone" value="" required>
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 reveal fade-anim-default" data-scroll="" data-scroll-call="reveal"
                                    data-delay="500">
                                    <label>@lang("Message *")</label><br>
                                    <span class="wpcf7-form-control-wrap message">
                                        <textarea name="content" cols="40" rows="10" required></textarea>
                                    </span>
                                </div>
                            </div>
                            <div class="contact--form-button">
                                <div class="btn-wrapper reveal fade-anim-default" data-scroll=""
                                    data-scroll-call="reveal" data-delay="600">
                                    <input type="submit" value=@lang("Send") class="wpcf7-form-control wpcf7-submit">
                                </div>
                            </div>
                            <div class="wpcf7-response-output" aria-hidden="true"></div>
                        </x-forms.post>
                    </div>
                </div>

            </div>
            <div class="offset-lg-3 offset-1"></div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="contact--info">
                    <div class="row">
                        <div class="col-6">
                            <div class="contact--info-left">
                                <div class="contact--text">
                                    <p class="reveal fade-anim-default" data-scroll="" data-scroll-call="reveal"
                                        data-delay="300">
                                        FASHION Avant-garde
                                    </p>
                                    <p class="reveal fade-anim-default" data-scroll="" data-scroll-call="reveal"
                                        data-delay="400">
                                        @lang("Avant-garde is not simply fashion")
                                    </p>
                                    <p class="reveal fade-anim-default" data-scroll="" data-scroll-call="reveal"
                                        data-delay="500">
                                        @lang("but also a lifestyle")
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="contact--info-right">
                                <p class="reveal fade-anim-default" data-scroll="" data-scroll-call="reveal"
                                    data-delay="300">
                                    <a href="https://www.google.com/maps/place/12+Khu%E1%BA%A5t+Duy+Ti%E1%BA%BFn,+Thanh+Xu%C3%A2n+B%E1%BA%AFc,+Thanh+Xu%C3%A2n,+H%C3%A0+N%E1%BB%99i,+Vi%E1%BB%87t+Nam/@20.9935323,105.8000186,17z/data=!3m1!4b1!4m5!3m4!1s0x3135acbfac608051:0x4d0dd9816456b506!8m2!3d20.9935273!4d105.8022073?hl=vi-VN" target="_blank">
                                        {!! $setting['address'] !!}
                                    </a>
                                </p>
                                <p class="reveal fade-anim-default" data-scroll="" data-scroll-call="reveal"
                                    data-delay="400">
                                    <a href="tel:0345736211">
                                        {!! $setting['tel'] !!}
                                    </a>
                                </p>
                                <p class="reveal fade-anim-default" data-scroll="" data-scroll-call="reveal"
                                    data-delay="500">
                                    <a href="mailto:hotro@mediaiconic.com">
                                        {!! $setting['email'] !!}
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contact--decor-text marquee">
        <div class="marquee__inner" style="animation-duration: 66s;">
            <span> contact us for personalized offer</span>
            <span> contact us for personalized offer</span>
            <span> contact us for personalized offer</span>
            <span> contact us for personalized offer</span>
        </div>
    </div>
</div>


@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var url_string = window.location.href;
            setTimeout(() => {
                if (new RegExp('\#contact-scroll').test(url_string)) {
                    $('html, body').animate({
                        scrollTop: $("#contact-scroll").offset().top - 0
                    }, 100);
                }
            }, 1000)
        })
    </script>
@endpush