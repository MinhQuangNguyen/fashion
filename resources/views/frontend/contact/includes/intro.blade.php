<section class="intro-wide-img" data-scroll-section="">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="intro-wide-img--content">
                    <p class="small-title-left big-letters reveal animtitle is-inview"
                        data-scroll="" data-scroll-call="load_reveal" data-delay="200">@lang("Contact me")</p>

                    <h2 class="intro-title big reveal animtitle is-inview" data-scroll=""
                        data-delay="600">
                        <span class="small">@lang("Change,") <span class="decoration">@lang("Yourself")</span> @lang("to")</span> <br> @lang("create the") <br><span
                            class="decoration2">@lang("Different")</span>    
                    </h2>
                </div>
                <div class="intro-wide-img--image">
                    <div class="image-view reveal gsap-fade-anim-bottom is-inview" data-scroll=""
                        data-scroll-call="load_reveal" data-delay="1">
                        <img width="810" height="584"
                            class="scroll-obj-fit-cover is-inview lazyloaded" data-scroll=""
                            data-scroll-speed="1" src="img/contact.jpg" alt=""
                            data-ll-status="loaded">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="intro-wide-img--text marquee">
        <div class="marquee__inner" style="animation-duration: 38s;">
            <span>Contact Me</span>
            <span>Contact Me</span>
            <span>Contact Me</span>
            <span>Contact Me</span>
        </div>
    </div>
</section>