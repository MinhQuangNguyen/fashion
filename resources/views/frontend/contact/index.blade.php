@extends('frontend.layouts.iconic')

@section('title', __('Contact'))

@section('content')
@include('frontend.contact.includes.intro')
@include('frontend.contact.includes.contact')
@include('frontend.home.includes.outStory')
@endsection
