@extends('frontend.layouts.iconic')

@section('title', __('Home'))

@section('content')
@include('frontend.home.includes.intro')
@include('frontend.home.includes.slideRight')
@include('frontend.home.includes.slideLeft')
@include('frontend.home.includes.outStory')
@endsection
