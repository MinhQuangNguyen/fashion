<section class="our-story" data-scroll-section="">
    <div class="container">
        <div class="row">
            <div class="offset-3"></div>
            <div class="col-lg-4 col-12">
                <h2 class="section-title category-title reveal left animtitle" data-scroll=""
                    data-delay="600" data-scroll-call="reveal" data-scroll-speed="1.5"
                    style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
                    @lang('Avant')<br><span>@lang('Garde')</span></h2>
            </div>
            <div class="col-lg-5 col-12">
                <div class="our-story--content">
                    <div class="wrapper">
                        <p class="reveal fade-anim-default" data-scroll="" data-scroll-call="reveal"
                            data-delay="300">
                            @lang('The Avant-garde art school reflects the endless values of creativity, breakthroughs and exclusivity. Avant-garde artists have a passion to become the pioneers of the era. But above all, it is to affirm a personal voice and outlook towards the world and human life.')
                          </p>
                        <div class="our-story--content-link">
                            <a class="small-title-right align-left reveal animtitle" data-scroll=""
                                data-scroll-call="reveal" data-delay="300"
                                href="/gioi-thieu"><span class="word">@lang('view') </span><span class="word">@lang('MORE')</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="our-story--image">
            <div class="image-view reveal gsap-fade-anim-bottom is-inview" data-scroll=""
                data-scroll-call="reveal" data-delay="0.2">
                <img width="810" height="584" class="scroll-obj-fit-cover is-inview lazyloaded"
                    data-scroll="" data-scroll-speed="1" alt="The Dark House" src="/img/the-dark-store.jpg" alt=""
                    data-ll-status="loaded">
            </div>
        </div>
    </div>
    <div class="our-story--text marquee">
        <div class="marquee__inner" style="animation-duration: 18s;">
            <span>Avant-garde</span>
            <span>Avant-garde</span>
            <span>Avant-garde</span>
            <span>Avant-garde</span>
        </div>
    </div>
</section>