<div class="slider-right-side" data-scroll-section="" data-scroll-section-id="section1"
    style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
    <div class="container">
        <div class="row">
            <div class="col-12 slider-col-right">
                <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-autoheight"
                    data-method="imageSlider">
                    <div class="swiper-wrapper" id="swiper-wrapper-d37a736e8bdfab10c" aria-live="polite"
                        style="height: 683px; transform: translate3d(-1366px, 0px, 0px); transition: all 0ms ease 0s;">
                        @foreach($posts as $post) 
                        <div class="swiper-slide"
                                role="group" aria-label="4 / 9">
                                <a href="{{ asset($post->image) }}" data-fancybox="gallery" data-barba-prevent="" data-caption="{{ $post->title }} &lt;br&gt; {{ $post->tagser }}">
                                    <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                        data-scroll="" data-delay="200" data-scroll-call="reveal" src="{{ asset($post->image) }}" alt=""
                                        data-lazy-src="{{ asset($post->image) }}">
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p class="reveal left fade-anim-default" data-scroll="" data-scroll-call="reveal" data-delay="300">
                   @lang("Main materials such as wax, poly, nylon, leather, Lien (Cotton) are used")
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-8">
                <div class="slider-right-side--content">
                    <div class="content-wrapper">

                        <a href="/bo-suu-tap"
                            class="small-title-left post reveal animtitle" data-scroll=""
                            data-scroll-speed="1.5" data-scroll-call="reveal" data-barba-prevent="" data-delay="200"
                            style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">@lang("view")
                            @lang("MORE")</a>
                            <h3 class="section-title category-title reveal left animtitle" data-scroll="" data-delay="600"
                            data-scroll-call="reveal" data-scroll-speed="1.5"
                            style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
                            @lang("Fashion Collection 2021")</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slider-right-side--text post marquee">
        <div class="marquee__inner" style="animation-duration: 80s;">
            <span>Spring-summer collection 2021</span>
            <span>Spring-summer collection 2021</span>
            <span>Spring-summer collection 2021</span>
            <span>Spring-summer collection 2021</span>
        </div>
    </div>
</div>
