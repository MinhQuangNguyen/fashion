<div class="slider-left-side" data-scroll-section="">
    <div class="container">
        <div class="row">
            <div class="col-12 slider-col">
                <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-autoheight"
                    data-method="imageSlider">
                    <div class="swiper-wrapper" id="swiper-wrapper-898195a5401552ba" aria-live="polite"
                        style="height: 683px; transform: translate3d(-1366px, 0px, 0px); transition: all 0ms ease 0s;">

                        <div class="swiper-slide swiper-slide-next" role="group">
                            <a href="/img/22.jpg" data-fancybox="gallery" data-barba-prevent=""
                                data-caption="Japan Calf Leather Tailored Shirt JKT &lt;br&gt; Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/22.jpg" alt=""
                                    data-lazy-src="/img/22.jpg">
                            </a>
                        </div>



                        <div class="swiper-slide swiper-slide-active" role="group">
                            <a href="/img/21.jpg" data-fancybox="gallery" data-barba-prevent=""
                                data-caption="High-neck blouson Black &lt;br&gt; Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/21.jpg" alt=""
                                    data-lazy-src="/img/21.jpg">
                            </a>
                        </div>

                        <div class="swiper-slide swiper-slide-active" role="group">
                            <a href="/img/20.jpg" data-fancybox="gallery" data-barba-prevent=""
                                data-caption="Square Print Linen Coat Black Stripe &lt;br&gt; Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/20.jpg" alt=""
                                    data-lazy-src="/img/20.jpg">
                            </a>
                        </div>


                        <div class="swiper-slide swiper-slide-next" role="group">
                            <a href="/img/23.jpg" data-fancybox="gallery" data-barba-prevent=""
                                data-caption="High-Neck horse leather JKT &lt;br&gt; Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/23.jpg" alt=""
                                    data-lazy-src="/img/23.jpg">
                            </a>
                        </div>

                        <div class="swiper-slide swiper-slide-next" role="group">
                            <a href="/img/24.jpg" data-fancybox="gallery" data-barba-prevent=""
                                data-caption="Destroy dyed Horse leather jacket - ST105-0049A &lt;br&gt; Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/24.jpg" alt=""
                                    data-lazy-src="/img/24.jpg">
                            </a>
                        </div>

                        <div class="swiper-slide swiper-slide-next" role="group">
                            <a href="/img/25.jpg" data-fancybox="gallery" data-barba-prevent=""
                                data-caption="PAINTED RIDERS JACKET &lt;br&gt; Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/25.jpg" alt=""
                                    data-lazy-src="/img/25.jpg">
                            </a>
                        </div>

                    </div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-6"></div>
            <div class="col-6">
                <div class="slider-left-side--content">
                    <div class="content-wrapper">
                        <a class="small-title-right has-mask is-inview" data-barba-prevent="" data-scroll="" data-scroll-call="reveal"
                            data-delay="200" data-scroll-speed="1.5" href="/lookbook"><span
                                class="word">@lang('view') </span><span class="word">@lang('MORE')</span></a>
                            <h3 class="section-title category-title reveal left animtitle" data-scroll="" data-delay="600"
                                data-scroll-call="reveal" data-scroll-speed="1.5"
                                style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
                                @lang('Lookbook Limited The Dark House')
                            </h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p class="reveal left fade-anim-default" data-scroll="" data-scroll-call="reveal" data-delay="300">
                  @lang('There were the presence of fashionista: Alexander McQueen, omme des Garçons, Viktor and Rolf')
                </p>
            </div>
        </div>
    </div>
    <div class="slider-left-side--text marquee">
        <div class="marquee__inner" style="animation-duration: 60s;">
            <span>Lookbook Limited The Dark House</span>
            <span>Lookbook Limited The Dark House</span>
            <span>Lookbook Limited The Dark House</span>
            <span>Lookbook Limited The Dark House</span>
        </div>
    </div>
</div>
