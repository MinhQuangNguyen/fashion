@extends('frontend.layouts.iconic')

@section('title', __('Giới thiệu'))
@section('content')
    @include('frontend.about.includes.intro')
    @include('frontend.about.includes.about')
    @include('frontend.project.includes.list')
    @include('frontend.project.includes.nextList')
@endsection
