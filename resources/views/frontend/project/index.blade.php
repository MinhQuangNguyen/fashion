@extends('frontend.layouts.iconic')

@section('title', __('Lookbook'))

@section('content')
@include('frontend.project.includes.intro')
@include('frontend.project.includes.list')
@include('frontend.project.includes.buttonContact')
@include('frontend.project.includes.nextList')

@endsection
