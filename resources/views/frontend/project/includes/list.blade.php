<div class="slider-right-side" data-scroll-section="" data-scroll-section-id="section1"
    style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
    <div class="container">
        <div class="row">
            <div class="offset-lg-3 offset-1"></div>
            <div class="col-lg-6 col-10">
                <div class="post-content">
                    <p class="reveal fade-anim-default" data-scroll="" data-scroll-call="reveal" data-delay="300">
                      @lang("With the desire to bring fashion into the daily beauty and enhance your beauty, The Dark House wants to send out costumes when we wear the street, the first thing is always to do for ourselves. you guys are comfortable and really confident in being yourself.")
                    </p>
                </div>
            </div>
            <div class="offset-lg-3 offset-1"></div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 slider-col-right">
                <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-autoheight"
                    data-method="imageSlider">
                     <div class="swiper-wrapper" id="swiper-wrapper-898195a5401552ba" aria-live="polite"
                        style="height: 683px; transform: translate3d(-1366px, 0px, 0px); transition: all 0ms ease 0s;">
                        
                        <div class="swiper-slide swiper-slide-active"
                            role="group" aria-label="4 / 9">
                            <a href="/img/30.jpg" data-fancybox="gallery" data-barba-prevent="" data-caption="Coat Silk Herringbone Sand Blast Finish &lt;br&gt; Lookbook Lostboy Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/30.jpg" alt=""
                                    data-lazy-src="/img/30.jpg">
                            </a>
                        </div>

                        <div class="swiper-slide swiper-slide-next"
                            role="group" aria-label="4 / 9">
                            <a href="/img/31.jpg" data-fancybox="gallery" data-barba-prevent="" data-caption="FASCNATE Limited Coat Camel /Linen Shadow Check &lt;br&gt; Lookbook Lostboy Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/31.jpg" alt=""
                                    data-lazy-src="/img/31.jpg">
                            </a>
                        </div>

                        <div class="swiper-slide swiper-slide-next"
                            role="group" aria-label="4 / 9">
                            <a href="/img/32.jpg" data-fancybox="gallery" data-barba-prevent="" data-caption="HANDSTITCH FINE BISTR. GABARDINE JACKET &lt;br&gt; Lookbook Lostboy Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/32.jpg" alt=""
                                    data-lazy-src="/img/32.jpg">
                            </a>
                        </div>

                        <div class="swiper-slide swiper-slide-next"
                            role="group" aria-label="4 / 9">
                            <a href="/img/33.jpg" data-fancybox="gallery" data-barba-prevent="" data-caption="16AW New England Lamb Melton Jacket &lt;br&gt; Lookbook Lostboy Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/33.jpg" alt=""
                                    data-lazy-src="/img/33.jpg">
                            </a>
                        </div>

                        <div class="swiper-slide swiper-slide-next"
                            role="group" aria-label="4 / 9">
                            <a href="/img/35.jpg" data-fancybox="gallery" data-barba-prevent="" data-caption="17SS Ace/Pa Satin Stretch Blouson &lt;br&gt; Lookbook Lostboy Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/35.jpg" alt=""
                                    data-lazy-src="/img/35.jpg">
                            </a>
                        </div>

                    </div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p class="reveal left fade-anim-default" data-scroll="" data-scroll-call="reveal" data-delay="300">
                   @lang("Lookbook Lostboy Collection 2021 comes from designer Minh Quang Nguyen")
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-8">
                <div class="slider-right-side--content">
                    <div class="content-wrapper">
                        <a href="/img/30.jpg"
                            data-fancybox="gallery" class="small-title-left post reveal animtitle" data-scroll=""
                            data-caption="Coat Silk Herringbone Sand Blast Finish &lt;br&gt; Lookbook Lostboy Collection 2021"
                            data-scroll-speed="1.5" data-scroll-call="reveal" data-barba-prevent="" data-delay="200"
                            style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">@lang("view")
                            LOOKBOOK</a>
                            <h3 class="section-title category-title reveal left animtitle" data-scroll="" data-delay="600"
                            data-scroll-call="reveal" data-scroll-speed="1.5"
                            style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
                            Lookbook Lostboy Collection 2021
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slider-right-side--text post marquee">
        <div class="marquee__inner" style="animation-duration: 80s;">
            <span>Decorative elements for table decoration</span>
            <span>Decorative elements for table decoration</span>
            <span>Decorative elements for table decoration</span>
            <span>Decorative elements for table decoration</span>
        </div>
    </div>
</div>

