<div class="about" data-scroll-section="" data-scroll-section-id="section2"
    style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
    <div class="container">
        <div class="row">
            <div class="offset-lg-3 offset-1"></div>
            <div class="col-lg-6 col-10">
                <div class="about--example">
                    <p class="reveal fade-anim-default" data-scroll="" data-scroll-call="reveal" data-delay="300">
                        @lang("Make a difference for yourself.")<br>
                        @lang("All products are customized upon request.")
                    </p>
                </div>
                <div class="about--btn reveal fade-anim-default" data-scroll="" data-scroll-call="reveal" data-delay="300">
                    <a href="/contact" class="btn">@lang("Contact us to get deals")</a>
                </div>
            </div>
            <div class="offset-lg-3 offset-1"></div>
        </div>
    </div>
</div>

