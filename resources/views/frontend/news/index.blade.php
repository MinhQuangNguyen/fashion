@extends('frontend.layouts.iconic')

@section('title', __('Bộ sưu tập'))

@section('content')
@include('frontend.news.includes.intro')
@include('frontend.news.includes.list')
@include('frontend.news.includes.buttonContact')
@include('frontend.news.includes.nextList')

@endsection
