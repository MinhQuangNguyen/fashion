<div class="slider-left-side" data-scroll-section="">
    <div class="container">
        <div class="row">
            <div class="col-12 slider-col">
               <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-autoheight"
                    data-method="imageSlider">
                    <div class="swiper-wrapper" id="swiper-wrapper-898195a5401552ba" aria-live="polite"
                        style="height: 683px; transform: translate3d(-1366px, 0px, 0px); transition: all 0ms ease 0s;">
                        
                        <div class="swiper-slide swiper-slide-active" role="group">
                            <a href="/img/11.jpg" data-fancybox="gallery" data-barba-prevent="" 
                                data-caption="Salt Shrink Cotton Cupro Batting No Collar Jacket &lt;br&gt; Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/11.jpg" alt=""
                                    data-lazy-src="/img/11.jpg">
                            </a>
                        </div>

                        <div class="swiper-slide swiper-slide-next" role="group">
                            <a href="/img/12.jpg" data-fancybox="gallery" data-barba-prevent=""
                                data-caption="Army Jacket Coyote &lt;br&gt; Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/12.jpg" alt=""
                                    data-lazy-src="/img/12.jpg">
                            </a>
                        </div>

                        <div class="swiper-slide swiper-slide-next" role="group">
                            <a href="/img/13.jpg" data-fancybox="gallery" data-barba-prevent=""
                                data-caption="Linen jersey Riders &lt;br&gt; Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/13.jpg" alt=""
                                    data-lazy-src="/img/13.jpg">
                            </a>
                        </div>

                        <div class="swiper-slide swiper-slide-next" role="group">
                            <a href="/img/14.jpg" data-fancybox="gallery" data-barba-prevent=""
                                data-caption="Linen Broad Jacket &lt;br&gt; Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/14.jpg" alt=""
                                    data-lazy-src="/img/14.jpg">
                            </a>
                        </div>

                        <div class="swiper-slide swiper-slide-next" role="group">
                            <a href="/img/15.jpg" data-fancybox="gallery" data-barba-prevent=""
                                data-caption="Linen Broad Coat &lt;br&gt; Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/15.jpg" alt=""
                                    data-lazy-src="/img/15.jpg">
                            </a>
                        </div>

                        <div class="swiper-slide swiper-slide-next" role="group">
                            <a href="/img/16.jpg" data-fancybox="gallery" data-barba-prevent=""
                                data-caption="Square Print Linen Coat Black Stripe &lt;br&gt; Collection 2021">
                                <img width="450" height="600" class="obj-fit-cover reveal fade-anim-bottom"
                                    data-scroll="" data-delay="200" data-scroll-call="reveal" src="/img/16.jpg" alt=""
                                    data-lazy-src="/img/16.jpg">
                            </a>
                        </div>
                    </div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-6"></div>
            <div class="col-6">
                <div class="slider-left-side--content">
                    <div class="content-wrapper">
                        <a class="small-title-right has-mask is-inview" data-scroll="" data-fancybox="gallery" data-barba-prevent=""
                            data-delay="200" data-scroll-speed="1.5"
                            data-caption="Linen Broad Jacket &lt;br&gt; Collection 2021"
                            href="/img/14.jpg"><span class="word">@lang("view") </span><span class="word">@lang("COLLECTION")</span></a>
                        <h3 class="section-title category-title reveal left animtitle" data-scroll="" data-delay="600"
                            data-scroll-call="reveal" data-scroll-speed="1.5"
                            style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);">
                            @lang("Fall Winter 2021 Collection")</h3>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p class="reveal fade-anim-default" data-scroll="" data-scroll-call="reveal" data-delay="300">
                    @lang("The Dark House collection is all the time and money to create a masterpiece")
                </p>
            </div>
        </div>
    </div>
    <div class="slider-left-side--text marquee">
        <div class="marquee__inner" style="animation-duration: 60s;">
            <span>Autumn-winter collection 2021</span>
            <span>Autumn-winter collection 2021</span>
            <span>Autumn-winter collection 2021</span>
            <span>Autumn-winter collection 2021</span>
        </div>
    </div>
</div>