<footer class="page-footer" data-scroll-section="">
    <div class="container">
        <div class="page-footer--decoration"></div>
        <div class="row">
            <div class="col-md-3 col-12">
                <div class="page-footer--logo">
                    <a href="/" title="The Dark House" class="site-logo">
                        <img src="/img/logo.png" alt="The Dark House"" width="250px">
                    </a>
                </div>

                <div class="page-footer--email">
                    <p class="test-desc-footer">
                        @lang("Minimalistic, futuristic fashion brand, looking to bring a luxurious and modern look to customers")
                    </p>
                    <p>
                        <a href="https://www.google.com/maps/place/12+Khu%E1%BA%A5t+Duy+Ti%E1%BA%BFn,+Thanh+Xu%C3%A2n+B%E1%BA%AFc,+Thanh+Xu%C3%A2n,+H%C3%A0+N%E1%BB%99i,+Vi%E1%BB%87t+Nam/@20.9935323,105.8000186,17z/data=!3m1!4b1!4m5!3m4!1s0x3135acbfac608051:0x4d0dd9816456b506!8m2!3d20.9935273!4d105.8022073?hl=vi-VN" target="_blank">
                            {!! $setting['address'] !!}
                        </a>
                    </p>
                    <p>
                        <a href="tel:0345736211">
                            {!! $setting['tel'] !!}
                        </a>
                    </p>
                    <p>
                        <a href="mailto:hotro@mediaiconic.com">
                            {!! $setting['email'] !!}
                        </a>
                    </p>
                </div>
            </div>
            <div class="col-md-5 col-sm-6 col-12">
                <div class="page-footer--title">
                    <h3>@lang("Collection")</h3>
                </div>
                <div class="page-footer--content">
                    <ul>
                        <li>
                            <a href="">
                                @lang("Avant-garde Spring - Summer collection 2021 (Designer Minh Quang Nguyen)")
                            </a>
                        </li>
                        <li>
                            <a href="">
                                @lang("Avant-garde Fall - Winter Collection 2021 (Designer Minh Quang Nguyen)")
                            </a>
                        </li>
                        <li>
                            <a href="">
                                @lang("New collection for the 2021 Halloween season")
                            </a>
                        </li>
                        <li>
                            <a href="">
                                @lang("Darkwear new collection 2022")
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-12">
                <div class="page-footer--title">
                    <h3>@lang("Lookbook Collection")</h3>
                </div>
                <div class="page-footer--content">
                    <ul>
                        <li>
                            <a href="">
                                @lang("Lookbook limited Bloody Sky")
                            </a>
                        </li>
                        <li>
                            <a href="">
                                @lang("Lookbook Lostboy collection 2020")
                            </a>
                        </li>
                        <li>
                            <a href="">
                                @lang("Lookbook end session 2019 - Darkwear")
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-12"></div>
            <div class="col-md-5 col-sm-6 col-12">
                <div class="page-footer--social">
                    <ul>
                        <li>
                            <a href="" target="_blank" rel="noreferrer noopener">
                                <img src="/img/facebook.svg" alt="socical-the-dark-house"
                                    class="socical-the-dark-house">
                            </a>
                        </li>
                        <li>
                            <a href="" target="_blank" rel="noreferrer noopener">
                                <img src="/img/ins.svg" alt="socical-the-dark-house" class="socical-the-dark-house">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-12">
                <div class="page-footer--crafted-by">
                    <a href="https://mediaiconic.com/" target="_blank" rel="noreferrer noopener">
                        @lang("© 2021 The Dark House Copyright Media Iconic")
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>
