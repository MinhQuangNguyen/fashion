<div class="menu-mobile">
    <div class="back-drop"></div>
    <ul class="navbar-nav">
        @if(isset($menu))
        @foreach ($menu as $menuItem)
            @if ($menuItem->hasChild())
                <li class="nav-item dropdown {{ request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)) || request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)."/*") ? 'active': ''}}">
                    <a class="nav-link" href="{{$menuItem->clickable ? $menuItem->link: '#'}}">
                        {{ $menuItem->name }}
                    </a>
                    {{-- <div class="dropdown-menu" aria-labelledby="navbarDropdown-{{ $menuItem->id }}">
                        @foreach ($menuItem->getChild() as $subMenu)
                            <a class="dropdown-item {{request()->is(strlen($subMenu->link) > 1 ? substr($subMenu->link, 1) : $subMenu->link) || request()->is((strlen($subMenu->link) > 1 ? substr($subMenu->link, 1) : $subMenu->link)."/*") ? 'active': ''}}" data-href="{{ $subMenu->link }}" href="{{ $subMenu->link }}">
                                {{ $subMenu->name }}
                            </a>
                        @endforeach
                    </div> --}}
                </li>
            @else
            <?php $link = strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link ?>
            <li class="nav-item {{ request()->is($link) || request()->is($link.'/*') ? 'active': '' }}">
                <a class="nav-link" href="{{ $menuItem->link }}">
                    {{ $menuItem->name }}
                </a>
            </li>
            @endif
        @endforeach
        @endif
    </ul>
    <div class="bottom-bar">
        @include("frontend.includes.lang-search")
    </div>
</div>
