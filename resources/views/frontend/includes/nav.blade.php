    <!-- Mobile Menu -->
    <div class="mobile-menu">
        <div class="mobile-menu-wrapper">
            <div class="mobile-text marquee">
                <div class="marquee__inner" style="animation-duration: 38s;">
                    <span>The Dark House</span>
                    <span>The Dark House</span>
                    <span>The Dark House</span>
                    <span>The Dark House</span>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-6 offset-lg-1">
                        <ul class="main-menu">

                            @if(isset($menu))
                                @foreach ($menu as $menuItem)
                                    @if ($menuItem->hasChild())
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-6 current_page_item {{ request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)) || request()->is((strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link)."/*") ? 'active': ''}}">
                                                    <a class="nav-link" href="{{$menuItem->clickable ? $menuItem->link: '#'}}">
                                                        {{ $menuItem->name }}
                                                    </a>
                                                </li>
                                            @else
                                            <?php $link = strlen($menuItem->link) > 1 ? substr($menuItem->link, 1) : $menuItem->link ?>
                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-6 current_page_item {{ request()->is($link) || request()->is($link.'/*') ? 'active': '' }}">
                                                <a class="nav-link" href="{{ $menuItem->link }}">
                                                    {{ $menuItem->name }}
                                                </a>
                                            </li>
                                            @endif
                                    @endforeach
                                @endif

                            
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-6 offset-lg-1">
                        <div class="mobile-menu-information">
                            <div class="company-information">
                                <p>FASHION Avant-garde<br>
                                    @lang("Avant-garde is not simply fashion")<br>
                                    @lang("but also a lifestyle")</p>
                            </div>
                            <div class="adress-information">
                                <p>
                                    <a href="https://www.google.com/maps/place/12+Khu%E1%BA%A5t+Duy+Ti%E1%BA%BFn,+Thanh+Xu%C3%A2n+B%E1%BA%AFc,+Thanh+Xu%C3%A2n,+H%C3%A0+N%E1%BB%99i,+Vi%E1%BB%87t+Nam/@20.9935323,105.8000186,17z/data=!3m1!4b1!4m5!3m4!1s0x3135acbfac608051:0x4d0dd9816456b506!8m2!3d20.9935273!4d105.8022073?hl=vi-VN" target="_blank">
                                        {!! $setting['address'] !!}
                                    </a><br>
                                    <a href="tel:0345736211">{!! $setting['tel'] !!}</a><br>
                                    <a href="mailto:hotro@mediaiconic.com">{!! $setting['email'] !!}</a>
                                </p>
                            </div>
                            <div class="social-media">
                                <ul>
                                    <li>
                                        <a href="" target="_blank" rel="noreferrer noopener">
                                            <img src="/img/facebook.svg" alt="socical-the-dark-house"
                                                class="socical-the-dark-house">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" target="_blank" rel="noreferrer noopener">
                                            <img src="/img/ins.svg" alt="socical-the-dark-house"
                                                class="socical-the-dark-house">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Mobile Menu -->
