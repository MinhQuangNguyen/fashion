<!doctype html>
<html lang="{{ htmlLang() }}" @langrtl dir="rtl" @endlangrtl>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ appName() }} | @yield('title')</title>
    <meta name="author" content="@yield('meta_author', 'The Dark House')">

    <meta property="og:locale" content="{{ htmlLang() }}" />
    <meta property="og:title" content="@yield('title', appName())" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ request()->fullUrl() }}" />
    <meta property="og:description" name="description" content="The Dark House là thương hiệu thời trang tối giản, tương lai, mong muốn mang đến vẻ ngoài sang trọng và hiện đại cho khách hàng." />
    <meta property="og:site_name" content="{{ appURL() }}" />
    <meta property="og:image" content="/img/meta.png" />
    <link rel="icon" sizes="94x94" type="image/gif" href="/img/flaticon.png" />
    @yield('meta')
    @stack('before-styles')
    <link href="/css/frontend.min.css" rel="stylesheet">
    <livewire:styles />
    @stack('after-styles')
</head>

<body>
 <!-- Header -->

 <header id="page-header" class="page-header" data-scroll-section="">
    <div class="container">
        <div class="page-header--wrapper">
            <div class="header-logo">
                <a href="/" title="The Dark House" class="site-logo">
                    <img src="/img/logo.png" alt="The Dark House">
                </a>
            </div>

            <div class="nav">
                <div class="lang">
                    <a href="/lang/vi" class="{{ app()->getLocale() === 'vi' ? 'activeLang' : 'noneActive' }}" onClick="window.location.reload();return true;"><span>VI</span></a>
                     <a href="/lang/en" class="{{ app()->getLocale() === 'en' ? 'activeLang' : 'noneActive' }}" onClick="window.location.reload();return true;"><span>EN</span></a>
                </div>
                
                <div class="nav-icon" data-method="openMobileMenu">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="mouse">
    <div class="cursor-follower" style="transform: translate(-50%, -50%) translate(855px, 163px);">
        <span class="next">Next</span>
        <span class="prev">Prev</span>
        <span class="view">View Project</span>
    </div>
    <!-- <div class="cursor-follower-border"></div> -->
</div>
    
    <!-- /Header -->

<div data-barba="wrapper" aria-live="polite">
    <div id="page-wrapper">
        <main id="barba-wrapper" data-barba="container" data-barba-namespace="home" style="opacity: 1;">
            <div id="js-scroll" data-scroll-container="">
                @include('frontend.includes.header', ['menu' => $menu])

                @yield('content')
                @include('frontend.includes.footer')
            </div>
            <!-- id="js-scroll"-->
        </main>
        <!-- id="barba-wrapper" data-barba="container"  -->
    </div>
    <!-- data-barba="wrapper" -->
</div>



    @stack('before-scripts')
    <script src='/js/wp-polyfill.min.js' id="wp-polyfill-js"></script>
    <script src='/js/hooks.min.js' id="wp-hooks-js"></script>
    <script src='/js/i18n.min.js' id="wp-i18n-js"></script>
    <script src='/js/lodash.min.js' id="lodash-js"></script>
    <script src='/js/api-fetch.min.js' id="wp-api-fetch-js"></script>
    <script src='/js/jquery.min.js' id="jquery-js" defer=""></script>
    <script data-no-minify="1" async="" src='/js/lazyload.min.js'></script>
    <script src='/js/de3067bf44ba56646c1be00cb7f1fbdd.js' data-minify="1" defer=""></script>

    <livewire:scripts />

    @stack('after-scripts')

</body>

</html>
