<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\AboutController;
// use App\Http\Controllers\Frontend\FieldController;
use App\Http\Controllers\Frontend\ProjectController;
use App\Http\Controllers\Frontend\NewsController;
// use App\Http\Controllers\Frontend\TermsController;
use App\Http\Controllers\Frontend\ContactController;
// use App\Http\Controllers\Frontend\RecruitmentController;
use Tabuna\Breadcrumbs\Trail;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */

Route::get('/', [HomeController::class, 'index'])
    ->name('index')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Home'), route('frontend.index'));
    });

Route::get('/404', function () { abort(404);});


Route::get('/gioi-thieu', [AboutController::class, 'index']);
Route::get('/about', [AboutController::class, 'index']);


// Route::get('/linh-vuc/{slug}', [FieldController::class, 'index']);
// Route::get('/field/{slug}', [FieldController::class, 'index']);
// Route::get('/linh-vuc', function () {
//     return redirect('/linh-vuc/bat-dong-san-cao-cap');
// });
// Route::get('/field', function () {
//     return redirect('/field/luxury-real-estate');
// });

Route::get('/bo-suu-tap/{categorySlug}', [NewsController::class, 'index']);
Route::get('/collection/{categorySlug}', [NewsController::class, 'index']);
Route::get('/bo-suu-tap', function () {
    return redirect('/bo-suu-tap/session-2021');
});
Route::get('/collection', function () {
    return redirect('/collection/session-2021');
});

Route::get('bo-suu-tap/{categorySlug}/{slug}.html', [NewsController::class, 'detail']);
Route::get('news/{categorySlug}/{slug}.html', [NewsController::class, 'detail']);

Route::get('/lookbook', [ProjectController::class, 'index']);
Route::get('/lookbook-collection', [ProjectController::class, 'index']);



Route::get('/lien-he', [ContactController::class, 'index'])->name('lien-he.index');
Route::get('/contact', [ContactController::class, 'index'])->name('contact.index');
Route::post('/contact', [ContactController::class, 'store'])->name('contact.store');


Route::get('terms', [TermsController::class, 'index'])
    ->name('pages.terms')
    ->breadcrumbs(function (Trail $trail) {
        $trail->parent('frontend.index')
            ->push(__('Terms & Conditions'), route('frontend.pages.terms'));
    });
