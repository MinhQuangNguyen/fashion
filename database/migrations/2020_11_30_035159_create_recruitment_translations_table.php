<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecruitmentTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recruitment_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('recruitment_id')->unsigned();
            $table->string('locale')->index();

            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->text('content')->nullable();
            $table->string('rank')->nullable();
            $table->string('form')->nullable();
            $table->string('degree')->nullable();
            $table->string('department')->nullable();
            $table->string('location')->nullable();
            $table->string('contact')->nullable();
            $table->text('note')->nullable();

            $table->unique(['recruitment_id','locale']);
            $table->foreign('recruitment_id')->references('id')->on('recruitments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recruitment_translations');
    }
}
